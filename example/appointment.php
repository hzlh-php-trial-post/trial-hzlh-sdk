<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();

use Sdk\Appointment\Model\Appointment;
use Sdk\Snapshot\Model\Snapshot;
use Sdk\Member\Model\Member;
use Sdk\Dictionary\Model\Dictionary;

$appointment = new Appointment();

$appointmentRepository = new Sdk\Appointment\Repository\AppointmentRepository();
$appointment = $appointmentRepository->scenario($appointmentRepository::FETCH_ONE_MODEL_UN)->fetchOne(2);

$loanProductSnapshot = new Snapshot(9);
$appointment->setLoanProductSnapshot($loanProductSnapshot);

$member = new Member(1);
$appointment->setMember($member);

$appointment->setLoanObject(1);  // 贷款对象 1 企业 2 个人
$appointment->setLoanAmount(13);  // 贷款金额
$appointment->setLoanTerm(2);  // 贷款期限
$appointment->setAttachments(array(
    array("index"=>0,"attachments"=>array(array("name"=>"附件名称11","identify"=>"附件地址11.jpg"))),
    array("index"=>1,"attachments"=>array(array("name"=>"附件名称111","identify"=>"附件地址111.jpg")))
));

$appointment->setContactsInfo(
    new Sdk\Appointment\Model\ContactsInfo(
        '吴飞宁',
        '18629630203',
        '北京市,高新区',
        '长安3号'
    )
);
$appointment->setLoanPurposeDescribe('');
$appointment->setCreditReports(array(
    array("name"=>"征信报告名称","identify"=>"征信报告地址.pdf")
));
$appointment->setAuthorizedReports(array(1,2));

$appointment->setGuarantyStyle(new Dictionary(462));
$appointment->setGuarantyStyleType(new Dictionary(459));
$appointment->setLoanPurpose(new Dictionary(460));

// var_dump($appointment->add());die;

$appointment->setId(3);

// $appointment->getTransactionInfo()->setTransactionTime("法定工作日 12:00-17:00");
// $appointment->getTransactionInfo()->setTransactionArea("陕西省,西安市,雁塔区");
// $appointment->getTransactionInfo()->setTransactionAddress("长延堡街道");
// $appointment->getTransactionInfo()->setContactName("张三");
// $appointment->getTransactionInfo()->setContactPhone("029-871233004");
// $appointment->getTransactionInfo()->setRemark("注意事项");
// $appointment->getTransactionInfo()->setCarryData(array(0,1));
// $appointment->getTransactionInfo()->setCreditMinLine(12);
// $appointment->getTransactionInfo()->setCreditMaxLine(14);
// $appointment->getTransactionInfo()->setCreditValidityStartTime('1542174930');
// $appointment->getTransactionInfo()->setCreditValidityStartTime('1542174930');
// var_dump($appointment->approve());

// $appointment->setRejectReason(new Dictionary(461));
// $appointment->setRejectReasonDescribe('数据不正规');
// var_dump($appointment->reject());

$appointment->setStatus(2);
$appointment->getLoanResultInfo()->setLoanFailReason("不好看");
$appointment->getLoanResultInfo()->setLoanAmount(0);
$appointment->getLoanResultInfo()->setLoanTerm(0);
$appointment->getLoanResultInfo()->setRepaymentMethod(0);
$appointment->getLoanResultInfo()->setLoanResultStatus(-2);
var_dump($appointment->completed());

// var_dump($appointment->resubmit());
// var_dump($appointment->revoke());
// var_dump($appointment->deletes());

//  var_dump($appointmentRepository->scenario($appointmentRepository::FETCH_ONE_MODEL_UN)->fetchOne(3)); exit();//done
//  var_dump($appointmentRepository->fetchList(array(1, 2, 3))); exit();//done
// exit();
