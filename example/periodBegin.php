<?php
include '../vendor/autoload.php';
require '../Core.php';

$core = Marmot\Core::getInstance();
$core->initCli();


use Sdk\periodBegin\Model\PeriodBegin;
use Sdk\periodBegin\Repository\PeriodBeginRepository;
use Sdk\AccountSubject\Repository\AccountSubjectRepository;
use Sdk\AccountTemplate\Repository\AccountTemplateRepository;
use Sdk\Statistical\Adapter\StaticsTrialBalanceCountAdapter;
use Sdk\Statistical\Repository\StatisticalRepository;

// periodBegin 新增
//$periodBegin = new PeriodBegin();
//$id = 1;
// $accountSubjectRepository = new AccountSubjectRepository();
// $accountSubject = $accountSubjectRepository->fetchOne($id);
// $accountTemplateRepository = new AccountTemplateRepository();
// $accountTemplate = $accountTemplateRepository->fetchOne($id);
// $periodBegin->setPeriodBeginBalance('100');
// $periodBegin->setAccumulatedDebitAmount('100');
// $periodBegin->setAccumulatedCreditAmount('100');
// $periodBegin->setAccountSubject($accountSubject);
// $periodBegin->setAccountTemplate($accountTemplate);
//var_dump($periodBegin->add());//done

//编辑
// $id = 1;

// $periodBeginRepository = new PeriodBeginRepository();
// $periodBegin = $periodBeginRepository->fetchOne($id);
// $periodBegin->setAccumulatedDebitAmount('110');
// $periodBegin->setAccumulatedCreditAmount('110');

// var_dump($periodBegin->edit());//done

//试算平衡

$trialBalanceCountAdapter = new StaticsTrialBalanceCountAdapter();
$statisticalRepository = new StatisticalRepository($trialBalanceCountAdapter);

$filter['accountTemplateId'] = 1;

var_dump($statisticalRepository->analyse($filter));//done

// $periodBeginRepository = new PeriodBeginRepository();
// $periodBegin = $periodBeginRepository->fetchOne($id);

// var_dump($periodBegin->trialBalance());//done

// var_dump($periodBegin->offStock());//done
// var_dump($periodBegin->onShelf());//done
// var_dump($periodBeginRepository->fetchOne(1));//done
// var_dump($periodBeginRepository->fetchList(array(1,2,3)));//done
