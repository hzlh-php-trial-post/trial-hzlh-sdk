# 贷款产品 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [编辑](#编辑)
	* [上架](#上架)
	* [下架](#下架)
	* [关闭](#关闭)
	* [撤销](#撤销)
	* [删除](#删除)
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例            | 描述       |
| :---:       | :----:     | :----:            | :----:          |:----:     |
| title       | string     | 是                | 出具信用等级证书    | 贷款产品标题   |
| enterpriseName       | string     | 是                | 企业名称    | 企业名称   |
| cover        | array      |  否    | array("name"=>"贷款产品封面","identify"=>"1.jpg")   | 贷款产品封面  |
| productObject        | array      |  是    | array(1,2))   | 贷款对象  |
| guarantyStyles        | array      |  是    | array(1,2))   | 担保方式  |
| labels        | array      |  是    | array(1,2))   | 产品特点  |
| minLoanPeriod       | int     | 是                | 1    | 最短放款期限   |
| maxLoanPeriod       | int     | 是                | 1    | 最长放款期限   |
| supportCity        | array      |  是    | array("陕西","西安"))   | 支持城市  |
| minLoanAmount       | float     | 是                | 1    | 最小可贷额度   |
| maxLoanAmount       | float     | 是                | 1    | 最大可贷额度   |
| minLoanTerm       | int     | 是                | 1    | 最小贷款期限   |
| maxLoanTerm       | int     | 是                | 1    | 最大贷款期限   |
| loanTermUnit       | int     | 是                | 1    | 贷款期限单位   |
| loanInterestRate       | float     | 是                | 0.107    | 贷款利率   |
| loanInterestRateUnit       | int     | 是                | 1    | 贷款利率单位   |
| repaymentMethods        | array      |  是    | array(1,2))   | 还款方式  |
| isSupportEarlyRepayment       | int     | 是                | 1    | 是否支持提前还款   |
| earlyRepaymentTerm       | int     | 是                | 1    | 提前还款期限   |
| isExistEarlyRepaymentCost       | int     | 是                | 1    | 是否存在提前还款费用   |
| applicationMaterial       | string     | 是                | 申请材料    | 申请材料   |
| applicationCondition       | string     | 是                | 申请条件    | 申请条件   |
| contract     | array      |  否    | array("name"=>"贷款产品合同","identify"=>"1.pdf")   | 贷款产品合同  |
| application     | array      |  否    | array("name"=>"贷款申请书","identify"=>"1.pdf")   | 贷款申请书  |
| enterprise   | Enterprise |                 |                     | 所属企业  |
| snapshot   | Snapshot |                 |                     |  金融产品快照 |
| loanMonthInterestRate       | float     | 是                | 0.107    | 贷款月利率   |
| volume       | int        |                  |                     | 申请量    |
| volumeSuccess       | int        |                  |                     | 申请成功量    |
| attentionDegree| int      |                  |                     | 关注度    |
| applyStatus     | int         |                   | 0                         | 审核状态  (0待审核 2审核通过 -2审核驳回)|
| status          | int         |                   |                          | 状态 (0上架 -2下架 -4撤销 -6关闭 -8删除)     |
| rejectReason    | string      | 是                |                                              | 驳回原因         |
| updateTime      | int         |                  | 1535444931                                    | 更新时间         |
| creditTime      | int         |                  | 1535444931                                    | 创建时间         |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$loanProduct = $this->getRepository()->scenario(LoanProductRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
loanProduct: LoanProduct
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
$loanProductList = $this->getRepository()->scenario(LoanProductRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
loanProductList: {
	0 => [
		LoanProduct //object
	],
	1 => [
		LoanProduct //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
list($count, $loanProductList) = $this->getRepository()->scenario(LoanProductRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
loanProductList: {
	0 => [
		LoanProduct //object
	],
	1 => [
		LoanProduct //object
	],
}
```

### <a name="新增">新增</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"loanProducts",
        "attributes"=>array(
            "title"=>"维修洗衣机贷款产品",
            "cover"=>array("name"=>"维修洗衣机贷款产品", "identify"=>"3.jpg"),
            "guarantyStyles"=>array(1,2),
            "supportCity"=>array("陕西", "上海"),
            "repaymentMethods"=>array(1,2),
            "applicationMaterial"=>"申请资料",
            "applicationCondition"=>"申请条件",
            "contract"=>array("name"=>"维修洗衣机贷款产品合同", "identify"=>"3.pdf"),
            "application"=>array("name"=>"维修洗衣机贷款产品申请书", "identify"=>"3.pdf"),
            "productObject"=>array(1,2),
            "loanInterestRate"=>"0.145",
            "loanInterestRateUnit"=>1,
            "minLoanAmount"=>"10",
            "maxLoanAmount"=>"20",
            "minLoanPeriod"=>1,
            "maxLoanPeriod"=>3,
            "minLoanTerm"=>1,
            "maxLoanTerm"=>2,
            "loanTermUnit"=>1,
            "isSupportEarlyRepayment"=>1,
            "earlyRepaymentTerm"=>1,
            "isExistEarlyRepaymentCost"=>0
        ),
        "relationships"=>array(
            "labels"=>array(
                "data"=>array(
                    array("type"=>"labels","id"=>4),
                    array("type"=>"labels","id"=>7),
                )
            ),
            "enterprise"=>array(
                "data"=>array(
                    array("type"=>"enterprises","id"=>2)
                )
            ),
        )
    )
);
```

**调用示例**

```
$loanProduct = $this->getLoanProduct();
$loanProduct = $this->executeAction();

$enterprise = $this->fetchEnterprise();
$loanProduct->setEnterprise(;

return $loanProduct->add();
```

### <a name="编辑">编辑</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"loanProducts",
        "attributes"=>array(
            "title"=>"维修洗衣机贷款产品",
            "cover"=>array("name"=>"维修洗衣机贷款产品", "identify"=>"3.jpg"),
            "guarantyStyles"=>array(1,2),
            "supportCity"=>array("陕西", "上海"),
            "repaymentMethods"=>array(1,2),
            "applicationMaterial"=>"申请资料",
            "applicationCondition"=>"申请条件",
            "contract"=>array("name"=>"维修洗衣机贷款产品合同", "identify"=>"3.pdf"),
            "application"=>array("name"=>"维修洗衣机贷款产品申请书", "identify"=>"3.pdf"),
            "productObject"=>array(1,2),
            "loanInterestRate"=>"0.145",
            "loanInterestRateUnit"=>1,
            "minLoanAmount"=>"10",
            "maxLoanAmount"=>"20",
            "minLoanPeriod"=>1,
            "maxLoanPeriod"=>3,
            "minLoanTerm"=>1,
            "maxLoanTerm"=>2,
            "loanTermUnit"=>1,
            "isSupportEarlyRepayment"=>1,
            "earlyRepaymentTerm"=>1,
            "isExistEarlyRepaymentCost"=>0
        ),
        "relationships"=>array(
            "labels"=>array(
                "data"=>array(
                    array("type"=>"labels","id"=>4),
                    array("type"=>"labels","id"=>7),
                )
            )
        )
    )
);
```

**调用示例**

```
$loanProduct = $this->fetchLoanProduct();
$loanProduct = $this->executeAction();

$loanProduct->edit();
```

### <a name="上架">上架</a>

**调用示例**

```
$loanProduct = $this->getRepository()->fetchOne(int $id);

$loanProduct->onShelf();
```

### <a name="下架">下架</a>

**调用示例**

```
$loanProduct = $this->getRepository()->fetchOne(int $id);

$loanProduct->offStock();
```

### <a name="关闭">关闭</a>

**调用示例**

```
$loanProduct = $this->getRepository()->fetchOne(int $id);

$loanProduct->close();
```

### <a name="撤销">撤销</a>

**调用示例**

```
$loanProduct = $this->getRepository()->fetchOne(int $id);

$loanProduct->revoke();
```

### <a name="删除">删除</a>

**调用示例**

```
$loanProduct = $this->getRepository()->fetchOne(int $id);

$loanProduct->deletes();
```

### <a name="重新编辑">重新编辑</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"loanProducts",
        "attributes"=>array(
            "title"=>"维修洗衣机贷款产品",
            "cover"=>array("name"=>"维修洗衣机贷款产品", "identify"=>"3.jpg"),
            "guarantyStyles"=>array(1,2),
            "supportCity"=>array("陕西", "上海"),
            "repaymentMethods"=>array(1,2),
            "applicationMaterial"=>"申请资料",
            "applicationCondition"=>"申请条件",
            "contract"=>array("name"=>"维修洗衣机贷款产品合同", "identify"=>"3.pdf"),
            "application"=>array("name"=>"维修洗衣机贷款产品申请书", "identify"=>"3.pdf"),
            "productObject"=>array(1,2),
            "loanInterestRate"=>"0.145",
            "loanInterestRateUnit"=>1,
            "minLoanAmount"=>"10",
            "maxLoanAmount"=>"20",
            "minLoanPeriod"=>1,
            "maxLoanPeriod"=>3,
            "minLoanTerm"=>1,
            "maxLoanTerm"=>2,
            "loanTermUnit"=>1,
            "isSupportEarlyRepayment"=>1,
            "earlyRepaymentTerm"=>1,
            "isExistEarlyRepaymentCost"=>0
        ),
        "relationships"=>array(
            "labels"=>array(
                "data"=>array(
                    array("type"=>"labels","id"=>4),
                    array("type"=>"labels","id"=>7),
                )
            )
        )
    )
);
```
**调用示例**

```
$loanProduct = $this->fetchLoanProduct();
$loanProduct = $this->executeAction();

$loanProduct->resubmit();
```

### <a name="审核通过">审核通过</a>

**调用示例**

```
$loanProduct = $this->getRepository()->fetchOne(int $id);

$loanProduct->approve();
```

### <a name="审核驳回">审核驳回</a>

**参数**

```json
$data = array(
	"data"=>array(
		"type"=>"loanProducts",
		"attributes"=>array(
			"rejectReason"=>'编号重复'
		)
	)
);
```

**调用示例**

```
$loanProduct = $this->getRepository()->fetchOne(int $id);

$loanProduct->setRejectReason();

$loanProduct->reject();
```
