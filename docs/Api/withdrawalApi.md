# 提现 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [提现](#提现)
	* [确认转账完成](#确认转账完成)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例              | 描述       |
| :---:       | :----:     | :----:            | :----:           |:----:     |
| transactionNumber | string  |                | 1                | 交易流水号   |
| number      | string        |                | 1                | 交易单号   |
| memberAccount  | MemberAccount |  是         |                   | 所属账户  |
| bankCard    | BankCard   |  是               |                   | 银行卡    |
| amount      | string     | 是                | 100               | 提现金额  |
| serviceCharge| string     | 是                | 100               | 提现手续费  |
| transferVoucher | array  | 是                |                   | 转账凭证  |
| cellphone | string  | 是                |                   | 申请人手机号  |
| status      | int        |                   |                   | 状态     |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$withdrawal = $this->getRepository()->scenario(WithdrawalRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
withdrawal: Withdrawal
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
$withdrawalList = $this->getRepository()->scenario(WithdrawalRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
withdrawalList: {
	0 => [
		Withdrawal //object
	],
	1 => [
		Withdrawal //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
list($count, $withdrawalList) = $this->getRepository()->scenario(WithdrawalRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
withdrawalList: {
	0 => [
		Withdrawal //object
	],
	1 => [
		Withdrawal //object
	],
}
```

### <a name="提现">提现</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"withdrawals",
        "attributes"=>array(
            "amount"=>"1000",
            "paymentPassword"=>"123456"
        ),
        "relationships"=>array(
            "bankCard"=>array(
                "data"=>array(
                    array("type"=>"bankCards","id"=>7)
                )
            ),
            "memberAccount"=>array(
                "data"=>array(
                    array("type"=>"memberAccounts","id"=>7)
                )
            ),
        )
    )
);
```

**调用示例**

```
$withdrawal = $this->getWithdrawal();

$withdrawal->setMemberAccount();
$withdrawal->setBankCard();
$withdrawal->setAmount();
$withdrawal->setPaymentPassword();

$withdrawal->withdrawal();
```

### <a name="确认转账完成">确认转账完成</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"withdrawals",
        "attributes"=>array(
            "transferVoucher"=>array(
                "name"=>"转账凭证", "identify"=>"3.jpg"
            ),
            'serviceCharge'=>1
        )
    )
);
```

**调用示例**

```
$withdrawal =  $this->getRepository()->fetchOne(int $id);

$withdrawal->setServiceCharge();
$withdrawal->setTransferVoucher();

$withdrawal->transferCompleted();
```
