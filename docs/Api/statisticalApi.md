# 统计分析接口示例

---

### 申请量高的9个企业的产品数量和申请量统计(金融超市首页)

路由

	通过GET传参
	/statisticals/staticsApplyEnterprise

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"enterprise_id": "1",
						"loan_product_count": "12",
						"application_volume": "19"
					},
					{
						"enterprise_id": "2",
						"loan_product_count": "11",
						"application_volume": "18"
					},
					{
						"enterprise_id": "3",
						"loan_product_count": "10",
						"application_volume": "17"
					},
					{
						"enterprise_id": "4",
						"loan_product_count": "9",
						"application_volume": "16"
					},
					{
						"enterprise_id": "5",
						"loan_product_count": "8",
						"application_volume": "15"
					},
					{
						"enterprise_id": "6",
						"loan_product_count": "7",
						"application_volume": "14"
					},
					{
						"enterprise_id": "7",
						"loan_product_count": "6",
						"application_volume": "13"
					},
					{
						"enterprise_id": "8",
						"loan_product_count": "6",
						"application_volume": "12"
					},
					{
						"enterprise_id": "9",
						"loan_product_count": "6",
						"application_volume": "1"
					},
				]
			}
		}
	}

### 活跃度高的9个企业的服务数量和服务雇主统计(服务超市首页)

路由

	通过GET传参
	/statisticals/staticsActiveEnterprise

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"enterprise_id": "19",
						"service_count": "8",
						"service_employer": "7"
					},
					{
						"enterprise_id": "17",
						"service_count": "7",
						"service_employer": "57"
					},
					{
						"enterprise_id": "45",
						"service_count": "7",
						"service_employer": 0
					},
					{
						"enterprise_id": "65",
						"service_count": "5",
						"service_employer": "7"
					},
					{
						"enterprise_id": "227",
						"service_count": "5",
						"service_employer": "42"
					},
					{
						"enterprise_id": "22",
						"service_count": "5",
						"service_employer": "41"
					},
					{
						"enterprise_id": "787",
						"service_count": "5",
						"service_employer": 0
					},
					{
						"enterprise_id": "847",
						"service_count": "5",
						"service_employer": 0
					},
					{
						"enterprise_id": "196",
						"service_count": "4",
						"service_employer": 0
					}
				]
			}
		}
	}

### 统计成为服务商不同状态的数量

路由

	通过GET传参
	/statisticals/staticsServiceAuthenticationCount

	1、检索条件
	    1.1 filter[enterpriseId] | 根据企业id搜索

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"pending_total": "1", //待审核
						"approve_total": "1", //已通过
						"reject_total": "0" //已驳回
					}
				]
			}
		}
	}

### 统计需求不同审核状态的数量

路由

	通过GET传参
	/statisticals/staticsServiceRequirementCount

	1、检索条件
	    1.1 filter[memberId] | 根据用户id搜索

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"all_total": "13", //全部
						"pending_total": "12", //待审核
						"approve_total": "1", //已通过
						"reject_total": "0" //已驳回
					}
				]
			}
		}
	}

### 统计服务不同审核状态的数量

路由

	通过GET传参
	/statisticals/staticsServiceCount

	1、检索条件
	    1.1 filter[enterpriseId] | 根据企业id搜索

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"all_total": "2", // 全部
						"shelf_total": "0", //上架
						"stock_total": "0", //下架
						"pending_total": "2", //待审核
						"reject_total": "0" //已驳回
					}
				]
			}
		}
	}

### 统计买家优惠券记录不同状态的数量

路由

	通过GET传参
	/statisticals/staticsMemberCouponCount

	1、检索条件
	    1.1 filter[memberId] | 根据用户id搜索

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"oa_total": "1",//平台
                    	"store_total": "2"//店铺
					}
				]
			}
		}
	}

### 优惠券统计

路由

	通过GET传参
	/statisticals/couponStatistics

	1、检索条件
	    1.1 filter[releaseType] | 根据发布人类型搜索，0:平台，2:商家
	    1.2	filter[enterpriseId] | 根据企业ID搜索
	    1.2 filter[id] 根据优惠券ID搜索

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"receiveRate": "100", //优惠券领取率
						"useRate": "100", //优惠券使用率
						"costEffectivenessRatio": "100", //优惠券费效比
						"pullingRate": "100", //优惠券拉新率
					}
				]
			}
		}
	}

### 企业成交量

路由

	通过GET传参
	/statisticals/staticsEnterpriseOrderVolume

	1、检索条件
	    1.1 filter[enterpriseIds] | 根据企业id搜索,支持多id搜索

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"seller_enterprise_id": "8",
						"order_volume": "1"
					},
					{
						"seller_enterprise_id": "9",
						"order_volume": "5"
					},
					{
						"seller_enterprise_id": "10",
						"order_volume": "12"
					}
				]
			}
		}
	}

### 统计贷款产品不同审核状态的数量

路由

	通过GET传参
	/statisticals/staticsLoanProductCount

	1、检索条件
	    1.1 filter[enterpriseId] | 根据企业id搜索

### 接口返回示例

	{
		"data": {
			"type": "statisticals",
			"id": "0",
			"attributes": {
				"result": [
					{
						"all_total": "2", // 全部
						"shelf_total": "0", //上架
						"stock_total": "0", //下架
						"pending_total": "1", //待审核
						"reject_total": "0", //已驳回
					}
				]
			}
		}
	}