# 服务分类API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增服务分类](#新增服务分类)
	* [编辑服务分类](#编辑服务分类)
	* [启用](#启用)
	* [禁用](#禁用)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称     		| 类型		 | 请求参数是否是必填 | 示例            | 描述       |
| :---:            | :----:     | :------:      |:------------:      |:-------:       |
| name             | string     | 是            | 人力资源             | 名称            |
| qualificationName| string     | 是            | 服务许可证           | 资质认证名称      |
| isQualification  | int        | 是            | 0                  | 是否需要资质认证   |
| commission       | float      | 是            | 2                  | 交易服务费        |
| status           | int        | 是            |                    | 状态             |
| updateTime       | int        |               | 1535444931         | 更新时间         |
| creditTime       | int        |               | 1535444931         | 创建时间         |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
SERVICECATEGORY_FETCH_ONE
```
**请求示例**

```
$serviceCategory = $this->getRepository()->scenario(ServiceCategoryRepository::SERVICECATEGORY_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
serviceCategory: ServiceCategory
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
SERVICECATEGORY_LIST
```
**请求示例**

```
$serviceCategoryList = $this->getRepository()->scenario(ServiceCategoryRepository::SERVICECATEGORY_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
serviceCategoryList: {
	0 => [
		ServiceCategory //object
	],
	1 => [
		ServiceCategory //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
SERVICECATEGORY_LIST
```
**请求示例**

```
list($count, $serviceCategoryList) = $this->getRepository()->scenario(ServiceCategoryRepository::SERVICECATEGORY_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
serviceCategoryList: {
	0 => [
		ServiceCategory //object
	],
	1 => [
		ServiceCategory //object
	],
}
```
### <a name="新增服务分类">新增服务分类</a>

**参数**

```
$data = array(
	"data"=>array(
        "type"=>"serviceCategories",
        "attributes"=>array(
            "name"=>"分类图片测试",
            "qualificationName"=>"",
            "commission"=>2,
            "isQualification"=>0,
            "status"=>0
        ),
        "relationships"=> array(
            "parentCategory"=> array(
                "data"=> array(
                    array(
                        "type"=> "parentCategories",
                        "id"=> 1
                    )
                )
            )
        )   
    ),
);
```
**调用示例**

```
$serviceCategory = $this->getServiceCategory();

$serviceCategory->setParentCategory($parentCategory);
$serviceCategory->setName($command->name);
$serviceCategory->setQualificationName($command->qualificationName);
$serviceCategory->setIsQualification($command->isQualification);
$serviceCategory->setCommission($command->commission);
$serviceCategory->setStatus($command->status);

$serviceCategory->add();

```
### <a name="编辑服务分类">编辑服务分类</a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"serviceCategories",
		"attributes"=>array(
			"name"=>"分类名称",
			"qualificationName"=>"资质认证名称",
            "commission"=>2,
            "status"=>0,
			"isQualification"=>0 //是否需要资质认证 0 不需要 2 需要
		)
	)
);
```
**调用示例**

```
$serviceCategory = $this->getRepository()->fetchOne(int $id);

$serviceCategory->setName($command->name);
$serviceCategory->setQualificationName($command->qualificationName);
$serviceCategory->setIsQualification($command->isQualification);
$serviceCategory->setCommission($command->commission);
$serviceCategory->setStatus($command->status);

$serviceCategory->edit();

```
### <a name="启用"> 启用 </a>

**调用示例**

```
$serviceCategory = $this->getRepository()->fetchOne(int $id);

$serviceCategory->enable();

```
### <a name="禁用"> 禁用 </a>

**调用示例**

```
$serviceCategory = $this->getRepository()->fetchOne(int $id);

$serviceCategory->disable();

```