# 职称 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [编辑](#编辑)
	* [删除](#删除)
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| enterpriseName  | string     |               | 陕西传媒网络有限公司                            | 企业名称         |
| realName        | string     |               | 张三                                         | 企业员工姓名      |
| certificates    | array      | 是            | array(array("name"=>"资质认证图片", "identify"=>"1.jpg"))| 职业资格证书     |
| parentCategory  | Dictionary |  是            |                                              | 父级职称         |
| category        | Dictionary |  是            |                                              | 二级职称         |
| type            | Dictionary |  是            |                                              | 三级职称         |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 0                            | 状态   (0 正常 -2 删除)          |
| applyStatus     | int        |               | 0                            | 审核状态  (0待审核 2审核通过 -2审核驳回)|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$professionalTitle = $this->getRepository()->scenario(ProfessionalTitleRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
professionalTitle: ProfessionalTitle
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
$professionalTitleList = $this->getRepository()->scenario(ProfessionalTitleRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
professionalTitleList: {
	0 => [
		ProfessionalTitle //object
	],
	1 => [
		ProfessionalTitle //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
list($count, $professionalTitleList) = $this->getRepository()->scenario(ProfessionalTitleRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
professionalTitleList: {
	0 => [
		ProfessionalTitle //object
	],
	1 => [
		ProfessionalTitle //object
	],
}
```

### <a name="新增">新增</a>

**参数**

```json
$data = array(
    "data"=>array(
        array(
            "type"=>"professionalTitles",
            "attributes"=>array(
                "certificates" => array(array("name"=>"证书1", "identify"=>"证书1.jpg"))
            ),
            "relationships"=>array(
                "type"=>array(
                    "data"=>array(
                        array("type"=>"dictionaries","id"=>6)
                    )
                ),
                "staff"=>array(
                    "data"=>array(
                        array("type"=>"staffs","id"=>2)
                    )
                ),
            ),
        ),
        array(
            "type"=>"professionalTitles",
            "attributes"=>array(
                "certificates" => array(array("name"=>"证书2", "identify"=>"证书1.jpg"))
            ),
            "relationships"=>array(
                "type"=>array(
                    "data"=>array(
                        array("type"=>"dictionaries","id"=>7)
                    )
                ),
                "staff"=>array(
                    "data"=>array(
                        array("type"=>"staffs","id"=>2)
                    )
                ),
            ),
        ),
    )
);
```

**调用示例**

```
$professionalTitle = $this->getProfessionalTitle();
$professionalTitle = $this->executeAction();

...

return $professionalTitle->batchAdd();
```

### <a name="编辑">编辑</a>

**参数**

```json
$data = array("data"=>array(
    "type"=>"professionalTitles",
    "attributes"=>array(
        "certificates" => array(array("name"=>"证书9", "identify"=>"证书9.jpg"))
    ),
    "relationships"=>array(
        "type"=>array(
            "data"=>array(
                array("type"=>"dictionaries","id"=>9)
            )
        ),
    )
));
```

**调用示例**

```
$professionalTitle = $this->fetchProfessionalTitle();
$professionalTitle = $this->executeAction();

$professionalTitle->edit();
```

### <a name="删除">删除</a>

**调用示例**

```
$professionalTitle = $this->getRepository()->fetchOne(int $id);

$professionalTitle->deletes();
```

### <a name="重新编辑">重新编辑</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"professionalTitles",
        "attributes"=>array(
            "certificates" => array(array("name"=>"证书6", "identify"=>"证书6.jpg"))
        ),
        "relationships"=>array(
            "type"=>array(
                "data"=>array(
                    array("type"=>"dictionaries","id"=>6)
                )
            ),
        )
    )
);
```

**调用示例**

```
$professionalTitle = $this->fetchLoanProduct();
$professionalTitle = $this->executeAction();

$professionalTitle->resubmit();
```

### <a name="审核通过">审核通过</a>

**调用示例**

```
$professionalTitle = $this->getRepository()->fetchOne(int $id);

$professionalTitle->approve();
```

### <a name="审核驳回">审核驳回</a>

**参数**

```json
$data = array(
	"data"=>array(
		"type"=>"professionalTitles",
		"attributes"=>array(
			"rejectReason"=>"驳回原因"
		)
	)
);
```

**调用示例**

```
$professionalTitle = $this->getRepository()->fetchOne(int $id);

$professionalTitle->setRejectReason();

$professionalTitle->reject();
```