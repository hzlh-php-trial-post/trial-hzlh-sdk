# 用户账户API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
    * [获取单条数据场景](#获取单条数据场景)
    * [获取多条数据场景](#获取多条数据场景)
    * [根据检索条件查询数据](#根据检索条件查询数据) 

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称         | 类型        |请求参数是否必填  |  示例                       | 描述             |
| :---:           | :----:     | :------:       |:------------:              |:-------:        |
| accountBalance  | string     |                |   200                      | 主账户余额        |
| frozenAccountBalance | string |               |   200                      | 冻结账户余额      |
| updateTime      | int        |               | 1535444931                  | 更新时间          |
| creditTime      | int        |               | 1535444931                  | 创建时间          |
| status          | int        |               | 0                           | 状态 0正常 -2 冻结 |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
MEMBER_ACCOUNT_FETCH_ONE
```
**请求示例**

```
$memberAccount = $this->getRepository()->scenario(MemberAccountRepository::MEMBER_ACCOUNT_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
memberAccount: MemberAccount
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
MEMBER_ACCOUNT_LIST
```
**请求示例**

```
$memberAccountList = $this->getRepository()->scenario(MemberAccountRepository::MEMBER_ACCOUNT_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
memberAccountList: {
    0 => [
        MemberAccount //object
    ],
    1 => [
        MemberAccount //object
    ],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
MEMBER_ACCOUNT_LIST
```
**请求示例**

```
list($count, $memberAccountList) = $this->getRepository()->scenario(MemberAccountRepository::MEMBER_ACCOUNT_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
memberAccountList: {
    0 => [
        MemberAccount //object
    ],
    1 => [
        MemberAccount //object
    ],
}
```