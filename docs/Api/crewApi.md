# 员工接口API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [登录](#登录)
	* [修改密码](#修改密码)
	* [启用](#启用)
	* [禁用](#禁用)

---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| cellphone       | string     | 是            | 13720406329                                  | 手机号           |
| userName        | string     | 是            | 13720406329                                  | 用户名           |
| realName        | string     | 是            | 张三三                                        | 姓名             |
| avatar          | array      | 否            | array("name"=>"用户头像", "identify"=>"1.jpg")| 用户头像          |
| gender          | int        | 否            | 1                                            | 性别 (1 男 2 女)  |
| password        | string     | 是            |                                              | 密码             |
| oldPassword     | string     | 是            |                                              | 旧密码           |
| workNumber      | string     | 是            | 12345                                        | 工号             |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 0                                            | 状态  (0启用 -2禁用)|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(CrewRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(CrewRepository::LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(CrewRepository::LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="登录">登录</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"crews",
        "attributes"=>array(
            "cellphone"=>"13720406329",
            "password"=>"password123456",
        )
    )
);
```
**调用示例**

```
$crew = new Sdk\Crew\Model\Crew;

$crew->setCellphone('13720406329);
$crew->setPassword('password1234567');

$crew->signIn();

```
### <a name="修改密码">修改密码</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"crews",
        "attributes"=>array(
            "oldPassword"=>"password1234567",
            "password"=>"password123456",
        )
    )
);
```
**调用示例**

```

$crew = $this->getCrewRepository()->fetchOne(int $id);

$crew->setOldPassword('password1234567');
$crew->setPassword('password1234567');

$crew->updatePassword();

```

### <a name="启用">启用</a>

**调用示例**

```
$crew = $this->getRepository()->fetchOne(int $id);

$crew->enable();

```
### <a name="禁用">禁用</a>

**调用示例**

```
$crew = $this->getRepository()->fetchOne(int $id);

$crew->disable();

```