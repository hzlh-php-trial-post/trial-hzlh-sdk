# 数据标签API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)	
   * [新增标签](#新增标签)
	* [编辑标签](#编辑标签)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称     		| 类型		 | 请求参数是否是必填 | 示例            | 描述       |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 医疗健康                                         | 名称            |
| remark          | string     | 否            |                                               | 备注            |
| pid            | int      | 是            |  | 父级id       |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 0                                            | 状态 |

## <a name="接口示例">接口示例</a>

### <a name="新增标签">新增标签</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"tags",
        "attributes"=>array(
            "name"=>"名称",
            "remark"=>"备注",
            "pid" => 0
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>员工id)
                )
            )
        )
    )
);
```
**调用示例**

```
$tag = $this->gettag();

$tag->setName($command->name);
$tag->setRemark($command->remark);
$tag->setPid($command->pid);;
$crew->setCrew($crew);

$tag->add();

```
### <a name="编辑标签">编辑标签</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"tags",
        "attributes"=>array(
            "name"=>"名称",
            "remark"=>"备注",
            "pid" => 1
        )
    )
);
```
**调用示例**

```
$tag = $this->gettag();

$tag->setName($command->name);
$tag->setRemark($command->remark);
$tag->setPid($command->pid);

$tag->edit();

```