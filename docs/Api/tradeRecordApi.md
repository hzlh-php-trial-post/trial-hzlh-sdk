# 交易流水 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
    * [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例              | 描述       |
| :---:       | :----:     | :----:            | :----:           |:----:      |
| referenceId        | int     |                | 1              | 关联对象id    |
| reference          | ITradeAble     |                |               | 关联对象      |
| tradeTime        | int      |               |       1558886577           | 交易时间    |
| tradeType      | int        |                   |      1            | 交易记录类型       |
| tradeMoney      | float        |                   |      100            | 交易金额       |
| debtor      | string        |                   |      微信账户            | 付款方       |
| creditor      | string        |                   |      汇众联合账户            | 收款方      |
| balance      | float        |                   |     100             | 账户余额      |
| comment      | string        |                   |     微信支付充值             | 备注      |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$tradeRecord = $this->getRepository()->scenario(TradeRecordRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
tradeRecord: TradeRecord
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$tradeRecordList = $this->getRepository()->scenario(TradeRecordRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
tradeRecordList: {
	0 => [
		TradeRecord //object
	],
	1 => [
		TradeRecord //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

*场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
list($count, $tradeRecordList) = $this->getRepository()->scenario(TradeRecordRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
tradeRecordList: {
	0 => [
		TradeRecord //object
	],
	1 => [
		TradeRecord //object
	],
}
```
