# 财务问题API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [本人删除](#本人删除)
	* [平台删除](#平台删除)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称              | 类型        |请求参数是否必填  |  示例                                                          | 描述            |
| :---:                | :----:     | :------:       |:------------:                                                 |:-------:       |
| number               | string     |  自动生成       | Q201903021                                                    | 编号             |
| content              | string     | 是             | 宜昌高新区（自贸片区）政务服务局 关于调整建设项目中介服务机构备选库的通知 | 问题内容         |
| category             | Label      | 是             | 1                                                             | 问题分类         |
| pageViews        	   | int        |                 | 0                                                            | 浏览量          |
| commentCount         | int        |                 | 0                                                            | 评论数          |
| attentionDegree      | int        |                 | 0                                                            | 关注度          |
| member               | Member     | 是              | 1                                                             | 提问用户        |
| financialAnswer      | FinancialAnswer |            | 1                                                             | 答案           |
| status        	   | int        |                 | 0                                                             | 状态  (0正常 -2删除)|
| deleteType           | int        |                 | 0 正常, 1 本人删除, 2 平台删除                                   | 删除类型        |
| deleteReason         | string     |                 | 不符合要求                                                      | 删除原因        |
| deletedBy            | Member/Crew| 是               | 1                                                             | 删除人          |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$financialQuestion = $this->getRepository()->scenario(FinancialQuestionRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
financialQuestion: FinancialQuestion
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LIST_MODEL_UN
```
**请求示例**

```
$financialQuestionList = $this->getRepository()->scenario(FinancialQuestionRepository::LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
financialQuestionList: {
	0 => [
		FinancialQuestion //object
	],
	1 => [
		FinancialQuestion //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LIST_MODEL_UN
```
**请求示例**

```
list($count, $financialQuestionList) = $this->getRepository()->scenario(FinancialQuestionRepository::LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
financialQuestionList: {
	0 => [
		FinancialQuestion //object
	],
	1 => [
		FinancialQuestion //object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"financialQuestions",
        "attributes"=>array(
            "content"=>"问题内容"
        ),
        "relationships"=>array(
            "category"=>array(
                "data"=>array(
                    array("type"=>"labels", "id"=>1)
                )
            ),
            "member"=>array(
                "data"=>array(
                    array("type"=>"members", "id"=>1)
                )
            )
        )
    )
);
```
**调用示例**

```
$financialQuestion = $this->getFinancialQuestion();

$financialQuestion->setCategory(new Label(2));
$financialQuestion->setContent('嘻嘻嘻');
$financialQuestion->setMember(new Member());

$financialQuestion->add();

```
### <a name="本人删除"> 本人删除 </a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"financialQuestions",
        "attributes"=>array(
            "deleteReason"=>"",
            "deleteType"=>1
        )
        "relationships"=>array(
            "deleteBy"=>array(
                "data"=>array(
                    array("type"=>"deleteBys", "id"=>1)
                )
            ),
        )
    )
);
```
**调用示例**

```
$financialQuestion = $this->getRepository()->fetchOne(int $id);

$financialQuestion->setDeleteInfo(
	new Sdk\FinancialQA\Model\DeleteInfo(
		1,
		'',
		new Member()
	)
);

$financialQuestion->deletes()

```
### <a name="平台删除"> 平台删除 </a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"financialQuestions",
        "attributes"=>array(
            "deleteReason"=>"删除原因",
            "deleteType"=>2
        )
        "relationships"=>array(
            "deleteBy"=>array(
                "data"=>array(
                    array("type"=>"deleteBys", "id"=>1)
                )
            ),
        )
    )
);
```
**调用示例**

```
$financialQuestion = $this->getRepository()->fetchOne(int $id);

$financialQuestion->setDeleteInfo(
	new Sdk\FinancialQA\Model\DeleteInfo(
		2,
		'删除原因',
		new Crew()
	)
);

$financialQuestion->platformDelete()

```