# 银行卡 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [绑定银行卡](#绑定银行卡)
	* [解绑银行卡](#解绑银行卡)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称              | 类型        |请求参数是否必填  |  示例                                                   | 描述            |
| :---:                | :----:     | :------:       |:------------:                                          |:-------:       |
| accountType          | int        | 是             | 1 对公  2 对私                                           | 账户类型        |
| cardType             | int        | 是             | 1 借记卡 2 贷记卡                                         | 银行卡类型      |
| memberAccount        | MemberAccount | 是          |                                                         | 账户           |
| bank                 | Bank       | 是             |                                                         | 银行           |
| cardNumber           | string     | 是             |  6212263700000856445                                    | 银行卡号        |
| bankBranchArea       | string     | 是             | 陕西省,西安市,雁塔区                                       | 开户支行区域     |
| bankBranchAddress    | string     | 是             | 长延堡街道雁环路                                           | 开户支行地址     |
| cellphone            | string     | 是             | 13623098087                                             | 预留手机号       |
| cardholderName       | string     | 是             | 张三                                                  	| 持卡对象名称     |
| licence              | array     	| 否             | 图片名称,地址                                             | 开户许可证       |
| unbindReason         | int        | 否             | 0 其他,1 我担心资金安全, 2 我不再使用该银行卡                 | 解绑原因         |
| status        	   | int        |                | 0正常, -2删除                                            | 状态            |
## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$bankCard = $this->getRepository()->scenario(BankCardRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
bankCard: BankCard
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
```

**请求示例**

```
$bankCardList = $this->getRepository()->scenario(BankCardRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
bankCardList: {
	0 => [
		BankCard //object
	],
	1 => [
		BankCard //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
```

**请求示例**

```
list($count, $bankCardList) = $this->getRepository()->scenario(BankCardRepository::PORTAL_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
bankCardList: {
	0 => [
		BankCard //object
	],
	1 => [
		BankCard //object
	],
}
```

### <a name="绑定银行卡">绑定银行卡</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"bankCards",
        "attributes"=>array(
            "cardholderName"=>"持卡对象名称",
            "cardNumber"=>"卡号",
            "bankBranchArea"=>"开户支行区域",
            "bankBranchAddress"=>"开户支行地址",
            "cellphone"=>"预留手机号",
            "licence"=>array("neme"=>"开户许可证","identify"=>"licence.jpg"),
            "paymentPassword"=>123456,
            "accountType"=>1,
            "cardType"=>1
        ),
        "relationships"=>array(
            "memberAccount"=>array(
                "data"=>array(
                    array("type"=>"memberAccounts", "id"=>1)
                )
            ),
            "bank"=>array(
                "data"=>array(
                    array("type"=>"banks", "id"=>1)
                )
            )
        )
    )
);
```

**调用示例**

```
$bankCard = $this->getBankCard();
$bankCard->setAccountType();
$bankCard->setCardType();
$bankCard->setCardNumber();
$bankCard->setBankBranchArea();
$bankCard->setBankBranchAddress();
$bankCard->setCellphone();
$bankCard->setCardholderName();
$bankCard->setLicence();
$bankCard->setBank();
$bankCard->setMemberAccount();
$bankCard->setPaymentPassword();

$bankCard->bind();
```

### <a name="解绑银行卡">解绑银行卡</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"bankCards",
        "attributes"=>array(
            "unbindReason"=>1,
            "paymentPassword"=>123456,
        ),
    )
);
```

**调用示例**

```
$bankCard = $this->fetchBankCard();

$bankCard->setUnbindReason();
$bankCard->setPaymentPassword();

$bankCard->unBind();
```
