# 预约申请API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [撤销](#撤销)
	* [删除](#删除)
---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例            | 描述       |
| :---:       | :----:     | :----:            | :----:          |:----:     |
| number       | string     | 是                | SQ202004131    | 申请编号    |
| loanProductSnapshot | Snapshot |             |             | 金融产品快照信息  |
| loanProduct  | LoanProduct |                 |             | 金融产品信息  |
| loanProductTitle  | string     | 是                | 出具信用等级证书    | 金融产品标题   |
| enterpriseName  | string     | 是                | 出具信用等级证书    | 企业名称   |
| score           | int     | 是                | 出具信用等级证书    | 资料完成度   |
| borrowerName    | string     | 是                | 张三    | 借贷人名称   |
| sellerEnterprise   | Enterprise |                 |                     | 卖家企业信息 |
| member   | Member |                 |                     | 申请用户信息  |
| applicant   | Member |                 |                     | 申请对象信息  |
| loanObject     | int         |                   | 0        | 贷款对象  (1  企业 2 个人)|
| loanAmount       | float     | 是                | 100    | 贷款金额   |
| loanTerm       | int     | 是                | 1    | 贷款期限   |
| loanTermUnit       | int     | 是                | 1    | 贷款期限单位(1月 2 天)   |
| guarantyStyle       | Dictionary     | 是                | 1    | 担保方式   |
| guarantyStyleType       | Dictionary     | 是                | 1    | 担保方式类型   |
| loanPurpose       | Dictionary     | 是                | 1    | 贷款用途   |
| loanPurposeDescribe    | string     | 是                | 张三    | 贷款用途描述   |
| attachments        | array      |  是    |                      | 申请资料附件  |
| creditReports        | array      |  是    |                      | 征信报告  |
| authorizedReports        | array      |  是    |                      | 授权报表  |
| contactsName     | string     | 是            | 张三三                                        | 联系人             |
| contactsCellphone| string      | 是            | 13720406754                                 | 联系电话          |
| contactsArea     | string     | 是            | 陕西省,西安市,雁塔区                           | 联系地址地区          |
| contactsAddress  | string     | 是            | 雁环路                                      | 联系地址的详细地址     |
| transactionInfo       | TransactionInfo     | 是                |     | 办理信息   |
| loanResultInfo     | LoanResultInfo         | 是                 |                         | 贷款结果信息 |
| status          | int         |                   |                          | 状态 (0 待审核 2 已通过 -2已驳回 -4撤销 4完成-贷款成功 6完成-贷款失败 -6 驳回/完成删除 -8撤销删除)     |
| rejectReason    | Dictionary      | 是                |                                              | 驳回原因         |
| rejectReasonDescribe    | string      | 是                |                                              | 驳回原因描述        |
| updateTime      | int         |                  | 1535444931                                    | 更新时间         |
| creditTime      | int         |                  | 1535444931                                    | 创建时间         |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(AppointmentRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(AppointmentRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(AppointmentRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="新增">新增</a>

**参数**

```json
$data = array("data"=>array(
		"type"=>"appointments",
		"attributes"=>array(
			"contactsName"=>"联系人",
			"contactsCellphone"=>"联系电话",
			"contactsArea"=>"联系地区",
			"contactsAddress"=>"联系详细地址",
			"loanAmount"=>"5",
			"loanTerm"=>2,
			"loanObject"=>1,
			"loanPurposeDescribe"=>"贷款用途描述",
			"attachments"=>array(
				array("index"=>"0","attachments"=>array(array("name"=>"附件名称","identify"=>"附件地址.jpg"))),
				array("index"=>1,"attachments"=>array(array("name"=>"附件名称11","identify"=>"附件地址11.jpg")))
			),
			"creditReports"=>array(
				array("name"=>"征信报告名称","identify"=>"征信报告地址.pdf")
			),
			"authorizedReports"=>array(1,2)
		),
		"relationships"=>array(
			"snapshot"=>array(
				"data"=>array(
					array("type"=>"snapshots","id"=>10)
				)
			),
			"member"=>array(
				"data"=>array(
					array("type"=>"members","id"=>2)
				)
			),
			"guarantyStyle"=>array(
				"data"=>array(
					array("type"=>"guarantyStyles","id"=>458)
				)
			),
			"guarantyStyleType"=>array(
				"data"=>array(
					array("type"=>"guarantyStyleTypes","id"=>0)
				)
			),
			"loanPurpose"=>array(
				"data"=>array(
					array("type"=>"loanPurposes","id"=>459)
				)
			),
		)
	)
);
```
**调用示例**

```
$appointment = $this->getAppointment();

$appointment->setLoanAmount();
$appointment->setLoanTerm();
$appointment->setLoanObject();
$appointment->setLoanAmount();
$appointment->setAttachments();
$appointment->setContactsInfo(); //用户信息值对象
$appointment->setSnapshot();

$appointment->add();

```
### <a name="重新编辑">重新编辑</a>

**参数**

```json
$data = array("data"=>array(
		"type"=>"appointments",
		"attributes"=>array(
			"contactsName"=>"联系人",
			"contactsCellphone"=>"联系电话",
			"contactsArea"=>"联系地区",
			"contactsAddress"=>"联系详细地址",
			"loanAmount"=>"100",
			"loanTerm"=>12,
			"loanObject"=>1,
			"loanPurposeDescribe"=>"贷款用途描述",
			"attachments"=>array(
				array("index"=>"贷款产品所需资料的索引","attachments"=>array(array("name"=>"附件名称","identify"=>"附件地址.jpg"))),
				array("index"=>1,"attachments"=>array(array("name"=>"附件名称","identify"=>"附件地址.jpg")))
			),
			"creditReports"=>array(
				array("name"=>"征信报告名称","identify"=>"征信报告地址.pdf")
			),
			"authorizedReports"=>array(1,2)
		)
		"relationships"=>array(
			"guarantyStyle"=>array(
				"data"=>array(
					array("type"=>"guarantyStyles","id"=>2)
				)
			),
			"guarantyStyleType"=>array(
				"data"=>array(
					array("type"=>"guarantyStyleTypes","id"=>2)
				)
			),
			"loanPurpose"=>array(
				"data"=>array(
					array("type"=>"loanPurposes","id"=>2)
				)
			),
		)
	)
);
```
**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->setLoanAmount();
$appointment->setLoanTerm();
$appointment->setLoanObject();
$appointment->setLoanAmount();
$appointment->setAttachments();
$appointment->setContactsInfo(); //用户信息值对象

$appointment->resubmit();

```
### <a name="审核通过">审核通过</a>

**参数**

```json
$data = array("data"=>array(
        "type"=>"appointments",
        "attributes"=>array(
        )
        "relationships"=>array(
            "transactionInfo"=>array(
                "data"=>array(
                    "type"=>"transactionInfos",
                    "attributes"=>array(
                        "transactionTime" => "办理时间",
                        "transactionArea" => "办理地区",
                        "transactionAddress" => "办理详细地址",
                        "contactName" => "联系人",
                        "contactPhone" => "联系电话",
                        "remark" => "注意事项",
                        "carryData" => "携带资料",
                        "creditMinLine" => "最小授信额度",
                        "creditMaxLine" => "最大授信额度",
                        "creditValidityStartTime" => "授信有效期开始时间",
                        "creditValidityEndTime" => "授信有效期结束时间",
                    )
                )
            )
        )
    )
);
```
**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->setRemark();

$appointment->approve();

```
### <a name="审核驳回">审核驳回</a>

**参数**

```json
$data = array("data"=>array(
		"type"=>"appointments",
		"attributes"=>array(
			"rejectReasonDescribe"=>"驳回原因描述"
		)
		"relationships"=>array(
			"rejectReason"=>array(
				"data"=>array(
					array("type"=>"rejectReasons","id"=>2)
				)
			)
		)
	)
);
```
**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->setRejectReason();

$appointment->reject();

```
### <a name="完成">完成</a>

**参数**

```json
$data = array("data"=>array(
        "type"=>"appointments",
        "attributes"=>array(
        )
        "relationships"=>array(
            "loanResultInfo"=>array(
                "data"=>array(
                    "type"=>"loanResultInfos",
                    "attributes"=>array(
                        "loanFailReason" => "贷款失败原因",
                        "loanAmount" => "贷款金额",
                        "loanTerm" => "贷款期限",
                        "repaymentMethod" => "还款方式",
                        "status" => "贷款结果状态",
                    )
                )
            )
        )
    )
);
```
**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->setLoanFailReason();
$appointment->setLoanContractNumber();
$appointment->setLoanStatus();

$appointment->completed();

```
### <a name="撤销">撤销</a>

**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->revoke();

```
### <a name="删除">删除</a>

**调用示例**

```
$appointment = $this->getRepository()->fetchOne(int $id);

$appointment->deletes();

```