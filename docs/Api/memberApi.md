# 用户API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
    * [获取单条数据场景](#获取单条数据场景)
    * [获取多条数据场景](#获取多条数据场景)
    * [根据检索条件查询数据](#根据检索条件查询数据) 
    * [注册](#注册)
    * [登录](#登录)
    * [忘记密码](#忘记密码)
    * [修改密码](#修改密码)
    * [编辑个人信息](#编辑个人信息)
    * [换绑手机号](#换绑手机号)
    * [启用](#启用)
    * [禁用](#禁用)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称                       | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:                         | :----:     | :------:       |:------------:                                |:-------:       |
| cellphone                     | string     | 是             | 13720406329                                  | 手机号           |
| userName                      | string     | 是             | 13720406329                                  | 用户名           |
| realName                      | string     | 是             | 张三三                                        | 姓名             |
| avatar                        | array      | 否             | array("name"=>"用户头像", "identify"=>"1.jpg")| 用户头像          |
| gender                        | int        | 否             | 1                                            | 性别 (1 男 2 女)  |
| password                      | string     | 是             |                                              | 密码             |
| oldPassword                   | string     | 是             |                                              | 旧密码           |
| nickName                      | string     | 是             | 张三三                                        | 昵称             |
| birthday                      | string     | 是             | 1994-09-03                                   | 出生日期          |
| area                          | string     | 否             | 陕西省,西安市,雁塔区                            | 联系地址地区       |
| address                       | string     | 否             | 长延堡街道                                     | 联系地址详细地址    |
| briefIntroduction             | string     | 否             | 简介                                          | 简介             |
| naturalAuthenticatingState    | int        |                | 0                                            | 实名认证状态  (0未认证 2已认证)|
| enterpriseAuthenticatingState | int        |                | 0                                            | 企业认证状态  (0未认证 2已认证)|
| updateTime                    | int        |                | 1535444931                                   | 更新时间         |
| creditTime                    | int        |                | 1535444931                                   | 创建时间         |
| status                        | int        |                | 0                                            | 状态  (0启用 -2禁用)|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
MEMBER_FETCH_ONE
```
**请求示例**

```
$member = $this->getRepository()->scenario(MemberRepository::MEMBER_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
member: Member
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
MEMBER_LIST
```
**请求示例**

```
$memberList = $this->getRepository()->scenario(MemberRepository::MEMBER_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
memberList: {
    0 => [
        Member //object
    ],
    1 => [
        Member //object
    ],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
MEMBER_LIST
```
**请求示例**

```
list($count, $memberList) = $this->getRepository()->scenario(MemberRepository::MEMBER_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
memberList: {
    0 => [
        Member //object
    ],
    1 => [
        Member //object
    ],
}
```
### <a name="注册">注册</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"members",
        "attributes"=>array(
            "cellphone"=>"13720406329",
            "password"=>"password123456",
        )
    )
);
```
**调用示例**

```
$member = $this->getMember();

$member->setCellphone();
$member->setPassword();

$member->signUp();

```
### <a name="登录">登录</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"members",
        "attributes"=>array(
            "cellphone"=>"13720406329",
            "password"=>"password123456",
        )
    )
);
```
**调用示例**

```
$member = $this->getMember();

$member->setCellphone();
$member->setPassword();

$member->signIn();

```
### <a name="忘记密码">忘记密码</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"members",
        "attributes"=>array(
            "cellphone"=>"13720406329",
            "password"=>"password123456",
        )
    )
);
```
**调用示例**

```
$member = $this->getMember();

$member->setCellphone();
$member->setPassword();

$member->resetPassword();

```
### <a name="修改密码">修改密码</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"members",
        "attributes"=>array(
            "oldPassword"=>"password1234567",
            "password"=>"password123456",
        )
    )
);
```
**调用示例**

```
$member = $this->getRepository()->fetchOne(int $id);

$member->setOldPassword();
$member->setPassword();

$member->updatePassword();

```
### <a name="编辑个人信息">编辑个人信息</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"members",
        "attributes"=>array(
            "nickName"=>"昵称",
            "area"=>"地区",
            "address"=>"详细地址",
            "briefIntroduction"=>"简介",
            "birthday"=>"出生日期",
            "avatar"=>array('name'=>'头像', 'identify'=>'头像地址'),
            "gender"=>"性别"
        )
    )
);
```
**调用示例**

```
$member = $this->getRepository()->fetchOne(int $id);

$member->setNickName();
$member->setAvatar();
$member->setGender();
$member->setBirthday();
$member->setArea();
$member->setAddress();
$member->setBriefIntroduction();

$member->edit();

```
### <a name="换绑手机号">换绑手机号</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"members",
        "attributes"=>array(
            "cellphone"=>"新手机号"
        )
    )
);
```
**调用示例**

```
$member = $this->getRepository()->fetchOne(int $id);

$member->setCellphone();

$member->updateCellphone();

```
### <a name="启用"> 启用 </a>

**调用示例**

```
$member = $this->getRepository()->fetchOne(int $id);

$member->enable();

```
### <a name="禁用"> 禁用 </a>

**调用示例**

```
$member = $this->getRepository()->fetchOne(int $id);

$member->disable();

```