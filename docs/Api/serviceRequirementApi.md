# 我的需求API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [发布需求](#发布需求)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [撤销](#撤销)
	* [关闭](#关闭)
	* [删除](#删除)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称         | 类型        |请求参数是否必填  	|  示例                                        | 描述            |
| :---:           | :----:      | :------:          |:------------:                                |:-------:       |
| number          | string      |                   | XQ201905031                                  | 服务需求编号     |
| member          | Member      | 是                |                                              | 所属用户         |
| serviceCategory | ServiceCategory| 是             |                                              | 服务需求分类     |
| title           | string      | 是                | 出具信用等级证书                                | 服务需求标题     |
| detail          | array     	| 是                | 文本内容，图片地址                              | 服务需求详情     |
| contactName     | string      | 是                |  张三                                         | 联系人          |
| contactPhone    | string      | 是                |  13720405678                                 | 联系电话         |
| minPrice        | float       | 否                |  10000                                       | 最小价格         |
| maxPrice        | float       | 否                |  20000                                       | 最大价格         |
| validityStartTime | int       | 否                |  1535444931                                  | 有效期开始时间    |
| validityEndTime | int         | 否                |  1535444931                                  | 有效期结束时间    |
| applyStatus     | int         |                   | 0                         | 审核状态  (0待审核 2审核通过 -2审核驳回)|
| status          | int         |                   |                          | 状态 (0正常 -2 撤销 -4关闭 -6删除)     |
| rejectReason    | string      | 是                |                                              | 驳回原因         |
| updateTime      | int         |                  | 1535444931                                    | 更新时间         |
| creditTime      | int         |                  | 1535444931                                    | 创建时间         |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
SERVICE_REQUIREMENT_FETCH_ONE
```
**请求示例**

```
$serviceRequirement = $this->getRepository()->scenario(ServiceRequirementepository::SERVICE_REQUIREMENT_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
serviceRequirement: ServiceRequirement
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
OA_SERVICE_REQUIREMENT_LIST
PORTAL_SERVICE_REQUIREMENT_LIST
```
**请求示例**

```
$serviceRequirementList = $this->getRepository()->scenario(ServiceRequirementRepository::PORTAL_SERVICE_REQUIREMENT_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
serviceRequirementList: {
	0 => [
		ServiceRequirement //object
	],
	1 => [
		ServiceRequirement //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
OA_SERVICE_REQUIREMENT_LIST
PORTAL_SERVICE_REQUIREMENT_LIST
```
**请求示例**

```
list($count, $serviceRequirementList) = $this->getRepository()->scenario(ServiceRequirementRepository::OA_SERVICE_REQUIREMENT_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
serviceRequirementList: {
	0 => [
		ServiceRequirement //object
	],
	1 => [
		ServiceRequirement //object
	],
}
```
### <a name="发布需求">发布需求</a>

**参数**

```
$data = array("data"=>array(
		"type"=>"serviceRequirements",
		"attributes"=>array(
			"title"=>"维修洗衣机服务需求求",
		    "detail"=>array(array("type"=>"text", "value"=>"文本内容")),
			"contactName"=>"张三",
			"contactPhone"=>"13720406320",
			"minPrice"=>"1000",
			"maxPrice"=>"2000",
			"validityStartTime"=>"1554811362",
			"validityEndTime"=>"1554811362",
			"number"=>"XQ201912211"
		),
		"relationships"=>array(
			"serviceCategory"=>array(
				"data"=>array(
					array("type"=>"serviceCategories","id"=>5)
				)
			),
			"member"=>array(
				"data"=>array(
					array("type"=>"members","id"=>2)
				)
			),
		)
	)
);
```
**调用示例**

```
$requirements = $this->getRequirements();

$requirements->setServiceCategory($serviceCategory);
$requirements->setTitle($command->title);
$requirements->setDetail($command->detail);
$requirements->setMinPrice($command->minPrice);
$requirements->setMaxPrice($command->maxPrice);
$requirements->setValidityStartTime($command->validityStartTime);
$requirements->setValidityEndTime($command->validityEndTime);
$requirements->setContactName($command->contactName);
$requirements->setContactPhone($command->contactPhone);

$requirements->add();

```
### <a name="审核通过"> 审核通过 </a>

**调用示例**

```
$requirements = $this->getRepository()->fetchOne(int $id);

$requirements->approve();

```
### <a name="审核驳回"> 审核驳回 </a>

**参数**

```
$data = array(
	"data"=>array(
		"type"=>"serviceRequirements",
        "attributes"=>array(
        	"rejectReason"=>"审核驳回原因"
        )
    )
);
```

**调用示例**

```
$requirements = $this->getRepository()->fetchOne(int $id);

$requirements->setRejectReason($command->rejectReason);

$requirements->reject();

```
### <a name="撤销"> 撤销 </a>

**调用示例**

```
$requirements = $this->getRepository()->fetchOne(int $id);

$requirements->revoke();

```
### <a name="关闭"> 关闭 </a>

**调用示例**

```
$requirements = $this->getRepository()->fetchOne(int $id);

$requirements->close();

```
### <a name="删除"> 删除 </a>

**调用示例**

```
$requirements = $this->getRepository()->fetchOne(int $id);

$requirements->deletes();

```