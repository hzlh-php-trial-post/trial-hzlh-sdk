# 审核表金融机构认证接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [重新认证](#重新认证)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)

## <a name="参数说明">参数说明</a>
     
| 英文名称                       | 类型        |请求参数是否必填  |  示例                                         | 描述            |
| :---:                         | :----:     | :------:       |:------------:                                 |:-------:       |
| enterpriseName                | string     | 是             | 信息传媒有限公司                                | 企业名称         |
| code                          | string     | 是             | 923456091291023456                            | 机构编码         |
| licence                       | array      | 是             | array("name"=>"licence", "identify"=>"1.jpg") | 金融许可证       |
| organizationsCategory         | int        | 是             | 1                                             | 机构类型         |
| organizationsCategoryParent   | int        | 是             | 1                                             | 机构类型父级     |
| organizationCode              | string     | 是             | 923456091291023456                            | 机构代码         |
| organizationCodeCertificate   | array      | 是             | array("name"=>"licence", "identify"=>"1.jpg") | 金融机构代码证    |
| updateTime                    | int        |                | 1535444931                                    | 更新时间         |
| creditTime                    | int        |                | 1535444931                                    | 创建时间         |
| applyStatus     | int        |               | 0                            | 状态 0 待审核, 2 审核通过, -2 审核驳回|

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、include请求参数
	    1.1 include=relation | 获取用户的include数据
	2、fields[TYPE]请求参数
	    2.1 fields[unAuditedFinanceAuthentications]
	    2.2 fields[enterprises]
	3、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	unAuditedFinanceAuthentications/1?fields[enterprise]=name

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/unAuditedFinanceAuthentications/{id:\d+}

示例

	unAuditedFinanceAuthentications/1

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/unAuditedFinanceAuthentications/{ids:\d+,[\d,]+}

示例

	unAuditedFinanceAuthentications/1,2,3

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/unAuditedFinanceAuthentications

	1、检索条件
	    1.1 filter[relation] | 根据企业ID搜索
	    1.2 filter[title] | 根据企业名称搜索
	    1.3 filter[applyStatus] | 根据审核状态搜索
	2、排序
		2.1 sort=-id | -id 根据id倒序 | id 根据id正序
		2.2 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
		2.3 sort=-status | -status 根据状态倒序 | status 根据状态正序

示例

	unAuditedFinanceAuthentications?sort=-id&page[number]=1&page[size]=20

### <a name="重新认证">重新认证</a>

路由

	通过POST传参
	/unAuditedFinanceAuthentications/{id:\d+}/resubmit

示例

    $data = array("data"=>array(
            "type"=>"unAuditedFinanceAuthentications",
            "attributes"=>array(
                "code"=>"机构编码",
                "licence"=>array(
                    'name' => '金融许可证', 'identify' => '金融许可证.jpg'
                ),
                "organizationsCategory": 2,
                "organizationsCategoryParent": 2,
                "organizationCode"=>"机构代码",
                "organizationCodeCertificate"=>array(
                    'name' => '金融机构代码证', 'identify' => '金融机构代码证.jpg'
                )
            )
        )
    );

	$response = $client->request(
        'PATCH',
        'unAuditedFinanceAuthentications/{id:\d+}/resubmit',
        [
            'headers'=>['Content-Type' => 'application/vnd.api+json'],
            'json' => $data
        ]
	);

### <a name="审核通过">审核通过示例</a>

路由

	通过PATCH传参
	/unAuditedFinanceAuthentications/{id:\d+}/approve

示例

	unAuditedFinanceAuthentications/1/approve

### <a name="审核驳回">审核驳回示例</a>

路由

	通过PATCH传参
	/unAuditedFinanceAuthentications/{id:\d+}/reject

示例

    $data = array("data"=>array(
						"type"=>"unAuditedFinanceAuthentications",
                        "attributes"=>array(
                        	"rejectReason"=>"审核驳回原因"
                        )
                    )
     		);
	$response = $client->request(
        'PATCH',
        'unAuditedFinanceAuthentications/1/reject',
        [
            'headers'=>['Content-Type' => 'application/vnd.api+json'],
            'json' => $data
        ]
    );
