# 商家家优惠劵API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [删除](#删除)
---

## <a name="所需参数说明">所需参数说明</a>
| 英文名称              | 类型        |请求参数是否必填  |  示例                        | 描述            |
| :---:                | :----:     | :------:       |:------------:               |:-------:       |
| name             | string     | 是             | 同城优品30元优惠券                         | 优惠券名称       |
| merchantCouponType            | int     | 是             | 0                  | 0 ：满减券,2 ：折扣券       |
| denomination                 | int     | 是             | 30.00            | 优惠面额,根据type传，type为0时传     |
| discount              | int     | 是             | 7              | 优惠券折扣值,根据type传，type为2时传  |
| useStandard           | int        | 是             |  30                      | 优惠券使用门槛        |
| validityStartTime     | int        | 是              | 1323332232                   | 开始时间 |
| validityEndTime     | int        | 是              | 1323332232                   | 结束时间 |
| issueTotal     | int        | 是              | 9999                   | 优惠券总发行量 |
| perQuota     | int        | 是              | 20                   | 优惠券每人限领 |
| useScenario     | int        | 是              | 1                   | 领取场景（0 : 无限制，1:注册送，2 ：下单送） |
| receivingMode     | int        | 是              | 0                   | 优惠券领取方式（0 : 手动领取，2 ：平台自动发放）	 |
| receivingUsers     | int        | 是              | 2                   | 优惠券可领取用户（全部用户 ：0，普通用户 ：2，vip用户 ：4）
| isSuperposition     | int        | 是              | 2                   | 优惠券是否和店铺优惠券叠加使用（0 ：No，2 ：yes） |
| applyScope     | int        | 是              | 1                   | 优惠券使用范围（1 ：平台通用，2 ：店铺通用，3 ：指定分类， 4 ：指定商品，5 ：指定店铺） |
| applySituation     | int        | 是              | []                   | 适用情况，(平台通用,useScope字端为1时：[]；店铺通用，useScope字端为2时：[enterpriseId]；指定分类，useScope字端为3时：[serviceCategoryIds]；指定商品，useScope字端为4时：[serviceIds]； 指定店铺，useScope字端为5时：[enterpriseIds])
| number     | int        | 是              | YH+10位时间戳+自增长序号                   | 优惠券编号 |
| status     | int        | 是              | 1323332232                   | 状态 （0 ：正常，-2 ：已过期，-4 ：已停用，-6 ：已删除）|
| createTime     | int        | 是              | 1323332232                   | 创建时间 |
| releaseType     | int        | 是              | 0                   | 判断当前发布优惠券的是平台还是某一店铺 (0: 平台，2: 店铺） |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(MerchantCouponRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(MerchantCouponRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(MerchantCouponRepository::PORTAL_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object 
	],
}
```

### <a name="新增">新增</a>

**参数**

```
$data = array(
    "data"=>array(
        "type"=>"releaseCoupons",
        "attributes"=>array(
            "name" => "同城优品30元优惠券",
            "merchantCouponType" => "0",
            "denomination" => "30.00",
            "discount" => "7 ",
            "useStandard" => "30",
            "validityStartTime" => "1323332232",
            "validityEndTime" => "1323332232",
            "issueTotal" => "9999",
            "perQuota" => "20",
            "useScenario"=>"1",
            "receivingMode" => "0",
            "receivingUsers" => "2",
            "isSuperposition" => "2",
            "applyScope" => "1",
            "applySituation" => "[]",
            "releaseType" => "0"
        ),
        "relationships"=>array(
            "reference"=>array(
                "data"=>array(
                    array("type"=>"crews", "id"=>10)
                )
            )
        )
    )
);
```
**调用示例**

```
$crew = $this->getCrewRepository()->fetchOne(int $id);
$enterprise = $this->getEnterpriseRepository()->fetchOne(int $id);
$enterpriseId = [1];

$merchantCoupon = new Sdk\MerchantCoupon\Model\MerchantCoupon;
$merchantCoupon->setName('下单送优惠劵');
$merchantCoupon->setCouponType(2);   // 0 ：满减券,2 ：折扣券
$merchantCoupon->setDenomination(10);    //优惠面额,根据type传，type为0时传
$merchantCoupon->setDiscount(30);     //优惠券折扣值,根据type传，type为2时传
$merchantCoupon->setUseStandard(300);    //优惠券使用门槛 满300
$merchantCoupon->setIssueTotal(1000);    //优惠券总发行量
$merchantCoupon->setPerQuota(1);     //优惠券每人限领
$merchantCoupon->setValidityStartTime(0);
$merchantCoupon->setValidityEndTime(0);
$merchantCoupon->setUseScenario(2);   //领取场景（0 : 无限制，1:注册送，2 ：下单送）
$merchantCoupon->setReceivingMode(2);    //优惠券领取方式（0 : 手动领取，2 ：平台自动发放）
$merchantCoupon->setReceivingUsers(0);   //优惠券可领取用户（全部用户 ：0，新人用户 ：2，vip用户 ：4）
$merchantCoupon->setIsSuperposition(0);   //优惠券是否和店铺优惠券叠加使用（0 ：No，2 ：yes）
$merchantCoupon->setApplyScope(1);    //优惠券使用范围（1 ：平台通用，2 ：店铺通用)）
$merchantCoupon->setApplySituation($enterpriseId);  //平台,applyScope:1 : []；店铺，useScope:2 ：[enterpriseId])
$merchantCoupon->setStatus(0);    //状态 （0 ：正常，-2 ：已过期，-4 ：已停用，-6 ：已删除）
$merchantCoupon->setReleaseType(0);   // (0: 平台，2: 店铺）
$merchantCoupon->setReference($enterprise); // 平台: $crew 店铺: $enterprise

$merchantCoupon->add();

```

### <a name="删除">删除</a>

**调用示例**

```
$merchantCoupon = $this->getRepository()->fetchOne(int $id);

$merchantCoupon->deletes();

```