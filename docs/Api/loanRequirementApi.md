# 贷款需求 SDK 接口示例

## 目录

* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [撤销](#撤销)
	* [删除](#删除)
	* [关闭](#关闭)
	* [接受邀约](#接受邀约)
	* [确认匹配](#确认匹配)
	* [邀请临柜办理](#邀请临柜办理)
	* [完成](#完成)
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)

--

## <a name="所需参数说明">所需参数说明</a>

| 英文名称     | 类型        | 请求参数是否是必填   | 示例            | 描述       |
| :---:       | :----:     | :----:            | :----:          |:----:     |
| number      | string     | 是                | SQ202004131     | 申请编号    |
| title       | string     | 是                | 标题             | 标题       |
| description    | string     | 是                | 描述             | 描述       |
| contactName | string     | 是                | 张三             | 联系人     |
| contactPhone| string     | 是                | 13720406329     | 联系电话    |
| borrowerName| string     | 是                | 张三             | 借贷人名称   |
| area        | string     | 是                | 张三             | 所在地区    |
| loanPurpose | Dictionary | 是                | 1                | 贷款用途   |
| loanPurposeDescribe| string | 是             | 张三             | 贷款用途描述   |
| collateral  | Dictionary | 是                | 1               | 抵押物        |
| collateralDescribe| string | 是              | 张三             | 抵押物描述   |
| labels     | array       | 是                | array(1,2)      | 标签        |
| attachments        | array      |  是    |                      | 可提供资料  |
| validityTime        | int      |  是    |                      | 有效期  |
| loanAmount       | float     | 是                | 100    | 贷款金额   |
| loanTerm       | int     | 是                | 1    | 贷款期限   |
| loanObject     | int         |                   | 0        | 贷款对象  (1  企业 2 个人)|
| isHaveCollateral     | int         |                   | 0        | 是否有抵押物  (0 是 -2 否)|
| isAllowViewCreditInformation     | int         |                   | 0        | 是否允许查看相关信用信息  (0 是 -2 否)|
| member   | Member |                 |                     | 申请用户信息  |
| applicant   | Member |                 |                     | 申请对象信息  |
| tenderInformations       | TenderInformation     | 是                |     | 投标信息   |
| status          | int         |                   |                          | 状态   |
| applyStatus          | int         |                   |                          | 审核状态   |
| closeReason    | string      | 是                |                                              | 关闭原因         |
| rejectReason    | string      | 是                |                                              | 驳回原因         |
| updateTime      | int         |                  | 1535444931                                    | 更新时间         |
| creditTime      | int         |                  | 1535444931                                    | 创建时间         |


## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```

**请求示例**

```
$loanRequirement = $this->getRepository()->scenario(LoanRequirementRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
loanRequirement: LoanRequirement
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
$loanRequirementList = $this->getRepository()->scenario(LoanRequirementRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
loanRequirementList: {
	0 => [
		LoanRequirement //object
	],
	1 => [
		LoanRequirement //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```

**请求示例**

```
list($count, $loanRequirementList) = $this->getRepository()->scenario(LoanRequirementRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```

**接口返回示例**

```
type: array
count: 2,
loanRequirementList: {
	0 => [
		LoanRequirement //object
	],
	1 => [
		LoanRequirement //object
	],
}
```

### <a name="新增">新增</a>

**参数**

```json
$data = array("data"=>array(
        "type"=>"loanRequirements",
        "attributes"=>array(
            "title"=>"发布需求标题",
            "description"=>"发布需求描述",
            "loanObject"=>1,
            "contactName"=>"联系人",
            "contactPhone"=>"联系电话",
            "area"=>"联系地区",
            "validityTime"=>"1560909890",
            "loanPurposeDescribe"=>"贷款用途描述",
            "collateralDescribe"=>"抵押物描述",
            "loanAmount"=>"5",
            "loanTerm"=>2,
            "isHaveCollateral"=>0,
            "isAllowViewCreditInformation"=>0,
            "attachments"=>array(
                array("name"=>"附件名称","identify"=>"附件地址.jpg"),
                array("name"=>"附件名称11","identify"=>"附件地址11.jpg")
            ),
        ),
        "relationships"=>array(
            "member"=>array(
                "data"=>array(
                    array("type"=>"members","id"=>2)
                )
            ),
            "loanPurpose"=>array(
                "data"=>array(
                    array("type"=>"loanPurposes","id"=>459)
                )
            ),
            "collateral"=>array(
                "data"=>array(
                    array("type"=>"collaterals","id"=>458)
                )
            ),
            "labels"=>array(
                "data"=>array(
                    array("type"=>"labels","id"=>1),
                    array("type"=>"labels","id"=>3),
                )
            )
        )
    )
);
```

**调用示例**

```
$loanRequirement = $this->getLoanRequirement();
$loanRequirement = $this->executeAction();

$member = $this->fetchMember();
$loanPurpose = $this->fetchLoanPurpose();
$collateral = $this->fetchCollateral();
$loanRequirement->setMember();
$loanRequirement->setLoanPurpose();
$loanRequirement->setCollateral();

return $loanRequirement->add();
```

### <a name="撤销">撤销</a>

**调用示例**

```
$loanRequirement = $this->getRepository()->fetchOne(int $id);

$loanRequirement->revoke();
```

### <a name="删除">删除</a>

**调用示例**

```
$loanRequirement = $this->getRepository()->fetchOne(int $id);

$loanRequirement->deletes();
```

### <a name="关闭">关闭</a>

**调用示例**

```
$loanRequirement = $this->getRepository()->fetchOne(int $id);

$loanRequirement->close();
```

### <a name="接受邀约示例">接受邀约示例</a>

**参数**

```json
$data = array("data"=>array(
        "type"=>"loanRequirements",
        "attributes"=>array(
        ),
        "relationships"=>array(
            "tenderInformation"=>array(
                "data"=>array(
                    "type"=>"tenderInformations",
                    "attributes"=>array(
                        "creditMinLine" => "最小授信额度",
                        "creditMaxLine" => "最大授信额度",
                        "creditMinLoanTerm" => "授信最小贷款期限",
                        "creditMaxLoanTerm" => "授信最大贷款期限",
                        "creditValidityTime" => "授信有效期",
                    ),
                    "relationships"=>array(
                        "enterprise"=>array(
                            "data"=>array(
                                array("type"=>"enterprises","id"=>2)
                            )
                        ),
                    )
                )
            )
        )
    )
);
```

**调用示例**

```
$loanRequirement = $this->fetchLoanRequirement();
$loanRequirement = $this->executeAction();

$loanRequirement->bid();
```

### <a name="确认匹配">确认匹配</a>

**调用示例**

```
$loanRequirement = $this->fetchLoanRequirement();
$loanRequirement = $this->executeAction();

$loanRequirement->matching();
```

### <a name="邀请临柜办理">邀请临柜办理</a>

**参数**

```json
$data = array("data"=>array(
        "type"=>"loanRequirements",
        "attributes"=>array(
        ),
        "relationships"=>array(
            "transactionInfo"=>array(
                "data"=>array(
                    "type"=>"transactionInfos",
                    "attributes"=>array(
                        "transactionTime" => "办理时间",
                        "transactionArea" => "办理地区",
                        "transactionAddress" => "办理详细地址",
                        "contactName" => "联系人",
                        "contactPhone" => "联系电话",
                        "remark" => "注意事项",
                        "carryData" => "携带资料"
                    )
                )
            )
        )
    )
);
```

**调用示例**

```
$loanRequirement = $this->fetchLoanRequirement();
$loanRequirement = $this->executeAction();

$loanRequirement->transaction();
```

### <a name="完成">完成</a>

**参数**

```json
$data = array("data"=>array(
        "type"=>"loanRequirements",
        "attributes"=>array(
        ),
        "relationships"=>array(
            "loanResultInfo"=>array(
                "data"=>array(
                    "type"=>"loanResultInfos",
                    "attributes"=>array(
                        "loanFailReason" => "贷款失败原因",
                        "loanAmount" => "贷款金额",
                        "loanTerm" => "贷款期限",
                        "repaymentMethod" => "1",
                        "status" => "-2",

                    )
                )
            )
        )
    )
);
```

**调用示例**

```
$loanRequirement = $this->fetchLoanRequirement();
$loanRequirement = $this->executeAction();

$loanRequirement->completed();
```

### <a name="重新编辑">重新编辑</a>

**参数**

```json
$data = array("data"=>array(
        "type"=>"loanRequirements",
        "attributes"=>array(
            "title"=>"发布需求标题",
            "description"=>"发布需求描述",
            "loanObject"=>1,
            "contactName"=>"联系人",
            "contactPhone"=>"联系电话",
            "area"=>"联系地区",
            "validityTime"=>"1560909890",
            "loanPurposeDescribe"=>"贷款用途描述",
            "collateralDescribe"=>"抵押物描述",
            "loanAmount"=>"5",
            "loanTerm"=>2,
            "isHaveCollateral"=>0,
            "isAllowViewCreditInformation"=>0,
            "attachments"=>array(
                array("name"=>"附件名称","identify"=>"附件地址.jpg"),
                array("name"=>"附件名称11","identify"=>"附件地址11.jpg")
            ),
        ),
        "relationships"=>array(
            "loanPurpose"=>array(
                "data"=>array(
                    array("type"=>"loanPurposes","id"=>459)
                )
            ),
            "collateral"=>array(
                "data"=>array(
                    array("type"=>"collaterals","id"=>458)
                )
            ),
            "labels"=>array(
                "data"=>array(
                    array("type"=>"labels","id"=>1),
                    array("type"=>"labels","id"=>3),
                )
            )
        )
    )
);
```
**调用示例**

```
$loanRequirement = $this->fetchLoanRequirement();
$loanRequirement = $this->executeAction();

$loanRequirement->resubmit();
```

### <a name="审核通过">审核通过</a>

**调用示例**

```
$loanRequirement = $this->getRepository()->fetchOne(int $id);

$loanRequirement->approve();
```

### <a name="审核驳回">审核驳回</a>

**参数**

```json
$data = array(
	"data"=>array(
		"type"=>"loanRequirements",
		"attributes"=>array(
			"rejectReason"=>'编号重复'
		)
	)
);
```

**调用示例**

```
$loanRequirement = $this->getRepository()->fetchOne(int $id);

$loanRequirement->setRejectReason();

$loanRequirement->reject();
```
