# 财政新规API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [编辑](#编辑)
	* [上架](#上架)
	* [下架](#下架)
	* [置顶](#置顶)
	* [取消置顶](#取消置顶)

---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称              | 类型        |请求参数是否必填  |  示例                                                          | 描述            |
| :---:                | :----:     | :------:       |:------------:                                                 |:-------:       |
| title                | string     | 是             | 宜昌高新区（自贸片区）政务服务局 关于调整建设项目中介服务机构备选库的通知  | 标题            |
| number               | string     | 自动生成        | XW201903021                                                    | 财智新规编号         |
| source               | string     | 否             | 中国网客户端                                                     | 新规来源         |
| category             | int        | 是             | 1                                                              | 新闻分类         |
| detail               | array     	| 是             | 文本内容，图片地址                                                  | 财智新规详情     |
| description          | string     | 是             | 财智新规描述                                                        | 财智新规描述     |
| cover			       | array		| 否			 |  array('name'=>'封面图', 'identify'=>'封面.JPG')                   | 财智新规封面图   |
| status        	   | int        |                | 0       | 状态  (0上架 -2下架)  |
| stick	        	   | int        |                | 0       | 状态  (0未置顶 2置顶) |
| collection      	   | int        |                | 0       | 收藏量    |
| pageViews        	   | int        |                | 0       | 浏览量    |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(MoneyWiseNewsRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(MoneyWiseNewsRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
OA_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(MoneyWiseNewsRepository::OA_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```

### <a name="新增">新增</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"news",
        "attributes"=>array(
            "title"=>"政策标题",
            "source"=>"新闻来源",
            "detail"=>array(array("type"=>"text", "value"=>"文本内容")),
            "cover"=>array("name"=>"封面图","identify"=>"封面图.jpg"),
            "description"=>"新闻描述新闻描述新闻描述新闻描述"
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews", "id"=>1)
                )
            ),
            "category"=>array(
                "data"=>array(
                    array("type"=>"dictionaries","id"=>4)
                )
            )
        )
    )
);
```

### <a name="编辑">编辑</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"news",
        "attributes"=>array(
            "title"=>"政策标题",
            "source"=>"新闻来源",
            "detail"=>array(array("type"=>"text", "value"=>"文本内容")),
            "cover"=>array("name"=>"封面图","identify"=>"封面图.jpg"),
            "description"=>"新闻描述新闻描述新闻描述新闻描述"
        ),
        "relationships"=>array(
            "category"=>array(
                "data"=>array(
                    array("type"=>"dictionaries","id"=>4)
                )
            )
        )
    )
);
```

### <a name="上架">上架</a>

**调用示例**

```
$moneyWiseNews = $this->getRepository()->fetchOne(int $id);

$moneyWiseNews->onShelf();

```
### <a name="下架">下架</a>

**调用示例**

```
$moneyWiseNews = $this->getRepository()->fetchOne(int $id);

$moneyWiseNews->offStock();

```

### <a name="置顶">置顶</a>

**调用示例**

```
$moneyWiseNews = $this->getRepository()->fetchOne(int $id);

$moneyWiseNews->top();

```
### <a name="取消置顶">取消置顶</a>

**调用示例**

```
$moneyWiseNews = $this->getRepository()->fetchOne(int $id);

$moneyWiseNews->cancelTop();

```