# 买家优惠劵API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [删除](#删除)
	* [领取优惠券](#领取优惠券)
---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称              | 类型        |请求参数是否必填  |  示例                        | 描述            |
| :---:                | :----:     | :------:       |:------------:               |:-------:       |
| releaseCoupon             | ReleaseCoupon     | 是             |                   | 优惠券对象       |
| member            | Member     | 是             |                   | 账户对象       |
| status     | int        | 是              | 1323332232                   | 状态 （0 ：未使用，2 ：已使用，-2 ：已删除）|
| createTime     | int        | 是              | 1323332232                   | 创建时间 |
| updateTime     | int        | 是              | 1323332232                   | 修改时间 |
| statusTime     | int        | 是              | 1323332232                   | 状态修改时间 |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(CouponRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(CouponRepository::PORTAL_LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object 
	],
	1 => [
		object 
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PORTAL_LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(CouponRepository::PORTAL_LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		coupon //object
	],
	1 => [
		coupon //object
	],
}
```
### <a name="领取优惠券">领取优惠券</a>

**参数**

```
	$data = array(
		"data"=>array(
			"type"=>"memberCoupons",
			"attributes"=>array(
			),
			"relationships"=>array(
				"releaseCoupon"=>array(
					"data"=>array(
						array("type"=>"releaseCoupons", "id"=>1)
					)
				)
				"member"=>array(
					"data"=>array(
						array("type"=>"members", "id"=>1)
					)
				)
			)
	    )
	);
```
**调用示例**

```
$member = $this->getMemberRepository()->fetchOne(int $id);
$merchantCoupon = $this->MerchantCouponRepository()->fetchOne(int $id);

$coupon = new Sdk\Coupon\Model\Coupon;
$coupon->setMember($member);
$coupon->setMerchantCoupon($merchantCoupon);

$coupon->receive();

```

### <a name="删除">删除</a>

**调用示例**

```
$coupon = $this->getRepository()->fetchOne(int $id);

$coupon->deletes();

```