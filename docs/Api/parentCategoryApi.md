# 父级服务分类API文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增服务父级分类](#新增服务父级分类)
	* [编辑服务父级分类](#编辑服务父级分类)

---
## <a name="所需参数说明">所需参数说明</a>
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 人力资源                                       | 名称            |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
PARENTCATEGORY_FETCH_ONE
```
**请求示例**

```
$parentCategory = $this->getRepository()->scenario(ParentCategoryRepository::PARENTCATEGORY_FETCH_ONE)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
parentCategory: ParentCategory
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
PARENTCATEGORY_LIST
```
**请求示例**

```
$parentCategoryList = $this->getRepository()->scenario(ParentCategoryRepository::PARENTCATEGORY_LIST)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
parentCategoryList: {
	0 => [
		ParentCategory //object
	],
	1 => [
		ParentCategory //object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
PARENTCATEGORY_LIST
```
**请求示例**

```
list($count, $parentCategoryList) = $this->getRepository()->scenario(ParentCategoryRepository::PARENTCATEGORY_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
parentCategoryList: {
	0 => [
		ParentCategory //object
	],
	1 => [
		ParentCategory //object
	],
}
```
### <a name="新增服务父级分类">新增服务父级分类</a>

**参数**

```
$data = array("data"=>array(
			"type"=>"parentCategories",
			"attributes"=>array(
				"name"=>"分类名称",
		)
	)
);
```
**调用示例**

```
$parentCategory = $this->getParentCategory();

$parentCategory->setName($command->name);

$parentCategory->add();

```
### <a name="编辑服务父级分类">编辑服务父级分类</a>

**参数**

```
$data = array("data"=>array(
			"type"=>"parentCategories",
			"attributes"=>array(
				"name"=>"分类名称",
		)
	)
);
```
**调用示例**

```
$parentCategory = $this->getRepository()->fetchOne(int $id);

$parentCategory->setName($command->name);

$parentCategory->edit();

```