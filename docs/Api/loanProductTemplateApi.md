# 产品模板 API 文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增模板](#新增模板)
	* [编辑模板](#编辑模板)
	* [删除模板](#删除模板)

## <a name="所需参数说明">所需参数说明</a>

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name            | string     | 是            | 发改委                                         | 名称            |
| file            | array      | 是            | array("name"=>"模板图片", "identify"=>"1.jpg") | 模板示例        |
| fileType        | int        | 是            | 1                                            | 文件类型         |
| enterprise      | Enterprise | 是            |                                              | 企业信息         |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| status          | int        |               | 0                                            | 状态 |

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
LOAN_PRODUCT_TEMPLATE_FETCH_ONE
```
**请求示例**

```
$loanProductTemplate = $this->getRepository()->scenario(LoanProductTemplateRepository::LOAN_PRODUCT_TEMPLATE_FETCH_ONE)->fetchOne(int $id);
```

**接口返回示例**

```
type: object
loanProductTemplate: LoanProductTemplate
```

### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LOAN_PRODUCT_TEMPLATE_LIST
```
**请求示例**

```
$loanProductTemplateList = $this->getRepository()->scenario(LoanProductTemplateRepository::LOAN_PRODUCT_TEMPLATE_LIST)->fetchList(array $ids);
```

**接口返回示例**

```
type: array
loanProductTemplateList: {
	0 => [
		LoanProductTemplate //object
	],
	1 => [
		LoanProductTemplate //object
	],
}
```

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LOAN_PRODUCT_TEMPLATE_LIST
```
**请求示例**

```
list($count, $loanProductTemplateList) = $this->getRepository()->scenario(LoanProductTemplateRepository::LOAN_PRODUCT_TEMPLATE_LIST)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
loanProductTemplateList: {
	0 => [
		LoanProductTemplate //object
	],
	1 => [
		LoanProductTemplate //object
	],
}
```

### <a name="新增模板">新增模板</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"loanProductTemplates",
        "attributes"=>array(
            "name"=>"名称",
            "file"=>array('name'=>'图片名称', 'identify'=>'图片地址.jpg'),
            "fileType"=>1,
        ),
        "relationships"=>array(
            "enterprise"=>array(
                "data"=>array(
                    array("type"=>"enterprises","id"=>企业id)
                )
            )
        )
    )
);
```

**调用示例**

```
$loanProductTemplate = $this->getLoanProductTemplate();
$enterprise = $this->fetchEnterprise($command->enterpriseId);

$loanProductTemplate->setName($command->name);
$loanProductTemplate->setFileType($command->fileType);
$loanProductTemplate->setFile($command->file);
$loanProductTemplate->setEnterprise($enterprise);

$loanProductTemplate->add();
```

### <a name="编辑模板">编辑模板</a>

**参数**

```json
$data = array(
    "data"=>array(
        "type"=>"loanProductTemplates",
        "attributes"=>array(
            "name"=>"名称",
            "file"=>array('name'=>'图片名称', 'identify'=>'图片地址.png'),
            "fileType"=>1,
        )
    )
);
```

**调用示例**

```
$loanProductTemplate = $this->getLoanProductTemplate()->fetchOne(int $id);

$loanProductTemplate->setName($command->name);
$loanProductTemplate->setFileType($command->fileType);
$loanProductTemplate->setFile($command->file);

$loanProductTemplate->edit();
```

### <a name="删除模板">删除模板</a>

**调用示例**

```
$loanProductTemplate = $this->getRepository()->fetchOne(int $id);

$loanProductTemplate->deletes();

```