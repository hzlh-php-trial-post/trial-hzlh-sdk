# 期初API样例文档

## 目录
* [所需参数说明](#所需参数说明)
* [接口示例](#接口示例)
	* [获取单条数据场景](#获取单条数据场景)
	* [获取多条数据场景](#获取多条数据场景)
	* [根据检索条件查询数据](#根据检索条件查询数据)
	* [新增](#新增)
	* [编辑](#编辑)
	* [试算平衡](#试算平衡)

---

## <a name="所需参数说明">所需参数说明</a>

| 英文名称          		 | 类型        	   |请求参数是否必填 |  示例              | 描述       	|
| :---:            		 | :----:      	   | :------:      |:------------:      |:-------:  	|
| periodBeginBalance     | float      	   | 是            | 100.01             | 期初余额   		|
| accumulatedDebitAmount | float      	   | 是            | 100.01             | 累计借方   		|
| accumulatedCreditAmount| float           | 是            | 100.01             | 累计贷方    	|
| yearBeginBalance       | float           | 是            | 100.01             | 年初余额    	|
| accountSubject         | AccountSubject  | 是            | AccountSubject     | 关联科目   		|
| accountTemplate        | AccountTemplate | 是            | AccountTemplate    | 关联帐套   		|
| status           		 | int         	   | 是            | 0         		    | 状态(0:正常)	|
| updateTime       		 | int         	   |               | 1535444931         | 更新时间     	|
| creditTime       		 | int         	   |               | 1535444931         | 创建时间     	|

## <a name="接口示例">接口示例</a>

### <a name="获取单条数据场景">获取单条数据场景</a>

**场景说明**

```
FETCH_ONE_MODEL_UN
```
**请求示例**

```
$data = $this->getRepository()->scenario(PeriodBeginRepository::FETCH_ONE_MODEL_UN)->fetchOne(int $id);
```
**接口返回示例**

```
type: object
data: data
```
### <a name="获取多条数据场景">获取多条数据场景</a>

**场景说明**

```
LIST_MODEL_UN
```
**请求示例**

```
$list = $this->getRepository()->scenario(PeriodBeginRepository::LIST_MODEL_UN)->fetchList(array $ids);
```
**接口返回示例**

```
type: array
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

**场景说明**

```
LIST_MODEL_UN
```
**请求示例**

```
list($count, $list) = $this->getRepository()->scenario(PeriodBeginRepository::LIST_MODEL_UN)->search($filter, $sort, $page, $size);
```
**接口返回示例**

```
type: array
count: 2,
list: {
	0 => [
		object
	],
	1 => [
		object
	],
}
```
### <a name="新增">新增</a>

**参数**

```
	$data=array(
		"data"=>array(
			"type"=>"periodBegins",
			"attributes"=>array(
                "periodBeginBalance"=>"100.01",
                "accumulatedDebitAmount"=>'100.01',
                "accumulatedCreditAmount"=>'100.01'
			),
			"relationships"=>array(
                "accountSubject"=> array(
                    "data"=> array(
                        array(
                            "type"=> "accountSubjects",
                            "id"=> 1
                        )
                    )
                ),
				"accountTemplate"=> array(
					"data"=> array(
						array(
							"type"=> "accountTemplates",
							"id"=> 1
						)
					)
				)
		)
	);
```
**调用示例**

```
$accountSubject = $this->getAccountSubjectRepository()->fetchOne(int $id);
$accountTemplate = $this->getAccountTemplateRepository()->fetchOne(int $id);

$periodBegin = new Sdk\periodBegin\Model\periodBegin();
$periodBegin->setPeriodBeginBalance('100');
$periodBegin->setAccumulatedDebitAmount('100');
$periodBegin->setAccumulatedCreditAmount('100');
$periodBegin->setAccountSubject($accountSubject);
$periodBegin->setAccountTemplate($accountTemplate);

$periodBegin->add();

```
### <a name="编辑">编辑</a>

**参数**

```
	$data = array(
		"data"=>array(
			"type"=>"periodBegins",
			"attributes"=>array(
                "accumulatedDebitAmount"=>'100.01',
                "accumulatedCreditAmount"=>'100.01'
			)
		)
	);
```
**调用示例**

```
$periodBegin = $this->getRepository()->fetchOne(int $id);
$periodBegin->setAccumulatedDebitAmount('100');
$periodBegin->setAccumulatedCreditAmount('100');

$periodBegin->edit();

```

### <a name="试算平衡">试算平衡</a>

**调用示例**

```

$trialBalanceCountAdapter = new StaticsTrialBalanceCountAdapter();
$statisticalRepository = new StatisticalRepository($trialBalanceCountAdapter);

$filter['accountTemplateId'] = 1;

$statisticalRepository->analyse($filter);

```