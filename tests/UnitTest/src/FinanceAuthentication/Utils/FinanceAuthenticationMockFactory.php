<?php
namespace Sdk\FinanceAuthentication\Utils;

use Sdk\FinanceAuthentication\Model\FinanceAuthentication;
use Sdk\Enterprise\Model\Enterprise;

use Sdk\FinanceAuthentication\Model\FinanceAuthenticationCategoryFactory;

class FinanceAuthenticationMockFactory
{
    /**
     * [generateFinanceAuthenticationArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateFinanceAuthenticationArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $financeAuthentication = array();

        $financeAuthentication = array(
            'data'=>array(
                'type'=>'financeAuthentications',
                'id'=>$faker->randomNumber(2)
            )
        );

        $value = array();
        $attributes = array();

        //enterpriseName
        $enterpriseName = self::generateEnterpriseName($faker, $value);
        $attributes['enterpriseName'] = $enterpriseName;
        //organizationsCategory
        $organizationsCategory = 1;
        $attributes['organizationsCategory'] = $organizationsCategory;
        //organizationsCategoryParent
        $organizationsCategoryParent = 1;
        $attributes['organizationsCategoryParent'] = $organizationsCategoryParent;
        //code
        $code = self::generateCode($faker, $value);
        $attributes['code'] = $code;
        //licence
        $licence = self::generateLicence($faker, $value);
        $attributes['licence'] = $licence;
        //organizationCode
        $organizationCode = self::generateOrganizationCode($faker, $value);
        $attributes['organizationCode'] = $organizationCode;
        //organizationCodeCertificate
        $organizationCodeCertificate = self::generateOrganizationCodeCertificate($faker, $value);
        $attributes['organizationCodeCertificate'] = $organizationCodeCertificate;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $financeAuthentication['data']['attributes'] = $attributes;
        //enterprise
        $financeAuthentication['data']['relationships']['enterprise']['data'] = array(
            'type' => 'enterprises',
            'id' => $faker->randomNumber(1)
        );
        
        return $financeAuthentication;
    }

    /**
     * [generateFinanceAuthenticationObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateFinanceAuthenticationObject(
        FinanceAuthentication $financeAuthentication,
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : FinanceAuthentication {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        //enterpriseName
        $enterpriseName = self::generateEnterpriseName($faker, $value);
        $financeAuthentication->setEnterpriseName($enterpriseName);
        //organizationsCategory
        $organizationsCategory = self::generateOrganizationsCategory($faker, $value);
        $financeAuthentication->setOrganizationsCategory($organizationsCategory);
        //organizationsCategoryParent
        $organizationsCategoryParent = self::generateOrganizationsCategoryParent($faker, $value);
        $financeAuthentication->setOrganizationsCategoryParent($organizationsCategoryParent);
        //code
        $code = self::generateCode($faker, $value);
        $financeAuthentication->setCode($code);
        //licence
        $licence = self::generateLicence($faker, $value);
        $financeAuthentication->setLicence($licence);
        //organizationCode
        $organizationCode = self::generateOrganizationCode($faker, $value);
        $financeAuthentication->setOrganizationCode($organizationCode);
        //organizationCodeCertificate
        $organizationCodeCertificate = self::generateOrganizationCodeCertificate($faker, $value);
        $financeAuthentication->setOrganizationCodeCertificate($organizationCodeCertificate);

        //enterprise
        $enterprise = self::generateEnterprise($faker, $value);
        $financeAuthentication->setEnterprise($enterprise);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $financeAuthentication->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $financeAuthentication->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $financeAuthentication->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $financeAuthentication->setStatus($status);

        return $financeAuthentication;
    }

    private static function generateEnterpriseName($faker, array $value = array())
    {
        return $enterpriseName = isset($value['enterpriseName']) ?
        $value['enterpriseName'] : $faker->name;
    }

    const ORGANIZATIONS_SECOND_CATEGORY_ID = 1;

    private static function generateOrganizationsCategory($faker, array $value = array())
    {
        return $organizationsCategory = isset($value['organizationsCategory']) ?
        $value['organizationsCategory'] : FinanceAuthenticationCategoryFactory::create(
            self::ORGANIZATIONS_SECOND_CATEGORY_ID,
            FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_SECOND_CATEGORY']
        );
    }

    const ORGANIZATIONS_FIRST_CATEGORY_ID = 1;

    private static function generateOrganizationsCategoryParent($faker, array $value = array())
    {
        return $organizationsCategoryParent = isset($value['organizationsCategoryParent']) ?
        $value['organizationsCategoryParent'] : FinanceAuthenticationCategoryFactory::create(
            self::ORGANIZATIONS_FIRST_CATEGORY_ID,
            FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_FIRST_CATEGORY']
        );
    }

    private static function generateCode($faker, array $value = array())
    {
        return $code = isset($value['code']) ?
        $value['code'] : $faker->word;
    }

    private static function generateLicence($faker, array $value = array())
    {
        return $licence = isset($value['licence']) ?
        $value['licence'] : array('name' => '金融许可证', 'identify' => '金融许可证.png');
    }

    private static function generateOrganizationCode($faker, array $value = array())
    {
        return $organizationCode = isset($value['organizationCode']) ?
            $value['organizationCode'] : $faker->word;
    }

    private static function generateOrganizationCodeCertificate($faker, array $value = array())
    {
        return $organizationCodeCertificate = isset($value['organizationCodeCertificate']) ?
        $value['organizationCodeCertificate'] : array('name' => '金融机构代码证', 'identify' => '金融机构代码证.png');
    }

    private static function generateEnterprise($faker, array $value = array())
    {
        return $enterprise = isset($value['enterprise']) ?
            $value['enterprise'] : \Sdk\Enterprise\Utils\EnterpriseMockFactory::generateEnterpriseObject(
                new Enterprise(),
                $faker->numerify()
            );
    }
}
