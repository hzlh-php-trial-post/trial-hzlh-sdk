<?php
namespace Sdk\FinanceAuthentication\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullFinanceAuthenticationTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullFinanceAuthentication::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsFinanceAuthentication()
    {
        $this->assertInstanceof('Sdk\FinanceAuthentication\Model\FinanceAuthentication', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
