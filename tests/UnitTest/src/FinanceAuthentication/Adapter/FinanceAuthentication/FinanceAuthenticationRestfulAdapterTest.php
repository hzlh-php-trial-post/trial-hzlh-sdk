<?php
namespace Sdk\FinanceAuthentication\Adapter\FinanceAuthentication;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\FinanceAuthentication\Model\FinanceAuthentication;
use Sdk\FinanceAuthentication\Model\NullFinanceAuthentication;
use Sdk\FinanceAuthentication\Utils\FinanceAuthenticationMockFactory;
use Sdk\FinanceAuthentication\Translator\FinanceAuthenticationRestfulTranslator;

class FinanceAuthenticationRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(FinanceAuthenticationRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends FinanceAuthenticationRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIFinanceAuthenticationdapter()
    {
        $this->assertInstanceOf(
            'Sdk\FinanceAuthentication\Adapter\FinanceAuthentication\IFinanceAuthenticationAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('financeAuthentications', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\FinanceAuthentication\Translator\FinanceAuthenticationRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'FINANCIAL_AUTHENTICATION_LIST',
                FinanceAuthenticationRestfulAdapter::SCENARIOS['FINANCIAL_AUTHENTICATION_LIST']
            ],
            [
                'FINANCIAL_AUTHENTICATION_FETCH_ONE',
                FinanceAuthenticationRestfulAdapter::SCENARIOS['FINANCIAL_AUTHENTICATION_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $financeAuthentication = FinanceAuthenticationMockFactory::generateFinanceAuthenticationObject(
            new FinanceAuthentication(),
            $id
        );

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullFinanceAuthentication())
            ->willReturn($financeAuthentication);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($financeAuthentication, $result);
    }

    /**
     * 为FinanceAuthenticationRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$FinanceAuthentication，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareFinanceAuthenticationTranslator(
        FinanceAuthentication $financeAuthentication,
        array $keys,
        array $financeAuthenticationArray
    ) {
        $translator = $this->prophesize(FinanceAuthenticationRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($financeAuthentication),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($financeAuthenticationArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(FinanceAuthentication $financeAuthentication)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($financeAuthentication);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    const ID = 1;

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinanceAuthenticationTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $financeAuthentication = FinanceAuthenticationMockFactory::generateFinanceAuthenticationObject(
            new FinanceAuthentication(),
            self::ID
        );
        $financeAuthenticationArray = array();

        $this->prepareFinanceAuthenticationTranslator(
            $financeAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate',
                'enterprise'
            ),
            $financeAuthenticationArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('financeAuthentications', $financeAuthenticationArray);

        $this->success($financeAuthentication);

        $result = $this->stub->add($financeAuthentication);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareFinanceAuthenticationTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $financeAuthentication = FinanceAuthenticationMockFactory::generateFinanceAuthenticationObject(
            new FinanceAuthentication(),
            self::ID
        );
        $financeAuthenticationArray = array();

        $this->prepareFinanceAuthenticationTranslator(
            $financeAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate',
                'enterprise'
            ),
            $financeAuthenticationArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('financeAuthentications', $financeAuthenticationArray);

        $this->failure($financeAuthentication);
        $result = $this->stub->add($financeAuthentication);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparefinanceAuthenticationTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $financeAuthentication = FinanceAuthenticationMockFactory::generateFinanceAuthenticationObject(
            new FinanceAuthentication(),
            self::ID
        );
        $financeAuthenticationArray = array();

        $this->preparefinanceAuthenticationTranslator(
            $financeAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate'
            ),
            $financeAuthenticationArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'financeAuthentications/'.$financeAuthentication->getId(),
                $financeAuthenticationArray
            );

        $this->success($financeAuthentication);

        $result = $this->stub->edit($financeAuthentication);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparefinanceAuthenticationTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $financeAuthentication = FinanceAuthenticationMockFactory::generateFinanceAuthenticationObject(
            new FinanceAuthentication(),
            self::ID
        );
        $financeAuthenticationArray = array();

        $this->preparefinanceAuthenticationTranslator(
            $financeAuthentication,
            array(
                'organizationsCategory',
                'organizationsCategoryParent',
                'code',
                'licence',
                'organizationCode',
                'organizationCodeCertificate'
            ),
            $financeAuthenticationArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'financeAuthentications/'.$financeAuthentication->getId(),
                $financeAuthenticationArray
            );

        $this->failure($financeAuthentication);
        $result = $this->stub->edit($financeAuthentication);
        $this->assertFalse($result);
    }
}
