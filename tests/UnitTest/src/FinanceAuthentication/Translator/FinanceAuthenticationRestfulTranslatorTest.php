<?php
namespace Sdk\FinanceAuthentication\Translator;

use Sdk\FinanceAuthentication\Model\NullFinanceAuthentication;
use Sdk\FinanceAuthentication\Model\FinanceAuthentication;

use Sdk\FinanceAuthentication\Model\FinanceAuthenticationCategoryFactory;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Enterprise\Model\Enterprise;

class FinanceAuthenticationRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            FinanceAuthenticationRestfulTranslator::class
        )
            ->setMethods(['getEnterpriseRestfulTranslator'])
            ->getMock();

        $this->childStub =
        new class extends FinanceAuthenticationRestfulTranslator {
            public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
            {
                return parent::getEnterpriseRestfulTranslator();
            }
        };
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->childStub->getEnterpriseRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new FinanceAuthentication());
        $this->assertInstanceOf('Sdk\FinanceAuthentication\Model\FinanceAuthentication', $result);
    }

    public function setMethods(FinanceAuthentication $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['enterpriseName'])) {
            $expectObject->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['organizationsCategory'])) {
            $expectObject->setOrganizationsCategory(FinanceAuthenticationCategoryFactory::create(
                $attributes['organizationsCategory'],
                FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_SECOND_CATEGORY']
            ));
        }
        if (isset($attributes['organizationsCategoryParent'])) {
            $expectObject->setOrganizationsCategoryParent(FinanceAuthenticationCategoryFactory::create(
                $attributes['organizationsCategoryParent'],
                FinanceAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_FIRST_CATEGORY']
            ));
        }
        if (isset($attributes['code'])) {
            $expectObject->setCode($attributes['code']);
        }
        if (isset($attributes['licence'])) {
            $expectObject->setLicence($attributes['licence']);
        }
        if (isset($attributes['organizationCode'])) {
            $expectObject->setOrganizationCode($attributes['organizationCode']);
        }
        if (isset($attributes['organizationCodeCertificate'])) {
            $expectObject->setOrganizationCodeCertificate($attributes['organizationCodeCertificate']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['enterprise']['data'])) {
            $expectObject->setEnterprise(new Enterprise($relationships['enterprise']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $financeAuthentication = \Sdk\FinanceAuthentication\Utils\FinanceAuthenticationMockFactory::generateFinanceAuthenticationArray(); //phpcs:ignore

        $data =  $financeAuthentication['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($financeAuthentication);

        $expectObject = new FinanceAuthentication();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $financeAuthentication = \Sdk\FinanceAuthentication\Utils\FinanceAuthenticationMockFactory::generateFinanceAuthenticationArray(); //phpcs:ignore

        $data =  $financeAuthentication['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($financeAuthentication);

        $expectArray = array();

        $expectObject = new FinanceAuthentication();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $financeAuthentication = \Sdk\FinanceAuthentication\Utils\FinanceAuthenticationMockFactory::generateFinanceAuthenticationObject(new FinanceAuthentication(), 1, 1); //phpcs:ignore

        $actual = $this->stub->objectToArray($financeAuthentication, array(
            'code',
            'organizationsCategory',
            'organizationsCategoryParent',
            'licence',
            'enterprise'
        ));

        $expectedArray = array(
            'data'=>array(
                'type'=>'financeAuthentications'
            )
        );


        $expectedArray['data']['attributes'] = array(
            'code' => $financeAuthentication->getCode(),
            'organizationsCategory' => $financeAuthentication->getOrganizationsCategory()->getId(),
            'organizationsCategoryParent' => $financeAuthentication->getOrganizationsCategoryParent()->getId(),
            'licence' => $financeAuthentication->getLicence()
        );

        $expectedArray['data']['relationships']['enterprise']['data'] = array(
            array(
                'type' => 'enterprises',
                'id' => $financeAuthentication->getEnterprise()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
