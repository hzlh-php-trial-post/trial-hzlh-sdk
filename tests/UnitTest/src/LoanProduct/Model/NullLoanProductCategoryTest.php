<?php
namespace Sdk\LoanProduct\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullLoanProductCategoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullLoanProductCategory::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsLoanProduct()
    {
        $this->assertInstanceof('Sdk\LoanProduct\Model\LoanProductCategory', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
