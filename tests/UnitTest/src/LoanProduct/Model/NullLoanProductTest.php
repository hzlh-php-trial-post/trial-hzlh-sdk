<?php
namespace Sdk\LoanProduct\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullLoanProductTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullLoanProduct::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsLoanProduct()
    {
        $this->assertInstanceof('Sdk\LoanProduct\Model\LoanProduct', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function tesOnShelf()
    {
        $result = $this->stub->onShelf();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }


    public function testOffStock()
    {
        $result = $this->stub->offStock();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
