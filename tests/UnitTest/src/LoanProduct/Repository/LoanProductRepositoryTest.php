<?php
namespace Sdk\LoanProduct\Repository;

use Sdk\LoanProduct\Adapter\LoanProduct\LoanProductRestfulAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class LoanProductRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(LoanProductRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends LoanProductRepository {
            public function getAdapter() : LoanProductRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProduct\Adapter\LoanProduct\LoanProductRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为LoanProductRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以LoanProductRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(LoanProductRestfulAdapter::class);
        $adapter->scenario(Argument::exact(LoanProductRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(LoanProductRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
    /**
     * 生成模拟数据，传参为1
     * 为LoanProductRestfulAdapter建立预言
     * 建立预期状况：onShelf() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testOnShelf()
    {
        $loanProduct = \Sdk\LoanProduct\Utils\MockFactory::generateLoanProductObject(1);

        $adapter = $this->prophesize(LoanProductRestfulAdapter::class);
        $adapter->onShelf(Argument::exact($loanProduct))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->onShelf($loanProduct);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为LoanProductRestfulAdapter建立预言
     * 建立预期状况：offStock() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testOffStock()
    {
        $loanProduct = \Sdk\LoanProduct\Utils\MockFactory::generateLoanProductObject(1);

        $adapter = $this->prophesize(LoanProductRestfulAdapter::class);
        $adapter->offStock(Argument::exact($loanProduct))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->offStock($loanProduct);
        $this->assertTrue($result);
    }
}
