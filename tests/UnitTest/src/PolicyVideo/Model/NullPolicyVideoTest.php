<?php
namespace Sdk\PolicyVideo\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullPolicyVideoTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullPolicyVideo::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsPolicyVideo()
    {
        $this->assertInstanceof('Sdk\PolicyVideo\Model\PolicyVideo', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
