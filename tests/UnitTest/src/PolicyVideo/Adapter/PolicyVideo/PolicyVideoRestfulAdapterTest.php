<?php
namespace Sdk\PolicyVideo\Adapter\PolicyVideo;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\PolicyVideo\Model\PolicyVideo;
use Sdk\PolicyVideo\Model\NullPolicyVideo;
use Sdk\PolicyVideo\Utils\MockFactory;
use Sdk\PolicyVideo\Translator\PolicyVideoRestfulTranslator;

class PolicyVideoRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PolicyVideoRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends PolicyVideoRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIPolicyVideoAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyVideo\Adapter\PolicyVideo\IPolicyVideoAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('policyVideos', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyVideo\Translator\PolicyVideoRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_POLICY_VIDEO_LIST',
                PolicyVideoRestfulAdapter::SCENARIOS['OA_POLICY_VIDEO_LIST']
            ],
            [
                'PORTAL_POLICY_VIDEO_LIST',
                PolicyVideoRestfulAdapter::SCENARIOS['PORTAL_POLICY_VIDEO_LIST']
            ],
            [
                'POLICY_VIDEO_FETCH_ONE',
                PolicyVideoRestfulAdapter::SCENARIOS['POLICY_VIDEO_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $policyVideo = MockFactory::generatePolicyVideoObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullPolicyVideo())
            ->willReturn($policyVideo);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($policyVideo, $result);
    }
    /**
     * 为PolicyVideoRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$policyVideo$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function preparePolicyVideoTranslator(
        PolicyVideo $policyVideo,
        array $keys,
        array $policyVideoArray
    ) {
        $translator = $this->prophesize(PolicyVideoRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($policyVideo),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($policyVideoArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(PolicyVideo $policyVideo)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($policyVideo);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);
        $policyVideoArray = array();

        $this->preparePolicyVideoTranslator(
            $policyVideo,
            array(
                'title',
                'source',
                'cover',
                'videoLink',
                'crew'
            ),
            $policyVideoArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('policyVideos', $policyVideoArray);

        $this->success($policyVideo);

        $result = $this->stub->add($policyVideo);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);
        $policyVideoArray = array();

        $this->preparePolicyVideoTranslator(
            $policyVideo,
            array(
                'title',
                'source',
                'cover',
                'videoLink',
                'crew'
            ),
            $policyVideoArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('policyVideos', $policyVideoArray);

        $this->failure($policyVideo);
        $result = $this->stub->add($policyVideo);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);
        $policyVideoArray = array();

        $this->preparePolicyVideoTranslator(
            $policyVideo,
            array(
                'title',
                'source',
                'cover',
                'videoLink'
            ),
            $policyVideoArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('policyVideos/'.$policyVideo->getId(), $policyVideoArray);

        $this->success($policyVideo);

        $result = $this->stub->edit($policyVideo);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);
        $policyVideoArray = array();

        $this->preparePolicyVideoTranslator(
            $policyVideo,
            array(
                'title',
                'source',
                'cover',
                'videoLink'
            ),
            $policyVideoArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('policyVideos/'.$policyVideo->getId(), $policyVideoArray);

        $this->failure($policyVideo);
        $result = $this->stub->edit($policyVideo);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行onShelf（）
     * 判断 result 是否为true
     */
    public function testOnShelfSuccess()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'policyVideos/'.$policyVideo->getId().'/onShelf'
            );

        $this->success($policyVideo);

        $result = $this->stub->onShelf($policyVideo);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行onShelf（）
     * 判断 result 是否为false
     */
    public function testOnShelfFailure()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'policyVideos/'.$policyVideo->getId().'/onShelf'
            );

        $this->failure($policyVideo);
        $result = $this->stub->onShelf($policyVideo);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行offStock（）
     * 判断 result 是否为true
     */
    public function testOffStockSuccess()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'policyVideos/'.$policyVideo->getId().'/offStock'
            );

        $this->success($policyVideo);

        $result = $this->stub->offStock($policyVideo);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行offStock（）
     * 判断 result 是否为false
     */
    public function testOffStockFailure()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'policyVideos/'.$policyVideo->getId().'/offStock'
            );

        $this->failure($policyVideo);
        $result = $this->stub->offStock($policyVideo);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行recommend（）
     * 判断 result 是否为true
     */
    public function testRecommendSuccess()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'policyVideos/'.$policyVideo->getId().'/recommend'
            );

        $this->success($policyVideo);

        $result = $this->stub->recommend($policyVideo);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行recommend（）
     * 判断 result 是否为false
     */
    public function testRecommendFailure()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'policyVideos/'.$policyVideo->getId().'/recommend'
            );

        $this->failure($policyVideo);
        $result = $this->stub->recommend($policyVideo);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行cancelRecommend（）
     * 判断 result 是否为true
     */
    public function testCancelRecommendSuccess()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'policyVideos/'.$policyVideo->getId().'/cancelRecommend'
            );

        $this->success($policyVideo);

        $result = $this->stub->cancelRecommend($policyVideo);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparePolicyVideoTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行cancelRecommend（）
     * 判断 result 是否为false
     */
    public function testCancelRecommendFailure()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'policyVideos/'.$policyVideo->getId().'/cancelRecommend'
            );

        $this->failure($policyVideo);
        $result = $this->stub->cancelRecommend($policyVideo);
        $this->assertFalse($result);
    }
}
