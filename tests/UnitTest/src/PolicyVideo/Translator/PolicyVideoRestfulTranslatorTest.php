<?php
namespace Sdk\PolicyVideo\Translator;

use Sdk\PolicyVideo\Model\NullPolicyVideo;
use Sdk\PolicyVideo\Model\PolicyVideo;
use Sdk\PolicyVideo\Model\Answer;
use Sdk\PolicyVideo\Model\DeleteInfo;

use Sdk\Crew\Model\Crew;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Crew\Translator\CrewRestfulTranslator;

use Sdk\PolicyVideo\Utils\MockFactory;

class PolicyVideoRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            PolicyVideoRestfulTranslator::class
        )
            ->setMethods([
                'getCrewRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends PolicyVideoRestfulTranslator {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new PolicyVideo());
        $this->assertInstanceOf('Sdk\PolicyVideo\Model\NullPolicyVideo', $result);
    }

    public function setMethods(PolicyVideo $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['title'])) {
            $expectObject->setTitle($attributes['title']);
        }
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['source'])) {
            $expectObject->setSource($attributes['source']);
        }
        if (isset($attributes['cover'])) {
            $expectObject->setCover($attributes['cover']);
        }
        if (isset($attributes['videoLink'])) {
            $expectObject->setVideoLink($attributes['videoLink']);
        }
        if (isset($attributes['recommendStatus'])) {
            $expectObject->setRecommendStatus($attributes['recommendStatus']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }

        if (isset($relationships['crew']['data'])) {
            $expectObject->setCrew(new Crew($relationships['crew']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $policyVideo = MockFactory::generatePolicyVideoArray();

        $data =  $policyVideo['data'];
        $relationships = $data['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);
        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($policyVideo);

        $expectObject = new PolicyVideo();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $policyVideo = MockFactory::generatePolicyVideoArray();
        $data =  $policyVideo['data'];
        $relationships = $data['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);
        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($policyVideo);
        $expectArray = array();

        $expectObject = new PolicyVideo();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $policyVideo = MockFactory::generatePolicyVideoObject(1, 1);

        $actual = $this->stub->objectToArray(
            $policyVideo,
            array(
                'title',
                'source',
                'cover',
                'videoLink',
                'recommendStatus',
                'crew',
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'policyVideos'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'title'=>$policyVideo->getTitle(),
            'source'=>$policyVideo->getSource(),
            'cover'=>$policyVideo->getCover(),
            'videoLink'=>$policyVideo->getVideoLink(),
            'recommendStatus'=>$policyVideo->getRecommendStatus()
        );

        $expectedArray['data']['relationships']['crew']['data'] = array(
            array(
                'type' => 'crews',
                'id' => $policyVideo->getCrew()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
