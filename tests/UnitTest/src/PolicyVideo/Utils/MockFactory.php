<?php
namespace Sdk\PolicyVideo\Utils;

use Sdk\PolicyVideo\Model\PolicyVideo;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generatePolicyVideoArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generatePolicyVideoArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $policyVideo = array();

        $policyVideo = array(
            'data'=>array(
                'type'=>'policyVideos',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //title
        $title = self::generateTitle($faker, $value);
        $attributes['title'] = $title;
        //cover
        $cover = self::generateCover($faker, $value);
        $attributes['cover'] = $cover;
        //source
        $source = self::generateSource($faker, $value);
        $attributes['source'] = $source;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $policyVideo['data']['attributes'] = $attributes;
        //crew
        $policyVideo['data']['relationships']['crew']['data'] = array(
            'type' => 'crews',
            'id' => $faker->randomNumber(1)
        );

        return $policyVideo;
    }
    /**
     * [generatePolicyVideoObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generatePolicyVideoObject(int $id = 0, int $seed = 0, array $value = array()) : PolicyVideo
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $policyVideo = new PolicyVideo($id);

        //title
        $title = self::generateTitle($faker, $value);
        $policyVideo->setTitle($title);
        //cover
        $cover = self::generateCover($faker, $value);
        $policyVideo->setCover($cover);
        //source
        $source = self::generateSource($faker, $value);
        $policyVideo->setSource($source);
        //crew
        $crew = self::generateCrew($faker, $value);
        $policyVideo->setCrew($crew);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $policyVideo->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $policyVideo->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $policyVideo->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $policyVideo->setStatus($status);

        return $policyVideo;
    }

    private static function generateTitle($faker, array $value = array())
    {
        return isset($value['title']) ?
        $value['title'] : $faker->title;
    }

    private static function generateCover($faker, array $value = array())
    {
        return isset($value['cover']) ?
        $value['cover'] : array('title' => 'cover', 'identify' => 'cover.jpg');
    }

    private static function generateSource($faker, array $value = array())
    {
        return isset($value['source']) ?
        $value['source'] : $faker->word;
    }

    private static function generateCrew($faker, array $value = array())
    {
        return isset($value['crew']) ?
            $value['crew'] : \Sdk\Crew\Utils\MockFactory::generateCrewObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
