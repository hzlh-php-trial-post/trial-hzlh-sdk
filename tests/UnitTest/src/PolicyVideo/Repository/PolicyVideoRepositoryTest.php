<?php
namespace Sdk\PolicyVideo\Repository;

use Sdk\PolicyVideo\Adapter\PolicyVideo\PolicyVideoRestfulAdapter;
use Sdk\PolicyVideo\Adapter\PolicyVideo\PolicyVideoMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class PolicyVideoRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PolicyVideoRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends PolicyVideoRepository {
            public function getAdapter() : PolicyVideoRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : PolicyVideoMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyVideo\Adapter\PolicyVideo\PolicyVideoRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyVideo\Adapter\PolicyVideo\PolicyVideoMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为PolicyVideoRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以PolicyVideoRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(PolicyVideoRestfulAdapter::class);
        $adapter->scenario(Argument::exact(PolicyVideoRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);//phpcs:ignore

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(PolicyVideoRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
