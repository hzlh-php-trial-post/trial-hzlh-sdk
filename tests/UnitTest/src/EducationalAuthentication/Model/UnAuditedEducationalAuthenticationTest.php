<?php
namespace Sdk\EducationalAuthentication\Model;

use Sdk\EducationalAuthentication\Repository\UnAuditedEducationalAuthenticationRepository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class UnAuditedEducationalAuthenticationTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(UnAuditedEducationalAuthentication::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends UnAuditedEducationalAuthentication{
            public function getRepository() : UnAuditedEducationalAuthenticationRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\EducationalAuthentication\Repository\UnAuditedEducationalAuthenticationRepository',
            $this->childStub->getRepository()
        );
    }

    //rejectReason 测试 ---------------------------------------------------------- start
    /**
     * 设置 UnAuditedEducationalAuthentication setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->stub->setRejectReason('string');
        $this->assertEquals('string', $this->stub->getRejectReason());
    }

    /**
     * 设置 UnAuditedEducationalAuthentication setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->stub->setRejectReason(array(1, 2, 3));
    }
    //rejectReason 测试 ----------------------------------------------------------   end
}
