<?php
namespace Sdk\EducationalAuthentication\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullUnAuditedEducationalAuthenticationTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullUnAuditedEducationalAuthentication::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsUnAuditedEducationalAuthentication()
    {
        $this->assertInstanceof('Sdk\EducationalAuthentication\Model\UnAuditedEducationalAuthentication', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
