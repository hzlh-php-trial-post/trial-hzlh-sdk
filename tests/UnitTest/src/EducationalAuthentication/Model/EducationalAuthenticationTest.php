<?php
namespace Sdk\EducationalAuthentication\Model;

use Sdk\Enterprise\Model\Enterprise;

use Sdk\EducationalAuthentication\Repository\EducationalAuthenticationRepository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class EducationalAuthenticationTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(EducationalAuthentication::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends EducationalAuthentication{
            public function getRepository() : EducationalAuthenticationRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\EducationalAuthentication\Repository\EducationalAuthenticationRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 EducationalAuthentication setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {

        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 EducationalAuthentication setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //enterpriseName 测试 ---------------------------------------------------------- start
    /**
     * 设置 EducationalAuthentication setEnterpriseName() 正确的传参类型,期望传值正确
     */
    public function testSetEnterprisesNameCorrectType()
    {
        $this->stub->setEnterpriseName('string');
        $this->assertEquals('string', $this->stub->getEnterpriseName());
    }

    /**
     * 设置 EducationalAuthentication setEnterpriseName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterprisesNameWrongType()
    {
        $this->stub->setEnterpriseName(array(1, 2, 3));
    }
    //enterpriseName 测试 ----------------------------------------------------------   end

    //code 测试 ---------------------------------------------------------- start
    /**
     * 设置 EducationalAuthentication setCode() 正确的传参类型,期望传值正确
     */
    public function testSetCodeCorrectType()
    {
        $this->stub->setCode('string');
        $this->assertEquals('string', $this->stub->getCode());
    }

    /**
     * 设置 EducationalAuthentication setCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCodeWrongType()
    {
        $this->stub->setCode(array(1, 2, 3));
    }
    //code 测试 ----------------------------------------------------------   end

    //licence 测试 ---------------------------------------------------------- start
    /**
     * 设置 EducationalAuthentication setLicence() 正确的传参类型,期望传值正确
     */
    public function testSetLicenceCorrectType()
    {
        $this->stub->setLicence(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getLicence());
    }

    /**
     * 设置 EducationalAuthentication setLicence() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLicenceWrongType()
    {
        $this->stub->setLicence('string');
    }
    //licence 测试 ----------------------------------------------------------   end

    //organizationCode 测试 ---------------------------------------------------------- start
    /**
     * 设置 EducationalAuthentication setOrganizationCode() 正确的传参类型,期望传值正确
     */
    public function testSetOrganizationCodeCorrectType()
    {
        $this->stub->setOrganizationCode('string');
        $this->assertEquals('string', $this->stub->getOrganizationCode());
    }

    /**
     * 设置 EducationalAuthentication setOrganizationCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOrganizationCodeWrongType()
    {
        $this->stub->setOrganizationCode(array(1, 2, 3));
    }
    //organizationCode 测试 ----------------------------------------------------------   end

    //organizationCodeCertificate 测试 ---------------------------------------------------------- start
    /**
     * 设置 EducationalAuthentication setOrganizationCodeCertificate() 正确的传参类型,期望传值正确
     */
    public function testSetOrganizationCodeCertificateCorrectType()
    {
        $this->stub->setOrganizationCodeCertificate(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getOrganizationCodeCertificate());
    }

    /**
     * 设置 EducationalAuthentication setOrganizationCodeCertificate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOrganizationCodeCertificateWrongType()
    {
        $this->stub->setOrganizationCodeCertificate('string');
    }
    //organizationCodeCertificate 测试 ----------------------------------------------------------   end

    //enterprise 测试 ---------------------------------------------------------- start
    /**
     * 设置 EducationalAuthentication setEnterprise() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseCorrectType()
    {
        $enterprise = new Enterprise();
        $this->stub->setEnterprise($enterprise);
        $this->assertEquals($enterprise, $this->stub->getEnterprise());
    }

    /**
     * 设置 EducationalAuthentication setEnterprise() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseWrongType()
    {
        $this->stub->setEnterprise(array(1, 2, 3));
    }
    //enterprise 测试 ----------------------------------------------------------   end
}
