<?php
namespace Sdk\EducationalAuthentication\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullEducationalAuthenticationTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullEducationalAuthentication::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsEducationalAuthentication()
    {
        $this->assertInstanceof('Sdk\EducationalAuthentication\Model\EducationalAuthentication', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
