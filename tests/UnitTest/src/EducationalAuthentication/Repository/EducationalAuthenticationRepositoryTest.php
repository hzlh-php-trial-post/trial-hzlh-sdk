<?php
namespace Sdk\EducationalAuthentication\Repository;

use Sdk\EducationalAuthentication\Adapter\EducationalAuthentication\EducationalAuthenticationRestfulAdapter;

use Sdk\EducationalAuthentication\Utils\MockFactory;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class EducationalAuthenticationRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(EducationalAuthenticationRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends EducationalAuthenticationRepository {
            public function getAdapter() : EducationalAuthenticationRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\EducationalAuthentication\Adapter\EducationalAuthentication\EducationalAuthenticationRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为EducationalAuthenticationRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以EducationalAuthenticationRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(EducationalAuthenticationRestfulAdapter::class);
        $adapter->scenario(Argument::exact(EducationalAuthenticationRepository::LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(EducationalAuthenticationRepository::LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
