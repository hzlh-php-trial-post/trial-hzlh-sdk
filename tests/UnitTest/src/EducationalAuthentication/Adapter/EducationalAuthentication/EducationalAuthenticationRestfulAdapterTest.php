<?php
namespace Sdk\EducationalAuthentication\Adapter\EducationalAuthentication;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\EducationalAuthentication\Model\EducationalAuthentication;
use Sdk\EducationalAuthentication\Model\NullEducationalAuthentication;
use Sdk\EducationalAuthentication\Utils\EducationalAuthenticationMockFactory;
use Sdk\EducationalAuthentication\Translator\EducationalAuthenticationRestfulTranslator;

class EducationalAuthenticationRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(EducationalAuthenticationRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends EducationalAuthenticationRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIEducationalAuthenticationdapter()
    {
        $this->assertInstanceOf(
            'Sdk\EducationalAuthentication\Adapter\EducationalAuthentication\IEducationalAuthenticationAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('educationalAgencyAuthentications', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\EducationalAuthentication\Translator\EducationalAuthenticationRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'EDUCATIONAL_AUTHENTICATION_LIST',
                EducationalAuthenticationRestfulAdapter::SCENARIOS['EDUCATIONAL_AUTHENTICATION_LIST']
            ],
            [
                'EDUCATIONAL_AUTHENTICATION_FETCH_ONE',
                EducationalAuthenticationRestfulAdapter::SCENARIOS['EDUCATIONAL_AUTHENTICATION_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    // public function testFetchOne()
    // {
    //     $id = 1;

    //     $educationalAuthentication = EducationalAuthenticationMockFactory::generateEducationalAuthenticationObject(
    //         new EducationalAuthentication(),
    //         $id
    //     );

    //     $this->stub->expects($this->exactly(1))
    //         ->method('fetchOneAction')
    //         ->with($id, new NullEducationalAuthentication())
    //         ->willReturn($educationalAuthentication);

    //     $result = $this->stub->fetchOne($id);
    //     $this->assertEquals($educationalAuthentication, $result);
    // }

    /**
     * 为EducationalAuthenticationRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$EducationalAuthentication，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareEducationalAuthenticationTranslator(
        EducationalAuthentication $educationalAuthentication,
        array $keys,
        array $educationalAuthenticationArray
    ) {
        $translator = $this->prophesize(EducationalAuthenticationRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($educationalAuthentication),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($educationalAuthenticationArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(EducationalAuthentication $educationalAuthentication)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($educationalAuthentication);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    const ID = 1;

}
