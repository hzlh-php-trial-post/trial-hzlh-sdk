<?php
namespace Sdk\EducationalAuthentication\Adapter\EducationalAuthentication;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\EducationalAuthentication\Model\UnAuditedEducationalAuthentication;
use Sdk\EducationalAuthentication\Model\NullUnAuditedEducationalAuthentication;
use Sdk\EducationalAuthentication\Utils\UnAuditedEducationalAuthenticationMockFactory;
use Sdk\EducationalAuthentication\Translator\UnAuditedEducationalAuthenticationRestfulTranslator;

class UnAuditedEducationalAuthenticationRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(UnAuditedEducationalAuthenticationRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends UnAuditedEducationalAuthenticationRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIUnAuditedEducationalAuthenticationdapter()
    {
        $this->assertInstanceOf(
            'Sdk\EducationalAuthentication\Adapter\EducationalAuthentication\IUnAuditedEducationalAuthenticationAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedEducationalAgencyAuthentications', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\EducationalAuthentication\Translator\UnAuditedEducationalAuthenticationRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'UNAUDITED_FINANCE_AUTHENTICATION_LIST',
                'fields'=>[],
                UnAuditedEducationalAuthenticationRestfulAdapter::SCENARIOS['UNAUDITED_EDUCATIONAL_AUTHENTICATION_LIST']
            ],
            [
                'UNAUDITED_FINANCE_AUTHENTICATION_FETCH_ONE',
                'fields'=>[],
                UnAuditedEducationalAuthenticationRestfulAdapter::SCENARIOS['UNAUDITED_EDUCATIONAL_AUTHENTICATION_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    /**
     * 为UnAuditedEducationalAuthenticationRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$UnAuditedEducationalAuthentication，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareUnAuditedEducationalAuthenticationTranslator(
        UnAuditedEducationalAuthentication $unAuditedEducationalAuthentication,
        array $keys,
        array $unAuditedEducationalAuthenticationArray
    ) {
        $translator = $this->prophesize(UnAuditedEducationalAuthenticationRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($unAuditedEducationalAuthentication),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($unAuditedEducationalAuthenticationArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UnAuditedEducationalAuthentication $unAuditedEducationalAuthentication)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($unAuditedEducationalAuthentication);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
}
