<?php
namespace Sdk\EducationalAuthentication\Translator;

use Sdk\EducationalAuthentication\Model\NullUnAuditedEducationalAuthentication;
use Sdk\EducationalAuthentication\Model\UnAuditedEducationalAuthentication;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Enterprise\Model\Enterprise;

class UnAuditedEducationalAuthenticationRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            UnAuditedEducationalAuthenticationRestfulTranslator::class
        )->setMethods(['getEnterpriseRestfulTranslator'])->getMock();
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new UnAuditedEducationalAuthentication());
        $this->assertInstanceOf('Sdk\EducationalAuthentication\Model\UnAuditedEducationalAuthentication', $result);
    }

    public function setMethods(UnAuditedEducationalAuthentication $expectObject, array $attributes, array $relationships)
    {
        $educationalAuthentication = new EducationalAuthenticationRestfulTranslatorTest();

        $expectObject = $educationalAuthentication->setMethods($expectObject, $attributes, $relationships);

        if (isset($attributes['rejectReason'])) {
            $expectObject->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['applyStatus'])) {
            $expectObject->setApplyStatus($attributes['applyStatus']);
        }
        
        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $unAuditedEducationalAuthentication =
        \Sdk\EducationalAuthentication\Utils\UnAuditedEducationalAuthenticationMockFactory::generateUnAuditedEducationalAuthenticationArray(); //phpcs:ignore

        $data =  $unAuditedEducationalAuthentication['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($unAuditedEducationalAuthentication);

        $expectObject = new UnAuditedEducationalAuthentication();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $unAuditedEducationalAuthentication =
        \Sdk\EducationalAuthentication\Utils\UnAuditedEducationalAuthenticationMockFactory::generateUnAuditedEducationalAuthenticationArray(); //phpcs:ignore

        $data =  $unAuditedEducationalAuthentication['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($unAuditedEducationalAuthentication);

        $expectArray = array();

        $expectObject = new UnAuditedEducationalAuthentication();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $unAuditedEducationalAuthentication =
        \Sdk\EducationalAuthentication\Utils\UnAuditedEducationalAuthenticationMockFactory::generateUnAuditedEducationalAuthenticationObject(1); //phpcs:ignore

        $actual = $this->stub->objectToArray(
            $unAuditedEducationalAuthentication,
            array(
                'rejectReason'
            )
        );

        $expectedArray = array(
            'data'=>array(
                'type'=>'unAuditedEducationalAuthentications'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'rejectReason' => $unAuditedEducationalAuthentication->getRejectReason()
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
