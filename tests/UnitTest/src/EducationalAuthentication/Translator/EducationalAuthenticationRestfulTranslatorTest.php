<?php
namespace Sdk\EducationalAuthentication\Translator;

use Sdk\EducationalAuthentication\Model\NullEducationalAuthentication;
use Sdk\EducationalAuthentication\Model\EducationalAuthentication;

use Sdk\EducationalAuthentication\Model\EducationalAuthenticationCategoryFactory;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Enterprise\Model\Enterprise;

class EducationalAuthenticationRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            EducationalAuthenticationRestfulTranslator::class
        )
            ->setMethods(['getEnterpriseRestfulTranslator'])
            ->getMock();

        $this->childStub =
        new class extends EducationalAuthenticationRestfulTranslator {
            public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
            {
                return parent::getEnterpriseRestfulTranslator();
            }
        };
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->childStub->getEnterpriseRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new EducationalAuthentication());
        $this->assertInstanceOf('Sdk\EducationalAuthentication\Model\EducationalAuthentication', $result);
    }

    public function setMethods(EducationalAuthentication $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['enterpriseName'])) {
            $expectObject->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['organizationsCategory'])) {
            $expectObject->setOrganizationsCategory(EducationalAuthenticationCategoryFactory::create(
                $attributes['organizationsCategory'],
                EducationalAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_SECOND_CATEGORY']
            ));
        }
        if (isset($attributes['organizationsCategoryParent'])) {
            $expectObject->setOrganizationsCategoryParent(EducationalAuthenticationCategoryFactory::create(
                $attributes['organizationsCategoryParent'],
                EducationalAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_FIRST_CATEGORY']
            ));
        }
        if (isset($attributes['name'])) {
            $expectObject->setName($attributes['name']);
        }
        if (isset($attributes['licence'])) {
            $expectObject->setLicence($attributes['licence']);
        }
        if (isset($attributes['category'])) {
            $expectObject->setCategory($attributes['category']);
        }
        if (isset($attributes['operationType'])) {
            $expectObject->setOperationType($attributes['operationType']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['enterprise']['data'])) {
            $expectObject->setEnterprise(new Enterprise($relationships['enterprise']['data']['id']));
        }
        if (isset($attributes['area'])) {
            $expectObject->setArea($attributes['area']);
        }
        if (isset($attributes['address'])) {
            $expectObject->setAddress($attributes['address']);
        }
        if (isset($attributes['website'])) {
            $expectObject->setWebsite($attributes['website']);
        }
        if (isset($attributes['businessScope'])) {
            $expectObject->setBusinessScope($attributes['businessScope']);
        }
        if (isset($attributes['mpWeChat'])) {
            $expectObject->setMpWeChat($attributes['mpWeChat']);
        }
        if (isset($attributes['profile'])) {
            $expectObject->setProfile($attributes['profile']);
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $educationalAuthentication = \Sdk\EducationalAuthentication\Utils\EducationalAuthenticationMockFactory::generateEducationalAuthenticationArray(); //phpcs:ignore

        $data =  $educationalAuthentication['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($educationalAuthentication);

        $expectObject = new EducationalAuthentication();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $educationalAuthentication = \Sdk\EducationalAuthentication\Utils\EducationalAuthenticationMockFactory::generateEducationalAuthenticationArray(); //phpcs:ignore

        $data =  $educationalAuthentication['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($educationalAuthentication);

        $expectArray = array();

        $expectObject = new EducationalAuthentication();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $educationalAuthentication = \Sdk\EducationalAuthentication\Utils\EducationalAuthenticationMockFactory::generateEducationalAuthenticationObject(new EducationalAuthentication(), 1, 1); //phpcs:ignore

        $actual = $this->stub->objectToArray($educationalAuthentication, array(
                'licence',
                'category',
                'area',
                'address',
                'website',
                'businessScope',
                'mpWeChat',
                'profile',
                'enterprise'
        ));

        $expectedArray = array(
            'data'=>array(
                'type'=>'educationalAuthentications'
            )
        );


        $expectedArray['data']['attributes'] = array(
            'category' => $educationalAuthentication->getCategory(),
            'area' => $educationalAuthentication->getArea(),
            'address' => $educationalAuthentication->getAddress(),
            'website' => $educationalAuthentication->getWebsite(),
            'businessScope' => $educationalAuthentication->getBusinessScope(),
            'mpWeChat' => $educationalAuthentication->getMpWeChat(),
            'profile' => $educationalAuthentication->getProfile(),
            'licence' => $educationalAuthentication->getLicence()
        );

        $expectedArray['data']['relationships']['enterprise']['data'] = array(
            array(
                'type' => 'enterprises',
                'id' => $educationalAuthentication->getEnterprise()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
