<?php
namespace Sdk\EducationalAuthentication\Utils;

use Sdk\EducationalAuthentication\Model\UnAuditedEducationalAuthentication;

use Sdk\Common\Model\IApplyAble;

class UnAuditedEducationalAuthenticationMockFactory
{
    /**
     * [generateEducationalAuthenticationArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateUnAuditedEducationalAuthenticationArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $unAuditedEducationalAuthentication = array();

        $unAuditedEducationalAuthentication = array(
            'data'=>array(
                'type'=>'unAuditedEducationalAuthentications',
                'id'=>$faker->randomNumber(2)
            )
        );
        
        $value = array();
        $attributes = array();

        //rejectReason
        $rejectReason = self::generateRejectReason($faker, $value);
        $attributes['rejectReason'] = $rejectReason;

        //applyStatus
        $applyStatus = self::generateApplyStatus($faker, $value);
        $attributes['applyStatus'] = $applyStatus;

        $EducationalAuthentication = \Sdk\EducationalAuthentication\Utils\EducationalAuthenticationMockFactory::generateEducationalAuthenticationArray(); //phpcs:ignore

        $unAuditedEducationalAuthentication['data']['attributes'] = array_merge(
            $EducationalAuthentication['data']['attributes'],
            $attributes
        );

        $unAuditedEducationalAuthentication['data']['relationships'] = $EducationalAuthentication['data']['relationships'];
        
        return $unAuditedEducationalAuthentication;
    }

    /**
     * [generateEducationalAuthenticationObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateUnAuditedEducationalAuthenticationObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UnAuditedEducationalAuthentication {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditedEducationalAuthentication = new UnAuditedEducationalAuthentication($id);

        $unAuditedEducationalAuthentication =
            \Sdk\EducationalAuthentication\Utils\EducationalAuthenticationMockFactory::generateEducationalAuthenticationObject($unAuditedEducationalAuthentication); //phpcs:ignore

        //rejectReason
        $rejectReason = self::generateRejectReason($faker, $value);
        $unAuditedEducationalAuthentication->setRejectReason($rejectReason);

        //applyStatus
        $applyStatus = self::generateApplyStatus($faker, $value);
        $unAuditedEducationalAuthentication->setApplyStatus($applyStatus);

        return $unAuditedEducationalAuthentication;
    }

    private static function generateRejectReason($faker, array $value = array())
    {
        return $rejectReason = isset($value['rejectReason']) ?
        $value['rejectReason'] : $faker->word;
    }

    public static function generateApplyStatus($faker, array $value = array())
    {
        return $applyStatus = isset($value['applyStatus']) ?
        $value['applyStatus'] : $faker->randomElement(
            IApplyAble::APPLY_STATUS
        );
    }

    public static function generateEnterprise($faker, array $value = array())
    {
        return $enterprise = isset($value['enterprise']) ?
        $value['enterprise'] : 5;
    }
}
