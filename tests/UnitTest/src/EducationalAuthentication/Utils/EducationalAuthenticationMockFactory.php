<?php
namespace Sdk\EducationalAuthentication\Utils;

use Sdk\EducationalAuthentication\Model\EducationalAuthentication;
use Sdk\Enterprise\Model\Enterprise;

use Sdk\EducationalAuthentication\Model\EducationalAuthenticationCategoryFactory;

class EducationalAuthenticationMockFactory
{
    /**
     * [generateEducationalAuthenticationArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateEducationalAuthenticationArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $educationalAuthentication = array();

        $educationalAuthentication = array(
            'data'=>array(
                'type'=>'educationalAuthentications',
                'id'=>$faker->randomNumber(2)
            )
        );

        $value = array();
        $attributes = array();

        //enterpriseName
        $enterpriseName = self::generateEnterpriseName($faker, $value);
        $attributes['enterpriseName'] = $enterpriseName;
        //organizationsCategory
        $organizationsCategory = 1;
        $attributes['organizationsCategory'] = $organizationsCategory;
        //organizationsCategoryParent
        $organizationsCategoryParent = 1;
        $attributes['organizationsCategoryParent'] = $organizationsCategoryParent;
        //code
        $code = self::generateCode($faker, $value);
        $attributes['code'] = $code;
        //licence
        $licence = self::generateLicence($faker, $value);
        $attributes['licence'] = $licence;
        //organizationCode
        $organizationCode = self::generateOrganizationCode($faker, $value);
        $attributes['organizationCode'] = $organizationCode;
        //organizationCodeCertificate
        $organizationCodeCertificate = self::generateOrganizationCodeCertificate($faker, $value);
        $attributes['organizationCodeCertificate'] = $organizationCodeCertificate;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $educationalAuthentication['data']['attributes'] = $attributes;
        //enterprise
        $educationalAuthentication['data']['relationships']['enterprise']['data'] = array(
            'type' => 'enterprises',
            'id' => $faker->randomNumber(1)
        );
        
        return $educationalAuthentication;
    }

    /**
     * [generateEducationalAuthenticationObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateEducationalAuthenticationObject(
        EducationalAuthentication $educationalAuthentication,
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : EducationalAuthentication {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        //enterpriseName
        $enterpriseName = self::generateEnterpriseName($faker, $value);
        $educationalAuthentication->setEnterpriseName($enterpriseName);
        //organizationsCategory
        $organizationsCategory = self::generateOrganizationsCategory($faker, $value);
        $educationalAuthentication->setOrganizationsCategory($organizationsCategory);
        //organizationsCategoryParent
        $organizationsCategoryParent = self::generateOrganizationsCategoryParent($faker, $value);
        $educationalAuthentication->setOrganizationsCategoryParent($organizationsCategoryParent);
        //code
        $code = self::generateCode($faker, $value);
        $educationalAuthentication->setCode($code);
        //licence
        $licence = self::generateLicence($faker, $value);
        $educationalAuthentication->setLicence($licence);
        //organizationCode
        $organizationCode = self::generateOrganizationCode($faker, $value);
        $educationalAuthentication->setOrganizationCode($organizationCode);
        //organizationCodeCertificate
        $organizationCodeCertificate = self::generateOrganizationCodeCertificate($faker, $value);
        $educationalAuthentication->setOrganizationCodeCertificate($organizationCodeCertificate);

        //enterprise
        $enterprise = self::generateEnterprise($faker, $value);
        $educationalAuthentication->setEnterprise($enterprise);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $educationalAuthentication->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $educationalAuthentication->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $educationalAuthentication->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $educationalAuthentication->setStatus($status);

        return $educationalAuthentication;
    }

    private static function generateEnterpriseName($faker, array $value = array())
    {
        return $enterpriseName = isset($value['enterpriseName']) ?
        $value['enterpriseName'] : $faker->name;
    }

    const ORGANIZATIONS_SECOND_CATEGORY_ID = 1;

    private static function generateOrganizationsCategory($faker, array $value = array())
    {
        return $organizationsCategory = isset($value['organizationsCategory']) ?
        $value['organizationsCategory'] : EducationalAuthenticationCategoryFactory::create(
            self::ORGANIZATIONS_SECOND_CATEGORY_ID,
            EducationalAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_SECOND_CATEGORY']
        );
    }

    const ORGANIZATIONS_FIRST_CATEGORY_ID = 1;

    private static function generateOrganizationsCategoryParent($faker, array $value = array())
    {
        return $organizationsCategoryParent = isset($value['organizationsCategoryParent']) ?
        $value['organizationsCategoryParent'] : EducationalAuthenticationCategoryFactory::create(
            self::ORGANIZATIONS_FIRST_CATEGORY_ID,
            EducationalAuthenticationCategoryFactory::TYPE['ORGANIZATIONS_FIRST_CATEGORY']
        );
    }

    private static function generateCode($faker, array $value = array())
    {
        return $code = isset($value['code']) ?
        $value['code'] : $faker->word;
    }

    private static function generateLicence($faker, array $value = array())
    {
        return $licence = isset($value['licence']) ?
        $value['licence'] : array('name' => '金融许可证', 'identify' => '金融许可证.png');
    }

    private static function generateOrganizationCode($faker, array $value = array())
    {
        return $organizationCode = isset($value['organizationCode']) ?
            $value['organizationCode'] : $faker->word;
    }

    private static function generateOrganizationCodeCertificate($faker, array $value = array())
    {
        return $organizationCodeCertificate = isset($value['organizationCodeCertificate']) ?
        $value['organizationCodeCertificate'] : array('name' => '金融机构代码证', 'identify' => '金融机构代码证.png');
    }

    private static function generateEnterprise($faker, array $value = array())
    {
        return $enterprise = isset($value['enterprise']) ?
            $value['enterprise'] : \Sdk\Enterprise\Utils\EnterpriseMockFactory::generateEnterpriseObject(
                new Enterprise(),
                $faker->numerify()
            );
    }
}
