<?php
namespace Sdk\PlatformAccount\Repository;

use Sdk\PlatformAccount\Adapter\PlatformAccount\PlatformAccountRestfulAdapter;
use Sdk\PlatformAccount\Utils\MockFactory;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class PlatformAccountRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PlatformAccountRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends PlatformAccountRepository {
            public function getAdapter() : PlatformAccountRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PlatformAccount\Adapter\PlatformAccount\PlatformAccountRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为PlatformAccountRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以PlatformAccountRepository::FETCH_ONE_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(PlatformAccountRestfulAdapter::class);
        $adapter->scenario(Argument::exact(PlatformAccountRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(PlatformAccountRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    public function testFetchPlatformAccount()
    {
        $platformAccount = MockFactory::generatePlatformAccount();

        $adapter = $this->prophesize(PlatformAccountRestfulAdapter::class);
        $adapter->fetchPlatformAccount()->shouldBeCalledTimes(1)->willReturn($platformAccount);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->fetchPlatformAccount();
        $this->assertEquals($platformAccount, $result);
    }
}
