<?php
namespace Sdk\PlatformAccount\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\PlatformAccount\Utils\MockFactory;
use Sdk\PlatformAccount\Model\PlatformAccount;

class PlatformAccountRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new PlatformAccountRestfulTranslator();
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new PlatformAccount());
        $this->assertInstanceOf('Sdk\PlatformAccount\Model\NullPlatformAccount', $result);
    }

    public function setMethods(PlatformAccount $expectObject, array $attributes)
    {
        if (isset($attributes['accountBalance'])) {
            $expectObject->setAccountBalance($attributes['accountBalance']);
        }
        if (isset($attributes['incomeAmount'])) {
            $expectObject->setIncomeAmount($attributes['incomeAmount']);
        }
        if (isset($attributes['expenditureAmount'])) {
            $expectObject->setExpenditureAmount($attributes['expenditureAmount']);
        }
        if (isset($attributes['profitAmount'])) {
            $expectObject->setProfitAmount($attributes['profitAmount']);
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $platformAccount = MockFactory::generatePlatformAccountArray();
        $data = $platformAccount['data'];

        $actual = $this->stub->arrayToObject($platformAccount);

        $expectObject = new PlatformAccount();

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0, array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $platformAccount = MockFactory::generatePlatformAccountArray();
        $data = $platformAccount['data'];
        
        $actual = $this->stub->arrayToObjects($platformAccount);

        $expectArray = array();

        $expectObject = new PlatformAccount();

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes);

        $expectArray = [1, [$data['id'] => $expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
    /**
     * 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
}
