<?php
namespace Sdk\PlatformAccount\Adapter\PlatformAccount;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\PlatformAccount\Utils\MockFactory;
use Sdk\PlatformAccount\Model\PlatformAccount;
use Sdk\PlatformAccount\Model\NullPlatformAccount;
use Sdk\PlatformAccount\Translator\PlatformAccountRestfulTranslator;

class PlatformAccountRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PlatformAccountRestfulAdapter::class)
            ->setMethods(['isSuccess', 'get', 'translateToObject'])->getMock();

        $this->childStub = new class extends PlatformAccountRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIPlatformAccountAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PlatformAccount\Adapter\PlatformAccount\IPlatformAccountAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('platformAccount', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\PlatformAccount\Translator\PlatformAccountRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }

    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'PLATFORM_ACCOUNT_FETCH_ONE',
                PlatformAccountRestfulAdapter::SCENARIOS['PLATFORM_ACCOUNT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchPlatformAccount()
    {
        $platformAccount = MockFactory::generatePlatformAccount();
        $platformAccountArray = array('platformAccount');

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with('platformAccount');

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->willReturn($platformAccount);

        $result = $this->stub->fetchPlatformAccount();
        $this->assertEquals($platformAccount, $result);
    }
}
