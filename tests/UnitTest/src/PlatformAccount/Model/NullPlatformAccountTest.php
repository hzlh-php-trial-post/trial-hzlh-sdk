<?php
namespace Sdk\PlatformAccount\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullPlatformAccountTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullPlatformAccount::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsPlatformAccount()
    {
        $this->assertInstanceof('Sdk\PlatformAccount\Model\PlatformAccount', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
