<?php
namespace Sdk\GovernmentOrgans\Repository;

use Sdk\GovernmentOrgans\Adapter\GovernmentOrgans\GovernmentOrgansRestfulAdapter;
use Sdk\GovernmentOrgans\Adapter\GovernmentOrgans\GovernmentOrgansMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class GovernmentOrgansRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(GovernmentOrgansRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends GovernmentOrgansRepository {
            public function getAdapter() : GovernmentOrgansRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : GovernmentOrgansMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\GovernmentOrgans\Adapter\GovernmentOrgans\GovernmentOrgansRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\GovernmentOrgans\Adapter\GovernmentOrgans\GovernmentOrgansMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为GovernmentOrgansRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以GovernmentOrgansRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(GovernmentOrgansRestfulAdapter::class);
        $adapter->scenario(Argument::exact(GovernmentOrgansRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);//phpcs:ignore

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(GovernmentOrgansRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
