<?php
namespace Sdk\GovernmentOrgans\Translator;

use Sdk\GovernmentOrgans\Model\NullGovernmentOrgans;
use Sdk\GovernmentOrgans\Model\GovernmentOrgans;
use Sdk\GovernmentOrgans\Model\Answer;
use Sdk\GovernmentOrgans\Model\DeleteInfo;

use Sdk\Crew\Model\Crew;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Crew\Translator\CrewRestfulTranslator;

use Sdk\GovernmentOrgans\Utils\MockFactory;

class GovernmentOrgansRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            GovernmentOrgansRestfulTranslator::class
        )
            ->setMethods([
                'getCrewRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends GovernmentOrgansRestfulTranslator {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new GovernmentOrgans());
        $this->assertInstanceOf('Sdk\GovernmentOrgans\Model\NullGovernmentOrgans', $result);
    }

    public function setMethods(GovernmentOrgans $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['name'])) {
            $expectObject->setName($attributes['name']);
        }
        if (isset($attributes['area'])) {
            $expectObject->setArea($attributes['area']);
        }
        if (isset($attributes['address'])) {
            $expectObject->setAddress($attributes['address']);
        }
        if (isset($attributes['serviceTime'])) {
            $expectObject->setServiceTime($attributes['serviceTime']);
        }
        if (isset($attributes['hotline'])) {
            $expectObject->setHotline($attributes['hotline']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }

        if (isset($relationships['crew']['data'])) {
            $expectObject->setCrew(new Crew($relationships['crew']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansArray();

        $data =  $governmentOrgans['data'];
        $relationships = $data['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);
        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($governmentOrgans);

        $expectObject = new GovernmentOrgans();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansArray();
        $data =  $governmentOrgans['data'];
        $relationships = $data['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);
        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($governmentOrgans);
        $expectArray = array();

        $expectObject = new GovernmentOrgans();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1, 1);

        $actual = $this->stub->objectToArray(
            $governmentOrgans,
            array(
                'name',
                'area',
                'address',
                'serviceTime',
                'hotline',
                'crew',
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'governmentOrgans'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'name'=>$governmentOrgans->getName(),
            'area'=>$governmentOrgans->getArea(),
            'address'=>$governmentOrgans->getAddress(),
            'serviceTime'=>$governmentOrgans->getServiceTime(),
            'hotline'=>$governmentOrgans->getHotline()
        );

        $expectedArray['data']['relationships']['crew']['data'] = array(
            array(
                'type' => 'crews',
                'id' => $governmentOrgans->getCrew()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
