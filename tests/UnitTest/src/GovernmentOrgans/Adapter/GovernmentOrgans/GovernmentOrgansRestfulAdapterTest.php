<?php
namespace Sdk\GovernmentOrgans\Adapter\GovernmentOrgans;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\GovernmentOrgans\Model\GovernmentOrgans;
use Sdk\GovernmentOrgans\Model\NullGovernmentOrgans;
use Sdk\GovernmentOrgans\Utils\MockFactory;
use Sdk\GovernmentOrgans\Translator\GovernmentOrgansRestfulTranslator;

class GovernmentOrgansRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(GovernmentOrgansRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends GovernmentOrgansRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIGovernmentOrgansAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\GovernmentOrgans\Adapter\GovernmentOrgans\IGovernmentOrgansAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('governmentOrgans', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\GovernmentOrgans\Translator\GovernmentOrgansRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_GOVERNMENT_ORGANS_LIST',
                GovernmentOrgansRestfulAdapter::SCENARIOS['OA_GOVERNMENT_ORGANS_LIST']
            ],
            [
                'PORTAL_GOVERNMENT_ORGANS_LIST',
                GovernmentOrgansRestfulAdapter::SCENARIOS['PORTAL_GOVERNMENT_ORGANS_LIST']
            ],
            [
                'GOVERNMENT_ORGANS_FETCH_ONE',
                GovernmentOrgansRestfulAdapter::SCENARIOS['GOVERNMENT_ORGANS_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $governmentOrgans = MockFactory::generateGovernmentOrgansObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullGovernmentOrgans())
            ->willReturn($governmentOrgans);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($governmentOrgans, $result);
    }
    /**
     * 为GovernmentOrgansRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$governmentOrgans$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareGovernmentOrgansTranslator(
        GovernmentOrgans $governmentOrgans,
        array $keys,
        array $governmentOrgansArray
    ) {
        $translator = $this->prophesize(GovernmentOrgansRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($governmentOrgans),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($governmentOrgansArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(GovernmentOrgans $governmentOrgans)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($governmentOrgans);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGovernmentOrgansTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1);
        $governmentOrgansArray = array();

        $this->prepareGovernmentOrgansTranslator(
            $governmentOrgans,
            array(
                'name',
                'area',
                'address',
                'serviceTime',
                'hotline',
                'crew'
            ),
            $governmentOrgansArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('governmentOrgans', $governmentOrgansArray);

        $this->success($governmentOrgans);

        $result = $this->stub->add($governmentOrgans);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGovernmentOrgansTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1);
        $governmentOrgansArray = array();

        $this->prepareGovernmentOrgansTranslator(
            $governmentOrgans,
            array(
                'name',
                'area',
                'address',
                'serviceTime',
                'hotline',
                'crew'
            ),
            $governmentOrgansArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('governmentOrgans', $governmentOrgansArray);

        $this->failure($governmentOrgans);
        $result = $this->stub->add($governmentOrgans);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGovernmentOrgansTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1);
        $governmentOrgansArray = array();

        $this->prepareGovernmentOrgansTranslator(
            $governmentOrgans,
            array(
                'name',
                'area',
                'address',
                'serviceTime',
                'hotline'
            ),
            $governmentOrgansArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('governmentOrgans/'.$governmentOrgans->getId(), $governmentOrgansArray);

        $this->success($governmentOrgans);

        $result = $this->stub->edit($governmentOrgans);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGovernmentOrgansTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1);
        $governmentOrgansArray = array();

        $this->prepareGovernmentOrgansTranslator(
            $governmentOrgans,
            array(
                'name',
                'area',
                'address',
                'serviceTime',
                'hotline'
            ),
            $governmentOrgansArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('governmentOrgans/'.$governmentOrgans->getId(), $governmentOrgansArray);

        $this->failure($governmentOrgans);
        $result = $this->stub->edit($governmentOrgans);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGovernmentOrgansTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行onShelf（）
     * 判断 result 是否为true
     */
    public function testOnShelfSuccess()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'governmentOrgans/'.$governmentOrgans->getId().'/onShelf'
            );

        $this->success($governmentOrgans);

        $result = $this->stub->onShelf($governmentOrgans);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGovernmentOrgansTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行onShelf（）
     * 判断 result 是否为false
     */
    public function testOnShelfFailure()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'governmentOrgans/'.$governmentOrgans->getId().'/onShelf'
            );

        $this->failure($governmentOrgans);
        $result = $this->stub->onShelf($governmentOrgans);
        $this->assertFalse($result);
    }

        /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGovernmentOrgansTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行offStock（）
     * 判断 result 是否为true
     */
    public function testOffStockSuccess()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'governmentOrgans/'.$governmentOrgans->getId().'/offStock'
            );

        $this->success($governmentOrgans);

        $result = $this->stub->offStock($governmentOrgans);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareGovernmentOrgansTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行offStock（）
     * 判断 result 是否为false
     */
    public function testOffStockFailure()
    {
        $governmentOrgans = MockFactory::generateGovernmentOrgansObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'governmentOrgans/'.$governmentOrgans->getId().'/offStock'
            );

        $this->failure($governmentOrgans);
        $result = $this->stub->offStock($governmentOrgans);
        $this->assertFalse($result);
    }
}
