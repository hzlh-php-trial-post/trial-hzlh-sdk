<?php
namespace Sdk\GovernmentOrgans\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullGovernmentOrgansTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullGovernmentOrgans::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsGovernmentOrgans()
    {
        $this->assertInstanceof('Sdk\GovernmentOrgans\Model\GovernmentOrgans', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
