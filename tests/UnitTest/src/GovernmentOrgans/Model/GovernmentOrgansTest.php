<?php
namespace Sdk\GovernmentOrgans\Model;

use Sdk\GovernmentOrgans\Repository\GovernmentOrgansRepository;

use Sdk\Crew\Model\Crew;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IOnShelfAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class GovernmentOrgansTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(GovernmentOrgans::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends GovernmentOrgans{
            public function getRepository() : GovernmentOrgansRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
            public function getIOnShelfAbleAdapter() : IOnShelfAbleAdapter
            {
                return parent::getIOnShelfAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\GovernmentOrgans\Repository\GovernmentOrgansRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    public function testGetIOnShelfAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOnShelfAbleAdapter',
            $this->childStub->getIOnShelfAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //name 测试 ---------------------------------------------------------- start
    /**
     * 设置setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }
    //name 测试 ----------------------------------------------------------   end

    //area 测试 ---------------------------------------------------------- start
    /**
     * 设置setArea() 正确的传参类型,期望传值正确
     */
    public function testSetAreaCorrectType()
    {
        $this->stub->setArea('string');
        $this->assertEquals('string', $this->stub->getArea());
    }

    /**
     * 设置setArea() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAreaWrongType()
    {
        $this->stub->setArea(array(1, 2, 3));
    }
    //area 测试 ----------------------------------------------------------   end

    //address 测试 ---------------------------------------------------------- start
    /**
     * 设置setAddress() 正确的传参类型,期望传值正确
     */
    public function testSetAddressCorrectType()
    {
        $this->stub->setAddress('string');
        $this->assertEquals('string', $this->stub->getAddress());
    }

    /**
     * 设置setAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAddressWrongType()
    {
        $this->stub->setAddress(array(1, 2, 3));
    }
    //address 测试 ----------------------------------------------------------   end

    //hotline 测试 ---------------------------------------------------------- start
    /**
     * 设置setHotline() 正确的传参类型,期望传值正确
     */
    public function testSetHotlineCorrectType()
    {
        $this->stub->setHotline('string');
        $this->assertEquals('string', $this->stub->getHotline());
    }

    /**
     * 设置setHotline() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHotlineWrongType()
    {
        $this->stub->setHotline(array(1, 2, 3));
    }
    //hotline 测试 ----------------------------------------------------------   end

    //serviceTime 测试 ---------------------------------------------------------- start
    /**
     * 设置setServiceTime() 正确的传参类型,期望传值正确
     */
    public function testSetServiceTimeCorrectType()
    {
        $this->stub->setServiceTime('string');
        $this->assertEquals('string', $this->stub->getServiceTime());
    }

    /**
     * 设置setServiceTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetServiceTimeWrongType()
    {
        $this->stub->setServiceTime(array(1, 2, 3));
    }
    //serviceTime 测试 ----------------------------------------------------------   end

    //crew 测试 ---------------------------------------------------------- start
    /**
     * 设置setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();

        $this->stub->setCrew($object);
        $this->assertEquals($object, $this->stub->getCrew());
    }

    /**
     * 设置setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->stub->setCrew(array(1, 2, 3));
    }
    //crew 测试 ----------------------------------------------------------   end
}
