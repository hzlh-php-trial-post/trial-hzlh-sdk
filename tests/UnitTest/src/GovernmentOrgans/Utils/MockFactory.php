<?php
namespace Sdk\GovernmentOrgans\Utils;

use Sdk\GovernmentOrgans\Model\GovernmentOrgans;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateGovernmentOrgansArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateGovernmentOrgansArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $governmentOrgans = array();

        $governmentOrgans = array(
            'data'=>array(
                'type'=>'governmentOrganss',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //area
        $area = self::generateArea($faker, $value);
        $attributes['area'] = $area;
        //address
        $address = self::generateAddress($faker, $value);
        $attributes['address'] = $address;
        //hotline
        $hotline = self::generateHotline($faker, $value);
        $attributes['hotline'] = $hotline;
        //serviceTime
        $serviceTime = self::generateServiceTime($faker, $value);
        $attributes['serviceTime'] = $serviceTime;

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $governmentOrgans['data']['attributes'] = $attributes;
        //crew
        $governmentOrgans['data']['relationships']['crew']['data'] = array(
            'type' => 'crews',
            'id' => $faker->randomNumber(1)
        );

        return $governmentOrgans;
    }
    /**
     * [generateGovernmentOrgansObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateGovernmentOrgansObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : GovernmentOrgans {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $governmentOrgans = new GovernmentOrgans($id);

        //name
        $name = self::generateName($faker, $value);
        $governmentOrgans->setName($name);
        //area
        $area = self::generateArea($faker, $value);
        $governmentOrgans->setArea($area);
        //address
        $address = self::generateAddress($faker, $value);
        $governmentOrgans->setAddress($address);
        //hotline
        $hotline = self::generateHotline($faker, $value);
        $governmentOrgans->setHotline($hotline);
        //serviceTime
        $serviceTime = self::generateServiceTime($faker, $value);
        $governmentOrgans->setServiceTime($serviceTime);

        //crew
        $crew = self::generateCrew($faker, $value);
        $governmentOrgans->setCrew($crew);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $governmentOrgans->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $governmentOrgans->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $governmentOrgans->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $governmentOrgans->setStatus($status);

        return $governmentOrgans;
    }

    private static function generateName($faker, array $value = array())
    {
        return isset($value['name']) ?
        $value['name'] : $faker->name;
    }

    private static function generateArea($faker, array $value = array())
    {
        return isset($value['area']) ?
        $value['area'] : $faker->area;
    }

    private static function generateAddress($faker, array $value = array())
    {
        return isset($value['address']) ?
        $value['address'] : $faker->word;
    }

    private static function generateHotline($faker, array $value = array())
    {
        return isset($value['hotline']) ?
        $value['hotline'] : $faker->word;
    }

    private static function generateServiceTime($faker, array $value = array())
    {
        return isset($value['serviceTime']) ?
        $value['serviceTime'] : '$faker->word';
    }

    private static function generateCrew($faker, array $value = array())
    {
        return isset($value['crew']) ?
            $value['crew'] : \Sdk\Crew\Utils\MockFactory::generateCrewObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
