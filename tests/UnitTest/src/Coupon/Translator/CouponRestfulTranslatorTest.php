<?php
namespace Sdk\Coupon\Translator;

use Sdk\Coupon\Model\NullCoupon;
use Sdk\Coupon\Model\Coupon;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\Member\Model\Member;
use Sdk\MerchantCoupon\Translator\MerchantCouponRestfulTranslator;
use Sdk\MerchantCoupon\Model\MerchantCoupon;

class CouponRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            CouponRestfulTranslator::class
        )
            ->setMethods(['getMemberRestfulTranslator','getMerchantCouponRestfulTranslator'])
            ->getMock();

        $this->childStub =
        new class extends CouponRestfulTranslator {
            public function getMemberRestfulTranslator() : MemberRestfulTranslator
            {
                return parent::getMemberRestfulTranslator();
            }
            public function getMerchantCouponRestfulTranslator() : MerchantCouponRestfulTranslator
            {
                return parent::getMerchantCouponRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetMemberRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberRestfulTranslator',
            $this->childStub->getMemberRestfulTranslator()
        );
    }

    public function testGetMerchantCouponRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\MerchantCoupon\Translator\MerchantCouponRestfulTranslator',
            $this->childStub->getMerchantCouponRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Coupon());
        $this->assertInstanceOf('Sdk\Coupon\Model\NullCoupon', $result);
    }

    public function setMethods(Coupon $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['releaseType'])) {
            $expectObject->setReleaseType($attributes['releaseType']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['member']['data'])) {
            $expectObject->setMember(new Member($relationships['member']['data']['id']));
        }
        if (isset($relationships['MerchantCoupon']['data'])) {
            $expectObject->setMerchantCoupon(new MerchantCoupon($relationships['MerchantCoupon']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $coupon = \Sdk\Coupon\Utils\MockFactory::generateCouponArray();

        $data =  $coupon['data'];
        $relationships = $data['relationships'];

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($coupon);

        $expectObject = new Coupon();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $coupon = \Sdk\Coupon\Utils\MockFactory::generateCouponArray();

        $data =  $coupon['data'];
        $relationships = $data['relationships'];

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($coupon);
        $expectArray = array();

        $expectObject = new Coupon();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $coupon = \Sdk\Coupon\Utils\MockFactory::generateCouponObject(1, 1);

        $actual = $this->stub->objectToArray($coupon, array(
            'merchantCoupon',
            'member',
        ));
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'memberCoupons'
            )
        );

        $expectedArray['data']['attributes'] = array();

        $expectedArray['data']['relationships']['member']['data'] = array(
            array(
                'type' => 'members',
                'id' => $coupon->getMember()->getId()
            )
        );

        $expectedArray['data']['relationships']['releaseCoupon']['data'] = array(
            array(
                'type' => 'releaseCoupons',
                'id' => $coupon->getMerchantCoupon()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
