<?php
namespace Sdk\Coupon\Utils;

use Sdk\Coupon\Model\Coupon;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateCouponArray 生成收货地址数组]
     * @return [array] [地址信息]
     */
    public static function generateCouponArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $coupon = array();

        $coupon = array(
            'data'=>array(
                'type'=>'memberCoupons',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();
        //releaseType
        $releaseType = self::generateReleaseType($faker, $value);
        $attributes['releaseType'] = $releaseType;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $coupon['data']['attributes'] = $attributes;

        $coupon['data']['relationships']['merchantCoupon']['data'] = array(
            'type' => 'merchantCoupons',
            'id' => $faker->randomNumber(1)
        );

        $coupon['data']['relationships']['member']['data'] = array(
            'type' => 'members',
            'id' => $faker->randomNumber(1)
        );

        return $coupon;
    }

    /**
     * [generateCouponObject 生成收货地址信息对象]
     * @param  int|integer $id
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]             [地址信息]
     */
    public static function generateCouponObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Coupon {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $coupon = new Coupon($id);

        //releaseType
        $releaseType = self::generateReleaseType($faker, $value);
        $coupon->setReleaseType($releaseType);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $coupon->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $coupon->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $coupon->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $coupon->setStatus($status);

        //merchantCoupon
        $merchantCoupon = self::generateMerchantCoupon($faker, $value);
        $coupon->setMerchantCoupon($merchantCoupon);
        //member
        $member = self::generateMember($faker, $value);
        $coupon->setMember($member);

        return $coupon;
    }

    private static function generateReleaseType($faker, array $value = array())
    {
        return $releaseType = isset($attributes['releaseType']) ?
        $attributes['releaseType'] : 0;
    }

    private static function generateMerchantCoupon($faker, array $value = array())
    {
        return $merchantCoupon = isset($value['merchantCoupon']) ?
            $value['merchantCoupon'] : \Sdk\MerchantCoupon\Utils\MockFactory::generateMerchantCouponObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generateMember($faker, array $value = array())
    {
        return $member = isset($value['member']) ?
            $value['member'] : \Sdk\Member\Utils\MockFactory::generateMemberObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
