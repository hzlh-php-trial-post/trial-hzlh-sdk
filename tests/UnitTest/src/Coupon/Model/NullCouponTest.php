<?php
namespace Sdk\Coupon\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullCouponTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullCoupon::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCoupon()
    {
        $this->assertInstanceof('Sdk\Coupon\Model\Coupon', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
