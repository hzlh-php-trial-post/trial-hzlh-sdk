<?php
namespace Sdk\Coupon\Adapter\Coupon;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Coupon\Model\Coupon;
use Sdk\Coupon\Model\NullCoupon;
use Sdk\Coupon\Utils\MockFactory;
use Sdk\Coupon\Translator\CouponRestfulTranslator;

class CouponRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CouponRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends CouponRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsICouponAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Coupon\Adapter\Coupon\ICouponAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('memberCoupons', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Coupon\Translator\CouponRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'PORTAL_COUPON_LIST',
                CouponRestfulAdapter::SCENARIOS['PORTAL_COUPON_LIST']
            ],
            [
                'COUPON_FETCH_ONE',
                CouponRestfulAdapter::SCENARIOS['COUPON_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $coupon = MockFactory::generateCouponObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullCoupon())
            ->willReturn($coupon);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($coupon, $result);
    }

    /**
     * 为CouponRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Coupon$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareCouponTranslator(
        Coupon $coupon,
        array $keys,
        array $couponArray
    ) {
        $translator = $this->prophesize(CouponRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($coupon),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($couponArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Coupon $coupon)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($coupon);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCouponTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testReceiveSuccess()
    {
        $coupon = MockFactory::generateCouponObject(1);
        $couponArray = array();

        $this->prepareCouponTranslator(
            $coupon,
            array(
                'merchantCoupon',
                'member'
            ),
            $couponArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('memberCoupons', $couponArray);

        $this->success($coupon);

        $result = $this->stub->receive($coupon);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCouponTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testReceiveFailure()
    {
        $coupon = MockFactory::generateCouponObject(1);
        $couponArray = array();

        $this->prepareCouponTranslator(
            $coupon,
            array(
                'merchantCoupon',
                'member'
            ),
            $couponArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('memberCoupons', $couponArray);

        $this->failure($coupon);
        $result = $this->stub->receive($coupon);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCouponTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $coupon = MockFactory::generateCouponObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'memberCoupons/'.$coupon->getId().'/delete'
            );

        $this->success($coupon);

        $result = $this->stub->deletes($coupon);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCouponTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $coupon = MockFactory::generateCouponObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'memberCoupons/'.$coupon->getId().'/delete'
            );

        $this->failure($coupon);
        $result = $this->stub->deletes($coupon);
        $this->assertFalse($result);
    }
}
