<?php
namespace Sdk\TradeRecord\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullTradeRecordTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullTradeRecord::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsTradeRecord()
    {
        $this->assertInstanceof('Sdk\TradeRecord\Model\TradeRecord', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
