<?php
namespace Sdk\PolicySubject\Utils;

use Sdk\PolicySubject\Model\PolicySubject;

class MockFactory
{
    /**
     * [generatePolicySubjectArray 生成政策专题数组]
     * @return [array] [政策专题数组]
     */
    public static function generatePolicySubjectArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $policySubject = array();

        $policySubject = array(
            'data'=>array(
                'type'=>'policySubjects',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //title
        $title = self::generateTitle($faker, $value);
        $attributes['title'] = $title;
        //cover
        $cover = self::generateCover($faker, $value);
        $attributes['cover'] = $cover;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $policySubject['data']['attributes'] = $attributes;
        //crew
        $policySubject['data']['relationships']['crew']['data'] = array(
            'type' => 'crews',
            'id' => $faker->randomNumber(1)
        );

        return $policySubject;
    }
    /**
     * [generatePolicySubjectObject 生成政策专题对象]
     * @param  int|integer $id    [政策专题Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [政策专题对象]
     */
    public static function generatePolicySubjectObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : PolicySubject {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $policySubject = new PolicySubject($id);

        //title
        $title = self::generateTitle($faker, $value);
        $policySubject->setTitle($title);
        //cover
        $cover = self::generateCover($faker, $value);
        $policySubject->setCover($cover);
        //crew
        $crew = self::generateCrew($faker, $value);
        $policySubject->setCrew($crew);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $policySubject->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $policySubject->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $policySubject->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $policySubject->setStatus($status);

        return $policySubject;
    }

    private static function generateTitle($faker, array $value = array())
    {
        return isset($value['title']) ?
        $value['title'] : $faker->title;
    }

    private static function generateCover($faker, array $value = array())
    {
        return isset($value['cover']) ?
        $value['cover'] : array('title' => 'cover', 'identify' => 'cover.jpg');
    }

    private static function generateCrew($faker, array $value = array())
    {
        return isset($value['crew']) ?
            $value['crew'] : \Sdk\Crew\Utils\MockFactory::generateCrewObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
