<?php
namespace Sdk\LoanProductTemplate\Translator;

use Sdk\LoanProductTemplate\Model\NullLoanProductTemplate;
use Sdk\LoanProductTemplate\Model\LoanProductTemplate;

use Sdk\Enterprise\Model\Enterprise;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\LoanProductTemplate\Utils\MockFactory;

class LoanProductTemplateRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            LoanProductTemplateRestfulTranslator::class
        )
            ->setMethods([
                'getEnterpriseRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends LoanProductTemplateRestfulTranslator {
            public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
            {
                return parent::getEnterpriseRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->childStub->getEnterpriseRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new LoanProductTemplate());
        $this->assertInstanceOf('Sdk\LoanProductTemplate\Model\NullLoanProductTemplate', $result);
    }

    public function setMethods(LoanProductTemplate $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['name'])) {
            $expectObject->setName($attributes['name']);
        }
        if (isset($attributes['fileType'])) {
            $expectObject->setFileType($attributes['fileType']);
        }
        if (isset($attributes['file'])) {
            $expectObject->setFile($attributes['file']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($relationships['enterprise']['data'])) {
            $expectObject->setEnterprise(new Enterprise($relationships['enterprise']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateArray();

        $data =  $loanProductTemplate['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($loanProductTemplate);

        $expectObject = new LoanProductTemplate();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateArray();
        $data =  $loanProductTemplate['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($loanProductTemplate);
        $expectArray = array();

        $expectObject = new LoanProductTemplate();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateObject(1, 1);

        $actual = $this->stub->objectToArray(
            $loanProductTemplate,
            array(
                'name',
                'fileType',
                'file',
                'enterprise'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'loanProductTemplates'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'name'=>$loanProductTemplate->getName(),
            'fileType'=>$loanProductTemplate->getFileType(),
            'file'=>$loanProductTemplate->getFile()
        );

        $expectedArray['data']['relationships']['enterprise']['data'] = array(
            array(
                'type' => 'enterprises',
                'id' => $loanProductTemplate->getEnterprise()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
