<?php
namespace Sdk\LoanProductTemplate\Repository;

use Sdk\LoanProductTemplate\Adapter\LoanProductTemplate\LoanProductTemplateRestfulAdapter;
use Sdk\LoanProductTemplate\Adapter\LoanProductTemplate\LoanProductTemplateMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class LoanProductTemplateRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(LoanProductTemplateRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends LoanProductTemplateRepository {
            public function getAdapter() : LoanProductTemplateRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : LoanProductTemplateMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProductTemplate\Adapter\LoanProductTemplate\LoanProductTemplateRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProductTemplate\Adapter\LoanProductTemplate\LoanProductTemplateMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为LoanProductTemplateRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以LoanProductTemplateRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(LoanProductTemplateRestfulAdapter::class);
        $adapter->scenario(Argument::exact(LoanProductTemplateRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(LoanProductTemplateRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为LoanProductTemplateRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testDeletes()
    {
        $accountTemplate = \Sdk\LoanProductTemplate\Utils\MockFactory::generateLoanProductTemplateObject(1);

        $adapter = $this->prophesize(LoanProductTemplateRestfulAdapter::class);
        $adapter->deletes(
            Argument::exact($accountTemplate)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deletes($accountTemplate);
        $this->assertTrue($result);
    }
}
