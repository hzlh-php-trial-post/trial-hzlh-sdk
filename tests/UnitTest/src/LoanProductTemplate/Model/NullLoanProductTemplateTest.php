<?php
namespace Sdk\LoanProductTemplate\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullLoanProductTemplateTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullLoanProductTemplate::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsLoanProductTemplate()
    {
        $this->assertInstanceof('Sdk\LoanProductTemplate\Model\LoanProductTemplate', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
