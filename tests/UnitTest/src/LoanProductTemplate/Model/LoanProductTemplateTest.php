<?php
namespace Sdk\LoanProductTemplate\Model;

use Sdk\LoanProductTemplate\Repository\LoanProductTemplateRepository;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Member\Model\Member;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class LoanProductTemplateTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(LoanProductTemplate::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends LoanProductTemplate{
            public function getRepository() : LoanProductTemplateRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProductTemplate\Repository\LoanProductTemplateRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //name 测试 ---------------------------------------------------------- start
    /**
     * 设置setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }
    //name 测试 ----------------------------------------------------------   end

    //fileType 测试 ---------------------------------------------------------- start
    /**
     * 设置setFileType() 正确的传参类型,期望传值正确
     */
    public function testSetFileTypeCorrectType()
    {
        $this->stub->setFileType(1);
        $this->assertEquals(1, $this->stub->getFileType());
    }

    /**
     * 设置setFileType() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetFileTypeWrongTypeButNumeric()
    {
        $this->stub->setFileType('1');
        $this->assertTrue(is_int($this->stub->getFileType()));
        $this->assertEquals(1, $this->stub->getFileType());
    }
    //fileType 测试 ----------------------------------------------------------   end

    //file 测试 ---------------------------------------------------------- start
    /**
     * 设置setFile() 正确的传参类型,期望传值正确
     */
    public function testSetFileCorrectType()
    {
        $this->stub->setFile(array());
        $this->assertEquals(array(), $this->stub->getFile());
    }

    /**
     * 设置setFile() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFileWrongType()
    {
        $this->stub->setFile('1');
    }
    //file 测试 ----------------------------------------------------------   end

    //enterprise 测试 -------------------------------------------------------- start
    /**
     * 设置 setEnterprise() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseCorrectType()
    {
        $object = new \Sdk\Enterprise\Model\Enterprise();

        $this->stub->setEnterprise($object);
        $this->assertSame($object, $this->stub->getEnterprise());
    }

    /**
     * 设置 setEnterprise() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseType()
    {
        $this->stub->setEnterprise(array(1,2,3));
    }
    //enterprise 测试 -------------------------------------------------------- end
    
    //删除
    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(LoanProductTemplate::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(LoanProductTemplateRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFailure()
    {
        $this->stub = $this->getMockBuilder(LoanProductTemplate::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(LoanProductTemplateRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }
}
