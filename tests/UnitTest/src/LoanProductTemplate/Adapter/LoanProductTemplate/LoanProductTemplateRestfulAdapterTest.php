<?php
namespace Sdk\LoanProductTemplate\Adapter\LoanProductTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\LoanProductTemplate\Model\LoanProductTemplate;
use Sdk\LoanProductTemplate\Model\NullLoanProductTemplate;
use Sdk\LoanProductTemplate\Utils\MockFactory;
use Sdk\LoanProductTemplate\Translator\LoanProductTemplateRestfulTranslator;

class LoanProductTemplateRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(LoanProductTemplateRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends LoanProductTemplateRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsILoanProductTemplateAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProductTemplate\Adapter\LoanProductTemplate\ILoanProductTemplateAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('loanProductTemplates', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProductTemplate\Translator\LoanProductTemplateRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'LOAN_PRODUCT_TEMPLATE_LIST',
                LoanProductTemplateRestfulAdapter::SCENARIOS['LOAN_PRODUCT_TEMPLATE_LIST']
            ],
            [
                'LOAN_PRODUCT_TEMPLATE_FETCH_ONE',
                LoanProductTemplateRestfulAdapter::SCENARIOS['LOAN_PRODUCT_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $loanProductTemplate = MockFactory::generateLoanProductTemplateObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullLoanProductTemplate())
            ->willReturn($loanProductTemplate);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($loanProductTemplate, $result);
    }
    /**
     * 为LoanProductTemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$loanProductTemplate$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareLoanProductTemplateTranslator(
        LoanProductTemplate $loanProductTemplate,
        array $keys,
        array $loanProductTemplateArray
    ) {
        $translator = $this->prophesize(LoanProductTemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($loanProductTemplate),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($loanProductTemplateArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(LoanProductTemplate $loanProductTemplate)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($loanProductTemplate);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateObject(1);
        $loanProductTemplateArray = array();

        $this->prepareLoanProductTemplateTranslator(
            $loanProductTemplate,
            array(
                'name',
                'fileType',
                'file',
                'enterprise'
            ),
            $loanProductTemplateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('loanProductTemplates', $loanProductTemplateArray);

        $this->success($loanProductTemplate);

        $result = $this->stub->add($loanProductTemplate);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateObject(1);
        $loanProductTemplateArray = array();

        $this->prepareLoanProductTemplateTranslator(
            $loanProductTemplate,
            array(
                'name',
                'fileType',
                'file',
                'enterprise'
            ),
            $loanProductTemplateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('loanProductTemplates', $loanProductTemplateArray);

        $this->failure($loanProductTemplate);
        $result = $this->stub->add($loanProductTemplate);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateObject(1);
        $loanProductTemplateArray = array();

        $this->prepareLoanProductTemplateTranslator(
            $loanProductTemplate,
            array(
                'name',
                'fileType',
                'file'
            ),
            $loanProductTemplateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProductTemplates/'.$loanProductTemplate->getId(), $loanProductTemplateArray);

        $this->success($loanProductTemplate);

        $result = $this->stub->edit($loanProductTemplate);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateObject(1);
        $loanProductTemplateArray = array();

        $this->prepareLoanProductTemplateTranslator(
            $loanProductTemplate,
            array(
                'name',
                'fileType',
                'file'
            ),
            $loanProductTemplateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('loanProductTemplates/'.$loanProductTemplate->getId(), $loanProductTemplateArray);

        $this->failure($loanProductTemplate);
        $result = $this->stub->edit($loanProductTemplate);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'loanProductTemplates/'.$loanProductTemplate->getId().'/delete'
            );

        $this->success($loanProductTemplate);

        $result = $this->stub->deletes($loanProductTemplate);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareLoanProductTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $loanProductTemplate = MockFactory::generateLoanProductTemplateObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'loanProductTemplates/'.$loanProductTemplate->getId().'/delete'
            );

        $this->failure($loanProductTemplate);
        $result = $this->stub->deletes($loanProductTemplate);
        $this->assertFalse($result);
    }
}
