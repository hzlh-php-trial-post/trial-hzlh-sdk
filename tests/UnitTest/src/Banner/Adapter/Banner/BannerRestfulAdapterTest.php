<?php
namespace Sdk\Banner\Adapter\Banner;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Banner\Model\Banner;
use Sdk\Banner\Model\NullBanner;
use Sdk\Banner\Utils\MockFactory;
use Sdk\Banner\Translator\BannerRestfulTranslator;

class BannerRestfulAdapterTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(BannerRestfulAdapter::class)
            ->setMethods([
                'get',
                'isSuccess',
                'translateToObjects',
                'fetchOneAction',
                'getTranslator',
                'objectToArray',
                'post',
                'translateToObject',
                'patch',
                'delete'
            ])->getMock();
        
        $this->childStub  = new class extends BannerRestfulAdapter{
            public function getResources() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Adapter\Restful\GuzzleAdapter',
            $this->stub
        );
    }

    public function testImplementsIBannerAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Banner\Adapter\Banner\IBannerAdapter',
            $this->stub
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('banners', $this->childStub->getResources());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'BANNER_LIST',
                BannerRestfulAdapter::SCENARIOS['BANNER_LIST']
            ],
            [
                'BANNER_FETCH_ONE',
                BannerRestfulAdapter::SCENARIOS['BANNER_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $banner = MockFactory::generateBannerObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullBanner())
            ->willReturn($banner);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($banner, $result);
    }

    /**
     * 为BannerRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$banner,$keys,$bannerArray为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareBannerTranslator(
        Banner $banner,
        array $keys,
        array $bannerArray
    ) {
        $translator = $this->prophesize(BannerRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($banner),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($bannerArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Banner $banner)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($banner);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBannerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $banner = MockFactory::generateBannerObject(1);
        $bannerArray = array();

        $this->prepareBannerTranslator(
            $banner,
            array(
                'launchType',
                'type',
                'place',
                'image',
                'link',
                'remark',
                'crew',
                'tag'
            ),
            $bannerArray
        );

        $this->stub
            ->method('post')
            ->with('banners', $bannerArray);

        $this->success($banner);
        $result = $this->stub->add($banner);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBannerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $banner = MockFactory::generateBannerObject(1);
        $bannerArray = array();

        $this->prepareBannerTranslator(
            $banner,
            array(
                'launchType',
                'type',
                'place',
                'image',
                'link',
                'remark',
                'crew',
                'tag'
            ),
            $bannerArray
        );

        $this->stub
            ->method('post')
            ->with('banners', $bannerArray);
        
            $this->failure($banner);
            $result = $this->stub->add($banner);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBannerTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $banner = MockFactory::generateBannerObject(1);
        $bannerArray = array();

        $this->prepareBannerTranslator(
            $banner,
            array(
                'launchType',
                'type',
                'place',
                'image',
                'link',
                'remark',
                'tag'
            ),
            $bannerArray
        );

        $this->stub
            ->method('patch')
            ->with('banners/'.$banner->getId(), $bannerArray);

        $this->success($banner);
        $result = $this->stub->edit($banner);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBannerTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $banner = MockFactory::generateBannerObject(1);
        $bannerArray = array();

        $this->prepareBannerTranslator(
            $banner,
            array(
                'launchType',
                'type',
                'place',
                'image',
                'link',
                'remark',
                'tag'
            ),
            $bannerArray
        );

        $this->stub
            ->method('patch')
            ->with('banners/'.$banner->getId(), $bannerArray);
        
            $this->failure($banner);
            $result = $this->stub->edit($banner);
            $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBannerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行delete（）
     * 判断 result 是否为true
     */
    public function testDeleteSuccess()
    {
        $banner = MockFactory::generateBannerObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'banners/'.$banner->getId().'/delete'
            );

        $this->success($banner);

        $result = $this->stub->deletes($banner);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBannerTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行delete（）
     * 判断 result 是否为false
     */
    public function testDeleteFailure()
    {
        $banner = MockFactory::generateBannerObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'banners/'.$banner->getId().'/delete'
            );

        $this->failure($banner);
        $result = $this->stub->deletes($banner);
        $this->assertFalse($result);
    }
}
