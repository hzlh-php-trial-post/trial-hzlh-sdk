<?php
namespace Sdk\Banner\Utils;

use Sdk\Banner\Model\Banner;
use Sdk\Crew\Model\Crew;

class MockFactory
{
    /**
     * [generateBannerArray 生成横幅信息数组]
     * @return [array] [横幅数组]
     */
    public static function generateBannerArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $banner = array();

        $banner = array(
            'data'=>array(
                'type'=>'banners',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //launchType
        $launchType = self::generateLaunchType($faker, $value);
        $attributes['launchType'] = $launchType;
        //bannerType
        $bannerType = self::generateBannerType($faker, $value);
        $attributes['bannerType'] = $bannerType;
        //place
        $place = self::generatePlace($faker, $value);
        $attributes['place'] = $place;
        //image
        $image = self::generateImage($faker, $value);
        $attributes['image'] = $image;
        //link
        $link = self::generateLink($faker, $value);
        $attributes['link'] = $link;
        //remark
        $remark = self::generateRemark($faker, $value);
        $attributes['remark'] = $remark;
        //tag
        $tag = self::generateTag($faker, $value);
        $attributes['tag'] = $tag;
        
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $banner['data']['attributes'] = $attributes;

        //crew
        $banner['data']['relationships']['crew']['data'] = array(
            'type' => 'crews',
            'id' => $faker->randomNumber(1)
        );

        return $banner;
    }
    /**
     * [generateBannerObject 生成横幅对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [横幅对象]
     */
    public static function generateBannerObject(int $id = 0, int $seed = 0, array $value = array()) : Banner
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $banner = new Banner($id);

        //launchType
        $launchType = self::generateLaunchType($faker, $value);
        $banner->setLaunchType($launchType);
        //bannerType
        $bannerType = self::generateBannerType($faker, $value);
        $banner->setBannerType($bannerType);
        //place
        $place = self::generatePlace($faker, $value);
        $banner->setPlace($place);
        //image
        $image = self::generateImage($faker, $value);
        $banner->setImage($image);
        //link
        $link = self::generateLink($faker, $value);
        $banner->setLink($link);
        //remark
        $remark = self::generateRemark($faker, $value);
        $banner->setRemark($remark);
        //tag
        $tag = self::generateTag($faker, $value);
        $banner->setTag($tag);
        //crew
        $crew = self::generateCrew($faker, $value);
        $banner->setCrew($crew);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $banner->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $banner->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $banner->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $banner->setStatus($status);

        return $banner;
    }

    private static function generateLaunchType($faker, array $value = array())
    {
        return $launchType = isset($value['launchType']) ?
        $value['launchType'] : $faker->randomElement(Banner::LAUNCH_TYPE);
    }

    private static function generateBannerType($faker, array $value = array())
    {
        return $bannerType = isset($value['bannerType']) ?
        $value['bannerType'] : $faker->randomElement(Banner::TYPE);
    }

    private static function generatePlace($faker, array $value = array())
    {
        return $place = isset($value['place']) ?
        $value['place'] : 0;
    }

    private static function generateImage($faker, array $value = array())
    {
        return $image = isset($value['image']) ?
        $value['image'] : array('name' => 'image', 'identify' => 'image.jpg');
    }

    private static function generateLink($faker, array $value = array())
    {
        return $link = isset($value['link']) ?
        $value['link'] : 'https://example.com';
    }

    private static function generateRemark($faker, array $value = array())
    {
        return $remark = isset($value['remark']) ?
        $value['remark'] : 'string';
    }

    private static function generateTag($faker, array $value = array())
    {
        return $tag = isset($value['tag']) ?
        $value['tag'] : 'MA';
    }

    private static function generateCrew($faker, array $value = array())
    {
        return  $crew = isset($value['crew']) ?
        $value['crew'] : new Crew();
        // unset($faker);

        // return $crew = isset($value['crew']) ?
        //     $value['crew'] : \Sdk\Member\Utils\MockFactory::generateCrewObject(
        //         new Crew(),
        //         0,
        //         0
        //     );
    }
}
