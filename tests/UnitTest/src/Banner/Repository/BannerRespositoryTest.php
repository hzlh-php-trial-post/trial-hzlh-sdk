<?php
namespace Sdk\Banner\Repository;

use PHPUnit\Framework\TestCase;
use Sdk\Banner\Adapter\Banner\BannerRestfulAdapter;

use Prophecy\Argument;

class BannerRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(BannerRepository::class)
        ->setMethods(['getAdapter'])
        ->getMock();

        $this->childStub = new class extends BannerRepository{
            public function getAdapter() : BannerRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Banner\Adapter\Banner\BannerRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }

    /**
     * 为BannerRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以BannerRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(BannerRestfulAdapter::class);
        $adapter->scenario(Argument::exact(BannerRepository::LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(BannerRepository::LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * delete(),tagSearch()未添加测试
     */
}
