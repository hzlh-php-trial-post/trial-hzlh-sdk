<?php
namespace Sdk\Banner\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;
use Sdk\Banner\Repository\BannerRepository;
use Sdk\Crew\Model\Crew;

class BannerTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Banner::class)
            ->setMethods([
                'getRepository'
            ])
            ->getMock();

        $this->childStub = new Class extends Banner{
            public function getRepository() : BannerRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testImplementsIOperatAble()
    {
        $this->assertInstanceOf('Sdk\Common\Model\IOperatAble', $this->stub);
    }

    public function testImplementsIOnShelfAble()
    {
        $this->assertInstanceOf('Sdk\Common\Model\IOnShelfAble', $this->stub);
    }

     //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Banner setId() 正确的传参类型,期望传值正确
     */
    public function testSetBannerIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Banner setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetBannerIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 -------------------------------------------------------------   end

    //launchType 测试 ----------------------------------------------------- start
    /**
     * 循环测试 setLaunchType() 是否符合预定范围
     *
     * @dataProvider launchTypeProvider
     */
    public function testSetLaunchTypeCorrectType($actual, $expected)
    {
        $this->stub->setLaunchType($actual);
        $this->assertEquals($expected, $this->stub->getLaunchType());
    }
    /**
     * 循环测试 DispatchDepartment setLaunchType() 数据构建器
     */
    public function launchTypeProvider()
    {
        return array(
            array(Banner::LAUNCH_TYPE['NULL'], Banner::LAUNCH_TYPE['NULL']),
            array(Banner::LAUNCH_TYPE['PC'], Banner::LAUNCH_TYPE['PC']),
            array(Banner::LAUNCH_TYPE['APP'],Banner::LAUNCH_TYPE['APP'])
        );
    }
    /**
     * 设置 Banner setLaunchType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLaunchTypeWrongType()
    {
        $this->stub->setLaunchType('string');
    }
    //launchType 测试 ------------------------------------------------------   end

    //bannerType 测试 ----------------------------------------------------- start
    /**
     *循环测试 setBannerType() 是否符合预定范围
     *
     * @dataProvider bannerTypeProvider
     */
    public function testSetBannerTypeCorrectType($actual, $expected)
    {
        $this->stub->setBannerType($actual);
        $this->assertEquals($expected, $this->stub->getBannerType());
    }
    /**
     * 循环测试 DispatchDepartment setBannerType() 数据构建器
     */
    public function bannerTypeProvider()
    {
        return array(
            array(Banner::TYPE['NULL'], Banner::TYPE['NULL']),
            array(Banner::TYPE['ADVERTISEMENT'], Banner::TYPE['ADVERTISEMENT']),
            array(Banner::TYPE['BANNER'],Banner::TYPE['BANNER'])
        );
    }
    /**
     * 设置 Banner setBannerType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBannerTypeWrongType()
    {
        $this->stub->setBannerType('string');
    }
    //bannerType 测试 ------------------------------------------------------   end

    //place 测试 ----------------------------------------------------------- start
    /**
     * 设置 Banner setPlace() 正确的传参类型,期望传值正确
     */
    public function testSetPlaceCorrectType()
    {
        $this->stub->setPlace(1);
        $this->assertEquals(1, $this->stub->getPlace());
    }

    /**
     * 设置 Banner setPlace() 错误的传参类型,但是传参是数值,期望返回类型正确,值正确.
     *
     */
    public function testSetPlaceWrongTypeButNumeric()
    {
        $this->stub->setPlace('1');
        $this->assertEquals(1, $this->stub->getPlace());
    }
    //place 测试 -----------------------------------------------------------   end

    //placeDetail 测试 ---------------------------------------------------------- start
    /**
     * 设置 Banner setPlaceDetail() 正确的传参类型,期望传值正确
     */
    public function testSetPlaceDetailCorrectType()
    {
        $this->stub->setPlaceDetail(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getPlaceDetail());
    }

    /**
     * 设置 Banner setPlaceDetail() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPlaceDetailWrongType()
    {
        $this->stub->setPlaceDetail('string');
    }
    //placeDetail 测试 ----------------------------------------------------------   end

    //iamge 测试 ---------------------------------------------------------- start
    /**
     * 设置 Banner setImage() 正确的传参类型,期望传值正确
     */
    public function testSetImageCorrectType()
    {
        $this->stub->setImage(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getImage());
    }

    /**
     * 设置 Banner setImage() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetImageWrongType()
    {
        $this->stub->setImage('string');
    }
    //iamge 测试 ----------------------------------------------------------   end

    //link 测试 ---------------------------------------------------------- start
    /**
     * 设置 Banner setLink() 正确的传参类型,期望传值正确
     */
    public function testSetLinkCorrectType()
    {
        $this->stub->setLink('string');
        $this->assertEquals('string', $this->stub->getLink());
    }

    /**
     * 设置 Banner setLink() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLinkWrongType()
    {
        $this->stub->setLink(array(1,2,3));
    }
    //link 测试 ----------------------------------------------------------   end

    //remark 测试 ---------------------------------------------------------- start
    /**
     * 设置 Banner setRemark() 正确的传参类型,期望传值正确
     */
    public function testSetRemarkCorrectType()
    {
        $this->stub->setRemark('string');
        $this->assertEquals('string', $this->stub->getRemark());
    }

    /**
     * 设置 Banner setRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRemarkWrongType()
    {
        $this->stub->setRemark(array(1,2,3));
    }
    //remark 测试 ----------------------------------------------------------   end

    //tag 测试 ---------------------------------------------------------- start
    /**
     * 设置 Banner setTag() 正确的传参类型,期望传值正确
     */
    public function testSetTagCorrectType()
    {
        $this->stub->setTag('string');
        $this->assertEquals('string', $this->stub->getTag());
    }

    /**
     * 设置 Banner setTag() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTagWrongType()
    {
        $this->stub->setTag(array(1,2,3));
    }
    //tag 测试 ----------------------------------------------------------   end

    //crew 测试 -------------------------------------------------------- start
    /**
     * 设置 Banner setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->stub->setCrew($object);
        $this->assertSame($object, $this->stub->getCrew());
    }

    /**
     * 设置 Banner setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewType()
    {
        $this->stub->setCrew(array(1,2,3));
    }
    //crew 测试 -------------------------------------------------------- end

    //status 测试 ------------------------------------------------------- start
    /**
     * 循环测试 setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatusCorrectType($actual, $expected)
    {
        $this->stub->setStatus($actual);
        $this->assertEquals($expected, $this->stub->getStatus());
    }
    /**
     * 循环测试 DispatchDepartment setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Banner::BANNER_STATUS['ON_SHELF'], Banner::BANNER_STATUS['ON_SHELF']),
            array(Banner::BANNER_STATUS['OFF_STOCK'], Banner::BANNER_STATUS['OFF_STOCK']),
            array(Banner::BANNER_STATUS['DELETE'],Banner::BANNER_STATUS['DELETE'])
        );
    }
    /**
     * 设置 Banner setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('string');
    }
    //status 测试 ----------------------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Banner\Repository\BannerRepository',
            $this->childStub->getRepository()
        );
    }

    public function testIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getRepository()
        );
    }

    public function testgetIOnShelfAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOnShelfAbleAdapter',
            $this->childStub->getRepository()
        );
    }
    /**
     * 还有add(),edit(),isMaxNumber(),delete() 未添加单元测试
     */
}
