<?php
namespace Sdk\Banner\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullBannerTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = NullBanner::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsBanner()
    {
        $this->assertInstanceOf('Sdk\Banner\Model\Banner', $this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf('Marmot\Interfaces\INull', $this->stub);
    }
}
