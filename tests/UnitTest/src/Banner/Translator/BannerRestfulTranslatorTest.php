<?php
namespace Sdk\Banner\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Banner\Model\Banner;
use Sdk\Banner\Translator\BannerRestfulTranslator;

use Sdk\Crew\Model\Crew;

class BannerRestfulTranslatorTest extends TestCase
{
    private $stub;

    private $childStub;


    public function setUP()
    {
        $this->stub = $this->getMockBuilder(BannerRestfulTranslator::class)
        ->setMethods([
            'getCrewRestfulTranslator'
        ])
        ->getMock();

        $this->childStub = new class extends BannerRestfulTranslator {
            public function getCrewRestfulTranslator()
            {
                return parent::getCrewRestfulTranslator();
            }
        };
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Banner());
        $this->assertInstanceOf('Sdk\Banner\Model\NullBanner', $result);
    }

    public function setMethods(
        Banner $expectObject,
        array  $attributes,
        array  $relationships
    ) {
        if (isset($attributes['launchType'])) {
            $expectObject->setLaunchType($attributes['launchType']);
        }
        if (isset($attributes['bannerType'])) {
            $expectObject->setBannerType($attributes['bannerType']);
        }
        if (isset($attributes['place'])) {
            $expectObject->setPlace($attributes['place']);
        }
        if (isset($attributes['placeDetail'])) {
            $expectObject->setPlaceDetail($attributes['placeDetail']);
        }
        if (isset($attributes['image'])) {
            $expectObject->setImage($attributes['image']);
        }
        if (isset($attributes['link'])) {
            $expectObject->setLink($attributes['link']);
        }
        if (isset($attributes['remark'])) {
            $expectObject->setRemark($attributes['remark']);
        }
        if (isset($attributes['tag'])) {
            $expectObject->setTag($attributes['tag']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['crew']['data'])) {
            $expectObject->setCrew(new Crew($relationships['crew']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $banner = \Sdk\Banner\Utils\MockFactory::generateBannerArray();

        $data =  $banner['data'];
        $relationships = $data['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(BannerRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);

        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($banner);

        $expectObject = new Banner();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);
        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $banner = \Sdk\Banner\Utils\MockFactory::generateBannerArray();
        $data =  $banner['data'];
        $relationships = $data['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(BannerRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);

        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($banner);

        $expectArray = array();

        $expectObject = new Banner();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $banner = \Sdk\Banner\Utils\MockFactory::generateBannerObject(1, 1);

        $actual = $this->stub->objectToArray($banner, array(
            'id',
            'launchType',
            'type',
            'place',
            'image',
            'link',
            'remark',
            'tag',
            'crew'
            ));

        $expectedArray = array(
            'data'=>array(
                'type'=>'banners',
                'id'=>$banner->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'launchType'=>$banner->getLaunchType(),
            'type'=>$banner->getBannerType(),
            'place'=>$banner->getPlace(),
            'image'=>$banner->getImage(),
            'link'=>$banner->getLink(),
            'remark'=>$banner->getRemark(),
            'tag'=>$banner->getTag(),
        );

        $expectedArray['data']['relationships']['crew']['data'] = array(
            array(
                'type' => 'crews',
                'id' => $banner->getCrew()->getId()
            )
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
