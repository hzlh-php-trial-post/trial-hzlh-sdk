<?php
namespace Sdk\MerchantCoupon\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullMerchantCouponTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullMerchantCoupon::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsMerchantCoupon()
    {
        $this->assertInstanceof('Sdk\MerchantCoupon\Model\MerchantCoupon', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

//    public function testDiscontinue()
//    {
//        $result = $this->stub->discontinue();
//        $this->assertFalse($result);
//        $this->assertEquals(RESOURCE_STATUS_NOT_NORMAL, Core::getLastError()->getId());
//    }

    public function testDeletes()
    {
        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }
}
