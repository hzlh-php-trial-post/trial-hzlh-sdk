<?php
namespace Sdk\MerchantCoupon\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\MerchantCoupon\Repository\MerchantCouponRepository;

class MerchantCouponTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MerchantCoupon::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends MerchantCoupon{
            public function getRepository() : MerchantCouponRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\MerchantCoupon\Repository\MerchantCouponRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 MerchantCoupon setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //releaseType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setReleaseType() 是否符合预定范围
     * @dataProvider releaseTypeProvider
     */
    public function testSetReleaseType($actual, $expected)
    {
        $this->stub->setReleaseType($actual);
        $this->assertEquals($expected, $this->stub->getReleaseType());
    }
    /**
     * 循环测试  setReleaseType() 数据构建器
     */
    public function releaseTypeProvider()
    {
        return array(
            array(MerchantCoupon::RELEASE_TYPE['PLATFORM'], MerchantCoupon::RELEASE_TYPE['PLATFORM']),
            array(MerchantCoupon::RELEASE_TYPE['MERCHANT'], MerchantCoupon::RELEASE_TYPE['MERCHANT'])
        );
    }
    /**
     * 设置 MerchantCoupon setReleaseType() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetReleaseTypeWrongType()
    {
        $this->stub->setReleaseType('string');
    }
    //releaseType 测试 ------------------------------------------------------   end

    //name 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置 MerchantCoupon setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array(3));
    }
    //name 测试 ----------------------------------------------------------   end

    //couponType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setTypeMode() 是否符合预定范围
     * @dataProvider couponTypeProvider
     */
    public function testSetCouponType($actual, $expected)
    {
        $this->stub->setCouponType($actual);
        $this->assertEquals($expected, $this->stub->getCouponType());
    }
    /**
     * 循环测试 DispatchDepartment setReceivingMode() 数据构建器
     */
    public function couponTypeProvider()
    {
        return array(
            array(MerchantCoupon::COUPON_TYPE['FULL_REDUCTION'], MerchantCoupon::COUPON_TYPE['FULL_REDUCTION']),
            array(MerchantCoupon::COUPON_TYPE['DISCOUNT'], MerchantCoupon::COUPON_TYPE['DISCOUNT'])
        );
    }
    /**
     * 设置 MerchantCoupon setCardType() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetCouponTypeWrongType()
    {
        $this->stub->setCouponType('string');
    }
    //couponType 测试 ------------------------------------------------------   end

    //denomination 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setDenomination() 正确的传参类型,期望传值正确
     */
    public function testSetVolumeCorrectType()
    {
        $this->stub->setDenomination(1);
        $this->assertEquals(1, $this->stub->getDenomination());
    }

    /**
     * 设置 MerchantCoupon setDenomination() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetDenominationWrongTypeButNumeric()
    {
        $this->stub->setDenomination('1');
        $this->assertEquals(1, $this->stub->getDenomination());
    }
    //denomination 测试 ----------------------------------------------------------   end

    //discount 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setDiscount() 正确的传参类型,期望传值正确
     */
    public function testSetDiscountType()
    {
        $this->stub->setDiscount(1);
        $this->assertEquals(1, $this->stub->getDiscount());
    }

    /**
     * 设置 MerchantCoupon setDiscount() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetDiscountWrongTypeButNumeric()
    {
        $this->stub->setDiscount('1');
        $this->assertEquals(1, $this->stub->getDiscount());
    }
    //discount 测试 ----------------------------------------------------------   end

    //useStandard 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setUseStandard() 正确的传参类型,期望传值正确
     */
    public function testSetUseStandardType()
    {
        $this->stub->setUseStandard(1);
        $this->assertEquals(1, $this->stub->getUseStandard());
    }

    /**
     * 设置 MerchantCoupon setUseStandard() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetUseStandardWrongTypeButNumeric()
    {
        $this->stub->setUseStandard('1');
        $this->assertEquals(1, $this->stub->getUseStandard());
    }
    //useStandard 测试 ----------------------------------------------------------   end

    //validityStartTime 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setValidityStartTime() 正确的传参类型,期望传值正确
     */
    public function testSetValidityStartTimeCorrectType()
    {
        $this->stub->setValidityStartTime('string');
        $this->assertEquals('string', $this->stub->getValidityStartTime());
    }

    /**
     * 设置 MerchantCoupon setValidityStartTime() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetValidityStartTimeWrongType()
    {
        $this->stub->setValidityStartTime(array(1, 2, 3));
    }
    //validityStartTime 测试 ----------------------------------------------------------   end
    
    //validityEndTime 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setValidityEndTime() 正确的传参类型,期望传值正确
     */
    public function testSetValidityEndTimeCorrectType()
    {
        $this->stub->setValidityEndTime('string');
        $this->assertEquals('string', $this->stub->getValidityEndTime());
    }

    /**
     * 设置 MerchantCoupon setValidityEndTime() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetValidityEndTimeWrongType()
    {
        $this->stub->setValidityEndTime(array(1, 2, 3));
    }
    //validityEndTime 测试 ----------------------------------------------------------   end

    //issueTotal 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setIssueTotal() 正确的传参类型,期望传值正确
     */
    public function testSetIssueTotalType()
    {
        $this->stub->setIssueTotal(1);
        $this->assertEquals(1, $this->stub->getIssueTotal());
    }

    /**
     * 设置 MerchantCoupon setIssueTotal() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIssueTotalWrongTypeButNumeric()
    {
        $this->stub->setIssueTotal('1');
        $this->assertEquals(1, $this->stub->getIssueTotal());
    }
    //issueTotal 测试 ----------------------------------------------------------   end

    //distributeTotal 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setDistributeTotal() 正确的传参类型,期望传值正确
     */
    public function testSetDistributeTotalType()
    {
        $this->stub->setDistributeTotal(1);
        $this->assertEquals(1, $this->stub->getDistributeTotal());
    }

    /**
     * 设置 MerchantCoupon setDistributeTotal() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetdistributeTotalWrongTypeButNumeric()
    {
        $this->stub->setDistributeTotal('1');
        $this->assertEquals(1, $this->stub->getDistributeTotal());
    }
    //distributeTotal 测试 ----------------------------------------------------------   end

    //perQuota 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setPerQuota() 正确的传参类型,期望传值正确
     */
    public function testSetPerQuotaType()
    {
        $this->stub->setPerQuota(1);
        $this->assertEquals(1, $this->stub->getPerQuota());
    }

    /**
     * 设置 MerchantCoupon setPerQuota() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetPerQuotaWrongTypeButNumeric()
    {
        $this->stub->setPerQuota('1');
        $this->assertEquals(1, $this->stub->getPerQuota());
    }
    //perQuota 测试 ----------------------------------------------------------   end

    //receivingMode 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setReceivingMode() 是否符合预定范围
     * @dataProvider receivingModeProvider
     */
    public function testSetReceivingMode($actual, $expected)
    {
        $this->stub->setReceivingMode($actual);
        $this->assertEquals($expected, $this->stub->getReceivingMode());
    }
    /**
     * 循环测试 DispatchDepartment setReceivingMode() 数据构建器
     */
    public function receivingModeProvider()
    {
        return array(
            array(MerchantCoupon::RECEIVING_MODE['MANUAL'],MerchantCoupon::RECEIVING_MODE['MANUAL']),
            array(MerchantCoupon::RECEIVING_MODE['AUTOMATIC'],MerchantCoupon::RECEIVING_MODE['AUTOMATIC']),
        );
    }
    /**
     * 设置 MerchantCoupon setReceivingMode() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetReceivingModeWrongType()
    {
        $this->stub->setReceivingMode('string');
    }
    //receivingMode 测试 ------------------------------------------------------   end

    //receivingUsers 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setReceivingUsers() 是否符合预定范围
     * @dataProvider receivingUsersProvider
     */
    public function testSetReceivingUsers($actual, $expected)
    {
        $this->stub->setReceivingUsers($actual);
        $this->assertEquals($expected, $this->stub->getReceivingUsers());
    }
    /**
     * 循环测试 DispatchDepartment setReceivingUsers() 数据构建器
     */
    public function receivingUsersProvider()
    {
        return array(
            array(MerchantCoupon::RECEIVING_USERS['ALL'],MerchantCoupon::RECEIVING_USERS['ALL']),
            array(MerchantCoupon::RECEIVING_USERS['NEW'],MerchantCoupon::RECEIVING_USERS['NEW']),
        );
    }
    /**
     * 设置 MerchantCoupon setReceivingUsers() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetReceivingUsersWrongType()
    {
        $this->stub->setReceivingUsers('string');
    }
    //receivingUsers 测试 ------------------------------------------------------   end
    
    //applySituation 测试 ------------------------------------------------------- start
     /**
      * 设置 MerchantCoupon setApplySituation() 正确的传参类型,期望传值正确
      */
    public function testSetLicenceCorrectType()
    {
        $this->stub->setApplySituation(array());
        $this->assertEquals(array(), $this->stub->getApplySituation());
    }

    /**
     * 设置 MerchantCoupon setApplySituation() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetApplySituationWrongType()
    {
        $this->stub->setApplySituation('licence');
    }
    //applySituation 测试 -------------------------------------------------------   end

    //number 测试 ---------------------------------------------------------- start
    /**
     * 设置 MerchantCoupon setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('string');
        $this->assertEquals('string', $this->stub->getNumber());
    }

    /**
     * 设置 MerchantCoupon setNumber() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array(3));
    }
    //number 测试 ----------------------------------------------------------   end
}
