<?php
namespace Sdk\PolicyProduct\Adapter\PolicyProduct;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\PolicyProduct\Model\PolicyProduct;
use Sdk\PolicyProduct\Model\NullPolicyProduct;
use Sdk\PolicyProduct\Utils\MockFactory;
use Sdk\PolicyProduct\Translator\PolicyProductRestfulTranslator;

class PolicyProductRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PolicyProductRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends PolicyProductRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIPolicyProductAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyProduct\Adapter\PolicyProduct\IPolicyProductAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('policyProducts', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyProduct\Translator\PolicyProductRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_POLICY_PRODUCT_LIST',
                PolicyProductRestfulAdapter::SCENARIOS['OA_POLICY_PRODUCT_LIST']
            ],
            [
                'PORTAL_POLICY_PRODUCT_LIST',
                PolicyProductRestfulAdapter::SCENARIOS['PORTAL_POLICY_PRODUCT_LIST']
            ],
            [
                'POLICY_PRODUCT_FETCH_ONE',
                PolicyProductRestfulAdapter::SCENARIOS['POLICY_PRODUCT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $policyProduct = MockFactory::generatePolicyProductObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullPolicyProduct())
            ->willReturn($policyProduct);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($policyProduct, $result);
    }
}
