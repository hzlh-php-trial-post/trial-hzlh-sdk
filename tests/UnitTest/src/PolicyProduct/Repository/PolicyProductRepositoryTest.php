<?php
namespace Sdk\PolicyProduct\Repository;

use Sdk\PolicyProduct\Adapter\PolicyProduct\PolicyProductRestfulAdapter;
use Sdk\PolicyProduct\Adapter\PolicyProduct\PolicyProductMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class PolicyProductRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PolicyProductRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends PolicyProductRepository {
            public function getAdapter() : PolicyProductRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : PolicyProductMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyProduct\Adapter\PolicyProduct\PolicyProductRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyProduct\Adapter\PolicyProduct\PolicyProductMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为PolicyProductRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以PolicyProductRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(PolicyProductRestfulAdapter::class);
        $adapter->scenario(Argument::exact(PolicyProductRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(PolicyProductRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
