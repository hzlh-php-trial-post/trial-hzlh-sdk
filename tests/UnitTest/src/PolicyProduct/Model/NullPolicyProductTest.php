<?php
namespace Sdk\PolicyProduct\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullPolicyProductTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullPolicyProduct::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsPolicyProduct()
    {
        $this->assertInstanceof('Sdk\PolicyProduct\Model\PolicyProduct', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
