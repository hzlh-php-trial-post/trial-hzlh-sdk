<?php
namespace Sdk\PolicyProduct\Model;

use Sdk\PolicyProduct\Repository\PolicyProductRepository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class PolicyProductTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PolicyProduct::class)
            ->setMethods()->getMock();

        $this->childStub = new Class extends PolicyProduct{
            public function getRepository() : PolicyProductRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\PolicyProduct\Repository\PolicyProductRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //product 测试 ---------------------------------------------------------- start
    /**
     * 设置setProduct() 正确的传参类型,期望传值正确
     */
    public function testSetProductCorrectType()
    {
        $object = new \Sdk\Service\Model\NullService;

        $this->stub->setProduct($object);
        $this->assertEquals($object, $this->stub->getProduct());
    }

    /**
     * 设置setProduct() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetProductWrongType()
    {
        $this->stub->setProduct(array(1, 2, 3));
    }
    //product 测试 ----------------------------------------------------------   end

    //title 测试 ---------------------------------------------------------- start
    /**
     * 设置setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->stub->setTitle('string');
        $this->assertEquals('string', $this->stub->getTitle());
    }

    /**
     * 设置setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->stub->setTitle(array(1, 2, 3));
    }
    //title 测试 ----------------------------------------------------------   end

    //cover 测试 ---------------------------------------------------------- start
    /**
     * 设置setCover() 正确的传参类型,期望传值正确
     */
    public function testSetCoverCorrectType()
    {
        $this->stub->setCover(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getCover());
    }

    /**
     * 设置setCover() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCoverWrongTypeButNumeric()
    {
        $this->stub->setCover('string');
    }
    //cover 测试 ----------------------------------------------------------   end

    //attribute 测试 ---------------------------------------------------------- start
    /**
     * 设置setAttribute() 正确的传参类型,期望传值正确
     */
    public function testSetAttributeCorrectType()
    {
        $this->stub->setAttribute('string');
        $this->assertEquals('string', $this->stub->getAttribute());
    }
    //attribute 测试 ----------------------------------------------------------   end
    
    //productPrice 测试 ---------------------------------------------------------- start
    /**
     * 设置setProductPrice() 正确的传参类型,期望传值正确
     */
    public function testSetProductPriceCorrectType()
    {
        $this->stub->setProductPrice(1.01);
        $this->assertEquals(1.01, $this->stub->getProductPrice());
    }

    /**
     * 设置setProductPrice() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetProductPriceWrongTypeButNumeric()
    {
        $this->stub->setProductPrice('string');
    }
    //productPrice 测试 ----------------------------------------------------------   end

    //enterprise 测试 -------------------------------------------------------- start
    /**
     * 设置 setEnterprise() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseCorrectType()
    {
        $object = new \Sdk\Enterprise\Model\Enterprise();

        $this->stub->setEnterprise($object);
        $this->assertSame($object, $this->stub->getEnterprise());
    }

    /**
     * 设置 setEnterprise() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseType()
    {
        $this->stub->setEnterprise(array(1,2,3));
    }
    //enterprise 测试 -------------------------------------------------------- end
}
