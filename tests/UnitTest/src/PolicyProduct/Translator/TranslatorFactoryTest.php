<?php
namespace Sdk\PolicyProduct\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Service\Translator\ServiceRestfulTranslator;
use Sdk\LoanProduct\Translator\LoanProductRestfulTranslator;

use Sdk\PolicyProduct\Model\PolicyProduct;

class TranslatorFactoryTest extends TestCase
{
    private $stub;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        $this->stub = new TranslatorFactory();
    }

    public function testServiceRestfulTranslator()
    {
        $translator = $this->stub->getTranslator(PolicyProduct::CATEGORY['SERVICE']);

        $this->assertInstanceOf(
            'Sdk\Service\Translator\ServiceRestfulTranslator',
            $translator
        );
    }

    public function testLoanProductRestfulTranslator()
    {
        $translator = $this->stub->getTranslator(PolicyProduct::CATEGORY['FINANCE']);

        $this->assertInstanceOf(
            'Sdk\LoanProduct\Translator\LoanProductRestfulTranslator',
            $translator
        );
    }
}
