<?php
namespace Sdk\PolicyProduct\Translator;

use Sdk\PolicyProduct\Model\NullPolicyProduct;
use Sdk\PolicyProduct\Model\PolicyProduct;
use Sdk\PolicyProduct\Model\Answer;
use Sdk\PolicyProduct\Model\DeleteInfo;

use Sdk\Enterprise\Model\Enterprise;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\PolicyProduct\Utils\MockFactory;

class PolicyProductRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            PolicyProductRestfulTranslator::class
        )
            ->setMethods([
                'getEnterpriseRestfulTranslator'
            ])->getMock();

        $this->childStub =
        new class extends PolicyProductRestfulTranslator {
            public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
            {
                return parent::getEnterpriseRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->childStub->getEnterpriseRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new PolicyProduct());
        $this->assertInstanceOf('Sdk\PolicyProduct\Model\NullPolicyProduct', $result);
    }

    public function setMethods(PolicyProduct $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['title'])) {
            $expectObject->setTitle($attributes['title']);
        }
        if (isset($attributes['attribute'])) {
            $expectObject->setAttribute($attributes['attribute']);
        }
        if (isset($attributes['cover'])) {
            $expectObject->setCover($attributes['cover']);
        }
        if (isset($attributes['productPrice'])) {
            $expectObject->setProductPrice($attributes['productPrice']);
        }
        if (isset($attributes['category'])) {
            $expectObject->setCategory($attributes['category']);
        }
        if (isset($relationships['enterprise']['data'])) {
            $expectObject->setEnterprise(new Enterprise($relationships['enterprise']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $policyProduct = MockFactory::generatePolicyProductArray();

        $data =  $policyProduct['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($policyProduct);

        $expectObject = new PolicyProduct();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $policyProduct = MockFactory::generatePolicyProductArray();
        $data =  $policyProduct['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($policyProduct);
        $expectArray = array();

        $expectObject = new PolicyProduct();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $policyProduct = MockFactory::generatePolicyProductObject(1, 1);

        $actual = $this->stub->objectToArray(
            $policyProduct,
            array(
                'category'
            )
        );
        
        $expectedArray = array(
            'type'=>'policyProducts'
        );

        $expectedArray['attributes'] = array(
            'category'=>$policyProduct->getCategory(),
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
