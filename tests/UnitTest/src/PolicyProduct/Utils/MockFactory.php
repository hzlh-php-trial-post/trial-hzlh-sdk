<?php
namespace Sdk\PolicyProduct\Utils;

use Sdk\PolicyProduct\Model\PolicyProduct;
use Sdk\Enterprise\Model\Enterprise;

class MockFactory
{
    /**
     * [generatePolicyArray 生成政策信息数组]
     * @return [array] [政策数组]
     */
    public static function generatePolicyProductArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $policyProduct = array();

        $policyProduct = array(
            'data'=>array(
                'type'=>'policyProducts',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //title
        $title = self::generateTitle($faker, $value);
        $attributes['title'] = $title;
        //attribute
        $attribute = self::generateAttribute($faker, $value);
        $attributes['attribute'] = $attribute;
        //cover
        $cover = self::generateCover($faker, $value);
        $attributes['cover'] = $cover;
        //productPrice
        $productPrice = self::generateProductPrice($faker, $value);
        $attributes['productPrice'] = $productPrice;
        //category
        $category = self::generateCategory($faker, $value);
        $attributes['category'] = $category;

        $policyProduct['data']['attributes'] = $attributes;

        //enterprise
        $policyProduct['data']['relationships']['enterprise']['data'] = array(
            'type' => 'enterprises',
            'id' => $faker->randomNumber(1)
        );

        return $policyProduct;
    }
    /**
     * [generatePolicyObject 生成政策对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [政策对象]
     */
    public static function generatePolicyProductObject(int $id = 0, int $seed = 0, array $value = array()) : PolicyProduct //phpcs:ignore
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $policyProduct = new PolicyProduct($id);

        //title
        $title = self::generateTitle($faker, $value);
        $policyProduct->setTitle($title);
        //attribute
        $attribute = self::generateAttribute($faker, $value);
        $policyProduct->setAttribute($attribute);
        //cover
        $cover = self::generateCover($faker, $value);
        $policyProduct->setCover($cover);
        //productPrice
        $productPrice = self::generateProductPrice($faker, $value);
        $policyProduct->setProductPrice($productPrice);
        //category
        $category = self::generateCategory($faker, $value);
        $policyProduct->setCategory($category);
        //enterprise
        $enterprise = self::generateEnterprise($faker, $value);
        $policyProduct->setEnterprise($enterprise);

        return $policyProduct;
    }

    private static function generateTitle($faker, array $value = array())
    {
        return $title = isset($value['title']) ?
            $value['title'] : $faker->title;
    }
    private static function generateAttribute($faker, array $value = array())
    {
        return $attribute = isset($value['title']) ?
            $value['attribute'] : '$faker->word';
    }
    private static function generateCover($faker, array $value = array())
    {
        return $cover = isset($value['cover']) ?
            $value['cover'] : array('name' => 'cover', 'identify' => 'cover.jpg');
    }
    private static function generateProductPrice($faker, array $value = array())
    {
        return $productPrice = isset($value['productPrice']) ?
            $value['productPrice'] : $faker->randomFloat();
    }
    private static function generateCategory($faker, array $value = array())
    {
        return $category = isset($value['category']) ?
            $value['category'] : $faker->randomElement(
                $array = array(
                PolicyProduct::CATEGORY['SERVICE'],
                PolicyProduct::CATEGORY['FINANCE'],
                )
            );
    }
    private static function generateEnterprise($faker, array $value = array())
    {
        return $crew = isset($value['crew']) ?
            $value['crew'] : \Sdk\Enterprise\Utils\EnterpriseMockFactory::generateEnterpriseObject(
                new Enterprise()
            );
    }
}
