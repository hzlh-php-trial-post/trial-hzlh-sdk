<?php
namespace Sdk\Staff\Adapter\Staff;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Staff\Model\Staff;
use Sdk\Staff\Model\NullStaff;
use Sdk\Staff\Utils\MockFactory;
use Sdk\Staff\Translator\StaffRestfulTranslator;

class StaffRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(StaffRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends StaffRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIStaffAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Staff\Adapter\Staff\IStaffAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('staffs', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Staff\Translator\StaffRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'STAFF_LIST',
                StaffRestfulAdapter::SCENARIOS['STAFF_LIST']
            ],
            [
                'STAFF_FETCH_ONE',
                StaffRestfulAdapter::SCENARIOS['STAFF_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $staff = MockFactory::generateStaffObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullStaff())
            ->willReturn($staff);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($staff, $result);
    }
    /**
     * 为StaffRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$staff$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareStaffTranslator(
        Staff $staff,
        array $keys,
        array $staffArray
    ) {
        $translator = $this->prophesize(StaffRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($staff),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($staffArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(Staff $staff)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($staff);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $staff = MockFactory::generateStaffObject(1);
        $staffArray = array();

        $this->prepareStaffTranslator(
            $staff,
            array(
                'avatar',
                'realName',
                'cellphone',
                'cardId',
                'birthday',
                'briefIntroduction',
                'gender',
                'status',
                'education',
                'enterprise'
            ),
            $staffArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('staffs', $staffArray);

        $this->success($staff);

        $result = $this->stub->add($staff);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $staff = MockFactory::generateStaffObject(1);
        $staffArray = array();

        $this->prepareStaffTranslator(
            $staff,
            array(
                'avatar',
                'realName',
                'cellphone',
                'cardId',
                'birthday',
                'briefIntroduction',
                'gender',
                'status',
                'education',
                'enterprise'
            ),
            $staffArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('staffs', $staffArray);

        $this->failure($staff);
        $result = $this->stub->add($staff);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $staff = MockFactory::generateStaffObject(1);
        $staffArray = array();

        $this->prepareStaffTranslator(
            $staff,
            array(
                'avatar',
                'realName',
                'cellphone',
                'cardId',
                'birthday',
                'briefIntroduction',
                'gender',
                'status',
                'education'
            ),
            $staffArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('staffs/'.$staff->getId(), $staffArray);

        $this->success($staff);

        $result = $this->stub->edit($staff);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $staff = MockFactory::generateStaffObject(1);
        $staffArray = array();

        $this->prepareStaffTranslator(
            $staff,
            array(
                'avatar',
                'realName',
                'cellphone',
                'cardId',
                'birthday',
                'briefIntroduction',
                'gender',
                'status',
                'education'
            ),
            $staffArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('staffs/'.$staff->getId(), $staffArray);

        $this->failure($staff);
        $result = $this->stub->edit($staff);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行recommendHomePage（）
     * 判断 result 是否为true
     */
    public function testRecommendHomePageSuccess()
    {
        $staff = MockFactory::generateStaffObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('staffs/'.$staff->getId().'/recommendHomePage');

        $this->success($staff);

        $result = $this->stub->recommendHomePage($staff);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行recommendHomePage（）
     * 判断 result 是否为false
     */
    public function testRecommendHomePageFailure()
    {
        $staff = MockFactory::generateStaffObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('staffs/'.$staff->getId().'/recommendHomePage');

        $this->failure($staff);
        $result = $this->stub->recommendHomePage($staff);
        $this->assertFalse($result);
    }

        /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行cancelRecommendHomePage（）
     * 判断 result 是否为true
     */
    public function testCancelRecommendHomePageSuccess()
    {
        $staff = MockFactory::generateStaffObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('staffs/'.$staff->getId().'/cancelRecommendHomePage');

        $this->success($staff);

        $result = $this->stub->cancelRecommendHomePage($staff);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行cancelRecommendHomePage（）
     * 判断 result 是否为false
     */
    public function testCancelRecommendHomePageFailure()
    {
        $staff = MockFactory::generateStaffObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('staffs/'.$staff->getId().'/cancelRecommendHomePage');

        $this->failure($staff);
        $result = $this->stub->cancelRecommendHomePage($staff);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $staff = MockFactory::generateStaffObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'staffs/'.$staff->getId().'/delete'
            );

        $this->success($staff);

        $result = $this->stub->deletes($staff);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareStaffTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $staff = MockFactory::generateStaffObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'staffs/'.$staff->getId().'/delete'
            );

        $this->failure($staff);
        $result = $this->stub->deletes($staff);
        $this->assertFalse($result);
    }
}
