<?php
namespace Sdk\Staff\Translator;

use Sdk\Staff\Model\NullStaff;
use Sdk\Staff\Model\Staff;

use Sdk\Enterprise\Model\Enterprise;

use Sdk\User\Translator\UserRestfulTranslatorTest;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\Staff\Utils\MockFactory;

class StaffRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            StaffRestfulTranslator::class
        )
            ->setMethods([
                'getEnterpriseRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends StaffRestfulTranslator {
            public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
            {
                return parent::getEnterpriseRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->childStub->getEnterpriseRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Staff());
        $this->assertInstanceOf('Sdk\Staff\Model\NullStaff', $result);
    }

    public function setMethods(Staff $expectObject, array $attributes, array $relationships)
    {
        $user = new UserRestfulTranslatorTest();
        $expectObject = $user->setMethods($expectObject, $attributes);

        if (isset($attributes['birthday'])) {
            $expectObject->setBirthday($attributes['birthday']);
        }
        if (isset($attributes['education'])) {
            $expectObject->setEducation($attributes['education']);
        }
        if (isset($attributes['cardId'])) {
            $expectObject->setCardId($attributes['cardId']);
        }
        if (isset($attributes['briefIntroduction'])) {
            $expectObject->setBriefIntroduction($attributes['briefIntroduction']);
        }
        if (isset($attributes['professionalTitles'])) {
            $expectObject->setProfessionalTitle($attributes['professionalTitles']);
        }
        if (isset($attributes['isRecommendHomePage'])) {
            $expectObject->setIsRecommendHomePage($attributes['isRecommendHomePage']);
        }
        if (isset($relationships['enterprise']['data'])) {
            $expectObject->setEnterprise(new Enterprise($relationships['enterprise']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $staff = MockFactory::generateStaffArray();

        $data =  $staff['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->any())
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($staff);

        $expectObject = new Staff();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $staff = MockFactory::generateStaffArray();
        $data =  $staff['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($staff);
        $expectArray = array();

        $expectObject = new Staff();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $staff = MockFactory::generateStaffObject(1, 1);

        $actual = $this->stub->objectToArray(
            $staff,
            array(
                'birthday',
                'education',
                'cardId',
                'briefIntroduction',
                'professionalTitle',
                'isRecommendHomePage',
                'enterprise'
            )
        );

        $expectedArray = array(
            'data'=>array(
                'type'=>'staffs'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'birthday'=>$staff->getBirthday(),
            'education'=>$staff->getEducation(),
            'cardId'=>$staff->getCardId(),
            'briefIntroduction'=>$staff->getBriefIntroduction(),
            'professionalTitle'=>$staff->getProfessionalTitle(),
            'isRecommendHomePage'=>$staff->getIsRecommendHomePage()
        );

        $expectedArray['data']['relationships']['enterprise']['data'] = array(
            array(
                'type' => 'enterprises',
                'id' => $staff->getEnterprise()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
