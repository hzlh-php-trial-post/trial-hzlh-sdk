<?php
namespace Sdk\Staff\Repository;

use Sdk\Staff\Adapter\Staff\StaffRestfulAdapter;
use Sdk\Staff\Adapter\Staff\StaffMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class StaffRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(StaffRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends StaffRepository {
            public function getAdapter() : StaffRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : StaffMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Staff\Adapter\Staff\StaffRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Staff\Adapter\Staff\StaffMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为StaffRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以StaffRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(StaffRestfulAdapter::class);
        $adapter->scenario(Argument::exact(StaffRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(StaffRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为StaffRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行recommendHomePage（）
     * 判断 result 是否为true
     */
    public function testRecommendHomePage()
    {
        $staff = \Sdk\Staff\Utils\MockFactory::generateStaffObject(1);

        $adapter = $this->prophesize(StaffRestfulAdapter::class);
        $adapter->recommendHomePage(
            Argument::exact($staff)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->recommendHomePage($staff);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为StaffRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行cancelRecommendHomePage()
     * 判断 result 是否为true
     */
    public function testCancelRecommendHomePage()
    {
        $staff = \Sdk\Staff\Utils\MockFactory::generateStaffObject(1);

        $adapter = $this->prophesize(StaffRestfulAdapter::class);
        $adapter->cancelRecommendHomePage(
            Argument::exact($staff)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->cancelRecommendHomePage($staff);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为StaffRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletes()
    {
        $staff = \Sdk\Staff\Utils\MockFactory::generateStaffObject(1);

        $adapter = $this->prophesize(StaffRestfulAdapter::class);
        $adapter->deletes(
            Argument::exact($staff)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deletes($staff);
        $this->assertTrue($result);
    }
}
