<?php
namespace Sdk\Staff\Model;

use Sdk\Staff\Repository\StaffRepository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class StaffTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Staff::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends Staff{
            public function getRepository() : StaffRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Staff\Repository\StaffRepository',
            $this->childStub->getRepository()
        );
    }

    public function testExtendsUser()
    {
        $this->assertInstanceOf('Sdk\User\Model\User', $this->stub);
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //birthday 测试 ---------------------------------------------------------- start
    /**
     * 设置setBirthday() 正确的传参类型,期望传值正确
     */
    public function testSetBirthdayCorrectType()
    {
        $this->stub->setBirthday('string');
        $this->assertEquals('string', $this->stub->getBirthday());
    }

    /**
     * 设置setBirthday() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBirthdayWrongType()
    {
        $this->stub->setBirthday(array(1, 2, 3));
    }
    //birthday 测试 ----------------------------------------------------------   end

    //education 测试 ---------------------------------------------------------- start
    /**
     * 设置setEducation() 正确的传参类型,期望传值正确
     */
    public function testSetEducationCorrectType()
    {
        $this->stub->setEducation(1);
        $this->assertEquals(1, $this->stub->getEducation());
    }

    /**
     * 设置setEducation() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEducationWrongType()
    {
        $this->stub->setEducation(array(1, 2, 3));
    }
    //education 测试 ----------------------------------------------------------   end

    //cardId 测试 ---------------------------------------------------------- start
    /**
     * 设置setCardId() 正确的传参类型,期望传值正确
     */
    public function testSetCardIdCorrectType()
    {
        $this->stub->setCardId('string');
        $this->assertEquals('string', $this->stub->getCardId());
    }

    /**
     * 设置setCardId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCardIdWrongType()
    {
        $this->stub->setCardId(array(1, 2, 3));
    }
    //cardId 测试 ----------------------------------------------------------   end

    //briefIntroduction 测试 ---------------------------------------------------------- start
    /**
     * 设置setBriefIntroduction() 正确的传参类型,期望传值正确
     */
    public function testSetBriefIntroductionCorrectType()
    {
        $this->stub->setBriefIntroduction('string');
        $this->assertEquals('string', $this->stub->getBriefIntroduction());
    }

    /**
     * 设置setBriefIntroduction() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBriefIntroductionWrongType()
    {
        $this->stub->setBriefIntroduction(array(1, 2, 3));
    }
    //briefIntroduction 测试 ----------------------------------------------------------   end

    //professionalTitle 测试 ---------------------------------------------------------- start
    /**
     * 设置setProfessionalTitle() 正确的传参类型,期望传值正确
     */
    public function testSetProfessionalTitleCorrectType()
    {
        $this->stub->setProfessionalTitle(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getProfessionalTitle());
    }

    /**
     * 设置setProfessionalTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetProfessionalTitleWrongType()
    {
        $this->stub->setProfessionalTitle('string');
    }
    //professionalTitle 测试 ----------------------------------------------------------   end

    //enterprise 测试 ---------------------------------------------------------- start
    /**
     * 设置setEnterprise() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseCorrectType()
    {
        $object = new \Sdk\Enterprise\Model\Enterprise();

        $this->stub->setEnterprise($object);
        $this->assertEquals($object, $this->stub->getEnterprise());
    }

    /**
     * 设置setEnterprise() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseWrongType()
    {
        $this->stub->setEnterprise(array(1, 2, 3));
    }
    //enterprise 测试 ----------------------------------------------------------   end

    //删除
    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(Staff::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(StaffRepository::class);
        $repository->deletes(Argument::exact($this->stub))
            ->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFailure()
    {
        $this->stub = $this->getMockBuilder(Staff::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(StaffRepository::class);
        $repository->deletes(Argument::exact($this->stub))
            ->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }

    //推荐
    public function testRecommendHomePageSuccess()
    {
        $this->stub = $this->getMockBuilder(Staff::class)
            ->setMethods([
                'getRepository',
                'isRecommendHomePage'
            ])->getMock();

        $this->stub->expects($this->any())
            ->method('isRecommendHomePage')
            ->willReturn(true);

        $repository = $this->prophesize(StaffRepository::class);
        $repository->recommendHomePage(Argument::exact($this->stub))
            ->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->recommendHomePage();
        $this->assertTrue($result);
    }
    public function testRecommendHomePageFailure()
    {
        $this->stub = $this->getMockBuilder(Staff::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(StaffRepository::class);
        $repository->recommendHomePage(Argument::exact($this->stub))
            ->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->recommendHomePage();
        $this->assertFalse($result);
    }

    //取消推荐
    public function testCancelRecommendHomePageSuccess()
    {
        $this->stub = $this->getMockBuilder(Staff::class)
            ->setMethods([
                'getRepository',
                'isNotRecommendHomePage'
            ])->getMock();

        $this->stub->expects($this->any())
            ->method('isNotRecommendHomePage')
            ->willReturn(true);

        $repository = $this->prophesize(StaffRepository::class);
        $repository->cancelRecommendHomePage(Argument::exact($this->stub))
            ->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->cancelRecommendHomePage();
        $this->assertTrue($result);
    }
    public function testCancelRecommendHomePageFailure()
    {
        $this->stub = $this->getMockBuilder(Staff::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(StaffRepository::class);
        $repository->cancelRecommendHomePage(Argument::exact($this->stub))
            ->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->cancelRecommendHomePage();
        $this->assertFalse($result);
    }
}
