<?php
namespace Sdk\Staff\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullStaffTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullStaff::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsStaff()
    {
        $this->assertInstanceof('Sdk\Staff\Model\Staff', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
