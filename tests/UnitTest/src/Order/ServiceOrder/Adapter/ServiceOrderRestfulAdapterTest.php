<?php
namespace Sdk\Order\ServiceOrder\Adapter\ServiceOrder;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\Order\ServiceOrder\Model\NullServiceOrder;
use Sdk\Order\ServiceOrder\Utils\MockFactory;
use Sdk\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator;

class ServiceOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ServiceOrderRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends ServiceOrderRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIServiceOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\ServiceOrder\Adapter\ServiceOrder\IServiceOrderAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('serviceOrders', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_SERVICE_ORDER_LIST',
                ServiceOrderRestfulAdapter::SCENARIOS['OA_SERVICE_ORDER_LIST']
            ],
            [
                'PORTAL_SERVICE_ORDER_LIST',
                ServiceOrderRestfulAdapter::SCENARIOS['PORTAL_SERVICE_ORDER_LIST']
            ],
            [
                'SERVICE_ORDER_FETCH_ONE',
                ServiceOrderRestfulAdapter::SCENARIOS['SERVICE_ORDER_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $serviceOrder = MockFactory::generateServiceOrderObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullServiceOrder())
            ->willReturn($serviceOrder);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($serviceOrder, $result);
    }
    /**
     * 为ServiceOrderRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$ServiceOrder$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareServiceOrderTranslator(
        ServiceOrder $serviceOrder,
        array $keys,
        array $serviceOrderArray
    ) {
        $translator = $this->prophesize(ServiceOrderRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($serviceOrder),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($serviceOrderArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(ServiceOrder $serviceOrder)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($serviceOrder);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'remark',
                'buyerMemberAccount',
                'memberCoupon',
                'orderAddress',
                'orderCommodities'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('serviceOrders', $serviceOrderArray);

        $this->success($serviceOrder);

        $result = $this->stub->add($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'remark',
                'buyerMemberAccount',
                'memberCoupon',
                'orderAddress',
                'orderCommodities'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('serviceOrders', $serviceOrderArray);

        $this->failure($serviceOrder);
        $result = $this->stub->add($serviceOrder);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'paymentPassword'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with(
                'payments/'.$serviceOrder->getPaymentId(),
                $serviceOrderArray
            );

        $this->success($serviceOrder);

        $result = $this->stub->edit($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'paymentPassword'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with(
                'payments/'.$serviceOrder->getPaymentId(),
                $serviceOrderArray
            );

        $this->failure($serviceOrder);
        $result = $this->stub->edit($serviceOrder);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testUpdateOrderAmountSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'amount',
                'remark'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/updateOrderAmount',
                $serviceOrderArray
            );

        $this->success($serviceOrder);

        $result = $this->stub->updateOrderAmount($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testUpdateOrderAmountFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'amount',
                'remark'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/updateOrderAmount',
                $serviceOrderArray
            );

        $this->failure($serviceOrder);
        $result = $this->stub->updateOrderAmount($serviceOrder);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testUpdateOrderAddressSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'orderAddress'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/updateOrderAddress',
                $serviceOrderArray
            );

        $this->success($serviceOrder);

        $result = $this->stub->updateOrderAddress($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testUpdateOrderAddressFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'orderAddress'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/updateOrderAddress',
                $serviceOrderArray
            );

        $this->failure($serviceOrder);
        $result = $this->stub->updateOrderAddress($serviceOrder);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testBuyerCancelSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'cancelReason'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/buyerCancel',
                $serviceOrderArray
            );

        $this->success($serviceOrder);

        $result = $this->stub->buyerCancel($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testBuyerCancelFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'cancelReason'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/buyerCancel',
                $serviceOrderArray
            );

        $this->failure($serviceOrder);
        $result = $this->stub->buyerCancel($serviceOrder);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testSellerCancelSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'cancelReason'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/sellerCancel',
                $serviceOrderArray
            );

        $this->success($serviceOrder);

        $result = $this->stub->sellerCancel($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testSellerCancelFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'cancelReason'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/sellerCancel',
                $serviceOrderArray
            );

        $this->failure($serviceOrder);
        $result = $this->stub->sellerCancel($serviceOrder);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testPerformanceBeginSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/performanceBegin'
            );

        $this->success($serviceOrder);

        $result = $this->stub->performanceBegin($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testPerformanceBeginFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/performanceBegin'
            );

        $this->failure($serviceOrder);
        $result = $this->stub->performanceBegin($serviceOrder);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testPerformanceEndSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/performanceEnd'
            );

        $this->success($serviceOrder);

        $result = $this->stub->performanceEnd($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testPerformanceEndFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/performanceEnd'
            );

        $this->failure($serviceOrder);
        $result = $this->stub->performanceEnd($serviceOrder);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testBuyerConfirmationSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/buyerConfirmation'
            );

        $this->success($serviceOrder);

        $result = $this->stub->buyerConfirmation($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testBuyerConfirmationFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/buyerConfirmation'
            );

        $this->failure($serviceOrder);
        $result = $this->stub->buyerConfirmation($serviceOrder);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testBuyerDeleteSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/buyerDelete'
            );

        $this->success($serviceOrder);

        $result = $this->stub->buyerDelete($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testBuyerDeleteFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/buyerDelete'
            );

        $this->failure($serviceOrder);
        $result = $this->stub->buyerDelete($serviceOrder);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testBuyerPermanentDeleteSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/buyerPermanentDelete'
            );

        $this->success($serviceOrder);

        $result = $this->stub->buyerPermanentDelete($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testBuyerPermanentDeleteFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/buyerPermanentDelete'
            );

        $this->failure($serviceOrder);
        $result = $this->stub->buyerPermanentDelete($serviceOrder);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testSellerDeleteSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/sellerDelete'
            );

        $this->success($serviceOrder);

        $result = $this->stub->sellerDelete($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testSellerDeleteFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'serviceOrders/'.$serviceOrder->getId().'/sellerDelete'
            );

        $this->failure($serviceOrder);
        $result = $this->stub->sellerDelete($serviceOrder);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testPaymentFailureSuccess()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'failureReason'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'payments/'.$serviceOrder->getPaymentId().'/paymentFailure',
                $serviceOrderArray
            );

        $this->success($serviceOrder);

        $result = $this->stub->paymentFailure($serviceOrder);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareServiceOrderTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testPaymentFailureFailure()
    {
        $serviceOrder = MockFactory::generateServiceOrderObject(1);
        $serviceOrderArray = array();

        $this->prepareServiceOrderTranslator(
            $serviceOrder,
            array(
                'failureReason'
            ),
            $serviceOrderArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'payments/'.$serviceOrder->getPaymentId().'/paymentFailure',
                $serviceOrderArray
            );

        $this->failure($serviceOrder);
        $result = $this->stub->paymentFailure($serviceOrder);
        $this->assertFalse($result);
    }
}
