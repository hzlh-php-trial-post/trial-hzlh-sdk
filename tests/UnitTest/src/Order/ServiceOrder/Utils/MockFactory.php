<?php
namespace Sdk\Order\ServiceOrder\Utils;

use Sdk\Order\ServiceOrder\Model\ServiceOrder;

use Sdk\Payment\Model\Payment;

class MockFactory
{
    /**
     * [generateServiceOrderArray 生成收货地址数组]
     * @return [array] [地址信息]
     */
    public static function generateServiceOrderArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $serviceOrder = array();

        $serviceOrder = array(
            'data'=>array(
                'type'=>'serviceOrders',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();
        //platformPreferentialAmount
        $platformPreferentialAmount = self::generatePlatformPreferentialAmount($faker, $value);
        $attributes['platformPreferentialAmount'] = $platformPreferentialAmount;
        //businessPreferentialAmount
        $businessPreferentialAmount = self::generateBusinessPreferentialAmount($faker, $value);
        $attributes['businessPreferentialAmount'] = $businessPreferentialAmount;
        //paymentType
        $paymentType = Payment::PAYMENT_TYPE['WECHAT'];
        $attributes['paymentType'] = $paymentType;
        //transactionNumber
        $transactionNumber = self::generateTransactionNumber($faker, $value);
        $attributes['transactionNumber'] = $transactionNumber;
        //transactionInfo
        $transactionInfo = self::generateTransactionInfo($faker, $value);
        $attributes['transactionInfo'] = $transactionInfo;
        //paymentTime
        $paymentTime = self::generatePaymentTime($faker, $value);
        $attributes['paymentTime'] = $paymentTime;
        //paymentId
        $paymentId = self::generatePaymentId($faker, $value);
        $attributes['paymentId'] = $paymentId;
        //amount
        $amount = self::generateAmount($faker, $value);
        $attributes['amount'] = $amount;
        //cancelReason
        $cancelReason = self::generateCancelReason($faker, $value);
        $attributes['cancelReason'] = $cancelReason;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $serviceOrder['data']['attributes'] = $attributes;

        return $serviceOrder;
    }

    /**
     * [generateServiceOrderObject 生成收货地址信息对象]
     * @param  int|integer $id
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]             [地址信息]
     */
    public static function generateServiceOrderObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ServiceOrder {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $serviceOrder = new ServiceOrder($id);

        //platformPreferentialAmount
        $platformPreferentialAmount = self::generatePlatformPreferentialAmount($faker, $value);
        $serviceOrder->setPlatformPreferentialAmount($platformPreferentialAmount);
        //businessPreferentialAmount
        $businessPreferentialAmount = self::generateBusinessPreferentialAmount($faker, $value);
        $serviceOrder->setBusinessPreferentialAmount($businessPreferentialAmount);
        //transactionNumber
        $transactionNumber = self::generateTransactionNumber($faker, $value);
        //transactionInfo
        $transactionInfo = self::generateTransactionInfo($faker, $value);
        //paymentTime
        $paymentTime = self::generatePaymentTime($faker, $value);
        //paymentId
        $paymentId = self::generatePaymentId($faker, $value);
        $serviceOrder->setPaymentId($paymentId);

        $serviceOrder->setPayment(
            new Payment(
                $type = Payment::PAYMENT_TYPE['WECHAT'],
                $paymentTime,
                $transactionNumber,
                $transactionInfo
            )
        );
        //cancelReason
        $cancelReason = self::generateCancelReason($faker, $value);
        $serviceOrder->setCancelReason($cancelReason);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $serviceOrder->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $serviceOrder->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $serviceOrder->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $serviceOrder->setStatus($status);

        return $serviceOrder;
    }

    private static function generatePlatformPreferentialAmount($faker, array $value = array())
    {
        return $generatePlatformPreferentialAmount = isset($value['generatePlatformPreferentialAmount']) ?
            $value['generatePlatformPreferentialAmount'] : 10.02;
    }

    private static function generateBusinessPreferentialAmount($faker, array $value = array())
    {
        return $generateBusinessPreferentialAmount = isset($value['generateBusinessPreferentialAmount']) ?
            $value['generateBusinessPreferentialAmount'] : 10.02;
    }

    private static function generateTransactionNumber($faker, array $value = array())
    {
        return $generateTransactionNumber = isset($attributes['generateTransactionNumber']) ?
        $attributes['generateTransactionNumber'] : '715600321312312312312312312312';
    }

    private static function generateTransactionInfo($faker, array $value = array())
    {
        return $generateTransactionInfo = isset($attributes['generateTransactionInfo']) ?
        $attributes['generateTransactionInfo'] : array(1, 2, 3);
    }

    private static function generatePaymentTime($faker, array $value = array())
    {
        return $generatePaymentTime = isset($attributes['generatePaymentTime']) ?
        $attributes['generatePaymentTime'] : '13202938747';
    }

    private static function generatePaymentId($faker, array $value = array())
    {
        return $generatePaymentId = isset($attributes['generatePaymentId']) ?
        $attributes['generatePaymentId'] : '1_21';
    }

    private static function generateAmount($faker, array $value = array())
    {
        return $generateAmount = isset($attributes['generateAmount']) ?
        $attributes['generateAmount'] : 1.20;
    }

    private static function generateCancelReason($faker, array $value = array())
    {
        return $cancelReason = isset($attributes['cancelReason']) ?
        $attributes['cancelReason'] : 1;
    }
}
