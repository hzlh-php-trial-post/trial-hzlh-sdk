<?php
namespace Sdk\Order\ServiceOrder\Repository;

use Sdk\Order\ServiceOrder\Adapter\ServiceOrder\ServiceOrderRestfulAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class ServiceOrderRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ServiceOrderRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends ServiceOrderRepository {
            public function getAdapter() : ServiceOrderRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\ServiceOrder\Adapter\ServiceOrder\ServiceOrderRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为ServiceOrderRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以ServiceOrderRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(ServiceOrderRestfulAdapter::class);
        $adapter->scenario(Argument::exact(ServiceOrderRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
    /**
     * 生成模拟数据，传参为1
     * 为ServiceOrderRestfulAdapter建立预言
     * 建立预期状况：updateOrderAmount() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testUpdateOrderAmount()
    {
        $serviceOrder = \Sdk\Order\ServiceOrder\Utils\MockFactory::generateServiceOrderObject(1);

        $adapter = $this->prophesize(ServiceOrderRestfulAdapter::class);
        $adapter->updateOrderAmount(Argument::exact($serviceOrder))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->updateOrderAmount($serviceOrder);
        $this->assertTrue($result);
    }

        /**
     * 生成模拟数据，传参为1
     * 为ServiceOrderRestfulAdapter建立预言
     * 建立预期状况：updateOrderAddress() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testUpdateOrderAddress()
    {
        $serviceOrder = \Sdk\Order\ServiceOrder\Utils\MockFactory::generateServiceOrderObject(1);

        $adapter = $this->prophesize(ServiceOrderRestfulAdapter::class);
        $adapter->updateOrderAddress(Argument::exact($serviceOrder))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->updateOrderAddress($serviceOrder);
        $this->assertTrue($result);
    }

        /**
     * 生成模拟数据，传参为1
     * 为ServiceOrderRestfulAdapter建立预言
     * 建立预期状况：paymentFailure() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testPaymentFailure()
    {
        $serviceOrder = \Sdk\Order\ServiceOrder\Utils\MockFactory::generateServiceOrderObject(1);

        $adapter = $this->prophesize(ServiceOrderRestfulAdapter::class);
        $adapter->paymentFailure(Argument::exact($serviceOrder))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->paymentFailure($serviceOrder);
        $this->assertTrue($result);
    }
}
