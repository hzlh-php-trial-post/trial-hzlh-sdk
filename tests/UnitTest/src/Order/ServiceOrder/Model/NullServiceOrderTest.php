<?php
namespace Sdk\Order\ServiceOrder\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullServiceOrderTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullServiceOrder::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsServiceOrder()
    {
        $this->assertInstanceof('Sdk\Order\ServiceOrder\Model\ServiceOrder', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testPlaceOrderAction()
    {
        $result = $this->stub->placeOrderAction();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testPayAction()
    {
        $result = $this->stub->payAction();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testPaymentFailure()
    {
        $result = $this->stub->paymentFailure();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
