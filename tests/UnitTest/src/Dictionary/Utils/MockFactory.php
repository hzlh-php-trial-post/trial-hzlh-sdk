<?php
namespace Sdk\Dictionary\Utils;

use Sdk\Dictionary\Model\Dictionary;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateDictionaryArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateDictionaryArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $dictionary = array();

        $dictionary = array(
            'data'=>array(
                'type'=>'dictionaries',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //remark
        $remark = self::generateRemark($faker, $value);
        $attributes['remark'] = $remark;
        //category
        $category = self::generateCategory($faker, $value);
        $attributes['category'] = $category;
        //parentId
        $parentId = self::generateParentId($faker, $value);
        $attributes['parentId'] = $parentId;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $dictionary['data']['attributes'] = $attributes;
        //crew
        $dictionary['data']['relationships']['crew']['data'] = array(
            'type' => 'crews',
            'id' => $faker->randomNumber(1)
        );

        return $dictionary;
    }

    /**
     * [generateDictionaryObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateDictionaryObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Dictionary {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $dictionary = new Dictionary($id);

        //name
        $name = self::generateName($faker, $value);
        $dictionary->setName($name);
        //remark
        $remark = self::generateRemark($faker, $value);
        $dictionary->setRemark($remark);
        //category
        $category = self::generateCategory($faker, $value);
        $attributes['category'] = $category;
        //parentId
        $parentId = self::generateParentId($faker, $value);
        $attributes['parentId'] = $parentId;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $dictionary->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $dictionary->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $dictionary->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $dictionary->setStatus($status);
        //crew
        $crew = self::generateCrew($faker, $value);
        $dictionary->setCrew($crew);

        return $dictionary;
    }

    private static function generateName($faker, array $value = array())
    {
        return isset($value['name']) ? $value['name'] : $faker->name();
    }

    private static function generateRemark($faker, array $value = array())
    {
        return isset($value['remark']) ? $value['remark'] : $faker->word();
    }

    private static function generateCategory($faker, array $value = array())
    {
        return isset($value['category']) ? $value['category'] : $faker->randomElement(Dictionary::CATEGORY);
    }

    private static function generateParentId($faker, array $value = array())
    {
        return isset($value['parentId']) ? $value['parentId'] : $faker->randomNumber();
    }

    private static function generateCrew($faker, array $value = array())
    {
        return isset($value['crew']) ?
                $value['crew'] :
                \Sdk\Crew\Utils\MockFactory::generateCrewObject($faker->randomNumber());
    }
}
