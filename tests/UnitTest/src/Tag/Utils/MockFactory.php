<?php
namespace Sdk\Tag\Utils;

use Sdk\Tag\Model\Tag\Tag;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateTagArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateTagArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $Tag = array();

        $Tag = array(
            'data'=>array(
                'type'=>'tags',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //pid
        $pid = self::generateIcon($faker, $value);
        $attributes['pid'] = $pid;
        //remark
        $remark = self::generateRemark($faker, $value);
        $attributes['remark'] = $remark;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $Tag['data']['attributes'] = $attributes;
        //crew
        $Tag['data']['relationships']['crew']['data'] = array(
            'type' => 'crews',
            'id' => $faker->randomNumber(1)
        );

        return $Tag;
    }
    /**
     * [generateTagObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateTagObject(int $id = 0, int $seed = 0, array $value = array()) : Tag
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $Tag = new Tag($id);

        //name
        $name = self::generateName($faker, $value);
        $Tag->setName($name);
        //pid
        $pid = self::generateIcon($faker, $value);
        $Tag->setPid($pid);
        //remark
        $remark = self::generateRemark($faker, $value);
        $Tag->setRemark($remark);
        //crew
        $crew = self::generateCrew($faker, $value);
        $Tag->setCrew($crew);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $Tag->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $Tag->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $Tag->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $Tag->setStatus($status);

        return $Tag;
    }

    private static function generateName($faker, array $value = array())
    {
        return $name = isset($value['name']) ?
        $value['name'] : $faker->name;
    }

    private static function generateIcon($faker, array $value = array())
    {
        return $pid = isset($value['pid']) ?
        $value['pid'] : $faker->randomNumber(1);
    }

    private static function generateRemark($faker, array $value = array())
    {
        return $remark = isset($value['remark']) ?
        $value['remark'] : $faker->word;
    }

    private static function generateCrew($faker, array $value = array())
    {
        return $crew = isset($value['crew']) ?
            $value['crew'] : \Sdk\Crew\Utils\MockFactory::generateCrewObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
