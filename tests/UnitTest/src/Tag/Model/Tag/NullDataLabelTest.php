<?php
namespace Sdk\Tag\Model\Tag;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullTagTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullTag::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsTag()
    {
        $this->assertInstanceof('Sdk\Tag\Model\Tag\Tag', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
