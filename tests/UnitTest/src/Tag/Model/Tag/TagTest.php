<?php
namespace Sdk\Tag\Model\Tag;

use Sdk\Tag\Repository\Tag\TagRepository;
use Sdk\Common\Model\IEnableAble;
use Sdk\Crew\Model\Crew;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class TagTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Tag::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends Tag{
            public function getRepository() : TagRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Tag\Repository\Tag\TagRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 User setId() 正确的传参类型,期望传值正确
     */
    public function testSetTagIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 User setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetTagIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 ---------------------------------------------------------- start
    /**
     * 设置 Tag setName() 正确的传参类型,期望传值正确
     */
    public function testSetTagNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置 Tag setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTagNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }
    //name 测试 ----------------------------------------------------------   end

    //icon 测试 ---------------------------------------------------------- start
    /**
     * 设置 Tag setPid() 正确的传参类型,期望传值正确
     */
    public function testSetTagPidCorrectType()
    {
        $this->stub->setPid(1);
        $this->assertEquals(1, $this->stub->getPid());
    }

    /**
     * 设置 Tag setPid() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTagPidWrongType()
    {
        $this->stub->setPid('string');
    }
    //icon 测试 ----------------------------------------------------------   end
    

    //remark 测试 ---------------------------------------------------------- start
    /**
     * 设置 Tag setRemark() 正确的传参类型,期望传值正确
     */
    public function testSetTagRemarkCorrectType()
    {
        $this->stub->setRemark('string');
        $this->assertEquals('string', $this->stub->getRemark());
    }

    /**
     * 设置 Tag setRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTagRemarkWrongType()
    {
        $this->stub->setRemark(array(1, 2, 3));
    }
    //remark 测试 ----------------------------------------------------------   end

    //crew 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetTagCrewCorrectType()
    {
        $object = new Crew();
        $this->stub->setCrew($object);
        $this->assertSame($object, $this->stub->getCrew());
    }

    /**
     * 设置 Crew setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTagCrewType()
    {
        $this->stub->setCrew(array(1,2,3));
    }
    //crew 测试 -------------------------------------------------------- end
}
