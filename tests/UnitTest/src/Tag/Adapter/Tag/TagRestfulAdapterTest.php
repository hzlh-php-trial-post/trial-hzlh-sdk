<?php
namespace Sdk\Tag\Adapter\Tag;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Tag\Model\Tag\Tag;
use Sdk\Tag\Model\Tag\NullTag;
use Sdk\Tag\Utils\MockFactory;
use Sdk\Tag\Translator\Tag\TagRestfulTranslator;

class TagRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(TagRestfulAdapter::class)
            ->setMethods([
                'post',
                'patch',
                'isSuccess',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends TagRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsITagAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Tag\Adapter\Tag\ITagAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('tags', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Tag\Translator\Tag\TagRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'TAG_LIST',
                TagRestfulAdapter::SCENARIOS['TAG_LIST']
            ],
            [
                'TAG_FETCH_ONE',
                TagRestfulAdapter::SCENARIOS['TAG_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 为TagRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$tag，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareTagTranslator(
        Tag $tag,
        array $keys,
        array $tagArray
    ) {
        $translator = $this->prophesize(TagRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($tag),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($tagArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Tag $tag)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($tag);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTagTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $tag = MockFactory::generateTagObject(1);
        $tagArray = array();

        $this->prepareTagTranslator(
            $tag,
            array(
                'name',
                'pid',
                'remark',
                'crew'
            ),
            $tagArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('tags', $tagArray);

        $this->success($tag);

        $result = $this->stub->add($tag);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTagTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $tag = MockFactory::generateTagObject(1);
        $tagArray = array();

        $this->prepareTagTranslator(
            $tag,
            array(
                'name',
                'pid',
                'remark',
                'crew'
            ),
            $tagArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('tags', $tagArray);

        $this->failure($tag);
        $result = $this->stub->add($tag);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTagTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $tag = MockFactory::generateTagObject(1);
        $tagArray = array();

        $this->prepareTagTranslator(
            $tag,
            array(
                'pid',
                'remark'
            ),
            $tagArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'tags/'.$tag->getId(),
                $tagArray
            );

        $this->success($tag);

        $result = $this->stub->edit($tag);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareTagTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $tag = MockFactory::generateTagObject(1);
        $tagArray = array();

        $this->prepareTagTranslator(
            $tag,
            array(
                'pid',
                'remark'
            ),
            $tagArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'tags/'.$tag->getId(),
                $tagArray
            );

        $this->failure($tag);
        $result = $this->stub->edit($tag);
        $this->assertFalse($result);
    }
}
