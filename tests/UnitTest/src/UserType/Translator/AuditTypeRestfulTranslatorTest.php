<?php
namespace Sdk\UserType\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\UserType\Model\AuditType;

class AuditTypeRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AuditTypeRestfulTranslator::class)
            ->setMethods([
                'getCrewRestfulTranslator',
                'getUserTypeRestfulTranslator'
            ])
            ->getMock();

        $this->childStub = new MockAuditTypeRestfulTranslator();
        parent::setUp();
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function testGetUserTypeRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Translator\UserTypeRestfulTranslator',
            $this->childStub->getUserTypeRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new AuditType());
        $this->assertInstanceOf('Sdk\UserType\Model\NullAuditType', $result);
    }
}
