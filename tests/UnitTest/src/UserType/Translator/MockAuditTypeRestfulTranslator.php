<?php
namespace Sdk\UserType\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\Crew\Translator\CrewRestfulTranslator;
use Sdk\UserType\Model\NullAuditType;
use Sdk\UserType\Model\AuditType;
use Sdk\UserType\Translator\UserTypeRestfulTranslator;

class MockAuditTypeRestfulTranslator extends AuditTypeRestfulTranslator
{
    public function getCrewRestfulTranslator()
    {
        return parent::getCrewRestfulTranslator();
    }

    public function getUserTypeRestfulTranslator()
    {
        return parent::getUserTypeRestfulTranslator();
    }
}
