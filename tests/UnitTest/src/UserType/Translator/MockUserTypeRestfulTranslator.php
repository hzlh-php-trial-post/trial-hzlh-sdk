<?php
namespace Sdk\UserType\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\Crew\Translator\CrewRestfulTranslator;
use Sdk\UserType\Model\NullUserType;
use Sdk\UserType\Model\UserType;

class MockUserTypeRestfulTranslator extends UserTypeRestfulTranslator
{
    public function getCrewRestfulTranslator()
    {
        return parent::getCrewRestfulTranslator();
    }
}
