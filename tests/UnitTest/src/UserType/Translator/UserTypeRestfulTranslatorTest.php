<?php
namespace Sdk\UserType\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\UserType\Model\UserType;

class UserTypeRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(UserTypeRestfulTranslator::class)
            ->setMethods([
                'getCrewRestfulTranslator'
            ])
            ->getMock();

        $this->childStub = new MockUserTypeRestfulTranslator();
        parent::setUp();
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new UserType());
        $this->assertInstanceOf('Sdk\UserType\Model\NullUserType', $result);
    }
}
