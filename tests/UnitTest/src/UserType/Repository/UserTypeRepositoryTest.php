<?php
namespace Sdk\UserType\Repository;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

use Sdk\UserType\Adapter\UserType\UserTypeRestfulAdapter;
use Sdk\UserType\Utils\MockUserTypeFactory;

class UserTypeRepositoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(UserTypeRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Adapter\UserType\IUserTypeAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Adapter\UserType\UserTypeMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $adapter = $this->prophesize(UserTypeRestfulAdapter::class);
        $adapter->scenario(Argument::exact(UserTypeRepository::LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(UserTypeRepository::LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    public function testEnable()
    {
        $userType = MockUserTypeFactory::generateUserTypeObject(1);

        $adapter = $this->prophesize(UserTypeRestfulAdapter::class);
        $adapter->enable(Argument::exact($userType))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->enable($userType);
        $this->assertTrue($result);
    }

    public function testDisable()
    {
        $userType = MockUserTypeFactory::generateUserTypeObject(1);

        $adapter = $this->prophesize(UserTypeRestfulAdapter::class);
        $adapter->disable(Argument::exact($userType))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->disable($userType);
        $this->assertTrue($result);
    }
}
