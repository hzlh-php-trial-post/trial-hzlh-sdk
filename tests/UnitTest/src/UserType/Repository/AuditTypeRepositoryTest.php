<?php
namespace Sdk\UserType\Repository;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

use Sdk\UserType\Adapter\AuditType\AuditTypeRestfulAdapter;
use Sdk\UserType\Utils\MockAuditTypeFactory;

class AuditTypeRepositoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AuditTypeRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Adapter\AuditType\IAuditTypeAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Adapter\AuditType\AuditTypeMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $adapter = $this->prophesize(AuditTypeRestfulAdapter::class);
        $adapter->scenario(Argument::exact(AuditTypeRepository::AUDIT_LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(AuditTypeRepository::AUDIT_LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    public function testApprove()
    {
        $auditType = MockAuditTypeFactory::generateAuditTypeObject(1);

        $adapter = $this->prophesize(AuditTypeRestfulAdapter::class);
        $adapter->approve(Argument::exact($auditType))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->approve($auditType);
        $this->assertTrue($result);
    }

    public function testReject()
    {
        $auditType = MockAuditTypeFactory::generateAuditTypeObject(1);

        $adapter = $this->prophesize(AuditTypeRestfulAdapter::class);
        $adapter->reject(Argument::exact($auditType))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->reject($auditType);
        $this->assertTrue($result);
    }
}
