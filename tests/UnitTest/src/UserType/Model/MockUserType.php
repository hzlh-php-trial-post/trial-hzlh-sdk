<?php
namespace Sdk\UserType\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Crew\Model\Crew;
use Sdk\UserType\Repository\UserTypeRepository;

class MockUserType extends UserType
{
    public function getRepository() : UserTypeRepository
    {
        return parent::getRepository();
    }

    public function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return parent::getIOperatAbleAdapter();
    }
}
