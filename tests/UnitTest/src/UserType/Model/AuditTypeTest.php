<?php
namespace Sdk\UserType\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Crew\Model\Crew;
use Sdk\UserType\Repository\AuditTypeRepository;
use Sdk\UserType\Model\MockAuditType;

class AuditTypeTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AuditType::class)
            ->setMethods([
                'getRepository'
            ])
            ->getMock();

        $this->childStub = new MockAuditType();
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Repository\AuditTypeRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 AuditType setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 AuditType setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertSame('string', $this->stub->getName());
    }

    /**
     * 设置 AuditType setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameType()
    {
        $this->stub->setName(array(1,2,3));
    }
    //name 测试 -------------------------------------------------------- end

    //commission 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setCommission() 正确的传参类型,期望传值正确
     */
    public function testSetCommissionCorrectType()
    {
        $this->stub->setCommission(1);
        $this->assertSame(1, $this->stub->getCommission());
    }

    /**
     * 设置 AuditType setCommission() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCommissionWrongType()
    {
        $this->stub->setCommission('string');
    }
    //commission 测试 -------------------------------------------------------- end

    //subsidy 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setSubsidy() 正确的传参类型,期望传值正确
     */
    public function testSetSubsidyTypeCorrectType()
    {
        $this->stub->setSubsidy(1);
        $this->assertEquals(1, $this->stub->getSubsidy());
    }

    /**
     * 设置 AuditType setSubsidy() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubsidyTypeWrongType()
    {
        $this->stub->setSubsidy('string');
    }
    //subsidy 测试 --------------------------------------------------------   end

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->stub->setDescription('string');
        $this->assertSame('string', $this->stub->getDescription());
    }

    /**
     * 设置 AuditType setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionType()
    {
        $this->stub->setDescription(array(1,2,3));
    }
    //description 测试 -------------------------------------------------------- end

    //userType 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setUserType() 正确的传参类型,期望传值正确
     */
    public function testSetUserTypeCorrectType()
    {
        $object = new UserType();
        $this->stub->setUserType($object);
        $this->assertSame($object, $this->stub->getUserType());
    }

    /**
     * 设置 AuditType setUserType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserTypeType()
    {
        $this->stub->setUserType(1);
    }
    //userType 测试 -------------------------------------------------------- end

    //areaId 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setAreaId() 正确的传参类型,期望传值正确
     */
    public function testSetAreaIdCorrectType()
    {
        $this->stub->setAreaId(0);
        $this->assertSame(0, $this->stub->getAreaId());
    }

    /**
     * 设置 AuditType setAreaId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAreaIdType()
    {
        $this->stub->setAreaId('string');
    }
    //areaId 测试 -------------------------------------------------------- end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertSame(0, $this->stub->getStatus());
    }

    /**
     * 设置 AuditType setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusType()
    {
        $this->stub->setStatus('string');
    }
    //status 测试 -------------------------------------------------------- end

    //applyStatus 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setApplyStatus() 正确的传参类型,期望传值正确
     */
    public function testSetApplyStatusCorrectType()
    {
        $this->stub->setApplyStatus(1);
        $this->assertSame(1, $this->stub->getApplyStatus());
    }

    /**
     * 设置 AuditType setApplyStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyStatusType()
    {
        $this->stub->setApplyStatus('string');
    }
    //applyStatus 测试 -------------------------------------------------------- end

    //rejectReason 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->stub->setRejectReason('string');
        $this->assertSame('string', $this->stub->getRejectReason());
    }

    /**
     * 设置 AuditType setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonType()
    {
        $this->stub->setRejectReason(array(1,2,3));
    }
    //rejectReason 测试 -------------------------------------------------------- end

    //crew 测试 -------------------------------------------------------- start
    /**
     * 设置 AuditType setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->stub->setCrew($object);
        $this->assertSame($object, $this->stub->getCrew());
    }

    /**
     * 设置 AuditType setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewType()
    {
        $this->stub->setCrew(array(1,2,3));
    }
    //crew 测试 -------------------------------------------------------- end
}
