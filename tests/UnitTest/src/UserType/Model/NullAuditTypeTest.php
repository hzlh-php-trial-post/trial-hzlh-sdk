<?php
namespace Sdk\UserType\Model;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullAuditTypeTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullAuditType::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCoupon()
    {
        $this->assertInstanceof('Sdk\UserType\Model\AuditType', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
