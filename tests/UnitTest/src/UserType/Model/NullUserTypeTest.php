<?php
namespace Sdk\UserType\Model;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullUserTypeTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullUserType::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCoupon()
    {
        $this->assertInstanceof('Sdk\UserType\Model\UserType', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
