<?php
namespace Sdk\UserType\Model;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\UserType\Repository\AuditTypeRepository;

class MockAuditType extends AuditType
{
    public function getRepository() : AuditTypeRepository
    {
        return parent::getRepository();
    }

    public function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return parent::getIOperatAbleAdapter();
    }
}
