<?php
namespace Sdk\UserType\Adapter\AuditType;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

use Sdk\UserType\Adapter\AuditType\MockAuditTypeRestfulAdapter;
use Sdk\UserType\Model\NullAuditType;
use Sdk\UserType\Model\AuditType;
use Sdk\UserType\Translator\AuditTypeRestfulTranslator;
use Sdk\UserType\Utils\MockAuditTypeFactory;

class AuditTypeRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AuditTypeRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new MockAuditTypeRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIAuditTypeAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Adapter\AuditType\IAuditTypeAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('unAuditedStrategyTypes', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Translator\AuditTypeRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'AUDIT_TYPE_LIST',
                AuditTypeRestfulAdapter::SCENARIOS['AUDIT_TYPE_LIST']
            ],
            [
                'AUDIT_TYPE_FETCH_ONE',
                AuditTypeRestfulAdapter::SCENARIOS['AUDIT_TYPE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $auditType = MockAuditTypeFactory::generateAuditTypeObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullAuditType())
            ->willReturn($auditType);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($auditType, $result);
    }

    private function prepareAuditTypeTranslator(
        AuditType $auditType,
        array $keys,
        array $auditTypeArray
    ) {
        $translator = $this->prophesize(AuditTypeRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($auditType),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($auditTypeArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(AuditType $auditType)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($auditType);
    }

    // private function failure()
    // {
    //     $this->stub->expects($this->exactly(1))
    //         ->method('isSuccess')
    //         ->willReturn(false);
    //     $this->stub->expects($this->exactly(0))
    //         ->method('translateToObject');
    // }

    public function testAddAction()
    {
        $auditType = MockAuditTypeFactory::generateAuditTypeObject(1);
        $auditTypeArray = array();

        $this->prepareAuditTypeTranslator(
            $auditType,
            array(
                "name",
                "description",
                "areaId",
                "commission",
                "subsidy",
                "status",
                "crew",
                "strategyType"
            ),
            $auditTypeArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('unAuditedStrategyTypes', $auditTypeArray);

        $this->success($auditType);

        $result = $this->stub->add($auditType);
        $this->assertTrue($result);
    }

    public function testEditAction()
    {
        $auditType = MockAuditTypeFactory::generateAuditTypeObject(1);
        $data = array();

        $this->prepareAuditTypeTranslator(
            $auditType,
            array(
                "name",
                "description",
                "areaId",
                "commission",
                "subsidy",
                "status",
                "crew",
                "strategyType"
            ),
            $data
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('unAuditedStrategyTypes/'.$auditType->getId(), $data);

        $this->success($auditType);

        $result = $this->stub->edit($auditType);
        $this->assertTrue($result);
    }

    public function testApprove()
    {
        $auditType = MockAuditTypeFactory::generateAuditTypeObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('unAuditedStrategyTypes/'.$auditType->getId().'/approve');

        $this->success($auditType);

        $result = $this->stub->approve($auditType);
        $this->assertTrue($result);
    }

    public function testReject()
    {
        $auditType = MockAuditTypeFactory::generateAuditTypeObject(1);
        $data = array();

        $this->prepareAuditTypeTranslator(
            $auditType,
            array(
                "rejectReason"
            ),
            $data
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('unAuditedStrategyTypes/'.$auditType->getId().'/reject', $data);

        $this->success($auditType);

        $result = $this->stub->reject($auditType);
        $this->assertTrue($result);
    }
}
