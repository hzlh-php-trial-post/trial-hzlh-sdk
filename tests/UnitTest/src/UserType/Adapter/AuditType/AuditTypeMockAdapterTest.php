<?php
namespace Sdk\UserType\Adapter\AuditType;

use PHPUnit\Framework\TestCase;

use Sdk\UserType\Model\AuditType;

class AuditTypeMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new AuditTypeMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new AuditType()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new AuditType()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Model\AuditType',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'Sdk\UserType\Model\AuditType',
                $each
            );
        }
    }
}
