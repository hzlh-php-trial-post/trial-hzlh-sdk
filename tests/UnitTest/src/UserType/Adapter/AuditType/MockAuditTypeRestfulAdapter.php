<?php
namespace Sdk\UserType\Adapter\AuditType;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\UserType\Model\AuditType;

class MockAuditTypeRestfulAdapter extends AuditTypeRestfulAdapter
{
    public function getMapError() : array
    {
        return parent::getMapError();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getResource(): string
    {
        return parent::getResource();
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }

    public function addAction(AuditType $auditType): bool
    {
        return parent::addAction($auditType);
    }

    public function editAction(AuditType $auditType): bool
    {
        return parent::editAction($auditType);
    }
}
