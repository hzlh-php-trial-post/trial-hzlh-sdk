<?php
namespace Sdk\UserType\Adapter\UserType;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;
use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

use Sdk\UserType\Adapter\UserType\MockUserTypeRestfulAdapter;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Sdk\UserType\Model\NullUserType;
use Sdk\UserType\Model\UserType;
use Sdk\UserType\Translator\UserTypeRestfulTranslator;
use Sdk\UserType\Utils\MockUserTypeFactory;

class UserTypeRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(UserTypeRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new MockUserTypeRestfulAdapter();
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIUserTypeAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Adapter\UserType\IUserTypeAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('strategyTypes', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\UserType\Translator\UserTypeRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'USERTYPE_LIST',
                UserTypeRestfulAdapter::SCENARIOS['USERTYPE_LIST']
            ],
            [
                'USERTYPE_FETCH_ONE',
                UserTypeRestfulAdapter::SCENARIOS['USERTYPE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $userType = MockUserTypeFactory::generateUserTypeObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullUserType())
            ->willReturn($userType);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($userType, $result);
    }

    private function prepareUserTypeTranslator(
        UserType $userType,
        array $keys,
        array $userTypeArray
    ) {
        $translator = $this->prophesize(UserTypeRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($userType),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($userTypeArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(UserType $userType)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($userType);
    }

    // private function failure()
    // {
    //     $this->stub->expects($this->exactly(1))
    //         ->method('isSuccess')
    //         ->willReturn(false);
    //     $this->stub->expects($this->exactly(0))
    //         ->method('translateToObject');
    // }

    public function testAddAction()
    {
        $userType = MockUserTypeFactory::generateUserTypeObject(1);
        $userTypeArray = array();

        $this->prepareUserTypeTranslator(
            $userType,
            array(
                "name",
                "description",
                "areaId",
                "commission",
                "subsidy",
                "status",
                "crew"
            ),
            $userTypeArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('strategyTypes', $userTypeArray);

        $this->success($userType);

        $result = $this->stub->add($userType);
        $this->assertTrue($result);
    }

    public function testEditAction()
    {
        $userType = MockUserTypeFactory::generateUserTypeObject(1);
        $data = array();

        $this->prepareUserTypeTranslator(
            $userType,
            array(
                "name",
                "description",
                "areaId",
                "commission",
                "subsidy",
                "status",
                "crew"
            ),
            $data
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('strategyTypes/'.$userType->getId(), $data);

        $this->success($userType);

        $result = $this->stub->edit($userType);
        $this->assertTrue($result);
    }

    public function testEnable()
    {
        $userType = MockUserTypeFactory::generateUserTypeObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('strategyTypes/'.$userType->getId().'/enable');

        $this->success($userType);

        $result = $this->stub->enable($userType);
        $this->assertTrue($result);
    }

    public function testDisable()
    {
        $userType = MockUserTypeFactory::generateUserTypeObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('strategyTypes/'.$userType->getId().'/disable');

        $this->success($userType);

        $result = $this->stub->disable($userType);
        $this->assertTrue($result);
    }
}
