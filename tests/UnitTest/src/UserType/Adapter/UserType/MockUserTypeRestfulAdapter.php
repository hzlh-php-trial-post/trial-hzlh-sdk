<?php
namespace Sdk\UserType\Adapter\UserType;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\UserType\Model\UserType;

class MockUserTypeRestfulAdapter extends UserTypeRestfulAdapter
{
    public function getMapError() : array
    {
        return parent::getMapError();
    }

    public function getTranslator() : IRestfulTranslator
    {
        return parent::getTranslator();
    }

    public function getResource(): string
    {
        return parent::getResource();
    }

    public function getScenario() : array
    {
        return parent::getScenario();
    }

    public function addAction(UserType $userType): bool
    {
        return parent::addAction($userType);
    }

    public function editAction(UserType $userType): bool
    {
        return parent::editAction($userType);
    }
}
