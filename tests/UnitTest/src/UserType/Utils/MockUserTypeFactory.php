<?php
namespace Sdk\UserType\Utils;

use Sdk\Crew\Model\Crew;

use Sdk\UserType\Model\UserType;

class MockUserTypeFactory
{
    public static function generateUserTypeArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $userType = array();

        $userType = array(
            'data'=>array(
                'type'=>'strategyTypes',
                'id'=>$faker->randomNumber(1)
            )
        );

        $value = array();
        $attributes = array();

        $attributes['commission'] = $faker->randomNumber();

        $attributes['subsidy'] = $faker->randomNumber();

        $attributes['name'] = $faker->word();

        $attributes['description'] = $faker->word();

        $attributes['areaId'] = $faker->randomNumber();

        $attributes['status'] = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);

        $expression['create_time'] = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);

        $expression['update_time'] = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);

        $expression['status_time'] = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);

        $userType['data']['attributes'] = $attributes;

        $userType['data']['relationships']['crew']['data'] = array(
            array(
                'type' => 'crews',
                'id' => $faker->randomNumber(1)
            )
        );

        return $userType;
    }

    public static function generateUserTypeObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : UserType {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $userType = new UserType($id);
        $userType->setId($id);

        $userType->setCommission($faker->randomNumber());

        $userType->setSubsidy($faker->randomNumber());

        $userType->setName($faker->word());

        $userType->setDescription($faker->word());

        $userType->setCreateTime($faker->unixTime());

        $userType->setUpdateTime($faker->unixTime());

        $userType->setStatusTime($faker->unixTime());

        self::generateAreaId($userType, $faker, $value);
        self::generateStatus($userType, $faker, $value);
        self::generateCrew($userType, $faker, $value);

        return $userType;
    }

    private static function generateAreaId($userType, $faker, $value) : void
    {
        $areaId = isset($value['areaId']) ? $value['areaId'] : $faker->randomNumber();

        $userType->setAreaId($areaId);
    }

    private static function generateStatus($userType, $faker, $value) : void
    {
        $status = isset($value['status'])
        ? $value['status'] : \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);

        $userType->setStatus($status);
    }

    private static function generateCrew($userType, $faker, $value) : void
    {
        unset($faker);
        $crew = isset($value['crew']) ? $value['crew'] : new Crew();

        $userType->setCrew($crew);
    }
}
