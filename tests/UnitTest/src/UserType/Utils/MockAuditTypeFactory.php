<?php
namespace Sdk\UserType\Utils;

use Sdk\Crew\Model\Crew;

use Sdk\UserType\Model\AuditType;

class MockAuditTypeFactory
{
    public static function generateAuditTypeArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $auditType = array();

        $auditType = array(
            'data'=>array(
                'type'=>'unAuditedStrategyTypes',
                'id'=>$faker->randomNumber(1)
            )
        );

        $value = array();
        $attributes = array();

        $attributes['commission'] = $faker->randomNumber();

        $attributes['subsidy'] = $faker->randomNumber();

        $attributes['name'] = $faker->word();

        $attributes['description'] = $faker->word();

        $attributes['areaId'] = $faker->randomNumber();

        $attributes['status'] = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);

        $attributes['applyStatus'] = $faker->randomNumber();

        $attributes['rejectReason'] = $faker->word();

        $expression['create_time'] = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);

        $expression['update_time'] = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);

        $expression['status_time'] = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);

        $auditType['data']['attributes'] = $attributes;

        $auditType['data']['relationships']['crew']['data'] = array(
            array(
                'type' => 'crews',
                'id' => $faker->randomNumber(1)
            )
        );

        return $auditType;
    }

    public static function generateAuditTypeObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : AuditType {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $auditType = new AuditType($id);
        $auditType->setId($id);

        $auditType->setCommission($faker->randomNumber());

        $auditType->setSubsidy($faker->randomNumber());

        $auditType->setName($faker->word());

        $auditType->setDescription($faker->word());

        $auditType->setRejectReason($faker->word());

        $auditType->setCreateTime($faker->unixTime());

        $auditType->setUpdateTime($faker->unixTime());

        $auditType->setStatusTime($faker->unixTime());

        self::generateAreaId($auditType, $faker, $value);
        self::generateApplyStatus($auditType, $faker, $value);
        self::generateStatus($auditType, $faker, $value);
        self::generateCrew($auditType, $faker, $value);

        return $auditType;
    }

    private static function generateAreaId($auditType, $faker, $value) : void
    {
        $areaId = isset($value['areaId']) ? $value['areaId'] : $faker->randomNumber();

        $auditType->setAreaId($areaId);
    }

    private static function generateApplyStatus($auditType, $faker, $value) : void
    {
        $applyStatus = isset($value['applyStatus'])
        ? $value['applyStatus'] : $faker->randomNumber();

        $auditType->setApplyStatus($applyStatus);
    }

    private static function generateStatus($auditType, $faker, $value) : void
    {
        $status = isset($value['status'])
        ? $value['status'] : \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);

        $auditType->setStatus($status);
    }

    private static function generateCrew($auditType, $faker, $value) : void
    {
        unset($faker);
        $crew = isset($value['crew']) ? $value['crew'] : new Crew();

        $auditType->setCrew($crew);
    }
}
