<?php
namespace Sdk\Withdrawal\Adapter\Withdrawal;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Withdrawal\Model\Withdrawal;
use Sdk\Withdrawal\Model\NullWithdrawal;
use Sdk\Withdrawal\Utils\MockFactory;
use Sdk\Withdrawal\Translator\WithdrawalRestfulTranslator;

class WithdrawalRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(WithdrawalRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends WithdrawalRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIWithdrawalAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Withdrawal\Adapter\Withdrawal\IWithdrawalAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('withdrawals', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Withdrawal\Translator\WithdrawalRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }

    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_WITHDRAWAL_LIST',
                WithdrawalRestfulAdapter::SCENARIOS['OA_WITHDRAWAL_LIST']
            ],
            [
                'PORTAL_WITHDRAWAL_LIST',
                WithdrawalRestfulAdapter::SCENARIOS['PORTAL_WITHDRAWAL_LIST']
            ],
            [
                'WITHDRAWAL_FETCH_ONE',
                WithdrawalRestfulAdapter::SCENARIOS['WITHDRAWAL_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $withdrawal = MockFactory::generateWithdrawalObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, NullWithdrawal::getInstance())
            ->willReturn($withdrawal);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($withdrawal, $result);
    }
    /**
     * 为WithdrawalRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$withdrawal，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareWithdrawalTranslator(
        Withdrawal $withdrawal,
        array $keys,
        array $withdrawalArray
    ) {
        $translator = $this->prophesize(WithdrawalRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($withdrawal),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($withdrawalArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Withdrawal $withdrawal)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($withdrawal);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWithdrawalTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行withdraw（）
     * 判断 result 是否为true
     */
    public function testWithdrawSuccess()
    {
        $withdrawal = MockFactory::generateWithdrawalObject(1);
        $withdrawalArray = array();

        $this->prepareWithdrawalTranslator(
            $withdrawal,
            array(
                'amount',
                'paymentPassword',
                'bankCard',
                'memberAccount'
            ),
            $withdrawalArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('withdrawals', $withdrawalArray);

        $this->success($withdrawal);

        $result = $this->stub->withdraw($withdrawal);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWithdrawalTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行withdraw（）
     * 判断 result 是否为false
     */
    public function testWithdrawFailure()
    {
        $withdrawal = MockFactory::generateWithdrawalObject(1);
        $withdrawalArray = array();

        $this->prepareWithdrawalTranslator(
            $withdrawal,
            array(
                'amount',
                'paymentPassword',
                'bankCard',
                'memberAccount'
            ),
            $withdrawalArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('withdrawals', $withdrawalArray);

        $this->failure($withdrawal);
        $result = $this->stub->withdraw($withdrawal);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWithdrawalTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行transferCompleted（）
     * 判断 result 是否为true
     */
    public function testTransferCompletedSuccess()
    {
        $withdrawal = MockFactory::generateWithdrawalObject(1);
        $withdrawalArray = array();

        $this->prepareWithdrawalTranslator(
            $withdrawal,
            array('serviceCharge', 'transferVoucher'),
            $withdrawalArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'withdrawals/'.$withdrawal->getId().'/transferCompleted',
                $withdrawalArray
            );

        $this->success($withdrawal);

        $result = $this->stub->transferCompleted($withdrawal);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareWithdrawalTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行transferCompleted（）
     * 判断 result 是否为false
     */
    public function testTransferCompletedFailure()
    {
        $withdrawal = MockFactory::generateWithdrawalObject(1);
        $withdrawalArray = array();

        $this->prepareWithdrawalTranslator(
            $withdrawal,
            array('serviceCharge', 'transferVoucher'),
            $withdrawalArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'withdrawals/'.$withdrawal->getId().'/transferCompleted',
                $withdrawalArray
            );

        $this->failure($withdrawal);
        $result = $this->stub->transferCompleted($withdrawal);
        $this->assertFalse($result);
    }
}
