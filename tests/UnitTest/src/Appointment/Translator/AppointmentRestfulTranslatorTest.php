<?php
namespace Sdk\Appointment\Translator;

use Sdk\Appointment\Model\NullAppointment;
use Sdk\Appointment\Model\Appointment;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\LoanProduct\Translator\LoanProductRestfulTranslator;
use Sdk\Snapshot\Translator\SnapshotRestfulTranslator;
use Sdk\Appointment\Model\ContactsInfo;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\Member\Model\Member;

use Sdk\Appointment\Model\AppointmentCategoryFactory;

class AppointmentRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AppointmentRestfulTranslator::class)
            ->setMethods([
                'getMemberRestfulTranslator',
                'getEnterpriseRestfulTranslator',
                'getLoanProductRestfulTranslator'
            ])->getMock();

        $this->childStub =
        new class extends AppointmentRestfulTranslator {
            public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
            {
                return parent::getEnterpriseRestfulTranslator();
            }
            public function getMemberRestfulTranslator() : MemberRestfulTranslator
            {
                return parent::getMemberRestfulTranslator();
            }
            public function getLoanProductRestfulTranslator() : LoanProductRestfulTranslator
            {
                return parent::getLoanProductRestfulTranslator();
            }
            public function getSnapshotRestfulTranslator() : SnapshotRestfulTranslator
            {
                return parent::getSnapshotRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->childStub->getEnterpriseRestfulTranslator()
        );
    }
    
    public function testGetMemberRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberRestfulTranslator',
            $this->childStub->getMemberRestfulTranslator()
        );
    }

    public function testGetLoanProductRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\LoanProduct\Translator\LoanProductRestfulTranslator',
            $this->childStub->getLoanProductRestfulTranslator()
        );
    }

    public function testGetSnapshotRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Snapshot\Translator\SnapshotRestfulTranslator',
            $this->childStub->getSnapshotRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Appointment());
        $this->assertInstanceOf('Sdk\Appointment\Model\NullAppointment', $result);
    }

    public function setMethods(Appointment $expectObject, array $attributes, array $relationships)
    {
        $contactsName = isset($attributes['contactsName'])
        ? $attributes['contactsName']
        : '';
        $contactsCellphone = isset($attributes['contactsCellphone'])
        ? $attributes['contactsCellphone']
        : '';
        $contactsArea = isset($attributes['contactsArea'])
        ? $attributes['contactsArea']
        : '';
        $contactsAddress = isset($attributes['contactsAddress'])
        ? $attributes['contactsAddress']
        : '';
        $expectObject->setContactsInfo(
            new ContactsInfo(
                $contactsName,
                $contactsCellphone,
                $contactsArea,
                $contactsAddress
            )
        );
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['score'])) {
            $expectObject->setScore($attributes['score']);
        }
        if (isset($attributes['loanObject'])) {
            $expectObject->setLoanObject($attributes['loanObject']);
        }
        if (isset($attributes['loanProductTitle'])) {
            $expectObject->setLoanProductTitle($attributes['loanProductTitle']);
        }
        if (isset($attributes['enterpriseName'])) {
            $expectObject->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['borrowerName'])) {
            $expectObject->setBorrowerName($attributes['borrowerName']);
        }
        if (isset($attributes['loanAmount'])) {
            $expectObject->setLoanAmount($attributes['loanAmount']);
        }
        if (isset($attributes['loanTerm'])) {
            $expectObject->setLoanTerm($attributes['loanTerm']);
        }
        if (isset($attributes['loanTermUnit'])) {
            $expectObject->setLoanTermUnit($attributes['loanTermUnit']);
        }
        if (isset($attributes['loanPurposeDescribe'])) {
            $expectObject->setLoanPurposeDescribe($attributes['loanPurposeDescribe']);
        }
        if (isset($attributes['attachments'])) {
            $expectObject->setAttachments($attributes['attachments']);
        }
        if (isset($attributes['creditReports'])) {
            $expectObject->setCreditReports($attributes['creditReports']);
        }
        if (isset($attributes['authorizedReports'])) {
            $expectObject->setAuthorizedReports($attributes['authorizedReports']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['rejectReasonDescribe'])) {
            $expectObject->setRejectReasonDescribe($attributes['rejectReasonDescribe']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['sellerEnterprise']['data'])) {
            $expectObject->setSellerEnterprise(new Enterprise($relationships['sellerEnterprise']['data']['id']));
        }
        if (isset($relationships['member']['data'])) {
            $expectObject->setMember(new Member($relationships['member']['data']['id']));
        }
        if (isset($relationships['loanProduct']['data'])) {
            $expectObject->setLoanProduct(new LoanProduct($relationships['loanProduct']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $appointment = \Sdk\Appointment\Utils\MockFactory::generateAppointmentArray();

        $data =  $appointment['data'];
        $relationships = $data['relationships'];

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $sellerEnterprise = new Enterprise($relationships['sellerEnterprise']['data']['id']);
        $sellerEnterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $sellerEnterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['sellerEnterprise']))
            ->shouldBeCalledTimes(1)->willReturn($sellerEnterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($sellerEnterpriseRestfulTranslator->reveal());

        $loanProduct = new LoanProduct($relationships['loanProduct']['data']['id']);
        $loanProductRestfulTranslator = $this->prophesize(LoanProductRestfulTranslator::class);
        $loanProductRestfulTranslator->arrayToObject(Argument::exact($relationships['loanProduct']))
            ->shouldBeCalledTimes(1)->willReturn($loanProduct);

        $this->stub->expects($this->exactly(1))
            ->method('getLoanProductRestfulTranslator')
            ->willReturn($loanProductRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($appointment);

        $expectObject = new Appointment();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $appointment = \Sdk\Appointment\Utils\MockFactory::generateAppointmentArray();
        $data =  $appointment['data'];
        $relationships = $data['relationships'];

        $sellerEnterprise = new Enterprise($relationships['sellerEnterprise']['data']['id']);
        $sellerEnterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $sellerEnterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['sellerEnterprise']))
            ->shouldBeCalledTimes(1)->willReturn($sellerEnterprise);

        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($sellerEnterpriseRestfulTranslator->reveal());

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $loanProduct = new LoanProduct($relationships['loanProduct']['data']['id']);
        $loanProductRestfulTranslator = $this->prophesize(LoanProductRestfulTranslator::class);
        $loanProductRestfulTranslator->arrayToObject(Argument::exact($relationships['loanProduct']))
            ->shouldBeCalledTimes(1)->willReturn($loanProduct);

        $this->stub->expects($this->exactly(1))
            ->method('getLoanProductRestfulTranslator')
            ->willReturn($loanProductRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($appointment);
        $expectArray = array();

        $expectObject = new Appointment();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $appointment = \Sdk\Appointment\Utils\MockFactory::generateAppointmentObject(1, 1);

        $actual = $this->stub->objectToArray(
            $appointment,
            array(
                'loanProductSnapshot',
                'member',
                'loanObject',
                'loanAmount',
                'loanTerm',
                'guarantyStyle',
                'guarantyStyleType',
                'loanPurpose',
                'loanPurposeDescribe',
                'attachments',
                'creditReports',
                'authorizedReports',
                'rejectReason',
                'rejectReasonDescribe',
                'loanResultInfo',
                'transactionInfo',
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'appointments'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'loanObject' => $appointment->getLoanObject(),
            'loanAmount' => $appointment->getLoanAmount(),
            'loanTerm' => $appointment->getLoanTerm(),
            'loanPurposeDescribe' => $appointment->getLoanPurposeDescribe(),
            'attachments' => $appointment->getAttachments(),
            'creditReports' => $appointment->getCreditReports(),
            'authorizedReports' => $appointment->getAuthorizedReports(),
            'rejectReasonDescribe' => $appointment->getRejectReasonDescribe(),
            'contactsName' => $appointment->getContactsInfo()->getName(),
            'contactsCellphone' => $appointment->getContactsInfo()->getCellphone(),
            'contactsArea' => $appointment->getContactsInfo()->getArea(),
            'contactsAddress' => $appointment->getContactsInfo()->getAddress()
        );

        $expectedArray['data']['relationships']['member']['data'] = array(
            array(
                'type' => 'members',
                'id' => $appointment->getMember()->getId()
            )
        );
        $expectedArray['data']['relationships']['snapshot']['data'] = array(
            array(
                'type'=>'snapshots',
                'id'=>$appointment->getLoanProductSnapshot()->getId()
            )
        );
        $expectedArray['data']['relationships']['guarantyStyle']['data'] = array(
            array(
                'type'=>'dictionaries',
                'id'=>$appointment->getGuarantyStyle()->getId()
            )
        );
        $expectedArray['data']['relationships']['guarantyStyleType']['data'] = array(
            array(
                'type'=>'dictionaries',
                'id'=>$appointment->getGuarantyStyleType()->getId()
            )
        );
        $expectedArray['data']['relationships']['loanPurpose']['data'] = array(
            array(
                'type'=>'dictionaries',
                'id'=>$appointment->getLoanPurpose()->getId()
            )
        );
        $expectedArray['data']['relationships']['rejectReason']['data'] = array(
            array(
                'type'=>'dictionaries',
                'id'=>$appointment->getRejectReason()->getId()
            )
        );
        $expectedArray['data']['relationships']['loanResultInfo']['data'] = array(
            'type' => 'loanResultInfos',
            'attributes' => array
                (
                    'status' => 0,
                    'repaymentMethod' => 1,
                    'loanAmount' => 0,
                    'loanTerm' => 0,
                    'loanTermUnit' => 1,
                    'loanFailReason' => ''
                )
        );
        $expectedArray['data']['relationships']['transactionInfo']['data'] = array(
            'type' => 'transactionInfos',
            'attributes' => array
                (
                    'creditMinLine' => 0,
                    'creditMaxLine' => 0,
                    'creditValidityStartTime' => 0,
                    'creditValidityEndTime' => 0,
                    'transactionTime' => '',
                    'transactionArea' => '',
                    'transactionAddress' => '',
                    'contactName' => '',
                    'contactPhone' => '',
                    'remark' => '',
                    'carryData' => []
                )
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
