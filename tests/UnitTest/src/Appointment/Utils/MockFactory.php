<?php
namespace Sdk\Appointment\Utils;

use Sdk\Appointment\Model\Appointment;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Snapshot\Model\Snapshot;
use Sdk\LoanProduct\Model\LoanProduct;
use Sdk\Dictionary\Model\Dictionary;

use Sdk\Common\Model\IApplyAble;

class MockFactory
{
    /**
     * [generateAppointmentArray 生成产品申请信息数组]
     * @return [array] [产品申请数组]
     */
    public static function generateAppointmentArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $appointment = array();

        $appointment = array(
            'data'=>array(
                'type'=>'appointments',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //number
        $attributes['number'] = self::generateNumber($faker, $value);
        //score
        $attributes['score'] = self::generateScore($faker, $value);
        //loanProductTitle
        $attributes['loanProductTitle'] = self::generateLoanProductTitle($faker, $value);
        //enterpriseName
        $attributes['enterpriseName'] = self::generateEnterpriseName($faker, $value);
        //borrowerName
        $attributes['borrowerName'] = self::generateBorrowerName($faker, $value);
        //loanObject
        $attributes['loanObject'] = self::generateLoanObject($faker, $value);
        //loanAmount
        $attributes['loanAmount'] = self::generateLoanAmount($faker, $value);
        //loanTerm
        $attributes['loanTerm'] = self::generateLoanTerm($faker, $value);
        //loanTermUnit
        $attributes['loanTermUnit'] = self::generateLoanTermUnit($faker, $value);
        //loanPurposeDescribe
        $attributes['loanPurposeDescribe'] = self::generateLoanPurposeDescribe($faker, $value);
        //attachments
        $attributes['attachments'] = self::generateAttachments($faker, $value);
        //creditReports
        $attributes['creditReports'] = self::generateCreditReports($faker, $value);
        //authorizedReports
        $attributes['authorizedReports'] = self::generateAuthorizedReports($faker, $value);
        //rejectReasonDescribe
        $attributes['rejectReasonDescribe'] = self::generateRejectReasonDescribe($faker, $value);
        //contactsName
        $attributes['contactsName'] = self::generateContactsName($faker, $value);
        //contactsCellphone
        $attributes['contactsCellphone'] = self::generateContactsCellphone($faker, $value);
        //contactsArea
        $attributes['contactsArea'] = self::generateContactsArea($faker, $value);
        //contactsAddress
        $attributes['contactsAddress'] = self::generateContactsAddress($faker, $value);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $appointment['data']['attributes'] = $attributes;

        //loanProductSnapshot
        // $appointment['data']['relationships']['loanProductSnapshot']['data'] = array(
        //     'type' => 'snapshots',
        //     'id' => $faker->randomNumber(1)
        // );
        //loanProduct
        $appointment['data']['relationships']['loanProduct']['data'] = array(
            'type' => 'loanProducts',
            'id' => $faker->randomNumber(1)
        );
        //member
        $appointment['data']['relationships']['member']['data'] = array(
            'type' => 'members',
            'id' => $faker->randomNumber(1)
        );
        //sellerEnterprise
        $appointment['data']['relationships']['sellerEnterprise']['data'] = array(
            'type' => 'enterprises',
            'id' => $faker->randomNumber(1)
        );
        //applicant
        // $appointment['data']['relationships']['applicant']['data'] = array(
        //     'type' => 'enterprises',
        //     'id' => $faker->randomNumber(1)
        // );
        //guarantyStyle
        // $appointment['data']['relationships']['guarantyStyle']['data'] = array(
        //     'type' => 'dictionaries',
        //     'id' => $faker->randomNumber(1)
        // );
        // //guarantyStyleType
        // $appointment['data']['relationships']['guarantyStyleType']['data'] = array(
        //     'type' => 'dictionaries',
        //     'id' => $faker->randomNumber(1)
        // );
        // //loanPurpose
        // $appointment['data']['relationships']['loanPurpose']['data'] = array(
        //     'type' => 'dictionaries',
        //     'id' => $faker->randomNumber(1)
        // );
        // //rejectReason
        // $appointment['data']['relationships']['rejectReason']['data'] = array(
        //     'type' => 'dictionaries',
        //     'id' => $faker->randomNumber(1)
        // );
        // //transactionInfo
        // $appointment['data']['relationships']['transactionInfo']['data'] = array(
        //     'type' => 'transactionInfos',
        //     'id' => $faker->randomNumber(1)
        // );
        // //loanResultInfo
        // $appointment['data']['relationships']['loanResultInfo']['data'] = array(
        //     'type' => 'loanResultInfos',
        //     'id' => $faker->randomNumber(1)
        // );

        return $appointment;
    }

    /**
     * [generateAppointmentObject 生成产品申请对象]
     * @param  int|integer $id    [产品申请Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [产品申请对象]
     */
    public static function generateAppointmentObject(int $id = 0, int $seed = 0, array $value = array()) : Appointment
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $appointment = new Appointment($id);

        //enterprise
        $enterprise = self::generateEnterprise($faker, $value);
        $appointment->setSellerEnterprise($enterprise);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $appointment->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $appointment->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $appointment->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $appointment->setStatus($status);

        return $appointment;
    }

    private static function generateApplyStatus($faker, array $value = array())
    {
        return $applyStatus = isset($value['applyStatus']) ?
            $value['applyStatus'] : 0;
    }

    private static function generateEnterprise($faker, array $value = array())
    {
        unset($faker);

        return $enterprise = isset($value['sellerEnterprise']) ?
            $value['sellerEnterprise'] : \Sdk\Enterprise\Utils\EnterpriseMockFactory::generateEnterpriseObject(
                new Enterprise(),
                0,
                0
            );
    }

    private static function generateNumber($faker, array $value = array())
    {
        return isset($value['number']) ?
        $value['number'] : $faker->creditCardNumber;
    }

    private static function generateScore($faker, array $value = array())
    {
        return isset($value['score']) ?
        $value['score'] : 70;
    }

    private static function generateLoanProductTitle($faker, array $value = array())
    {
        return isset($value['loanProductTitle']) ?
        $value['loanProductTitle'] : $faker->word();
    }
    private static function generateEnterpriseName($faker, array $value = array())
    {
        return isset($value['enterpriseName']) ?
        $value['enterpriseName'] : $faker->company();
    }

    private static function generateBorrowerName($faker, array $value = array())
    {
        return isset($value['borrowerName']) ?
        $value['borrowerName'] : $faker->name();
    }

    private static function generateLoanObject($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanObject']) ?
        $value['loanObject'] : 1;
    }

    private static function generateLoanTerm($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanTerm']) ?
        $value['loanTerm'] : 1;
    }

    private static function generateLoanTermUnit($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanTermUnit']) ?
        $value['loanTermUnit'] : 1;
    }

    private static function generateLoanPurposeDescribe($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanPurposeDescribe']) ?
        $value['loanPurposeDescribe'] : 'hahahahahahah';
    }

    private static function generateAttachments($faker, array $value = array())
    {
        return isset($value['attachments']) ?
        $value['attachments'] : array(
                        [
                            "index" => "0",
                            "attachments" => [
                                [
                                    "name" => "附件名称",
                                    "identify" => "附件地址.jpg"
                                ]
                            ]
                        ],
                        [
                            "index" => "1",
                            "attachments" => [
                                [
                                    "name" => "附件名称",
                                    "identify" => "附件地址.jpg"
                                ]
                            ]
                        ]
                    );
    }

    private static function generateCreditReports($faker, array $value = array())
    {
        return isset($value['creditReports']) ?
        $value['creditReports'] : array(1, 2);
    }

    private static function generateAuthorizedReports($faker, array $value = array())
    {
        return isset($value['authorizedReports']) ?
        $value['authorizedReports'] : [];
    }

    private static function generateRejectReasonDescribe($faker, array $value = array())
    {
        return isset($value['rejectReasonDescribe']) ?
        $value['rejectReasonDescribe'] : '描述描述描述';
    }

    private static function generateLoanStatus($faker, array $value = array())
    {
        unset($faker);
        return isset($value['loanStatus']) ?
        $value['loanStatus'] : 2;
    }

    private static function generateLoanContractNumber($faker, array $value = array())
    {
        return isset($value['loanContractNumber']) ?
        $value['loanContractNumber'] : $faker->creditCardNumber();
    }

    private static function generateLoanFailReason($faker, array $value = array())
    {
        return isset($value['loanFailReason']) ?
        $value['loanFailReason'] : $faker->word();
    }

    private static function generateContactsName($faker, array $value = array())
    {
        return isset($value['contactsName']) ?
            $value['contactsName'] : $faker->name();
    }

    private static function generateContactsCellphone($faker, array $value = array())
    {
        return isset($value['contactsCellphone']) ?
            $value['contactsCellphone'] : $faker->phoneNumber();
    }

    private static function generateContactsArea($faker, array $value = array())
    {
        return isset($value['contactsArea']) ?
            $value['contactsArea'] : $faker->area();
    }

    private static function generateContactsAddress($faker, array $value = array())
    {
        return isset($value['contactsAddress']) ?
            $value['contactsAddress'] : $faker->address();
    }

    private static function generateRemark($faker, array $value = array())
    {
        return isset($value['remark']) ?
        $value['remark'] : $faker->word();
    }

    private static function generateLoanAmount($faker, array $value = array())
    {
        return isset($value['loanAmount']) ?
            $value['loanAmount'] : $faker->randomDigit();
    }

    private static function generateRejectReason($faker, array $value = array())
    {
        return isset($value['rejectReason']) ?
        $value['rejectReason'] : $faker->word();
    }

    private static function generateLoanProduct($faker, array $value = array())
    {
        return isset($value['loanProduct']) ?
            $value['loanProduct'] : \LoanProduct\Utils\MockFactory::generateLoanProductObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generateMember($faker, array $value = array())
    {
        unset($faker);

        return $member = isset($value['member']) ?
            $value['member'] : \Sdk\Member\Utils\MockFactory::generateMemberObject(
                new Member(),
                0,
                0
            );
    }
}
