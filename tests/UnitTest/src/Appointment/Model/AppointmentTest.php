<?php
namespace Sdk\Appointment\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Appointment\Repository\AppointmentRepository;

class AppointmentTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Appointment::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends Appointment{
            public function getRepository() : AppointmentRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Appointment\Repository\AppointmentRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Appointment setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //loanObject 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setLoanObject() 是否符合预定范围
     * @dataProvider loanObjectProvider
     */
    public function testSetLoanObject($actual, $expected)
    {
        $this->stub->setLoanObject($actual);
        $this->assertEquals($expected, $this->stub->getLoanObject());
    }
    /**
     * 循环测试  setLoanObject() 数据构建器
     */
    public function loanObjectProvider()
    {
        return array(
            array(Appointment::LOAN_OBJECT['ENTERPRISE'], Appointment::LOAN_OBJECT['ENTERPRISE']),
            array(Appointment::LOAN_OBJECT['NATURAL_PERSON'], Appointment::LOAN_OBJECT['NATURAL_PERSON'])
        );
    }
    /**
     * 设置 Appointment setLoanObject() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetLoanObjectWrongType()
    {
        $this->stub->setLoanObject('string');
    }
    //loanObject 测试 ------------------------------------------------------   end

    //number 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('string');
        $this->assertEquals('string', $this->stub->getNumber());
    }
    /**
     * 设置 Appointment setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array(3));
    }
    //number 测试 ----------------------------------------------------------   end

    //score 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setScore() 正确的传参类型,期望传值正确
     */
    public function testSetScoreCorrectType()
    {
        $this->stub->setScore(1);
        $this->assertEquals(1, $this->stub->getScore());
    }
    /**
     * 设置 Appointment setScore() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetScoreWrongType()
    {
        $this->stub->setScore('string');
    }
    //score 测试 ----------------------------------------------------------   end

    //loanProductSnapshot 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setLoanProductSnapshot() 正确的传参类型,期望传值正确
     */
    public function testSetLoanProductSnapshotCorrectType()
    {
        $object = new \Sdk\Snapshot\Model\Snapshot();

        $this->stub->setLoanProductSnapshot($object);
        $this->assertEquals($object, $this->stub->getLoanProductSnapshot());
    }
    /**
     * 设置 Appointment setLoanProductSnapshot() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanProductSnapshotWrongType()
    {
        $this->stub->setLoanProductSnapshot('string');
    }
    //loanProductSnapshot 测试 ----------------------------------------------------------   end

    //loanProduct 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setLoanProduct() 正确的传参类型,期望传值正确
     */
    public function testSetLoanProductCorrectType()
    {
        $object = new \Sdk\LoanProduct\Model\LoanProduct();

        $this->stub->setLoanProduct($object);
        $this->assertEquals($object, $this->stub->getLoanProduct());
    }
    /**
     * 设置 Appointment setLoanProduct() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanProductWrongType()
    {
        $this->stub->setLoanProduct('string');
    }
    //loanProduct 测试 ----------------------------------------------------------   end

    //loanTermUnit 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setTypeMode() 是否符合预定范围
     * @dataProvider loanTermUnitProvider
     */
    public function testSetLoanTermUnit($actual, $expected)
    {
        $this->stub->setLoanTermUnit($actual);
        $this->assertEquals($expected, $this->stub->getLoanTermUnit());
    }
    /**
     * 循环测试 DispatchDepartment setReceivingMode() 数据构建器
     */
    public function loanTermUnitProvider()
    {
        return array(
            array(Appointment::LOAN_TERM_UNIT['MONTH'], Appointment::LOAN_TERM_UNIT['MONTH']),
            array(Appointment::LOAN_TERM_UNIT['DAY'], Appointment::LOAN_TERM_UNIT['DAY'])
        );
    }
    /**
     * 设置 Appointment setCardType() 错误的传参类型,期望期望抛出TypeError exception
     * @expectedException TypeError
     */
    public function testSetLoanTermUnitWrongType()
    {
        $this->stub->setLoanTermUnit('string');
    }
    //loanTermUnit 测试 ------------------------------------------------------   end

     //loanProductTitle 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setLoanProductTitle() 正确的传参类型,期望传值正确
     */
    public function testSetLoanProductTitleCorrectType()
    {
        $this->stub->setLoanProductTitle('string');
        $this->assertEquals('string', $this->stub->getLoanProductTitle());
    }

    /**
     * 设置 Appointment setLoanProductTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanProductTitleWrongType()
    {
        $this->stub->setLoanProductTitle(array(1, 2, 3));
    }
    //loanProductTitle 测试 ----------------------------------------------------------   end

    //enterpriseName 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setEnterpriseName() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseNameCorrectType()
    {
        $this->stub->setEnterpriseName('string');
        $this->assertEquals('string', $this->stub->getEnterpriseName());
    }

    /**
     * 设置 Service setEnterpriseName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseNameWrongType()
    {
        $this->stub->setEnterpriseName(array(1, 2, 3));
    }
    //enterpriseName 测试 ----------------------------------------------------------   end

    //member 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $object = new \Sdk\Member\Model\Member();

        $this->stub->setMember($object);
        $this->assertEquals($object, $this->stub->getMember());
    }
    /**
     * 设置 Appointment setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->stub->setMember('string');
    }
    //member 测试 ----------------------------------------------------------   end

    //sellerEnterprise 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setSellerEnterprise() 正确的传参类型,期望传值正确
     */
    public function testSetSellerEnterpriseCorrectType()
    {
        $object = new \Sdk\Enterprise\Model\Enterprise();

        $this->stub->setSellerEnterprise($object);
        $this->assertEquals($object, $this->stub->getSellerEnterprise());
    }
    /**
     * 设置 Appointment setSellerEnterprise() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSellerEnterpriseWrongType()
    {
        $this->stub->setSellerEnterprise('string');
    }
    //sellerEnterprise 测试 ----------------------------------------------------------   end

    //borrowerName 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setBorrowerName() 正确的传参类型,期望传值正确
     */
    public function testSetBorrowerNameCorrectType()
    {
        $this->stub->setBorrowerName('string');
        $this->assertEquals('string', $this->stub->getBorrowerName());
    }

    /**
     * 设置 Service setBorrowerName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBorrowerNameWrongType()
    {
        $this->stub->setBorrowerName(array(1, 2, 3));
    }
    //borrowerName 测试 ----------------------------------------------------------   end

    //applicant 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setApplicant() 正确的传参类型,期望传值正确
     */
    public function testApplicantiseCorrectType()
    {
        $object = new \Sdk\Enterprise\Model\Enterprise();

        $this->stub->setApplicant($object);
        $this->assertEquals($object, $this->stub->getApplicant());
    }
    /**
     * 设置 Appointment setApplicant() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplicantWrongType()
    {
        $this->stub->setApplicant('string');
    }
    //applicant 测试 ----------------------------------------------------------   end

    //guarantyStyle 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setGuarantyStyle() 正确的传参类型,期望传值正确
     */
    public function testGuarantyStyleiseCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setGuarantyStyle($object);
        $this->assertEquals($object, $this->stub->getGuarantyStyle());
    }
    /**
     * 设置 Appointment setGuarantyStyle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGuarantyStyleWrongType()
    {
        $this->stub->setGuarantyStyle('string');
    }
    //guarantyStyle 测试 ----------------------------------------------------------   end

    //guarantyStyleType 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setGuarantyStyleType() 正确的传参类型,期望传值正确
     */
    public function testGuarantyStyleTypeiseCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setGuarantyStyleType($object);
        $this->assertEquals($object, $this->stub->getGuarantyStyleType());
    }
    /**
     * 设置 Appointment setGuarantyStyleType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGuarantyStyleTypeWrongType()
    {
        $this->stub->setGuarantyStyleType('string');
    }
    //guarantyStyleType 测试 ----------------------------------------------------------   end

    //loanPurpose 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setLoanPurpose() 正确的传参类型,期望传值正确
     */
    public function testLoanPurposeiseCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setLoanPurpose($object);
        $this->assertEquals($object, $this->stub->getLoanPurpose());
    }
    /**
     * 设置 Appointment setLoanPurpose() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanPurposeWrongType()
    {
        $this->stub->setLoanPurpose('string');
    }
    //loanPurpose 测试 ----------------------------------------------------------   end

    //loanPurposeDescribe 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setLoanPurposeDescribe() 正确的传参类型,期望传值正确
     */
    public function testLoanPurposeDescribeiseCorrectType()
    {
        $this->stub->setLoanPurposeDescribe('string');
        $this->assertEquals('string', $this->stub->getLoanPurposeDescribe());
    }
    /**
     * 设置 Appointment setLoanPurposeDescribe() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanPurposeDescribeWrongType()
    {
        $this->stub->setLoanPurposeDescribe(array(1, 2, 3));
    }
    //loanPurposeDescribe 测试 ----------------------------------------------------------   end

    //attachments 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setAttachments() 正确的传参类型,期望传值正确
     */
    public function testAttachmentsiseCorrectType()
    {
        $this->stub->setAttachments(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getAttachments());
    }
    /**
     * 设置 Appointment setAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAttachmentsWrongType()
    {
        $this->stub->setAttachments('string');
    }
    //attachments 测试 ----------------------------------------------------------   end

    //creditReports 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setCreditReports() 正确的传参类型,期望传值正确
     */
    public function testCreditReportsiseCorrectType()
    {
        $this->stub->setCreditReports(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getCreditReports());
    }
    /**
     * 设置 Appointment setCreditReports() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditReportsWrongType()
    {
        $this->stub->setCreditReports('string');
    }
    //creditReports 测试 ----------------------------------------------------------   end

    //authorizedReports 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setAuthorizedReports() 正确的传参类型,期望传值正确
     */
    public function testAuthorizedReportsiseCorrectType()
    {
        $this->stub->setAuthorizedReports(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getAuthorizedReports());
    }
    /**
     * 设置 Appointment setAuthorizedReports() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAuthorizedReportsWrongType()
    {
        $this->stub->setAuthorizedReports('string');
    }
    //authorizedReports 测试 ----------------------------------------------------------   end

    //contactsInfo 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setContactsInfo() 正确的传参类型,期望传值正确
     */
    public function testContactsInfoiseCorrectType()
    {
        $object = new ContactsInfo();

        $this->stub->setContactsInfo($object);
        $this->assertEquals($object, $this->stub->getContactsInfo());
    }
    /**
     * 设置 Appointment setContactsInfo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContactsInfoWrongType()
    {
        $this->stub->setContactsInfo('string');
    }
    //contactsInfo 测试 ----------------------------------------------------------   end

    //transactionInfo 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setTransactionInfo() 正确的传参类型,期望传值正确
     */
    public function testTransactionInfoiseCorrectType()
    {
        $object = new TransactionInfo();

        $this->stub->setTransactionInfo($object);
        $this->assertEquals($object, $this->stub->getTransactionInfo());
    }
    /**
     * 设置 Appointment setTransactionInfo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransactionInfoWrongType()
    {
        $this->stub->setTransactionInfo('string');
    }
    //transactionInfo 测试 ----------------------------------------------------------   end

    //loanResultInfo 测试 ---------------------------------------------------------- start
    /**
     * 设置 Appointment setLoanResultInfo() 正确的传参类型,期望传值正确
     */
    public function testLoanResultInfoiseCorrectType()
    {
        $object = new LoanResultInfo();

        $this->stub->setLoanResultInfo($object);
        $this->assertEquals($object, $this->stub->getLoanResultInfo());
    }
    /**
     * 设置 Appointment setLoanResultInfo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanResultInfoWrongType()
    {
        $this->stub->setLoanResultInfo('string');
    }
    //loanResultInfo 测试 ----------------------------------------------------------   end

    //rejectReason 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setRejectReason($object);
        $this->assertEquals($object, $this->stub->getRejectReason());
    }

    /**
     * 设置 Service setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->stub->setRejectReason(array(1, 2, 3));
    }
    //rejectReason 测试 ----------------------------------------------------------   end

    //rejectReasonDescribe 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setRejectReasonDescribe() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonDescribeCorrectType()
    {
        $this->stub->setRejectReasonDescribe('string');
        $this->assertEquals('string', $this->stub->getRejectReasonDescribe());
    }

    /**
     * 设置 Service setRejectReasonDescribe() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonDescribeWrongType()
    {
        $this->stub->setRejectReasonDescribe(array(1, 2, 3));
    }
    //rejectReasonDescribe 测试 ----------------------------------------------------------   end

    //loanAmount 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setLoanAmount() 正确的传参类型,期望传值正确
     */
    public function testSetLoanAmountCorrectType()
    {
        $this->stub->setLoanAmount('0.00');
        $this->assertEquals('0.00', $this->stub->getLoanAmount());
    }

    /**
     * 设置 Service setLoanAmount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanAmountWrongType()
    {
        $this->stub->setLoanAmount(array(1, 2, 3));
    }
    //loanAmount 测试 ----------------------------------------------------------   end

    //loanTerm 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setLoanTerm() 正确的传参类型,期望传值正确
     */
    public function testSetLoanTermCorrectType()
    {
        $this->stub->setLoanTerm(1);
        $this->assertEquals(1, $this->stub->getLoanTerm());
    }

    /**
     * 设置 Service setLoanTerm() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLoanTermWrongType()
    {
        $this->stub->setLoanTerm('string');
    }
    //loanTerm 测试 ----------------------------------------------------------   end

    //status 测试 ---------------------------------------------------------- start
    /**
     * 设置 Service setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(1);
        $this->assertEquals(1, $this->stub->getStatus());
    }

    /**
     * 设置 Service setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('string');
    }
    //status 测试 ----------------------------------------------------------   end
}
