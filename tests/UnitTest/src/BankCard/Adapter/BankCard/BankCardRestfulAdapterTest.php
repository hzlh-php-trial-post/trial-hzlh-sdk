<?php
namespace Sdk\BankCard\Adapter\BankCard;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\BankCard\Model\BankCard;
use Sdk\BankCard\Model\NullBankCard;
use Sdk\BankCard\Utils\MockFactory;
use Sdk\BankCard\Translator\BankCardRestfulTranslator;

class BankCardRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(BankCardRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends BankCardRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIBankCardAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\BankCard\Adapter\BankCard\IBankCardAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('bankCards', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\BankCard\Translator\BankCardRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'PORTAL_BANK_CARD_LIST',
                BankCardRestfulAdapter::SCENARIOS['PORTAL_BANK_CARD_LIST']
            ],
            [
                'BANK_CARD_FETCH_ONE',
                BankCardRestfulAdapter::SCENARIOS['BANK_CARD_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $bankCard = MockFactory::generateBankCardObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullBankCard())
            ->willReturn($bankCard);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($bankCard, $result);
    }
    /**
     * 为BankCardRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$bankCard，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareBankCardTranslator(
        BankCard $bankCard,
        array $keys,
        array $bankCardArray
    ) {
        $translator = $this->prophesize(BankCardRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($bankCard),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($bankCardArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(BankCard $bankCard)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($bankCard);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBankCardTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行bind()）
     * 判断 result 是否为true
     */
    public function testBindSuccess()
    {
        $bankCard = MockFactory::generateBankCardObject(1);
        $bankCardArray = array();

        $this->prepareBankCardTranslator(
            $bankCard,
            array(
                'cardholderName',
                'cardNumber',
                'bankBranchArea',
                'bankBranchAddress',
                'cellphone',
                'licence',
                'accountType',
                'cardType',
                'memberAccount',
                'paymentPassword',
                'bank',
            ),
            $bankCardArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('bankCards', $bankCardArray);

        $this->success($bankCard);

        $result = $this->stub->bind($bankCard);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBankCardTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行bind()
     * 判断 result 是否为false
     */
    public function testBindFailure()
    {
        $bankCard = MockFactory::generateBankCardObject(1);
        $bankCardArray = array();

        $this->prepareBankCardTranslator(
            $bankCard,
            array(
                'cardholderName',
                'cardNumber',
                'bankBranchArea',
                'bankBranchAddress',
                'cellphone',
                'licence',
                'accountType',
                'cardType',
                'memberAccount',
                'paymentPassword',
                'bank',
            ),
            $bankCardArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('bankCards', $bankCardArray);

        $this->failure($bankCard);
        $result = $this->stub->bind($bankCard);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBankCardTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行unBind
     * 判断 result 是否为true
     */
    public function testUnBindSuccess()
    {
        $bankCard = MockFactory::generateBankCardObject(1);
        $bankCardArray = array();

        $this->prepareBankCardTranslator(
            $bankCard,
            array('unbindReason','paymentPassword'),
            $bankCardArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'bankCards/'.$bankCard->getId().'/unBind',
                $bankCardArray
            );

        $this->success($bankCard);

        $result = $this->stub->unBind($bankCard);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareBankCardTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行unBind（）
     * 判断 result 是否为false
     */
    public function testUnBindFailure()
    {
        $bankCard = MockFactory::generateBankCardObject(1);
        $bankCardArray = array();

        $this->prepareBankCardTranslator(
            $bankCard,
            array('unbindReason','paymentPassword'),
            $bankCardArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'bankCards/'.$bankCard->getId().'/unBind',
                $bankCardArray
            );

        $this->failure($bankCard);
        $result = $this->stub->unBind($bankCard);
        $this->assertFalse($result);
    }
}
