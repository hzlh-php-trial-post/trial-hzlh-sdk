<?php
namespace Sdk\BankCard\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullBankCardTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullBankCard::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsBankCard()
    {
        $this->assertInstanceof('Sdk\BankCard\Model\BankCard', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testBind()
    {
        $result = $this->stub->bind();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testUnBind()
    {
        $result = $this->stub->unBind();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
