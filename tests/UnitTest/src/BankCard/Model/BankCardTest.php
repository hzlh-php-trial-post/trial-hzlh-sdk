<?php
namespace Sdk\BankCard\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Bank\Model\Bank;
use Sdk\Bank\Model\NullBank;

use Sdk\MemberAccount\Model\MemberAccount;

use Sdk\BankCard\Repository\BankCardRepository;

class BankCardTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(BankCard::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends BankCard{
            public function getRepository() : BankCardRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\BankCard\Repository\BankCardRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 BankCard setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 BankCard setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //accountType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setAccountType() 是否符合预定范围
     *
     * @dataProvider accountTypeProvider
     */
    public function testSetAccountType($actual, $expected)
    {
        $this->stub->setAccountType($actual);
        $this->assertEquals($expected, $this->stub->getAccountType());
    }
    /**
     * 循环测试 DispatchDepartment setAccountType() 数据构建器
     */
    public function accountTypeProvider()
    {
        return array(
            array(BankCard::ACCOUNT_TYPE['PUBLIC'], BankCard::ACCOUNT_TYPE['PUBLIC']),
            array(BankCard::ACCOUNT_TYPE['PRIVATE'], BankCard::ACCOUNT_TYPE['PRIVATE'])
        );
    }
    /**
     * 设置 BankCard setAccountType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAccountTypeWrongType()
    {
        $this->stub->setAccountType('string');
    }
    //accountType 测试 ------------------------------------------------------   end
    
    //cardType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setCardType() 是否符合预定范围
     *
     * @dataProvider cardTypeProvider
     */
    public function testSetCardType($actual, $expected)
    {
        $this->stub->setCardType($actual);
        $this->assertEquals($expected, $this->stub->getCardType());
    }
    /**
     * 循环测试 DispatchDepartment setCardType() 数据构建器
     */
    public function cardTypeProvider()
    {
        return array(
            array(BankCard::CARD_TYPE['DEBIT_CARD'], BankCard::CARD_TYPE['DEBIT_CARD']),
            array(BankCard::CARD_TYPE['CREDIT_CARD'], BankCard::CARD_TYPE['CREDIT_CARD'])
        );
    }
    /**
     * 设置 BankCard setCardType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCardTypeWrongType()
    {
        $this->stub->setCardType('string');
    }
    //cardType 测试 ------------------------------------------------------   end
    
    //cardNumber 测试 ---------------------------------------------------------- start
    /**
     * 设置 BankCard setCardNumber() 正确的传参类型,期望传值正确
     */
    public function testSetCardNumberCorrectType()
    {
        $this->stub->setCardNumber('string');
        $this->assertEquals('string', $this->stub->getCardNumber());
    }

    /**
     * 设置 BankCard setCardNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCardNumberWrongType()
    {
        $this->stub->setCardNumber(array(1, 2, 3));
    }
    //cardNumber 测试 ----------------------------------------------------------   end
    
    //bankBranchArea 测试 ---------------------------------------------------------- start
    /**
     * 设置 BankCard setBankBranchArea() 正确的传参类型,期望传值正确
     */
    public function testSetBankBranchAreaCorrectType()
    {
        $this->stub->setBankBranchArea('string');
        $this->assertEquals('string', $this->stub->getBankBranchArea());
    }

    /**
     * 设置 BankCard setBankBranchArea() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBankBranchAreaWrongType()
    {
        $this->stub->setBankBranchArea(array(1, 2, 3));
    }
    //bankBranchArea 测试 ----------------------------------------------------------   end
    
    //bankBranchAddress 测试 ---------------------------------------------------------- start
    /**
     * 设置 BankCard setBankBranchAddress() 正确的传参类型,期望传值正确
     */
    public function testSetBankBranchAddressCorrectType()
    {
        $this->stub->setBankBranchAddress('string');
        $this->assertEquals('string', $this->stub->getBankBranchAddress());
    }

    /**
     * 设置 BankCard setBankBranchAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBankBranchAddressWrongType()
    {
        $this->stub->setBankBranchAddress(array(1, 2, 3));
    }
    //bankBranchAddress 测试 ----------------------------------------------------------   end
    
    //cellphone 测试 ---------------------------------------------------------- start
    /**
     * 设置 BankCard setCellphone() 正确的传参类型,期望传值正确
     */
    public function testSetCellphoneCorrectType()
    {
        $this->stub->setCellphone('string');
        $this->assertEquals('string', $this->stub->getCellphone());
    }

    /**
     * 设置 BankCard setCellphone() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCellphoneWrongType()
    {
        $this->stub->setCellphone(array(1, 2, 3));
    }
    //cellphone 测试 ----------------------------------------------------------   end
    
    //cardholderName 测试 ---------------------------------------------------------- start
    /**
     * 设置 BankCard setCardholderName() 正确的传参类型,期望传值正确
     */
    public function testSetCardholderNameCorrectType()
    {
        $this->stub->setCardholderName('string');
        $this->assertEquals('string', $this->stub->getCardholderName());
    }

    /**
     * 设置 BankCard setCardholderName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCardholderNameWrongType()
    {
        $this->stub->setCardholderName(array(1, 2, 3));
    }
    //cardholderName 测试 ----------------------------------------------------------   end
    
    //licence 测试 ------------------------------------------------------- start
     /**
      * 设置 BankCard setLicence() 正确的传参类型,期望传值正确
      */
    public function testSetLicenceCorrectType()
    {
        $this->stub->setLicence(array());
        $this->assertEquals(array(), $this->stub->getLicence());
    }

    /**
     * 设置 BankCard setLicence() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLicenceWrongType()
    {
        $this->stub->setLicence('licence');
    }
    //licence 测试 -------------------------------------------------------   end
    
    //bank 测试 -------------------------------------------------------- start
    /**
     * 设置 BankCard setBank() 正确的传参类型,期望传值正确
     */
    public function testSetBankCorrectType()
    {
        $object = new Bank();
        $this->stub->setBank($object);
        $this->assertSame($object, $this->stub->getBank());
    }

    /**
     * 设置 BankCard setBank() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBankType()
    {
        $this->stub->setBank(array(1,2,3));
    }
    //bank 测试 -------------------------------------------------------- end
    
    //memberAccount 测试 -------------------------------------------------------- start
    /**
     * 设置 BankCard setMemberAccount() 正确的传参类型,期望传值正确
     */
    public function testSetMemberAccountCorrectType()
    {
        $object = new MemberAccount();
        $this->stub->setMemberAccount($object);
        $this->assertSame($object, $this->stub->getMemberAccount());
    }

    /**
     * 设置 BankCard setMemberAccount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberAccountType()
    {
        $this->stub->setMemberAccount(array(1,2,3));
    }
    //memberAccount 测试 -------------------------------------------------------- end
    
    //unbindReason 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setUnbindReason() 是否符合预定范围
     *
     * @dataProvider unbindReasonProvider
     */
    public function testSetUnbindReason($actual, $expected)
    {
        $this->stub->setUnbindReason($actual);
        $this->assertEquals($expected, $this->stub->getUnbindReason());
    }
    /**
     * 循环测试 DispatchDepartment setUnbindReason() 数据构建器
     */
    public function unbindReasonProvider()
    {
        return array(
            array(BankCard::UNBIND_REASON['OTHER'], BankCard::UNBIND_REASON['OTHER']),
            array(BankCard::UNBIND_REASON['WORRY'], BankCard::UNBIND_REASON['WORRY']),
            array(BankCard::UNBIND_REASON['ABANDON'], BankCard::UNBIND_REASON['ABANDON'])
        );
    }
    /**
     * 设置 BankCard setUnbindReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUnbindReasonWrongType()
    {
        $this->stub->setUnbindReason('string');
    }
    //unbindReason 测试 ------------------------------------------------------   end
    
    //paymentPassword 测试 ---------------------------------------------------------- start
    /**
     * 设置 BankCard setPaymentPassword() 正确的传参类型,期望传值正确
     */
    public function testSetPaymentPasswordCorrectType()
    {
        $this->stub->setPaymentPassword('string');
        $this->assertEquals('string', $this->stub->getPaymentPassword());
    }

    /**
     * 设置 BankCard setPaymentPassword() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPaymentPasswordWrongType()
    {
        $this->stub->setPaymentPassword(array(1, 2, 3));
    }
    //paymentPassword 测试 ----------------------------------------------------------   end
    
    public function testBindFalse()
    {
        $bank = new NullBank();
        $this->stub->setBank($bank);

        $result = $this->stub->bind();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testBind()
    {
        $this->stub = $this->getMockBuilder(BankCard::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $bank = \Sdk\Bank\Utils\MockFactory::generateBankObject(1);
        $memberAccount = \Sdk\MemberAccount\Utils\MockFactory::generateMemberAccountObject(1);
        $this->stub->setBank($bank);
        $this->stub->setMemberAccount($memberAccount);

        $repository = $this->prophesize(BankCardRepository::class);
        $repository->bind(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->bind();
        $this->assertTrue($result);
    }

    public function testUnBindFalse()
    {
        $this->stub = $this->getMockBuilder(BankCard::class)
            ->setMethods(['isNormal'])->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('isNormal')
            ->willReturn(false);

        $result = $this->stub->unBind();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_STATUS_NOT_NORMAL, Core::getLastError()->getId());
    }

    public function testUnBind()
    {
        $this->stub = $this->getMockBuilder(BankCard::class)
            ->setMethods(['getRepository', 'isNormal'])->getMock();

        $this->stub->expects($this->exactly(1))
            ->method('isNormal')
            ->willReturn(true);

        $repository = $this->prophesize(BankCardRepository::class);
        $repository->unBind(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->unBind();
        $this->assertTrue($result);
    }

    public function testIsNormal()
    {
        $this->stub->setStatus(BankCard::STATUS['NORMAL']);

        $result = $this->stub->isNormal();

        $this->assertTrue($result);
    }
}
