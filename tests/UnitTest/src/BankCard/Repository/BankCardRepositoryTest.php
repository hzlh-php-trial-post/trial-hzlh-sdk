<?php
namespace Sdk\BankCard\Repository;

use Sdk\BankCard\Adapter\BankCard\BankCardRestfulAdapter;

use Sdk\BankCard\Utils\MockFactory;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class BankCardRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(BankCardRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends BankCardRepository {
            public function getAdapter() : BankCardRestfulAdapter
            {
                return parent::getAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\BankCard\Adapter\BankCard\BankCardRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    /**
     * 为BankCardRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以BankCardRepository::PORTAL_LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(BankCardRestfulAdapter::class);
        $adapter->scenario(Argument::exact(BankCardRepository::PORTAL_LIST_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(BankCardRepository::PORTAL_LIST_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    public function testBind()
    {
        $bankCard = MockFactory::generateBankCardObject(1);
        $adapter = $this->prophesize(BankCardRestfulAdapter::class);
        $adapter->bind(Argument::exact($bankCard))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->bind($bankCard);
        $this->assertTrue($result);
    }

    public function testUnBind()
    {
        $bankCard = MockFactory::generateBankCardObject(1);
        $adapter = $this->prophesize(BankCardRestfulAdapter::class);
        $adapter->unBind(Argument::exact($bankCard))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->unBind($bankCard);
        $this->assertTrue($result);
    }
}
