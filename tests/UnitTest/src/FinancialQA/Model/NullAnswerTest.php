<?php
namespace Sdk\FinancialQA\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullAnswerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullAnswer::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsAnswer()
    {
        $this->assertInstanceof('Sdk\FinancialQA\Model\Answer', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
