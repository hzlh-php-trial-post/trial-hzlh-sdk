<?php
namespace Sdk\FinancialQA\Model;

use Sdk\FinancialQA\Repository\FinancialAnswerRepository;

use Sdk\Member\Model\Member;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class FinancialAnswerTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswer::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends FinancialAnswer{
            public function getRepository() : FinancialAnswerRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Repository\FinancialAnswerRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetAnswerIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetAnswerIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //content 测试 ---------------------------------------------------------- start
    /**
     * 设置setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->stub->setContent('string');
        $this->assertEquals('string', $this->stub->getContent());
    }

    /**
     * 设置setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->stub->setContent(array(1, 2, 3));
    }
    //content 测试 ----------------------------------------------------------   end

    //replyCount 测试 ---------------------------------------------------------- start
    /**
     * 设置setReplyCount() 正确的传参类型,期望传值正确
     */
    public function testSetReplyCountCorrectType()
    {
        $this->stub->setReplyCount(1);
        $this->assertEquals(1, $this->stub->getReplyCount());
    }

    /**
     * 设置setReplyCount() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetReplyCountWrongTypeButNumeric()
    {
        $this->stub->setReplyCount('1');
        $this->assertTrue(is_int($this->stub->getReplyCount()));
        $this->assertEquals(1, $this->stub->getReplyCount());
    }
    //replyCount 测试 ----------------------------------------------------------   end

    //likesCount 测试 ---------------------------------------------------------- start
    /**
     * 设置setLikesCount() 正确的传参类型,期望传值正确
     */
    public function testSetLikesCountCorrectType()
    {
        $this->stub->setLikesCount(1);
        $this->assertEquals(1, $this->stub->getLikesCount());
    }

    /**
     * 设置setLikesCount() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetLikesCountWrongTypeButNumeric()
    {
        $this->stub->setLikesCount('1');
        $this->assertTrue(is_int($this->stub->getLikesCount()));
        $this->assertEquals(1, $this->stub->getLikesCount());
    }
    //likesCount 测试 ----------------------------------------------------------   end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $object = new Member();
        $this->stub->setMember($object);
        $this->assertSame($object, $this->stub->getMember());
    }

    /**
     * 设置 setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberType()
    {
        $this->stub->setMember(array(1,2,3));
    }
    //member 测试 -------------------------------------------------------- end

    //question 测试 -------------------------------------------------------- start
    /**
     * 设置 setQuestion() 正确的传参类型,期望传值正确
     */
    public function testSetQuestionCorrectType()
    {
        $object = new FinancialQuestion();
        $this->stub->setQuestion($object);
        $this->assertSame($object, $this->stub->getQuestion());
    }

    /**
     * 设置 setQuestion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetQuestionType()
    {
        $this->stub->setQuestion(array(1,2,3));
    }
    //question 测试 -------------------------------------------------------- end

    //deleteInfo 测试 -------------------------------------------------------- start
    /**
     * 设置 setDeleteInfo() 正确的传参类型,期望传值正确
     */
    public function testSetDeleteInfoCorrectType()
    {
        $object = new DeleteInfo();
        $this->stub->setDeleteInfo($object);
        $this->assertSame($object, $this->stub->getDeleteInfo());
    }

    /**
     * 设置 setDeleteInfo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeleteInfoType()
    {
        $this->stub->setDeleteInfo(array(1,2,3));
    }
    //deleteInfo 测试 -------------------------------------------------------- end

    //平台删除
    public function testPlatformDeleteSuccess()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswer::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(true);

        $repository = $this->prophesize(FinancialAnswerRepository::class);
        $repository->platformDelete(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->platformDelete();
        $this->assertTrue($result);
    }

    public function testPlatformDeleteFailure()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswer::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(false);

        $repository = $this->prophesize(FinancialAnswerRepository::class);
        $repository->platformDelete(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->platformDelete();
        $this->assertFalse($result);
    }

    //提问者删除
    public function testQuestionerDeleteSuccess()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswer::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(true);

        $repository = $this->prophesize(FinancialAnswerRepository::class);
        $repository->questionerDelete(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->questionerDelete();
        $this->assertTrue($result);
    }

    public function testQuestionerDeleteFailure()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswer::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(false);

        $repository = $this->prophesize(FinancialAnswerRepository::class);
        $repository->questionerDelete(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->questionerDelete();
        $this->assertFalse($result);
    }

    //本人删除
    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswer::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();

        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(true);

        $repository = $this->prophesize(FinancialAnswerRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFailure()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswer::class)
            ->setMethods([
                'isNormal',
                'getRepository'
            ])->getMock();
        
        $this->stub->expects($this->once())
                   ->method('isNormal')
                   ->willReturn(false);

        $repository = $this->prophesize(FinancialAnswerRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }

    public function testIsNormal()
    {
        $this->stub->setStatus(FinancialAnswer::STATUS['NORMAL']);

        $result = $this->stub->isNormal();

        $this->assertTrue($result);
    }
}
