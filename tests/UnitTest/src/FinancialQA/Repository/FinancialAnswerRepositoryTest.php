<?php
namespace Sdk\FinancialQA\Repository;

use Sdk\FinancialQA\Adapter\FinancialAnswer\FinancialAnswerRestfulAdapter;
use Sdk\FinancialQA\Adapter\FinancialAnswer\FinancialAnswerMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class FinancialAnswerRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(FinancialAnswerRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends FinancialAnswerRepository {
            public function getAdapter() : FinancialAnswerRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : FinancialAnswerMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Adapter\FinancialAnswer\FinancialAnswerRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\FinancialQA\Adapter\FinancialAnswer\FinancialAnswerMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为FinancialAnswerRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以FinancialAnswerRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(FinancialAnswerRestfulAdapter::class);
        $adapter->scenario(Argument::exact(FinancialAnswerRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(FinancialAnswerRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为FinancialAnswerRestfulAdapter建立预言
     * 建立预期状况：platformDelete() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testPlatformDelete()
    {
        $financialAnswer = \Sdk\FinancialQA\Utils\FinancialAnswer\MockFactory::generateAnswerObject(1);

        $adapter = $this->prophesize(FinancialAnswerRestfulAdapter::class);
        $adapter->platformDelete(
            Argument::exact($financialAnswer)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->platformDelete($financialAnswer);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为FinancialAnswerRestfulAdapter建立预言
     * 建立预期状况：questionerDelete() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testQuestionerDelete()
    {
        $financialAnswer = \Sdk\FinancialQA\Utils\FinancialAnswer\MockFactory::generateAnswerObject(1);

        $adapter = $this->prophesize(FinancialAnswerRestfulAdapter::class);
        $adapter->questionerDelete(
            Argument::exact($financialAnswer)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->questionerDelete($financialAnswer);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为FinancialAnswerRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testDeletes()
    {
        $financialAnswer = \Sdk\FinancialQA\Utils\FinancialAnswer\MockFactory::generateAnswerObject(1);

        $adapter = $this->prophesize(FinancialAnswerRestfulAdapter::class);
        $adapter->deletes(
            Argument::exact($financialAnswer)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deletes($financialAnswer);
        $this->assertTrue($result);
    }
}
