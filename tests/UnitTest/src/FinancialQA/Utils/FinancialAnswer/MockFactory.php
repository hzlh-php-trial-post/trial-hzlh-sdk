<?php
namespace Sdk\FinancialQA\Utils\FinancialAnswer;

use Sdk\FinancialQA\Model\FinancialAnswer;
use Sdk\FinancialQA\Model\DeleteInfo;

class MockFactory
{
    /**
     * [generateFinancialAnswerArray 生成问题数组]
     * @return [array] [问题数组]
     */
    public static function generateAnswerArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $financialAnswer = array();

        $financialAnswer = array(
            'data'=>array(
                'type'=>'financialAnswers',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //content
        $content = self::generateContent($faker, $value);
        $attributes['content'] = $content;
        //replyCount
        $replyCount = self::generateReplyCount($faker, $value);
        $attributes['replyCount'] = $replyCount;
        //likesCount
        $likesCount = self::generateLikesCount($faker, $value);
        $attributes['likesCount'] = $likesCount;
        //deleteType
        $deleteType = self::generateDeleteType($faker, $value);
        $attributes['deleteType'] = $deleteType;
        //deleteReason
        $deleteReason = self::generateDeleteReason($faker, $value);
        $attributes['deleteReason'] = $deleteReason;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $financialAnswer['data']['attributes'] = $attributes;
        //member
        $financialAnswer['data']['relationships']['member']['data'] = array(
            'type' => 'members',
            'id' => $faker->randomNumber(1)
        );
        //question
        $financialAnswer['data']['relationships']['financialQuestion']['data'] = array(
            'type' => 'financialQuestions',
            'id' => $faker->randomNumber(1)
        );

        return $financialAnswer;
    }

    /**
     * [generateFinancialAnswerObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateAnswerObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : FinancialAnswer {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $financialAnswer = new FinancialAnswer($id);

        //content
        $content = self::generateContent($faker, $value);
        $financialAnswer->setContent($content);
        //member
        $member = self::generateMember($faker, $value);
        $financialAnswer->setMember($member);
        //deleteReason
        $deleteReason = self::generateDeleteReason($faker, $value);
        //deleteType
        $deleteType = self::generateDeleteType($faker, $value);
        //deleteBy
        $deleteBy = self::generateMember($faker, $value);
        $financialAnswer->setDeleteInfo(
            new DeleteInfo(
                $deleteType,
                $deleteReason,
                $deleteBy
            )
        );
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $financialAnswer->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $financialAnswer->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $financialAnswer->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $financialAnswer->setStatus($status);

        return $financialAnswer;
    }

    private static function generateContent($faker, array $value = array())
    {
        return $content = isset($value['content']) ?
        $value['content'] : $faker->word;
    }
    private static function generateReplyCount($faker, array $value = array())
    {
        return $replyCount = isset($value['replyCount']) ?
        $value['replyCount'] : $faker->numerify();
    }
    private static function generateLikesCount($faker, array $value = array())
    {
        return $likesCount = isset($value['likesCount']) ?
        $value['likesCount'] : $faker->numerify();
    }
    private static function generateDeleteType($faker, array $value = array())
    {
        return $deleteType = isset($value['deleteType']) ?
        $value['deleteType'] : $faker->randomElement(
            $array = array(
                DeleteInfo::TYPE['NULL'],
                DeleteInfo::TYPE['DELETED'],
                DeleteInfo::TYPE['PLATFORM_DELETED'],
                DeleteInfo::TYPE['QUESTIONER_DELETED']
            )
        );
    }
    private static function generateDeleteReason($faker, array $value = array())
    {
        return $deleteReason = isset($value['deleteReason']) ?
        $value['deleteReason'] : $faker->word;
    }
    private static function generateMember($faker, array $value = array())
    {
        return $member = isset($value['member']) ?
            $value['member'] : \Sdk\Member\Utils\MockFactory::generateMemberObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
