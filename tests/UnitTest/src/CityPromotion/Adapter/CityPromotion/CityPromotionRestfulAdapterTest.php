<?php
namespace Sdk\CityPromotion\Adapter\CityPromotion;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\CityPromotion\Model\CityPromotion;
use Sdk\CityPromotion\Model\NullCityPromotion;
use Sdk\CityPromotion\Utils\MockFactory;
use Sdk\CityPromotion\Translator\CityPromotionRestfulTranslator;

class CityPromotionRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CityPromotionRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends CityPromotionRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsICityPromotionAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\CityPromotion\Adapter\CityPromotion\ICityPromotionAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('cityPromotions', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\CityPromotion\Translator\CityPromotionRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_CITY_PROMOTION_LIST',
                CityPromotionRestfulAdapter::SCENARIOS['OA_CITY_PROMOTION_LIST']
            ],
            [
                'PORTAL_CITY_PROMOTION_LIST',
                CityPromotionRestfulAdapter::SCENARIOS['PORTAL_CITY_PROMOTION_LIST']
            ],
            [
                'CITY_PROMOTION_FETCH_ONE',
                CityPromotionRestfulAdapter::SCENARIOS['CITY_PROMOTION_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $cityPromotion = MockFactory::generateCityPromotionObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullCityPromotion())
            ->willReturn($cityPromotion);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($cityPromotion, $result);
    }
    /**
     * 为CityPromotionRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$cityPromotion$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareCityPromotionTranslator(
        CityPromotion $cityPromotion,
        array $keys,
        array $cityPromotionArray
    ) {
        $translator = $this->prophesize(CityPromotionRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($cityPromotion),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($cityPromotionArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(CityPromotion $cityPromotion)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($cityPromotion);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);
        $cityPromotionArray = array();

        $this->prepareCityPromotionTranslator(
            $cityPromotion,
            array(
                'title',
                'cover',
                'detail',
                'crew'
            ),
            $cityPromotionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('cityPromotions', $cityPromotionArray);

        $this->success($cityPromotion);

        $result = $this->stub->add($cityPromotion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);
        $cityPromotionArray = array();

        $this->prepareCityPromotionTranslator(
            $cityPromotion,
            array(
                'title',
                'cover',
                'detail',
                'crew'
            ),
            $cityPromotionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('cityPromotions', $cityPromotionArray);

        $this->failure($cityPromotion);
        $result = $this->stub->add($cityPromotion);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);
        $cityPromotionArray = array();

        $this->prepareCityPromotionTranslator(
            $cityPromotion,
            array(
                'title',
                'cover',
                'detail',
                'videoLink'
            ),
            $cityPromotionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('cityPromotions/'.$cityPromotion->getId(), $cityPromotionArray);

        $this->success($cityPromotion);

        $result = $this->stub->edit($cityPromotion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);
        $cityPromotionArray = array();

        $this->prepareCityPromotionTranslator(
            $cityPromotion,
            array(
                'title',
                'cover',
                'detail',
                'videoLink'
            ),
            $cityPromotionArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('cityPromotions/'.$cityPromotion->getId(), $cityPromotionArray);

        $this->failure($cityPromotion);
        $result = $this->stub->edit($cityPromotion);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行onShelf（）
     * 判断 result 是否为true
     */
    public function testOnShelfSuccess()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'cityPromotions/'.$cityPromotion->getId().'/onShelf'
            );

        $this->success($cityPromotion);

        $result = $this->stub->onShelf($cityPromotion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行onShelf（）
     * 判断 result 是否为false
     */
    public function testOnShelfFailure()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'cityPromotions/'.$cityPromotion->getId().'/onShelf'
            );

        $this->failure($cityPromotion);
        $result = $this->stub->onShelf($cityPromotion);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行offStock（）
     * 判断 result 是否为true
     */
    public function testOffStockSuccess()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'cityPromotions/'.$cityPromotion->getId().'/offStock'
            );

        $this->success($cityPromotion);

        $result = $this->stub->offStock($cityPromotion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行offStock（）
     * 判断 result 是否为false
     */
    public function testOffStockFailure()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'cityPromotions/'.$cityPromotion->getId().'/offStock'
            );

        $this->failure($cityPromotion);
        $result = $this->stub->offStock($cityPromotion);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行recommend（）
     * 判断 result 是否为true
     */
    public function testRecommendSuccess()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'cityPromotions/'.$cityPromotion->getId().'/recommend'
            );

        $this->success($cityPromotion);

        $result = $this->stub->recommend($cityPromotion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行recommend（）
     * 判断 result 是否为false
     */
    public function testRecommendFailure()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'cityPromotions/'.$cityPromotion->getId().'/recommend'
            );

        $this->failure($cityPromotion);
        $result = $this->stub->recommend($cityPromotion);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行cancelRecommend（）
     * 判断 result 是否为true
     */
    public function testCancelRecommendSuccess()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'cityPromotions/'.$cityPromotion->getId().'/cancelRecommend'
            );

        $this->success($cityPromotion);

        $result = $this->stub->cancelRecommend($cityPromotion);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCityPromotionTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行cancelRecommend（）
     * 判断 result 是否为false
     */
    public function testCancelRecommendFailure()
    {
        $cityPromotion = MockFactory::generateCityPromotionObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'cityPromotions/'.$cityPromotion->getId().'/cancelRecommend'
            );

        $this->failure($cityPromotion);
        $result = $this->stub->cancelRecommend($cityPromotion);
        $this->assertFalse($result);
    }
}
