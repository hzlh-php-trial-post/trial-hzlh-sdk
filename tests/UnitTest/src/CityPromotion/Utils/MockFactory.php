<?php
namespace Sdk\CityPromotion\Utils;

use Sdk\CityPromotion\Model\CityPromotion;

class MockFactory
{
    /**
     * [generateCityPromotionArray 生成城市推广数组]
     * @return [array] [用户数组]
     */
    public static function generateCityPromotionArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $moneyWiseNews = array();

        $moneyWiseNews = array(
            'data'=>array(
                'type'=>'moneyWiseNewss',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //title
        $title = self::generateTitle($faker, $value);
        $attributes['title'] = $title;
        //detail
        $detail = self::generateDetaile($faker, $value);
        $attributes['detail'] = $detail;
        //cover
        $cover = self::generateCover($faker, $value);
        $attributes['cover'] = $cover;
        //recommendStatus
        $recommendStatus = self::generateRecommendStatus($faker, $value);
        $attributes['recommendStatus'] = $recommendStatus;

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $moneyWiseNews['data']['attributes'] = $attributes;
        //crew
        $moneyWiseNews['data']['relationships']['crew']['data'] = array(
            'type' => 'crews',
            'id' => $faker->randomNumber(1)
        );

        return $moneyWiseNews;
    }
    /**
     * [generateCityPromotionObject 生成城市推广对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateCityPromotionObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : CityPromotion {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $moneyWiseNews = new CityPromotion($id);

        //title
        $title = self::generateTitle($faker, $value);
        $moneyWiseNews->setTitle($title);
        //detail
        $detail = self::generateDetaile($faker, $value);
        $moneyWiseNews->setDetail($detail);
        //cover
        $cover = self::generateCover($faker, $value);
        $moneyWiseNews->setCover($cover);
        //recommendStatus
        $recommendStatus = self::generateRecommendStatus($faker, $value);
        $moneyWiseNews->setRecommendStatus($recommendStatus);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $moneyWiseNews->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $moneyWiseNews->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $moneyWiseNews->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $moneyWiseNews->setStatus($status);
        //crew
        $crew = self::generateCrew($faker, $value);
        $moneyWiseNews->setCrew($crew);

        return $moneyWiseNews;
    }

    private static function generateDetaile($faker, array $value = array())
    {
        return isset($value['detail']) ?
        $value['detail'] : array();
    }

    private static function generateTitle($faker, array $value = array())
    {
        return isset($value['title']) ?
        $value['title'] : $faker->name;
    }

    private static function generateCover($faker, array $value = array())
    {
        return isset($value['cover']) ?
        $value['cover'] : array();
    }

    private static function generateRecommendStatus($faker, array $value = array())
    {
        return isset($value['recommendStatus']) ?
        $value['recommendStatus'] : $faker->numerify();
    }

    private static function generateCrew($faker, array $value = array())
    {
        return isset($value['crew']) ?
            $value['crew'] : \Sdk\Crew\Utils\MockFactory::generateCrewObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
