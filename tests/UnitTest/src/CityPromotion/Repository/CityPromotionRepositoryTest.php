<?php
namespace Sdk\CityPromotion\Repository;

use Sdk\CityPromotion\Adapter\CityPromotion\CityPromotionRestfulAdapter;
use Sdk\CityPromotion\Adapter\CityPromotion\CityPromotionMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class CityPromotionRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CityPromotionRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends CityPromotionRepository {
            public function getAdapter() : CityPromotionRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : CityPromotionMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\CityPromotion\Adapter\CityPromotion\CityPromotionRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\CityPromotion\Adapter\CityPromotion\CityPromotionMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为CityPromotionRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以CityPromotionRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(CityPromotionRestfulAdapter::class);
        $adapter->scenario(Argument::exact(CityPromotionRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);//phpcs:ignore

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(CityPromotionRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
