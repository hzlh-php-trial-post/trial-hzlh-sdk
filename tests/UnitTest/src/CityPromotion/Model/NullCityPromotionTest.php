<?php
namespace Sdk\CityPromotion\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullCityPromotionTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullCityPromotion::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCityPromotion()
    {
        $this->assertInstanceof('Sdk\CityPromotion\Model\CityPromotion', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
