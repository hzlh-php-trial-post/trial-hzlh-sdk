<?php
namespace Sdk\CityPromotion\Model;

use Sdk\CityPromotion\Repository\CityPromotionRepository;

use Sdk\Crew\Model\Crew;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IOnShelfAbleAdapter;
use Sdk\Common\Adapter\IRecommendAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class CityPromotionTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CityPromotion::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends CityPromotion{
            public function getRepository() : CityPromotionRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
            public function getIOnShelfAbleAdapter() : IOnShelfAbleAdapter
            {
                return parent::getIOnShelfAbleAdapter();
            }
            public function getIRecommendAbleAdapter() : IRecommendAbleAdapter
            {
                return parent::getIRecommendAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\CityPromotion\Repository\CityPromotionRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    public function testGetIOnShelfAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOnShelfAbleAdapter',
            $this->childStub->getIOnShelfAbleAdapter()
        );
    }

    public function testGetIRecommendAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IRecommendAbleAdapter',
            $this->childStub->getIRecommendAbleAdapter()
        );
    }
    
    //title 测试 ---------------------------------------------------------- start
    /**
     * 设置setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->stub->setTitle('string');
        $this->assertEquals('string', $this->stub->getTitle());
    }

    /**
     * 设置setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->stub->setTitle(array(1, 2, 3));
    }
    //title 测试 ----------------------------------------------------------   end

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //cover 测试 ---------------------------------------------------------- start
    /**
     * 设置setCover() 正确的传参类型,期望传值正确
     */
    public function testSetCoverCorrectType()
    {
        $this->stub->setCover(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getCover());
    }

    /**
     * 设置setCover() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCoverWrongType()
    {
        $this->stub->setCover('string');
    }
    //cover 测试 ----------------------------------------------------------   end

    //detail 测试 ---------------------------------------------------------- start
    /**
     * 设置setDetail() 正确的传参类型,期望传值正确
     */
    public function testSetDetailCorrectType()
    {
        $this->stub->setDetail(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getDetail());
    }

    /**
     * 设置setDetail() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDetailWrongType()
    {
        $this->stub->setDetail('string');
    }
    //detail 测试 ----------------------------------------------------------   end

    //recommendStatus 测试 ---------------------------------------------------------- start
    /**
     * 设置setRecommendStatus() 正确的传参类型,期望传值正确
     */
    public function testSetRecommendStatusCorrectType()
    {
        $this->stub->setRecommendStatus(1);
        $this->assertEquals(1, $this->stub->getRecommendStatus());
    }

    /**
     * 设置setRecommendStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRecommendStatusWrongType()
    {
        $this->stub->setRecommendStatus('string');
    }
    //recommendStatus 测试 ----------------------------------------------------------   end


    //crew 测试 ---------------------------------------------------------- start
    /**
     * 设置setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();

        $this->stub->setCrew($object);
        $this->assertEquals($object, $this->stub->getCrew());
    }

    /**
     * 设置setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->stub->setCrew(array(1, 2, 3));
    }
    //crew 测试 ----------------------------------------------------------   end
}
