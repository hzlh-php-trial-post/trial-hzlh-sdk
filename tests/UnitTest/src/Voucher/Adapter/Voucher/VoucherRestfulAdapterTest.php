<?php
namespace Sdk\Voucher\Adapter\Voucher;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Voucher\Model\Voucher;
use Sdk\Voucher\Model\NullVoucher;
use Sdk\Voucher\Utils\MockFactory;
use Sdk\Voucher\Translator\VoucherRestfulTranslator;

class VoucherRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(VoucherRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends VoucherRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIVoucherAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Voucher\Adapter\Voucher\IVoucherAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('vouchers', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Voucher\Translator\VoucherRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'PORTAL_VOUCHER_LIST',
                VoucherRestfulAdapter::SCENARIOS['PORTAL_VOUCHER_LIST']
            ],
            [
                'VOUCHER_FETCH_ONE',
                VoucherRestfulAdapter::SCENARIOS['VOUCHER_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $voucher = MockFactory::generateVoucherObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullVoucher())
            ->willReturn($voucher);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($voucher, $result);
    }
    /**
     * 为VoucherRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$voucher，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareVoucherTranslator(
        Voucher $voucher,
        array $keys,
        array $voucherArray
    ) {
        $translator = $this->prophesize(VoucherRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($voucher),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($voucherArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Voucher $voucher)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($voucher);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareVoucherTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array();

        $this->prepareVoucherTranslator(
            $voucher,
            array(
                'type',
                'voucherType',
                'generationDate',
                'attachments',
                'voucherRecords',
                'accountTemplate',
                'member'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('vouchers', $voucherArray);

        $this->success($voucher);

        $result = $this->stub->add($voucher);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareVoucherTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array();

        $this->prepareVoucherTranslator(
            $voucher,
            array(
                'type',
                'voucherType',
                'generationDate',
                'attachments',
                'voucherRecords',
                'accountTemplate',
                'member'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('vouchers', $voucherArray);

        $this->failure($voucher);
        $result = $this->stub->add($voucher);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareVoucherTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array();

        $this->prepareVoucherTranslator(
            $voucher,
            array(
                'type',
                'generationDate',
                'attachments',
                'voucherRecords'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'vouchers/'.$voucher->getId(),
                $voucherArray
            );

        $this->success($voucher);

        $result = $this->stub->edit($voucher);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparevoucherTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array();

        $this->preparevoucherTranslator(
            $voucher,
            array(
                'type',
                'generationDate',
                'attachments',
                'voucherRecords'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'vouchers/'.$voucher->getId(),
                $voucherArray
            );

        $this->failure($voucher);
        $result = $this->stub->edit($voucher);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareVoucherTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行insert（）
     * 判断 result 是否为true
     */
    public function testInsertSuccess()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array();

        $this->prepareVoucherTranslator(
            $voucher,
            array(
                'type',
                'number',
                'voucherType',
                'generationDate',
                'attachments',
                'voucherRecords',
                'accountTemplate',
                'member'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with(
                'vouchers/insert',
                $voucherArray
            );

        $this->success($voucher);

        $result = $this->stub->insert($voucher);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparevoucherTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行insert（）
     * 判断 result 是否为false
     */
    public function testInsertFailure()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array();

        $this->preparevoucherTranslator(
            $voucher,
            array(
                'type',
                'number',
                'voucherType',
                'generationDate',
                'attachments',
                'voucherRecords',
                'accountTemplate',
                'member'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with(
                'vouchers/insert',
                $voucherArray
            );

        $this->failure($voucher);
        $result = $this->stub->insert($voucher);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareVoucherTranslator方法
     * 揭示预言中的delete，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $voucher = MockFactory::generateVoucherObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'vouchers/'.$voucher->getId().'/delete'
            );

        $this->success($voucher);

        $result = $this->stub->deletes($voucher);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparevoucherTranslator方法
     * 揭示预言中的delete，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $voucher = MockFactory::generateVoucherObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'vouchers/'.$voucher->getId().'/delete'
            );

        $this->failure($voucher);
        $result = $this->stub->deletes($voucher);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareVoucherTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行success（）
     * 执行arrange（）
     * 判断 result 是否为true
     */
    public function testArrangeSuccess()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array();

        $this->prepareVoucherTranslator(
            $voucher,
            array(
                'generationDate',
                'accountTemplate'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'vouchers/arrange',
                $voucherArray
            );

        $this->success($voucher);

        $result = $this->stub->arrange($voucher);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparevoucherTranslator方法
     * 揭示预言中的patch，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行arrange（）
     * 判断 result 是否为false
     */
    public function testArrangeFailure()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array();

        $this->preparevoucherTranslator(
            $voucher,
            array(
                'generationDate',
                'accountTemplate'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'vouchers/arrange',
                $voucherArray
            );

        $this->failure($voucher);
        $result = $this->stub->arrange($voucher);
        $this->assertFalse($result);
    }
}
