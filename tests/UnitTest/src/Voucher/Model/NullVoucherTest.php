<?php
namespace Sdk\Voucher\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullVoucherTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullVoucher::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsVoucher()
    {
        $this->assertInstanceof('Sdk\Voucher\Model\Voucher', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testInsert()
    {
        $result = $this->stub->insert();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testDeletes()
    {
        $result = $this->stub->deletes();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testArrange()
    {
        $result = $this->stub->arrange();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
