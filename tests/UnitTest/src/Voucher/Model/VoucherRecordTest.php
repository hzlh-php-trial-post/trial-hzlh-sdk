<?php
namespace Sdk\Voucher\Model;

use PHPUnit\Framework\TestCase;

class VoucherRecordTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(VoucherRecord::class)
            ->setMethods([
                'getRepository'
            ])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 VoucherRecord setId() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherRecordIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 VoucherRecord setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetVoucherRecordIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //abstract 测试 ---------------------------------------------------------- start
    /**
     * 设置 VoucherRecord setAbstract() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherRecordAbstractCorrectType()
    {
        $this->stub->setAbstract('string');
        $this->assertEquals('string', $this->stub->getAbstract());
    }

    /**
     * 设置 VoucherRecord setAbstract() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherRecordAbstractWrongType()
    {
        $this->stub->setAbstract(array(1, 2, 3));
    }
    //abstract 测试 ----------------------------------------------------------   end

    //debitAmount 测试 ---------------------------------------------------------- start
    /**
     * 设置 VoucherRecord setDebitAmount() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherRecordDebitAmountCorrectType()
    {
        $this->stub->setDebitAmount(1.01);
        $this->assertEquals(1.01, $this->stub->getDebitAmount());
    }

    /**
     * 设置 VoucherRecord setDebitAmount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherRecordDebitAmountWrongType()
    {
        $this->stub->setDebitAmount('string');
    }
    //debitAmount 测试 ----------------------------------------------------------   end
    
    //creditAmount 测试 ---------------------------------------------------------- start
    /**
     * 设置 VoucherRecord setCreditAmount() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherRecordCreditAmountCorrectType()
    {
        $this->stub->setCreditAmount(1.01);
        $this->assertEquals(1.01, $this->stub->getCreditAmount());
    }

    /**
     * 设置 VoucherRecord setCreditAmount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherRecordCreditAmountWrongType()
    {
        $this->stub->setCreditAmount('string');
    }
    //creditAmount 测试 ----------------------------------------------------------   end
    
    //subjectId 测试 ---------------------------------------------------------- start
    /**
     * 设置 VoucherRecord setSubjectId() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherRecordSubjectIdCorrectType()
    {
        $this->stub->setSubjectId('1_1001');
        $this->assertEquals('1_1001', $this->stub->getSubjectId());
    }

    /**
     * 设置 VoucherRecord setSubjectId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherRecordSubjectIdWrongType()
    {
        $this->stub->setSubjectId(array(1, 2, 3));
    }
    //subjectId 测试 ----------------------------------------------------------   end
 
    //accountSubject 测试 ---------------------------------------------------------- start
    /**
     * 设置 VoucherRecord setAccountSubject() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherRecordAccountSubjectCorrectType()
    {
        $this->stub->setAccountSubject(array());
        $this->assertEquals(array(), $this->stub->getAccountSubject());
    }

    /**
     * 设置 VoucherRecord setAccountSubject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherRecordAccountSubjectWrongType()
    {
        $this->stub->setAccountSubject('string');
    }
    //accountSubject 测试 ----------------------------------------------------------   end
}
