<?php
namespace Sdk\Voucher\Model;

use Sdk\Voucher\Repository\VoucherRepository;

use Sdk\Member\Model\Member;
use Sdk\AccountTemplate\Model\AccountTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class VoucherTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Voucher::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends Voucher{
            public function getRepository() : VoucherRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Voucher\Repository\VoucherRepository',
            $this->childStub->getRepository()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Voucher setId() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Voucher setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetVoucherIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //type 测试 ---------------------------------------------------------- start
    /**
     * 设置 Voucher setType() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherTypeCorrectType()
    {
        $this->stub->setType(1);
        $this->assertEquals(1, $this->stub->getType());
    }

    /**
     * 设置 Voucher setType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherTypeWrongType()
    {
        $this->stub->setType('string');
    }
    //type 测试 ----------------------------------------------------------   end

    //number 测试 ---------------------------------------------------------- start
    /**
     * 设置 Voucher setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherNumberCorrectType()
    {
        $this->stub->setNumber(1);
        $this->assertEquals(1, $this->stub->getNumber());
    }

    /**
     * 设置 Voucher setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherNumberWrongType()
    {
        $this->stub->setNumber('string');
    }
    //number 测试 ----------------------------------------------------------   end
    
    //generationDate 测试 ---------------------------------------------------------- start
    /**
     * 设置 Voucher setGenerationDate() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherGenerationDateCorrectType()
    {
        $this->stub->setGenerationDate('1595400484');
        $this->assertEquals('1595400484', $this->stub->getGenerationDate());
    }

    /**
     * 设置 Voucher setGenerationDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherGenerationDateWrongTypeButNumeric()
    {
        $this->stub->setGenerationDate(array(1, 2, 3));
    }
    //generationDate 测试 ----------------------------------------------------------   end
 
    //attachments 测试 ---------------------------------------------------------- start
    /**
     * 设置 Voucher setAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherAttachmentsCorrectType()
    {
        $this->stub->setAttachments(array());
        $this->assertEquals(array(), $this->stub->getAttachments());
    }

    /**
     * 设置 Voucher setAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherAttachmentsWrongType()
    {
        $this->stub->setAttachments('string');
    }
    //attachments 测试 ----------------------------------------------------------   end

    //voucherRecords 测试 ------------------------------------------------------- start
    /**
     * 设置 Voucher addVoucherRecord() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherRecordsCorrectType()
    {
        $object = new VoucherRecord();

        $this->stub->addVoucherRecord($object);
        $this->assertEquals(array($object), $this->stub->getVoucherRecords());
    }

    /**
     * 设置 Voucher addVoucherRecord() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherRecordsWrongType()
    {
        $this->stub->addVoucherRecord('string');
    }
    //voucherRecords 测试 ------------------------------------------------------- end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 Voucher setMember() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherMemberCorrectType()
    {
        $object = new Member();
        $this->stub->setMember($object);
        $this->assertSame($object, $this->stub->getMember());
    }

    /**
     * 设置 Voucher setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherMemberType()
    {
        $this->stub->setMember(array(1,2,3));
    }
    //member 测试 -------------------------------------------------------- end

    //accountTemplate 测试 -------------------------------------------------------- start
    /**
     * 设置 Voucher setAccountTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherAccountTemplateCorrectType()
    {
        $object = new AccountTemplate();
        $this->stub->setAccountTemplate($object);
        $this->assertSame($object, $this->stub->getAccountTemplate());
    }

    /**
     * 设置 Voucher setAccountTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherAccountTemplateType()
    {
        $this->stub->setAccountTemplate(array(1,2,3));
    }
    //accountTemplate 测试 -------------------------------------------------------- end
    
    //插入凭证
    public function testInsertSuccess()
    {
        $this->stub = $this->getMockBuilder(Voucher::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(VoucherRepository::class);
        $repository->insert(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->insert();
        $this->assertTrue($result);
    }

    public function testInsertFailure()
    {
        $this->stub = $this->getMockBuilder(Voucher::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(VoucherRepository::class);
        $repository->insert(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->insert();
        $this->assertFalse($result);
    }

    //删除凭证
    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(Voucher::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(VoucherRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFailure()
    {
        $this->stub = $this->getMockBuilder(Voucher::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(VoucherRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }

    //整理凭证
    public function testArrangeSuccess()
    {
        $this->stub = $this->getMockBuilder(Voucher::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(VoucherRepository::class);
        $repository->arrange(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->arrange();
        $this->assertTrue($result);
    }

    public function testArrangeFailure()
    {
        $this->stub = $this->getMockBuilder(Voucher::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(VoucherRepository::class);
        $repository->arrange(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->arrange();
        $this->assertFalse($result);
    }
}
