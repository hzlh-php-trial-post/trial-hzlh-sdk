<?php
namespace Sdk\Voucher\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullVoucherRecordTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullVoucherRecord::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsVoucherRecord()
    {
        $this->assertInstanceof('Sdk\Voucher\Model\VoucherRecord', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
