<?php
namespace Sdk\Voucher\Utils;

use Sdk\Voucher\Model\Voucher;
use Sdk\Voucher\Model\VoucherRecord;

class MockFactory
{
    /**
     * [generateVoucherArray 生成凭证数组]
     * @return [array] [凭证信息]
     */
    public static function generateVoucherArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $voucher = array();

        $voucher = array(
            'data'=>array(
                'type'=>'vouchers',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //type
        $type = self::generateType($faker, $value);
        $attributes['type'] = $type;
        //number
        $number = self::generateNumber($faker, $value);
        $attributes['number'] = $number;
        //generationDate
        $generationDate = '2020-01';
        $attributes['generationDate'] = $generationDate;
        //attachments
        $attachments = self::generateAttachments($faker, $value);
        $attributes['attachments'] = $attachments;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $voucher['data']['attributes'] = $attributes;
        //accountTemplate
        $voucher['data']['relationships']['accountTemplate']['data'] = array(
            'type' => 'accountTemplates',
            'id' => $faker->randomNumber(1)
        );
        //member
        $voucher['data']['relationships']['member']['data'] = array(
            'type' => 'members',
            'id' => $faker->randomNumber(1)
        );
        //voucherRecords
        // $voucher['data']['relationships']['voucherRecords']['data'] = [
        //     array(
        //         'type' => 'voucherRecords',
        //         'id' => $faker->randomNumber(1)
        //     ),
        //     array(
        //         'type' => 'voucherRecords',
        //         'id' => $faker->randomNumber(1)
        //     ),
        //     array(
        //         'type' => 'voucherRecords',
        //         'id' => $faker->randomNumber(1)
        //     )
        // ];

        return $voucher;
    }

    /**
     * [generateVoucherObject 生成凭证信息对象]
     * @param  int|integer $id
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]             [凭证信息]
     */
    public static function generateVoucherObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Voucher {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $voucher = new Voucher($id);

        //type
        $type = self::generateType($faker, $value);
        $voucher->setType($type);
        //number
        $number = self::generateNumber($faker, $value);
        $voucher->setNumber($number);
        //attachments
        $attachments = self::generateAttachments($faker, $value);
        $voucher->setAttachments($attachments);
        //accountTemplate
        $accountTemplate = self::generateAccountTemplate($faker, $value);
        $voucher->setAccountTemplate($accountTemplate);
        //member
        $member = self::generateMember($faker, $value);
        $voucher->setMember($member);
        //voucherRecords
        $voucherRecords = self::generateVoucherRecords($faker, $value);
        foreach ($voucherRecords as $voucherRecord) {
            $voucher->addVoucherRecord($voucherRecord);
        }
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $voucher->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $voucher->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $voucher->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $voucher->setStatus($status);

        return $voucher;
    }

    private static function generateType($faker, array $value = array())
    {
        return $type = isset($attributes['type']) ?
        $attributes['type'] : Voucher::TYPE['JI'];
    }

    private static function generateNumber($faker, array $value = array())
    {
        return $number = isset($attributes['number']) ?
        $attributes['number'] : $faker->numerify();
    }

    private static function generateAttachments($faker, array $value = array())
    {
        return $attachments = isset($attributes['attachments']) ?
        $attributes['attachments'] : array();
    }

    private static function generateVoucherRecords($faker, array $value = array())
    {
        return $voucherRecords = isset($attributes['voucherRecords']) ?
        $attributes['voucherRecords'] : array(new VoucherRecord());
    }

    private static function generateAccountTemplate($faker, array $value = array())
    {
        return $accountTemplate = isset($attributes['accountTemplate']) ?
            $attributes['accountTemplate'] :
            \Sdk\AccountTemplate\Utils\MockFactory::generateAccountTemplateObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generateMember($faker, array $value = array())
    {
        return $member = isset($attributes['member']) ?
            $attributes['member'] :
            \Sdk\Member\Utils\MockFactory::generateMemberObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
