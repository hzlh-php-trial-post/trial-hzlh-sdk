<?php
namespace Sdk\Voucher\Repository;

use Sdk\Voucher\Adapter\Voucher\VoucherRestfulAdapter;
use Sdk\Voucher\Adapter\Voucher\VoucherMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class VoucherRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(VoucherRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends VoucherRepository {
            public function getAdapter() : VoucherRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : VoucherMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Voucher\Adapter\Voucher\VoucherRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Voucher\Adapter\Voucher\VoucherMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为VoucherRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以VoucherRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(VoucherRestfulAdapter::class);
        $adapter->scenario(Argument::exact(VoucherRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(VoucherRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为VoucherRestfulAdapter建立预言
     * 建立预期状况：insert() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行insert（）
     * 判断 result 是否为true
     */
    public function testInsert()
    {
        $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherObject(1);

        $adapter = $this->prophesize(VoucherRestfulAdapter::class);
        $adapter->insert($voucher)->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->insert($voucher);
        $this->assertTrue($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为VoucherRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletes()
    {
        $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherObject(1);

        $adapter = $this->prophesize(VoucherRestfulAdapter::class);
        $adapter->deletes($voucher)->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deletes($voucher);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 为VoucherRestfulAdapter建立预言
     * 建立预期状况：arrange() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行arrange（）
     * 判断 result 是否为true
     */
    public function testArrange()
    {
        $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherObject(1);

        $adapter = $this->prophesize(VoucherRestfulAdapter::class);
        $adapter->arrange($voucher)->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->arrange($voucher);
        $this->assertTrue($result);
    }
}
