<?php
namespace Sdk\Voucher\Translator;

use Sdk\Voucher\Model\NullVoucher;
use Sdk\Voucher\Model\Voucher;
use Sdk\Voucher\Model\IdentityInfo;
use Sdk\Common\Model\IdentifyCard;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\Member\Model\Member;

use Sdk\AccountTemplate\Translator\AccountTemplateRestfulTranslator;
use Sdk\AccountTemplate\Model\AccountTemplate;

class VoucherRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            VoucherRestfulTranslator::class
        )->setMethods([
            'getMemberRestfulTranslator',
            'getAccountTemplateRestfulTranslator',
            'getVoucherRecordRestfulTranslator'
        ])->getMock();

        $this->childStub =
        new class extends VoucherRestfulTranslator {
            public function getMemberRestfulTranslator() : MemberRestfulTranslator
            {
                return parent::getMemberRestfulTranslator();
            }
            public function getAccountTemplateRestfulTranslator() : AccountTemplateRestfulTranslator
            {
                return parent::getAccountTemplateRestfulTranslator();
            }
            public function getVoucherRecordRestfulTranslator() : VoucherRecordRestfulTranslator
            {
                return parent::getVoucherRecordRestfulTranslator();
            }
        };
    }

    public function testGetMemberRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberRestfulTranslator',
            $this->childStub->getMemberRestfulTranslator()
        );
    }

    public function testGetAccountTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\AccountTemplate\Translator\AccountTemplateRestfulTranslator',
            $this->childStub->getAccountTemplateRestfulTranslator()
        );
    }

    public function testGetVoucherRecordRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Voucher\Translator\VoucherRecordRestfulTranslator',
            $this->childStub->getVoucherRecordRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Voucher());
        $this->assertInstanceOf('Sdk\Voucher\Model\NullVoucher', $result);
    }

    public function setMethods(Voucher $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['type'])) {
            $expectObject->setType($attributes['type']);
        }
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['generationDate'])) {
            $expectObject->setGenerationDate($attributes['generationDate']);
        }
        if (isset($attributes['attachments'])) {
            $expectObject->setAttachments($attributes['attachments']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($relationships['member']['data'])) {
            $expectObject->setMember(new Member($relationships['member']['data']['id']));
        }
        if (isset($relationships['accountTemplate']['data'])) {
            $expectObject->setAccountTemplate(
                new AccountTemplate($relationships['accountTemplate']['data']['id'])
            );
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherArray();

        $data =  $voucher['data'];
        $relationships = $data['relationships'];

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $accountTemplate = new AccountTemplate($relationships['accountTemplate']['data']['id']);
        $accountTemplateRestfulTranslator = $this->prophesize(AccountTemplateRestfulTranslator::class);
        $accountTemplateRestfulTranslator->arrayToObject(Argument::exact($relationships['accountTemplate']))
            ->shouldBeCalledTimes(1)->willReturn($accountTemplate);

        $this->stub->expects($this->exactly(1))
            ->method('getAccountTemplateRestfulTranslator')
            ->willReturn($accountTemplateRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($voucher);

        $expectObject = new Voucher();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherArray();
        $data =  $voucher['data'];
        $relationships = $data['relationships'];

        $accountTemplate = new AccountTemplate($relationships['accountTemplate']['data']['id']);
        $accountTemplateRestfulTranslator = $this->prophesize(AccountTemplateRestfulTranslator::class);
        $accountTemplateRestfulTranslator->arrayToObject(Argument::exact($relationships['accountTemplate']))
            ->shouldBeCalledTimes(1)->willReturn($accountTemplate);

        $this->stub->expects($this->exactly(1))
            ->method('getAccountTemplateRestfulTranslator')
            ->willReturn($accountTemplateRestfulTranslator->reveal());

        $member = new Member($relationships['member']['data']['id']);
        $memberRestfulTranslator = $this->prophesize(MemberRestfulTranslator::class);
        $memberRestfulTranslator->arrayToObject(Argument::exact($relationships['member']))
            ->shouldBeCalledTimes(1)->willReturn($member);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberRestfulTranslator')
            ->willReturn($memberRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($voucher);

        $expectArray = array();

        $expectObject = new Voucher();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $voucher = \Sdk\Voucher\Utils\MockFactory::generateVoucherObject(1, 1);

        $actual = $this->stub->objectToArray(
            $voucher,
            array(
                'type',
                'number',
                'generationDate',
                'attachments',
                'accountTemplate',
                'member'
            )
        );

        $expectedArray = array(
            'data'=>array(
                'type'=>'vouchers'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'type'=>$voucher->getType(),
            'number'=>$voucher->getNumber(),
            'generationDate'=>$voucher->getGenerationDate(),
            'attachments'=>$voucher->getAttachments()
        );

        $expectedArray['data']['relationships']['member']['data'] = array(
            array(
                'type' => 'members',
                'id' => $voucher->getMember()->getId()
            )
        );
        $expectedArray['data']['relationships']['accountTemplate']['data'] = array(
            array(
                'type' => 'accountTemplates',
                'id' => $voucher->getAccountTemplate()->getId()
            )
        );

        $this->assertEquals($expectedArray, $actual);
    }
}
