<?php
namespace Sdk\AccountTemplate\Model;

use Sdk\AccountTemplate\Repository\AccountTemplateRepository;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Member\Model\Member;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class AccountTemplateTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AccountTemplate::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends AccountTemplate{
            public function getRepository() : AccountTemplateRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\AccountTemplate\Repository\AccountTemplateRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //industry 测试 ---------------------------------------------------------- start
    /**
     * 设置setIndustry() 正确的传参类型,期望传值正确
     */
    public function testSetIndustryCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setIndustry($object);
        $this->assertEquals($object, $this->stub->getIndustry());
    }

    /**
     * 设置setIndustry() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIndustryWrongType()
    {
        $this->stub->setIndustry(array(1, 2, 3));
    }
    //industry 测试 ----------------------------------------------------------   end
    
    //name 测试 ---------------------------------------------------------- start
    /**
     * 设置setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }
    //name 测试 ----------------------------------------------------------   end

    //typeVat 测试 ---------------------------------------------------------- start
    /**
     * 设置setTypeVat() 正确的传参类型,期望传值正确
     */
    public function testSetTypeVatCorrectType()
    {
        $this->stub->setTypeVat(1);
        $this->assertEquals(1, $this->stub->getTypeVat());
    }

    /**
     * 设置setTypeVat() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetTypeVatWrongTypeButNumeric()
    {
        $this->stub->setTypeVat('1');
        $this->assertTrue(is_int($this->stub->getTypeVat()));
        $this->assertEquals(1, $this->stub->getTypeVat());
    }
    //typeVat 测试 ----------------------------------------------------------   end

    //activationDate 测试 ---------------------------------------------------------- start
    /**
     * 设置setActivationDate() 正确的传参类型,期望传值正确
     */
    public function testSetActivationDateCorrectType()
    {
        $this->stub->setActivationDate(1);
        $this->assertEquals(1, $this->stub->getActivationDate());
    }

    /**
     * 设置setActivationDate() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetActivationDateWrongTypeButNumeric()
    {
        $this->stub->setActivationDate('1');
        // $this->assertTrue(is_int($this->stub->getActivationDate()));
        $this->assertEquals(1, $this->stub->getActivationDate());
    }
    //activationDate 测试 ----------------------------------------------------------   end
    
    //accountingStandard 测试 ---------------------------------------------------------- start
    /**
     * 设置setAccountingStandard() 正确的传参类型,期望传值正确
     */
    public function testSetAccountingStandardCorrectType()
    {
        $this->stub->setAccountingStandard(1);
        $this->assertEquals(1, $this->stub->getAccountingStandard());
    }

    /**
     * 设置setAccountingStandard() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetAccountingStandardWrongTypeButNumeric()
    {
        $this->stub->setAccountingStandard('1');
        $this->assertTrue(is_int($this->stub->getAccountingStandard()));
        $this->assertEquals(1, $this->stub->getAccountingStandard());
    }
    //accountingStandard 测试 ----------------------------------------------------------   end
    
    //voucherApproval 测试 ---------------------------------------------------------- start
    /**
     * 设置setVoucherApproval() 正确的传参类型,期望传值正确
     */
    public function testSetVoucherApprovalCorrectType()
    {
        $this->stub->setVoucherApproval(1);
        $this->assertEquals(1, $this->stub->getVoucherApproval());
    }

    /**
     * 设置setVoucherApproval() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetVoucherApprovalWrongTypeButNumeric()
    {
        $this->stub->setVoucherApproval('1');
        $this->assertTrue(is_int($this->stub->getVoucherApproval()));
        $this->assertEquals(1, $this->stub->getVoucherApproval());
    }
    //voucherApproval 测试 ----------------------------------------------------------   end

    //enterprise 测试 -------------------------------------------------------- start
    /**
     * 设置 setEnterprise() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseCorrectType()
    {
        $object = new \Sdk\Enterprise\Model\Enterprise();

        $this->stub->setEnterprise($object);
        $this->assertSame($object, $this->stub->getEnterprise());
    }

    /**
     * 设置 setEnterprise() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseType()
    {
        $this->stub->setEnterprise(array(1,2,3));
    }
    //enterprise 测试 -------------------------------------------------------- end
    
    //删除
    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(AccountTemplate::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(AccountTemplateRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFailure()
    {
        $this->stub = $this->getMockBuilder(AccountTemplate::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(AccountTemplateRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }
}
