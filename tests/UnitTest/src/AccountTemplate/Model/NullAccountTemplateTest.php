<?php
namespace Sdk\AccountTemplate\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullAccountTemplateTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullAccountTemplate::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsAccountTemplate()
    {
        $this->assertInstanceof('Sdk\AccountTemplate\Model\AccountTemplate', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
