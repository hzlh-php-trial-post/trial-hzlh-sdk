<?php
namespace Sdk\AccountTemplate\Translator;

use Sdk\AccountTemplate\Model\NullAccountTemplate;
use Sdk\AccountTemplate\Model\AccountTemplate;
use Sdk\AccountTemplate\Model\Answer;
use Sdk\AccountTemplate\Model\DeleteInfo;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Dictionary\Model\Dictionary;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

use Sdk\AccountTemplate\Utils\MockFactory;

class AccountTemplateRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            AccountTemplateRestfulTranslator::class
        )
            ->setMethods([
                'getEnterpriseRestfulTranslator',
                'getDictionaryRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends AccountTemplateRestfulTranslator {
            public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
            {
                return parent::getEnterpriseRestfulTranslator();
            }
            public function getDictionaryRestfulTranslator() : DictionaryRestfulTranslator
            {
                return parent::getDictionaryRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->childStub->getEnterpriseRestfulTranslator()
        );
    }

    public function testGetDictionaryRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Translator\DictionaryRestfulTranslator',
            $this->childStub->getDictionaryRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new AccountTemplate());
        $this->assertInstanceOf('Sdk\AccountTemplate\Model\NullAccountTemplate', $result);
    }

    public function setMethods(AccountTemplate $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['name'])) {
            $expectObject->setName($attributes['name']);
        }
        if (isset($attributes['typeVat'])) {
            $expectObject->setTypeVat($attributes['typeVat']);
        }
        if (isset($attributes['activationDate'])) {
            $expectObject->setActivationDate($attributes['activationDate']);
        }
        if (isset($attributes['currentPeriod'])) {
            $expectObject->setCurrentPeriod($attributes['currentPeriod']);
        }
        if (isset($attributes['accountingStandard'])) {
            $expectObject->setAccountingStandard($attributes['accountingStandard']);
        }
        if (isset($attributes['voucherApproval'])) {
            $expectObject->setVoucherApproval($attributes['voucherApproval']);
        }
        if (isset($relationships['industry']['data'])) {
            $expectObject->setIndustry(new Dictionary($relationships['industry']['data']['id']));
        }
        if (isset($relationships['enterprise']['data'])) {
            $expectObject->setEnterprise(new Enterprise($relationships['enterprise']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $accountTemplate = MockFactory::generateAccountTemplateArray();

        $data =  $accountTemplate['data'];
        $relationships = $data['relationships'];

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $dictionary = new Dictionary($relationships['industry']['data']['id']);
        $dictionaryRestfulTranslator = $this->prophesize(DictionaryRestfulTranslator::class);
        $dictionaryRestfulTranslator->arrayToObject(Argument::exact($relationships['industry']))
            ->shouldBeCalledTimes(1)->willReturn($dictionary);
        $this->stub->expects($this->exactly(1))
            ->method('getDictionaryRestfulTranslator')
            ->willReturn($dictionaryRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($accountTemplate);

        $expectObject = new AccountTemplate();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $accountTemplate = MockFactory::generateAccountTemplateArray();
        $data =  $accountTemplate['data'];
        $relationships = $data['relationships'];

        $dictionary = new Dictionary($relationships['industry']['data']['id']);
        $dictionaryRestfulTranslator = $this->prophesize(DictionaryRestfulTranslator::class);
        $dictionaryRestfulTranslator->arrayToObject(Argument::exact($relationships['industry']))
            ->shouldBeCalledTimes(1)->willReturn($dictionary);
        $this->stub->expects($this->exactly(1))
            ->method('getDictionaryRestfulTranslator')
            ->willReturn($dictionaryRestfulTranslator->reveal());

        $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
            ->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseRestfulTranslator')
            ->willReturn($enterpriseRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($accountTemplate);
        $expectArray = array();

        $expectObject = new AccountTemplate();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $accountTemplate = MockFactory::generateAccountTemplateObject(1, 1);

        $actual = $this->stub->objectToArray(
            $accountTemplate,
            array(
                'name',
                'typeVat',
                'activationDate',
                'accountingStandard',
                'voucherApproval',
                'industry',
                'enterprise'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'accountTemplates'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'name'=>$accountTemplate->getName(),
            'typeVat'=>$accountTemplate->getTypeVat(),
            'activationDate'=>$accountTemplate->getActivationDate(),
            'accountingStandard'=>$accountTemplate->getAccountingStandard(),
            'voucherApproval'=>$accountTemplate->getVoucherApproval()
        );

        $expectedArray['data']['relationships']['industry']['data'] = array(
            array(
                'type' => 'dictionaries',
                'id' => $accountTemplate->getIndustry()->getId()
            )
        );
        $expectedArray['data']['relationships']['enterprise']['data'] = array(
            array(
                'type' => 'enterprises',
                'id' => $accountTemplate->getEnterprise()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
