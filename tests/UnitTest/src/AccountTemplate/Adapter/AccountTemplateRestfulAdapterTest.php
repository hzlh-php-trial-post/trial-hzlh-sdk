<?php
namespace Sdk\AccountTemplate\Adapter\AccountTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\AccountTemplate\Model\AccountTemplate;
use Sdk\AccountTemplate\Model\NullAccountTemplate;
use Sdk\AccountTemplate\Utils\MockFactory;
use Sdk\AccountTemplate\Translator\AccountTemplateRestfulTranslator;

class AccountTemplateRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AccountTemplateRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends AccountTemplateRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIAccountTemplateAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\AccountTemplate\Adapter\AccountTemplate\IAccountTemplateAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('accountTemplates', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\AccountTemplate\Translator\AccountTemplateRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'ACCOUNT_TEMPLATE_LIST',
                AccountTemplateRestfulAdapter::SCENARIOS['ACCOUNT_TEMPLATE_LIST']
            ],
            [
                'ACCOUNT_TEMPLATE_FETCH_ONE',
                AccountTemplateRestfulAdapter::SCENARIOS['ACCOUNT_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $accountTemplate = MockFactory::generateAccountTemplateObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullAccountTemplate())
            ->willReturn($accountTemplate);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($accountTemplate, $result);
    }
    /**
     * 为AccountTemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$accountTemplate$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareAccountTemplateTranslator(
        AccountTemplate $accountTemplate,
        array $keys,
        array $accountTemplateArray
    ) {
        $translator = $this->prophesize(AccountTemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($accountTemplate),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($accountTemplateArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(AccountTemplate $accountTemplate)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($accountTemplate);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $accountTemplate = MockFactory::generateAccountTemplateObject(1);
        $accountTemplateArray = array();

        $this->prepareAccountTemplateTranslator(
            $accountTemplate,
            array(
                'name',
                'industry',
                'typeVat',
                'activationDate',
                'accountingStandard',
                'voucherApproval',
                'enterprise'
            ),
            $accountTemplateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('accountTemplates', $accountTemplateArray);

        $this->success($accountTemplate);

        $result = $this->stub->add($accountTemplate);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $accountTemplate = MockFactory::generateAccountTemplateObject(1);
        $accountTemplateArray = array();

        $this->prepareAccountTemplateTranslator(
            $accountTemplate,
            array(
                'name',
                'industry',
                'typeVat',
                'activationDate',
                'accountingStandard',
                'voucherApproval',
                'enterprise'
            ),
            $accountTemplateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('accountTemplates', $accountTemplateArray);

        $this->failure($accountTemplate);
        $result = $this->stub->add($accountTemplate);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $accountTemplate = MockFactory::generateAccountTemplateObject(1);
        $accountTemplateArray = array();

        $this->prepareAccountTemplateTranslator(
            $accountTemplate,
            array(
                'name',
                'industry',
                'typeVat',
                'activationDate',
                'currentPeriod',
                'voucherApproval'
            ),
            $accountTemplateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('accountTemplates/'.$accountTemplate->getId(), $accountTemplateArray);

        $this->success($accountTemplate);

        $result = $this->stub->edit($accountTemplate);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $accountTemplate = MockFactory::generateAccountTemplateObject(1);
        $accountTemplateArray = array();

        $this->prepareAccountTemplateTranslator(
            $accountTemplate,
            array(
                'name',
                'industry',
                'typeVat',
                'activationDate',
                'currentPeriod',
                'voucherApproval'
            ),
            $accountTemplateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('accountTemplates/'.$accountTemplate->getId(), $accountTemplateArray);

        $this->failure($accountTemplate);
        $result = $this->stub->edit($accountTemplate);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $accountTemplate = MockFactory::generateAccountTemplateObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'accountTemplates/'.$accountTemplate->getId().'/delete'
            );

        $this->success($accountTemplate);

        $result = $this->stub->deletes($accountTemplate);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $accountTemplate = MockFactory::generateAccountTemplateObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'accountTemplates/'.$accountTemplate->getId().'/delete'
            );

        $this->failure($accountTemplate);
        $result = $this->stub->deletes($accountTemplate);
        $this->assertFalse($result);
    }
}
