<?php
namespace Sdk\ProfessionalTitle\Model;

use Sdk\ProfessionalTitle\Repository\ProfessionalTitleRepository;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Member\Model\Member;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IResubmitAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;

use Sdk\Common\Model\IApplyAble;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class ProfessionalTitleTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ProfessionalTitle::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter',
                'getIResubmitAbleAdapter',
                'getIApplyAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends ProfessionalTitle{
            public function getRepository() : ProfessionalTitleRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
            public function getIResubmitAbleAdapter() : IResubmitAbleAdapter
            {
                return parent::getIResubmitAbleAdapter();
            }
            public function getIApplyAbleAdapter() : IApplyAbleAdapter
            {
                return parent::getIApplyAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ProfessionalTitle\Repository\ProfessionalTitleRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    public function testGetIResubmitAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IResubmitAbleAdapter',
            $this->childStub->getIResubmitAbleAdapter()
        );
    }

    public function testGetIApplyAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IApplyAbleAdapter',
            $this->childStub->getIApplyAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //parentCategory 测试 ---------------------------------------------------------- start
    /**
     * 设置setParentCategory() 正确的传参类型,期望传值正确
     */
    public function testSetParentCategoryCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setParentCategory($object);
        $this->assertEquals($object, $this->stub->getParentCategory());
    }

    /**
     * 设置setParentCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetParentCategoryWrongType()
    {
        $this->stub->setParentCategory(array(1, 2, 3));
    }
    //parentCategory 测试 ----------------------------------------------------------   end

    //category 测试 ---------------------------------------------------------- start
    /**
     * 设置setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setCategory($object);
        $this->assertEquals($object, $this->stub->getCategory());
    }

    /**
     * 设置setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory(array(1, 2, 3));
    }
    //category 测试 ----------------------------------------------------------   end

    //type 测试 ---------------------------------------------------------- start
    /**
     * 设置setType() 正确的传参类型,期望传值正确
     */
    public function testSetTypeCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setType($object);
        $this->assertEquals($object, $this->stub->getType());
    }

    /**
     * 设置setType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTypeWrongType()
    {
        $this->stub->setType(array(1, 2, 3));
    }
    //type 测试 ----------------------------------------------------------   end

    //certificates 测试 ---------------------------------------------------------- start
    /**
     * 设置setCertificates() 正确的传参类型,期望传值正确
     */
    public function testSetCertificatesCorrectType()
    {
        $this->stub->setCertificates(array());
        $this->assertEquals(array(), $this->stub->getCertificates());
    }

    /**
     * 设置setCertificates() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCertificatesWrongType()
    {
        $this->stub->setCertificates('1');
    }
    //certificates 测试 ----------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $object = new \Sdk\Staff\Model\Staff();

        $this->stub->setStaff($object);
        $this->assertSame($object, $this->stub->getStaff());
    }

    /**
     * 设置 setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffType()
    {
        $this->stub->setStaff(array(1,2,3));
    }
    //staff 测试 -------------------------------------------------------- end
    
    //enterpriseName 测试 ---------------------------------------------------------- start
    /**
     * 设置setEnterpriseName() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseNameCorrectType()
    {
        $this->stub->setEnterpriseName('string');
        $this->assertEquals('string', $this->stub->getEnterpriseName());
    }

    /**
     * 设置setEnterpriseName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseNameWrongType()
    {
        $this->stub->setEnterpriseName(array());
    }
    //enterpriseName 测试 ----------------------------------------------------------   end

    //rejectReason 测试 ---------------------------------------------------------- start
    /**
     * 设置setRejectReason() 正确的传参类型,期望传值正确
     */
    public function testSetRejectReasonCorrectType()
    {
        $this->stub->setRejectReason('string');
        $this->assertEquals('string', $this->stub->getRejectReason());
    }

    /**
     * 设置setRejectReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRejectReasonWrongType()
    {
        $this->stub->setRejectReason(array());
    }
    //rejectReason 测试 ----------------------------------------------------------   end

    //applyStatus 测试 ---------------------------------------------------------- start
    /**
     * 设置setApplyStatus() 正确的传参类型,期望传值正确
     */
    public function testSetApplyStatusCorrectType()
    {
        $this->stub->setApplyStatus(IApplyAble::APPLY_STATUS['PENDING']);
        $this->assertEquals(IApplyAble::APPLY_STATUS['PENDING'], $this->stub->getApplyStatus());
    }

    /**
     * 设置setApplyStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyStatusWrongType()
    {
        $this->stub->setApplyStatus('string');
    }
    //applyStatus 测试 ----------------------------------------------------------   end

    public function testBatchAddSuccess()
    {
        $this->stub = $this->getMockBuilder(ProfessionalTitle::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(ProfessionalTitleRepository::class);
        $repository->batchAdd(array(Argument::exact($this->stub)))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->batchAdd(array(Argument::exact($this->stub)));
        $this->assertTrue($result);
    }

    public function testBatchAddFailure()
    {
        $this->stub = $this->getMockBuilder(ProfessionalTitle::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(ProfessionalTitleRepository::class);
        $repository->batchAdd(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->batchAdd(array());
        $this->assertFalse($result);
    }

    //删除
    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(ProfessionalTitle::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(ProfessionalTitleRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFailure()
    {
        $this->stub = $this->getMockBuilder(ProfessionalTitle::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(ProfessionalTitleRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }
}
