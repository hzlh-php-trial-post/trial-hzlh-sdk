<?php
namespace Sdk\ProfessionalTitle\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullProfessionalTitleTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullProfessionalTitle::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsProfessionalTitle()
    {
        $this->assertInstanceof('Sdk\ProfessionalTitle\Model\ProfessionalTitle', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
