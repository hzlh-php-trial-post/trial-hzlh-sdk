<?php
namespace Sdk\ProfessionalTitle\Adapter\ProfessionalTitle;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\ProfessionalTitle\Model\ProfessionalTitle;
use Sdk\ProfessionalTitle\Model\NullProfessionalTitle;
use Sdk\ProfessionalTitle\Utils\MockFactory;
use Sdk\ProfessionalTitle\Translator\ProfessionalTitleRestfulTranslator;

class ProfessionalTitleRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ProfessionalTitleRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends ProfessionalTitleRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIProfessionalTitleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\ProfessionalTitle\Adapter\ProfessionalTitle\IProfessionalTitleAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('professionalTitles', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ProfessionalTitle\Translator\ProfessionalTitleRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_PROFESSIONAL_TITLE_LIST',
                ProfessionalTitleRestfulAdapter::SCENARIOS['OA_PROFESSIONAL_TITLE_LIST']
            ],
            [
                'PORTAL_PROFESSIONAL_TITLE_LIST',
                ProfessionalTitleRestfulAdapter::SCENARIOS['PORTAL_PROFESSIONAL_TITLE_LIST']
            ],
            [
                'PROFESSIONAL_TITLE_FETCH_ONE',
                ProfessionalTitleRestfulAdapter::SCENARIOS['PROFESSIONAL_TITLE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $professionalTitle = MockFactory::generateProfessionalTitleObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullProfessionalTitle())
            ->willReturn($professionalTitle);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($professionalTitle, $result);
    }
    /**
     * 为ProfessionalTitleRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$professionalTitle$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareProfessionalTitleTranslator(
        ProfessionalTitle $professionalTitle,
        array $keys,
        array $professionalTitleArray
    ) {
        $translator = $this->prophesize(ProfessionalTitleRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($professionalTitle),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($professionalTitleArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(ProfessionalTitle $professionalTitle)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($professionalTitle);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareProfessionalTitleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleObject(1);
        $professionalTitleArray = array();

        $this->prepareProfessionalTitleTranslator(
            $professionalTitle,
            array(
                'type',
                'certificates'
            ),
            $professionalTitleArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('professionalTitles/'.$professionalTitle->getId(), $professionalTitleArray);

        $this->success($professionalTitle);

        $result = $this->stub->edit($professionalTitle);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareProfessionalTitleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleObject(1);
        $professionalTitleArray = array();

        $this->prepareProfessionalTitleTranslator(
            $professionalTitle,
            array(
                'type',
                'certificates'
            ),
            $professionalTitleArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('professionalTitles/'.$professionalTitle->getId(), $professionalTitleArray);

        $this->failure($professionalTitle);
        $result = $this->stub->edit($professionalTitle);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareProfessionalTitleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行resubmit（）
     * 判断 result 是否为true
     */
    public function testResubmitSuccess()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleObject(1);
        $professionalTitleArray = array();

        $this->prepareProfessionalTitleTranslator(
            $professionalTitle,
            array(
                'type',
                'certificates'
            ),
            $professionalTitleArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('professionalTitles/'.$professionalTitle->getId().'/resubmit', $professionalTitleArray);

        $this->success($professionalTitle);

        $result = $this->stub->resubmit($professionalTitle);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareProfessionalTitleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行resubmit（）
     * 判断 result 是否为false
     */
    public function testResubmitFailure()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleObject(1);
        $professionalTitleArray = array();

        $this->prepareProfessionalTitleTranslator(
            $professionalTitle,
            array(
                'type',
                'certificates'
            ),
            $professionalTitleArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('professionalTitles/'.$professionalTitle->getId().'/resubmit', $professionalTitleArray);

        $this->failure($professionalTitle);
        $result = $this->stub->resubmit($professionalTitle);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareProfessionalTitleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'professionalTitles/'.$professionalTitle->getId().'/delete'
            );

        $this->success($professionalTitle);

        $result = $this->stub->deletes($professionalTitle);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareProfessionalTitleTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'professionalTitles/'.$professionalTitle->getId().'/delete'
            );

        $this->failure($professionalTitle);
        $result = $this->stub->deletes($professionalTitle);
        $this->assertFalse($result);
    }
}
