<?php
namespace Sdk\ProfessionalTitle\Translator;

use Sdk\ProfessionalTitle\Model\NullProfessionalTitle;
use Sdk\ProfessionalTitle\Model\ProfessionalTitle;

use Sdk\Staff\Model\Staff;
use Sdk\Dictionary\Model\Dictionary;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Staff\Translator\StaffRestfulTranslator;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

use Sdk\ProfessionalTitle\Utils\MockFactory;

class ProfessionalTitleRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            ProfessionalTitleRestfulTranslator::class
        )
            ->setMethods([
                'getStaffRestfulTranslator',
                'getDictionaryRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends ProfessionalTitleRestfulTranslator {
            public function getStaffRestfulTranslator() : StaffRestfulTranslator
            {
                return parent::getStaffRestfulTranslator();
            }
            public function getDictionaryRestfulTranslator() : DictionaryRestfulTranslator
            {
                return parent::getDictionaryRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetStaffRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Staff\Translator\StaffRestfulTranslator',
            $this->childStub->getStaffRestfulTranslator()
        );
    }

    public function testGetDictionaryRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Translator\DictionaryRestfulTranslator',
            $this->childStub->getDictionaryRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new ProfessionalTitle());
        $this->assertInstanceOf('Sdk\ProfessionalTitle\Model\NullProfessionalTitle', $result);
    }

    public function setMethods(ProfessionalTitle $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['certificates'])) {
            $expectObject->setCertificates($attributes['certificates']);
        }
        if (isset($attributes['enterpriseName'])) {
            $expectObject->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['rejectReason'])) {
            $expectObject->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['applyStatus'])) {
            $expectObject->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($relationships['parentCategory']['data'])) {
            $expectObject->setParentCategory(new Dictionary($relationships['parentCategory']['data']['id']));
        }
        if (isset($relationships['staff']['data'])) {
            $expectObject->setStaff(new Staff($relationships['staff']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleArray();

        $data =  $professionalTitle['data'];
        $relationships = $data['relationships'];

        $staff = new Staff($relationships['staff']['data']['id']);
        $staffRestfulTranslator = $this->prophesize(StaffRestfulTranslator::class);
        $staffRestfulTranslator->arrayToObject(Argument::exact($relationships['staff']))
            ->shouldBeCalledTimes(1)->willReturn($staff);
        $this->stub->expects($this->exactly(1))
            ->method('getStaffRestfulTranslator')
            ->willReturn($staffRestfulTranslator->reveal());

        $parentCategory = new Dictionary($relationships['parentCategory']['data']['id']);
        $parentCategoryRestfulTranslator = $this->prophesize(DictionaryRestfulTranslator::class);
        $parentCategoryRestfulTranslator->arrayToObject(Argument::exact($relationships['parentCategory']))
            ->shouldBeCalledTimes(1)->willReturn($parentCategory);
        $this->stub->expects($this->any())
            ->method('getDictionaryRestfulTranslator')
            ->willReturn($parentCategoryRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($professionalTitle);

        $expectObject = new ProfessionalTitle();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleArray();
        $data =  $professionalTitle['data'];
        $relationships = $data['relationships'];

        $parentCategory = new Dictionary($relationships['parentCategory']['data']['id']);
        $parentCategoryRestfulTranslator = $this->prophesize(DictionaryRestfulTranslator::class);
        $parentCategoryRestfulTranslator->arrayToObject(Argument::exact($relationships['parentCategory']))
            ->shouldBeCalledTimes(1)->willReturn($parentCategory);
        $this->stub->expects($this->exactly(1))
            ->method('getDictionaryRestfulTranslator')
            ->willReturn($parentCategoryRestfulTranslator->reveal());

        $staff = new Staff($relationships['staff']['data']['id']);
        $staffRestfulTranslator = $this->prophesize(StaffRestfulTranslator::class);
        $staffRestfulTranslator->arrayToObject(Argument::exact($relationships['staff']))
            ->shouldBeCalledTimes(1)->willReturn($staff);
        $this->stub->expects($this->exactly(1))
            ->method('getStaffRestfulTranslator')
            ->willReturn($staffRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($professionalTitle);
        $expectArray = array();

        $expectObject = new ProfessionalTitle();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $professionalTitle = MockFactory::generateProfessionalTitleObject(1, 1);

        $actual = $this->stub->objectToArray(
            $professionalTitle,
            array(
                'certificates',
                'enterpriseName',
                'rejectReason',
                'staff',
                'parentCategory',
                'category',
                'type',
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'professionalTitles'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'certificates'=>$professionalTitle->getCertificates(),
            'enterpriseName'=>$professionalTitle->getEnterpriseName(),
            'rejectReason'=>$professionalTitle->getRejectReason()
        );

        $expectedArray['data']['relationships']['staff']['data'] = array(
            array(
                'type' => 'staffs',
                'id' => $professionalTitle->getStaff()->getId()
            )
        );
        $expectedArray['data']['relationships']['parentCategory']['data'] = array(
            array(
                'type' => 'dictionaries',
                'id' => $professionalTitle->getParentCategory()->getId()
            )
        );
        $expectedArray['data']['relationships']['category']['data'] = array(
            array(
                'type' => 'dictionaries',
                'id' => $professionalTitle->getCategory()->getId()
            )
        );
        $expectedArray['data']['relationships']['type']['data'] = array(
            array(
                'type' => 'dictionaries',
                'id' => $professionalTitle->getType()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
