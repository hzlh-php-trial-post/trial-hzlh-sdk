<?php
namespace Sdk\Bank\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class BankTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new Bank();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Bank setId() 正确的传参类型,期望传值正确
     */
    public function testSetBankIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 User setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetBankIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 ---------------------------------------------------------- start
    /**
     * 设置 Bank setName() 正确的传参类型,期望传值正确
     */
    public function testSetBankNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置 Bank setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBankNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }
    //name 测试 ----------------------------------------------------------   end
    
    //logo 测试 ---------------------------------------------------------- start
    /**
     * 设置 Bank setLogo() 正确的传参类型,期望传值正确
     */
    public function testSetBankLogoCorrectType()
    {
        $this->stub->setLogo(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getLogo());
    }

    /**
     * 设置 Bank setLogo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBankLogoWrongType()
    {
        $this->stub->setLogo('string');
    }
    //logo 测试 ----------------------------------------------------------   end
    
    //image 测试 ---------------------------------------------------------- start
    /**
     * 设置 Bank setImage() 正确的传参类型,期望传值正确
     */
    public function testSetBankImageCorrectType()
    {
        $this->stub->setImage(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getImage());
    }

    /**
     * 设置 Bank setImage() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetBankImageWrongType()
    {
        $this->stub->setImage('string');
    }
    //image 测试 ----------------------------------------------------------   end
    
    //status 测试 ---------------------------------------------------------- start
    /**
     * 设置 Bank setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetBankStatusCorrectType()
    {
        $this->stub->setStatus(1);
        $this->assertEquals(1, $this->stub->getStatus());
    }

    /**
     * 设置 User setStatus() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetBankStatusWrongTypeButNumeric()
    {
        $this->stub->setStatus('1');
        $this->assertEquals(1, $this->stub->getStatus());
    }
    //status 测试 ----------------------------------------------------------   end
}
