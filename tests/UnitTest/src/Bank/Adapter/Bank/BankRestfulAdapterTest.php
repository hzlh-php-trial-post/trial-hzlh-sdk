<?php
namespace Sdk\Bank\Adapter\Bank;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Bank\Model\Bank;
use Sdk\Bank\Model\NullBank;
use Sdk\Bank\Utils\MockFactory;
use Sdk\Bank\Translator\BankRestfulTranslator;

class BankRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(BankRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction'
            ])->getMock();

        $this->childStub = new class extends BankRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIBankAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Bank\Adapter\Bank\IBankAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('banks', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Bank\Translator\BankRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'BANK_LIST',
                BankRestfulAdapter::SCENARIOS['BANK_LIST']
            ],
            [
                'BANK_FETCH_ONE',
                BankRestfulAdapter::SCENARIOS['BANK_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $bank = MockFactory::generateBankObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullBank())
            ->willReturn($bank);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($bank, $result);
    }
}
