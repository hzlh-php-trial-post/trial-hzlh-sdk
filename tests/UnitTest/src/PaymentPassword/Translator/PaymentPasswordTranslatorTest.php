<?php
namespace Sdk\PaymentPassword\Translator;

use Sdk\PaymentPassword\Model\NullPaymentPassword;
use Sdk\PaymentPassword\Model\PaymentPassword;
use Sdk\PaymentPassword\Model\IdentityInfo;
use Sdk\Common\Model\IdentifyCard;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;

use Sdk\MemberAccount\Model\MemberAccount;

class PaymentPasswordTranslatorTest extends TestCase
{

    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(PaymentPasswordRestfulTranslator::class)
                ->setMethods([
                    'getMemberAccountRestfulTranslator'
                ])
                ->getMock();

        $this->childStub = new class extends PaymentPasswordRestfulTranslator {
            public function getMemberAccountRestfulTranslator() : MemberAccountRestfulTranslator
            {
                return parent::getMemberAccountRestfulTranslator();
            }
        };
    }

    public function testGetMemberAccountRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator',
            $this->childStub->getMemberAccountRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new PaymentPassword());
        $this->assertInstanceOf('Sdk\PaymentPassword\Model\NullPaymentPassword', $result);
    }

    private function setMethods(PaymentPassword $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['cellphone'])) {
            $expectObject->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['password'])) {
            $expectObject->setPassword($attributes['password']);
        }
        if (isset($attributes['oldPassword'])) {
            $expectObject->setOldPassword($attributes['oldPassword']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['memberAccount']['data'])) {
            $expectObject->setMemberAccount(new MemberAccount($relationships['memberAccount']['data']['id']));
        }

        return $expectObject;
    }

    private function prepareMemberAccount($relationships)
    {
        $memberAccount = new MemberAccount($relationships['memberAccount']['data']['id']);
        $memberAccountRestfulTranslator = $this->prophesize(MemberAccountRestfulTranslator::class);
        $memberAccountRestfulTranslator->arrayToObject(Argument::exact($relationships['memberAccount']))
            ->shouldBeCalledTimes(1)->willReturn($memberAccount);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberAccountRestfulTranslator')
            ->willReturn($memberAccountRestfulTranslator->reveal());
    }

    public function testArrayToObjectCorrectObject()
    {
        $paymentPassword = \Sdk\PaymentPassword\Utils\MockFactory::generatePaymentPasswordArray();

        $data =  $paymentPassword['data'];
        $relationships = $data['relationships'];

        $this->prepareMemberAccount($relationships);

        $actual = $this->stub->arrayToObject($paymentPassword);

        $expectObject = new PaymentPassword();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);
        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $paymentPassword = \Sdk\PaymentPassword\Utils\MockFactory::generatePaymentPasswordArray();
        $data =  $paymentPassword['data'];
        $relationships = $data['relationships'];

        $this->prepareMemberAccount($relationships);

        $actual = $this->stub->arrayToObjects($paymentPassword);

        $expectArray = array();

        $expectObject = new PaymentPassword();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $paymentPassword = \Sdk\PaymentPassword\Utils\MockFactory::generatePaymentPasswordObject(1, 1);

        $actual = $this->stub->objectToArray($paymentPassword);

        $expectedArray = array(
            'data'=>array(
                'type'=>'paymentPasswords'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'cellphone'=>$paymentPassword->getCellphone(),
            'password'=>$paymentPassword->getPassword(),
            'oldPassword'=>$paymentPassword->getOldPassword()
        );

        $expectedArray['data']['relationships']['memberAccount']['data'] = array(
            array(
                'type' => 'memberAccounts',
                'id' => $paymentPassword->getMemberAccount()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
