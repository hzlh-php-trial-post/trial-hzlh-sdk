<?php
namespace Sdk\AccountSubject\Model;

use Sdk\AccountSubject\Repository\AccountSubjectRepository;

use Sdk\Dictionary\Model\Dictionary;
use Sdk\Member\Model\Member;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class AccountSubjectTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AccountSubject::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends AccountSubject{
            public function getRepository() : AccountSubjectRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\AccountSubject\Repository\AccountSubjectRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //number 测试 ---------------------------------------------------------- start
    /**
     * 设置setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('string');
        $this->assertEquals('string', $this->stub->getNumber());
    }

    /**
     * 设置setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array(1, 2, 3));
    }
    //number 测试 ----------------------------------------------------------   end
    
    //name 测试 ---------------------------------------------------------- start
    /**
     * 设置setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }
    //name 测试 ----------------------------------------------------------   end

    //parentId 测试 ---------------------------------------------------------- start
    /**
     * 设置setTypeVat() 正确的传参类型,期望传值正确
     */
    public function testSetParentIdCorrectType()
    {
        $this->stub->setParentId(1);
        $this->assertEquals(1, $this->stub->getParentId());
    }

    /**
     * 设置setParentId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetParentIdWrongTypeButNumeric()
    {
        $this->stub->setParentId('1');
        $this->assertTrue(is_int($this->stub->getParentId()));
        $this->assertEquals(1, $this->stub->getParentId());
    }
    //parentId 测试 ----------------------------------------------------------   end

    //category 测试 ---------------------------------------------------------- start
    /**
     * 设置setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->stub->setCategory(1);
        $this->assertEquals(1, $this->stub->getCategory());
    }

    /**
     * 设置setCategory() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetCategoryWrongTypeButNumeric()
    {
        $this->stub->setCategory('1');
        $this->assertTrue(is_int($this->stub->getCategory()));
        $this->assertEquals(1, $this->stub->getCategory());
    }
    //category 测试 ----------------------------------------------------------   end
    
    //balanceDirection 测试 ---------------------------------------------------------- start
    /**
     * 设置setBalanceDirection() 正确的传参类型,期望传值正确
     */
    public function testSetBalanceDirectionCorrectType()
    {
        $this->stub->setBalanceDirection(1);
        $this->assertEquals(1, $this->stub->getBalanceDirection());
    }

    /**
     * 设置setAccountingStandard() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetBalanceDirectionWrongTypeButNumeric()
    {
        $this->stub->setBalanceDirection('1');
        $this->assertTrue(is_int($this->stub->getBalanceDirection()));
        $this->assertEquals(1, $this->stub->getBalanceDirection());
    }
    //balanceDirection 测试 ----------------------------------------------------------   end
    
    //level 测试 ---------------------------------------------------------- start
    /**
     * 设置setLevel() 正确的传参类型,期望传值正确
     */
    public function testSetLevelCorrectType()
    {
        $this->stub->setLevel(1);
        $this->assertEquals(1, $this->stub->getLevel());
    }

    /**
     * 设置setLevel() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetLevelWrongTypeButNumeric()
    {
        $this->stub->setLevel('1');
        $this->assertTrue(is_int($this->stub->getLevel()));
        $this->assertEquals(1, $this->stub->getLevel());
    }
    //level 测试 ----------------------------------------------------------   end

    //enterprise 测试 -------------------------------------------------------- start
    // /**
    //  * 设置 setEnterprise() 正确的传参类型,期望传值正确
    //  */
    // public function testSetEnterpriseCorrectType()
    // {
    //     $object = new \Sdk\Enterprise\Model\Enterprise();

    //     $this->stub->setEnterprise($object);
    //     $this->assertSame($object, $this->stub->getEnterprise());
    // }

    // /**
    //  * 设置 setEnterprise() 错误的传参类型,期望期望抛出TypeError exception
    //  *
    //  * @expectedException TypeError
    //  */
    // public function testSetEnterpriseType()
    // {
    //     $this->stub->setEnterprise(array(1,2,3));
    // }
    //enterprise 测试 -------------------------------------------------------- end
    
    //删除
    public function testDeletesSuccess()
    {
        $this->stub = $this->getMockBuilder(AccountSubject::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(AccountSubjectRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deletes();
        $this->assertTrue($result);
    }

    public function testDeletesFailure()
    {
        $this->stub = $this->getMockBuilder(AccountSubject::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $repository = $this->prophesize(AccountSubjectRepository::class);
        $repository->deletes(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deletes();
        $this->assertFalse($result);
    }
}
