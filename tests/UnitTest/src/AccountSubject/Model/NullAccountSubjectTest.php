<?php
namespace Sdk\AccountSubject\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullAccountSubjectTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullAccountSubject::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsAccountSubject()
    {
        $this->assertInstanceof('Sdk\AccountSubject\Model\AccountSubject', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
