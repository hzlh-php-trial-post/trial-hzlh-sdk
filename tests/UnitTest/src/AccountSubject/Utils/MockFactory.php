<?php
namespace Sdk\AccountSubject\Utils;

use Sdk\AccountSubject\Model\AccountSubject;
use Sdk\AccountTemplate\Model\AccountTemplate;

use Sdk\Enterprise\Model\Enterprise;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateAccountSubjectArray]
     * @return [array]
     */
    public static function generateAccountSubjectArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $accountSubject = array();

        $accountSubject = array(
            'data'=>array(
                'type'=>'accountSubjects',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //number
        $number = self::generateNumber($faker, $value);
        $attributes['number'] = $number;
        //parentId
        $parentId = self::generateParentId($faker, $value);
        $attributes['parentId'] = $parentId;
        //category
        $category = self::generateCategory($faker, $value);
        $attributes['category'] = $category;
        //balanceDirection
        $balanceDirection = self::generateBalanceDirection($faker, $value);
        $attributes['balanceDirection'] = $balanceDirection;
        //level
        $level = self::generateLevel($faker, $value);
        $attributes['level'] = $level;
        //accountTemplate
        $accountTemplate = self::generateAccountTemplate($faker, $value);
        $attributes['accountTemplate'] = $accountTemplate;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $accountSubject['data']['attributes'] = $attributes;

        //enterprise
        // $accountSubject['data']['relationships']['enterprise']['data'] = array(
        //     'type' => 'enterprises',
        //     'id' => $faker->randomNumber(1)
        // );

        //accountTemplate
        $accountSubject['data']['relationships']['accountTemplate']['data'] = array(
            'type' => 'accountTemplates',
            'id' => $faker->randomNumber(1)
        );
        
        return $accountSubject;
    }
    /**
     * [generateAccountSubjectObject 生成账套对象]
     * @param  int|integer $id    [账套Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [账套对象]
     */
    public static function generateAccountSubjectObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : AccountSubject {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $accountSubject = new AccountSubject($id);

        //name
        $name = self::generateName($faker, $value);
        $accountSubject->setName($name);
        //number
        $number = self::generateNumber($faker, $value);
        $accountSubject->setNumber($number);
        //parentId
        $parentId = self::generateParentId($faker, $value);
        $accountSubject->setParentId($parentId);
        //category
        $category = self::generateCategory($faker, $value);
        $accountSubject->setCategory($category);
        //balanceDirection
        $balanceDirection = self::generateBalanceDirection($faker, $value);
        $accountSubject->setBalanceDirection($balanceDirection);
        //level
        $level = self::generateLevel($faker, $value);
        $accountSubject->setLevel($level);
        //accountTemplate
        $accountTemplate = self::generateAccountTemplate($faker, $value);
        $accountSubject->setAccountTemplate($accountTemplate);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $accountSubject->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $accountSubject->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $accountSubject->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $accountSubject->setStatus($status);
        //enterprise
        // $enterprise = self::generateEnterprise($faker,$value);
        // $accountSubject->setEnterprise($enterprise);

        return $accountSubject;
    }

    private static function generateName($faker, array $value = array())
    {
        return $name = isset($value['name']) ?
        $value['name'] : $faker->name;
    }

    private static function generateNumber($faker, array $value = array())
    {
        return $number = isset($value['number']) ?
        $value['number'] : '100101';
    }

    private static function generateParentId($faker, array $value = array())
    {
        return $parentId = isset($value['parentId']) ?
        $value['parentId'] : $faker->numberBetween($min = 0, $max = 3);
    }

    private static function generateCategory($faker, array $value = array())
    {
        return $category = isset($value['category']) ?
        $value['category'] : $faker->numberBetween($min = 0, $max = 5);
    }

    private static function generateBalanceDirection($faker, array $value = array())
    {
        return $balanceDirection = isset($value['balanceDirection']) ?
        $value['balanceDirection'] : $faker->numberBetween($min = 1, $max = 2);
    }

    private static function generateLevel($faker, array $value = array())
    {
        return $level = isset($value['level']) ?
        $value['level'] : $faker->numberBetween($min = 1, $max = 4);
    }

    private static function generateEnterprise($faker, array $value = array())
    {
        return $enterprise = isset($value['enterprise']) ?
        $value['enterprise'] : \Sdk\Enterprise\Utils\EnterpriseMockFactory::generateEnterpriseObject(
            new Enterprise()
        );
    }

    private static function generateAccountTemplate($faker, array $value = array())
    {
        return $accountTemplate = isset($value['accountTemplate']) ?
        $value['accountTemplate'] : new AccountTemplate();
    }
}
