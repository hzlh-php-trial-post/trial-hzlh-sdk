<?php
namespace Sdk\AccountSubject\Repository;

use Sdk\AccountSubject\Adapter\AccountSubject\AccountSubjectRestfulAdapter;
use Sdk\AccountSubject\Adapter\AccountSubject\AccountSubjectMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class AccountSubjectRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AccountSubjectRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends AccountSubjectRepository {
            public function getAdapter() : AccountSubjectRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : AccountSubjectMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\AccountSubject\Adapter\AccountSubject\AccountSubjectRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\AccountSubject\Adapter\AccountSubject\AccountSubjectMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为AccountSubjectRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以AccountSubjectRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(AccountSubjectRestfulAdapter::class);
        $adapter->scenario(Argument::exact(AccountSubjectRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(AccountSubjectRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    /**
     * 生成模拟数据，传参为1
     * 为AccountSubjectRestfulAdapter建立预言
     * 建立预期状况：deletes() 方法将会被调用一次，并以模拟数据为参数，期望返回true
     * 揭示预言中的getAdapter，并将仿件对象链接到主体上
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testDeletes()
    {
        $accountSubject = \Sdk\AccountSubject\Utils\MockFactory::generateAccountSubjectObject(1);

        $adapter = $this->prophesize(AccountSubjectRestfulAdapter::class);
        $adapter->deletes(
            Argument::exact($accountSubject)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->deletes($accountSubject);
        $this->assertTrue($result);
    }
}
