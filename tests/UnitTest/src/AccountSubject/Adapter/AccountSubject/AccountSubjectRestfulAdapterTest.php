<?php
namespace Sdk\AccountSubject\Adapter\AccountSubject;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\AccountSubject\Model\AccountSubject;
use Sdk\AccountSubject\Model\NullAccountSubject;
use Sdk\AccountSubject\Utils\MockFactory;
use Sdk\AccountSubject\Translator\AccountSubjectRestfulTranslator;

class AccountSubjectRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AccountSubjectRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'delete',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends AccountSubjectRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIAccountSubjectAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\AccountSubject\Adapter\AccountSubject\IAccountSubjectAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('accountSubjects', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\AccountSubject\Translator\AccountSubjectRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'ACCOUNT_SUBJECT_LIST',
                AccountSubjectRestfulAdapter::SCENARIOS['ACCOUNT_SUBJECT_LIST']
            ],
            [
                'ACCOUNT_SUBJECT_FETCH_ONE',
                AccountSubjectRestfulAdapter::SCENARIOS['ACCOUNT_SUBJECT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $accountSubject = MockFactory::generateAccountSubjectObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullAccountSubject())
            ->willReturn($accountSubject);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($accountSubject, $result);
    }
    /**
     * 为AccountSubjectRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$accountSubject$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareAccountSubjectTranslator(
        AccountSubject $accountSubject,
        array $keys,
        array $accountSubjectArray
    ) {
        $translator = $this->prophesize(AccountSubjectRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($accountSubject),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($accountSubjectArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(AccountSubject $accountSubject)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($accountSubject);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountSubjectTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $accountSubject = MockFactory::generateAccountSubjectObject(1);
        $accountSubjectArray = array();

        $this->prepareAccountSubjectTranslator(
            $accountSubject,
            array(
                'name',
                'number',
                'parentId',
                'category',
                'balanceDirection',
                'status',
                'accountTemplate'
            ),
            $accountSubjectArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('accountSubjects/'.$accountSubject->getId(), $accountSubjectArray);

        $this->success($accountSubject);

        $result = $this->stub->add($accountSubject);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountSubjectTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $accountSubject = MockFactory::generateAccountSubjectObject(1);
        $accountSubjectArray = array();

        $this->prepareAccountSubjectTranslator(
            $accountSubject,
            array(
                'name',
                'number',
                'parentId',
                'category',
                'balanceDirection',
                'status',
                'accountTemplate'
            ),
            $accountSubjectArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('accountSubjects/'.$accountSubject->getId(), $accountSubjectArray);

        $this->failure($accountSubject);
        $result = $this->stub->add($accountSubject);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountSubjectTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $accountSubject = MockFactory::generateAccountSubjectObject(1);
        $accountSubjectArray = array();

        $this->prepareAccountSubjectTranslator(
            $accountSubject,
            array(
                'name',
                'status'
            ),
            $accountSubjectArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('accountSubjects/'.$accountSubject->getSubjectId(), $accountSubjectArray);

        $this->success($accountSubject);

        $result = $this->stub->edit($accountSubject);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountSubjectTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $accountSubject = MockFactory::generateAccountSubjectObject(1);
        $accountSubjectArray = array();

        $this->prepareAccountSubjectTranslator(
            $accountSubject,
            array(
                'name',
                'status'
            ),
            $accountSubjectArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('accountSubjects/'.$accountSubject->getSubjectId(), $accountSubjectArray);

        $this->failure($accountSubject);
        $result = $this->stub->edit($accountSubject);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountSubjectTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deletes（）
     * 判断 result 是否为true
     */
    public function testDeletesSuccess()
    {
        $accountSubject = MockFactory::generateAccountSubjectObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'accountSubjects/'.$accountSubject->getSubjectId().'/delete'
            );

        $this->success($accountSubject);

        $result = $this->stub->deletes($accountSubject);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareAccountSubjectTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deletes（）
     * 判断 result 是否为false
     */
    public function testDeletesFailure()
    {
        $accountSubject = MockFactory::generateAccountSubjectObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('delete')
            ->with(
                'accountSubjects/'.$accountSubject->getSubjectId().'/delete'
            );

        $this->failure($accountSubject);
        $result = $this->stub->deletes($accountSubject);
        $this->assertFalse($result);
    }
}
