<?php
namespace Sdk\AccountSubject\Translator;

use Sdk\AccountSubject\Model\NullAccountSubject;
use Sdk\AccountSubject\Model\AccountSubject;

use Sdk\Enterprise\Model\Enterprise;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\AccountSubject\Utils\MockFactory;
use Sdk\AccountTemplate\Model\AccountTemplate;

class AccountSubjectRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            AccountSubjectRestfulTranslator::class
        )
            ->setMethods([
                'getAccountTemplateRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends AccountSubjectRestfulTranslator {
            public function getAccountTemplateRestfulTranslator()
            {
                return parent::getAccountTemplateRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetAccountTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\AccountTemplate\Translator\AccountTemplateRestfulTranslator',
            $this->childStub->getAccountTemplateRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new AccountSubject());
        $this->assertInstanceOf('Sdk\AccountSubject\Model\NullAccountSubject', $result);
    }

    public function setMethods(AccountSubject $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['name'])) {
            $expectObject->setName($attributes['name']);
        }
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['parentId'])) {
            $expectObject->setParentId($attributes['parentId']);
        }
        if (isset($attributes['category'])) {
            $expectObject->setCategory($attributes['category']);
        }
        if (isset($attributes['balanceDirection'])) {
            $expectObject->setBalanceDirection($attributes['balanceDirection']);
        }
        if (isset($attributes['level'])) {
            $expectObject->setLevel($attributes['level']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        // if (isset($relationships['enterprise']['data'])) {
        //     $expectObject->setEnterprise(new Enterprise($relationships['enterprise']['data']['id']));
        // }
        if (isset($relationships['accountTemplate']['data'])) {
            $expectObject->setAccountTemplate(new AccountTemplate($relationships['accountTemplate']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $accountSubject = MockFactory::generateAccountSubjectArray();

        $data =  $accountSubject['data'];
        $relationships = $data['relationships'];

        // $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        // $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        // $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
        //     ->shouldBeCalledTimes(1)->willReturn($enterprise);
        // $this->stub->expects($this->exactly(1))
        //     ->method('getEnterpriseRestfulTranslator')
        //     ->willReturn($enterpriseRestfulTranslator->reveal());

        $accountTemplate = new AccountTemplate($relationships['accountTemplate']['data']['id']);
        $accountTemplateRestfulTranslator = $this->prophesize(AccountSubjectRestfulTranslator::class);
        $accountTemplateRestfulTranslator->arrayToObject(Argument::exact($relationships['accountTemplate']))
            ->shouldBeCalledTimes(1)->willReturn($accountTemplate);
        $this->stub->expects($this->exactly(1))
            ->method('getAccountTemplateRestfulTranslator')
            ->willReturn($accountTemplateRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($accountSubject);

        $expectObject = new AccountSubject();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $accountSubject = MockFactory::generateAccountSubjectArray();
        $data =  $accountSubject['data'];
        $relationships = $data['relationships'];

        // $enterprise = new Enterprise($relationships['enterprise']['data']['id']);
        // $enterpriseRestfulTranslator = $this->prophesize(EnterpriseRestfulTranslator::class);
        // $enterpriseRestfulTranslator->arrayToObject(Argument::exact($relationships['enterprise']))
        //     ->shouldBeCalledTimes(1)->willReturn($enterprise);
        // $this->stub->expects($this->exactly(1))
        //     ->method('getEnterpriseRestfulTranslator')
        //     ->willReturn($enterpriseRestfulTranslator->reveal());

        $accountTemplate = new AccountTemplate($relationships['accountTemplate']['data']['id']);
        $accountTemplateRestfulTranslator = $this->prophesize(AccountSubjectRestfulTranslator::class);
        $accountTemplateRestfulTranslator->arrayToObject(Argument::exact($relationships['accountTemplate']))
            ->shouldBeCalledTimes(1)->willReturn($accountTemplate);
        $this->stub->expects($this->exactly(1))
            ->method('getAccountTemplateRestfulTranslator')
            ->willReturn($accountTemplateRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($accountSubject);
        $expectArray = array();

        $expectObject = new AccountSubject();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $accountSubject = MockFactory::generateAccountSubjectObject(1, 1);

        $actual = $this->stub->objectToArray(
            $accountSubject,
            array(
                'name',
                'number',
                'parentId',
                'category',
                'balanceDirection',
                'status',
                'accountTemplate'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'accountSubjects'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'name'=>$accountSubject->getName(),
            'number'=>$accountSubject->getNumber(),
            'parentId'=>$accountSubject->getParentId(),
            'category'=>$accountSubject->getCategory(),
            'balanceDirection'=>$accountSubject->getBalanceDirection(),
            'status'=>$accountSubject->getStatus()
        );

        // $expectedArray['data']['relationships']['enterprise']['data'] = array(
        //     array(
        //         'type' => 'enterprises',
        //         'id' => $accountSubject->getEnterprise()->getId()
        //     )
        // );

        $expectedArray['data']['relationships']['accountTemplate']['data'] = array(
            array(
                'type' => 'accountTemplates',
                'id' => $accountSubject->getAccountTemplate()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
