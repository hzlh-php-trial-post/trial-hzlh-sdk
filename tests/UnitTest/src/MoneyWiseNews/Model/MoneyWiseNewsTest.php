<?php
namespace Sdk\MoneyWiseNews\Model;

use Sdk\MoneyWiseNews\Repository\MoneyWiseNewsRepository;

use Sdk\Crew\Model\Crew;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IOnShelfAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

class MoneyWiseNewsTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MoneyWiseNews::class)
            ->setMethods([
                'getRepository',
                'getIOperatAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends MoneyWiseNews{
            public function getRepository() : MoneyWiseNewsRepository
            {
                return parent::getRepository();
            }
            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }
            public function getIOnShelfAbleAdapter() : IOnShelfAbleAdapter
            {
                return parent::getIOnShelfAbleAdapter();
            }
            public function getITopAbleAdapter() : ITopAbleAdapter
            {
                return parent::getITopAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\MoneyWiseNews\Repository\MoneyWiseNewsRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperatAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    public function testGetIOnShelfAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOnShelfAbleAdapter',
            $this->childStub->getIOnShelfAbleAdapter()
        );
    }

    public function testGetITopAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\ITopAbleAdapter',
            $this->childStub->getITopAbleAdapter()
        );
    }

    //number 测试 ---------------------------------------------------------- start
    /**
     * 设置setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('string');
        $this->assertEquals('string', $this->stub->getNumber());
    }

    /**
     * 设置setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array(1, 2, 3));
    }
    //number 测试 ----------------------------------------------------------   end

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    
    //title 测试 ---------------------------------------------------------- start
    /**
     * 设置setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->stub->setTitle('string');
        $this->assertEquals('string', $this->stub->getTitle());
    }

    /**
     * 设置setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->stub->setTitle(array(1, 2, 3));
    }
    //title 测试 ----------------------------------------------------------   end

    //source 测试 ---------------------------------------------------------- start
    /**
     * 设置setSource() 正确的传参类型,期望传值正确
     */
    public function testSetSourceCorrectType()
    {
        $this->stub->setSource('string');
        $this->assertEquals('string', $this->stub->getSource());
    }

    /**
     * 设置setSource() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceWrongType()
    {
        $this->stub->setSource(array(1, 2, 3));
    }
    //source 测试 ----------------------------------------------------------   end

    //description 测试 ---------------------------------------------------------- start
    /**
     * 设置setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->stub->setDescription('string');
        $this->assertEquals('string', $this->stub->getDescription());
    }

    /**
     * 设置setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->stub->setDescription(array(1, 2, 3));
    }
    //description 测试 ----------------------------------------------------------   end

    //detail 测试 ---------------------------------------------------------- start
    /**
     * 设置setDetail() 正确的传参类型,期望传值正确
     */
    public function testSetDetailCorrectType()
    {
        $this->stub->setDetail(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getDetail());
    }

    /**
     * 设置setDetail() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDetailWrongType()
    {
        $this->stub->setDetail('string');
    }
    //detail 测试 ----------------------------------------------------------   end

    //collection 测试 ---------------------------------------------------------- start
    /**
     * 设置setCollection() 正确的传参类型,期望传值正确
     */
    public function testSetCollectionCorrectType()
    {
        $this->stub->setCollection(1);
        $this->assertEquals(1, $this->stub->getCollection());
    }

    /**
     * 设置setCollection() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCollectionWrongType()
    {
        $this->stub->setCollection('string');
    }
    //collection 测试 ----------------------------------------------------------   end

    //cover 测试 ---------------------------------------------------------- start
    /**
     * 设置setCover() 正确的传参类型,期望传值正确
     */
    public function testSetCoverCorrectType()
    {
        $this->stub->setCover(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getCover());
    }

    /**
     * 设置setCover() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCoverWrongType()
    {
        $this->stub->setCover('string');
    }
    //cover 测试 ----------------------------------------------------------   end

    //pageViews 测试 ---------------------------------------------------------- start
    /**
     * 设置setPageViews() 正确的传参类型,期望传值正确
     */
    public function testSetPageViewsCorrectType()
    {
        $this->stub->setPageViews(1);
        $this->assertEquals(1, $this->stub->getPageViews());
    }

    /**
     * 设置setPageViews() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPageViewsWrongType()
    {
        $this->stub->setPageViews('string');
    }
    //pageViews 测试 ----------------------------------------------------------   end

    //category 测试 ---------------------------------------------------------- start
    /**
     * 设置setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $object = new \Sdk\Dictionary\Model\Dictionary();

        $this->stub->setCategory($object);
        $this->assertEquals($object, $this->stub->getCategory());
    }

    /**
     * 设置setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory('string');
    }
    //category 测试 ----------------------------------------------------------   end

    //crew 测试 ---------------------------------------------------------- start
    /**
     * 设置setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();

        $this->stub->setCrew($object);
        $this->assertEquals($object, $this->stub->getCrew());
    }

    /**
     * 设置setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->stub->setCrew(array(1, 2, 3));
    }
    //crew 测试 ----------------------------------------------------------   end
}
