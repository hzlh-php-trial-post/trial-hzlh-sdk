<?php
namespace Sdk\MoneyWiseNews\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullMoneyWiseNewsTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullMoneyWiseNews::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsMoneyWiseNews()
    {
        $this->assertInstanceof('Sdk\MoneyWiseNews\Model\MoneyWiseNews', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
