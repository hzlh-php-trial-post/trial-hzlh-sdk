<?php
namespace Sdk\MoneyWiseNews\Translator;

use Sdk\MoneyWiseNews\Model\NullMoneyWiseNews;
use Sdk\MoneyWiseNews\Model\MoneyWiseNews;

use Sdk\Crew\Model\Crew;
use Sdk\Dictionary\Model\Dictionary;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Crew\Translator\CrewRestfulTranslator;
use Sdk\Dictionary\Translator\DictionaryRestfulTranslator;

use Sdk\MoneyWiseNews\Utils\MockFactory;

class MoneyWiseNewsRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            MoneyWiseNewsRestfulTranslator::class
        )
            ->setMethods([
                'getCrewRestfulTranslator',
                'getDictionaryRestfulTranslator'

            ])->getMock();

        $this->childStub =
        new class extends MoneyWiseNewsRestfulTranslator {
            public function getCrewRestfulTranslator() : CrewRestfulTranslator
            {
                return parent::getCrewRestfulTranslator();
            }
            public function getDictionaryRestfulTranslator() : DictionaryRestfulTranslator
            {
                return parent::getDictionaryRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetCrewRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Crew\Translator\CrewRestfulTranslator',
            $this->childStub->getCrewRestfulTranslator()
        );
    }

    public function testGetDictionaryRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Dictionary\Translator\DictionaryRestfulTranslator',
            $this->childStub->getDictionaryRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new MoneyWiseNews());
        $this->assertInstanceOf('Sdk\MoneyWiseNews\Model\NullMoneyWiseNews', $result);
    }

    public function setMethods(MoneyWiseNews $expectObject, array $attributes, array $relationships)
    {
        if (isset($attributes['title'])) {
            $expectObject->setTitle($attributes['title']);
        }
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['source'])) {
            $expectObject->setSource($attributes['source']);
        }
        if (isset($attributes['cover'])) {
            $expectObject->setCover($attributes['cover']);
        }
        if (isset($attributes['detail'])) {
            $expectObject->setDetail($attributes['detail']);
        }
        if (isset($attributes['description'])) {
            $expectObject->setDescription($attributes['description']);
        }
        if (isset($attributes['collection'])) {
            $expectObject->setCollection($attributes['collection']);
        }
        if (isset($attributes['pageViews'])) {
            $expectObject->setPageViews($attributes['pageViews']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }

        if (isset($relationships['crew']['data'])) {
            $expectObject->setCrew(new Crew($relationships['crew']['data']['id']));
        }
        if (isset($relationships['category']['data'])) {
            $expectObject->setCategory(new Dictionary($relationships['category']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsArray();

        $data =  $moneyWiseNews['data'];
        $relationships = $data['relationships'];

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);
        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $category = new Dictionary($relationships['category']['data']['id']);
        $categoryRestfulTranslator = $this->prophesize(DictionaryRestfulTranslator::class);
        $categoryRestfulTranslator->arrayToObject(Argument::exact($relationships['category']))
            ->shouldBeCalledTimes(1)->willReturn($category);
        $this->stub->expects($this->exactly(1))
            ->method('getDictionaryRestfulTranslator')
            ->willReturn($categoryRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($moneyWiseNews);

        $expectObject = new MoneyWiseNews();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsArray();
        $data =  $moneyWiseNews['data'];
        $relationships = $data['relationships'];

        $category = new Dictionary($relationships['category']['data']['id']);
        $categoryRestfulTranslator = $this->prophesize(DictionaryRestfulTranslator::class);
        $categoryRestfulTranslator->arrayToObject(Argument::exact($relationships['category']))
            ->shouldBeCalledTimes(1)->willReturn($category);
        $this->stub->expects($this->exactly(1))
            ->method('getDictionaryRestfulTranslator')
            ->willReturn($categoryRestfulTranslator->reveal());

        $crew = new Crew($relationships['crew']['data']['id']);
        $crewRestfulTranslator = $this->prophesize(CrewRestfulTranslator::class);
        $crewRestfulTranslator->arrayToObject(Argument::exact($relationships['crew']))
            ->shouldBeCalledTimes(1)->willReturn($crew);
        $this->stub->expects($this->exactly(1))
            ->method('getCrewRestfulTranslator')
            ->willReturn($crewRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($moneyWiseNews);
        $expectArray = array();

        $expectObject = new MoneyWiseNews();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }
        /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }

    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1, 1);

        $actual = $this->stub->objectToArray(
            $moneyWiseNews,
            array(
                'title',
                'source',
                'detail',
                'description',
                'cover',
                'crew',
                'category'
            )
        );
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'news'
            )
        );

        $expectedArray['data']['attributes'] = array(
            'title'=>$moneyWiseNews->getTitle(),
            'source'=>$moneyWiseNews->getSource(),
            'detail'=>$moneyWiseNews->getDetail(),
            'description'=>$moneyWiseNews->getDescription(),
            'cover'=>$moneyWiseNews->getCover()
        );

        $expectedArray['data']['relationships']['crew']['data'] = array(
            array(
                'type' => 'crews',
                'id' => $moneyWiseNews->getCrew()->getId()
            )
        );
        $expectedArray['data']['relationships']['category']['data'] = array(
            array(
                'type' => 'dictionaries',
                'id' => $moneyWiseNews->getCategory()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
