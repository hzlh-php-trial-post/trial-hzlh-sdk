<?php
namespace Sdk\MoneyWiseNews\Repository;

use Sdk\MoneyWiseNews\Adapter\MoneyWiseNews\MoneyWiseNewsRestfulAdapter;
use Sdk\MoneyWiseNews\Adapter\MoneyWiseNews\MoneyWiseNewsMockAdapter;

use PHPUnit\Framework\TestCase;

use Prophecy\Argument;

class MoneyWiseNewsRepositoryTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MoneyWiseNewsRepository::class)
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends MoneyWiseNewsRepository {
            public function getAdapter() : MoneyWiseNewsRestfulAdapter
            {
                return parent::getAdapter();
            }
            public function getMockAdapter() : MoneyWiseNewsMockAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\MoneyWiseNews\Adapter\MoneyWiseNews\MoneyWiseNewsRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }
    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\MoneyWiseNews\Adapter\MoneyWiseNews\MoneyWiseNewsMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }
    /**
     * 为MoneyWiseNewsRestfulAdapter建立预言
     * 建立预期状况：scenario() 方法将会被调用一次，并以MoneyWiseNewsRepository::LIST_MODEL_UN为参数
     * 揭示预言，并将仿件对象链接到主体上。
     * 执行scenario
     * 判断执行的$this->stub和$result是否相等，不相等则抛出异常
     */
    public function testScenario()
    {
        $adapter = $this->prophesize(MoneyWiseNewsRestfulAdapter::class);
        $adapter->scenario(Argument::exact(MoneyWiseNewsRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);//phpcs:ignore

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(MoneyWiseNewsRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
