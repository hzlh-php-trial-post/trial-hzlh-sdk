<?php
namespace Sdk\MoneyWiseNews\Utils;

use Sdk\MoneyWiseNews\Model\MoneyWiseNews;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateMoneyWiseNewsArray 生成用户信息数组]
     * @return [array] [用户数组]
     */
    public static function generateMoneyWiseNewsArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $moneyWiseNews = array();

        $moneyWiseNews = array(
            'data'=>array(
                'type'=>'moneyWiseNewss',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //title
        $title = self::generateTitle($faker, $value);
        $attributes['title'] = $title;
        //number
        $number = self::generateNumber($faker, $value);
        $attributes['number'] = $number;
        //source
        $source = self::generateSource($faker, $value);
        $attributes['source'] = $source;
        //detail
        $detail = self::generateDetaile($faker, $value);
        $attributes['detail'] = $detail;
        //description
        $description = self::generateDescription($faker, $value);
        $attributes['description'] = $description;
        //cover
        $cover = self::generateCover($faker, $value);
        $attributes['cover'] = $cover;
        //collection
        $collection = self::generateCollection($faker, $value);
        $attributes['collection'] = $collection;
        //pageViews
        $pageViews = self::generatePageViews($faker, $value);
        $attributes['pageViews'] = $pageViews;

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $moneyWiseNews['data']['attributes'] = $attributes;
        //crew
        $moneyWiseNews['data']['relationships']['crew']['data'] = array(
            'type' => 'crews',
            'id' => $faker->randomNumber(1)
        );
        //category
        $moneyWiseNews['data']['relationships']['category']['data'] = array(
            'type' => 'dictionaries',
            'id' => $faker->randomNumber(1)
        );

        return $moneyWiseNews;
    }
    /**
     * [generateMoneyWiseNewsObject 生成用户对象对象]
     * @param  int|integer $id    [用户Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [用户对象]
     */
    public static function generateMoneyWiseNewsObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : MoneyWiseNews {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $moneyWiseNews = new MoneyWiseNews($id);

        //title
        $title = self::generateTitle($faker, $value);
        $moneyWiseNews->setTitle($title);
        //number
        $number = self::generateNumber($faker, $value);
        $moneyWiseNews->setNumber($number);
        //source
        $source = self::generateSource($faker, $value);
        $moneyWiseNews->setSource($source);
        //detail
        $detail = self::generateDetaile($faker, $value);
        $moneyWiseNews->setDetail($detail);
        //description
        $description = self::generateDescription($faker, $value);
        $moneyWiseNews->setDescription($description);
        //cover
        $cover = self::generateCover($faker, $value);
        $moneyWiseNews->setCover($cover);
        //collection
        $collection = self::generateCollection($faker, $value);
        $moneyWiseNews->setCollection($collection);
        //pageViews
        $pageViews = self::generatePageViews($faker, $value);
        $moneyWiseNews->setPageViews($pageViews);
        //category
        $category = self::generateCategory($faker, $value);
        $moneyWiseNews->setCategory($category);
        //crew
        $crew = self::generateCrew($faker, $value);
        $moneyWiseNews->setCrew($crew);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $moneyWiseNews->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $moneyWiseNews->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $moneyWiseNews->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $moneyWiseNews->setStatus($status);

        return $moneyWiseNews;
    }

    private static function generateTitle($faker, array $value = array())
    {
        return isset($value['title']) ?
        $value['title'] : $faker->name;
    }

    private static function generateNumber($faker, array $value = array())
    {
        return isset($value['number']) ?
        $value['number'] : '100101';
    }

    private static function generateSource($faker, array $value = array())
    {
        return isset($value['source']) ?
        $value['source'] : $faker->word;
    }

    private static function generateDetaile($faker, array $value = array())
    {
        return isset($value['detail']) ?
        $value['detail'] : array();
    }

    private static function generateDescription($faker, array $value = array())
    {
        return isset($value['description']) ?
        $value['description'] : $faker->word;
    }

    private static function generateCover($faker, array $value = array())
    {
        return isset($value['cover']) ?
        $value['cover'] : array();
    }

    private static function generateCollection($faker, array $value = array())
    {
        return isset($value['collection']) ?
        $value['collection'] : $faker->numerify();
    }

    private static function generatePageViews($faker, array $value = array())
    {
        return isset($value['pageViews']) ?
        $value['pageViews'] : $faker->numerify();
    }

    private static function generateCategory($faker, array $value = array())
    {
        return isset($value['category']) ?
        $value['category'] : \Sdk\Dictionary\Utils\MockFactory::generateDictionaryObject(
            $faker->numerify(),
            $faker->numerify()
        );
    }

    private static function generateCrew($faker, array $value = array())
    {
        return isset($value['crew']) ?
            $value['crew'] : \Sdk\Crew\Utils\MockFactory::generateCrewObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }
}
