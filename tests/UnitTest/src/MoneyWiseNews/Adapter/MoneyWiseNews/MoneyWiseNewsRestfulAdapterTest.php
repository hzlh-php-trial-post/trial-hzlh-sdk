<?php
namespace Sdk\MoneyWiseNews\Adapter\MoneyWiseNews;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\MoneyWiseNews\Model\MoneyWiseNews;
use Sdk\MoneyWiseNews\Model\NullMoneyWiseNews;
use Sdk\MoneyWiseNews\Utils\MockFactory;
use Sdk\MoneyWiseNews\Translator\MoneyWiseNewsRestfulTranslator;

class MoneyWiseNewsRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MoneyWiseNewsRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends MoneyWiseNewsRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIMoneyWiseNewsAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\MoneyWiseNews\Adapter\MoneyWiseNews\IMoneyWiseNewsAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('news', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\MoneyWiseNews\Translator\MoneyWiseNewsRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'OA_MONEY_WISE_NEWS_LIST',
                MoneyWiseNewsRestfulAdapter::SCENARIOS['OA_MONEY_WISE_NEWS_LIST']
            ],
            [
                'PORTAL_MONEY_WISE_NEWS_LIST',
                MoneyWiseNewsRestfulAdapter::SCENARIOS['PORTAL_MONEY_WISE_NEWS_LIST']
            ],
            [
                'MONEY_WISE_NEWS_FETCH_ONE',
                MoneyWiseNewsRestfulAdapter::SCENARIOS['MONEY_WISE_NEWS_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullMoneyWiseNews())
            ->willReturn($moneyWiseNews);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($moneyWiseNews, $result);
    }
    /**
     * 为MoneyWiseNewsRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$moneyWiseNews$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareMoneyWiseNewsTranslator(
        MoneyWiseNews $moneyWiseNews,
        array $keys,
        array $moneyWiseNewsArray
    ) {
        $translator = $this->prophesize(MoneyWiseNewsRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($moneyWiseNews),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($moneyWiseNewsArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }
    private function success(MoneyWiseNews $moneyWiseNews)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($moneyWiseNews);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);
        $moneyWiseNewsArray = array();

        $this->prepareMoneyWiseNewsTranslator(
            $moneyWiseNews,
            array(
                'title',
                'source',
                'detail',
                'cover',
                'description',
                'category',
                'crew'
            ),
            $moneyWiseNewsArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('news', $moneyWiseNewsArray);

        $this->success($moneyWiseNews);

        $result = $this->stub->add($moneyWiseNews);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);
        $moneyWiseNewsArray = array();

        $this->prepareMoneyWiseNewsTranslator(
            $moneyWiseNews,
            array(
                'title',
                'source',
                'detail',
                'cover',
                'description',
                'category',
                'crew'
            ),
            $moneyWiseNewsArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('news', $moneyWiseNewsArray);

        $this->failure($moneyWiseNews);
        $result = $this->stub->add($moneyWiseNews);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);
        $moneyWiseNewsArray = array();

        $this->prepareMoneyWiseNewsTranslator(
            $moneyWiseNews,
            array(
                'title',
                'source',
                'detail',
                'cover',
                'description',
                'category'
            ),
            $moneyWiseNewsArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('news/'.$moneyWiseNews->getId(), $moneyWiseNewsArray);

        $this->success($moneyWiseNews);

        $result = $this->stub->edit($moneyWiseNews);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);
        $moneyWiseNewsArray = array();

        $this->prepareMoneyWiseNewsTranslator(
            $moneyWiseNews,
            array(
                'title',
                'source',
                'detail',
                'cover',
                'description',
                'category'
            ),
            $moneyWiseNewsArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('news/'.$moneyWiseNews->getId(), $moneyWiseNewsArray);

        $this->failure($moneyWiseNews);
        $result = $this->stub->edit($moneyWiseNews);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行onShelf（）
     * 判断 result 是否为true
     */
    public function testOnShelfSuccess()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$moneyWiseNews->getId().'/onShelf'
            );

        $this->success($moneyWiseNews);

        $result = $this->stub->onShelf($moneyWiseNews);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行onShelf（）
     * 判断 result 是否为false
     */
    public function testOnShelfFailure()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$moneyWiseNews->getId().'/onShelf'
            );

        $this->failure($moneyWiseNews);
        $result = $this->stub->onShelf($moneyWiseNews);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行offStock（）
     * 判断 result 是否为true
     */
    public function testOffStockSuccess()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$moneyWiseNews->getId().'/offStock'
            );

        $this->success($moneyWiseNews);

        $result = $this->stub->offStock($moneyWiseNews);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行offStock（）
     * 判断 result 是否为false
     */
    public function testOffStockFailure()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$moneyWiseNews->getId().'/offStock'
            );

        $this->failure($moneyWiseNews);
        $result = $this->stub->offStock($moneyWiseNews);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行top（）
     * 判断 result 是否为true
     */
    public function testTopSuccess()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$moneyWiseNews->getId().'/top'
            );

        $this->success($moneyWiseNews);

        $result = $this->stub->top($moneyWiseNews);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行top（）
     * 判断 result 是否为false
     */
    public function testTopFailure()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$moneyWiseNews->getId().'/top'
            );

        $this->failure($moneyWiseNews);
        $result = $this->stub->top($moneyWiseNews);
        $this->assertFalse($result);
    }

    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行cancelTop（）
     * 判断 result 是否为true
     */
    public function testCancelTopSuccess()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$moneyWiseNews->getId().'/cancelTop'
            );

        $this->success($moneyWiseNews);

        $result = $this->stub->cancelTop($moneyWiseNews);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareMoneyWiseNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行cancelTop（）
     * 判断 result 是否为false
     */
    public function testCancelTopFailure()
    {
        $moneyWiseNews = MockFactory::generateMoneyWiseNewsObject(1);

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$moneyWiseNews->getId().'/cancelTop'
            );

        $this->failure($moneyWiseNews);
        $result = $this->stub->cancelTop($moneyWiseNews);
        $this->assertFalse($result);
    }
}
