<?php
namespace Sdk\Deposit\Adapter\Deposit;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Deposit\Model\Deposit;
use Sdk\Deposit\Model\NullDeposit;
use Sdk\Deposit\Utils\MockFactory;
use Sdk\Deposit\Translator\DepositRestfulTranslator;

class DepositRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(DepositRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends DepositRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsIDepositAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Deposit\Adapter\Deposit\IDepositAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('deposits', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Deposit\Translator\DepositRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'DEPOSIT_LIST',
                DepositRestfulAdapter::SCENARIOS['DEPOSIT_LIST']
            ],
            [
                'DEPOSIT_FETCH_ONE',
                DepositRestfulAdapter::SCENARIOS['DEPOSIT_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $deposit = MockFactory::generateDepositObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullDeposit())
            ->willReturn($deposit);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($deposit, $result);
    }
    /**
     * 为DepositRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Deposit$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareDepositTranslator(
        Deposit $deposit,
        array $keys,
        array $depositArray
    ) {
        $translator = $this->prophesize(DepositRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($deposit),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($depositArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Deposit $deposit)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($deposit);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepositTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行deposit（）
     * 判断 result 是否为true
     */
    public function testDepositSuccess()
    {
        $deposit = MockFactory::generateDepositObject(1);
        $depositArray = array();

        $this->prepareDepositTranslator(
            $deposit,
            array(
                'amount',
                'memberAccount'
            ),
            $depositArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('deposits', $depositArray);

        $this->success($deposit);

        $result = $this->stub->deposit($deposit);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepositTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行deposit（）
     * 判断 result 是否为false
     */
    public function testDepositFailure()
    {
        $deposit = MockFactory::generateDepositObject(1);
        $depositArray = array();

        $this->prepareDepositTranslator(
            $deposit,
            array(
                'amount',
                'memberAccount'
            ),
            $depositArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('deposits', $depositArray);

        $this->failure($deposit);
        $result = $this->stub->deposit($deposit);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepositTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行pay（）
     * 判断 result 是否为true
     */
    public function testPaySuccess()
    {
        $deposit = MockFactory::generateDepositObject(1);
        $depositArray = array();

        $this->prepareDepositTranslator(
            $deposit,
            array(
                'paymentType',
                'transactionNumber',
                'transactionInfo'
            ),
            $depositArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('payments/'.$deposit->getPaymentId(), $depositArray);

        $this->success($deposit);

        $result = $this->stub->pay($deposit);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepositTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行pay（）
     * 判断 result 是否为false
     */
    public function testPayFailure()
    {
        $deposit = MockFactory::generateDepositObject(1);
        $depositArray = array();

        $this->prepareDepositTranslator(
            $deposit,
            array(
                'paymentType',
                'transactionNumber',
                'transactionInfo'
            ),
            $depositArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('payments/'.$deposit->getPaymentId(), $depositArray);

        $this->failure($deposit);
        $result = $this->stub->pay($deposit);
        $this->assertFalse($result);
    }
        /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepositTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行paymentFailure（）
     * 判断 result 是否为true
     */
    public function testPaymentFailureSuccess()
    {
        $deposit = MockFactory::generateDepositObject(1);
        $depositArray = array();

        $this->prepareDepositTranslator(
            $deposit,
            array(
                'failureReason'
            ),
            $depositArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('payments/'.$deposit->getPaymentId().'/paymentFailure', $depositArray);

        $this->success($deposit);

        $result = $this->stub->paymentFailure($deposit);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareDepositTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行paymentFailure（）
     * 判断 result 是否为false
     */
    public function testPaymentFailureFailure()
    {
        $deposit = MockFactory::generateDepositObject(1);
        $depositArray = array();

        $this->prepareDepositTranslator(
            $deposit,
            array(
                'failureReason'
            ),
            $depositArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with('payments/'.$deposit->getPaymentId().'/paymentFailure', $depositArray);

        $this->failure($deposit);
        $result = $this->stub->paymentFailure($deposit);
        $this->assertFalse($result);
    }
}
