<?php
namespace Sdk\Deposit\Utils;

use Sdk\Deposit\Model\Deposit;

use Sdk\Payment\Model\Payment;

use Sdk\Common\Model\IEnableAble;

class MockFactory
{
    /**
     * [generateDepositArray 生成充值信息数组]
     * @return [array] [充值数组]
     */
    public static function generateDepositArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $deposit = array();

        $deposit = array(
            'data'=>array(
                'type'=>'deposits',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //number
        $number = self::generateNumber($faker, $value);
        $attributes['number'] = $number;
        //paymentId
        $paymentId = self::generatePaymentId($faker, $value);
        $attributes['paymentId'] = $paymentId;
        //amount
        $amount = self::generateAmount($faker, $value);
        $attributes['amount'] = $amount;
        //failureReason
        $failureReason = self::generateFailureReason($faker, $value);
        $attributes['failureReason'] = $failureReason;
        //type
        $type = self::generateType($faker, $value);
        //transactionNumber
        $transactionNumber = self::generateTransactionNumber($faker, $value);
        //transactionInfo
        $transactionInfo = self::generateTransactionInfo($faker, $value);
        $time = time();
        $payment = new Payment(
            $type,
            $time,
            $transactionNumber,
            $transactionInfo
        );
        $attributes['payment'] = $payment;
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $deposit['data']['attributes'] = $attributes;
        //memberAccount
        $deposit['data']['relationships']['memberAccount']['data'] = array(
            'type' => 'memberAccounts',
            'id' => $faker->randomNumber(1)
        );
        //payment
        $deposit['data']['relationships']['payment']['data'] = array(
            'type' => 'payments',
            'id' => $faker->randomNumber(1)
        );

        return $deposit;
    }

    /**
     * [generateDepositObject 生成充值对象]
     * @param  int|integer $id    [充值Id]
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]           [充值对象]
     */
    public static function generateDepositObject(int $id = 0, int $seed = 0, array $value = array()) : Deposit
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $deposit = new Deposit($id);

        //number
        $number = self::generateNumber($faker, $value);
        $deposit->setNumber($number);
        //paymentId
        $paymentId = self::generatePaymentId($faker, $value);
        $deposit->setPaymentId($paymentId);
        //memberAccount
        $memberAccount = self::generateMemberAccount($faker, $value);
        $deposit->setMemberAccount($memberAccount);
        //type
        $type = self::generateType($faker, $value);
        //transactionNumber
        $transactionNumber = self::generateTransactionNumber($faker, $value);
        //transactionInfo
        $transactionInfo = self::generateTransactionInfo($faker, $value);
        $time = time();
        $payment = new Payment(
            $type,
            $time,
            $transactionNumber,
            $transactionInfo
        );
        $deposit->setPayment($payment);
        //amount
        $amount = self::generateAmount($faker, $value);
        $deposit->setAmount($amount);
        //failureReason
        $failureReason = self::generateFailureReason($faker, $value);
        $deposit->setFailureReason($failureReason);
        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $deposit->setCreateTime($createTime);
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $deposit->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $deposit->setStatusTime($statusTime);
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $deposit->setStatus($status);

        return $deposit;
    }

    private static function generateAmount($faker, array $value = array())
    {
        return $amount = isset($value['amount']) ?
        $value['amount'] : $faker->numerify();
    }

    private static function generateNumber($faker, array $value = array())
    {
        return $number = isset($value['number']) ?
        $value['number'] : $faker->word;
    }

    private static function generatePaymentId($faker, array $value = array())
    {
        return $paymentId = isset($value['paymentId']) ?
        $value['paymentId'] : '1_21';
    }

    private static function generateFailureReason($faker, array $value = array())
    {
        return $failureReason = isset($value['failureReason']) ?
        $value['failureReason'] : array(1, 2);
    }

    private static function generateMemberAccount($faker, array $value = array())
    {
        return $memberAccount = isset($value['memberAccount']) ?
            $value['memberAccount'] : \Sdk\MemberAccount\Utils\MockFactory::generateMemberAccountObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generatePayment($faker, array $value = array())
    {
        return $payment = isset($value['payment']) ?
            $value['payment'] : self::generatePaymentObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generateType($faker, array $value = array())
    {
        return $type = isset($value['type']) ?
        $value['type'] : $faker->randomElement(
            $array = array(
                Payment::PAYMENT_TYPE['WECHAT'],
                Payment::PAYMENT_TYPE['ALIPAY'],
                Payment::PAYMENT_TYPE['CHANPAY']
            )
        );
    }

    private static function generateTransactionNumber($faker, array $value = array())
    {
        return $transactionNumber = isset($value['transactionNumber']) ?
        $value['transactionNumber'] : $faker->word;
    }

    private static function generateTransactionInfo($faker, array $value = array())
    {
        unset($faker);
        return $transactionInfo = isset($value['transactionInfo']) ?
        $value['transactionInfo'] : array(1,2,3);
    }
}
