<?php
namespace Sdk\Deposit\Translator;

use Sdk\Deposit\Model\NullDeposit;
use Sdk\Deposit\Model\Deposit;
use Sdk\Payment\Model\Payment;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;
use Sdk\MemberAccount\Model\MemberAccount;

class DepositRestfulTranslatorTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            DepositRestfulTranslator::class
        )
            ->setMethods(['getMemberAccountRestfulTranslator'])
            ->getMock();

        $this->childStub =
        new class extends DepositRestfulTranslator {
            public function getMemberAccountRestfulTranslator() : MemberAccountRestfulTranslator
            {
                return parent::getMemberAccountRestfulTranslator();
            }
        };
        parent::setUp();
    }

    public function testGetMemberAccountRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator',
            $this->childStub->getMemberAccountRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(array(), new Deposit());
        $this->assertInstanceOf('Sdk\Deposit\Model\NullDeposit', $result);
    }

    public function setMethods(Deposit $expectObject, array $attributes, array $relationships)
    {
        $type = isset($attributes['paymentType'])? $attributes['paymentType'] : 0;
        $transactionNumber = isset($attributes['transactionNumber'])? $attributes['transactionNumber'] : '';
        $time = isset($attributes['paymentTime'])? $attributes['paymentTime'] : 0;
        $transactionInfo = isset($attributes['transactionInfo'])? $attributes['transactionInfo'] : array();
        $expectObject->setPayment(new Payment($type, $time, $transactionNumber, $transactionInfo));

        if (isset($attributes['amount'])) {
            $expectObject->setAmount($attributes['amount']);
        }
        if (isset($attributes['number'])) {
            $expectObject->setNumber($attributes['number']);
        }
        if (isset($attributes['failureReason'])) {
            $expectObject->setFailureReason($attributes['failureReason']);
        }
        if (isset($attributes['paymentId'])) {
            $expectObject->setPaymentId($attributes['paymentId']);
        }
        if (isset($attributes['cellphone'])) {
            $expectObject->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['isDefaultAddress'])) {
            $expectObject->setIsDefaultAddress($attributes['isDefaultAddress']);
        }
        if (isset($attributes['status'])) {
            $expectObject->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $expectObject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $expectObject->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $expectObject->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['memberAccount']['data'])) {
            $expectObject->setMemberAccount(new MemberAccount($relationships['memberAccount']['data']['id']));
        }

        return $expectObject;
    }

    public function testArrayToObjectCorrectObject()
    {
        $deposit = \Sdk\Deposit\Utils\MockFactory::generateDepositArray();

        $data =  $deposit['data'];
        $relationships = $data['relationships'];

        $memberAccount = new MemberAccount($relationships['memberAccount']['data']['id']);
        $memberAccountRestfulTranslator = $this->prophesize(MemberAccountRestfulTranslator::class);
        $memberAccountRestfulTranslator->arrayToObject(Argument::exact($relationships['memberAccount']))
            ->shouldBeCalledTimes(1)->willReturn($memberAccount);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberAccountRestfulTranslator')
            ->willReturn($memberAccountRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObject($deposit);

        $expectObject = new Deposit();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObjects()
    {
        $result = $this->stub->arrayToObjects(array());
        $this->assertEquals(array(0,array()), $result);
    }

    public function testArrayToObjectsOneCorrectObject()
    {
        $deposit = \Sdk\Deposit\Utils\MockFactory::generateDepositArray();
        $data =  $deposit['data'];
        $relationships = $data['relationships'];

        $memberAccount = new MemberAccount($relationships['memberAccount']['data']['id']);
        $memberAccountRestfulTranslator = $this->prophesize(MemberAccountRestfulTranslator::class);
        $memberAccountRestfulTranslator->arrayToObject(Argument::exact($relationships['memberAccount']))
            ->shouldBeCalledTimes(1)->willReturn($memberAccount);

        $this->stub->expects($this->exactly(1))
            ->method('getMemberAccountRestfulTranslator')
            ->willReturn($memberAccountRestfulTranslator->reveal());

        $actual = $this->stub->arrayToObjects($deposit);
        $expectArray = array();

        $expectObject = new Deposit();

        $expectObject->setId($data['id']);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $expectArray = [1, [$data['id']=>$expectObject]];

        $this->assertEquals($expectArray, $actual);
    }

    /**
     * 如果传参错误对象, 期望返回空数组
     */
    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->stub->objectToArray(null);
        $this->assertEquals(array(), $result);
    }
    /**
     * 传参正确对象, 返回对应数组
     */
    public function testObjectToArrayCorrectObject()
    {
        $deposit = \Sdk\Deposit\Utils\MockFactory::generateDepositObject(1, 1);

        $actual = $this->stub->objectToArray($deposit, array(
            'id',
            'amount',
            'paymentType',
            'transactionNumber',
            'transactionInfo',
            'memberAccount',
            'failureReason'
        ));
        
        $expectedArray = array(
            'data'=>array(
                'type'=>'deposits',
                'id'=>$deposit->getId()
            )
        );

        $expectedArray['data']['attributes'] = array(
            'amount'=>$deposit->getAmount(),
            'paymentType'=>$deposit->getPayment()->getType(),
            'transactionNumber'=>$deposit->getPayment()->getTransactionNumber(),
            'transactionInfo'=>$deposit->getPayment()->getTransactionInfo(),
            'failureReason'=>$deposit->getFailureReason(),
        );

        $expectedArray['data']['relationships']['memberAccount']['data'] = array(
            array(
                'type' => 'memberAccounts',
                'id' => $deposit->getMemberAccount()->getId()
            )
        );
        
        $this->assertEquals($expectedArray, $actual);
    }
}
