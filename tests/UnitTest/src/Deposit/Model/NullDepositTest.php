<?php
namespace Sdk\Deposit\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullDepositTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullDeposit::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsDeposit()
    {
        $this->assertInstanceof('Sdk\Deposit\Model\Deposit', $this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function tesDeposit()
    {
        $result = $this->stub->deposit();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testPay()
    {
        $result = $this->stub->pay();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testPaymentFailure()
    {
        $result = $this->stub->paymentFailure();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
