<?php
namespace Sdk\Deposit\Model;

use Sdk\Deposit\Repository\DepositRepository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use Sdk\MemberAccount\Model\MemberAccount;
use Sdk\Payment\Model\Payment;

class DepositTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Deposit::class)
            ->setMethods(
                [
                'getRepository',
                'IPayAble',
                'ITradeAble'
                ]
            )
            ->getMock();

        $this->childStub = new class extends Deposit {
            public function getRepository() : DepositRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Deposit\Repository\DepositRepository',
            $this->childStub->getRepository()
        );
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testCorrectImplementsIOperatAble()
    {
        $this->assertInstanceof('Sdk\Payment\Model\IPayAble', $this->stub);
    }

    public function testCorrectImplementsIModifyStatusAble()
    {
        $this->assertInstanceof('Sdk\TradeRecord\Model\ITradeAble', $this->stub);
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Deposit setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Deposit setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //paymentId 测试 -------------------------------------------------------- start
    /**
     * 设置 Deposit setPaymentId() 正确的传参类型,期望传值正确
     */
    public function testSetPaymentIdCorrectType()
    {
        $this->stub->setPaymentId('1_21');
        $this->assertEquals('1_21', $this->stub->getPaymentId());
    }

    /**
     * 设置 Deposit setPaymentId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPaymentIdWrongType()
    {
        $this->stub->setPaymentId(array());
    }
    //paymentId 测试 --------------------------------------------------------   end
    
    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 Deposit setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 Deposit setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array());
    }
    //number 测试 --------------------------------------------------------   end
    
    //failureReason 测试 -------------------------------------------------------- start
    /**
     * 设置 Deposit setFailureReason() 正确的传参类型,期望传值正确
     */
    public function testSetFailureReasonCorrectType()
    {
        $this->stub->setFailureReason(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getFailureReason());
    }

    /**
     * 设置 Deposit setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFailureReasonWrongType()
    {
        $this->stub->setFailureReason('failureReason');
    }
    //failureReason 测试 --------------------------------------------------------   end
    
    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 Deposit setMemberAccount() 正确的传参类型,期望传值正确
     */
    public function testSetMemberAccountCorrectType()
    {
        $object = new MemberAccount();
        $this->stub->setMemberAccount($object);
        $this->assertSame($object, $this->stub->getMemberAccount());
    }

    /**
     * 设置 Deposit setMemberAccount() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberAccountType()
    {
        $this->stub->setMemberAccount(array(1,2,3));
    }
    //member 测试 -------------------------------------------------------- end

    //payment 测试 -------------------------------------------------------- start
    /**
     * 设置 Deposit setPayment() 正确的传参类型,期望传值正确
     */
    public function testSetPaymentCorrectType()
    {
        $object = new Payment();
        $this->stub->setPayment($object);
        $this->assertSame($object, $this->stub->getPayment());
    }

    /**
     * 设置 Deposit setPayment() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPaymentType()
    {
        $this->stub->setPayment(array(1,2,3));
    }
    //payment 测试 -------------------------------------------------------- end
    
    //充值
    public function testDepositFailure()
    {
        $repository = $this->prophesize(DepositRepository::class);
        $repository->deposit(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->deposit();
        $this->assertFalse($result);
    }

    public function testDepositSuccess()
    {
        $repository = $this->prophesize(DepositRepository::class);
        $repository->deposit(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->deposit();
        $this->assertTrue($result);
    }
    //支付
    public function testPayFailure()
    {
        $repository = $this->prophesize(DepositRepository::class);
        $repository->pay(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->pay();
        $this->assertFalse($result);
    }

    public function testPaySuccess()
    {
        $repository = $this->prophesize(DepositRepository::class);
        $repository->pay(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->pay();
        $this->assertTrue($result);
    }
    //支付失败
    public function testPaymentFailureFailure()
    {
        $repository = $this->prophesize(DepositRepository::class);
        $repository->paymentFailure(Argument::exact($this->stub))->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->stub->paymentFailure();
        $this->assertFalse($result);
    }

    public function testPaymentFailureSuccess()
    {
        $repository = $this->prophesize(DepositRepository::class);
        $repository->paymentFailure(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->paymentFailure();
        $this->assertTrue($result);
    }
}
