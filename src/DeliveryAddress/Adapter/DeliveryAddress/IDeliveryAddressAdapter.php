<?php
namespace Trial\DeliveryAddress\Adapter\DeliveryAddress;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IModifyStatusAbleAdapter;

interface IDeliveryAddressAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IModifyStatusAbleAdapter
{
}
