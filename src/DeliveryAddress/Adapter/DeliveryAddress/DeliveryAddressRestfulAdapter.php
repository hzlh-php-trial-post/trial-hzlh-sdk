<?php
namespace Trial\DeliveryAddress\Adapter\DeliveryAddress;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\DeliveryAddress\Model\DeliveryAddress;
use Trial\DeliveryAddress\Model\NullDeliveryAddress;
use Trial\DeliveryAddress\Translator\DeliveryAddressRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class DeliveryAddressRestfulAdapter extends GuzzleAdapter implements IDeliveryAddressAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'DELIVERY_ADDRESS_LIST'=>[
                'fields'=>[
                    'deliveryAddresss'=>'area,address,postalCode,realName,cellphone,isDefaultAddress'
                ],
                'include'=> 'member,snapshots'
            ],
            'DELIVERY_ADDRESS_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'member,snapshots'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new DeliveryAddressRestfulTranslator();
        $this->resource = 'deliveryAddress';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullDeliveryAddress::getInstance());
    }

    protected function addAction(DeliveryAddress $deliveryAddress) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $deliveryAddress,
            array(
                'area',
                'address',
                'postalCode',
                'realName',
                'cellphone',
                'isDefaultAddress',
                'member'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deliveryAddress);
            return true;
        }

        return false;
    }

    protected function editAction(DeliveryAddress $deliveryAddress) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $deliveryAddress,
            array(
                'area',
                'address',
                'postalCode',
                'realName',
                'cellphone',
                'isDefaultAddress'
            )
        );

        $this->patch(
            $this->getResource().'/'.$deliveryAddress->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deliveryAddress);
            return true;
        }

        return false;
    }

    public function setAsDefaultAddress(DeliveryAddress $deliveryAddress) : bool
    {
        $this->patch(
            $this->getResource().'/'.$deliveryAddress->getId().'/setAsDefaultAddress'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deliveryAddress);
            return true;
        }
        return false;
    }
}
