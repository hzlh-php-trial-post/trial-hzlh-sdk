<?php
namespace Trial\DeliveryAddress\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ModifyStatusAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\DeliveryAddress\Adapter\DeliveryAddress\IDeliveryAddressAdapter;
use Trial\DeliveryAddress\Adapter\DeliveryAddress\DeliveryAddressMockAdapter;
use Trial\DeliveryAddress\Adapter\DeliveryAddress\DeliveryAddressRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\DeliveryAddress\Model\DeliveryAddress;

class DeliveryAddressRepository extends Repository implements IDeliveryAddressAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'DELIVERY_ADDRESS_LIST';
    const FETCH_ONE_MODEL_UN = 'DELIVERY_ADDRESS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new DeliveryAddressRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IDeliveryAddressAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IDeliveryAddressAdapter
    {
        return new DeliveryAddressMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function setAsDefaultAddress(DeliveryAddress $deliveryAddress) : bool
    {
        return $this->getAdapter()->setAsDefaultAddress($deliveryAddress);
    }
}
