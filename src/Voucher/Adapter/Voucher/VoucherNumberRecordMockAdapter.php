<?php
namespace Trial\Voucher\Adapter\Voucher;

use Trial\Voucher\Utils\MockFactory;

use Trial\Voucher\Model\VoucherNumberRecord;

class VoucherNumberRecordMockAdapter implements IVoucherNumberRecordAdapter
{
    public function settledAccount(VoucherNumberRecord $voucherNumberRecord) : bool
    {
        unset($voucherNumberRecord);
        return true;
    }

    public function counterClosing(VoucherNumberRecord $voucherNumberRecord) : bool
    {
        unset($voucherNumberRecord);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generateVoucherObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $voucherNumberRecordList = array();

        foreach ($ids as $id) {
            $voucherNumberRecordList[] = MockFactory::generateVoucherObject($id);
        }

        return $voucherNumberRecordList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateVoucherObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $voucherNumberRecordList = array();

        foreach ($ids as $id) {
            $voucherNumberRecordList[] = MockFactory::generateVoucherObject($id);
        }

        return $voucherNumberRecordList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
