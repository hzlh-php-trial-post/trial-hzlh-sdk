<?php
namespace Trial\Voucher\Adapter\Voucher;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Voucher\Model\VoucherNumberRecord;

interface IVoucherNumberRecordAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function settledAccount(VoucherNumberRecord $voucher) : bool;
    
    public function counterClosing(VoucherNumberRecord $voucher) : bool;
}
