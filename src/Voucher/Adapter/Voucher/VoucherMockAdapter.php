<?php
namespace Trial\Voucher\Adapter\Voucher;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;

use Trial\Voucher\Utils\MockFactory;

use Trial\Voucher\Model\Voucher;

class VoucherMockAdapter implements IVoucherAdapter
{
    use OperatAbleMockAdapterTrait;

    public function insert(Voucher $voucher) : bool
    {
        unset($voucher);
        return true;
    }

    public function deletes(Voucher $voucher) : bool
    {
        unset($voucher);
        return true;
    }

    public function arrange(Voucher $voucher) : bool
    {
        unset($voucher);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generateVoucherObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $voucherList = array();

        foreach ($ids as $id) {
            $voucherList[] = MockFactory::generateVoucherObject($id);
        }

        return $voucherList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateVoucherObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $voucherList = array();

        foreach ($ids as $id) {
            $voucherList[] = MockFactory::generateVoucherObject($id);
        }

        return $voucherList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
