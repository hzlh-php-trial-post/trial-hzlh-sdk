<?php
namespace Trial\Voucher\Adapter\Voucher;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Voucher\Model\Voucher;
use Trial\Voucher\Model\NullVoucher;
use Trial\Voucher\Translator\VoucherRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class VoucherRestfulAdapter extends GuzzleAdapter implements IVoucherAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_VOUCHER_LIST'=>[
                'fields'=>[],
                'include'=> 'accountTemplate,member,voucherRecords'
            ],
            'VOUCHER_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'accountTemplate,member,voucherRecords'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new VoucherRestfulTranslator();
        $this->resource = 'vouchers';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullVoucher::getInstance());
    }

    protected function addAction(Voucher $voucher) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $voucher,
            array(
                'type',
                'voucherType',
                'generationDate',
                'attachments',
                'voucherRecords',
                'accountTemplate',
                'member'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($voucher);
            return true;
        }

        return false;
    }

    protected function editAction(Voucher $voucher) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $voucher,
            array(
                'type',
                'generationDate',
                'attachments',
                'voucherRecords'
            )
        );

        $this->patch(
            $this->getResource().'/'.$voucher->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($voucher);
            return true;
        }

        return false;
    }

    public function insert(Voucher $voucher) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $voucher,
            array(
                'type',
                'number',
                'voucherType',
                'generationDate',
                'attachments',
                'voucherRecords',
                'accountTemplate',
                'member'
            )
        );

        $this->post(
            $this->getResource().'/insert',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($voucher);
            return true;
        }

        return false;
    }

    public function deletes(Voucher $voucher) : bool
    {
        $this->delete(
            $this->getResource().'/'.$voucher->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($voucher);
            return true;
        }

        return false;
    }

    public function arrange(Voucher $voucher) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $voucher,
            array(
                'generationDate',
                'accountTemplate'
            )
        );
        
        $this->patch(
            $this->getResource().'/arrange',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($voucher);
            return true;
        }

        return false;
    }
}
