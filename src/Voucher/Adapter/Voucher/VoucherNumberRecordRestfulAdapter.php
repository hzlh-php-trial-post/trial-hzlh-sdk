<?php
namespace Trial\Voucher\Adapter\Voucher;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Voucher\Model\VoucherNumberRecord;
use Trial\Voucher\Model\NullVoucherNumberRecord;
use Trial\Voucher\Translator\VoucherNumberRecordRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class VoucherNumberRecordRestfulAdapter extends GuzzleAdapter implements IVoucherNumberRecordAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_VOUCHER_LIST'=>[
                'fields'=>[],
                'include'=> 'accountTemplate,member'
            ],
            'VOUCHER_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'accountTemplate,member'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new VoucherNumberRecordRestfulTranslator();
        $this->resource = 'voucherNumberRecords';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullVoucherNumberRecord::getInstance());
    }

    public function settledAccount(VoucherNumberRecord $voucherNumberRecord) : bool
    {
        $this->post(
            $this->getResource().'/'.$voucherNumberRecord->getId().'/settledAccount'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($voucherNumberRecord);
            return true;
        }

        return false;
    }

    public function counterClosing(VoucherNumberRecord $voucherNumberRecord) : bool
    {
        $this->post(
            $this->getResource().'/'.$voucherNumberRecord->getId().'/counterClosing'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($voucherNumberRecord);
            return true;
        }

        return false;
    }
}
