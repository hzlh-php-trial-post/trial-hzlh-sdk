<?php
namespace Trial\Voucher\Adapter\Voucher;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Voucher\Model\Voucher;

interface IVoucherAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
    public function insert(Voucher $voucher) : bool;

    public function deletes(Voucher $voucher) : bool;

    public function arrange(Voucher $voucher) : bool;
}
