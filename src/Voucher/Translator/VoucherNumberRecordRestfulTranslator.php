<?php
namespace Trial\Voucher\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Voucher\Model\VoucherNumberRecord;
use Trial\Voucher\Model\NullVoucherNumberRecord;

use Trial\Common\Translator\RestfulTranslatorTrait;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class VoucherNumberRecordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $voucherNumberRecord = null)
    {
        return $this->translateToObject($expression, $voucherNumberRecord);
    }

    protected function translateToObject(array $expression, $voucherNumberRecord = null)
    {
        if (empty($expression)) {
            return NullVoucherNumberRecord::getInstance();
        }
        if ($voucherNumberRecord === null) {
            $voucherNumberRecord = new VoucherNumberRecord();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $voucherNumberRecord->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['recordDate'])) {
            $voucherNumberRecord->setRecordDate($attributes['recordDate']);
        }
        if (isset($attributes['numbers'])) {
            $voucherNumberRecord->setNumbers($attributes['numbers']);
        }
        if (isset($attributes['status'])) {
            $voucherNumberRecord->setStatus($attributes['status']);
        }
        if (isset($attributes['counterClosingTime'])) {
            $voucherNumberRecord->setCounterClosingTime($attributes['counterClosingTime']);
        }
        if (isset($attributes['accountTemplate'])) {
            $voucherNumberRecord->setAccountTemplate($attributes['accountTemplate']);
        }

        return $voucherNumberRecord;
    }

    public function objectToArray($voucherNumberRecord, array $keys = array())
    {
        $expression = array();

        if (!$voucherNumberRecord instanceof VoucherNumberRecord) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'recordDate',
                'numbers',
                'accountTemplate'
            );
        }

        $expression = array(
            'type'=>'voucherNumberRecords'
        );

        if (in_array('id', $keys)) {
            $expression['id'] = $voucherNumberRecord->getId();
        }

        $attributes = array();

        if (in_array('recordDate', $keys)) {
            $attributes['recordDate'] = $voucherNumberRecord->getRecordDate();
        }
        if (in_array('numbers', $keys)) {
            $attributes['numbers'] = $voucherNumberRecord->getNumbers();
        }
        if (in_array('accountTemplate', $keys)) {
            $attributes['accountTemplate'] = $voucherNumberRecord->getAccountTemplate();
        }

        $expression['attributes'] = $attributes;

        return $expression;
    }
}
