<?php
namespace Trial\Voucher\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Voucher\Model\Voucher;
use Trial\Voucher\Model\NullVoucher;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\AccountTemplate\Translator\AccountTemplateRestfulTranslator;
use Trial\Member\Translator\MemberRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class VoucherRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getAccountTemplateRestfulTranslator()
    {
        return new AccountTemplateRestfulTranslator();
    }

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function getVoucherRecordRestfulTranslator()
    {
        return new VoucherRecordRestfulTranslator();
    }

    public function arrayToObject(array $expression, $voucher = null)
    {
        return $this->translateToObject($expression, $voucher);
    }

    protected function translateToObject(array $expression, $voucher = null)
    {
        if (empty($expression)) {
            return NullVoucher::getInstance();
        }
        if ($voucher === null) {
            $voucher = new Voucher();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $voucher->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['numberType'])) {
            $voucher->setType($attributes['numberType']);
        }
        if (isset($attributes['number'])) {
            $voucher->setNumber($attributes['number']);
        }
        if (isset($attributes['voucherType'])) {
            $voucher->setVoucherType($attributes['voucherType']);
        }
        if (isset($attributes['generationDate'])) {
            $voucher->setGenerationDate($attributes['generationDate']);
        }
        if (isset($attributes['attachments'])) {
            $voucher->setAttachments($attributes['attachments']);
        }
        if (isset($attributes['totalAmount'])) {
            $voucher->setTotalAmount($attributes['totalAmount']);
        }
        if (isset($attributes['status'])) {
            $voucher->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $voucher->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $voucher->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $voucher->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['accountTemplate']['data'])) {
            $accountTemplate = $this->changeArrayFormat($relationships['accountTemplate']['data']);
            $voucher->setAccountTemplate(
                $this->getAccountTemplateRestfulTranslator()->arrayToObject($accountTemplate)
            );
        }
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $voucher->setMember(
                $this->getMemberRestfulTranslator()->arrayToObject($member)
            );
        }
        if (isset($relationships['voucherRecords']['data'])) {
            foreach ($relationships['voucherRecords']['data'] as $voucherRecordArray) {
                if (isset($expression['included'])) {
                    $voucherRecord = $this->changeArrayFormat($voucherRecordArray, $expression['included']);
                }

                if (!isset($expression['included'])) {
                    $voucherRecord = $this->changeArrayFormat($voucherRecordArray);
                }

                $voucherRecordObject = $this->getVoucherRecordRestfulTranslator()->arrayToObject($voucherRecord);
                
                $voucher->addVoucherRecord($voucherRecordObject);
            }
        }

        return $voucher;
    }

    public function objectToArray($voucher, array $keys = array())
    {
        $expression = array();

        if (!$voucher instanceof Voucher) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'type',
                'number',
                'voucherType',
                'generationDate',
                'attachments',
                'voucherRecords',
                'accountTemplate',
                'member'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'vouchers'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $voucher->getId();
        }

        $attributes = array();

        if (in_array('type', $keys)) {
            $attributes['type'] = $voucher->getType();
        }
        if (in_array('number', $keys)) {
            $attributes['number'] = $voucher->getNumber();
        }
        if (in_array('voucherType', $keys)) {
            $attributes['voucherType'] = $voucher->getVoucherType();
        }
        if (in_array('generationDate', $keys)) {
            $attributes['generationDate'] = $voucher->getGenerationDate();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachments'] = $voucher->getAttachments();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('accountTemplate', $keys)) {
            $expression['data']['relationships']['accountTemplate']['data'] = array(
                array(
                    'type' => 'accountTemplates',
                    'id' => $voucher->getAccountTemplate()->getId()
                )
            );
        }
        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $voucher->getMember()->getId()
                )
            );
        }
        if (in_array('voucherRecords', $keys)) {
            foreach ($voucher->getVoucherRecords() as $voucherRecord) {
                $voucherRecords[] = $this->getVoucherRecordRestfulTranslator()->objectToArray($voucherRecord);
            }

            $expression['data']['relationships']['voucherRecords']['data'] = $voucherRecords;
        }

        return $expression;
    }
}
