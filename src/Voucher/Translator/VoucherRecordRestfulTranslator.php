<?php
namespace Trial\Voucher\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Voucher\Model\VoucherRecord;
use Trial\Voucher\Model\NullVoucherRecord;

use Trial\Common\Translator\RestfulTranslatorTrait;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class VoucherRecordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $voucherRecord = null)
    {
        return $this->translateToObject($expression, $voucherRecord);
    }

    protected function translateToObject(array $expression, $voucherRecord = null)
    {
        if (empty($expression)) {
            return NullVoucherRecord::getInstance();
        }
        if ($voucherRecord === null) {
            $voucherRecord = new VoucherRecord();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $voucherRecord->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['abstract'])) {
            $voucherRecord->setAbstract($attributes['abstract']);
        }
        if (isset($attributes['debitAmount'])) {
            $voucherRecord->setDebitAmount($attributes['debitAmount']);
        }
        if (isset($attributes['creditAmount'])) {
            $voucherRecord->setCreditAmount($attributes['creditAmount']);
        }
        if (isset($attributes['subjectId'])) {
            $voucherRecord->setSubjectId($attributes['subjectId']);
        }
        if (isset($attributes['recordId'])) {
            $voucherRecord->setRecordId($attributes['recordId']);
        }
        if (isset($attributes['accountSubject'])) {
            $voucherRecord->setAccountSubject($attributes['accountSubject']);
        }

        return $voucherRecord;
    }

    public function objectToArray($voucherRecord, array $keys = array())
    {
        $expression = array();

        if (!$voucherRecord instanceof VoucherRecord) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'abstract',
                'debitAmount',
                'creditAmount',
                'recordId',
                'subjectId'
            );
        }

        $expression = array(
            'type'=>'voucherRecords'
        );

        if (in_array('id', $keys)) {
            $expression['id'] = $voucherRecord->getId();
        }

        $attributes = array();

        if (in_array('abstract', $keys)) {
            $attributes['abstract'] = $voucherRecord->getAbstract();
        }
        if (in_array('debitAmount', $keys)) {
            $attributes['debitAmount'] = $voucherRecord->getDebitAmount();
        }
        if (in_array('creditAmount', $keys)) {
            $attributes['creditAmount'] = $voucherRecord->getCreditAmount();
        }
        if (in_array('subjectId', $keys)) {
            $attributes['subjectId'] = $voucherRecord->getSubjectId();
        }
        if (in_array('recordId', $keys)) {
            $attributes['recordId'] = $voucherRecord->getRecordId();
        }

        $expression['attributes'] = $attributes;

        return $expression;
    }
}
