<?php
namespace Trial\Voucher\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullOperatAbleTrait;

class NullVoucher extends Voucher implements INull
{
    use NullOperatAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function insert() : bool
    {
        return $this->resourceNotExist();
    }

    public function deletes() : bool
    {
        return $this->resourceNotExist();
    }

    public function arrange() : bool
    {
        return $this->resourceNotExist();
    }
}
