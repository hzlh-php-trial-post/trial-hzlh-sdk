<?php
namespace Trial\Voucher\Model;

use Trial\Voucher\Repository\VoucherNumberRecordRepository;

use Trial\AccountTemplate\Model\AccountTemplate;

class VoucherNumberRecord
{
    const STATUS = array(
        'NOT_SETTLE_ACCOUNTS' => 0, //未结账
        'SETTLE_ACCOUNTS' => 2, //已结账
    );
    
    private $id;

    private $recordDate;

    private $numbers;

    private $accountTemplate;

    private $status;

    private $counterClosingTime;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->recordDate = '';
        $this->numbers = array();
        $this->status = self::STATUS['NOT_SETTLE_ACCOUNTS'];
        $this->counterClosingTime = 0;
        $this->accountTemplate = new AccountTemplate();
        $this->repository = new VoucherNumberRecordRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->recordDate);
        unset($this->numbers);
        unset($this->status);
        unset($this->counterClosingTime);
        unset($this->accountTemplate);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setRecordDate(string $recordDate) : void
    {
        $this->recordDate = $recordDate;
    }

    public function getRecordDate() : string
    {
        return $this->recordDate;
    }

    public function setNumbers(array $numbers) : void
    {
        $this->numbers = $numbers;
    }

    public function getNumbers() : array
    {
        return $this->numbers;
    }

    public function setAccountTemplate(AccountTemplate $accountTemplate) : void
    {
        $this->accountTemplate = $accountTemplate;
    }

    public function getAccountTemplate() : AccountTemplate
    {
        return $this->accountTemplate;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setCounterClosingTime(int $counterClosingTime) : void
    {
        $this->counterClosingTime = $counterClosingTime;
    }

    public function getCounterClosingTime() : int
    {
        return $this->counterClosingTime;
    }

    protected function getRepository() : VoucherNumberRecordRepository
    {
        return $this->repository;
    }

    // 结转
    public function settledAccount() : bool
    {
        return $this->getRepository()->settledAccount($this);
    }

    // 反结账
    public function counterClosing() : bool
    {
        return $this->getRepository()->counterClosing($this);
    }
}
