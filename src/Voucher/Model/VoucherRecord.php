<?php
namespace Trial\Voucher\Model;

use Trial\AccountSubject\Model\AccountSubject;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class VoucherRecord
{
    /**
     * [$id id]
     * @var [int]
     */
    private $id;
    /**
     * [$abstract 摘要]
     * @var [string]
     */
    private $abstract;
    /**
     * [$debitAmount 借方金额]
     * @var [float]
     */
    private $debitAmount;
    /**
     * [$creditAmount 贷方金额]
     * @var [float]
     */
    private $creditAmount;
    /**
     * [$subjectId 科目Id:1_1001]
     * @var [string]
     */
    private $subjectId;
    /**
     * [$accountSubject 科目]
     * @var [array]
     */
    private $accountSubject;
    /**
     * [$recordId 科目记录id]
     * @var [int]
     */
    private $recordId;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->abstract = '';
        $this->debitAmount = 0;
        $this->creditAmount = 0;
        $this->subjectId = '';
        $this->recordId = 0;
        $this->accountSubject = [];
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->abstract);
        unset($this->debitAmount);
        unset($this->creditAmount);
        unset($this->subjectId);
        unset($this->recordId);
        unset($this->accountSubject);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setAbstract(string $abstract) : void
    {
        $this->abstract = $abstract;
    }

    public function getAbstract() : string
    {
        return $this->abstract;
    }

    public function setRecordId(float $recordId) : void
    {
        $this->recordId = $recordId;
    }

    public function getRecordId() : int
    {
        return $this->recordId;
    }

    public function setDebitAmount(float $debitAmount) : void
    {
        $this->debitAmount = $debitAmount;
    }

    public function getDebitAmount() : float
    {
        return $this->debitAmount;
    }

    public function setCreditAmount(float $creditAmount) : void
    {
        $this->creditAmount = $creditAmount;
    }

    public function getCreditAmount() : float
    {
        return $this->creditAmount;
    }

    public function setSubjectId(string $subjectId) : void
    {
        $this->subjectId = $subjectId;
    }

    public function getSubjectId() : string
    {
        return $this->subjectId;
    }

    public function setAccountSubject(array $accountSubject) : void
    {
        $this->accountSubject = $accountSubject;
    }

    public function getAccountSubject() : array
    {
        return $this->accountSubject;
    }
}
