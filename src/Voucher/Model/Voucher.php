<?php
namespace Trial\Voucher\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Voucher\Repository\VoucherRepository;

use Trial\AccountTemplate\Model\AccountTemplate;
use Trial\Member\Model\Member;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class Voucher implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const TYPE = array(
        'JI' => 1 //记
    );

    const VOUCHER_TYPE = array(
        'NORMAL' => 0, //普通凭证
        'PROFIT_AND_LOSS' => 1, //结转损益凭证
    );


    const STATUS = array(
        'NORMAL' => 0, //正常
        'AUDITED' => 2, //已审核
        'SETTLED_ACCOUNTS' => 4, //已结账
        'DELETED_UNCLASSIFIED' => -2, //删除-未整理
        'DELETED_SORTED' => -4 //删除-已整理
    );

    /**
     * [$id id]
     * @var [int]
     */
    private $id;
    /**
     * [$type 凭证字类型]
     * @var [int]
     */
    private $type;
    /**
     * [$number 凭证字号]
     * @var [int]
     */
    private $number;
    /**
     * [$generationDate 凭证生成日期]
     * @var [string]
     */
    private $generationDate;
    /**
     * [$attachments 凭证单据]
     * @var [array]
     */
    private $attachments;
    /**
     * [$totalAmount 合计金额]
     * @var [float]
     */
    private $totalAmount;
    /**
     * [$voucherRecords 凭证记录]
     * @var [array]
     */
    private $voucherRecords;
    /**
     * [$accountTemplate 所属账套]
     * @var [AccountTemplate]
     */
    private $accountTemplate;
    /**
     * [$voucherType 凭证类型]
     * @var [int]
     */
    private $voucherType;
    /**
     * [$member 用户]
     * @var [Member]
     */
    private $member;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->type = self::TYPE['JI'];
        $this->number = 0;
        $this->voucherType = self::VOUCHER_TYPE['NORMAL'];
        $this->generationDate = '';
        $this->attachments = array();
        $this->voucherRecords = array();
        $this->accountTemplate = new AccountTemplate();
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS['NORMAL'];
        $this->repository = new VoucherRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->type);
        unset($this->number);
        unset($this->voucherType);
        unset($this->generationDate);
        unset($this->attachments);
        unset($this->voucherRecords);
        unset($this->accountTemplate);
        unset($this->member);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setType(int $type) : void
    {
        $this->type = in_array($type, self::TYPE) ? $type : self::TYPE['JI'];
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function setVoucherType(int $voucherType) : void
    {
        $this->voucherType = in_array($voucherType, self::VOUCHER_TYPE) ? $voucherType : self::VOUCHER_TYPE['NORMAL'];
    }

    public function getVoucherType() : int
    {
        return $this->voucherType;
    }

    public function setNumber(int $number)
    {
        $this->number = $number;
    }

    public function getNumber() : int
    {
        return $this->number;
    }

    public function setGenerationDate(string $generationDate) : void
    {
        $this->generationDate = $generationDate;
    }

    public function getGenerationDate() : string
    {
        return $this->generationDate;
    }

    public function setAttachments(array $attachments) : void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments() : array
    {
        return $this->attachments;
    }

    public function setTotalAmount(float $totalAmount) : void
    {
        $this->totalAmount = $totalAmount;
    }

    public function getTotalAmount() : float
    {
        return $this->totalAmount;
    }

    public function addVoucherRecord(VoucherRecord $voucherRecord) : void
    {
        $this->voucherRecords[] = $voucherRecord;
    }

    public function clearVoucherRecords()
    {
        $this->voucherRecords = [];
    }

    public function getVoucherRecords() : array
    {
        return $this->voucherRecords;
    }

    public function setAccountTemplate(AccountTemplate $accountTemplate) : void
    {
        $this->accountTemplate = $accountTemplate;
    }

    public function getAccountTemplate() : AccountTemplate
    {
        return $this->accountTemplate;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }

    protected function getRepository() : VoucherRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
    /**
     * [insert 插入凭证]
     * @return [type] [bool]
     */
    public function insert() : bool
    {
        return $this->getRepository()->insert($this);
    }
    /**
     * [deletes 删除凭证]
     * @return [type] [bool]
     */
    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }
    /**
     * [arrange 整理凭证]
     * @return [type] [bool]
     */
    public function arrange() : bool
    {
        return $this->getRepository()->arrange($this);
    }
}
