<?php
namespace Trial\Voucher\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Voucher\Model\Voucher;
use Trial\Voucher\Adapter\Voucher\IVoucherAdapter;
use Trial\Voucher\Adapter\Voucher\VoucherMockAdapter;
use Trial\Voucher\Adapter\Voucher\VoucherRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class VoucherRepository extends Repository implements IVoucherAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'PORTAL_VOUCHER_LIST';
    const FETCH_ONE_MODEL_UN = 'VOUCHER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new VoucherRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IVoucherAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IVoucherAdapter
    {
        return new VoucherMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function insert(Voucher $voucher) : bool
    {
        return $this->getAdapter()->insert($voucher);
    }

    public function deletes(Voucher $voucher) : bool
    {
        return $this->getAdapter()->deletes($voucher);
    }

    public function arrange(Voucher $voucher) : bool
    {
        return $this->getAdapter()->arrange($voucher);
    }
}
