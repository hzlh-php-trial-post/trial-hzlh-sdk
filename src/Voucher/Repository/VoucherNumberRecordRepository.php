<?php
namespace Trial\Voucher\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Voucher\Model\VoucherNumberRecord;
use Trial\Voucher\Adapter\Voucher\VoucherNumberRecordMockAdapter;
use Trial\Voucher\Adapter\Voucher\IVoucherNumberRecordAdapter;
use Trial\Voucher\Adapter\Voucher\VoucherNumberRecordRestfulAdapter;

class VoucherNumberRecordRepository extends Repository implements IVoucherNumberRecordAdapter
{
    use FetchRepositoryTrait, ErrorRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'PORTAL_VOUCHER_NUMBER_RECORD_LIST';
    const FETCH_ONE_MODEL_UN = 'VOUCHER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new VoucherNumberRecordRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IVoucherNumberRecordAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IVoucherNumberRecordAdapter
    {
        return new VoucherNumberRecordMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function settledAccount(VoucherNumberRecord $voucher) : bool
    {
        return $this->getAdapter()->settledAccount($voucher);
    }

    public function counterClosing(VoucherNumberRecord $voucher) : bool
    {
        return $this->getAdapter()->counterClosing($voucher);
    }
}
