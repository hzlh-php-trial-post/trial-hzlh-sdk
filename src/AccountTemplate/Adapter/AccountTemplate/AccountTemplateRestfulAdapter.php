<?php
namespace Trial\AccountTemplate\Adapter\AccountTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\AccountTemplate\Model\AccountTemplate;
use Trial\AccountTemplate\Model\NullAccountTemplate;
use Trial\AccountTemplate\Translator\AccountTemplateRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class AccountTemplateRestfulAdapter extends GuzzleAdapter implements IAccountTemplateAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'ACCOUNT_TEMPLATE_LIST'=>[
            'fields'=>[
                'accountTemplates'=>'name,activationDate,currentPeriod,accountingStandard,voucherApproval,status,createTime,updateTime,enterprise,industry' //phpcs:ignore
            ],
            'include'=> 'enterprise,industry'
        ],
        'ACCOUNT_TEMPLATE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'enterprise,industry'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new AccountTemplateRestfulTranslator();
        $this->resource = 'accountTemplates';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => ACCOUNT_TEMPLATE_NAME_EXIST
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullAccountTemplate::getInstance());
    }

    protected function addAction(AccountTemplate $accountTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $accountTemplate,
            array(
                'name',
                'industry',
                'typeVat',
                'activationDate',
                'accountingStandard',
                'voucherApproval',
                'enterprise'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($accountTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(AccountTemplate $accountTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $accountTemplate,
            array(
                'name',
                'industry',
                'typeVat',
                'activationDate',
                'currentPeriod',
                'voucherApproval'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$accountTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($accountTemplate);
            return true;
        }

        return false;
    }

    public function deletes(AccountTemplate $accountTemplate) : bool
    {
        $this->delete(
            $this->getResource().'/'.$accountTemplate->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($accountTemplate);
            return true;
        }
        return false;
    }
}
