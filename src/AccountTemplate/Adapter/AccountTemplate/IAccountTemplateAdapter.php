<?php
namespace Trial\AccountTemplate\Adapter\AccountTemplate;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IAccountTemplateAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
