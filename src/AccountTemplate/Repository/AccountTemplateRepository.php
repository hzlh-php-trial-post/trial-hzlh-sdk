<?php
namespace Trial\AccountTemplate\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\AccountTemplate\Adapter\AccountTemplate\IAccountTemplateAdapter;
use Trial\AccountTemplate\Adapter\AccountTemplate\AccountTemplateMockAdapter;
use Trial\AccountTemplate\Adapter\AccountTemplate\AccountTemplateRestfulAdapter;
use Trial\AccountTemplate\Model\AccountTemplate;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class AccountTemplateRepository extends Repository implements IAccountTemplateAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ACCOUNT_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'ACCOUNT_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new AccountTemplateRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IAccountTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IAccountTemplateAdapter
    {
        return new AccountTemplateMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(AccountTemplate $accountTemplate) : bool
    {
        return $this->getAdapter()->deletes($accountTemplate);
    }
}
