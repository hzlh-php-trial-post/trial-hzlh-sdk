<?php
namespace Trial\Distribution\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IEnableAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Enterprise\Model\Enterprise;
use Trial\Service\Model\Service;
use Trial\Distribution\Model\SystemTemplate;

use Trial\Distribution\Repository\EnterpriseTemplateRepository;

class EnterpriseTemplate implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    private $id;

    private $validPeriod;

    private $expireTime;

    private $lastUseTime;

    private $service;

    private $enterprise;

    private $systemTemplate;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->validPeriod = 0;
        $this->expireTime = 0;
        $this->lastUseTime = 0;
        $this->service = new Service();
        $this->enterprise = new Enterprise();
        $this->systemTemplate = new SystemTemplate();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->statusTime = 0;
        $this->repository = new EnterpriseTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->validPeriod);
        unset($this->expireTime);
        unset($this->lastUseTime);
        unset($this->service);
        unset($this->enterprise);
        unset($this->systemTemplate);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setValidPeriod(int $validPeriod) : void
    {
        $this->validPeriod = $validPeriod;
    }

    public function getValidPeriod() : int
    {
        return $this->validPeriod;
    }

    public function setExpireTime(int $expireTime) : void
    {
        $this->expireTime = $expireTime;
    }

    public function getExpireTime() : int
    {
        return $this->expireTime;
    }

    public function setLastUseTime(int $lastUseTime) : void
    {
        $this->lastUseTime = $lastUseTime;
    }

    public function getLastUseTime() : int
    {
        return $this->lastUseTime;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setService(Service $service) : void
    {
        $this->service = $service;
    }

    public function getService() : Service
    {
        return $this->service;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setSystemTemplate(SystemTemplate $systemTemplate) : void
    {
        $this->systemTemplate = $systemTemplate;
    }

    public function getSystemTemplate() : SystemTemplate
    {
        return $this->systemTemplate;
    }

    protected function getEnterpriseTemplateRepository(): EnterpriseTemplateRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getEnterpriseTemplateRepository();
    }

    public function add() : bool
    {
        if (!$this->validateAddScenario()) {
            return false;
        }

        $repository = $this->getIOperatAbleAdapter();

        return $repository->add($this);
    }

    protected function validateAddScenario() : bool
    {
        $status = $this->getSystemTemplate()->getStatus();
        if($status == IEnableAble::STATUS['DISABLED']){
            Core::setLastError(RESOURCE_STATUS_OFF_STOCK,array('pointer'=>'status'));
            return false;
        }

        return true;
    }

    //更新最后使用时间
    public function updateLastUseTime() : bool
    {
        return $this->getEnterpriseTemplateRepository()->updateLastUseTime($this);
    }

    //续费
    public function renew() : bool
    {
        return $this->getEnterpriseTemplateRepository()->renew($this);
    }
}
