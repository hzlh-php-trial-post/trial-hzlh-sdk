<?php
namespace Trial\Distribution\Model;

use Trial\Common\Model\Document;

use Trial\Distribution\Repository\SystemTemplateDocumentRepository;

class SystemTemplateDocument extends Document
{
    public function addInfo()
    {
        return $this->getSystemTemplateDocumentRepository()->add($this);
    }

    protected function getSystemTemplateDocumentRepository() : SystemTemplateDocumentRepository
    {
        return new SystemTemplateDocumentRepository();
    }
}
