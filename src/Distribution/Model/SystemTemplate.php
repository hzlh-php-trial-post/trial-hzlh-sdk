<?php
namespace Trial\Distribution\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IEnableAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\EnableAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

use Trial\Crew\Model\Crew;
use Trial\Enterprise\Model\Enterprise;
use Trial\Service\Model\Service;
use Trial\Distribution\Model\EnterpriseTemplate;

use Trial\Distribution\Repository\SystemTemplateRepository;

class SystemTemplate implements IObject, IOperatAble, IEnableAble
{
    use Object, OperatAbleTrait, EnableAbleTrait;

    const CATEGORY = [
        'NULL'=>0,
        'SERVICE' => 1, //服务
        'ENTERPRISE' => 2 //企业
    ];

    const IS_FREE = [
        'NO' => 0,//收费
        'YES' => 1 //免费
    ];

    const IS_DEFAULT = [
        'NO' => 0,//否
        'YES' => 1 //是
    ];

    private $id;

    private $title;

    private $description;

    private $category;

    private $contents;

    private $price;

    private $isFree;

    private $cover;

    private $viewNumber;

    private $useNumber;

    private $crew;

    private $isDefault;

    private $validPeriod;

    private $expireTime;

    private $lastUseTime;

    private $service;

    private $enterprise;

    private $enterpriseTemplateId;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->title = '';
        $this->description = '';
        $this->cover = [];
        $this->category = self::CATEGORY['ENTERPRISE'];
        $this->contents = '';
        $this->price = 0;
        $this->isFree = self::IS_FREE['YES'];
        $this->viewNumber = 0;
        $this->useNumber = 0;
        $this->isDefault = self::IS_DEFAULT['YES'];
        $this->crew = new Crew();
        $this->validPeriod = 0;
        $this->expireTime = 0;
        $this->lastUseTime = 0;
        $this->service = new Service();
        $this->enterprise =
          Core::$container->has('user') ? Core::$container->get('user') : new Enterprise();
        $this->enterpriseTemplateId = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->statusTime = 0;
        $this->repository = new SystemTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->description);
        unset($this->cover);
        unset($this->category);
        unset($this->contents);
        unset($this->price);
        unset($this->isFree);
        unset($this->viewNumber);
        unset($this->useNumber);
        unset($this->isDefault);
        unset($this->crew);
        unset($this->validPeriod);
        unset($this->expireTime);
        unset($this->lastUseTime);
        unset($this->enterprise);
        unset($this->service);
        unset($this->enterpriseTemplate);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setCover(array $cover) : void
    {
        $this->cover = $cover;
    }

    public function getCover() : array
    {
        return $this->cover;
    }

    public function setViewNumber(int $viewNumber) : void
    {
        $this->viewNumber = $viewNumber;
    }

    public function getViewNumber() : int
    {
        return $this->viewNumber;
    }

    public function setUseNumber(int $useNumber) : void
    {
        $this->useNumber = $useNumber;
    }

    public function getUseNumber() : int
    {
        return $this->useNumber;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array($category, array_values(self::CATEGORY)) ?
            $category : self::CATEGORY['ENTERPRISE'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setContents(string $contents) : void
    {
        $this->contents = $contents;
    }

    public function getContents() : string
    {
        return $this->contents;
    }

    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function setIsFree(int $isFree) : void
    {
        $this->isFree = in_array($isFree, array_values(self::IS_FREE)) ?
            $isFree : self::IS_FREE['YES'];
    }

    public function getIsFree() : int
    {
        return $this->isFree;
    }

    public function setIsDefault(int $isDefault) : void
    {
        $this->isDefault = in_array($isDefault, array_values(self::IS_DEFAULT)) ?
            $isDefault : self::IS_DEFAULT['YES'];
    }

    public function getIsDefault() : int
    {
        return $this->isDefault;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setValidPeriod(int $validPeriod) : void
    {
        $this->validPeriod = $validPeriod;
    }

    public function getValidPeriod() : int
    {
        return $this->validPeriod;
    }

    public function setExpireTime(int $expireTime) : void
    {
        $this->expireTime = $expireTime;
    }

    public function getExpireTime() : int
    {
        return $this->expireTime;
    }

    public function setLastUseTime(int $lastUseTime) : void
    {
        $this->lastUseTime = $lastUseTime;
    }

    public function getLastUseTime() : int
    {
        return $this->lastUseTime;
    }

    public function setService(Service $service) : void
    {
        $this->service = $service;
    }

    public function getService() : Service
    {
        return $this->service;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setEnterpriseTemplateId(int $enterpriseTemplateId) : void
    {
        $this->enterpriseTemplateId = $enterpriseTemplateId;
    }

    public function getEnterpriseTemplateId() : int
    {
        return $this->enterpriseTemplateId;
    }

    protected function getRepository(): SystemTemplateRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    public function edit() : bool
    {
        if (!$this->validateStatusScenario()) {
            return false;
        }

        $repository = $this->getIOperatAbleAdapter();

        return $repository->edit($this);
    }

    protected function validateStatusScenario() : bool
    {
        if($this->getStatus() == self::STATUS['ENABLED']){
            Core::setLastError(RESOURCE_STATUS_NOT_OFF_STOCK,array('pointer'=>'status'));
            return false;
        }

        return true;
    }

    //增加浏览量
    public function incrByView() : bool
    {
        return $this->getRepository()->incrByView($this);
    }

    //增加使用量
    public function incrByUse() : bool
    {
        return $this->getRepository()->incrByUse($this);
    }

    protected function validateDefaultStatusScenario() : bool
    {
        if($this->getIsDefault() == self::IS_DEFAULT['NO']){
            Core::setLastError(RESOURCE_STATUS_NO_SET_DEFAULT,array('pointer'=>'isDefault'));
            return false;
        }

        return true;
    }

    //设置默认
    public function setDefault() : bool
    {
        if ($this->validateDefaultStatusScenario()) {
            return false;
        }

        $repository = $this->getIOperatAbleAdapter();

        return $repository->setDefault($this);
    }

    //取消默认
    public function cancelSetDefault() : bool
    {
        if (!$this->validateDefaultStatusScenario()) {
            return false;
        }

        $repository = $this->getIOperatAbleAdapter();

        return $repository->cancelSetDefault($this);
    }
}
