<?php
namespace Trial\Distribution\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IEnableAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\EnableAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

use Trial\Crew\Model\Crew;

use Trial\Distribution\Repository\ElementRepository;

class Element implements IObject, IOperatAble, IEnableAble
{
    use Object, OperatAbleTrait, EnableAbleTrait;

    const CATEGORY = [
        'NULL'=>0,
        'SERVICE' => 1, //服务
        'ENTERPRISE' => 2 //企业
    ];

    private $id;

    private $repository;

    private $name;

    private $mark;

    private $crew;

    private $category;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->mark = '';
        $this->category = self::CATEGORY['ENTERPRISE'];
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new ElementRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->mark);
        unset($this->category);
        unset($this->crew);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }
    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }
    public function getName() : string
    {
        return $this->name;
    }

    public function setMark(string $mark) : void
    {
        $this->mark = $mark;
    }

    public function getMark() : string
    {
        return $this->mark;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }
    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array($category, array_values(self::CATEGORY)) ?
            $category : self::CATEGORY['ENTERPRISE'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    protected function getRepository(): ElementRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }
}
