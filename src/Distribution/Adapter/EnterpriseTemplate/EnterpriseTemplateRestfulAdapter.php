<?php
namespace Trial\Distribution\Adapter\EnterpriseTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Distribution\Model\EnterpriseTemplate;
use Trial\Distribution\Model\NullEnterpriseTemplate;
use Trial\Distribution\Translator\EnterpriseTemplateRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class EnterpriseTemplateRestfulAdapter extends GuzzleAdapter implements IEnterpriseTemplateAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'ENTERPRISE_TEMPLATE_LIST'=>[
                'fields'=>[],
                'include'=> ''
            ],
            'ENTERPRISE_TEMPLATE_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'systemTemplate, enterprise, service'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new EnterpriseTemplateRestfulTranslator();
        $this->resource = 'distributionEnterpriseTemplates';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullEnterpriseTemplate::getInstance());
    }

    protected function addAction(EnterpriseTemplate $enterpriseTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $enterpriseTemplate,
            array(
                'validPeriod',
                'systemTemplate',
                'enterprise',
                'service'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterpriseTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(EnterpriseTemplate $enterpriseTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $enterpriseTemplate,
            array(
                'validPeriod',
                'systemTemplate',
                'enterprise',
                'service'
            )
        );

        $this->patch(
            $this->getResource().'/'.$enterpriseTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterpriseTemplate);
            return true;
        }

        return false;
    }

    public function updateLastUseTime(EnterpriseTemplate $enterpriseTemplate) : bool
    {
        $this->patch(
            $this->getResource().'/'.$enterpriseTemplate->getId().'/'.'updateLastUseTime'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterpriseTemplate);
            return true;
        }

        return false;
    }

    public function renew(EnterpriseTemplate $enterpriseTemplate) : bool
    {
        $this->patch(
            $this->getResource().'/'.$enterpriseTemplate->getId().'/'.'renew'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterpriseTemplate);
            return true;
        }

        return false;
    }
}
