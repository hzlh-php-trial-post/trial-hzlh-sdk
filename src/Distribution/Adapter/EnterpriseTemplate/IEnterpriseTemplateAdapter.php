<?php
namespace Trial\Distribution\Adapter\EnterpriseTemplate;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

interface IEnterpriseTemplateAdapter extends IFetchAbleAdapter, IOperatAbleAdapter
{
}
