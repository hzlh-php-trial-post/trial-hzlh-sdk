<?php
namespace Trial\Distribution\Adapter\EnterpriseTemplate;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleMockAdapterTrait;

use Trial\Distribution\Model\EnterpriseTemplate;
use Trial\Distribution\Utils\MockFactory;

class EnterpriseTemplateMockAdapter implements IEnterpriseTemplateAdapter
{
    use OperatAbleMockAdapterTrait, ModifyStatusAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateEnterpriseTemplateObject($id);
    }

    public function fetchList(array $ids): array
    {
        $list = array();

        foreach ($ids as $id) {
            $list[] = MockFactory::generateEnterpriseTemplateObject($id);
        }

        return $list;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateEnterpriseTemplateObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $list = array();

        foreach ($ids as $id) {
            $list[] = MockFactory::generateEnterpriseTemplateObject($id);
        }

        return $list;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }
}
