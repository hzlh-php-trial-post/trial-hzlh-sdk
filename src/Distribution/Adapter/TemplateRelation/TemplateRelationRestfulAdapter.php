<?php
namespace Trial\Distribution\Adapter\TemplateRelation;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Distribution\Model\EnterpriseTemplate;
use Trial\Distribution\Model\NullEnterpriseTemplate;
use Trial\Distribution\Translator\SystemTemplateRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class TemplateRelationRestfulAdapter extends GuzzleAdapter implements ITemplateRelationAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_SYSTEM_TEMPLATE_LIST'=>[
                'fields'=>[],
                'include'=> ''
            ],
            'ENTERPRISE_TEMPLATE_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'systemTemplate, enterprise, service'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new SystemTemplateRestfulTranslator();
        $this->resource = 'distributionTemplates';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullEnterpriseTemplate::getInstance());
    }

    protected function addAction(EnterpriseTemplate $enterpriseTemplate) : bool
    {
        unset($enterpriseTemplate);
        return false;
    }

    protected function editAction(EnterpriseTemplate $enterpriseTemplate) : bool
    {
        unset($enterpriseTemplate);
        return false;
    }
}
