<?php
namespace Trial\Distribution\Adapter\TemplateRelation;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface ITemplateRelationAdapter extends IFetchAbleAdapter, IOperatAbleAdapter
{
}
