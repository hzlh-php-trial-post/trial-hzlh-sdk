<?php
namespace Trial\Distribution\Adapter\SystemTemplate;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

interface ISystemTemplateAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter
{
}
