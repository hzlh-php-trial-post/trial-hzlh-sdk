<?php
namespace Trial\Distribution\Adapter\SystemTemplate;

use Trial\Common\Adapter\Document\DocumentAdapter;

class SystemTemplateDocumentAdapter extends DocumentAdapter
{
    const DBNAME = 'systemTemplate';

    const COLLECTIONNAME = 'system_template_html';

    public function __construct()
    {
        parent::__construct(self::DBNAME, self::COLLECTIONNAME);
    }
}
