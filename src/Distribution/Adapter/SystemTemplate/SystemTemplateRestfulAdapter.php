<?php
namespace Trial\Distribution\Adapter\SystemTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Distribution\Model\SystemTemplate;
use Trial\Distribution\Model\NullSystemTemplate;
use Trial\Distribution\Translator\SystemTemplateRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class SystemTemplateRestfulAdapter extends GuzzleAdapter implements ISystemTemplateAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'SYSTEM_TEMPLATE_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'SYSTEM_TEMPLATE_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new SystemTemplateRestfulTranslator();
        $this->resource = 'distributionSystemTemplates';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullSystemTemplate::getInstance());
    }

    protected function addAction(SystemTemplate $systemTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $systemTemplate,
            array(
                'title',
                'description',
                'cover',
                'contents',
                'category',
                'price',
                'isFree',
                'isDefault',
                'crew'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(SystemTemplate $systemTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $systemTemplate,
            array(
                'title',
                'description',
                'cover',
                'contents',
                'category',
                'price',
                'isFree',
                'isDefault',
                'crew'
            )
        );

        $this->patch(
            $this->getResource().'/'.$systemTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemTemplate);
            return true;
        }

        return false;
    }

    public function incrByUse(SystemTemplate $systemTemplate) : bool
    {
        $this->patch(
            $this->getResource().'/'.$systemTemplate->getId().'/'.'incrByUse'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemTemplate);
            return true;
        }

        return false;
    }

    public function incrByView(SystemTemplate $systemTemplate) : bool
    {
        $this->patch(
            $this->getResource().'/'.$systemTemplate->getId().'/'.'incrByView'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemTemplate);
            return true;
        }

        return false;
    }

    public function setDefault(SystemTemplate $systemTemplate) : bool
    {
        $this->patch(
            $this->getResource().'/'.$systemTemplate->getId().'/'.'setDefault'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemTemplate);
            return true;
        }

        return false;
    }

    public function cancelSetDefault(SystemTemplate $systemTemplate) : bool
    {
        $this->patch(
            $this->getResource().'/'.$systemTemplate->getId().'/'.'cancelSetDefault'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemTemplate);
            return true;
        }

        return false;
    }
}
