<?php
namespace Trial\Distribution\Adapter\SystemTemplate;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleMockAdapterTrait;

use Trial\Distribution\Model\SystemTemplate;
use Trial\Distribution\Utils\MockFactory;

class SystemTemplateMockAdapter implements ISystemTemplateAdapter
{
    use OperatAbleMockAdapterTrait, ModifyStatusAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateSystemTemplateObject($id);
    }

    public function fetchList(array $ids): array
    {
        $serviceList = array();

        foreach ($ids as $id) {
            $serviceList[] = MockFactory::generateSystemTemplateObject($id);
        }

        return $serviceList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateSystemTemplateObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $serviceList = array();

        foreach ($ids as $id) {
            $serviceList[] = MockFactory::generateSystemTemplateObject($id);
        }

        return $serviceList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);

        return array($this->fetchList($ids), $count);
    }
}
