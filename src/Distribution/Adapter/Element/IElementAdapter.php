<?php
namespace Trial\Distribution\Adapter\Element;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

interface IElementAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter
{
}
