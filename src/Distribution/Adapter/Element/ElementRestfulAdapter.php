<?php
namespace Trial\Distribution\Adapter\Element;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Distribution\Model\Element;
use Trial\Distribution\Model\NullElement;
use Trial\Distribution\Translator\ElementRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class ElementRestfulAdapter extends GuzzleAdapter implements IElementAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'ELEMENT_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'ELEMENT_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ElementRestfulTranslator();
        $this->resource = 'distributionElements';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullElement::getInstance());
    }

    protected function addAction(Element $element) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $element,
            array(
                'name',
                'mark',
                'category',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($element);
            return true;
        }

        return false;
    }

    protected function editAction(Element $element) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $element,
            array(
                'name',
                'mark',
                'category',
                'crew'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$element->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($element);
            return true;
        }

        return false;
    }
}
