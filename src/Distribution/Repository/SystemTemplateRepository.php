<?php
namespace Trial\Distribution\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Distribution\Adapter\SystemTemplate\ISystemTemplateAdapter;
use Trial\Distribution\Adapter\SystemTemplate\SystemTemplateMockAdapter;
use Trial\Distribution\Adapter\SystemTemplate\SystemTemplateRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Distribution\Model\SystemTemplate;

class SystemTemplateRepository extends Repository implements ISystemTemplateAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'SYSTEM_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'SYSTEM_TEMPLATE_FETCH_ONE';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_SYSTEM_TEMPLATE_LIST';

    public function __construct()
    {
        $this->adapter = new SystemTemplateRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ISystemTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ISystemTemplateAdapter
    {
        return new SystemTemplateMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function incrByView(SystemTemplate $systemTemplate) : bool
    {
        return $this->getAdapter()->incrByView($systemTemplate);
    }

    public function incrByUse(SystemTemplate $systemTemplate) : bool
    {
        return $this->getAdapter()->incrByUse($systemTemplate);
    }

    public function setDefault(SystemTemplate $systemTemplate) : bool
    {
        return $this->getAdapter()->setDefault($systemTemplate);
    }

    public function cancelSetDefault(SystemTemplate $systemTemplate) : bool
    {
        return $this->getAdapter()->cancelSetDefault($systemTemplate);
    }
}
