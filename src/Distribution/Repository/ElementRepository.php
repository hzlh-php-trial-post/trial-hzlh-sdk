<?php
namespace Trial\Distribution\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Distribution\Adapter\Element\IElementAdapter;
use Trial\Distribution\Adapter\Element\ElementMockAdapter;
use Trial\Distribution\Adapter\Element\ElementRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class ElementRepository extends Repository implements IElementAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ELEMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'ELEMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ElementRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IElementAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IElementAdapter
    {
        return new ElementMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
