<?php
namespace Trial\Distribution\Repository;

use Marmot\Framework\Classes\Repository;
use Trial\Distribution\Adapter\SystemTemplate\SystemTemplateDocumentAdapter;
use Trial\Common\Repository\DocumentRepositoryTrait;

class SystemTemplateDocumentRepository extends Repository
{
	use DocumentRepositoryTrait;

	private $adapter;

	public function __construct()
    {
        $this->adapter = new SystemTemplateDocumentAdapter();
    }

    public function getAdapter() : SystemTemplateDocumentAdapter
    {
        return $this->adapter;
    }

    public function getActualAdapter() : SystemTemplateDocumentAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : SystemTemplateDocumentAdapter
    {
        return $this->adapter;
    }
}
