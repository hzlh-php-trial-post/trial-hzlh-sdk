<?php
namespace Trial\Distribution\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Distribution\Adapter\TemplateRelation\ITemplateRelationAdapter;
use Trial\Distribution\Adapter\TemplateRelation\TemplateRelationMockAdapter;
use Trial\Distribution\Adapter\TemplateRelation\TemplateRelationRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Distribution\Model\SystemTemplate;

class TemplateRelationRepository extends Repository implements ITemplateRelationAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'SYSTEM_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'SYSTEM_TEMPLATE_FETCH_ONE';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_SYSTEM_TEMPLATE_LIST';

    public function __construct()
    {
        $this->adapter = new TemplateRelationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ITemplateRelationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ITemplateRelationAdapter
    {
        return new TemplateRelationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
