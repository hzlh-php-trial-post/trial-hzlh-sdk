<?php
namespace Trial\Distribution\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Distribution\Adapter\EnterpriseTemplate\IEnterpriseTemplateAdapter;
use Trial\Distribution\Adapter\EnterpriseTemplate\EnterpriseTemplateMockAdapter;
use Trial\Distribution\Adapter\EnterpriseTemplate\EnterpriseTemplateRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Distribution\Model\EnterpriseTemplate;

class EnterpriseTemplateRepository extends Repository implements IEnterpriseTemplateAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ENTERPRISE_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'ENTERPRISE_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new EnterpriseTemplateRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IEnterpriseTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IEnterpriseTemplateAdapter
    {
        return new EnterpriseTemplateMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function updateLastUseTime(EnterpriseTemplate $enterpriseTemplate) : bool
    {
        return $this->getAdapter()->updateLastUseTime($enterpriseTemplate);
    }

    public function renew(EnterpriseTemplate $enterpriseTemplate) : bool
    {
        return $this->getAdapter()->renew($enterpriseTemplate);
    }
}
