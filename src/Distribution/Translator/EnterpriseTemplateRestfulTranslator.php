<?php
namespace Trial\Distribution\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Distribution\Model\EnterpriseTemplate;
use Trial\Distribution\Model\NullEnterpriseTemplate;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;
use Trial\Service\Translator\ServiceRestfulTranslator;
use Trial\Distribution\Translator\SystemTemplateRestfulTranslator;

class EnterpriseTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getServiceRestfulTranslator() : ServiceRestfulTranslator
    {
        return new ServiceRestfulTranslator();
    }

    public function getSystemTemplateRestfulTranslator() : SystemTemplateRestfulTranslator
    {
        return new SystemTemplateRestfulTranslator();
    }

    public function arrayToObject(array $expression, $enterpriseTemplate = null)
    {
        return $this->translateToObject($expression, $enterpriseTemplate);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $enterpriseTemplate = null)
    {

        if (empty($expression)) {
            return NullEnterpriseTemplate::getInstance();
        }

        if ($enterpriseTemplate == null) {
            $enterpriseTemplate = new EnterpriseTemplate();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $enterpriseTemplate->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['validPeriod'])) {
            $enterpriseTemplate->setValidPeriod($attributes['validPeriod']);
        }
        if (isset($attributes['expireTime'])) {
            $enterpriseTemplate->setExpireTime($attributes['expireTime']);
        }
        if (isset($attributes['lastUseTime'])) {
            $enterpriseTemplate->setLastUseTime($attributes['lastUseTime']);
        }
        if (isset($attributes['createTime'])) {
            $enterpriseTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $enterpriseTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $enterpriseTemplate->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $enterpriseTemplate->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['service']['data'])) {
            $service = $this->changeArrayFormat($relationships['service']['data']);
            $enterpriseTemplate->setService($this->getServiceRestfulTranslator()->arrayToObject($service));
        }
        if (isset($relationships['systemTemplate']['data'])) {
          $systemTemplate = $this->changeArrayFormat($relationships['systemTemplate']['data']);
          $enterpriseTemplate->setSystemTemplate($this->getSystemTemplateRestfulTranslator()->arrayToObject($systemTemplate));
        }
        if (isset($relationships['enterprise']['data'])) {
          $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
          $enterpriseTemplate->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }

        return $enterpriseTemplate;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($enterpriseTemplate, array $keys = array())
    {
        $expression = array();

        if (!$enterpriseTemplate instanceof EnterpriseTemplate) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'validPeriod',
                'systemTemplate',
                'enterprise',
                'service'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'distributionEnterpriseTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $enterpriseTemplate->getId();
        }

        $attributes = array();

        if (in_array('validPeriod', $keys)) {
            $attributes['validPeriod'] = $enterpriseTemplate->getValidPeriod();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('service', $keys)) {
            $expression['data']['relationships']['service']['data'] = array(
                array(
                    'type' => 'services',
                    'id' => $enterpriseTemplate->getService()->getId()
                )
             );
        }
        if (in_array('enterprise', $keys)) {
          $expression['data']['relationships']['enterprise']['data'] = array(
              array(
                  'type' => 'enterprises',
                  'id' => $enterpriseTemplate->getEnterprise()->getId()
              )
           );
        }
        if (in_array('systemTemplate', $keys)) {
          $expression['data']['relationships']['systemTemplate']['data'] = array(
              array(
                  'type' => 'systemTemplates',
                  'id' => $enterpriseTemplate->getSystemTemplate()->getId()
              )
           );
        }

        return $expression;
    }
}
