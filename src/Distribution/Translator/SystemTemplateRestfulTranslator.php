<?php
namespace Trial\Distribution\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Distribution\Model\SystemTemplate;
use Trial\Distribution\Model\NullSystemTemplate;
use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\Crew\Translator\CrewRestfulTranslator;
use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;
use Trial\Service\Translator\ServiceRestfulTranslator;

class SystemTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function getServiceRestfulTranslator()
    {
        return new ServiceRestfulTranslator();
    }

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $systemTemplate = null)
    {
        return $this->translateToObject($expression, $systemTemplate);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $systemTemplate = null)
    {
        if (empty($expression)) {
            return NullSystemTemplate::getInstance();
        }

        if ($systemTemplate == null) {
            $systemTemplate = new SystemTemplate();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $systemTemplate->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $systemTemplate->setTitle($attributes['title']);
        }
        if (isset($attributes['description'])) {
            $systemTemplate->setDescription($attributes['description']);
        }
        if (isset($attributes['category'])) {
            $systemTemplate->setCategory($attributes['category']);
        }
        if (isset($attributes['contents'])) {
            $systemTemplate->setContents($attributes['contents']);
        }
        if (isset($attributes['price'])) {
            $systemTemplate->setPrice($attributes['price']);
        }
        if (isset($attributes['isFree'])) {
            $systemTemplate->setIsFree($attributes['isFree']);
        }
        if (isset($attributes['cover'])) {
            $systemTemplate->setCover($attributes['cover']);
        }
        if (isset($attributes['viewNumber'])) {
            $systemTemplate->setViewNumber($attributes['viewNumber']);
        }
        if (isset($attributes['useNumber'])) {
            $systemTemplate->setUseNumber($attributes['useNumber']);
        }
        if (isset($attributes['validPeriod'])) {
            $systemTemplate->setValidPeriod($attributes['validPeriod']);
        }
        if (isset($attributes['expireTime'])) {
            $systemTemplate->setExpireTime($attributes['expireTime']);
        }
        if (isset($attributes['lastUseTime'])) {
            $systemTemplate->setLastUseTime($attributes['lastUseTime']);
        }
        if (isset($attributes['isDefault'])) {
            $systemTemplate->setIsDefault($attributes['isDefault']);
        }
        if (isset($attributes['enterpriseTemplateId'])) {
            $systemTemplate->setEnterpriseTemplateId($attributes['enterpriseTemplateId']);
        }
        if (isset($attributes['createTime'])) {
            $systemTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $systemTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $systemTemplate->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $systemTemplate->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $systemTemplate->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        if (isset($relationships['service']['data'])) {
          $service = $this->changeArrayFormat($relationships['service']['data']);
          $systemTemplate->setService($this->getServiceRestfulTranslator()->arrayToObject($service));
        }

        if (isset($relationships['enterprise']['data'])) {
          $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
          $systemTemplate->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }

        return $systemTemplate;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($systemTemplate, array $keys = array())
    {
        $expression = array();

        if (!$systemTemplate instanceof SystemTemplate) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'title',
                'description',
                'cover',
                'contents',
                'category',
                'price',
                'isFree',
                'isDefault',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'distributionSystemTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $systemTemplate->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $systemTemplate->getTitle();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $systemTemplate->getDescription();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $systemTemplate->getCover();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $systemTemplate->getCategory();
        }
        if (in_array('contents', $keys)) {
            $attributes['contents'] = $systemTemplate->getContents();
        }
        if (in_array('price', $keys)) {
            $attributes['price'] = $systemTemplate->getPrice();
        }
        if (in_array('isFree', $keys)) {
            $attributes['isFree'] = $systemTemplate->getIsFree();
        }
        if (in_array('isDefault', $keys)) {
          $attributes['isDefault'] = $systemTemplate->getIsDefault();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $systemTemplate->getCrew()->getId()
                )
             );
        }

        return $expression;
    }
}
