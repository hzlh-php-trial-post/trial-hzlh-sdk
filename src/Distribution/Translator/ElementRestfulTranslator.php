<?php
namespace Trial\Distribution\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Distribution\Model\Element;
use Trial\Distribution\Model\NullElement;
use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\Crew\Translator\CrewRestfulTranslator;

class ElementRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $element = null)
    {
        return $this->translateToObject($expression, $element);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $element = null)
    {
        if (empty($expression)) {
            return NullElement::getInstance();
        }

        if ($element == null) {
            $element = new Element();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $element->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $element->setName($attributes['name']);
        }
        if (isset($attributes['mark'])) {
            $element->setMark($attributes['mark']);
        }
        if (isset($attributes['category'])) {
            $element->setCategory($attributes['category']);
        }
        if (isset($attributes['createTime'])) {
            $element->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $element->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $element->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $element->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $element->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }
        
        return $element;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($element, array $keys = array())
    {
        $expression = array();

        if (!$element instanceof Element) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'name',
                'mark',
                'category',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'distributionElements'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $element->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $element->getName();
        }
        if (in_array('mark', $keys)) {
            $attributes['mark'] = $element->getMark();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $element->getCategory();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $element->getCrew()->getId()
                )
             );
        }

        return $expression;
    }
}
