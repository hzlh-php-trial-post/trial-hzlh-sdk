<?php
namespace Trial\Role\Adapter\Role;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

use Trial\Role\Model\Role;

interface IRoleAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter, IAsyncAdapter //phpcs:ignore
{
    public function permission(Role $permission) : bool;
}
