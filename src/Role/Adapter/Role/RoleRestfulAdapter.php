<?php
namespace Trial\Role\Adapter\Role;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Role\Model\Role;
use Trial\Role\Model\NullRole;
use Trial\Role\Translator\RoleRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class RoleRestfulAdapter extends GuzzleAdapter implements IRoleAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'ROLE_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'ROLE_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new RoleRestfulTranslator();
        $this->resource = 'roles';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => ROLE_NAME_EXIST
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullRole::getInstance());
    }

    protected function addAction(Role $role) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $role,
            array(
                'name',
                'description',
                'status',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($role);
            return true;
        }

        return false;
    }
        
    protected function editAction(Role $role) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $role,
            array(
                'name',
                'description'
            )
        );

        $this->patch(
            $this->getResource().'/'.$role->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($role);
            return true;
        }

        return false;
    }

    public function permission(Role $role) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $role,
            array('permission')
        );

        $this->patch(
            $this->getResource().'/'.$role->getId().'/permission',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($role);
            return true;
        }

        return false;
    }
}
