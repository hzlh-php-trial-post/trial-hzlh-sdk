<?php
namespace Trial\Role\Translator;

use Trial\Role\Model\Role;
use Trial\Role\Model\NullRole;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Crew\Translator\CrewRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class RoleRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $role = null)
    {
        return $this->translateToObject($expression, $role);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $role = null)
    {
        if (empty($expression)) {
            return NullRole::getInstance();
        }

        if ($role == null) {
            $role = new Role();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $role->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';
        
        if (isset($attributes['name'])) {
            $role->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $role->setDescription($attributes['description']);
        }
        if (isset($attributes['permission'])) {
            $role->setPermission($attributes['permission']);
        }
        if (isset($attributes['createTime'])) {
            $role->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $role->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $role->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $role->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $role->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }
        
        return $role;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($role, array $keys = array())
    {
        $expression = array();

        if (!$role instanceof Role) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'permission',
                'status',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'roles'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $role->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $role->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $role->getDescription();
        }
        if (in_array('permission', $keys)) {
            $attributes['permission'] = $role->getPermission();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $role->getStatus();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $role->getCrew()->getId()
                )
             );
        }

        return $expression;
    }
}
