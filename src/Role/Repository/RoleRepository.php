<?php
namespace Trial\Role\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\Role\Model\Role;
use Trial\Role\Adapter\Role\IRoleAdapter;
use Trial\Role\Adapter\Role\RoleMockAdapter;
use Trial\Role\Adapter\Role\RoleRestfulAdapter;

class RoleRepository extends Repository implements IRoleAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ROLE_LIST';
    const FETCH_ONE_MODEL_UN = 'ROLE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new RoleRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IRoleAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IRoleAdapter
    {
        return new RoleMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function permission(Role $role) : bool
    {
        return $this->getAdapter()->permission($role);
    }
}
