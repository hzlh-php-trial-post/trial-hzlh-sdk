<?php
namespace Trial\NaturalPerson\Adapter\NaturalPerson;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface INaturalPersonAdapter extends IFetchAbleAdapter, IApplyAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
