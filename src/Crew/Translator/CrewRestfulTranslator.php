<?php
namespace Trial\Crew\Translator;

use Trial\Crew\Model\Crew;
use Trial\Crew\Model\NullCrew;
use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\User\Translator\UserRestfulTranslator;

class CrewRestfulTranslator extends UserRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $crew = null)
    {
        return $this->translateToObject($expression, $crew);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $crew = null)
    {
        if (empty($expression)) {
            return NullCrew::getInstance();
        }

        if ($crew == null) {
            $crew = new Crew();
        }

        $crew = parent::translateToObject($expression, $crew);

        $data =  $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['workNumber'])) {
            $crew->setWorkNumber($attributes['workNumber']);
        }
        if (isset($attributes['remark'])) {
            $crew->setRemark($attributes['remark']);
        }
        if (isset($attributes['loginTime'])) {
            $crew->setSignInTime($attributes['loginTime']);
        }
        if (isset($attributes['roles'])) {
            $crew->setRoles($attributes['roles']);
        }
        return $crew;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {
        $user = parent::objectToArray($crew, $keys);

        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'workNumber',
                'remark',
                'status',
                'roles',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'crews',
                'id'=>$crew->getId()
            )
        );

        $attributes = array();

        if (in_array('workNumber', $keys)) {
            $attributes['workNumber'] = $crew->getWorkNumber();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $crew->getStatus();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $crew->getRemark();
        }
        if (in_array('roles', $keys)) {
            $attributes['roles'] = $crew->getRoles();
        }

        $expression['data']['attributes'] = array_merge($user['data']['attributes'], $attributes);

        return $expression;
    }
}
