<?php
namespace Trial\Crew\Adapter\Crew;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;
use Trial\Crew\Model\Crew;

use Marmot\Interfaces\IAsyncAdapter;

interface ICrewAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter
{
    public function signIn(Crew $crew);

    public function updatePassword(Crew $crew);
}
