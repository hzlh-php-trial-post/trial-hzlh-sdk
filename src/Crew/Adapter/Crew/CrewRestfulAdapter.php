<?php
namespace Trial\Crew\Adapter\Crew;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Crew\Model\Crew;
use Trial\Crew\Model\NullCrew;
use Trial\Crew\Translator\CrewRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class CrewRestfulAdapter extends GuzzleAdapter implements ICrewAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'CREW_LIST'=>[
            'fields'=>[
                'crews'=>'cellphone,realName,gender,workNumber,createTime,updateTime,status,loginTime'
            ]
        ],
        'CREW_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CrewRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'crews';
    }

    protected function getResource()
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            10 => CELLPHONE_NOT_EXIST,
            109 => STATUS_DISABLED,
            122 => CREW_REMARK_FORMAT_ERROR,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCrew::getInstance());
    }

    public function signIn(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array(
                'cellphone','password'
            )
        );
        $this->post(
            $this->getResource().'/signIn',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }

        return false;
    }

    public function updatePassword(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array(
                'oldPassword',
                'password'
            )
        );

        $this->patch(
            $this->getResource().'/'.$crew->getId().'/updatePassword',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }

        return false;
    }

    protected function addAction(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array('realName','cellphone','password','status','remark')
        );
        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }

        return false;
    }

    protected function editAction(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array('realName','cellphone','status','remark')
        );

        $this->patch(
            $this->getResource().'/'.$crew->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }

        return false;
    }

    public function passwordInit(Crew $crew) : bool
    {
        return $this->passwordInitAction($crew);
    }

    protected function passwordInitAction(Crew $crew) : bool
    {
        $this->patch(
            $this->getResource().'/'.$crew->getId().'/passwordInit'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }
        return false;
    }

    public function distributionRole(Crew $crew) : bool
    {
        return $this->distributionRoleAction($crew);
    }

    protected function distributionRoleAction(Crew $crew) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $crew,
            array('roles')
        );

        $this->patch(
            $this->getResource().'/'.$crew->getId().'/distributionRoles',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($crew);
            return true;
        }
        return false;
    }
}
