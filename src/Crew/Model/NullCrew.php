<?php
namespace Trial\Crew\Model;

use Marmot\Interfaces\INull;

use Trial\Common\Model\NullEnableAbleTrait;
use Trial\Common\Model\NullOperatAbleTrait;

class NullCrew extends Crew implements INull
{
    use NullEnableAbleTrait, NullOperatAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
