<?php
namespace Trial\Contract\Adapter\Contract;

use Qxy\Contract\Contract\Adapter\Contract\ContractRestfulAdapter;

use Trial\Enterprise\Repository\EnterpriseRepository;
use Trial\Order\ServiceOrder\Repository\ServiceOrderRepository;

class ContractSdkRestfulAdapter extends ContractRestfulAdapter
{
    private $enterpriseRepository;

    private $orderRepository;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->enterpriseRepository = new EnterpriseRepository();
        $this->orderRepository = new ServiceOrderRepository();
    }

    public function __destruct()
    {
        unset($this->enterpriseRepository);
        unset($this->orderRepository);
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->orderRepository;
    }

    protected function fetchListEnterprise(array $ids) : array
    {
        return $this->getEnterpriseRepository()
                ->scenario(EnterpriseRepository::PORTAL_LIST_MODEL_UN)
                ->fetchList($ids);
    }

    protected function fetchListServiceOrder(array $ids) : array
    {
        return $this->getServiceOrderRepository()
                ->scenario(ServiceOrderRepository::PORTAL_LIST_MODEL_UN)
                ->fetchList($ids);
    }


    protected function fetchParts(array $partAs, array $partBs) : array
    {
        $partAs = $this->fetchListEnterprise($partAs);
        $partBs = $this->fetchListEnterprise($partBs);

        $partAs = empty($partAs[1]) ? [] : $partAs[1];
        $partBs = empty($partBs[1]) ? [] : $partBs[1];

        return [$partAs, $partBs];
    }
    protected function fetchOrder(array $order) : array
    {
        $order = $this->fetchListServiceOrder($order);

        return empty($order[1]) ? [] : $order[1];
    }
}
