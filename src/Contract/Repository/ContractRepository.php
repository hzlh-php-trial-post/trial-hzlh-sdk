<?php
namespace Trial\Contract\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\Contract\Adapter\Contract\ContractSdkRestfulAdapter;

use Marmot\Core;

class ContractRepository
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CONTRACT_LIST';
    const FETCH_ONE_MODEL_UN = 'CONTRACT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ContractSdkRestfulAdapter(
            Core::$container->has('contract.sdk.url') ? Core::$container->get('contract.sdk.url') : '',
            Core::$container->has('contract.sdk.authKey') ? Core::$container->get('contract.sdk.authKey') : []
        );
    }

    public function getAdapter() : ContractSdkRestfulAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
