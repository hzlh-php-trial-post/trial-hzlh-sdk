<?php
namespace Trial\Evaluation\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Evaluation\Model\EvaluationScore;
use Trial\Evaluation\Model\NullEvaluationScore;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;

class EvaluationScoreRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $evaluationScore = null)
    {
        return $this->translateToObject($expression, $evaluationScore);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $evaluationScore = null)
    {
        if (empty($expression)) {
            return NullEvaluationScore::getInstance();
        }

        if ($evaluationScore == null) {
            $evaluationScore = new EvaluationScore();
        }

        $data =  $expression['data'];
        if (isset($data['id'])) {
            $id = $data['id'];
            $evaluationScore->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['relationCategory'])) {
            $evaluationScore->setRelationCategory($attributes['relationCategory']);
        }
        if (isset($attributes['commodityStarReview'])) {
            $evaluationScore->setCommodityStarReview($attributes['commodityStarReview']);
        }
        if (isset($attributes['enterpriseStarReview'])) {
            $evaluationScore->setEnterpriseStarReview($attributes['enterpriseStarReview']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);

            $evaluationScore->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }

        return $evaluationScore;
    }

    public function objectToArray($evaluationScore, array $keys = array())
    {
        unset($evaluationScore);
        unset($keys);

        return array();
    }
}
