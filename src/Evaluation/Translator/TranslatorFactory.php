<?php
namespace Trial\Evaluation\Translator;

use Trial\Evaluation\Model\Evaluation;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Classes\NullTranslator;

class TranslatorFactory
{
    const MAPS = array(
        Evaluation::RELATION_CATEGORY['SERVICE'] =>
            'Trial\Service\Translator\ServiceRestfulTranslator'
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
