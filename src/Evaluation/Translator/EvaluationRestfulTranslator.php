<?php
namespace Trial\Evaluation\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Trial\Evaluation\Model\Evaluation;
use Trial\Evaluation\Model\NullEvaluation;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;
use Trial\Member\Translator\MemberRestfulTranslator;
use Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator;

class EvaluationRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getOrderRestfulTranslator()
    {
        return new ServiceOrderRestfulTranslator();
    }

    public function getEvaluationScoreRestfulTranslator()
    {
        return new EvaluationScoreRestfulTranslator();
    }

    public function getTranslatorFactory()
    {
        return new TranslatorFactory();
    }

    public function arrayToObject(array $expression, $evaluation = null)
    {
        return $this->translateToObject($expression, $evaluation);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $evaluation = null)
    {

        if (empty($expression)) {
            return NullEvaluation::getInstance();
        }

        if ($evaluation == null) {
            $evaluation = new Evaluation();
        }

        $data =  $expression['data'];
        if (isset($data['id'])) {
            $id = $data['id'];
            $evaluation->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['reason'])) {
            $evaluation->setReason($attributes['reason']);
        }
        if (isset($attributes['relationCategory'])) {
            $evaluation->setRelationCategory($attributes['relationCategory']);
        }
        if (isset($attributes['content'])) {
            $evaluation->setContent($attributes['content']);
        }
        if (isset($attributes['picture'])) {
            $evaluation->setPicture($attributes['picture']);
        }
        if (isset($attributes['status'])) {
            $evaluation->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $evaluation->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $evaluation->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $evaluation->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['commodityName'])) {
            $evaluation->setCommodityName($attributes['commodityName']);
        }
        if (isset($attributes['evaluationType'])) {
            $evaluation->setEvaluationType($attributes['evaluationType']);
        }
        if (isset($attributes['isAuto'])) {
            $evaluation->setIsAuto($attributes['isAuto']);
        }
        if (isset($attributes['isReply'])) {
            $evaluation->setIsReply($attributes['isReply']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);

            $evaluation->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);

            $evaluation->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }
        if (isset($relationships['order']['data'])) {
            $order = $this->changeArrayFormat($relationships['order']['data']);

            $evaluation->setOrder($this->getOrderRestfulTranslator()->arrayToObject($order));
        }
        if (isset($relationships['evaluationScore']['data'])) {
            $evaluationScore = $this->changeArrayFormat($relationships['evaluationScore']['data']);

            $evaluation->setEvaluationScore(
                $this->getEvaluationScoreRestfulTranslator()
                    ->arrayToObject($evaluationScore)
            );
        }
        if (isset($relationships['relation']['data'])) {
            if (isset($expression['included'])) {
                $relation = $this->changeArrayFormat($relationships['relation']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $relation = $this->changeArrayFormat($relationships['relation']['data']);
            }

            $relationTranslator = $this->getTranslatorFactory()->getTranslator($attributes['relationCategory']);
            $evaluation->setRelation($relationTranslator->arrayToObject($relation));
        }

        return $evaluation;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($evaluation, array $keys = array())
    {
        if (!$evaluation instanceof Evaluation) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'reason',
                'content',
                'picture',
                'relationCategory',
                'commodityStarReview',
                'enterpriseStarReview',
                'order',
                'member'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'evaluations',
                'id'=>$evaluation->getId()
            )
        );

        $attributes = array();

        if (in_array('reason', $keys)) {
            $attributes['reason'] = $evaluation->getReason();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $evaluation->getContent();
        }
        if (in_array('picture', $keys)) {
            $attributes['picture'] = $evaluation->getPicture();
        }
        if (in_array('relationCategory', $keys)) {
            $attributes['relationCategory'] = $evaluation->getRelationCategory();
        }
        if (in_array('commodityStarReview', $keys)) {
            $attributes['commodityStarReview'] = $evaluation->getEvaluationScore()->getCommodityStarReview();
        }
        if (in_array('enterpriseStarReview', $keys)) {
            $attributes['enterpriseStarReview'] = $evaluation->getEvaluationScore()->getEnterpriseStarReview();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('order', $keys)) {
            $expression['data']['relationships']['order']['data'] = array(
                array(
                    'type'=>'serviceOrders',
                    'id'=>$evaluation->getOrder()->getId()
                )
            );
        }
        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type'=>'members',
                    'id'=>$evaluation->getMember()->getId()
                )
            );
        }

        return $expression;
    }
}
