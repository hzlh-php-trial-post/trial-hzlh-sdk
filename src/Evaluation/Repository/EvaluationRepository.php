<?php
namespace Trial\Evaluation\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Evaluation\Adapter\Evaluation\IEvaluationAdapter;
use Trial\Evaluation\Adapter\Evaluation\EvaluationMockAdapter;
use Trial\Evaluation\Adapter\Evaluation\EvaluationRestfulAdapter;
use Trial\Evaluation\Model\Evaluation;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class EvaluationRepository extends Repository implements IEvaluationAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        EnableAbleRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'EVALUATION_LIST';
    const FETCH_ONE_MODEL_UN = 'EVALUATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new EvaluationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IEvaluationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IEvaluationAdapter
    {
        return new EvaluationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function maskedEvaluation(Evaluation $evaluation) : bool
    {
        return $this->getAdapter()->maskedEvaluation($evaluation);
    }

    public function cancelMaskedEvaluation(Evaluation $evaluation) : bool
    {
        return $this->getAdapter()->cancelMaskedEvaluation($evaluation);
    }
}
