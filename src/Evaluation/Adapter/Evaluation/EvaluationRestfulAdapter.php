<?php
namespace Trial\Evaluation\Adapter\Evaluation;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Evaluation\Model\Evaluation;
use Trial\Evaluation\Model\NullEvaluation;
use Trial\Evaluation\Translator\EvaluationRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class EvaluationRestfulAdapter extends GuzzleAdapter implements IEvaluationAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'EVALUATION_LIST'=>[
            'fields'=>[],
            'include'=>'member,order,evaluationScore,relation,enterprise'
        ],
        'EVALUATION_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'member,order,evaluationScore,relation,enterprise'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new EvaluationRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'evaluations';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullEvaluation::getInstance());
    }

    public function maskedEvaluation(Evaluation $evaluation) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $evaluation,
            array(
                'reason'
            )
        );

        $this->patch(
            $this->getResource().'/'.$evaluation->getId().'/masked',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($evaluation);
            return true;
        }

        return false;
    }

    public function cancelMaskedEvaluation(Evaluation $evaluation) : bool
    {
        $this->patch(
            $this->getResource().'/'.$evaluation->getId().'/cancelMasked'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($evaluation);
            return true;
        }
        return false;
    }

    protected function addAction(Evaluation $evaluation) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $evaluation,
            array(
                'content',
                'picture',
                'relationCategory',
                'commodityStarReview',
                'enterpriseStarReview',
                'order',
                'member'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($evaluation);
            return true;
        }

        return false;
    }

    protected function editAction(Evaluation $evaluation) : bool
    {
        unset($evaluation);
        return false;
    }
}
