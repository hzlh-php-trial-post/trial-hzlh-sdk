<?php
namespace Trial\Evaluation\Adapter\Evaluation;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\EnableAbleMockAdapterTrait;

use Trial\Evaluation\Model\Evaluation;
use Trial\Evaluation\Utils\MockFactory;

class EvaluationMockAdapter implements IEvaluationAdapter
{
    use OperatAbleMockAdapterTrait, EnableAbleMockAdapterTrait;

    public function maskedEvaluation(Evaluation $evaluation) : bool
    {
        unset($evaluation);
        return true;
    }
    
    public function cancelMaskedEvaluation(Evaluation $evaluation)
    {
        unset($evaluation);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generateEvaluationObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $evaluationList = array();

        foreach ($ids as $id) {
            $evaluationList[] = MockFactory::generateEvaluationObject($id);
        }

        return $evaluationList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateEvaluationObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateEvaluationObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
