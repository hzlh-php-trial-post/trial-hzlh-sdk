<?php
namespace Trial\Evaluation\Adapter\Evaluation;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Evaluation\Model\Evaluation;

use Marmot\Interfaces\IAsyncAdapter;

interface IEvaluationAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter
{
    public function maskedEvaluation(Evaluation $evaluation);
    
    public function cancelMaskedEvaluation(Evaluation $evaluation);
}
