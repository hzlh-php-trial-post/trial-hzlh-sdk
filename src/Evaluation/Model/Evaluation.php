<?php
namespace Trial\Evaluation\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Evaluation\Repository\EvaluationRepository;

use Trial\Member\Model\Member;
use Trial\Order\ServiceOrder\Model\ServiceOrder;
use Trial\Service\Model\NullService;
use Trial\Enterprise\Model\Enterprise;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class Evaluation implements IOperatAble, IObject
{
    use OperatAbleTrait,Object;

    const RELATION_CATEGORY = [
        'NULL' => 0, //未知
        'SERVICE' => 1 //服务评价
    ];

    const EVALUATION_TYPE = [
        'NULL' => 0,
        'NEGATIVE' => 1, //差评
        'INTERMEDIATE' => 2, //中评
        'FAVORABLE' => 3 //好评
    ];

    const IS_AUTO = [
        'MEMBER' => 0, //用户评价
        'AUTO' => 1 //自动评价
    ];

    const STATUS = [
        'NORMAL' => 0, //正常
        'MASKED' => -2 //已屏蔽
    ];

    const IS_REPLY = [
        'NO_REPLY' => 0, //未回复
        'REPLIED' => 2 //已回复
    ];

    private $id;

    private $snapshotId;

    private $relation;

    private $relationCategory;

    private $order;

    private $member;

    private $commodityName;

    private $evaluationType;

    private $content;

    private $picture;

    private $isAuto;

    private $likesCount;

    private $reason;

    private $evaluationScore;

    private $isReply;

    private $repository;

    private $enterprise;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->snapshotId = 0;
        $this->relation = new NullService();
        $this->relationCategory = self::RELATION_CATEGORY['SERVICE'];
        $this->order = new ServiceOrder();
        $this->member = new Member();
        $this->commodityName = '';
        $this->evaluationType = self::EVALUATION_TYPE['NULL'];
        $this->content = '';
        $this->picture = array();
        $this->isAuto = self::IS_AUTO['MEMBER'];
        $this->likesCount = 0;
        $this->reason = '';
        $this->evaluationScore = new EvaluationScore();
        $this->isReply = self::IS_REPLY['NO_REPLY'];
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new EvaluationRepository();
        $this->enterprise = new Enterprise();
    }


    public function __destruct()
    {
        unset($this->id);
        unset($this->snapshotId);
        unset($this->relation);
        unset($this->relationCategory);
        unset($this->order);
        unset($this->member);
        unset($this->commodityName);
        unset($this->evaluationType);
        unset($this->content);
        unset($this->picture);
        unset($this->isAuto);
        unset($this->likesCount);
        unset($this->reason);
        unset($this->evaluationScore);
        unset($this->isReply);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
        unset($this->enterprise);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSnapshotId(int $snapshotId) : void
    {
        $this->snapshotId = $snapshotId;
    }

    public function getSnapshotId() : int
    {
        return $this->snapshotId;
    }

    public function setRelation(IEvaluation $relation) : void
    {
        $this->relation = $relation;
    }

    public function getRelation() : IEvaluation
    {
        return $this->relation;
    }

    public function setRelationCategory(int $relationCategory) : void
    {
        $this->relationCategory = in_array($relationCategory, self::RELATION_CATEGORY) ?
                        $relationCategory :
                        self::RELATION_CATEGORY['SERVICE'];
    }

    public function getRelationCategory() : int
    {
        return $this->relationCategory;
    }

    public function setOrder(IObject $order) : void
    {
        $this->order = $order;
    }

    public function getOrder() : IObject
    {
        return $this->order;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setCommodityName(string $commodityName) : void
    {
        $this->commodityName = $commodityName;
    }

    public function getCommodityName() : string
    {
        return $this->commodityName;
    }

    public function setEvaluationType(int $evaluationType) : void
    {
        $this->evaluationType = $evaluationType;
    }

    public function getEvaluationType() : int
    {
        return $this->evaluationType;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setPicture(array $picture) : void
    {
        $this->picture = $picture;
    }

    public function getPicture() : array
    {
        return $this->picture;
    }

    public function setIsAuto(int $isAuto) : void
    {
        $this->isAuto = in_array($isAuto, self::IS_AUTO) ? $isAuto : self::IS_AUTO['MEMBER'];
    }

    public function getIsAuto() : int
    {
        return $this->isAuto;
    }

    public function setLikesCount(int $likesCount) : void
    {
        $this->likesCount = $likesCount;
    }

    public function getLikesCount() : int
    {
        return $this->likesCount;
    }

    public function setReason(string $reason) : void
    {
        $this->reason = $reason;
    }

    public function getReason() : string
    {
        return $this->reason;
    }

    public function setEvaluationScore(EvaluationScore $evaluationScore) : void
    {
        $this->evaluationScore = $evaluationScore;
    }

    public function getEvaluationScore() : EvaluationScore
    {
        return $this->evaluationScore;
    }

    public function setIsReply(int $isReply) : void
    {
        $this->isReply = in_array($isReply, self::IS_REPLY) ? $isReply : self::IS_REPLY['NO_REPLY'];
    }

    public function getIsReply() : int
    {
        return $this->isReply;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }

    protected function getRepository() : EvaluationRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function maskedEvaluation() : bool
    {
        return $this->getRepository()->maskedEvaluation($this);
    }

    public function cancelMaskedEvaluation() : bool
    {
        return $this->getRepository()->cancelMaskedEvaluation($this);
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }
}
