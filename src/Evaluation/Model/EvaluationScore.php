<?php
namespace Trial\Evaluation\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Enterprise\Model\Enterprise;
use Trial\Service\Model\NullService;

class EvaluationScore implements IObject
{
    use Object;

    const RELATION_CATEGORY = [
        'SERVICE' => 1 //服务评价
    ];

    const COMMODITY_SCORE_DIMENSION = [
        'DESCRIPTION' => 1 //与描述相符
    ];

    const ENTERPRISE_SCORE_DIMENSION = [
        'PROFESSIONAL' => 1, //专业度评价
        'SERVICE_ATTITUDE' => 2 //服务态度评价
    ];

    const SCORE = [
        Evaluation::EVALUATION_TYPE['NEGATIVE'] => 0, //差评0分
        Evaluation::EVALUATION_TYPE['INTERMEDIATE'] => 2.5, //中评2.5分
        Evaluation::EVALUATION_TYPE['FAVORABLE'] => 5 //好评5分
    ];

    private $id;
    
    private $relation;
    
    private $enterprise;
    
    private $relationCategory;
    
    private $commodityStarReview;
    
    private $enterpriseStarReview;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->relation = new NullService();
        $this->enterprise = new Enterprise();
        $this->relationCategory = self::RELATION_CATEGORY['SERVICE'];
        $this->commodityStarReview = array();
        $this->enterpriseStarReview = array();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->relation);
        unset($this->enterprise);
        unset($this->relationCategory);
        unset($this->commodityStarReview);
        unset($this->enterpriseStarReview);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setRelation(IEvaluation $relation) : void
    {
        $this->relation = $relation;
    }

    public function getRelation() : IEvaluation
    {
        return $this->relation;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setRelationCategory(int $relationCategory) : void
    {
        $this->relationCategory = in_array($relationCategory, self::RELATION_CATEGORY) ?
                        $relationCategory :
                        self::RELATION_CATEGORY['SERVICE'];
    }

    public function getRelationCategory() : int
    {
        return $this->relationCategory;
    }

    public function setCommodityStarReview(array $commodityStarReview) : void
    {
        $this->commodityStarReview = $commodityStarReview;
    }

    public function getCommodityStarReview() : array
    {
        return $this->commodityStarReview;
    }

    public function setEnterpriseStarReview(array $enterpriseStarReview) : void
    {
        $this->enterpriseStarReview = $enterpriseStarReview;
    }

    public function getEnterpriseStarReview() : array
    {
        return $this->enterpriseStarReview;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
