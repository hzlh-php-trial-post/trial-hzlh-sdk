<?php
namespace Trial\Member\Adapter\Member;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Member\Model\Member;

use Marmot\Interfaces\IAsyncAdapter;

interface IMemberAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter
{
    public function signIn(Member $member);

    public function signUp(Member $member);

    public function updatePassword(Member $member);

    public function resetPassword(Member $member);

    public function updateCellphone(Member $member);
    
    public function editPushIdentity(Member $member);
}
