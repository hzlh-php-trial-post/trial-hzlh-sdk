<?php
namespace Trial\ServiceRequirement\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\ApplyAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ModifyStatusAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;

use Trial\ServiceRequirement\Adapter\ServiceRequirement\IServiceRequirementAdapter;
use Trial\ServiceRequirement\Adapter\ServiceRequirement\ServiceRequirementMockAdapter;
use Trial\ServiceRequirement\Adapter\ServiceRequirement\ServiceRequirementRestfulAdapter;

class ServiceRequirementRepository extends Repository implements IServiceRequirementAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_SERVICE_REQUIREMENT_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_SERVICE_REQUIREMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'SERVICE_REQUIREMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ServiceRequirementRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IServiceRequirementAdapter
    {
        return new ServiceRequirementMockAdapter();
    }

    public function getActualAdapter(): IServiceRequirementAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
