<?php
namespace Trial\ServiceRequirement\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullApplyAbleTrait;
use Trial\Common\Model\NullModifyStatusAbleTrait;
use Trial\Common\Model\NullOperatAbleTrait;

class NullServiceRequirement extends ServiceRequirement implements INull
{
    use NullApplyAbleTrait, NullModifyStatusAbleTrait, NullOperatAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
