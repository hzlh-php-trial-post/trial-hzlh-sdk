<?php
namespace Trial\ServiceRequirement\Adapter\ServiceRequirement;

use Marmot\Interfaces\IAsyncAdapter;
use Trial\Common\Adapter\IFetchAbleAdapter;

interface IServiceRequirementAdapter extends IAsyncAdapter, IFetchAbleAdapter, IServiceRequirementOperatAdapter
{
}
