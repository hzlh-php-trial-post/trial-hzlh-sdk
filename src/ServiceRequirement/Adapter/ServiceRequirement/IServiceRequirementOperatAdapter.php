<?php
namespace Trial\ServiceRequirement\Adapter\ServiceRequirement;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IModifyStatusAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IServiceRequirementOperatAdapter extends IApplyAbleAdapter, IModifyStatusAbleAdapter, IOperatAbleAdapter
{
}
