<?php
namespace Trial\ServiceRequirement\Adapter\ServiceRequirement;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\ApplyAbleMockAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleMockAdapterTrait;

use Trial\ServiceRequirement\Model\ServiceRequirement;
use Trial\ServiceRequirement\Utils\MockFactory;

class ServiceRequirementMockAdapter implements IServiceRequirementAdapter
{
    use OperatAbleMockAdapterTrait, ApplyAbleMockAdapterTrait, ModifyStatusAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateRequirementObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $requirementList = array();

        foreach ($ids as $id) {
            $requirementList[] = MockFactory::generateRequirementObject($id);
        }

        return $requirementList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateRequirementObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $requirementList = array();

        foreach ($ids as $id) {
            $requirementList[] = MockFactory::generateRequirementObject($id);
        }

        return $requirementList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
