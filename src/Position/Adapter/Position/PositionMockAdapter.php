<?php
namespace Trial\Position\Adapter\Position;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\EnableAbleMockAdapterTrait;

use Trial\Position\Model\Position;
use Trial\Position\Utils\MockFactory;

class PositionMockAdapter implements IPositionAdapter
{
    use OperatAbleMockAdapterTrait, EnableAbleMockAdapterTrait;

    public function permission(Position $position) : bool
    {
        unset($position);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generatePositionObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $positionList = array();

        foreach ($ids as $id) {
            $positionList[] = MockFactory::generatePositionObject($id);
        }

        return $positionList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generatePositionObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generatePositionObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
