<?php
namespace Trial\Position\Adapter\Position;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

use Trial\Position\Model\Position;

interface IPositionAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter, IAsyncAdapter //phpcs:ignore
{
    public function permission(Position $permission) : bool;
}
