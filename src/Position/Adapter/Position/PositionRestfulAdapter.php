<?php
namespace Trial\Position\Adapter\Position;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Position\Model\Position;
use Trial\Position\Model\NullPosition;
use Trial\Position\Translator\PositionRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class PositionRestfulAdapter extends GuzzleAdapter implements IPositionAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'POSITION_LIST'=>[
                'fields'=>[],
                'include'=> 'enterprise'
            ],
            'POSITION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'enterprise'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PositionRestfulTranslator();
        $this->resource = 'positions';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => POSITION_NAME_EXIST
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPosition::getInstance());
    }

    protected function addAction(Position $position) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $position,
            array(
                'name',
                'status',
                'permission',
                'enterprise'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($position);
            return true;
        }

        return false;
    }
        
    protected function editAction(Position $position) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $position,
            array(
                'name',
                'status',
                'permission'
            )
        );

        $this->patch(
            $this->getResource().'/'.$position->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($position);
            return true;
        }

        return false;
    }

    public function permission(Position $position) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $position,
            array('permission')
        );

        $this->patch(
            $this->getResource().'/'.$position->getId().'/permission',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($position);
            return true;
        }

        return false;
    }
}
