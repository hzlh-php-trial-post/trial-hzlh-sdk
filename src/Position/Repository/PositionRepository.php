<?php
namespace Trial\Position\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\Position\Model\Position;
use Trial\Position\Adapter\Position\IPositionAdapter;
use Trial\Position\Adapter\Position\PositionMockAdapter;
use Trial\Position\Adapter\Position\PositionRestfulAdapter;

class PositionRepository extends Repository implements IPositionAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'POSITION_LIST';
    const FETCH_ONE_MODEL_UN = 'POSITION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PositionRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IPositionAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IPositionAdapter
    {
        return new PositionMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function permission(Position $position) : bool
    {
        return $this->getAdapter()->permission($position);
    }
}
