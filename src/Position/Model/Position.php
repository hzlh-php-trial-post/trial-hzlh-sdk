<?php
namespace Trial\Position\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IEnableAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\EnableAbleTrait;
use Trial\Common\Adapter\IEnableAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Enterprise\Model\Enterprise;

use Trial\Position\Repository\PositionRepository;

class Position implements IObject, IOperatAble, IEnableAble
{
    use Object, OperatAbleTrait, EnableAbleTrait;

    private $id;

    private $name;

    private $permission;

    private $enterprise;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->permission = array();
        $this->enterprise = new Enterprise();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new PositionRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->permission);
        unset($this->enterprise);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }


    public function setPermission(array $permission) : void
    {
        $this->permission = $permission;
    }

    public function getPermission() : array
    {
        return $this->permission;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    protected function getRepository() : PositionRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    public function permission() : bool
    {
        return $this->getRepository()->permission($this);
    }
}
