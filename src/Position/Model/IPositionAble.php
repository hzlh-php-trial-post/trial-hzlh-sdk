<?php
namespace Trial\Position\Model;

interface IPositionAble
{
    const PARENT_MENU_POSITION = array(
        'IDENTITY_AUTHENTICATION' => 1, //身份认证
        'IDENTITY_AUTHENTICATION_ADMIN' => 2,//身份认证管理
        'SHOP_ADMIN' => 5, //店铺管理
        'SERVICE_MANAGEMENT' => 9,//服务管理
        'ORDER_ADMIN' => 11,//订单管理
        'FINANCIAL_PRODUCTS_ADMIN' => 17,//金融产品管理
        'FINANCING_LIBRARY' => 21,//融资需求库
        'STAFF_ADMIN' => 23,//员工管理
        'MARKETING_MANAGEMENT' => 27,//营销管理
        'ACCOUNTING_TOOLS' => 30, //记账工具
        'CONTRACT' => 31, //电纸合同
    );

    const PARENT_MENU_POSITION_ZN = [
        self::PARENT_MENU_POSITION['IDENTITY_AUTHENTICATION'] => '身份认证',
        self::PARENT_MENU_POSITION['IDENTITY_AUTHENTICATION_ADMIN'] => '身份认证管理',
        self::PARENT_MENU_POSITION['SHOP_ADMIN'] => '店铺管理',
        self::PARENT_MENU_POSITION['SERVICE_MANAGEMENT'] => '服务管理',
        self::PARENT_MENU_POSITION['ORDER_ADMIN'] => '订单管理',
        self::PARENT_MENU_POSITION['FINANCIAL_PRODUCTS_ADMIN'] => '金融产品管理',
        self::PARENT_MENU_POSITION['FINANCING_LIBRARY'] => '融资需求库',
        self::PARENT_MENU_POSITION['STAFF_ADMIN'] => '员工管理',
        self::PARENT_MENU_POSITION['MARKETING_MANAGEMENT'] => '营销管理',
        self::PARENT_MENU_POSITION['ACCOUNTING_TOOLS'] => '记账工具',
        self::PARENT_MENU_POSITION['CONTRACT'] => '电纸合同',
    ];

    const MENU_POSITION = array(
        'CERTIFICATION_SERVICE_PROVIDER' => 3, //认证服务商
        'CERTIFIED_FINANCIAL_INSTITUTION' => 4, //认证金融机构
        'MY_SHOP' => 6, //我的店铺
        'SHOP_INFORMATION_SETTING' => 7, //店铺信息设置
        'SHOP_DECORATION' => 8,//店铺装修
        'SERVICE_LIST' => 10,//服务列表
        'ORDER_LIST' => 12,//订单列表
        'REFUND_MANAGEMENT' => 13,//退款管理
        'EVALUATION_MANAGEMENT' => 14,//评价管理
        'INVOICE_MANAGEMENT' => 15,//发票管理
        'CONTRACT_MANAGEMENT' => 16,//电子合同
        'PRODUCT_LIST' => 18,//产品列表
        'TEMPLATE_LIBRARY_ADMIN' => 19,//模板库管理
        'APPLY_ADMIN' => 20,//申请管理
        'FINANCING_LIBRARY_LIST' => 22,//需求库列表
        'STAFF_LIST' => 24,//员工列表
        'POSITION_ADMIN' => 25,//岗位管理
        'LOG_ADMIN' => 26,//查询日志
        'MEMBER' => 28,//会员中心
        'COUPON_MARKETING'=> 29, //优惠券营销
        'CONTRACT_TEMPLATE_LIBRARY' => 32, //合同模版库
        'CONTRACT_LIBRARY'=> 33, //合同管理
    );

    const  MENU_POSITION_ZN = [
        self::MENU_POSITION['CERTIFICATION_SERVICE_PROVIDER'] => '认证服务商',
        self::MENU_POSITION['CERTIFIED_FINANCIAL_INSTITUTION'] => '认证金融机构',
        self::MENU_POSITION['MY_SHOP'] => '我的店铺',
        self::MENU_POSITION['SHOP_INFORMATION_SETTING'] => '店铺信息设置',
        self::MENU_POSITION['SHOP_DECORATION'] => '店铺装修',
        self::MENU_POSITION['SERVICE_LIST'] => '服务列表',
        self::MENU_POSITION['ORDER_LIST'] => '订单列表',
        self::MENU_POSITION['REFUND_MANAGEMENT'] => '退款管理',
        self::MENU_POSITION['EVALUATION_MANAGEMENT'] => '评价管理',
        self::MENU_POSITION['INVOICE_MANAGEMENT'] => '发票管理',
        self::MENU_POSITION['CONTRACT_MANAGEMENT'] => '电子合同',
        self::MENU_POSITION['PRODUCT_LIST'] => '产品列表',
        self::MENU_POSITION['TEMPLATE_LIBRARY_ADMIN'] => '模板库管理',
        self::MENU_POSITION['APPLY_ADMIN'] => '申请管理',
        self::MENU_POSITION['FINANCING_LIBRARY_LIST'] => '需求库列表',
        self::MENU_POSITION['STAFF_LIST'] => '员工列表',
        self::MENU_POSITION['POSITION_ADMIN'] => '岗位管理',
        self::MENU_POSITION['LOG_ADMIN'] => '查询日志',
        self::MENU_POSITION['MEMBER'] => '会员中心',
        self::MENU_POSITION['COUPON_MARKETING'] => '优惠券营销',
        self::MENU_POSITION['CONTRACT_TEMPLATE_LIBRARY'] => '合同模版库',
        self::MENU_POSITION['CONTRACT_LIBRARY'] => '合同管理',
    ];

    const MENU_MAP = [
        self::MENU_POSITION['CERTIFICATION_SERVICE_PROVIDER'] => self::PARENT_MENU_POSITION['IDENTITY_AUTHENTICATION_ADMIN'],//phpcs:ignore
        self::MENU_POSITION['CERTIFIED_FINANCIAL_INSTITUTION'] => self::PARENT_MENU_POSITION['IDENTITY_AUTHENTICATION_ADMIN'],//phpcs:ignore
        self::MENU_POSITION['MY_SHOP'] => self::PARENT_MENU_POSITION['SHOP_ADMIN'],
        self::MENU_POSITION['SHOP_INFORMATION_SETTING'] => self::PARENT_MENU_POSITION['SHOP_ADMIN'],
        self::MENU_POSITION['SHOP_DECORATION'] => self::PARENT_MENU_POSITION['SHOP_ADMIN'],
        self::MENU_POSITION['SERVICE_LIST'] => self::PARENT_MENU_POSITION['SERVICE_MANAGEMENT'],
        self::MENU_POSITION['ORDER_LIST'] => self::PARENT_MENU_POSITION['ORDER_ADMIN'],
        self::MENU_POSITION['REFUND_MANAGEMENT'] => self::PARENT_MENU_POSITION['ORDER_ADMIN'],
        self::MENU_POSITION['EVALUATION_MANAGEMENT'] => self::PARENT_MENU_POSITION['ORDER_ADMIN'],
        self::MENU_POSITION['INVOICE_MANAGEMENT'] => self::PARENT_MENU_POSITION['ORDER_ADMIN'],
        self::MENU_POSITION['CONTRACT_MANAGEMENT'] => self::PARENT_MENU_POSITION['ORDER_ADMIN'],
        self::MENU_POSITION['PRODUCT_LIST'] => self::PARENT_MENU_POSITION['FINANCIAL_PRODUCTS_ADMIN'],
        self::MENU_POSITION['TEMPLATE_LIBRARY_ADMIN'] => self::PARENT_MENU_POSITION['FINANCIAL_PRODUCTS_ADMIN'],
        self::MENU_POSITION['APPLY_ADMIN'] => self::PARENT_MENU_POSITION['FINANCIAL_PRODUCTS_ADMIN'],
        self::MENU_POSITION['FINANCING_LIBRARY_LIST'] => self::PARENT_MENU_POSITION['FINANCING_LIBRARY'],
        self::MENU_POSITION['STAFF_LIST'] => self::PARENT_MENU_POSITION['STAFF_ADMIN'],
        self::MENU_POSITION['POSITION_ADMIN'] => self::PARENT_MENU_POSITION['STAFF_ADMIN'],
        self::MENU_POSITION['LOG_ADMIN'] => self::PARENT_MENU_POSITION['STAFF_ADMIN'],
        self::MENU_POSITION['MEMBER'] => self::PARENT_MENU_POSITION['MARKETING_MANAGEMENT'],
        self::MENU_POSITION['COUPON_MARKETING'] => self::PARENT_MENU_POSITION['MARKETING_MANAGEMENT'],
        self::MENU_POSITION['CONTRACT_TEMPLATE_LIBRARY'] => self::PARENT_MENU_POSITION['CONTRACT'],
        self::MENU_POSITION['CONTRACT_LIBRARY'] => self::PARENT_MENU_POSITION['CONTRACT'],
    ];

    const OPERATION = array(
        'OPERATION_SHOW' => '1',
        'OPERATION_ADD' => '2',
        'OPERATION_EDIT' => '3',
        'OPERATION_SHELF' => '4',
        'OPERATION_ENABLE' => '5',
        'OPERATION_REJECT' => '6',
        'OPERATION_TOP' => '7',
        'OPERATION_DELETE' => '8',
        'OPERATION_RESUBMIT' => '9',
        'OPERATION_REVOKE' => '10',
        'OPERATION_POLICY_VIDEO' => '11',
        'OPERATION_POLICY_INTERPRETATION' => '12',
        'OPERATION_RELATION_PRODUCT' => '13',
        'OPERATION_RELATION_SPECIAL' => '14',
        'OPERATION_QITA' => '15',
        'OPERATION_RESELECTION' => '16',
        'OPERATION_POLICY_VOIDE' => '17',
        'OPERATION_RECOMMEND' => '18',
        'OPERATION_TRANSFER_COMPLETED' => '19',
    );

    const OPERATION_CN = array(
        '1' => '查看',
        '2' => '添加',
        '3' => '编辑',
        '4' => '上架/下架',
        '5' => '启用/禁用',
        '6' => '审核通过/驳回',
        '7' => '置顶/取消置顶',
        '8' => '删除',
        '9' => '重新提交',//预留
        '10' => '撤销',//预留
        '11' => '政策视频',//预留
        '12' => '发布政策解读',
        '13' => '关联产品/取消关联',
        '14' => '推荐至专题',
        '15' => '其他',//预留
        '16' => '重新选择',
        '17' => '发布视频',
        '18' => '推荐/取消推荐',
        '19' => '转账',
       
    );

    public function getPosition() : Position;
}
