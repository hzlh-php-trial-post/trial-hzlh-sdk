<?php
namespace Trial\Position\Translator;

use Trial\Position\Model\Position;
use Trial\Position\Model\NullPosition;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class PositionRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $position = null)
    {
        return $this->translateToObject($expression, $position);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $position = null)
    {
        if (empty($expression)) {
            return NullPosition::getInstance();
        }

        if ($position == null) {
            $position = new Position();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $position->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $position->setName($attributes['name']);
        }
        if (isset($attributes['permission'])) {
            $position->setPermission($attributes['permission']);
        }
        if (isset($attributes['createTime'])) {
            $position->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $position->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $position->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $position->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $position->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }

        return $position;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($position, array $keys = array())
    {
        $expression = array();

        if (!$position instanceof Position) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'permission',
                'status',
                'enterprise'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'positions'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $position->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $position->getName();
        }
        if (in_array('permission', $keys)) {
            $attributes['permission'] = $position->getPermission();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $position->getStatus();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprises',
                    'id' => $position->getEnterprise()->getId()
                )
             );
        }

        return $expression;
    }
}
