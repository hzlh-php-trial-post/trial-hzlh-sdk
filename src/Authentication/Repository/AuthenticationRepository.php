<?php
namespace Trial\Authentication\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;
use Trial\Authentication\Adapter\Authentication\AuthenticationMockAdapter;
use Trial\Authentication\Adapter\Authentication\AuthenticationRestfulAdapter;
use Trial\Authentication\Adapter\Authentication\IAuthenticationAdapter;
use Trial\Authentication\Model\Authentication;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ResubmitAbleRepositoryTrait;
use Trial\Common\Repository\ApplyAbleRepositoryTrait;

class AuthenticationRepository extends Repository implements IAuthenticationAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ErrorRepositoryTrait;

    const LIST_MODEL_UN = 'AUTHENTICATION_LIST';
    const FETCH_ONE_MODEL_UN = 'AUTHENTICATION_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new AuthenticationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IAuthenticationAdapter
    {
        return new AuthenticationMockAdapter();
    }

    public function getActualAdapter(): IAuthenticationAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function relation(Authentication $authentication) : bool
    {
        return $this->getAdapter()->relation($authentication);
    }

    public function cancelRelation(Authentication $authentication) : bool
    {
        return $this->getAdapter()->cancelRelation($authentication);
    }
}
