<?php
namespace Trial\Authentication\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Authentication\Repository\AuthenticationRepository;

use Trial\Enterprise\Model\Enterprise;
use Trial\ServiceCategory\Model\ServiceCategory;

use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;
use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IResubmitAble;
use Trial\Common\Model\IApplyAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\ResubmitAbleTrait;
use Trial\Common\Model\ApplyAbleTrait;
use Trial\Crew\Model\Crew;
use Trial\UserType\Model\UserType;

class Authentication implements IOperatAble, IObject, IResubmitAble, IApplyAble
{
    use Object, OperatAbleTrait, ResubmitAbleTrait, ApplyAbleTrait;

    private $id;

    private $number;

    private $enterpriseName;

    private $serviceCategory;

    private $qualificationImage;

    private $qualifications;

    private $enterprise;

    private $rejectReason;

    private $repository;

    private $strategyTypeId;

    private $crew;

    private $userType;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = '';
        $this->enterpriseName = '';
        $this->serviceCategory = new ServiceCategory();
        $this->qualifications = array();
        $this->enterprise = new Enterprise();
        $this->rejectReason = '';
        $this->applyStatus = IApplyAble::APPLY_STATUS['PENDING'];
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new AuthenticationRepository();
        $this->strategyTypeId = 0;
        $this->crew = new Crew();
        $this->userType = new UserType();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->enterpriseName);
        unset($this->serviceCategory);
        unset($this->qualifications);
        unset($this->enterprise);
        unset($this->rejectReason);
        unset($this->applyStatus);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
        unset($this->strategyTypeId);
        unset($this->crew);
        unset($this->userType);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setEnterpriseName(string $enterpriseName)
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName(): string
    {
        return $this->enterpriseName;
    }

    public function setServiceCategory(ServiceCategory $serviceCategory)
    {
        $this->serviceCategory = $serviceCategory;
    }

    public function getServiceCategory()
    {
        return $this->serviceCategory;
    }

    public function setQualificationImage(array $qualificationImage) : void
    {
        $this->qualificationImage = $qualificationImage;
    }

    public function getQualificationImage() : array
    {
        return $this->qualificationImage;
    }

    public function addQualification(Qualification $qualification): void
    {
        $this->qualifications[] = $qualification;
    }

    public function clearQualifications()
    {
        $this->qualifications = [];
    }

    public function getQualifications(): array
    {
        return $this->qualifications;
    }

    public function setEnterprise(Enterprise $enterprise): void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise(): Enterprise
    {
        return $this->enterprise;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function setStrategyTypeId($strategyTypeId) : void
    {
        $this->strategyTypeId = $strategyTypeId;
    }

    public function getStrategyTypeId()
    {
        return $this->strategyTypeId;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserType(UserType $userType) : void
    {
        $this->userType = $userType;
    }

    public function getUserType() : UserType
    {
        return $this->userType;
    }

    protected function getRepository(): AuthenticationRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIResubmitAbleAdapter(): IResubmitAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIApplyAbleAdapter(): IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    public function relation() : bool
    {
        $repository = $this->getRepository();

        return $repository->relation($this);
    }

    public function cancelRelation() : bool
    {
        $repository = $this->getRepository();

        return $repository->cancelRelation($this);
    }
}
