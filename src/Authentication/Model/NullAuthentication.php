<?php
namespace Trial\Authentication\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Trial\Common\Model\NullOperatAbleTrait;
use Trial\Common\Model\NullResubmitAbleTrait;
use Trial\Common\Model\NullApplyAbleTrait;

class NullAuthentication extends Authentication implements INull
{
    use NullOperatAbleTrait, NullResubmitAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist(): bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
