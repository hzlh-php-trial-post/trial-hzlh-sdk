<?php
namespace Trial\Authentication\Adapter\Authentication;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Trial\Authentication\Model\Authentication;
use Trial\Authentication\Model\NullAuthentication;
use Trial\Authentication\Translator\AuthenticationRestfulTranslator;

class AuthenticationRestfulAdapter extends GuzzleAdapter implements IAuthenticationAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    const SCENARIOS = [
            'AUTHENTICATION_LIST'=>[
                'fields'=>[],
                'include'=>'enterprise,serviceCategory,strategyType'
            ],
            'AUTHENTICATION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=>'enterprise,serviceCategory,strategyType'
            ]
        ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
         parent::__construct(
             $uri,
             $authKey
         );
        $this->translator = new AuthenticationRestfulTranslator();
        $this->resource = 'authentications';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullAuthentication::getInstance());
    }

    protected function addAction(Authentication $authentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $authentication,
            array(
                'qualifications',
                'enterprise'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($authentication);
            return true;
        }

        return false;
    }
    protected function resubmitAction(Authentication $authentication) : bool
    {
        return $this->editAction($authentication);
    }

    protected function editAction(Authentication $authentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $authentication,
            array(
                'qualificationImage'
            )
        );

        $this->patch(
            $this->getResource().'/'.$authentication->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($authentication);
            return true;
        }

        return false;
    }

    public function relation(Authentication $authentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $authentication,
            array(
                'strategyTypeId',
                'crew'
            )
        );

        $this->post(
            $this->getResource().'/'.$authentication->getId().'/relate',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($authentication);
            return true;
        }

        return false;
    }

    public function cancelRelation(Authentication $authentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $authentication,
            array(
                'crew'
            )
        );

        $this->patch(
            $this->getResource().'/'.$authentication->getId().'/cancelRelate',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($authentication);
            return true;
        }

        return false;
    }
}
