<?php
namespace Trial\Authentication\Adapter\Authentication;

use Marmot\Interfaces\IAsyncAdapter;
use Trial\Authentication\Model\Authentication;
use Trial\Common\Adapter\IFetchAbleAdapter;

interface IAuthenticationAdapter extends IFetchAbleAdapter, IAuthenticationOperatAdapter, IAsyncAdapter
{
    public function relation(Authentication $authentication) : bool;

    public function cancelRelation(Authentication $authentication) : bool;
}
