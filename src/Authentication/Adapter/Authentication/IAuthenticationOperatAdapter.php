<?php
namespace Trial\Authentication\Adapter\Authentication;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;

interface IAuthenticationOperatAdapter extends IOperatAbleAdapter, IResubmitAbleAdapter, IApplyAbleAdapter
{
}
