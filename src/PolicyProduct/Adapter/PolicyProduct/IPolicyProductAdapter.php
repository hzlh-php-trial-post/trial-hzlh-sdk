<?php
namespace Trial\PolicyProduct\Adapter\PolicyProduct;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IPolicyProductAdapter extends IFetchAbleAdapter, IAsyncAdapter
{

}
