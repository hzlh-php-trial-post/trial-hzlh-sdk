<?php
namespace Trial\PolicyProduct\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\PolicyProduct\Model\PolicyProduct;
use Trial\TradeRecord\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        PolicyProduct::CATEGORY['SERVICE'] =>
        'Trial\Service\Translator\ServiceRestfulTranslator',
        PolicyProduct::CATEGORY['FINANCE'] =>
        'Trial\LoanProduct\Translator\LoanProductRestfulTranslator'
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
