<?php
namespace Trial\CreditData\CreditScore\Model\CreditScoreGrade;

use Marmot\Common\Model\IObject;
use Marmot\Core;

use Marmot\Common\Model\Object;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\CreditData\CreditScore\Repository\CreditScoreGrade\CreditScoreGradeRepository;
use Trial\Crew\Model\Crew;

class CreditScoreGrade implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    const CREDIT_SCORE_GRADE_STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2,
        'DELETE' => -4
    );

    private $id;

    private $name;

    private $description;

    private $minScore;

    private $maxScore;

    private $crew;

    private $repository;

    private $creditScoreGradeData;

    public function __construct($id = 0)
    {
        $this->id   = !empty($id) ? $id : 0;
        $this->name = '';
        $this->description = '';
        $this->minScore = 0;
        $this->maxScore = 0;
        $this->status = self::CREDIT_SCORE_GRADE_STATUS['ENABLED'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->creditScoreGradeData = array();

        $this->crew = new Crew();
        $this->repository = new CreditScoreGradeRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->description);
        unset($this->minScore);
        unset($this->maxScore);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
        unset($this->crew);
        unset($this->creditScoreGradeData);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setMinScore(int $minScore) : void
    {
        $this->minScore = $minScore;
    }

    public function getMinScore() : int
    {
        return $this->minScore;
    }

    public function setMaxScore(int $maxScore) : void
    {
        $this->maxScore = $maxScore;
    }

    public function getMaxScore() : int
    {
        return $this->maxScore;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setCreditScoreGradeData(array $creditScoreGradeData) : void
    {
        $this->creditScoreGradeData = $creditScoreGradeData;
    }

    public function getCreditScoreGradeData() : array
    {
        return $this->creditScoreGradeData;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function addAction() : bool
    {
        return $this->getIOperatAbleAdapter()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getIOperatAbleAdapter()->edit($this);
    }

    public function setData(CreditScoreGrade $creditScoreData)
    {
        return $this->getIOperatAbleAdapter()->setCreditScoreGrade($creditScoreData);
    }
    public function deletes()
    {
        return $this->getIOperatAbleAdapter()->delete($this);
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->repository;
    }
}
