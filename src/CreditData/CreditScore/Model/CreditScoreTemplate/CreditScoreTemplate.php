<?php
namespace Trial\CreditData\CreditScore\Model\CreditScoreTemplate;

use Marmot\Common\Model\IObject;
use Marmot\Core;

use Marmot\Common\Model\Object;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\CreditData\CreditScore\Repository\CreditScoreTemplate\CreditScoreTemplateRepository;
use Trial\Crew\Model\Crew;

class CreditScoreTemplate implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    const CREDIT_SCORE_GRADE_STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2,
        'DELETE' => -4
    );

    private $id;

    private $foreignId;

    private $proportion;

    private $params;

    private $applicationScene;

    private $ids;

    private $crew;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->foreignId = 0;
        $this->proportion = array();
        $this->params = array();
        $this->applicationScene = array();
        $this->status = self::CREDIT_SCORE_GRADE_STATUS['ENABLED'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->ids = array();
        $this->crew = new Crew();
        $this->repository = new CreditScoreTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->foreignId);
        unset($this->proportion);
        unset($this->params);
        unset($this->applicationScene);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->ids);
        unset($this->crew);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setForeignId(int $foreignId) : void
    {
        $this->foreignId = $foreignId;
    }

    public function getForeignId() : int
    {
        return $this->foreignId;
    }

    public function setProportion(array $proportion) : void
    {
        $this->proportion = $proportion;
    }

    public function getProportion() : array
    {
        return $this->proportion;
    }

    public function setParams(array $params) : void
    {
        $this->params = $params;
    }

    public function getParams() : array
    {
        return $this->params;
    }

    public function setApplicationScene(array $applicationScene) : void
    {
        $this->applicationScene = $applicationScene;
    }

    public function getApplicationScene() : array
    {
        return $this->applicationScene;
    }

    public function setIds(array $ids) : void
    {
        $this->status = $ids;
    }

    public function getIds() : array
    {
        return $this->ids;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : CreditScoreTemplateRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function setApplicationSceneAction() : bool
    {
        return $this->getRepository()->setApplicationSceneAction($this);
    }

    protected function addAction() : bool
    {
        return $this->getIOperatAbleAdapter()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getIOperatAbleAdapter()->edit($this);
    }
}
