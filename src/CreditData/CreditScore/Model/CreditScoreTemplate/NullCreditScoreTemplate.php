<?php
namespace Trial\CreditData\CreditScore\Model\CreditScoreTemplate;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullOperatAbleTrait;
use Trial\Common\Model\NullEnableAbleTrait;

class NullCreditScoreTemplate extends CreditScoreTemplate implements INull
{
    use NullOperatAbleTrait, NullEnableAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
