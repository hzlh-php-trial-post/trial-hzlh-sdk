<?php

namespace Trial\CreditData\CreditScore\Model\CreditScoreTemplate;

class CreditScoreTemplateFactory
{
    const TEMPLATE_STATUS = array(
            'ENABLED'  => 0,
            'DISABLED' => -2,
            'DELETE'   => -4,
        );

    const RULE = array(
        'MENU' => 1,
        'NUMBER' => 2
    );

    const DATA_SCORE = array(
        'SCORE_ADD' => "add",
        'SCORE_SUB' => "sub"
    );
    // 避免和天网征信id重复 这里采用负数
    const DATA_INDEX = [
            'HIGH_PRAISE_RATE' => -1,
            'COLLECTION_COUNT' => -2,
        ];

    const DATA_INDEX_CN = [
            self::DATA_INDEX['HIGH_PRAISE_RATE'] => '好评率',
            self::DATA_INDEX['COLLECTION_COUNT'] => '收藏量',
        ];

    const DATA_SETTING = [
        'HIGH_PRAISE_RATE_ONE' => 101,
        'HIGH_PRAISE_RATE_TWO' => 102,
        'HIGH_PRAISE_RATE_THREE' => 103,
        'HIGH_PRAISE_RATE_FOUR' => 104,
        'HIGH_PRAISE_RATE_FIVE' => 105,

        'COLLECTION_COUNT_ONE' => 201,
        'COLLECTION_COUNT_TWO' => 202,
        'COLLECTION_COUNT_THREE' => 203,
        'COLLECTION_COUNT_FOUR' => 204,
    ];

    const DATA_SETTING_CN = [
        self::DATA_SETTING['HIGH_PRAISE_RATE_ONE'] => '好评率95%-100%',
        self::DATA_SETTING['HIGH_PRAISE_RATE_TWO'] => '好评率85%-94%',
        self::DATA_SETTING['HIGH_PRAISE_RATE_THREE'] => '好评率70%-84%',
        self::DATA_SETTING['HIGH_PRAISE_RATE_FOUR'] => '好评率60%-69%',
        self::DATA_SETTING['HIGH_PRAISE_RATE_FIVE'] => '好评率60%以下',

        self::DATA_SETTING['COLLECTION_COUNT_ONE'] => '收藏量20条以下',
        self::DATA_SETTING['COLLECTION_COUNT_TWO'] => '收藏量21-50条',
        self::DATA_SETTING['COLLECTION_COUNT_THREE'] => '收藏量51-100条',
        self::DATA_SETTING['COLLECTION_COUNT_FOUR'] => '收藏量100条'
    ];
    
    const DATA_SETTING_NUM = [
        self::DATA_SETTING['HIGH_PRAISE_RATE_ONE'] => ['min' => 95,'max' => 100],
        self::DATA_SETTING['HIGH_PRAISE_RATE_TWO'] => ['min' => 85,'max' => 94],
        self::DATA_SETTING['HIGH_PRAISE_RATE_THREE'] => ['min' => 70,'max' => 84],
        self::DATA_SETTING['HIGH_PRAISE_RATE_FOUR'] => ['min' => 60,'max' => 69],
        self::DATA_SETTING['HIGH_PRAISE_RATE_FIVE'] => ['min' => 0,'max' => 59],
        
        self::DATA_SETTING['COLLECTION_COUNT_ONE'] => ['min' => 0,'max' => 20],
        self::DATA_SETTING['COLLECTION_COUNT_TWO'] => ['min' => 21,'max' => 50],
        self::DATA_SETTING['COLLECTION_COUNT_THREE'] => ['min' => 51,'max' => 100],
        self::DATA_SETTING['COLLECTION_COUNT_FOUR'] => ['min' => 101,'max' => PHP_INT_MAX],
    ];

    const DATA_SETTING_MAP = [
            self::DATA_SETTING['HIGH_PRAISE_RATE_ONE'] => self::DATA_INDEX['HIGH_PRAISE_RATE'],
            self::DATA_SETTING['HIGH_PRAISE_RATE_TWO'] => self::DATA_INDEX['HIGH_PRAISE_RATE'],
            self::DATA_SETTING['HIGH_PRAISE_RATE_THREE'] => self::DATA_INDEX['HIGH_PRAISE_RATE'],
            self::DATA_SETTING['HIGH_PRAISE_RATE_FOUR'] => self::DATA_INDEX['HIGH_PRAISE_RATE'],
            self::DATA_SETTING['HIGH_PRAISE_RATE_FIVE'] => self::DATA_INDEX['HIGH_PRAISE_RATE'],

            self::DATA_SETTING['COLLECTION_COUNT_ONE'] => self::DATA_INDEX['COLLECTION_COUNT'],
            self::DATA_SETTING['COLLECTION_COUNT_TWO'] => self::DATA_INDEX['COLLECTION_COUNT'],
            self::DATA_SETTING['COLLECTION_COUNT_THREE'] => self::DATA_INDEX['COLLECTION_COUNT'],
            self::DATA_SETTING['COLLECTION_COUNT_FOUR'] => self::DATA_INDEX['COLLECTION_COUNT'],
        ];

    const APPLICATION_SCENE = [
        'SERVICE_MALL' => 1,//服务超市
        'FINANCE_MALL' => 2,//金融超市
        'PRODUCTION_AND_MARKETING_DOCKING' => 3,//产销对接
        'EQUITY_ROADSHOW' => 4,//股权路演
    ];

    const APPLICATION_SCENE_CN = [
        self::APPLICATION_SCENE['SERVICE_MALL'] => '服务超市',
        self::APPLICATION_SCENE['FINANCE_MALL'] => '金融超市',
        self::APPLICATION_SCENE['PRODUCTION_AND_MARKETING_DOCKING'] => '产销对接',
        self::APPLICATION_SCENE['EQUITY_ROADSHOW'] => '股权路演',
    ];

    // 父级分类
    const DATA_SOURCE_PARENT = [
        'HUIZHONGLIANHE' => 0, //汇众联合
        'SKY_NET' => 1
    ];

    const DATA_SOURCE_PARENT_CN = [
        self::DATA_SOURCE_PARENT['HUIZHONGLIANHE'] => '汇众联合',
        self::DATA_SOURCE_PARENT['SKY_NET'] => '天网征信'
    ];

    // 子级分类
    const DATA_SOURCE = [
        'HUIZHONGLIANHE' => 0, //汇众联合
    ];

    const DATA_SOURCE_CN = [
        self::DATA_SOURCE['HUIZHONGLIANHE'] => '汇众联合',
    ];

    /**
     * @SuppressWarnings(PHPMD)
     */
    public static function getParams($isEncode = false)
    {
        $mapResult = array();
        foreach (self::DATA_SETTING_MAP as $mapKey => $map) {
            $mapResult[$map][] = $mapKey;
        }

        $mapResultCn = array();

        foreach ($mapResult as $key => $value) {
            $id = $isEncode == false ? $key : marmot_encode($key);
            
            $mapResultCn[$key]['id'] = $id;
        
            $mapResultCn[$key]['value'] = $id;
            $mapResultCn[$key]['dataSource'] = marmot_encode(self::DATA_SOURCE['HUIZHONGLIANHE']);
            $mapResultCn[$key]['label'] = self::DATA_INDEX_CN[$key];

            $mapResultCn[$key]['rule'] = marmot_encode(self::RULE['MENU']);

            foreach ($value as $index) {
                $mapResultCn[$key]['options'][] = array(
                    'id' => $isEncode == false ? $index : marmot_encode($index),
                    'name' => self::DATA_SETTING_CN[$index]
                );
            }

            $mapResultCn = array_values($mapResultCn);
        }

        return $mapResultCn;
    }
}
