<?php
namespace Trial\CreditData\CreditScore\Repository\CreditScoreGrade;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\CreditData\CreditScore\Adapter\CreditScoreGrade\CreditScoreGradeRestfulAdapter;
use Trial\CreditData\CreditScore\Adapter\CreditScoreGrade\CreditScoreGradeMockAdapter;
use Trial\CreditData\CreditScore\Adapter\CreditScoreGrade\ICreditScoreGradeAdapter;
use Trial\CreditData\CreditScore\Model\CreditScoreGrade\CreditScoreGrade;

class CreditScoreGradeRepository extends Repository implements ICreditScoreGradeAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'BANNER_LIST';
    const FETCH_ONE_MODEL_UN = 'BANNER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CreditScoreGradeRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getAdapter() : ICreditScoreGradeAdapter
    {
        return $this->adapter;
    }

    protected function getActualAdapter()
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ICreditScoreGradeAdapter
    {
        return new CreditScoreGradeMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function delete(CreditScoreGrade $creditScoreGrade) : bool
    {
        return $this->getAdapter()->deletes($creditScoreGrade);
    }

    public function setCreditScoreGrade(CreditScoreGrade $creditScoreGrade) : bool
    {
        return $this->getAdapter()->setCreditScoreGrade($creditScoreGrade);
    }
}
