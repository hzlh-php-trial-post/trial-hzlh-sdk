<?php
namespace Trial\CreditData\CreditScore\Repository\CreditScoreTemplate;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\CreditData\CreditScore\Adapter\CreditScoreTemplate\CreditScoreTemplateRestfulAdapter;
use Trial\CreditData\CreditScore\Adapter\CreditScoreTemplate\CreditScoreTemplateMockAdapter;
use Trial\CreditData\CreditScore\Adapter\CreditScoreTemplate\ICreditScoreTemplateAdapter;
use Trial\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;

class CreditScoreTemplateRepository extends Repository implements ICreditScoreTemplateAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CREDIT_SCORE_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'CREDIT_SCORE_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CreditScoreTemplateRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    protected function getActualAdapter() : ICreditScoreTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ICreditScoreTemplateAdapter
    {
        return new CreditScoreTemplateMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function setApplicationSceneAction(CreditScoreTemplate $creditScoreTemplate) : bool
    {
        return $this->getAdapter()->setApplicationSceneAction($creditScoreTemplate);
    }
}
