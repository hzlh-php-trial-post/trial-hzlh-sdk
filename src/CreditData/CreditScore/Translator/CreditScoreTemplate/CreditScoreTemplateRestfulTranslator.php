<?php
namespace Trial\CreditData\CreditScore\Translator\CreditScoreTemplate;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;
use Trial\CreditData\CreditScore\Model\CreditScoreTemplate\NullCreditScoreTemplate;
use Trial\Crew\Translator\CrewRestfulTranslator;

class CreditScoreTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $creditScoreTemplate = null)
    {
        return $this->translateToObject($expression, $creditScoreTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $creditScoreTemplate = null)
    {
        if (empty($expression)) {
            return NullCreditScoreTemplate::getInstance();
        }

        if ($creditScoreTemplate == null) {
            $creditScoreTemplate = new CreditScoreTemplate();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $creditScoreTemplate->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['foreignId'])) {
            $creditScoreTemplate->setForeignId($attributes['foreignId']);
        }
        if (isset($attributes['proportion'])) {
            $creditScoreTemplate->setProportion($attributes['proportion']);
        }
        if (isset($attributes['params'])) {
            $creditScoreTemplate->setParams($attributes['params']);
        }
        if (isset($attributes['applicationScene'])) {
            $creditScoreTemplate->setApplicationScene($attributes['applicationScene']);
        }
        if (isset($attributes['createTime'])) {
            $creditScoreTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $creditScoreTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $creditScoreTemplate->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $creditScoreTemplate->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $creditScoreTemplate->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $creditScoreTemplate;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($creditScoreTemplate, array $keys = array())
    {
        $expression = array();

        if (!$creditScoreTemplate instanceof CreditScoreTemplate) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'foreignId',
                'proportion',
                'params',
                'applicationScene',
                'status',
                'createTime',
                'updateTime',
                'statusTime',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'creditScoreTemplates'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $creditScoreTemplate->getId();
        }

        $attributes = array();

        if (in_array('foreignId', $keys)) {
            $attributes['foreignId'] = $creditScoreTemplate->getForeignId();
        }
        if (in_array('proportion', $keys)) {
            $attributes['proportion'] = $creditScoreTemplate->getProportion();
        }
        if (in_array('params', $keys)) {
            $attributes['params'] = $creditScoreTemplate->getParams();
        }
        if (in_array('applicationScene', $keys)) {
            $attributes['applicationScene'] = $creditScoreTemplate->getApplicationScene();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $creditScoreTemplate->getStatus();
        }
 

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $crew = Core::$container->get('crew');
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' =>$crew->getId()
                )
            );
        }

        return $expression;
    }
}
