<?php
namespace Trial\CreditData\CreditScore\Translator\CreditScoreGrade;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\CreditData\CreditScore\Model\CreditScoreGrade\CreditScoreGrade;
use Trial\CreditData\CreditScore\Model\CreditScoreGrade\NullCreditScoreGrade;
use Trial\Crew\Translator\CrewRestfulTranslator;

class CreditScoreGradeRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $creditScoreGrade = null)
    {
        return $this->translateToObject($expression, $creditScoreGrade);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $creditScoreGrade = null)
    {
        if (empty($expression)) {
            return NullCreditScoreGrade::getInstance();
        }

        if ($creditScoreGrade == null) {
            $creditScoreGrade = new CreditScoreGrade();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $creditScoreGrade->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $creditScoreGrade->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $creditScoreGrade->setDescription($attributes['description']);
        }
        if (isset($attributes['minScore'])) {
            $creditScoreGrade->setMinScore($attributes['minScore']);
        }
        if (isset($attributes['maxScore'])) {
            $creditScoreGrade->setMaxScore($attributes['maxScore']);
        }
        if (isset($attributes['createTime'])) {
            $creditScoreGrade->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $creditScoreGrade->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $creditScoreGrade->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $creditScoreGrade->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $creditScoreGrade->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $creditScoreGrade;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($creditScoreGrade, array $keys = array())
    {
        $expression = array();

        if (!$creditScoreGrade instanceof CreditScoreGrade) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'minScore',
                'maxScore',
                'status',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'creditScoreGrades'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $creditScoreGrade->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $creditScoreGrade->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $creditScoreGrade->getDescription();
        }
        if (in_array('minScore', $keys)) {
            $attributes['minScore'] = $creditScoreGrade->getMinScore();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $creditScoreGrade->getStatus();
        }

        if (in_array('creditScoreGradeData', $keys)) {
            $attributes = $creditScoreGrade->getCreditScoreGradeData();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $crew = Core::$container->get('crew');
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' =>$crew->getId()
                )
            );
        }

        return $expression;
    }
}
