<?php
namespace Trial\CreditData\CreditScore\Adapter\CreditScoreTemplate;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface ICreditScoreTemplateAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
}
