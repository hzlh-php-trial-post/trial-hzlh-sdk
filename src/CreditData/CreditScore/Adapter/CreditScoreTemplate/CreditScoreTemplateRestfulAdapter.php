<?php
namespace Trial\CreditData\CreditScore\Adapter\CreditScoreTemplate;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\CreditData\CreditScore\Model\CreditScoreTemplate\NullCreditScoreTemplate;
use Trial\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\CreditData\CreditScore\Translator\CreditScoreTemplate\CreditScoreTemplateRestfulTranslator;

class CreditScoreTemplateRestfulAdapter extends GuzzleAdapter implements ICreditScoreTemplateAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'CREDIT_SCORE_TEMPLATE_LIST'=>[
            'fields'=>[
                'templates'=>'foreignId,applicationScene,updateTime'
            ],
            'include'=> 'crew'
        ],
        'CREDIT_SCORE_TEMPLATE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'crew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CreditScoreTemplateRestfulTranslator();
        $this->resource = 'creditScoreTemplates';
        $this->scenario = array();
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCreditScoreTemplate::getInstance());
    }

    protected function addAction(CreditScoreTemplate $creditScoreTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditScoreTemplate,
            array(
                'foreignId',
                'params',
                'proportion',
                'crew'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($creditScoreTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(CreditScoreTemplate $creditScoreTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditScoreTemplate,
            array(
                'params',
                'proportion',
                'crew'
            )
        );

        $this->patch(
            $this->getResource().'/'.$creditScoreTemplate->getId(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($creditScoreTemplate);
            return true;
        }
   
        return false;
    }

    public function setApplicationSceneAction(CreditScoreTemplate $creditScoreTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditScoreTemplate,
            array(
                'applicationScene',
                'crew'
            )
        );

        $this->patch(
            $this->getResource().'/'.$creditScoreTemplate->getId().'/setApplicationScene',
            $data
        );
       
        if ($this->isSuccess()) {
            $this->translateToObject(new CreditScoreTemplate());
            return true;
        }

        return false;
    }
}
