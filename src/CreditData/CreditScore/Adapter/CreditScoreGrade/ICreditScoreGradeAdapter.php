<?php
namespace Trial\CreditData\CreditScore\Adapter\CreditScoreGrade;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface ICreditScoreGradeAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
}
