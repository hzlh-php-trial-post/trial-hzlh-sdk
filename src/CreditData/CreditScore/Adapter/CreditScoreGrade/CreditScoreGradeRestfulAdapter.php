<?php
namespace Trial\CreditData\CreditScore\Adapter\CreditScoreGrade;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\CreditData\CreditScore\Model\CreditScoreGrade\NullCreditScoreGrade;
use Trial\CreditData\CreditScore\Model\CreditScoreGrade\CreditScoreGrade;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\CreditData\CreditScore\Translator\CreditScoreGrade\CreditScoreGradeRestfulTranslator;

class CreditScoreGradeRestfulAdapter extends GuzzleAdapter implements ICreditScoreGradeAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'CREDIT_SCORE_GRADE_LIST'=>[
            'fields'=>[
                'grades'=>'id,name,description,minScore,maxScore,credit_score_grade_id'
            ],
            'include'=> 'crew'
        ],
        'BANNER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'crew'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CreditScoreGradeRestfulTranslator();
        $this->resource = 'creditScoreGrades';
        $this->scenario = array();
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCreditScoreGrade::getInstance());
    }

    protected function addAction(CreditScoreGrade $creditScoreGrade) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditScoreGrade,
            array(
                'name',
                'description',
                'minScore',
                'maxScore',
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($data);
            return true;
        }

        return false;
    }

    protected function editAction(CreditScoreGrade $creditScoreGrade) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditScoreGrade,
            array(
                'name',
                'description',
                'minScore',
                'maxScore',
            )
        );

        $this->patch(
            $this->getResource().'/'.$creditScoreGrade->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($creditScoreGrade);
            return true;
        }

        return false;
    }

    public function setCreditScoreGrade(CreditScoreGrade $creditScoreGrade) : bool
    {
        return$this->setCreditScoreGradeAction($creditScoreGrade);
    }

    public function deletes(CreditScoreGrade $creditScoreGrade)
    {
        return $this->deleteAction($creditScoreGrade);
    }

    protected function setCreditScoreGradeAction(CreditScoreGrade $creditScoreGrade) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditScoreGrade,
            array(
                'creditScoreGradeData',
                'crew'
            )
        );

        $this->post(
            $this->getResource().'/set',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject(new CreditScoreGrade());
            return true;
        }

        return false;
    }

    protected function deleteAction(CreditScoreGrade $creditScoreGrade)
    {
        $this->patch(
            $this->getResource().'/'.$creditScoreGrade->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject(new CreditScoreGrade());
            return true;
        }

        return false;
    }
}
