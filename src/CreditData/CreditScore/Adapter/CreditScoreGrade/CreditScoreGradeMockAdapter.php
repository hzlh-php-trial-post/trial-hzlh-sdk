<?php
namespace Trial\CreditData\CreditScore\Adapter\CreditScoreGrade;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\CreditData\CreditScore\Model\CreditScoreGrade\CreditScoreGrade;

class CreditScoreGradeMockAdapter implements ICreditScoreGradeAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOneAsync(int $id)
    {
        unset($id);
        return new CreditScoreGrade();
    }

    public function fetchListAsync(array $ids)
    {
        unset($ids);
        return array();
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);
        return array();
    }

    public function fetchOne($id)
    {
        return new CreditScoreGrade();
    }

    public function fetchList(array $ids): array
    {
        return array();
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ): array {
        return array();
    }
}
