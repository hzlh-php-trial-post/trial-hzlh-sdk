<?php
namespace Trial\SystemNotice\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\SystemNotice\Model\SystemNotice;
use Trial\SystemNotice\Adapter\SystemNotice\ISystemNoticeAdapter;
use Trial\SystemNotice\Adapter\SystemNotice\SystemNoticeMockAdapter;
use Trial\SystemNotice\Adapter\SystemNotice\SystemNoticeRestfulAdapter;

class SystemNoticeRepository extends Repository implements ISystemNoticeAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'SYSTEM_NOTICE_LIST';
    const FETCH_ONE_MODEL_UN = 'SYSTEM_NOTICE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new SystemNoticeRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ISystemNoticeAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ISystemNoticeAdapter
    {
        return new SystemNoticeMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(SystemNotice $systemNotice) : bool
    {
        return $this->getAdapter()->deletes($systemNotice);
    }
}
