<?php
namespace Trial\SystemNotice\Translator;

use Trial\SystemNotice\Model\SystemNotice;
use Trial\SystemNotice\Model\NullSystemNotice;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Crew\Translator\CrewRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class SystemNoticeRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $systemNotice = null)
    {
        return $this->translateToObject($expression, $systemNotice);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $systemNotice = null)
    {
        if (empty($expression)) {
            return NullSystemNotice::getInstance();
        }

        if ($systemNotice == null) {
            $systemNotice = new SystemNotice();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $systemNotice->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $systemNotice->setTitle($attributes['title']);
        }
        if (isset($attributes['content'])) {
            $systemNotice->setContent($attributes['content']);
        }
        if (isset($attributes['sendObject'])) {
            $systemNotice->setSendObject($attributes['sendObject']);
        }
        if (isset($attributes['sendTime'])) {
            $systemNotice->setSendTime($attributes['sendTime']);
        }
        if (isset($attributes['expiresTime'])) {
            $systemNotice->setExpiresTime($attributes['expiresTime']);
        }
        if (isset($attributes['createTime'])) {
            $systemNotice->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $systemNotice->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $systemNotice->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $systemNotice->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $systemNotice->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $systemNotice;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($systemNotice, array $keys = array())
    {
        $expression = array();

        if (!$systemNotice instanceof SystemNotice) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'sendObject',
                'sendTime',
                'expiresTime',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'systemNotices'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $systemNotice->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $systemNotice->getTitle();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $systemNotice->getContent();
        }
        if (in_array('sendObject', $keys)) {
            $attributes['sendObject'] = $systemNotice->getSendObject();
        }
        if (in_array('sendTime', $keys)) {
            $attributes['sendTime'] = $systemNotice->getSendTime();
        }
        if (in_array('expiresTime', $keys)) {
            $attributes['expiresTime'] = $systemNotice->getExpiresTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $systemNotice->getCrew()->getId()
                )
             );
        }

        return $expression;
    }
}
