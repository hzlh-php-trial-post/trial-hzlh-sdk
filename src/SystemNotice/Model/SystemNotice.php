<?php
namespace Trial\SystemNotice\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Crew\Model\Crew;

use Trial\SystemNotice\Repository\SystemNoticeRepository;

class SystemNotice implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const SEND_OBJECT= array(
        'ALL' => 1, //全体用户
        'SELLER' => 2, //商家
        'BUYER' => 3 //买家
    );

    const STATUS = [
        'NORMAL' => 0 , //正常
        'DELETE' => -2 //已删除
    ];

    /**
     * @var int $id id
     */
    private $id;
    /**
     * @var string $title 触发模快
     */
    private $title;
    /**
     * @var string $sendObject 发送对象
     */
    private $sendObject;
    /**
     * @var string $content 模版内容
     */
    private $content;
    /**
     * @var int $sendTime 发送时间
     */
    private $sendTime;
    /**
     * @var int $expiresTime 过期时间
     */
    private $expiresTime;

    private $crew;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->sendObject = '';
        $this->content = '';
        $this->expiresTime = 0;
        $this->sendTime = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->status = 0;
        $this->createTime = '';
        $this->updateTime = '';
        $this->statusTime = 0;
        $this->repository = new SystemNoticeRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->sendObject);
        unset($this->content);
        unset($this->expiresTime);
        unset($this->sendTime);
        unset($this->crew);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setSendObject(string $sendObject) : void
    {
        $this->sendObject = $sendObject;
    }

    public function getSendObject() : string
    {
        return $this->sendObject;
    }

    public function setExpiresTime(int $expiresTime) : void
    {
        $this->expiresTime = $expiresTime;
    }

    public function getExpiresTime() : int
    {
        return $this->expiresTime;
    }
 
    public function setSendTime(int $sendTime) : void
    {
        $this->sendTime = $sendTime;
    }

    public function getSendTime() : int
    {
        return $this->sendTime;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : SystemNoticeRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }
}
