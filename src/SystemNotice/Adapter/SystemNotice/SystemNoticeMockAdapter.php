<?php
namespace Trial\SystemNotice\Adapter\SystemNotice;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;

use Trial\SystemNotice\Model\SystemNotice;
use Trial\SystemNotice\Utils\MockFactory;

class SystemNoticeMockAdapter implements ISystemNoticeAdapter
{
    use OperatAbleMockAdapterTrait;

    public function deletes(SystemNotice $systemNotice) : bool
    {
        unset($systemNotice);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generateSystemNoticeObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $systemNoticeList = array();

        foreach ($ids as $id) {
            $systemNoticeList[] = MockFactory::generateSystemNoticeObject($id);
        }

        return $systemNoticeList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateSystemNoticeObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateSystemNoticeObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
