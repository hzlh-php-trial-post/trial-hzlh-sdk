<?php
namespace Trial\SystemNotice\Adapter\SystemNotice;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\SystemNotice\Model\SystemNotice;
use Trial\SystemNotice\Model\NullSystemNotice;
use Trial\SystemNotice\Translator\SystemNoticeRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class SystemNoticeRestfulAdapter extends GuzzleAdapter implements ISystemNoticeAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'SYSTEM_NOTICE_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'SYSTEM_NOTICE_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new SystemNoticeRestfulTranslator();
        $this->resource = 'systemNotices';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullSystemNotice::getInstance());
    }

    protected function addAction(SystemNotice $systemNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $systemNotice,
            array(
                'title',
                'content',
                'sendObject',
                'sendTime',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemNotice);
            return true;
        }

        return false;
    }

    protected function editAction(SystemNotice $systemNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $systemNotice,
            array(
                'title',
                'content',
                'sendObject',
                'sendTime'
            )
        );
 
        $this->patch(
            $this->getResource().'/'.$systemNotice->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemNotice);
            return true;
        }

        return false;
    }

    public function deletes(SystemNotice $systemNotice) : bool
    {
        $this->patch(
            $this->getResource().'/'.$systemNotice->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($systemNotice);
            return true;
        }

        return false;
    }
}
