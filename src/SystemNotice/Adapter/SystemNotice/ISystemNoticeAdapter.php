<?php
namespace Trial\SystemNotice\Adapter\SystemNotice;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\SystemNotice\Model\SystemNotice;

interface ISystemNoticeAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
    public function deletes(SystemNotice $systemNotice) : bool;
}
