<?php
namespace Trial\MemberSystemNotice\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\MemberSystemNotice\Model\MemberSystemNotice;
use Trial\MemberSystemNotice\Adapter\MemberSystemNotice\IMemberSystemNoticeAdapter;
use Trial\MemberSystemNotice\Adapter\MemberSystemNotice\MemberSystemNoticeMockAdapter;
use Trial\MemberSystemNotice\Adapter\MemberSystemNotice\MemberSystemNoticeRestfulAdapter;

class MemberSystemNoticeRepository extends Repository implements IMemberSystemNoticeAdapter
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;
        
    private $adapter;

    const PORTAL_LIST_MODEL_UN = 'PORTAL_MEMBER_SYSTEM_NOTICE_LIST';
    const FETCH_ONE_MODEL_UN = 'MEMBER_SYSTEM_NOTICE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new MemberSystemNoticeRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IMemberSystemNoticeAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IMemberSystemNoticeAdapter
    {
        return new MemberSystemNoticeMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function allRead(MemberSystemNotice $memberSystemNotice) : bool
    {
        return $this->getAdapter()->allRead($memberSystemNotice);
    }

    public function read(MemberSystemNotice $memberSystemNotice) : bool
    {
        return $this->getAdapter()->read($memberSystemNotice);
    }

    public function deletes(MemberSystemNotice $memberSystemNotice) : bool
    {
        return $this->getAdapter()->deletes($memberSystemNotice);
    }
}
