<?php
namespace Trial\MemberSystemNotice\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Member\Translator\MemberRestfulTranslator;
use Trial\SystemNotice\Translator\SystemNoticeRestfulTranslator;

use Trial\MemberSystemNotice\Model\MemberSystemNotice;
use Trial\MemberSystemNotice\Model\NullMemberSystemNotice;

class MemberSystemNoticeRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }

    protected function getSystemNoticeRestfulTranslator() : SystemNoticeRestfulTranslator
    {
        return new SystemNoticeRestfulTranslator();
    }

    public function arrayToObject(array $expression, $memberSystemNotice = null)
    {
        return $this->translateToObject($expression, $memberSystemNotice);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $memberSystemNotice = null)
    {
        if (empty($expression)) {
            return NullMemberSystemNotice::getInstance();
        }

        if ($memberSystemNotice == null) {
            $memberSystemNotice = new MemberSystemNotice();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $memberSystemNotice->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['noticeStatus'])) {
            $memberSystemNotice->setNoticeStatus($attributes['noticeStatus']);
        }
        if (isset($attributes['status'])) {
            $memberSystemNotice->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $memberSystemNotice->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $memberSystemNotice->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $memberSystemNotice->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $memberSystemNotice->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        if (isset($relationships['systemNotice']['data'])) {
            $systemNotice = $this->changeArrayFormat($relationships['systemNotice']['data']);
            $memberSystemNotice->setSystemNotice(
                $this->getSystemNoticeRestfulTranslator()->arrayToObject($systemNotice)
            );
        }

        return $memberSystemNotice;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($memberSystemNotice, array $keys = array())
    {
        $expression = array();

        if (!$memberSystemNotice instanceof MemberSystemNotice) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'systemNoticeIds',
                'member'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'memberSystemNotices'
            )
        );

        $attributes = array();

        if (in_array('systemNoticeIds', $keys)) {
            $attributes['systemNoticeIds'] = $memberSystemNotice->getSystemNoticeIds();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $memberSystemNotice->getMember()->getId()
                )
            );
        }


        return $expression;
    }
}
