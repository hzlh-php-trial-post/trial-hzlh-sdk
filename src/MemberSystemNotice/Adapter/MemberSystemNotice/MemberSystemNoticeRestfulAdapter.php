<?php
namespace Trial\MemberSystemNotice\Adapter\MemberSystemNotice;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Trial\MemberSystemNotice\Model\MemberSystemNotice;
use Trial\MemberSystemNotice\Model\NullMemberSystemNotice;
use Trial\MemberSystemNotice\Translator\MemberSystemNoticeRestfulTranslator;

class MemberSystemNoticeRestfulAdapter extends GuzzleAdapter implements IMemberSystemNoticeAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_MEMBER_SYSTEM_NOTICE_LIST'=>[
                'fields'=>[],
                'include'=> 'member,systemNotice'
            ],
            'MEMBER_SYSTEM_NOTICE_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'member,systemNotice'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new MemberSystemNoticeRestfulTranslator();
        $this->resource = 'memberSystemNotices';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            121 => RESOURCE_NOTICE_STATUS_READ
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullMemberSystemNotice::getInstance());
    }

    public function allRead(MemberSystemNotice $memberSystemNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $memberSystemNotice,
            array(
                'member'
            )
        );

        $this->patch(
            $this->getResource().'/allRead',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($memberSystemNotice);
            return true;
        }

        return false;
    }

    public function read(MemberSystemNotice $memberSystemNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $memberSystemNotice,
            array(
                'systemNoticeIds',
                'member'
            )
        );

        $this->patch(
            $this->getResource().'/read',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($memberSystemNotice);
            return true;
        }

        return false;
    }

    public function deletes(MemberSystemNotice $memberSystemNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $memberSystemNotice,
            array(
                'systemNoticeIds',
                'member'
            )
        );
        
        $this->patch(
            $this->getResource().'/delete',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($memberSystemNotice);
            return true;
        }
        return false;
    }
}
