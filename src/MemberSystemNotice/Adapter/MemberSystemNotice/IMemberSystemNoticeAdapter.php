<?php
namespace Trial\MemberSystemNotice\Adapter\MemberSystemNotice;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Trial\MemberSystemNotice\Model\MemberSystemNotice;

interface IMemberSystemNoticeAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function allRead(MemberSystemNotice $memberSystemNotice) : bool;

    public function read(MemberSystemNotice $memberSystemNotice) : bool;

    public function deletes(MemberSystemNotice $memberSystemNotice) : bool;
}
