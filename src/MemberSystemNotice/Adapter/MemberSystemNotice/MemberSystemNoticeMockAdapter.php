<?php
namespace Trial\MemberSystemNotice\Adapter\MemberSystemNotice;

use Trial\MemberSystemNotice\Model\MemberSystemNotice;
use Trial\MemberSystemNotice\Utils\MockFactory;

class MemberSystemNoticeMockAdapter implements IMemberSystemNoticeAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateMemberSystemNoticeObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $memberSystemNoticeList = array();

        foreach ($ids as $id) {
            $memberSystemNoticeList[] = MockFactory::generateMemberSystemNoticeObject($id);
        }

        return $memberSystemNoticeList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateMemberSystemNoticeObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateMemberSystemNoticeObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function allRead(MemberSystemNotice $memberSystemNotice) : bool
    {
        unset($memberSystemNotice);

        return true;
    }

    public function read(MemberSystemNotice $memberSystemNotice) : bool
    {
        unset($memberSystemNotice);

        return true;
    }

    public function deletes(MemberSystemNotice $memberSystemNotice) : bool
    {
        unset($memberSystemNotice);

        return true;
    }
}
