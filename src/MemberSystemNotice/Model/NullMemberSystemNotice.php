<?php
namespace Trial\MemberSystemNotice\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullMemberSystemNotice extends MemberSystemNotice implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function allRead() : bool
    {
        return $this->resourceNotExist();
    }

    public function read() : bool
    {
        return $this->resourceNotExist();
    }

    public function deletes() : bool
    {
        return $this->resourceNotExist();
    }
}
