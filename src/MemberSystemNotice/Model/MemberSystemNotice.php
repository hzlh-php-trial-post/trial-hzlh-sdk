<?php
namespace Trial\MemberSystemNotice\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Member\Model\Member;
use Trial\SystemNotice\Model\SystemNotice;
use Trial\MemberSystemNotice\Repository\MemberSystemNoticeRepository;

class MemberSystemNotice implements IObject
{
    use Object;

    const NOTICE_STATUS = [
        'UNREAD' => 0 , //未读
        'READ' => 2 //已读
    ];

    const STATUS = [
        'NORMAL' => 0 , //正常
        'DELETE' => -2 //已删除
    ];

    private $id;

    private $systemNoticeIds;

    private $noticeStatus;

    private $member;

    private $systemNotice;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->systemNoticeIds = array();
        $this->noticeStatus = self::NOTICE_STATUS['READ'];
        $this->systemNotice = new SystemNotice();
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = '';
        $this->updateTime = '';
        $this->statusTime = 0;
        $this->repository = new MemberSystemNoticeRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->systemNoticeIds);
        unset($this->systemNotice);
        unset($this->noticeStatus);
        unset($this->member);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSystemNoticeIds(array $systemNoticeIds) : void
    {
        $this->systemNoticeIds = $systemNoticeIds;
    }

    public function getSystemNoticeIds() : array
    {
        return $this->systemNoticeIds;
    }

    public function setNoticeStatus(int $noticeStatus) : void
    {
        $this->noticeStatus = $noticeStatus;
    }

    public function getNoticeStatus() : int
    {
        return $this->noticeStatus;
    }

    public function setSystemNotice(SystemNotice $systemNotice) : void
    {
        $this->systemNotice = $systemNotice;
    }

    public function getSystemNotice() : SystemNotice
    {
        return $this->systemNotice;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : MemberSystemNoticeRepository
    {
        return $this->repository;
    }

    public function allRead() : bool
    {
        return $this->getRepository()->allRead($this);
    }

    public function read() : bool
    {
        return $this->getRepository()->read($this);
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }
}
