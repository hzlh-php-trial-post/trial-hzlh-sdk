<?php
namespace Trial\PaymentPassword\Adapter\PaymentPassword;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IPaymentPasswordAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter
{
}
