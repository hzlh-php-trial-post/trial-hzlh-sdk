<?php
namespace Trial\EducationalAuthentication\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\EducationalAuthentication\Model\EducationalAuthentication;
use Trial\EducationalAuthentication\Model\NullEducationalAuthentication;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;

class EducationalAuthenticationRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $educationalAuthentication = null)
    {
        return $this->translateToObject($expression, $educationalAuthentication);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $educationalAuthentication = null)
    {
        if (empty($expression)) {
            return NullEducationalAuthentication::getInstance();
        }

        if ($educationalAuthentication == null) {
            $educationalAuthentication = new EducationalAuthentication();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $educationalAuthentication->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $educationalAuthentication->setName($attributes['name']);
        }
        if (isset($attributes['category'])) {
            $educationalAuthentication->setCategory($attributes['category']);
        }
        if (isset($attributes['licence'])) {
            $educationalAuthentication->setLicence($attributes['licence']);
        }
        if (isset($attributes['operationType'])) {
            $educationalAuthentication->setOperationType($attributes['operationType']);
        }
        if (isset($attributes['area'])) {
            $educationalAuthentication->setArea($attributes['area']);
        }
        if (isset($attributes['address'])) {
            $educationalAuthentication->setAddress($attributes['address']);
        }
        if (isset($attributes['website'])) {
            $educationalAuthentication->setWebsite($attributes['website']);
        }
        if (isset($attributes['businessScope'])) {
            $educationalAuthentication->setBusinessScope($attributes['businessScope']);
        }
        if (isset($attributes['mpWeChat'])) {
            $educationalAuthentication->setMpWeChat($attributes['mpWeChat']);
        }
        if (isset($attributes['profile'])) {
            $educationalAuthentication->setProfile($attributes['profile']);
        }
        if (isset($attributes['createTime'])) {
            $educationalAuthentication->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $educationalAuthentication->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $educationalAuthentication->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $educationalAuthentication->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $educationalAuthentication->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }

        if (isset($relationships['relation']['data'])) {
            $relation = $this->changeArrayFormat($relationships['relation']['data']);
            $educationalAuthentication->setRelation(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($relation)
            );
        }

        return $educationalAuthentication;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($educationalAuthentication, array $keys = array())
    {
        if (!$educationalAuthentication instanceof EducationalAuthentication) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'licence',
                'category',
                'area',
                'address',
                'website',
                'businessScope',
                'mpWeChat',
                'profile',
                'enterprise'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'educationalAgencyAuthentications',
            ),
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $educationalAuthentication->getId();
        }

        $attributes = array();

        if (in_array('category', $keys)) {
            $attributes['category'] = $educationalAuthentication->getCategory();
        }

        if (in_array('area', $keys)) {
            $attributes['area'] = $educationalAuthentication->getArea();
        }

        if (in_array('licence', $keys)) {
            $attributes['licence'] = $educationalAuthentication->getLicence();
        }

        if (in_array('address', $keys)) {
            $attributes['address'] = $educationalAuthentication->getAddress();
        }

        if (in_array('website', $keys)) {
            $attributes['website'] = $educationalAuthentication->getWebsite();
        }

        if (in_array('businessScope', $keys)) {
            $attributes['businessScope'] = $educationalAuthentication->getBusinessScope();
        }

        if (in_array('mpWeChat', $keys)) {
            $attributes['mpWeChat'] = $educationalAuthentication->getMpWeChat();
        }

        if (in_array('profile', $keys)) {
            $attributes['profile'] = $educationalAuthentication->getProfile();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprises',
                    'id' => $educationalAuthentication->getEnterprise()->getId(),
                ),
            );
        }

        return $expression;
    }
}
