<?php
namespace Trial\EducationalAuthentication\Translator;

use Trial\EducationalAuthentication\Model\UnAuditedEducationalAuthentication;
use Trial\EducationalAuthentication\Model\NullUnAuditedEducationalAuthentication;

class UnAuditedEducationalAuthenticationRestfulTranslator extends EducationalAuthenticationRestfulTranslator
{
    public function arrayToObject(array $expression, $unAuditedEducationalAuthentication = null)
    {
        return $this->translateToObject($expression, $unAuditedEducationalAuthentication);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $unAuditedEducationalAuthentication = null)
    {
        if (empty($expression)) {
            return NullUnAuditedEducationalAuthentication::getInstance();
        }

        if ($unAuditedEducationalAuthentication == null) {
            $unAuditedEducationalAuthentication = new UnAuditedEducationalAuthentication();
        }

        $unAuditedEducationalAuthentication = parent::translateToObject($expression, $unAuditedEducationalAuthentication);

        $data =  $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['rejectReason'])) {
            $unAuditedEducationalAuthentication->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['applyStatus'])) {
            $unAuditedEducationalAuthentication->setApplyStatus($attributes['applyStatus']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['relation']['data'])) {
            $relation = $this->changeArrayFormat($relationships['relation']['data']);
            $unAuditedEducationalAuthentication->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($relation)
            );
        }

        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $unAuditedEducationalAuthentication->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }

        return $unAuditedEducationalAuthentication;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($unAuditedEducationalAuthentication, array $keys = array())
    {
        $educationalAuthentication = parent::objectToArray($unAuditedEducationalAuthentication, $keys);
        
        if (!$unAuditedEducationalAuthentication instanceof UnAuditedEducationalAuthentication) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'rejectReason'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'unAuditedEducationalAgencyAuthentications'
            )
        );

        $attributes = array();

        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $unAuditedEducationalAuthentication->getRejectReason();
        }

        $expression['data']['attributes'] = array_merge($educationalAuthentication['data']['attributes'], $attributes);

        return $expression;
    }
}
