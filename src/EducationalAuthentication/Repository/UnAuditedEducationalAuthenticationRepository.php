<?php
namespace Trial\EducationalAuthentication\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ResubmitAbleRepositoryTrait;
use Trial\Common\Repository\ApplyAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\EducationalAuthentication\Adapter\EducationalAuthentication\IUnAuditedEducationalAuthenticationAdapter;
use Trial\EducationalAuthentication\Adapter\EducationalAuthentication\UnAuditedEducationalAuthenticationMockAdapter;
use Trial\EducationalAuthentication\Adapter\EducationalAuthentication\UnAuditedEducationalAuthenticationRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class UnAuditedEducationalAuthenticationRepository extends Repository implements IUnAuditedEducationalAuthenticationAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'UNAUDITED_EDUCATIONAL_AUTHENTICATION_LIST';
    const FETCH_ONE_MODEL_UN = 'UNAUDITED_EDUCATIONAL_AUTHENTICATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditedEducationalAuthenticationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IUnAuditedEducationalAuthenticationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IUnAuditedEducationalAuthenticationAdapter
    {
        return new UnAuditedEducationalAuthenticationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
