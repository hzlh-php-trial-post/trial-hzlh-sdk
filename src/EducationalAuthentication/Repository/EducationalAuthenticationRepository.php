<?php
namespace Trial\EducationalAuthentication\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\EducationalAuthentication\Adapter\EducationalAuthentication\EducationalAuthenticationMockAdapter;
use Trial\EducationalAuthentication\Adapter\EducationalAuthentication\EducationalAuthenticationRestfulAdapter;
use Trial\EducationalAuthentication\Adapter\EducationalAuthentication\IEducationalAuthenticationAdapter;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;

class EducationalAuthenticationRepository extends Repository implements IEducationalAuthenticationAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'EDUCATIONAL_AUTHENTICATION_LIST';
    const FETCH_ONE_MODEL_UN = 'EDUCATIONAL_AUTHENTICATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new EducationalAuthenticationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IEducationalAuthenticationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IEducationalAuthenticationAdapter
    {
        return new EducationalAuthenticationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
