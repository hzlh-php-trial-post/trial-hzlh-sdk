<?php
namespace Trial\EducationalAuthentication\Adapter\EducationalAuthentication;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Trial\EducationalAuthentication\Model\UnAuditedEducationalAuthentication;
use Trial\EducationalAuthentication\Model\NullUnAuditedEducationalAuthentication;

use Trial\EducationalAuthentication\Translator\UnAuditedEducationalAuthenticationRestfulTranslator;

class UnAuditedEducationalAuthenticationRestfulAdapter extends GuzzleAdapter implements IUnAuditedEducationalAuthenticationAdapter //phpcs:ignore
{
    use FetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
            'UNAUDITED_EDUCATIONAL_AUTHENTICATION_LIST'=>[
                'fields'=>[],
                'include'=>'relation,enterprise'
            ],
            'UNAUDITED_EDUCATIONAL_AUTHENTICATION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=>'relation,enterprise'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new UnAuditedEducationalAuthenticationRestfulTranslator();
        $this->resource = 'unAuditedEducationalAgencyAuthentications';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullUnAuditedEducationalAuthentication::getInstance());
    }

    protected function resubmitAction(UnAuditedEducationalAuthentication $unAuditedEducationalAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditedEducationalAuthentication,
            array(
                'licence',
                'category',
                'area',
                'address',
                'website',
                'businessScope',
                'mpWeChat',
                'profile'
            )
        );

        $this->patch(
            $this->getResource().'/'.$unAuditedEducationalAuthentication->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditedEducationalAuthentication);
            return true;
        }

        return false;
    }
}
