<?php
namespace Trial\EducationalAuthentication\Adapter\EducationalAuthentication;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Trial\EducationalAuthentication\Model\EducationalAuthentication;
use Trial\EducationalAuthentication\Model\NullEducationalAuthentication;

use Trial\EducationalAuthentication\Translator\EducationalAuthenticationRestfulTranslator;

class EducationalAuthenticationRestfulAdapter extends GuzzleAdapter implements IEducationalAuthenticationAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
            'EDUCATIONAL_AUTHENTICATION_LIST'=>[
                'fields'=>[],
                'include'=>'relation,enterprise'
            ],
            'EDUCATIONAL_AUTHENTICATION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=>'relation,enterprise'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new EducationalAuthenticationRestfulTranslator();
        $this->resource = 'educationalAgencyAuthentications';
        $this->scenario = array();
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullEducationalAuthentication::getInstance());
    }

    protected function addAction(EducationalAuthentication $educationalAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $educationalAuthentication,
            array(
                'licence',
                'category',
                'area',
                'address',
                'website',
                'businessScope',
                'mpWeChat',
                'profile',
                'enterprise'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($educationalAuthentication);
            return true;
        }

        return false;
    }

    protected function editAction(EducationalAuthentication $educationalAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $educationalAuthentication,
            array(
                'licence',
                'category',
                'area',
                'address',
                'website',
                'businessScope',
                'mpWeChat',
                'profile'
            )
        );

        $this->patch(
            $this->getResource().'/'.$educationalAuthentication->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($educationalAuthentication);
            return true;
        }

        return false;
    }
}
