<?php
namespace Trial\EducationalAuthentication\Adapter\EducationalAuthentication;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;
use Trial\Common\Adapter\IFetchAbleAdapter;
use Marmot\Interfaces\IAsyncAdapter;

interface IUnAuditedEducationalAuthenticationAdapter extends IResubmitAbleAdapter, IApplyAbleAdapter, IFetchAbleAdapter, IAsyncAdapter //phpcs:ignore
{
}
