<?php
namespace Trial\EducationalAuthentication\Adapter\EducationalAuthentication;

use Marmot\Interfaces\IAsyncAdapter;
use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IApplyAbleAdapter;

interface IEducationalAuthenticationAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
