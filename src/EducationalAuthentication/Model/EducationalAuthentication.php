<?php
namespace Trial\EducationalAuthentication\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\EducationalAuthentication\Repository\EducationalAuthenticationRepository;

use Trial\Enterprise\Model\Enterprise;

use Trial\Common\Model\IOperatAble;

use Trial\Common\Model\OperatAbleTrait;

use Trial\Common\Adapter\IOperatAbleAdapter;

class EducationalAuthentication implements IOperatAble, IObject
{
    use Object, OperatAbleTrait;

    const OPERATION_TYPE = array(
        'NULL' => 0,
        'ADD' => 1,
        'EDIT' => 2
    );

    const CATEGORY = array(
        'NULL' => 0,
        'EDUCATION_TEACH' => 1,
        'VOCATIONAL_EDUCATION' => 2,
        'TALENT_TRAIN' => 3,
        'EMPLOYMENT' => 4,
        'MAJOR_COURSES' => 5,
    );

    private $id;

    private $name;

    private $category;

    private $licence;

    private $area;

    private $address;

    private $website;

    private $businessScope;

    private $mpWeChat;

    private $profile;

    private $operationType;

    private $relationId;

    private $relation;

    private $enterprise;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->category = self::CATEGORY['NULL'];
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->licence = array();
        $this->area = '';
        $this->address = '';
        $this->website = '';
        $this->businessScope = '';
        $this->mpWeChat = '';
        $this->profile = '';
        $this->relation = new Enterprise();
        $this->enterprise = new Enterprise();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = 0;
        $this->statusTime = 0;
        $this->repository = new EducationalAuthenticationRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->category);
        unset($this->operationType);
        unset($this->licence);
        unset($this->area);
        unset($this->address);
        unset($this->website);
        unset($this->businessScope);
        unset($this->mpWeChat);
        unset($this->profile);
        unset($this->relation);
        unset($this->enterprise);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setCategory(int $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setLicence(array $licence) : void
    {
        $this->licence = $licence;
    }

    public function getLicence() : array
    {
        return $this->licence;
    }

    public function setOperationType(int $operationType) : void
    {
        $this->operationType = $operationType;
    }

    public function getOperationType() : int
    {
        return $this->operationType;
    }

    public function setArea(string $area) : void
    {
        $this->area = $area;
    }

    public function getArea() : string
    {
        return $this->area;
    }

    public function setAddress(string $address) : void
    {
        $this->address = $address;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function setWebsite(string $website) : void
    {
        $this->website = $website;
    }

    public function getWebsite() : string
    {
        return $this->website;
    }

    public function setBusinessScope(string $businessScope) : void
    {
        $this->businessScope = $businessScope;
    }

    public function getBusinessScope() : string
    {
        return $this->businessScope;
    }

    public function setMpWeChat(string $mpWeChat) : void
    {
        $this->mpWeChat = $mpWeChat;
    }

    public function getMpWeChat() : string
    {
        return $this->mpWeChat;
    }

    public function setProfile(string $profile) : void
    {
        $this->profile = $profile;
    }

    public function getProfile() : string
    {
        return $this->profile;
    }

    public function setEnterprise(Enterprise $enterprise): void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise(): Enterprise
    {
        return $this->enterprise;
    }

    public function setRelation(Enterprise $relation): void
    {
        $this->relation = $relation;
    }

    public function getRelation(): Enterprise
    {
        return $this->relation;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
