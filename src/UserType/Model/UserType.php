<?php
namespace Trial\UserType\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;
use Marmot\Core;

use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;

use Trial\Crew\Model\Crew;
use Trial\UserType\Repository\UserTypeRepository;

class UserType implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const USERTYPE_STATUS = [
        'ENABLE' => 0,
        'DISABLE' => -2
    ];

    private $id;

    private $commission;

    private $subsidy;

    private $name;

    private $description;

    private $areaId;

    private $crew;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->commission = 0;
        $this->subsidy = 0;
        $this->name = '';
        $this->description = '';
        $this->areaId = 0;
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->repository = new UserTypeRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->commission);
        unset($this->subsidy);
        unset($this->name);
        unset($this->description);
        unset($this->areaId);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->crew);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCommission(int $commission) : void
    {
        $this->commission = $commission;
    }

    public function getCommission() : int
    {
        return $this->commission;
    }

    public function setSubsidy(int $subsidy) : void
    {
        $this->subsidy = $subsidy;
    }

    public function getSubsidy() : int
    {
        return $this->subsidy;
    }

    public function setAreaId(int $areaId) : void
    {
        $this->areaId = $areaId;
    }

    public function getAreaId() : int
    {
        return $this->areaId;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : UserTypeRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function add(): bool
    {
        $repository = $this->getRepository();

        return $repository->add($this);
    }

    public function edit(): bool
    {
        $repository = $this->getRepository();

        return $repository->edit($this);
    }

    public function enable(): bool
    {
        $repository = $this->getRepository();

        return $repository->enable($this);
    }

    public function disable(): bool
    {
        $repository = $this->getRepository();

        return $repository->disable($this);
    }
}
