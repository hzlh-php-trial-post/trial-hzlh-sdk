<?php
namespace Trial\UserType\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;
use Marmot\Core;

use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;

use Trial\Crew\Model\Crew;
use Trial\UserType\Repository\AuditTypeRepository;

class AuditType implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const APPLY_STATUS = [
        'PENDING' => 0,//待审核
        'APPROVE' => 2,//已审核
        'REJECT' => -2 //已拒绝
    ];

    private $id;

    private $commission;

    private $subsidy;

    private $name;

    private $description;

    private $areaId;

    private $applyStatus;

    private $rejectReason;

    private $crew;

    private $userType;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->commission = 0;
        $this->subsidy = 0;
        $this->name = '';
        $this->description = '';
        $this->areaId = 0;
        $this->status = 0;
        $this->applyStatus = 0;
        $this->rejectReason = '';
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->userType = new UserType();
        $this->repository = new AuditTypeRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->commission);
        unset($this->subsidy);
        unset($this->name);
        unset($this->description);
        unset($this->areaId);
        unset($this->status);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->crew);
        unset($this->userType);
        unset($this->repository);
    }

    protected function getRepository() : AuditTypeRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCommission(int $commission) : void
    {
        $this->commission = $commission;
    }

    public function getCommission() : int
    {
        return $this->commission;
    }

    public function setSubsidy(int $subsidy) : void
    {
        $this->subsidy = $subsidy;
    }

    public function getSubsidy() : int
    {
        return $this->subsidy;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setAreaId(int $areaId) : void
    {
        $this->areaId = $areaId;
    }

    public function getAreaId() : int
    {
        return $this->areaId;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setApplyStatus(int $applyStatus): void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserType(UserType $userType) : void
    {
        $this->userType = $userType;
    }

    public function getUserType() : UserType
    {
        return $this->userType;
    }

    public function add(): bool
    {
        $repository = $this->getRepository();

        return $repository->add($this);
    }

    public function edit(): bool
    {
        $repository = $this->getRepository();

        return $repository->edit($this);
    }

    public function approve(): bool
    {
        $repository = $this->getRepository();

        return $repository->approve($this);
    }

    public function reject(): bool
    {
        $repository = $this->getRepository();

        return $repository->reject($this);
    }
}
