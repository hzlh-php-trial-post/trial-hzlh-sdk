<?php
namespace Trial\UserType\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\Crew\Translator\CrewRestfulTranslator;
use Trial\UserType\Model\NullUserType;
use Trial\UserType\Model\UserType;

class UserTypeRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $userType = null)
    {
        return $this->translateToObject($expression, $userType);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $userType = null)
    {
        if (empty($expression)) {
            return NullUserType::getInstance();
        }

        if ($userType == null) {
            $userType = new UserType();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $userType->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $userType->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $userType->setDescription($attributes['description']);
        }
        if (isset($attributes['areaId'])) {
            $userType->setAreaId($attributes['areaId']);
        }
        if (isset($attributes['commission'])) {
            $userType->setCommission($attributes['commission']);
        }
        if (isset($attributes['subsidy'])) {
            $userType->setSubsidy($attributes['subsidy']);
        }
        if (isset($attributes['createTime'])) {
            $userType->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $userType->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $userType->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $userType->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $userType->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $userType;
    }


    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($userType, array $keys = array())
    {
        $expression = array();

        if (!$userType instanceof userType) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'commission',
                'subsidy',
                'name',
                'description',
                'areaId',
                'status',
                'createTime',
                'updateTime',
                'statusTime',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'strategyTypes'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $userType->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $userType->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $userType->getDescription();
        }
        if (in_array('areaId', $keys)) {
            $attributes['areaId'] = $userType->getAreaId();
        }
        if (in_array('commission', $keys)) {
            $attributes['commission'] = $userType->getCommission();
        }
        if (in_array('subsidy', $keys)) {
            $attributes['subsidy'] = $userType->getSubsidy();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $userType->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $userType->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $userType->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $userType->getStatusTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $userType->getCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
