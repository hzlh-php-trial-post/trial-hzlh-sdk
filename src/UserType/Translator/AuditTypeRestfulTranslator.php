<?php
namespace Trial\UserType\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\Crew\Translator\CrewRestfulTranslator;
use Trial\UserType\Model\NullAuditType;
use Trial\UserType\Model\AuditType;
use Trial\UserType\Translator\UserTypeRestfulTranslator;

class AuditTypeRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    protected function getUserTypeRestfulTranslator()
    {
        return new UserTypeRestfulTranslator();
    }

    public function arrayToObject(array $expression, $auditType = null)
    {
        return $this->translateToObject($expression, $auditType);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $auditType = null)
    {
        if (empty($expression)) {
            return NullAuditType::getInstance();
        }

        if ($auditType == null) {
            $auditType = new AuditType();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $auditType->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['commission'])) {
            $auditType->setCommission($attributes['commission']);
        }
        if (isset($attributes['subsidy'])) {
            $auditType->setSubsidy($attributes['subsidy']);
        }
        if (isset($attributes['name'])) {
            $auditType->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $auditType->setDescription($attributes['description']);
        }
        if (isset($attributes['areaId'])) {
            $auditType->setAreaId($attributes['areaId']);
        }
        if (isset($attributes['applyStatus'])) {
            $auditType->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['rejectReason'])) {
            $auditType->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['createTime'])) {
            $auditType->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $auditType->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $auditType->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $auditType->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $auditType->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }
        if (isset($relationships['strategyType']['data'])) {
            $strategyType = $this->changeArrayFormat($relationships['strategyType']['data']);
            $auditType->setUserType($this->getUserTypeRestfulTranslator()->arrayToObject($strategyType));
        }

        return $auditType;
    }


    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($auditType, array $keys = array())
    {
        $expression = array();

        if (!$auditType instanceof AuditType) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'commission',
                'subsidy',
                'name',
                'description',
                'areaId',
                'status',
                'applyStatus',
                'rejectReason',
                'createTime',
                'updateTime',
                'statusTime',
                'crew',
                'strategyType'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'unAuditedStrategyTypes'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $auditType->getId();
        }

        $attributes = array();

        if (in_array('commission', $keys)) {
            $attributes['commission'] = $auditType->getCommission();
        }
        if (in_array('subsidy', $keys)) {
            $attributes['subsidy'] = $auditType->getSubsidy();
        }
        if (in_array('name', $keys)) {
            $attributes['name'] = $auditType->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $auditType->getDescription();
        }
        if (in_array('areaId', $keys)) {
            $attributes['areaId'] = $auditType->getAreaId();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $auditType->getStatus();
        }
        if (in_array('applyStatus', $keys)) {
            $attributes['applyStatus'] = $auditType->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $auditType->getRejectReason();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $auditType->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $auditType->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $auditType->getStatusTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $auditType->getCrew()->getId()
                )
            );
        }

        if (in_array('strategyType', $keys)) {
            $expression['data']['relationships']['strategyType']['data'] = array(
                array(
                    'type' => 'strategyTypes',
                    'id' => $auditType->getUserType()->getId()
                )
            );
        }

        return $expression;
    }
}
