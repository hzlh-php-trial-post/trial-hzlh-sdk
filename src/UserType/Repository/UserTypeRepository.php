<?php
namespace Trial\UserType\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\UserType\Adapter\UserType\IUserTypeAdapter;
use Trial\UserType\Adapter\UserType\UserTypeMockAdapter;
use Trial\UserType\Adapter\UserType\UserTypeRestfulAdapter;
use Trial\UserType\Model\UserType;

class UserTypeRepository extends Repository implements IUserTypeAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    const LIST_MODEL_UN = 'USERTYPE_LIST';
    const FETCH_ONE_MODEL_UN = 'USERTYPE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new UserTypeRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function getActualAdapter() : IUserTypeAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IUserTypeAdapter
    {
        return new UserTypeMockAdapter();
    }

    public function enable(UserType $userType): bool
    {
        return $this->getAdapter()->enable($userType);
    }

    public function disable(UserType $userType): bool
    {
        return $this->getAdapter()->disable($userType);
    }
}
