<?php
namespace Trial\UserType\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Usertype\Model\AuditType;
use Trial\UserType\Adapter\AuditType\IAuditTypeAdapter;
use Trial\UserType\Adapter\AuditType\AuditTypeMockAdapter;
use Trial\UserType\Adapter\AuditType\AuditTypeRestfulAdapter;

class AuditTypeRepository extends Repository implements IAuditTypeAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    const AUDIT_LIST_MODEL_UN = 'AUDIT_TYPE_LIST';
    const AUDIT_FETCH_ONE_MODEL_UN = 'AUDIT_TYPE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new AuditTypeRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function getActualAdapter() : IAuditTypeAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IAuditTypeAdapter
    {
        return new AuditTypeMockAdapter();
    }

    public function approve(AuditType $auditType): bool
    {
        return $this->getAdapter()->approve($auditType);
    }

    public function reject(AuditType $auditType): bool
    {
        return $this->getAdapter()->reject($auditType);
    }
}
