<?php
namespace Trial\UserType\Adapter\AuditType;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\UserType\Model\AuditType;

interface IAuditTypeAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
    public function approve(AuditType $auditType) : bool;

    public function reject(AuditType $auditType) : bool;
}
