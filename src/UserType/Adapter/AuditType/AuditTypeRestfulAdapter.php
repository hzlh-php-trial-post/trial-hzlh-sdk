<?php
namespace Trial\UserType\Adapter\AuditType;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Trial\UserType\Model\NullAuditType;
use Trial\UserType\Model\AuditType;
use Trial\UserType\Translator\AuditTypeRestfulTranslator;

class AuditTypeRestfulAdapter extends GuzzleAdapter implements IAuditTypeAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    const SCENARIOS = [
        'AUDIT_TYPE_LIST'=>[
            'fields'=>[
                'strategyTypes'=>
                    'name,status'
            ],
            'include'=>'relation,crew'
        ],
        'AUDIT_TYPE_FETCH_ONE'=>[
            'fields'=>[
                'strategyTypes'=>
                    'name,status'
            ],
            'include'=>'relation,crew'
        ]
    ];
    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new AuditTypeRestfulTranslator();
        $this->resource = 'unAuditedStrategyTypes';
        $this->scenario = array();
    }

    protected function getMapError() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullAuditType::getInstance());
    }

    protected function addAction(AuditType $auditType): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $auditType,
            array(
                "name",
                "description",
                "areaId",
                "commission",
                "subsidy",
                "status",
                "crew",
                "strategyType"
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($auditType);
            return true;
        }
        return false;
    }

    protected function editAction(AuditType $auditType): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $auditType,
            array(
                "name",
                "description",
                "areaId",
                "commission",
                "subsidy",
                "status",
                "crew",
                "strategyType"
            )
        );

        $this->patch(
            $this->getResource().'/'.$auditType->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($auditType);
            return true;
        }
        return false;
    }

    public function approve(AuditType $auditType) : bool
    {
        $this->patch(
            $this->getResource().'/'.$auditType->getId().'/approve'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($auditType);
            return true;
        }

        return false;
    }

    public function reject(AuditType $auditType) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $auditType,
            array(
                "rejectReason"
            )
        );

        $this->patch(
            $this->getResource().'/'.$auditType->getId().'/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($auditType);
            return true;
        }

        return false;
    }
}
