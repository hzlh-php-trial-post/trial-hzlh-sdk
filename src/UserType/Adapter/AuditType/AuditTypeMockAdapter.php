<?php
namespace Trial\UserType\Adapter\AuditType;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Model\IOperatAble;

use Trial\UserType\Utils\MockAuditTypeFactory;
use Trial\UserType\Model\AuditType;

class AuditTypeMockAdapter implements IAuditTypeAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockAuditTypeFactory::generateAuditTypeObject($id);
    }

    public function fetchList(array $ids): array
    {
        $auditTypeList = array();

        foreach ($ids as $id) {
            $auditTypeList[] = MockAuditTypeFactory::generateAuditTypeObject($id);
        }

        return $auditTypeList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockAuditTypeFactory::generateAuditTypeObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $auditTypeList = array();

        foreach ($ids as $id) {
            $auditTypeList[] = MockAuditTypeFactory::generateAuditTypeObject($id);
        }

        return $auditTypeList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function edit(IOperatAble $operatAbleObject) : bool
    {
        unset($operatAbleObject);

        return true;
    }

    public function add(IOperatAble $operatAbleObject) : bool
    {
        unset($operatAbleObject);

        return true;
    }

    public function approve(AuditType $auditType) : bool
    {
        unset($auditType);
        return true;
    }

    public function reject(AuditType $auditType) : bool
    {
        unset($auditType);
        return true;
    }
}
