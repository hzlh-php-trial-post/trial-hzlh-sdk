<?php
namespace Trial\UserType\Adapter\UserType;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\UserType\Model\UserType;

interface IUserTypeAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
    public function enable(UserType $userType) : bool;

    public function disable(UserType $userType) : bool;
}
