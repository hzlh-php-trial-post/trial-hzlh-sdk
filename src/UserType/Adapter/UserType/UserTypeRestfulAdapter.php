<?php
namespace Trial\UserType\Adapter\UserType;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Trial\UserType\Model\NullUserType;
use Trial\UserType\Model\UserType;
use Trial\UserType\Translator\UserTypeRestfulTranslator;

class UserTypeRestfulAdapter extends GuzzleAdapter implements IUserTypeAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

        const SCENARIOS = [
            'USERTYPE_LIST'=>[
                'fields'=>[
                    'crews'=>
                        'id'
                ],
                'include'=>'crew'
            ],
            'USERTYPE_FETCH_ONE'=>[
                'fields'=>[
                    'crews'=>
                        'id'
                ],
                'include'=>'crew'
            ]
        ];
        private $translator;

        private $resource;

        public function __construct(string $uri = '', array $authKey = [])
        {
            parent::__construct(
                $uri,
                $authKey
            );
            $this->translator = new UserTypeRestfulTranslator();
            $this->resource = 'strategyTypes';
            $this->scenario = array();
        }

        protected function getMapError() : array
        {
            return $this->commonMapErrors();
        }

        protected function getTranslator() : IRestfulTranslator
        {
            return $this->translator;
        }

        protected function getResource(): string
        {
            return $this->resource;
        }

        public function scenario($scenario) : void
        {
            $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
        }

        public function fetchOne($id)
        {
            return $this->fetchOneAction($id, NullUserType::getInstance());
        }

        protected function addAction(UserType $userType): bool
        {
            $data = $this->getTranslator()->objectToArray(
                $userType,
                array(
                "name",
                "description",
                "areaId",
                "commission",
                "subsidy",
                "status",
                "crew"
                )
            );

            $this->post(
                $this->getResource(),
                $data
            );

            if ($this->isSuccess()) {
                $this->translateToObject($userType);
                return true;
            }

            return false;
        }

        protected function editAction(UserType $userType): bool
        {
            $data = $this->getTranslator()->objectToArray(
                $userType,
                array(
                "name",
                "description",
                "areaId",
                "commission",
                "subsidy",
                "status",
                "crew"
                )
            );

            $this->patch(
                $this->getResource().'/'.$userType->getId(),
                $data
            );

            if ($this->isSuccess()) {
                $this->translateToObject($userType);
                return true;
            }

            return false;
        }

        public function enable(UserType $userType) : bool
        {
            $this->patch(
                $this->getResource().'/'.$userType->getId().'/enable'
            );

            if ($this->isSuccess()) {
                $this->translateToObject($userType);
                return true;
            }

            return false;
        }

        public function disable(UserType $userType) : bool
        {
            $this->patch(
                $this->getResource().'/'.$userType->getId().'/disable'
            );

            if ($this->isSuccess()) {
                $this->translateToObject($userType);
                return true;
            }

            return false;
        }
}
