<?php
namespace Trial\UserType\Adapter\UserType;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Model\IOperatAble;

use Trial\UserType\Utils\MockUserTypeFactory;
use Trial\UserType\Model\UserType;

class UserTypeMockAdapter implements IUserTypeAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockUserTypeFactory::generateUserTypeObject($id);
    }

    public function fetchList(array $ids): array
    {
        $userTypeList = array();

        foreach ($ids as $id) {
            $userTypeList[] = MockUserTypeFactory::generateUserTypeObject($id);
        }

        return $userTypeList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockUserTypeFactory::generateUserTypeObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $userTypeList = array();

        foreach ($ids as $id) {
            $userTypeList[] = MockUserTypeFactory::generateUserTypeObject($id);
        }

        return $userTypeList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function edit(IOperatAble $operatAbleObject) : bool
    {
        unset($operatAbleObject);

        return true;
    }

    public function add(IOperatAble $operatAbleObject) : bool
    {
        unset($operatAbleObject);

        return true;
    }

    public function enable(UserType $userType) : bool
    {
        unset($userType);

        return true;
    }

    public function disable(UserType $userType) : bool
    {
        unset($userType);

        return true;
    }
}
