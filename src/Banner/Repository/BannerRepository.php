<?php
namespace Trial\Banner\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\OnShelfAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\Banner\Adapter\Banner\IBannerAdapter;
use Trial\Banner\Adapter\Banner\BannerMockAdapter;
use Trial\Banner\Adapter\Banner\BannerRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Banner\Model\Banner;

class BannerRepository extends Repository implements IBannerAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'BANNER_LIST';
    const FETCH_ONE_MODEL_UN = 'BANNER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new BannerRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IBannerAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBannerAdapter
    {
        return new BannerMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function delete(Banner $banner) : bool
    {
        return $this->getAdapter()->deletes($banner);
    }

    public function tagSearch(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->tagSearch($filter, $sort, $number, $size);
    }
}
