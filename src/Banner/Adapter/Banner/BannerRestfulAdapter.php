<?php
namespace Trial\Banner\Adapter\Banner;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Banner\Model\Banner;
use Trial\Banner\Model\NullBanner;
use Trial\Banner\Translator\BannerRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class BannerRestfulAdapter extends GuzzleAdapter implements IBannerAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        OnShelfAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'BANNER_LIST'=>[
                'fields'=>[
                    'banners'=>'launchType,bannerType,place,image,status,link'
                ],
                'include'=> 'crew'
            ],
            'BANNER_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new BannerRestfulTranslator();
        $this->resource = 'banners';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function tagSearchAction(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        $header = array(
            'member_id' => isset($filter['tagMemberId']) ? $filter['tagMemberId'] : 0
        );

        $this->get(
            $this->getResource(),
            array(
                'filter'=>$filter,
                'sort'=>implode(',', $sort),
                'page'=>array('size'=>$size, 'number'=>$number)
            ),
            $header
        );
       
        return $this->isSuccess() ? $this->translateToObjects() : array(0, array());
    }

    public function tagSearch(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->tagSearchAction($filter, $sort, $number, $size);
    }
    
    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullBanner::getInstance());
    }

    protected function addAction(Banner $banner) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $banner,
            array(
                'launchType',
                'type',
                'place',
                'image',
                'link',
                'remark',
                'crew',
                'tag'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($banner);
            return true;
        }

        return false;
    }

    protected function editAction(Banner $banner) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $banner,
            array(
                'launchType',
                'type',
                'place',
                'image',
                'link',
                'remark',
                'tag'
            )
        );

        $this->patch(
            $this->getResource().'/'.$banner->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($banner);
            return true;
        }

        return false;
    }

    public function deletes(Banner $banner) : bool
    {
        $this->patch(
            $this->getResource().'/'.$banner->getId().'/delete'
        );
        if ($this->isSuccess()) {
            $this->translateToObject($banner);
            return true;
        }
        return false;
    }
}
