<?php
namespace Trial\Banner\Adapter\Banner;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleMockAdapterTrait;

use Trial\Banner\Utils\MockFactory;

class BannerMockAdapter implements IBannerAdapter
{
    use OperatAbleMockAdapterTrait, OnShelfAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateBannerObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $bannerList = array();

        foreach ($ids as $id) {
            $bannerList[] = MockFactory::generateBannerObject($id);
        }

        return $bannerList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateBannerObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $bannerList = array();

        foreach ($ids as $id) {
            $bannerList[] = MockFactory::generateBannerObject($id);
        }

        return $bannerList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
