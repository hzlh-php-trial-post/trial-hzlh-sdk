<?php
namespace Trial\Banner\Translator;

use Trial\Banner\Model\Banner;
use Trial\Banner\Model\NullBanner;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Crew\Translator\CrewRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class BannerRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $banner = null)
    {
        return $this->translateToObject($expression, $banner);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $banner = null)
    {
        if (empty($expression)) {
            return NullBanner::getInstance();
        }

        if ($banner == null) {
            $banner = new Banner();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $banner->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['launchType'])) {
            $banner->setLaunchType($attributes['launchType']);
        }
        if (isset($attributes['bannerType'])) {
            $banner->setBannerType($attributes['bannerType']);
        }
        if (isset($attributes['place'])) {
            $banner->setPlace($attributes['place']);
        }
        if (isset($attributes['placeDetail'])) {
            $banner->setPlaceDetail($attributes['placeDetail']);
        }
        if (isset($attributes['image'])) {
            $banner->setImage($attributes['image']);
        }
        if (isset($attributes['link'])) {
            $banner->setLink($attributes['link']);
        }
        if (isset($attributes['remark'])) {
            $banner->setRemark($attributes['remark']);
        }
        if (isset($attributes['tag'])) {
            $banner->setTag($attributes['tag']);
        }
        if (isset($attributes['createTime'])) {
            $banner->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $banner->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $banner->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $banner->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $banner->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $banner;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($banner, array $keys = array())
    {
        $expression = array();

        if (!$banner instanceof Banner) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'launchType',
                'type',
                'place',
                'image',
                'link',
                'remark',
                'tag',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'banners'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $banner->getId();
        }

        $attributes = array();

        if (in_array('launchType', $keys)) {
            $attributes['launchType'] = $banner->getLaunchType();
        }
        if (in_array('type', $keys)) {
            $attributes['type'] = $banner->getBannerType();
        }
        if (in_array('place', $keys)) {
            $attributes['place'] = $banner->getPlace();
        }
        if (in_array('image', $keys)) {
            $attributes['image'] = $banner->getImage();
        }
        if (in_array('link', $keys)) {
            $attributes['link'] = $banner->getLink();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $banner->getRemark();
        }
        if (in_array('tag', $keys)) {
            $attributes['tag'] = $banner->getTag();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $banner->getCrew()->getId()
                )
             );
        }

        return $expression;
    }
}
