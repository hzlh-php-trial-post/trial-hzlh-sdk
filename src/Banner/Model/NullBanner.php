<?php
namespace Trial\Banner\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullOperatAbleTrait;
use Trial\Common\Model\NullOnShelfAbleTrait;

class NullBanner extends Banner implements INull
{
    use NullOperatAbleTrait, NullOnShelfAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
