<?php
namespace Trial\Banner\Model;

class BannerModelFactory
{
    /**
     * pc位置详情
     */
    const PC_PLACE_DETAIL = array(
            Banner::PC_PLACE['HOME_PAGE_BANNER'] => array(
                'id' => 1,
                'title' => '首页banner',
                'url' => 'portal_yc_banner_home.png',
                'size' => '1920*516',
                'type' => Banner::TYPE['BANNER']
            ),
            Banner::PC_PLACE['SERVICE_BANNER'] => array(
                'id' => 2,
                'title' => '服务超市banner',
                'url' => 'portal_yc_banner_service.png',
                'size' => '1920*550',
                'type' => Banner::TYPE['BANNER']
            ),
            Banner::PC_PLACE['FINANCE_BANNER'] => array(
                'id' => 3,
                'title' => '金融超市banner',
                'url' => 'portal_yc_banner_home_finance.png',
                'size' => '1920*516',
                'type' => Banner::TYPE['BANNER']
            ),
            Banner::PC_PLACE['MONEY_WISE_BANNER'] => array(
                'id' => 4,
                'title' => '财智超市banner',
                'url' => 'portal_yc_banner_financial.png',
                'size' => '1920*800',
                'type' => Banner::TYPE['BANNER']
            ),
            Banner::PC_PLACE['HOME_PAGE_TOP_ADVERTISEMENT'] => array(
                'id' => 5,
                'title' => '首页顶部广告图',
                'url' => 'portal_yc_banner_home_top.png',
                'size' => '1920*58',
                'type' => Banner::TYPE['ADVERTISEMENT']
            ),
            Banner::PC_PLACE['HOME_PAGE_DEMAND'] => array(
                'id' => 6,
                'title' => '首页找需求广告图',
                'url' => 'portal_yc_banner_home_requirement.png',
                'size' => '254*350',
                'type' => Banner::TYPE['ADVERTISEMENT']
            ),
            Banner::PC_PLACE['HOME_PAGE_CAPITAL'] => array(
                'id' => 7,
                'title' => '首页找资金广告图',
                'url' => 'portal_yc_banner_home_fund.png',
                'size' => '385*146',
                'type' => Banner::TYPE['ADVERTISEMENT']
            ),
            Banner::PC_PLACE['USER_CENTER_APPOINTMENT_DETAIL'] => array(
                'id' => 8,
                'title' => '个人中心我的申请详情广告图',
                'url' => 'portal_yc_banner_user_appointment_detail.png',
                'size' => '208*185',
                'type' => Banner::TYPE['ADVERTISEMENT']
            )
        );
    /**
     * app位置详情
     */
    const APP_PLACE_DETAIL = array(
            Banner::APP_PLACE['HOME_PAGE_BANNER'] => array(
                'id' => 1,
                'title' => '首页banner',
                'url' => 'portal_yc_app_banner_home.png',
                'size' => '525*295',
                'type' => Banner::TYPE['BANNER']
            ),
            Banner::APP_PLACE['SERVICE_BANNER'] => array(
                'id' => 2,
                'title' => '服务超市banner',
                'url' => 'portal_yc_app_banner_service.png',
                'size' => '525*295',
                'type' => Banner::TYPE['BANNER']
            )
        );
}
