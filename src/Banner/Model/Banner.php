<?php
namespace Trial\Banner\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Common\Model\IOnShelfAble;
use Trial\Common\Model\OnShelfAbleTrait;
use Trial\Common\Adapter\IOnShelfAbleAdapter;

use Trial\Crew\Model\Crew;

use Trial\Banner\Repository\BannerRepository;

class Banner implements IObject, IOperatAble, IOnShelfAble
{
    use Object, OperatAbleTrait, OnShelfAbleTrait;

    const BANNER_STATUS = array(
        'ON_SHELF' => 0,
        'OFF_STOCK' => -2,
        'DELETE' => -4,
    );

    const LAUNCH_TYPE = array(
        'NULL' => 0,
        'PC' => 1,  //pc
        'APP' => 2, //app
    );

    const TYPE = array(
        'NULL' => 0,
        'ADVERTISEMENT' => 1,  //广告位
        'BANNER' => 2, //banner图
    );

    const PC_PLACE = array(
        'HOME_PAGE_BANNER' => 1, //首页banner
        'SERVICE_BANNER' => 2, //服务超市banner
        'FINANCE_BANNER' => 3, //金融超市banner
        'MONEY_WISE_BANNER' => 4, //财智超市banner
        'HOME_PAGE_TOP_ADVERTISEMENT' => 5, //首页顶部广告图
        'HOME_PAGE_DEMAND' => 6, //首页找需求广告图
        'HOME_PAGE_CAPITAL' => 7, //首页找资金广告图
        'USER_CENTER_APPOINTMENT_DETAIL' => 8, //个人中心我的申请详情
    );

    const APP_PLACE = array(
        'HOME_PAGE_BANNER' => 1, //首页banner
        'SERVICE_BANNER' => 2, //服务超市banner
    );

    const MAX_NUMBER = 5;

    private $id;

    private $launchType;

    private $bannerType;

    private $place;

    private $image;

    private $link;

    private $remark;

    private $tag;

    private $crew;

    private $repository;
    

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->launchType = self::LAUNCH_TYPE['NULL'];
        $this->bannerType = self::TYPE['NULL'];
        $this->place = 0;
        $this->image = array();
        $this->link = '';
        $this->remark = '';
        $this->tag = '';
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->status = self::BANNER_STATUS['OFF_STOCK'];
        $this->createTime = '';
        $this->updateTime = '';
        $this->statusTime = 0;
        $this->repository = new BannerRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->launchType);
        unset($this->bannerType);
        unset($this->place);
        unset($this->image);
        unset($this->link);
        unset($this->remark);
        unset($this->tag);
        unset($this->crew);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }
    
    public function setLaunchType(int $launchType) : void
    {
        $this->launchType = in_array($launchType, self::LAUNCH_TYPE) ?
            $launchType : self::LAUNCH_TYPE['NULL'];
    }

    public function getLaunchType() : int
    {
        return $this->launchType;
    }

    public function setBannerType(int $bannerType) : void
    {
        $this->bannerType = in_array($bannerType, self::TYPE) ? $bannerType : self::TYPE['NULL'];
    }

    public function getBannerType() : int
    {
        return $this->bannerType;
    }
    
    public function setPlace(int $place) : void
    {
        $this->place = $place;
    }

    public function getPlace() : int
    {
        return $this->place;
    }

    public function setPlaceDetail(array $placeDetail) : void
    {
        $this->placeDetail = $placeDetail;
    }

    public function getPlaceDetail() : array
    {
        return $this->placeDetail;
    }

    public function setImage(array $image) : void
    {
        $this->image = $image;
    }

    public function getImage() : array
    {
        return $this->image;
    }

    public function setLink(string $link) : void
    {
        $this->link = $link;
    }

    public function getLink() : string
    {
        return $this->link;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setTag(string $tag) : void
    {
        $this->tag = $tag;
    }

    public function getTag() : string
    {
        return $this->tag;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
    
    protected function getRepository() : BannerRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOnShelfAbleAdapter() : IOnShelfAbleAdapter
    {
        return $this->getRepository();
    }

    public function add() : bool
    {
        $repository = $this->getIOperatAbleAdapter();

        return $this->isMaxNumber() && $repository->add($this);
    }

    public function edit() : bool
    {
        if ($this->isOnShelf()) {
            Core::setLastError(RESOURCE_STATUS_ON_SHELF);
            return false;
        }

        $repository = $this->getIOperatAbleAdapter();

        return $repository->edit($this);
    }

    public function isMaxNumber() : bool
    {
        $filter = [];
        $sort = ['-updateTime'];

        $filter['launchType'] = $this->getLaunchType();
        $filter['place'] = $this->getPlace();
        $filter['status'] = self::BANNER_STATUS['ON_SHELF'];

        list($count, $list) = $this->getRepository()->search($filter, $sort);
        unset($list);

        if ($count >= self::MAX_NUMBER) {
            Core::setLastError(NUMBER_NOT_EXCEED_MAX);
            return false;
        }

        return true;
    }

    public function delete() : bool
    {
        if ($this->isOnShelf()) {
            Core::setLastError(RESOURCE_STATUS_ON_SHELF);
            return false;
        }

        return $this->getRepository()->delete($this);
    }
}
