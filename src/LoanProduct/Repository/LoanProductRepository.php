<?php
namespace Trial\LoanProduct\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ApplyAbleRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ResubmitAbleRepositoryTrait;
use Trial\Common\Repository\ModifyStatusAbleRepositoryTrait;

use Trial\LoanProduct\Adapter\LoanProduct\ILoanProductAdapter;
use Trial\LoanProduct\Adapter\LoanProduct\LoanProductMockAdapter;
use Trial\LoanProduct\Adapter\LoanProduct\LoanProductRestfulAdapter;

use Trial\LoanProduct\Model\LoanProduct;

class LoanProductRepository extends Repository implements ILoanProductAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ApplyAbleRepositoryTrait,
        OperatAbleRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_LOAN_PRODUCT_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_LOAN_PRODUCT_LIST';
    const FETCH_ONE_MODEL_UN = 'LOAN_PRODUCT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new LoanProductRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): ILoanProductAdapter
    {
        return new LoanProductMockAdapter();
    }

    public function getActualAdapter(): ILoanProductAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function portalFetchOne($id)
    {
        return $this->getAdapter()->portalFetchOne($id);
    }

    public function appFetchOne($id)
    {
        return $this->getAdapter()->appFetchOne($id);
    }

    public function tagSearch(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->tagSearch($filter, $sort, $number, $size);
    }
    
    public function onShelf(LoanProduct $loanProduct): bool
    {
        return $this->getAdapter()->onShelf($loanProduct);
    }

    public function offStock(LoanProduct $loanProduct): bool
    {
        return $this->getAdapter()->offStock($loanProduct);
    }
}
