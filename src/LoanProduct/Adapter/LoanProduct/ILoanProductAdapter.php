<?php
namespace Trial\LoanProduct\Adapter\LoanProduct;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IModifyStatusAbleAdapter;

interface ILoanProductAdapter extends IAsyncAdapter, IFetchAbleAdapter, ILoanProductOperatAdapter, IModifyStatusAbleAdapter//phpcs:ignore
{
}
