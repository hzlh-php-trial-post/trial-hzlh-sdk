<?php
namespace Trial\LoanProduct\Adapter\LoanProduct;

use Trial\Common\Adapter\ApplyAbleMockAdapterTrait;
use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleMockAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleMockAdapterTrait;

use Trial\LoanProduct\Model\LoanProduct;
use Trial\LoanProduct\Utils\MockFactory;

class LoanProductMockAdapter implements ILoanProductAdapter
{
    use OperatAbleMockAdapterTrait,
        ApplyAbleMockAdapterTrait,
        LoanProductMockAdapterTrait,
        ModifyStatusAbleMockAdapterTrait,
        ResubmitAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateLoanProductObject($id);
    }

    public function fetchList(array $ids): array
    {
        $loanProductList = array();

        foreach ($ids as $id) {
            $loanProductList[] = MockFactory::generateLoanProductObject($id);
        }

        return $loanProductList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateLoanProductObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $loanProductList = array();

        foreach ($ids as $id) {
            $loanProductList[] = MockFactory::generateLoanProductObject($id);
        }

        return $loanProductList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
