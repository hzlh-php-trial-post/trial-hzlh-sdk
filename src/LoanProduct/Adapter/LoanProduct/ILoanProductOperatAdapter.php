<?php
namespace Trial\LoanProduct\Adapter\LoanProduct;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;

interface ILoanProductOperatAdapter extends IOperatAbleAdapter, IResubmitAbleAdapter, IApplyAbleAdapter
{
}
