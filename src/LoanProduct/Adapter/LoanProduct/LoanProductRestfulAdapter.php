<?php
namespace Trial\LoanProduct\Adapter\LoanProduct;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Server;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

use Trial\LoanProduct\Model\LoanProduct;
use Trial\LoanProduct\Model\NullLoanProduct;

use Trial\LoanProduct\Translator\LoanProductRestfulTranslator;

class LoanProductRestfulAdapter extends GuzzleAdapter implements ILoanProductAdapter
{
    use CommonMapErrorsTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'OA_LOAN_PRODUCT_LIST' => [
            'fields' => [],
            'include' => 'enterprise,labels,guarantyStyles',
        ],
        'PORTAL_LOAN_PRODUCT_LIST' => [
            'fields' => [],
            'include' => 'enterprise,labels,guarantyStyles',
        ],
        'LOAN_PRODUCT_FETCH_ONE' => [
            'fields' => [],
            'include' => 'enterprise,snapshot,labels,guarantyStyles',
        ],
    ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new LoanProductRestfulTranslator();
        $this->resource = 'loanProducts';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullLoanProduct::getInstance());
    }

    public function portalFetchOne($id)
    {
        return $this->portalFetchOneAction($id, NullLoanProduct::getInstance());
    }

    protected function portalFetchOneAction(int $id, INull $null)
    {
        $header = array(
            'session_id' => session_id(),
            'ip' => Server::get('REMOTE_ADDR'),
            'member_id' => Core::$container->get('user')->getId()
        );

        $this->get(
            $this->getResource().'/'.$id,
            array(),
            $header
        );
        return $this->isSuccess() ? $this->translateToObject() : $null;
    }

    public function appFetchOne($id)
    {
        return $this->appFetchOneAction($id, NullLoanProduct::getInstance());
    }

    protected function appFetchOneAction(int $id, INull $null)
    {
        $header = array(
            'ip' => Server::get('REMOTE_ADDR'),
            'member_id' => Core::$container->get('user')->getId()
        );

        $this->get(
            $this->getResource().'/'.$id,
            array(),
            $header
        );
        return $this->isSuccess() ? $this->translateToObject() : $null;
    }

    public function tagSearch(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->tagSearchAction($filter, $sort, $number, $size);
    }

    protected function tagSearchAction(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        $header = array(
            'member_id' => isset($filter['tagMemberId']) ? $filter['tagMemberId'] : 0
        );

        $this->get(
            $this->getResource(),
            array(
                'filter'=>$filter,
                'sort'=>implode(',', $sort),
                'page'=>array('size'=>$size, 'number'=>$number)
            ),
            $header
        );
       
        return $this->isSuccess() ? $this->translateToObjects() : array(0, array());
    }

    /**
     * [addAction 发布贷款产品]
     * @param LoanProduct $loanProduct [object]
     * @return [type]           [bool]
     */
    protected function addAction(LoanProduct $loanProduct): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanProduct,
            array(
                'title',
                'cover',
                'tag',
                'introduction',
                'loanType',
                'associatedPolicies',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
                'enterprise',
                'againLoanType'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }

        return false;
    }

    /**
     * [editAction 编辑]
     * @param  LoanProduct $loanProduct [object]
     * @return [type]           [bool]
     */
    protected function editAction(LoanProduct $loanProduct): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanProduct,
            array(
                'title',
                'cover',
                'tag',
                'introduction',
                'loanType',
                'associatedPolicies',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
                'againLoanType'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $loanProduct->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }

        return false;
    }

    /**
    * [resubmitAction 重新认证]
    * @param  LoanProduct $loanProduct [object]
    * @return [type]           [bool]
    */
    protected function resubmitAction(LoanProduct $loanProduct): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanProduct,
            array(
                'title',
                'cover',
                'tag',
                'introduction',
                'loanType',
                'associatedPolicies',
                'productObject',
                'guarantyStyles',
                'labels',
                'minLoanPeriod',
                'maxLoanPeriod',
                'supportCity',
                'minLoanAmount',
                'maxLoanAmount',
                'minLoanTerm',
                'maxLoanTerm',
                'loanTermUnit',
                'loanInterestRate',
                'loanInterestRateUnit',
                'repaymentMethods',
                'isSupportEarlyRepayment',
                'earlyRepaymentTerm',
                'isExistEarlyRepaymentCost',
                'applicationMaterial',
                'applicationCondition',
                'contract',
                'application',
                'againLoanType'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $loanProduct->getId() . '/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }

        return false;
    }

    /**
     * [onShelf 上架贷款产品]
     * @param  LoanProduct $loanProduct [object]
     * @return [type]           [bool]
     */
    public function onShelf(LoanProduct $loanProduct): bool
    {
        return $this->onShelfAction($loanProduct);
    }

    protected function onShelfAction(LoanProduct $loanProduct): bool
    {
        $this->patch(
            $this->getResource() . '/' . $loanProduct->getId() . '/onShelf'
        );
        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }
        return false;
    }

    /**
     * [offStock 下架贷款产品]
     * @param  LoanProduct $loanProduct [object]
     * @return [type]           [bool]
     */
    public function offStock(LoanProduct $loanProduct): bool
    {
        return $this->offStockAction($loanProduct);
    }

    protected function offStockAction(LoanProduct $loanProduct): bool
    {
        $this->patch(
            $this->getResource() . '/' . $loanProduct->getId() . '/offStock'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanProduct);
            return true;
        }
        return false;
    }
}
