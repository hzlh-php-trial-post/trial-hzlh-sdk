<?php
namespace Trial\LoanProduct\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullApplyAbleTrait;
use Trial\Common\Model\NullOperatAbleTrait;
use Trial\Common\Model\NullResubmitAbleTrait;
use Trial\Common\Model\NullModifyStatusAbleTrait;

class NullLoanProduct extends LoanProduct implements INull
{
    use NullApplyAbleTrait,
        NullOperatAbleTrait,
        NullResubmitAbleTrait,
        NullModifyStatusAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist(): bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function onShelf(): bool
    {
        return $this->resourceNotExist();
    }

    public function offStock(): bool
    {
        return $this->resourceNotExist();
    }
}
