<?php
namespace Trial\PolicyInterpretation\Adapter\PolicyInterpretation;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\PolicyInterpretation\Model\PolicyInterpretation;

interface IPolicyInterpretationAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IOnShelfAbleAdapter
{

}
