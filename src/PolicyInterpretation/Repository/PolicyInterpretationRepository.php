<?php
namespace Trial\PolicyInterpretation\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\OnShelfAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\PolicyInterpretation\Adapter\PolicyInterpretation\IPolicyInterpretationAdapter;
use Trial\PolicyInterpretation\Adapter\PolicyInterpretation\PolicyInterpretationMockAdapter;
use Trial\PolicyInterpretation\Adapter\PolicyInterpretation\PolicyInterpretationRestfulAdapter;
use Trial\PolicyInterpretation\Model\PolicyInterpretation;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class PolicyInterpretationRepository extends Repository implements IPolicyInterpretationAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'POLICYINTERPRETATION_LIST';
    const FETCH_ONE_MODEL_UN = 'POLICYINTERPRETATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PolicyInterpretationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IPolicyInterpretationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IPolicyInterpretationAdapter
    {
        return new PolicyInterpretationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
