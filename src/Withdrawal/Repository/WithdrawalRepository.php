<?php
namespace Trial\Withdrawal\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;

use Trial\Withdrawal\Adapter\Withdrawal\IWithdrawalAdapter;
use Trial\Withdrawal\Adapter\Withdrawal\WithdrawalMockAdapter;
use Trial\Withdrawal\Adapter\Withdrawal\WithdrawalRestfulAdapter;

use Trial\Withdrawal\Model\Withdrawal;

class WithdrawalRepository extends Repository implements IWithdrawalAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_WITHDRAWAL_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_WITHDRAWAL_LIST';
    const FETCH_ONE_MODEL_UN = 'WITHDRAWAL_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new WithdrawalRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IWithdrawalAdapter
    {
        return new WithdrawalMockAdapter();
    }

    public function getActualAdapter(): IWithdrawalAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function withdraw(Withdrawal $withdrawal): bool
    {
        return $this->getAdapter()->withdraw($withdrawal);
    }

    public function transferCompleted(Withdrawal $withdrawal): bool
    {
        return $this->getAdapter()->transferCompleted($withdrawal);
    }
}
