<?php
namespace Trial\Withdrawal\Adapter\Withdrawal;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;

interface IWithdrawalAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
}
