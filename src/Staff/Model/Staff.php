<?php
namespace Trial\Staff\Model;

use Marmot\Core;

use Trial\User\Model\User;

use Trial\Staff\Repository\StaffRepository;

use Trial\Enterprise\Model\Enterprise;

use Trial\Position\Model\Position;

class Staff extends User
{
    const DEFAULT_BIRTHDAY = '0000-00-00';

    const EDUCATION = array(
        'NULL' => 0, //未知
        'PRIMARY_SCHOOL' => 1, //小学
        'JUNIOR_MIDDLE_SCHOOL' => 2, //初中
        'POLYTECHNIC_SCHOOL' => 3, //中专
        'JUNIOR_COLLEGE' => 4, //专科
        'UNDERGRADUATE' => 5, //本科
        'MASTER' => 6, //硕士研究生
        'DOCTOR' => 7 //博士研究生
    );

    const RECOMMEND_HOMEPAGE_STATUS = array(
        'NO' => 0,
        'YES' => 2
    );

    const TYPE = [
        'NORMAL' => 0, //普通员工
        'OWNERS' => 2 //企业主
    ];

    /**
     * [$birthday 出生日期]
     * @var [string]
     */
    private $birthday;
     /**
     * [$education 学历]
     * @var [int]
     */
    private $education;
     /**
     * [$cardId 身份证号]
     * @var [string]
     */
    private $cardId;
    /**
     * [$briefIntroduction 简介]
     * @var [string]
     */
    private $briefIntroduction;
    /**
     * [$isRecommendHomePage 是否推荐至主页]
     * @var [int]
     */
    private $isRecommendHomePage;
    /**
     * [$professionalTitle 职称]
     * @var [array]
     */
    protected $professionalTitle;
     /**
     * [$enterprise 所属企业]
     * @var [Object]
     */
    protected $roles;

    protected $enterprise;

    protected $positionType;

    protected $position;
    /**
     * [$repository]
     * @var [Object]
     */
    private $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->birthday = self::DEFAULT_BIRTHDAY;
        $this->education = self::EDUCATION['NULL'];
        $this->cardId = '';
        $this->briefIntroduction = '';
        $this->professionalTitle = '';
        $this->isRecommendHomePage = 0;
        $this->roles = '';
        $this->position = array();
        $this->positionType = 0;
        $this->enterprise = new Enterprise();
        $this->repository = new StaffRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->birthday);
        unset($this->education);
        unset($this->cardId);
        unset($this->briefIntroduction);
        unset($this->professionalTitle);
        unset($this->isRecommendHomePage);
        unset($this->roles);
        unset($this->position);
        unset($this->positionType);
        unset($this->enterprise);
        unset($this->repository);
    }

    public function setBirthday(string $birthday) : void
    {
        $this->birthday = $birthday;
    }

    public function getBirthday() : string
    {
        return $this->birthday;
    }
    
    public function setEducation(int $education) : void
    {
        $this->education = $education;
    }

    public function getEducation() : int
    {
        return $this->education;
    }

    public function setCardId(string $cardId) : void
    {
        $this->cardId = $cardId;
    }

    public function getCardId() : string
    {
        return $this->cardId;
    }

    public function setRoles(string $roles) : void
    {
        $this->roles = $roles;
    }

    public function getRoles() : string
    {
        return $this->roles;
    }

    public function setBriefIntroduction(string $briefIntroduction) : void
    {
        $this->briefIntroduction = $briefIntroduction;
    }

    public function getBriefIntroduction() : string
    {
        return $this->briefIntroduction;
    }

    public function setProfessionalTitle(array $professionalTitle) : void
    {
        $this->professionalTitle = $professionalTitle;
    }

    public function getProfessionalTitle() : array
    {
        return $this->professionalTitle;
    }

    public function setIsRecommendHomePage(int $isRecommendHomePage) : void
    {
        $this->isRecommendHomePage = $isRecommendHomePage;
    }

    public function getIsRecommendHomePage() : int
    {
        return $this->isRecommendHomePage;
    }
    
    public function setEnterprise(Enterprise $enterprise): void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise(): Enterprise
    {
        return $this->enterprise;
    }

    public function setPosition(array $position): void
    {
        $this->position = $position;
    }

    public function getPosition(): array
    {
        return $this->position;
    }

    public function setPositionType(int $positionType): void
    {
        $this->positionType = $positionType;
    }

    public function getPositionType(): int
    {
        return $this->positionType;
    }

    protected function getRepository() : StaffRepository
    {
        return $this->repository;
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }

    public function recommendHomePage() : bool
    {
        if (!$this->isRecommendHomePage()) {
            Core::setLastError(HAVE_RECOMMEND_TO_HOME_PAGE);
            return false;
        }

        return $this->getRepository()->recommendHomePage($this);
    }

    public function cancelRecommendHomePage() : bool
    {
        if (!$this->isNotRecommendHomePage()) {
            Core::setLastError(NOT_RECOMMEND_TO_HOME_PAGE);
            return false;
        }

        return $this->getRepository()->cancelRecommendHomePage($this);
    }

    public function distributionRole() : bool
    {
        return $this->getRepository()->distributionRole($this);
    }

    public function isRecommendHomePage() : bool
    {
        return $this->getIsRecommendHomePage() == self::RECOMMEND_HOMEPAGE_STATUS['YES'];
    }

    public function isNotRecommendHomePage() : bool
    {
        return $this->getIsRecommendHomePage() == self::RECOMMEND_HOMEPAGE_STATUS['NO'];
    }
}
