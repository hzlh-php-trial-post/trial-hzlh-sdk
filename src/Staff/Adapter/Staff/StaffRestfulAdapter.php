<?php
namespace Trial\Staff\Adapter\Staff;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Staff\Model\Staff;
use Trial\Staff\Model\NullStaff;
use Trial\Staff\Translator\StaffRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class StaffRestfulAdapter extends GuzzleAdapter implements IStaffAdapter
{
    use FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        EnableAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'STAFF_LIST'=>[
            'fields'=>[],
            'include' => 'enterprise,position',
        ],
        'STAFF_FETCH_ONE'=>[
            'fields'=>[],
            'include' => 'enterprise,position',
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new StaffRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'staffs';
    }

    protected function getResource()
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors(): array
    {
        $mapError = [
            100 => STAFF_CELLPHONE_EXIST
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullStaff::getInstance());
    }

    protected function addAction(Staff $staff) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $staff,
            array(
                'avatar',
                'realName',
                'cellphone',
                'cardId',
                'birthday',
                'briefIntroduction',
                'gender',
                'status',
                'education',
                'enterprise'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    protected function editAction(Staff $staff) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $staff,
            array(
                'avatar',
                'realName',
                'cellphone',
                'cardId',
                'birthday',
                'briefIntroduction',
                'gender',
                'status',
                'education',
            )
        );

        $this->patch(
            $this->getResource().'/'.$staff->getId(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    public function deletes(Staff $staff) : bool
    {
        $this->delete(
            $this->getResource().'/'.$staff->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    public function recommendHomePage(Staff $staff) : bool
    {
        $this->patch(
            $this->getResource().'/'.$staff->getId().'/recommendHomePage'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    public function cancelRecommendHomePage(Staff $staff) : bool
    {
        $this->patch(
            $this->getResource().'/'.$staff->getId().'/cancelRecommendHomePage'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    public function distributionRole(Staff $staff) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $staff,
            array('roles')
        );

        $this->patch(
            $this->getResource().'/'.$staff->getId().'/distributionPositionRoles',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }
}
