<?php
namespace Trial\Staff\Adapter\Staff;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\EnableAbleMockAdapterTrait;

use Trial\Staff\Model\Staff;
use Trial\Staff\Utils\MockFactory;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class StaffMockAdapter implements IStaffAdapter
{
    use OperatAbleMockAdapterTrait, EnableAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateStaffObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $staffList = array();

        foreach ($ids as $id) {
            $staffList[] = MockFactory::generateStaffObject($id);
        }

        return $staffList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateStaffObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $staffList = array();

        foreach ($ids as $id) {
            $staffList[] = MockFactory::generateStaffObject($id);
        }

        return $staffList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
