<?php
namespace Trial\Staff\Adapter\Staff;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Staff\Model\Staff;

use Marmot\Interfaces\IAsyncAdapter;

interface IStaffAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter
{

}
