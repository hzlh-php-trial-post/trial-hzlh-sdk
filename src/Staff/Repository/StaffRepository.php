<?php
namespace Trial\Staff\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Staff\Adapter\Staff\IStaffAdapter;
use Trial\Staff\Adapter\Staff\StaffMockAdapter;
use Trial\Staff\Adapter\Staff\StaffRestfulAdapter;
use Trial\Staff\Model\Staff;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class StaffRepository extends Repository implements IStaffAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'STAFF_LIST';
    const FETCH_ONE_MODEL_UN = 'STAFF_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new StaffRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IStaffAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IStaffAdapter
    {
        return new StaffMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(Staff $staff) : bool
    {
        return $this->getAdapter()->deletes($staff);
    }

    public function recommendHomePage(Staff $staff) : bool
    {
        return $this->getAdapter()->recommendHomePage($staff);
    }

    public function cancelRecommendHomePage(Staff $staff) : bool
    {
        return $this->getAdapter()->cancelRecommendHomePage($staff);
    }

    public function distributionRole(Staff $staff) : bool
    {
        return $this->getAdapter()->distributionRole($staff);
    }
}
