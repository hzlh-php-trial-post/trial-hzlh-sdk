<?php
namespace Trial\PlatformAccount\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\PlatformAccount\Adapter\PlatformAccount\IPlatformAccountAdapter;
use Trial\PlatformAccount\Adapter\PlatformAccount\PlatformAccountMockAdapter;
use Trial\PlatformAccount\Adapter\PlatformAccount\PlatformAccountRestfulAdapter;

use Trial\PlatformAccount\Model\PlatformAccount;

class PlatformAccountRepository extends Repository implements IPlatformAccountAdapter
{
    private $adapter;

    const FETCH_ONE_MODEL_UN = 'PLATFORM_ACCOUNT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PlatformAccountRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IPlatformAccountAdapter
    {
        return new PlatformAccountMockAdapter();
    }

    public function getActualAdapter(): IPlatformAccountAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function fetchPlatformAccount() : PlatformAccount
    {
        return $this->getAdapter()->fetchPlatformAccount();
    }
}
