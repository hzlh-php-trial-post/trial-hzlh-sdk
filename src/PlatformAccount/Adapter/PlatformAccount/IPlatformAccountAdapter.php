<?php
namespace Trial\PlatformAccount\Adapter\PlatformAccount;

use Trial\PlatformAccount\Model\PlatformAccount;

interface IPlatformAccountAdapter
{
    public function fetchPlatformAccount() : PlatformAccount;
}
