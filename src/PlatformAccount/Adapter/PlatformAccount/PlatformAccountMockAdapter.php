<?php
namespace Trial\PlatformAccount\Adapter\PlatformAccount;

use Trial\PlatformAccount\Model\PlatformAccount;
use Trial\PlatformAccount\Utils\MockFactory;

class PlatformAccountMockAdapter implements IPlatformAccountAdapter
{
    public function fetchPlatformAccount() : PlatformAccount
    {
        return MockFactory::generatePlatformAccountObject();
    }
}
