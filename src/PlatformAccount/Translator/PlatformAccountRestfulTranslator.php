<?php
namespace Trial\PlatformAccount\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\PlatformAccount\Model\PlatformAccount;
use Trial\PlatformAccount\Model\NullPlatformAccount;

class PlatformAccountRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $platformAccount = null)
    {
        return $this->translateToObject($expression, $platformAccount);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $platformAccount = null)
    {
        if (empty($expression)) {
            return NullPlatformAccount::getInstance();
        }

        if ($platformAccount == null) {
            $platformAccount = new PlatformAccount();
        }

        $data = $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['accountBalance'])) {
            $platformAccount->setAccountBalance($attributes['accountBalance']);
        }
        if (isset($attributes['incomeAmount'])) {
            $platformAccount->setIncomeAmount($attributes['incomeAmount']);
        }
        if (isset($attributes['expenditureAmount'])) {
            $platformAccount->setExpenditureAmount($attributes['expenditureAmount']);
        }
        if (isset($attributes['profitAmount'])) {
            $platformAccount->setProfitAmount($attributes['profitAmount']);
        }

        return $platformAccount;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($platformAccount, array $keys = array())
    {
        unset($platformAccount);
        unset($keys);

        return [];
    }
}
