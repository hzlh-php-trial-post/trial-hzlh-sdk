<?php
namespace Trial\Appointment\Adapter\Appointment;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

use Trial\Appointment\Model\Appointment;
use Trial\Appointment\Model\NullAppointment;

use Trial\Appointment\Translator\AppointmentRestfulTranslator;

class AppointmentRestfulAdapter extends GuzzleAdapter implements IAppointmentAdapter
{
    use CommonMapErrorsTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'APPOINTMENT_LIST' => [
            'fields' => [],
            'include' => 'loanProduct.labels,loanProductSnapshot,applicant,sellerEnterprise,guarantyStyle,guarantyStyleType,loanPurpose,transactionInfo,loanResultInfo'//phpcs:ignore
        ],
        'APPOINTMENT_FETCH_ONE' => [
            'fields' => [],
            'include' => 'loanProduct.labels,loanProductSnapshot,member,applicant,sellerEnterprise,guarantyStyle,guarantyStyleType,loanPurpose,transactionInfo,loanResultInfo,rejectReason'//phpcs:ignore
        ],
    ];
    
    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new AppointmentRestfulTranslator();
        $this->resource = 'appointments';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            111 => APPOINTMENT_NEED_NATURAL_PERSON_AUTHENTICATION,
            112 => APPOINTMENT_NEED_ENTERPRISE_AUTHENTICATION,
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullAppointment::getInstance());
    }

    /**
     * [addAction 在线申请]
     * @param Appointment $appointment [object]
     * @return [type][bool]
     */
    protected function addAction(Appointment $appointment): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array(
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress',
                'loanAmount',
                'loanTerm',
                'loanObject',
                'loanPurposeDescribe',
                'attachments',
                'creditReports',
                'authorizedReports',
                'loanProductSnapshot',
                'member',
                'guarantyStyle',
                'guarantyStyleType',
                'loanPurpose'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    /**
    * [resubmitAction 重新提交申请]
    * @param  Appointment $appointment [object]
    * @return [type][bool]
    */
    protected function resubmitAction(Appointment $appointment): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array(
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress',
                'loanAmount',
                'loanTerm',
                'loanObject',
                'loanPurposeDescribe',
                'attachments',
                'creditReports',
                'authorizedReports',
                'guarantyStyle',
                'guarantyStyleType',
                'loanPurpose'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $appointment->getId() . '/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    /**
     * [approve 审核通过]
     * @param  Appointment $appointment [object]
     * @return [type][bool]
     */
    protected function approveAction(Appointment $appointment) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array('transactionInfo')
        );

        $this->patch(
            $this->getResource() . '/' . $appointment->getId() . '/approve',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    /**
     * [reject 审核驳回]
     * @param  Appointment $appointment [object]
     * @return [type][bool]
     */
    protected function rejectAction(Appointment $appointment) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array(
                'rejectReason',
                'rejectReasonDescribe'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $appointment->getId() . '/reject',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    /**
     * [completed 完成]
     * @param  Appointment $appointment [object]
     * @return [type][bool]
     */
    public function completed(Appointment $appointment) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $appointment,
            array(
                'loanResultInfo'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $appointment->getId() . '/completed',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($appointment);
            return true;
        }

        return false;
    }

    protected function editAction(Appointment $appointment): bool
    {
        unset($appointment);
        return false;
    }
}
