<?php
namespace Trial\Appointment\Adapter\Appointment;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IModifyStatusAbleAdapter;

interface IAppointmentAdapter extends IAsyncAdapter, IFetchAbleAdapter, IAppointmentOperatAdapter, IModifyStatusAbleAdapter//phpcs:ignore
{
}
