<?php
namespace Trial\Appointment\Adapter\Appointment;

use Trial\Common\Adapter\ApplyAbleMockAdapterTrait;
use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleMockAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleMockAdapterTrait;

use Trial\Appointment\Utils\MockFactory;

class AppointmentMockAdapter implements IAppointmentAdapter
{
    use OperatAbleMockAdapterTrait,
        ApplyAbleMockAdapterTrait,
        ModifyStatusAbleMockAdapterTrait,
        ResubmitAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateAppointmentObject($id);
    }

    public function fetchList(array $ids): array
    {
        $appointmentList = array();

        foreach ($ids as $id) {
            $appointmentList[] = MockFactory::generateAppointmentObject($id);
        }

        return $appointmentList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateAppointmentObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $appointmentList = array();

        foreach ($ids as $id) {
            $appointmentList[] = MockFactory::generateAppointmentObject($id);
        }

        return $appointmentList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
