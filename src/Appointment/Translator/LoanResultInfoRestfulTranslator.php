<?php
namespace Trial\Appointment\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Appointment\Model\LoanResultInfo;
use Trial\Appointment\Model\NullLoanResultInfo;
use Trial\Common\Translator\RestfulTranslatorTrait;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class LoanResultInfoRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $loanResultInfo = null)
    {
        return $this->translateToObject($expression, $loanResultInfo);
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $loanResultInfo = null)
    {
        if (empty($expression)) {
            return NullLoanResultInfo::getInstance();
        }

        if ($loanResultInfo == null) {
            $loanResultInfo = new LoanResultInfo();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $loanResultInfo->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['repaymentMethod'])) {
            $loanResultInfo->setRepaymentMethod($attributes['repaymentMethod']);
        }
        if (isset($attributes['loanAmount'])) {
            $loanResultInfo->setLoanAmount($attributes['loanAmount']);
        }
        if (isset($attributes['loanTerm'])) {
            $loanResultInfo->setLoanTerm($attributes['loanTerm']);
        }
        if (isset($attributes['loanTermUnit'])) {
            $loanResultInfo->setLoanTermUnit($attributes['loanTermUnit']);
        }
        if (isset($attributes['loanFailReason'])) {
            $loanResultInfo->setLoanFailReason($attributes['loanFailReason']);
        }
        if (isset($attributes['status'])) {
            $loanResultInfo->setLoanResultStatus($attributes['status']);
        }

        return $loanResultInfo;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($loanResultInfo, array $keys = array())
    {
        if (!$loanResultInfo instanceof LoanResultInfo) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'loanResultInfoStatus',
                'repaymentMethod',
                'loanAmount',
                'loanTerm',
                'loanTermUnit',
                'loanFailReason'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'loanResultInfos'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $loanResultInfo->getId();
        }

        $attributes = array();

        if (in_array('loanResultInfoStatus', $keys)) {
            $attributes['status'] = $loanResultInfo->getLoanResultStatus();
        }
        if (in_array('repaymentMethod', $keys)) {
            $attributes['repaymentMethod'] = $loanResultInfo->getRepaymentMethod();
        }
        if (in_array('loanAmount', $keys)) {
            $attributes['loanAmount'] = $loanResultInfo->getLoanAmount();
        }
        if (in_array('loanTerm', $keys)) {
            $attributes['loanTerm'] = $loanResultInfo->getLoanTerm();
        }
        if (in_array('loanTermUnit', $keys)) {
            $attributes['loanTermUnit'] = $loanResultInfo->getLoanTermUnit();
        }
        if (in_array('loanFailReason', $keys)) {
            $attributes['loanFailReason'] = $loanResultInfo->getLoanFailReason();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
