<?php
namespace Trial\Appointment\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Appointment\Model\Appointment;
use Trial\TradeRecord\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        Appointment::LOAN_OBJECT['ENTERPRISE'] =>
        'Trial\Enterprise\Translator\EnterpriseRestfulTranslator',
        Appointment::LOAN_OBJECT['NATURAL_PERSON'] =>
        'Trial\NaturalPerson\Translator\NaturalPersonRestfulTranslator'
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
