<?php
namespace Trial\Appointment\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;

use Trial\Member\Translator\MemberRestfulTranslator;

use Trial\LoanProduct\Translator\LoanProductRestfulTranslator;

use Trial\Snapshot\Translator\SnapshotRestfulTranslator;

use Trial\Dictionary\Translator\DictionaryRestfulTranslator;

use Trial\Appointment\Model\Appointment;
use Trial\Appointment\Model\ContactsInfo;
use Trial\Appointment\Model\NullAppointment;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class AppointmentRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function getLoanProductRestfulTranslator()
    {
        return new LoanProductRestfulTranslator();
    }

    public function getSnapshotRestfulTranslator()
    {
        return new SnapshotRestfulTranslator();
    }

    public function getLoanResultInfoRestfulTranslator()
    {
        return new LoanResultInfoRestfulTranslator();
    }

    public function getTransactionInfoRestfulTranslator()
    {
        return new TransactionInfoRestfulTranslator();
    }

    public function getDictionaryRestfulTranslator()
    {
        return new DictionaryRestfulTranslator();
    }

    public function arrayToObject(array $expression, $appointment = null)
    {
        return $this->translateToObject($expression, $appointment);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $appointment = null)
    {
        if (empty($expression)) {
            return NullAppointment::getInstance();
        }

        if ($appointment == null) {
            $appointment = new Appointment();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $appointment->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['number'])) {
            $appointment->setNumber($attributes['number']);
        }
        if (isset($attributes['score'])) {
            $appointment->setScore($attributes['score']);
        }
        if (isset($attributes['loanObject'])) {
            $appointment->setLoanObject($attributes['loanObject']);
        }
        if (isset($attributes['loanProductTitle'])) {
            $appointment->setLoanProductTitle($attributes['loanProductTitle']);
        }
        if (isset($attributes['enterpriseName'])) {
            $appointment->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['borrowerName'])) {
            $appointment->setBorrowerName($attributes['borrowerName']);
        }
        if (isset($attributes['loanAmount'])) {
            $appointment->setLoanAmount($attributes['loanAmount']);
        }
        if (isset($attributes['loanTerm'])) {
            $appointment->setLoanTerm($attributes['loanTerm']);
        }
        if (isset($attributes['loanTermUnit'])) {
            $appointment->setLoanTermUnit($attributes['loanTermUnit']);
        }
        if (isset($attributes['loanPurposeDescribe'])) {
            $appointment->setLoanPurposeDescribe($attributes['loanPurposeDescribe']);
        }
        if (isset($attributes['attachments'])) {
            $appointment->setAttachments($attributes['attachments']);
        }
        if (isset($attributes['creditReports'])) {
            $appointment->setCreditReports($attributes['creditReports']);
        }
        if (isset($attributes['authorizedReports'])) {
            $appointment->setAuthorizedReports($attributes['authorizedReports']);
        }
        if (isset($attributes['status'])) {
            $appointment->setStatus($attributes['status']);
        }
        if (isset($attributes['rejectReasonDescribe'])) {
            $appointment->setRejectReasonDescribe($attributes['rejectReasonDescribe']);
        }
        if (isset($attributes['createTime'])) {
            $appointment->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $appointment->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $appointment->setStatusTime($attributes['statusTime']);
        }
        //联系人信息
        $contactsName = isset($attributes['contactsName']) ? $attributes['contactsName'] : '';
        $contactsCellphone = isset($attributes['contactsCellphone']) ? $attributes['contactsCellphone'] : '';
        $contactsArea = isset($attributes['contactsArea']) ? $attributes['contactsArea'] : '';
        $contactsAddress = isset($attributes['contactsAddress']) ? $attributes['contactsAddress'] : '';
        $appointment->setContactsInfo(
            new ContactsInfo(
                $contactsName,
                $contactsCellphone,
                $contactsArea,
                $contactsAddress
            )
        );

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['loanProductSnapshot']['data'])) {
            $loanProductSnapshot = $this->changeArrayFormat($relationships['loanProductSnapshot']['data']);
            $appointment->setLoanProductSnapshot(
                $this->getSnapshotRestfulTranslator()->arrayToObject($loanProductSnapshot)
            );
        }
        if (isset($relationships['loanProduct']['data'])) {
            $loanProduct = $this->changeArrayFormat($relationships['loanProduct']['data']);
            $appointment->setLoanProduct(
                $this->getLoanProductRestfulTranslator()->arrayToObject($loanProduct)
            );
        }
        if (isset($relationships['sellerEnterprise']['data'])) {
            $sellerEnterprise = $this->changeArrayFormat($relationships['sellerEnterprise']['data']);
            $appointment->setSellerEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($sellerEnterprise)
            );
        }
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $appointment->setMember(
                $this->getMemberRestfulTranslator()->arrayToObject($member)
            );
        }
        if (isset($relationships['applicant']['data'])) {
            if (isset($expression['included'])) {
                $applicant = $this->changeArrayFormat($relationships['applicant']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $applicant = $this->changeArrayFormat($relationships['applicant']['data']);
            }

            $applicantRestfulTranslator = $this->getTranslatorFactory()->getTranslator($attributes['loanObject']);
            $appointment->setApplicant($applicantRestfulTranslator->arrayToObject($applicant));
        }
        if (isset($relationships['guarantyStyle']['data'])) {
            $guarantyStyle = $this->changeArrayFormat($relationships['guarantyStyle']['data']);
            $appointment->setGuarantyStyle(
                $this->getDictionaryRestfulTranslator()->arrayToObject($guarantyStyle)
            );
        }
        if (isset($relationships['guarantyStyleType']['data'])) {
            $guarantyStyleType = $this->changeArrayFormat($relationships['guarantyStyleType']['data']);
            $appointment->setGuarantyStyleType(
                $this->getDictionaryRestfulTranslator()->arrayToObject($guarantyStyleType)
            );
        }
        if (isset($relationships['loanPurpose']['data'])) {
            $loanPurpose = $this->changeArrayFormat($relationships['loanPurpose']['data']);
            $appointment->setLoanPurpose(
                $this->getDictionaryRestfulTranslator()->arrayToObject($loanPurpose)
            );
        }
        if (isset($relationships['rejectReason']['data'])) {
            $rejectReason = $this->changeArrayFormat($relationships['rejectReason']['data']);
            $appointment->setRejectReason(
                $this->getDictionaryRestfulTranslator()->arrayToObject($rejectReason)
            );
        }
        if (isset($relationships['loanResultInfo']['data'])) {
            $loanResultInfo = $this->changeArrayFormat($relationships['loanResultInfo']['data']);
            $appointment->setLoanResultInfo(
                $this->getLoanResultInfoRestfulTranslator()->arrayToObject($loanResultInfo)
            );
        }
        if (isset($relationships['transactionInfo']['data'])) {
            $transactionInfo = $this->changeArrayFormat($relationships['transactionInfo']['data']);
            $appointment->setTransactionInfo(
                $this->getTransactionInfoRestfulTranslator()->arrayToObject($transactionInfo)
            );
        }

        return $appointment;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($appointment, array $keys = array())
    {
        if (!$appointment instanceof Appointment) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'loanProductSnapshot',
                'loanProduct',
                'sellerEnterprise',
                'applicant',
                'member',
                'loanObject',
                'loanAmount',
                'loanTerm',
                'loanTermUnit',
                'guarantyStyle',
                'guarantyStyleType',
                'loanPurpose',
                'loanPurposeDescribe',
                'attachments',
                'creditReports',
                'authorizedReports',
                'rejectReason',
                'rejectReasonDescribe',
                'loanResultInfo',
                'transactionInfo',
                'contactsName',
                'contactsCellphone',
                'contactsArea',
                'contactsAddress'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'appointments'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $appointment->getId();
        }

        $attributes = array();

        if (in_array('contactsName', $keys)) {
            $attributes['contactsName'] = $appointment->getContactsInfo()->getName();
        }
        if (in_array('contactsCellphone', $keys)) {
            $attributes['contactsCellphone'] = $appointment->getContactsInfo()->getCellphone();
        }
        if (in_array('contactsArea', $keys)) {
            $attributes['contactsArea'] = $appointment->getContactsInfo()->getArea();
        }
        if (in_array('contactsAddress', $keys)) {
            $attributes['contactsAddress'] = $appointment->getContactsInfo()->getAddress();
        }
        if (in_array('loanAmount', $keys)) {
            $attributes['loanAmount'] = $appointment->getLoanAmount();
        }
        if (in_array('loanTerm', $keys)) {
            $attributes['loanTerm'] = $appointment->getLoanTerm();
        }
        if (in_array('loanObject', $keys)) {
            $attributes['loanObject'] = $appointment->getLoanObject();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachments'] = $appointment->getAttachments();
        }
        if (in_array('loanPurposeDescribe', $keys)) {
            $attributes['loanPurposeDescribe'] = $appointment->getLoanPurposeDescribe();
        }
        if (in_array('creditReports', $keys)) {
            $attributes['creditReports'] = $appointment->getCreditReports();
        }
        if (in_array('authorizedReports', $keys)) {
            $attributes['authorizedReports'] = $appointment->getAuthorizedReports();
        }
        if (in_array('rejectReasonDescribe', $keys)) {
            $attributes['rejectReasonDescribe'] = $appointment->getRejectReasonDescribe();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('loanProductSnapshot', $keys)) {
            $expression['data']['relationships']['snapshot']['data'] = array(
                array(
                    'type'=>'snapshots',
                    'id'=>$appointment->getLoanProductSnapshot()->getId()
                )
            );
        }

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type'=>'members',
                    'id'=>$appointment->getMember()->getId()
                )
            );
        }

        if (in_array('guarantyStyle', $keys)) {
            $expression['data']['relationships']['guarantyStyle']['data'] = array(
                array(
                    'type'=>'dictionaries',
                    'id'=>$appointment->getGuarantyStyle()->getId()
                )
            );
        }

        if (in_array('guarantyStyleType', $keys)) {
            $expression['data']['relationships']['guarantyStyleType']['data'] = array(
                array(
                    'type'=>'dictionaries',
                    'id'=>$appointment->getGuarantyStyleType()->getId()
                )
            );
        }

        if (in_array('loanPurpose', $keys)) {
            $expression['data']['relationships']['loanPurpose']['data'] = array(
                array(
                    'type'=>'dictionaries',
                    'id'=>$appointment->getLoanPurpose()->getId()
                )
            );
        }

        if (in_array('rejectReason', $keys)) {
            $expression['data']['relationships']['rejectReason']['data'] = array(
                array(
                    'type'=>'dictionaries',
                    'id'=>$appointment->getRejectReason()->getId()
                )
            );
        }

        if (in_array('loanResultInfo', $keys)) {
            $expression['data']['relationships']['loanResultInfo'] =
                $this->getLoanResultInfoRestfulTranslator()->objectToArray($appointment->getLoanResultInfo());
        }

        if (in_array('transactionInfo', $keys)) {
            $expression['data']['relationships']['transactionInfo'] =
                $this->getTransactionInfoRestfulTranslator()->objectToArray($appointment->getTransactionInfo());
        }

        return $expression;
    }
}
