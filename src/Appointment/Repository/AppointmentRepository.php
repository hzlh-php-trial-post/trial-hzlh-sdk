<?php
namespace Trial\Appointment\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ApplyAbleRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ResubmitAbleRepositoryTrait;
use Trial\Common\Repository\ModifyStatusAbleRepositoryTrait;

use Trial\Appointment\Adapter\Appointment\IAppointmentAdapter;
use Trial\Appointment\Adapter\Appointment\AppointmentMockAdapter;
use Trial\Appointment\Adapter\Appointment\AppointmentRestfulAdapter;

use Trial\Appointment\Model\Appointment;

class AppointmentRepository extends Repository implements IAppointmentAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ApplyAbleRepositoryTrait,
        OperatAbleRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'APPOINTMENT_LIST';
    const PORTAL_LIST_MODEL_UN = 'APPOINTMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'APPOINTMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new AppointmentRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IAppointmentAdapter
    {
        return new AppointmentMockAdapter();
    }

    public function getActualAdapter(): IAppointmentAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function completed(Appointment $appointment) : bool
    {
        return $this->getAdapter()->completed($appointment);
    }
}
