<?php
namespace Trial\Appointment\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullOperatAbleTrait;

class NullAppointment extends Appointment implements INull
{
    use NullOperatAbleTrait;
    
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
