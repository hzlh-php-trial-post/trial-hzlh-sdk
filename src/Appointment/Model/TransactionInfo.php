<?php
namespace Trial\Appointment\Model;

class TransactionInfo
{
    private $id;

    private $creditMinLine;

    private $creditMaxLine;

    private $creditValidityStartTime;

    private $creditValidityEndTime;

    private $transactionTime;

    private $transactionArea;

    private $transactionAddress;

    private $contactName;

    private $contactPhone;

    private $remark;

    private $carryData;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->creditMinLine = 0.00;
        $this->creditMaxLine = 0.00;
        $this->creditValidityStartTime = 0;
        $this->creditValidityEndTime = 0;
        $this->transactionTime = '';
        $this->transactionArea = '';
        $this->transactionAddress = '';
        $this->contactName = '';
        $this->contactPhone = '';
        $this->remark = '';
        $this->carryData = array();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->creditMinLine);
        unset($this->creditMaxLine);
        unset($this->creditValidityStartTime);
        unset($this->creditValidityEndTime);
        unset($this->transactionTime);
        unset($this->transactionArea);
        unset($this->transactionAddress);
        unset($this->contactName);
        unset($this->contactPhone);
        unset($this->remark);
        unset($this->carryData);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCreditMinLine(float $creditMinLine) : void
    {
        $this->creditMinLine = $creditMinLine;
    }

    public function getCreditMinLine() : float
    {
        return $this->creditMinLine;
    }

    public function setCreditMaxLine(float $creditMaxLine) : void
    {
        $this->creditMaxLine = $creditMaxLine;
    }

    public function getCreditMaxLine() : float
    {
        return $this->creditMaxLine;
    }

    public function setCreditValidityStartTime(int $creditValidityStartTime) : void
    {
        $this->creditValidityStartTime = $creditValidityStartTime;
    }

    public function getCreditValidityStartTime() : int
    {
        return $this->creditValidityStartTime;
    }

    public function setCreditValidityEndTime(int $creditValidityEndTime) : void
    {
        $this->creditValidityEndTime = $creditValidityEndTime;
    }

    public function getCreditValidityEndTime() : int
    {
        return $this->creditValidityEndTime;
    }

    public function setTransactionArea(string $transactionArea) : void
    {
        $this->transactionArea = $transactionArea;
    }

    public function getTransactionArea() : string
    {
        return $this->transactionArea;
    }

    public function setTransactionAddress(string $transactionAddress) : void
    {
        $this->transactionAddress = $transactionAddress;
    }

    public function getTransactionAddress() : string
    {
        return $this->transactionAddress;
    }

    public function setTransactionTime(string $transactionTime) : void
    {
        $this->transactionTime = $transactionTime;
    }

    public function getTransactionTime() : string
    {
        return $this->transactionTime;
    }

    public function setContactName(string $contactName) : void
    {
        $this->contactName = $contactName;
    }

    public function getContactName() : string
    {
        return $this->contactName;
    }

    public function setContactPhone(string $contactPhone) : void
    {
        $this->contactPhone = $contactPhone;
    }

    public function getContactPhone() : string
    {
        return $this->contactPhone;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setCarryData(array $carryData) : void
    {
        $this->carryData = $carryData;
    }

    public function getCarryData() : array
    {
        return $this->carryData;
    }
}
