<?php
namespace Trial\Appointment\Model;

use Trial\LoanProduct\Model\LoanProduct;

class LoanResultInfo
{
    const LOAN_RESULT_STATUS = array(
        'PENDING' => 0, //待审核
        'SUCCESS' => 2, //成功
        'FAIL' => -2 //失败
    );

    private $id;

    private $loanResultStatus;

    private $repaymentMethod;

    private $loanAmount;

    private $loanTerm;

    private $loanTermUnit;

    private $loanFailReason;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->loanResultStatus = self::LOAN_RESULT_STATUS['PENDING'];
        $this->repaymentMethod = LoanProduct::REPAYMENT_METHOD['AVERAGE_CAPITAL_PLUS_INTEREST'];
        $this->loanAmount = 0.00;
        $this->loanTerm = 0;
        $this->loanTermUnit = LoanProduct::LOAN_TERM_UNIT['MONTH'];
        $this->loanFailReason = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->loanResultStatus);
        unset($this->repaymentMethod);
        unset($this->loanAmount);
        unset($this->loanTerm);
        unset($this->loanTermUnit);
        unset($this->loanFailReason);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setLoanResultStatus(string $loanResultStatus) : void
    {
        $this->loanResultStatus = $loanResultStatus;
    }

    public function getLoanResultStatus() : string
    {
        return $this->loanResultStatus;
    }

    public function setRepaymentMethod(int $repaymentMethod) : void
    {
        $this->repaymentMethod = $repaymentMethod;
    }

    public function getRepaymentMethod() : int
    {
        return $this->repaymentMethod;
    }

    public function setLoanAmount(float $loanAmount) : void
    {
        $this->loanAmount = $loanAmount;
    }

    public function getLoanAmount() : float
    {
        return $this->loanAmount;
    }

    public function setLoanTerm(int $loanTerm) : void
    {
        $this->loanTerm = $loanTerm;
    }

    public function getLoanTerm() : int
    {
        return $this->loanTerm;
    }

    public function setLoanTermUnit(int $loanTermUnit) : void
    {
        $this->loanTermUnit = $loanTermUnit;
    }

    public function getLoanTermUnit() : int
    {
        return $this->loanTermUnit;
    }

    public function setLoanFailReason(string $loanFailReason) : void
    {
        $this->loanFailReason = $loanFailReason;
    }

    public function getLoanFailReason() : string
    {
        return $this->loanFailReason;
    }
}
