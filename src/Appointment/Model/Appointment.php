<?php
namespace Trial\Appointment\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;
use Trial\Common\Adapter\IModifyStatusAbleAdapter;
use Trial\Common\Model\IApplyAble;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IResubmitAble;
use Trial\Common\Model\IModifyStatusAble;
use Trial\Common\Model\ApplyAbleTrait;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\ResubmitAbleTrait;
use Trial\Common\Model\ModifyStatusAbleTrait;

use Trial\Snapshot\Model\Snapshot;
use Trial\Snapshot\Model\ISnapshotAble;

use Trial\Member\Model\Member;
use Trial\Enterprise\Model\NullEnterprise;
use Trial\Enterprise\Model\Enterprise;
use Trial\LoanProduct\Model\LoanProduct;
use Trial\Appointment\Repository\AppointmentRepository;

use Trial\Dictionary\Model\Dictionary;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class Appointment implements IObject, IApplyAble, IOperatAble, IResubmitAble, IModifyStatusAble, ISnapshotAble
{
    use Object, ValidateTrait, ApplyAbleTrait, OperatAbleTrait, ResubmitAbleTrait, ModifyStatusAbleTrait;

    //状态
    const APPOINTMENT_STATUS = array(
        'NORMAL' => 0,     // 正常
        'REJECT' => -2,    // 已驳回
        'APPROVE' => 2,    // 已通过
        'REVOKED' => -4,   // 撤销
        'COMPLETED_LOAN_SUCCESS' => 4,  // 完成-贷款成功
        'COMPLETED_LOAN_FAIL' => 6,  // 完成-贷款失败
        'DELETED' => -6,    // 驳回/完成删除
        'REVOKED_DELETED' => -8  // 撤销删除
    );

    //贷款期限单位
    const LOAN_TERM_UNIT = array(
        'MONTH' => 1,  // 月
        'DAY' => 2     // 天
    );

    //贷款对象
    const LOAN_OBJECT = array(
        'ENTERPRISE' => 1,     // 企业
        'NATURAL_PERSON' => 2  // 个人
    );

    const AUTHORIZED_REPORTS = array(
        'STATEMENT_OF_FINANCIAL_POSITION' => 1, //资产负债表
        'STATEMENT_OF_CASH_FLOWS' => 2, //现金流量表
        'INCOME_STATEMENT' => 3 //利润表
    );
    
    private $id;

    private $number;

    private $score;

    private $loanProductSnapshot;

    private $loanProduct;

    private $loanProductTitle;

    private $enterpriseName;

    private $member;

    private $sellerEnterprise;

    private $borrowerName;

    private $applicant;

    private $loanObject;

    private $loanAmount;

    private $loanTerm;

    private $loanTermUnit;

    private $guarantyStyle;

    private $guarantyStyleType;

    private $loanPurpose;

    private $loanPurposeDescribe;

    private $attachments;

    private $creditReports;

    private $authorizedReports;

    private $contactsInfo;

    private $transactionInfo;

    private $loanResultInfo;

    private $rejectReason;

    private $rejectReasonDescribe;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = '';
        $this->score = 0;
        $this->loanProductSnapshot = new Snapshot();
        $this->loanProduct = new LoanProduct();
        $this->loanProductTitle = '';
        $this->enterpriseName = '';
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->sellerEnterprise = new Enterprise();
        $this->borrowerName = '';
        $this->applicant = new NullEnterprise();
        $this->loanObject = self::LOAN_OBJECT['ENTERPRISE'];
        $this->loanAmount = 0.00;
        $this->loanTerm = 0;
        $this->loanTermUnit = LoanProduct::LOAN_TERM_UNIT['MONTH'];
        $this->guarantyStyle = new Dictionary();
        $this->guarantyStyleType = new Dictionary();
        $this->loanPurpose = new Dictionary();
        $this->loanPurposeDescribe = '';
        $this->attachments = array();
        $this->creditReports = array();
        $this->authorizedReports = array();
        $this->contactsInfo = new ContactsInfo();
        $this->transactionInfo = new TransactionInfo();
        $this->loanResultInfo = new LoanResultInfo();
        $this->rejectReason = new Dictionary();
        $this->rejectReasonDescribe = '';
        $this->status = self::APPOINTMENT_STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new AppointmentRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->score);
        unset($this->loanProductSnapshot);
        unset($this->loanProduct);
        unset($this->loanProductTitle);
        unset($this->enterpriseName);
        unset($this->member);
        unset($this->sellerEnterprise);
        unset($this->borrowerName);
        unset($this->applicant);
        unset($this->loanObject);
        unset($this->loanAmount);
        unset($this->loanTerm);
        unset($this->loanTermUnit);
        unset($this->guarantyStyle);
        unset($this->guarantyStyleType);
        unset($this->loanPurpose);
        unset($this->loanPurposeDescribe);
        unset($this->attachments);
        unset($this->creditReports);
        unset($this->authorizedReports);
        unset($this->contactsInfo);
        unset($this->transactionInfo);
        unset($this->loanResultInfo);
        unset($this->rejectReason);
        unset($this->rejectReasonDescribe);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setScore(int $score) : void
    {
        $this->score = $score;
    }

    public function getScore() : int
    {
        return $this->score;
    }

    public function setLoanProductSnapshot(Snapshot $loanProductSnapshot) : void
    {
        $this->loanProductSnapshot = $loanProductSnapshot;
    }

    public function getLoanProductSnapshot() : Snapshot
    {
        return $this->loanProductSnapshot;
    }

    public function setLoanProduct(LoanProduct $loanProduct) : void
    {
        $this->loanProduct = $loanProduct;
    }

    public function getLoanProduct() : LoanProduct
    {
        return $this->loanProduct;
    }

    public function setLoanProductTitle(string $loanProductTitle) : void
    {
        $this->loanProductTitle = $loanProductTitle;
    }
    
    public function getLoanProductTitle() : string
    {
        return $this->loanProductTitle;
    }

    public function setEnterpriseName(string $enterpriseName) : void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName() : string
    {
        return $this->enterpriseName;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setSellerEnterprise(Enterprise $sellerEnterprise) : void
    {
        $this->sellerEnterprise = $sellerEnterprise;
    }

    public function getSellerEnterprise() : Enterprise
    {
        return $this->sellerEnterprise;
    }

    public function setBorrowerName(string $borrowerName) : void
    {
        $this->borrowerName = $borrowerName;
    }

    public function getBorrowerName() : string
    {
        return $this->borrowerName;
    }

    public function setApplicant(IAppointmentAble $applicant)
    {
        $this->applicant = $applicant;
    }

    public function getApplicant() : IAppointmentAble
    {
        return $this->applicant;
    }

    public function setLoanObject(int $loanObject) : void
    {
        $this->loanObject = $loanObject;
    }

    public function getLoanObject() : int
    {
        return $this->loanObject;
    }

    public function setLoanAmount(float $loanAmount) : void
    {
        $this->loanAmount = $loanAmount;
    }

    public function getLoanAmount() : float
    {
        return $this->loanAmount;
    }

    public function setLoanTerm(int $loanTerm) : void
    {
        $this->loanTerm = $loanTerm;
    }

    public function getLoanTerm() : int
    {
        return $this->loanTerm;
    }

    public function setLoanTermUnit(int $loanTermUnit) : void
    {
        $this->loanTermUnit = $loanTermUnit;
    }

    public function getLoanTermUnit() : int
    {
        return $this->loanTermUnit;
    }

    public function setGuarantyStyle(Dictionary $guarantyStyle) : void
    {
        $this->guarantyStyle = $guarantyStyle;
    }

    public function getGuarantyStyle() : Dictionary
    {
        return $this->guarantyStyle;
    }

    public function setGuarantyStyleType(Dictionary $guarantyStyleType) : void
    {
        $this->guarantyStyleType = $guarantyStyleType;
    }

    public function getGuarantyStyleType() : Dictionary
    {
        return $this->guarantyStyleType;
    }

    public function setLoanPurpose(Dictionary $loanPurpose) : void
    {
        $this->loanPurpose = $loanPurpose;
    }

    public function getLoanPurpose() : Dictionary
    {
        return $this->loanPurpose;
    }

    public function setLoanPurposeDescribe(string $loanPurposeDescribe) : void
    {
        $this->loanPurposeDescribe = $loanPurposeDescribe;
    }
    
    public function getLoanPurposeDescribe() : string
    {
        return $this->loanPurposeDescribe;
    }

    public function setAttachments(array $attachments) : void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments() : array
    {
        return $this->attachments;
    }

    public function setCreditReports(array $creditReports) : void
    {
        $this->creditReports = $creditReports;
    }

    public function getCreditReports() : array
    {
        return $this->creditReports;
    }

    public function setAuthorizedReports(array $authorizedReports) : void
    {
        $this->authorizedReports = $authorizedReports;
    }

    public function getAuthorizedReports() : array
    {
        return $this->authorizedReports;
    }

    public function setContactsInfo(ContactsInfo $contactsInfo) : void
    {
        $this->contactsInfo = $contactsInfo;
    }

    public function getContactsInfo() : ContactsInfo
    {
        return $this->contactsInfo;
    }

    public function setTransactionInfo(TransactionInfo $transactionInfo) : void
    {
        $this->transactionInfo = $transactionInfo;
    }

    public function getTransactionInfo() : TransactionInfo
    {
        return $this->transactionInfo;
    }

    public function setLoanResultInfo(LoanResultInfo $loanResultInfo) : void
    {
        $this->loanResultInfo = $loanResultInfo;
    }

    public function getLoanResultInfo() : LoanResultInfo
    {
        return $this->loanResultInfo;
    }

    public function setRejectReason(Dictionary $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason(): Dictionary
    {
        return $this->rejectReason;
    }

    public function setRejectReasonDescribe(string $rejectReasonDescribe) : void
    {
        $this->rejectReasonDescribe = $rejectReasonDescribe;
    }
    
    public function getRejectReasonDescribe() : string
    {
        return $this->rejectReasonDescribe;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIModifyStatusAbleAdapter() : IModifyStatusAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 用户提交申请
     * @return bool
     */
    public function add() : bool
    {
        if (!$this->validate()) {
            return false;
        }

        return $this->getIOperatAbleAdapter()->add($this);
    }

    /**
     * 用户撤销申请
     * @return bool
     */
    public function revoke() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_STATUS_NOT_NORMAL);
            return false;
        }

        return $this->getIModifyStatusAbleAdapter()->revoke($this);
    }

    /**
     * 用户删除申请
     * @return bool
     */
    public function deletes() : bool
    {
        if ($this->isReject() || $this->isRevoked()) {
            return $this->getIModifyStatusAbleAdapter()->deletes($this);
        }

        Core::setLastError(RESOURCE_STATUS_NOT_REJECT_OR_REVOKED);
        return false;
    }

    /**
     * 商家完成申请
     * @return bool
     */
    public function completed() : bool
    {
        if (!$this->isApprove()) {
            Core::setLastError(RESOURCE_STATUS_NOT_APPROVE);
            return false;
        }

        return $this->getRepository()->completed($this);
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::APPOINTMENT_STATUS['NORMAL'];
    }

    protected function isReject() : bool
    {
        return $this->getStatus() == self::APPOINTMENT_STATUS['REJECT'];
    }

    protected function isRevoked() : bool
    {
        return $this->getStatus() == self::APPOINTMENT_STATUS['REVOKED'];
    }

    protected function isApprove() : bool
    {
        return $this->getStatus() == self::APPOINTMENT_STATUS['APPROVE'];
    }
}
