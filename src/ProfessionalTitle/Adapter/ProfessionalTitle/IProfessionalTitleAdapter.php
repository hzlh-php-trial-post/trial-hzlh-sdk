<?php
namespace Trial\ProfessionalTitle\Adapter\ProfessionalTitle;

use Marmot\Interfaces\IAsyncAdapter;
use Trial\Common\Adapter\IFetchAbleAdapter;

interface IProfessionalTitleAdapter extends IFetchAbleAdapter, IProfessionalTitleOperatAdapter, IAsyncAdapter
{
}
