<?php
namespace Trial\ProfessionalTitle\Adapter\ProfessionalTitle;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleMockAdapterTrait;
use Trial\Common\Adapter\ApplyAbleMockAdapterTrait;

use Trial\ProfessionalTitle\Model\ProfessionalTitle;
use Trial\ProfessionalTitle\Utils\MockFactory;

class ProfessionalTitleMockAdapter implements IProfessionalTitleAdapter
{
    use OperatAbleMockAdapterTrait, ResubmitAbleMockAdapterTrait, ApplyAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateProfessionalTitleObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $professionalTitleList = array();

        foreach ($ids as $id) {
            $professionalTitleList[] = MockFactory::generateProfessionalTitleObject($id);
        }

        return $professionalTitleList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateProfessionalTitleObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $professionalTitleList = array();

        foreach ($ids as $id) {
            $professionalTitleList[] = MockFactory::generateProfessionalTitleObject($id);
        }

        return $professionalTitleList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
