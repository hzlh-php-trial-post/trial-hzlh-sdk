<?php
namespace Trial\ProfessionalTitle\Adapter\ProfessionalTitle;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;

interface IProfessionalTitleOperatAdapter extends IOperatAbleAdapter, IResubmitAbleAdapter, IApplyAbleAdapter
{
}
