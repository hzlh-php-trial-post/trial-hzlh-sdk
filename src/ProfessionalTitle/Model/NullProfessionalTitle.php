<?php
namespace Trial\ProfessionalTitle\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Trial\Common\Model\NullOperatAbleTrait;
use Trial\Common\Model\NullResubmitAbleTrait;
use Trial\Common\Model\NullApplyAbleTrait;
use Trial\Common\Model\NullModifyStatusAbleTrait;

class NullProfessionalTitle extends ProfessionalTitle implements INull
{
    use NullOperatAbleTrait, NullResubmitAbleTrait, NullModifyStatusAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist(): bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
