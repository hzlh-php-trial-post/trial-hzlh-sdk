<?php
namespace Trial\BusinessNotice\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Member\Model\Member;

use Trial\BusinessNotice\Repository\BusinessNoticeRepository;

class BusinessNotice implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    // 通知状态
    const NOTICE_STATUS = [
        'UNREAD' => 0 , //未读
        'READ' => 2 //已读
    ];

    // 消息状态
    const STATUS = [
        'NORMAL' => 0 , //正常
        'DELETE' => -2  //已删除
    ];

    // 触发模块
    const TRIGGER_MODULAR = [
        'SERVICE_ORDER' => 1, //服务订单
        'CONTRACT' => 2 //合同履约
    ];

    // 触发类型
    const TRIGGER_TYPE = [
        'PLACE_ORDER_UNPAID' => 1, //下单未付款
        'PLACE_ORDER_PAID' => 2, //下单已付款
        'PERFORMANCE_BEGIN' => 3, //接单成功
        'PERFORMANCE_END' => 4, //商家结束
        'BUYER_CONFIRMATION' => 5, //买家确认结束
        'COMMENT' => 6, //评价
        'CONTRACT_EXPIRATION_WARNING' => 10, //合同到期预警
        'BREACH_CONTRACT' => 11, //合同违约
        'CHANGE_CONTRACT_STATUS' => 12, //合同状态变化
        'RISK_CONTRACT' => 13, //风险预警
        'REMIND' => 14, //提醒信息
        'NULL' => 0 //评价
    ];

    // 处罚对象
    const SEND_OBJECT = [
        'BUYER' => 1, //买家
        'SELLER' => 2, //商家
        'MEMBER' => 3, //用户
        'OA_ADMIN' => 4 //OA平台管理员
    ];

    /**
     * @var int $id id
     */
    private $id;
    /**
     * @var array $ids ids
     */
    private $ids;
    /**
     * @var int $triggerModular 触发模快
     */
    private $triggerModular;
    /**
     * @var int $triggerType 触发类型
     */
    private $triggerType;
    /**
     * @var string $sendObject 发送对象
     */
    private $sendObject;
    /**
     * @var string $content PC模版内容
     */
    private $content;
    /**
     * @var string $appContent APP模版内容
     */
    private $appContent;
    /**
     * @var string $noticeStatus 消息状态
     */
    private $noticeStatus;
    /**
     * @var Member $member 用户
     */
    private $member;

    private $relation;

    private $commodity;

    private $relationId;

    private $route;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->ids = array();
        $this->triggerModular = 0;
        $this->triggerType = 0;
        $this->sendObject = 0;
        $this->content = '';
        $this->appContent = '';
        $this->route = '';
        $this->relationId = 0;
        $this->commodity = [];
        $this->noticeStatus = self::NOTICE_STATUS['UNREAD'];
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = '';
        $this->updateTime = '';
        $this->statusTime = 0;
        $this->repository = new BusinessNoticeRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->ids);
        unset($this->triggerModular);
        unset($this->triggerType);
        unset($this->sendObject);
        unset($this->content);
        unset($this->appContent);
        unset($this->route);
        unset($this->commodity);
        unset($this->relationId);
        unset($this->noticeStatus);
        unset($this->member);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setIds(array $ids) : void
    {
        $this->ids = $ids;
    }

    public function getIds() : array
    {
        return $this->ids;
    }

    public function setTriggerModular(int $triggerModular) : void
    {
        $this->triggerModular = $triggerModular;
    }

    public function getTriggerModular() : int
    {
        return $this->triggerModular;
    }

    public function setTriggerType(int $triggerType) : void
    {
        $this->triggerType = $triggerType;
    }

    public function getTriggerType() : int
    {
        return $this->triggerType;
    }

    public function setSendObject(int $sendObject) : void
    {
        $this->sendObject = $sendObject;
    }

    public function getSendObject() : int
    {
        return $this->sendObject;
    }

    public function setCommodity(array $commodity) : void
    {
        $this->commodity = $commodity;
    }

    public function getCommodity() : array
    {
        return $this->commodity;
    }

    public function setRoute(string $route) : void
    {
        $this->route = $route;
    }

    public function getRoute() : string
    {
        return $this->route;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setAppContent(string $appContent) : void
    {
        $this->appContent = $appContent;
    }

    public function getAppContent() : string
    {
        return $this->appContent;
    }

    public function setNoticeStatus(int $noticeStatus) : void
    {
        $this->noticeStatus = $noticeStatus;
    }

    public function getNoticeStatus() : int
    {
        return $this->noticeStatus;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : BusinessNoticeRepository
    {
        return $this->repository;
    }

    public function setRelation($relation) : void
    {
        $this->relation = $relation;
    }

    public function getRelation()
    {
        return $this->relation;
    }

    public function setRelationId(int $relationId) : void
    {
        $this->relationId = $relationId;
    }

    public function getRelationId() : int
    {
        return $this->relationId;
    }

    public function read() : bool
    {
        return $this->getRepository()->read($this);
    }

    public function allRead() : bool
    {
        return $this->getRepository()->allRead($this);
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
