<?php
namespace Trial\BusinessNotice\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;

use Trial\BusinessNotice\Model\BusinessNotice;
use Trial\BusinessNotice\Adapter\BusinessNotice\IBusinessNoticeAdapter;
use Trial\BusinessNotice\Adapter\BusinessNotice\BusinessNoticeMockAdapter;
use Trial\BusinessNotice\Adapter\BusinessNotice\BusinessNoticeRestfulAdapter;

class BusinessNoticeRepository extends Repository implements IBusinessNoticeAdapter
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const PORTAL_LIST_MODEL_UN = 'PORTAL_BUSINESS_NOTICE_LIST';
    const FETCH_ONE_MODEL_UN = 'BUSINESS_NOTICE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new BusinessNoticeRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IBusinessNoticeAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBusinessNoticeAdapter
    {
        return new BusinessNoticeMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function read(BusinessNotice $businessNotice) : bool
    {
        return $this->getAdapter()->read($businessNotice);
    }

    public function allRead(BusinessNotice $businessNotice) : bool
    {
        return $this->getAdapter()->allRead($businessNotice);
    }

    public function deletes(BusinessNotice $businessNotice) : bool
    {
        return $this->getAdapter()->deletes($businessNotice);
    }
}
