<?php
namespace Trial\BusinessNotice\Adapter\BusinessNotice;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Trial\BusinessNotice\Model\BusinessNotice;
use Trial\BusinessNotice\Model\NullBusinessNotice;
use Trial\BusinessNotice\Translator\BusinessNoticeRestfulTranslator;

class BusinessNoticeRestfulAdapter extends GuzzleAdapter implements IBusinessNoticeAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_BUSINESS_NOTICE_LIST'=>[
                'fields'=>[],
                'include'=> 'member,relation,commodity'
            ],
            'BUSINESS_NOTICE_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'member,relation,commodity'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new BusinessNoticeRestfulTranslator();
        $this->resource = 'businessNotices';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            121 => RESOURCE_NOTICE_STATUS_READ
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullBusinessNotice::getInstance());
    }

    public function read(BusinessNotice $businessNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $businessNotice,
            array('ids')
        );

        $this->patch(
            $this->getResource().'/read',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($businessNotice);
            return true;
        }

        return false;
    }

    public function allRead(BusinessNotice $businessNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $businessNotice,
            array('member')
        );

        $this->patch(
            $this->getResource().'/allRead',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($businessNotice);
            return true;
        }

        return false;
    }

    public function deletes(BusinessNotice $businessNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $businessNotice,
            array('ids')
        );

        $this->patch(
            $this->getResource().'/delete',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($businessNotice);
            return true;
        }
        return false;
    }

    protected function addAction(BusinessNotice $businessNotice) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $businessNotice,
            array(
              'relationId',
              'memberId',
              'triggerModular',
              'triggerType',
              'content',
              'appContent',
              'route',
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($businessNotice);
            return true;
        }

        return false;
    }

    protected function editAction(BusinessNotice $businessNotice) : bool
    {
        unset($businessNotice);
        return true;
    }
}
