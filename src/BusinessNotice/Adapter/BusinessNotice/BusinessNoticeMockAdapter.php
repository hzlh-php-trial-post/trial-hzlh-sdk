<?php
namespace Trial\BusinessNotice\Adapter\BusinessNotice;

use Trial\BusinessNotice\Model\BusinessNotice;
use Trial\BusinessNotice\Utils\MockFactory;

class BusinessNoticeMockAdapter implements IBusinessNoticeAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateBusinessNoticeObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $businessNoticeList = array();

        foreach ($ids as $id) {
            $businessNoticeList[] = MockFactory::generateBusinessNoticeObject($id);
        }

        return $businessNoticeList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateBusinessNoticeObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateBusinessNoticeObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function read(BusinessNotice $businessNotice) : bool
    {
        unset($businessNotice);

        return true;
    }

    public function allRead(BusinessNotice $businessNotice) : bool
    {
        unset($businessNotice);

        return true;
    }

    public function deletes(BusinessNotice $businessNotice) : bool
    {
        unset($businessNotice);

        return true;
    }
}
