<?php
namespace Trial\BusinessNotice\Adapter\BusinessNotice;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\BusinessNotice\Model\BusinessNotice;

interface IBusinessNoticeAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
    public function read(BusinessNotice $businessNotice) : bool;

    public function allRead(BusinessNotice $businessNotice) : bool;

    public function deletes(BusinessNotice $businessNotice) : bool;
}
