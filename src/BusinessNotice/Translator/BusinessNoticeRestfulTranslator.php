<?php
namespace Trial\BusinessNotice\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Member\Translator\MemberRestfulTranslator;

use Trial\BusinessNotice\Model\BusinessNotice;
use Trial\BusinessNotice\Model\NullBusinessNotice;

class BusinessNoticeRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }

    public function arrayToObject(array $expression, $businessNotice = null)
    {
        return $this->translateToObject($expression, $businessNotice);
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $businessNotice = null)
    {
        if (empty($expression)) {
            return NullBusinessNotice::getInstance();
        }

        if ($businessNotice == null) {
            $businessNotice = new BusinessNotice();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $businessNotice->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['triggerModular'])) {
            $businessNotice->setTriggerModular($attributes['triggerModular']);
        }
        if (isset($attributes['triggerType'])) {
            $businessNotice->setTriggerType($attributes['triggerType']);
        }
        if (isset($attributes['sendObject'])) {
            $businessNotice->setSendObject($attributes['sendObject']);
        }
        if (isset($attributes['noticeStatus'])) {
            $businessNotice->setNoticeStatus($attributes['noticeStatus']);
        }
        if (isset($attributes['commodity'])) {
            $businessNotice->setCommodity($attributes['commodity']);
        }
        if (isset($attributes['content'])) {
            $businessNotice->setContent($attributes['content']);
        }
        if (isset($attributes['appContent'])) {
            $businessNotice->setAppContent($attributes['appContent']);
        }
        if (isset($attributes['route'])) {
            $businessNotice->setRoute($attributes['route']);
        }
        if (isset($attributes['relationId'])) {
            $businessNotice->setRelationId($attributes['relationId']);
        }
        if (isset($attributes['status'])) {
            $businessNotice->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $businessNotice->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $businessNotice->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $businessNotice->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $businessNotice->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        if (isset($relationships['relation']['data'])) {
            if (isset($expression['included'])) {
                $relation = $this->changeArrayFormat($relationships['relation']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $relation = $this->changeArrayFormat($relationships['relation']['data']);
            }

            $relationTranslator = $this->getTranslatorFactory()->getTranslator($attributes['triggerModular']);
            $businessNotice->setRelation($relationTranslator->arrayToObject($relation));
        }

        return $businessNotice;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($businessNotice, array $keys = array())
    {
        $expression = array();

        if (!$businessNotice instanceof BusinessNotice) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'ids',
                'member'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'businessNotices'
            )
        );

        $attributes = array();

        if (in_array('ids', $keys)) {
            $attributes['ids'] = $businessNotice->getIds();
        }
        if (in_array('relationId', $keys)) {
            $attributes['relationId'] = $businessNotice->getRelationId();
        }
        if (in_array('memberId', $keys)) {
            $attributes['memberId'] = $businessNotice->getMember()->getId();
        }
        if (in_array('triggerModular', $keys)) {
            $attributes['triggerModular'] = $businessNotice->getTriggerModular();
        }
        if (in_array('triggerType', $keys)) {
            $attributes['triggerType'] = $businessNotice->getTriggerType();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $businessNotice->getContent();
        }
        if (in_array('appContent', $keys)) {
            $attributes['appContent'] = $businessNotice->getAppContent();
        }
        if (in_array('route', $keys)) {
            $attributes['route'] = $businessNotice->getRoute();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $businessNotice->getMember()->getId()
                )
            );
        }

        return $expression;
    }
}
