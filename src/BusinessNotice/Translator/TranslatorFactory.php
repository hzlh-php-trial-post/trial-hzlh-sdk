<?php
namespace Trial\BusinessNotice\Translator;

use Trial\BusinessNotice\Model\BusinessNotice;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Classes\NullTranslator;

class TranslatorFactory
{
    const MAPS = array(
        BusinessNotice::TRIGGER_MODULAR['SERVICE_ORDER'] =>
        'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator'
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
