<?php
namespace Trial\Order\RefundOrder\Adapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IRefundOrderAdapter extends IFetchAbleAdapter, IOperatAbleAdapter
{

}
