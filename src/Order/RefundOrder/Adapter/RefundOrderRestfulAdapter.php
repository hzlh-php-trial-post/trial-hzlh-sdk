<?php
namespace Trial\Order\RefundOrder\Adapter;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Order\RefundOrder\Model\NullRefundOrder;
use Trial\Order\RefundOrder\Model\RefundOrder;
use Trial\Order\RefundOrder\Translator\RefundOrderRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class RefundOrderRestfulAdapter extends GuzzleAdapter implements IRefundOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_REFUND_ORDER_LIST'=>[
                'fields'=>[],
                'include'=> 'order,member,enterprise,order.orderCommodities.commodity,order.buyerMemberAccount,order.orderAddress.deliveryAddress,order.memberCoupons.releaseCoupon'//phpcs:ignore
            ],
            'PORTAL_REFUND_ORDER_LIST'=>[
                'fields'=>[],
                'include'=> 'order,member,enterprise,order.orderCommodities.commodity,order.buyerMemberAccount,order.orderAddress.deliveryAddress,order.memberCoupons.releaseCoupon'//phpcs:ignore
            ],
            'REFUND_ORDER_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'order,member,enterprise,order.orderCommodities.commodity,order.buyerMemberAccount,order.orderAddress.deliveryAddress,order.memberCoupons.releaseCoupon'//phpcs:ignore
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new RefundOrderRestfulTranslator();
        $this->resource = 'refunds';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullRefundOrder::getInstance());
    }

    // 用户发起退款
    protected function addAction(RefundOrder $refundOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $refundOrder,
            array(
                'refundReason',
                'member',
                'enterprise',
                'order'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($refundOrder);
            return true;
        }

        return false;
    }

    // oa打款
    public function payment(RefundOrder $refundOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $refundOrder,
            array('voucher','crew')
        );
        //$data['data']['relationships']['crew']['data'][0]['id'] = 1;

        $this->patch(
            $this->getResource().'/'.$refundOrder->getId().'/confirmPayment',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($refundOrder);
            return true;
        }
        return false;
    }

    // 商家拒绝
    public function reject(RefundOrder $refundOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $refundOrder,
            array('refuseReason')
        );

        $this->patch(
            $this->getResource().'/'.$refundOrder->getId().'/refuse',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($refundOrder);
            return true;
        }
        return false;
    }

    // 商家同意
    public function approve(RefundOrder $refundOrder) : bool
    {

        $this->patch(
            $this->getResource().'/'.$refundOrder->getId().'/agree'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($refundOrder);
            return true;
        }
        return false;
    }

    protected function editAction(RefundOrder $refundOrder) : bool
    {
        unset($refundOrder);
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
