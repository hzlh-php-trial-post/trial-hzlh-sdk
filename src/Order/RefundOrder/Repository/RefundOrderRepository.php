<?php
namespace Trial\Order\RefundOrder\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\RecommendAbleRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\OnShelfAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Order\RefundOrder\Adapter\IRefundOrderAdapter;
use Trial\Order\RefundOrder\Adapter\RefundOrderRestfulAdapter;
use Trial\Order\RefundOrder\Model\RefundOrder;

class RefundOrderRepository extends Repository implements IRefundOrderAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait,
        RecommendAbleRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_REFUND_ORDER_LIST'; //OA列表场景
    const PORTAL_LIST_MODEL_UN = 'PORTAL_REFUND_ORDER_LIST'; //门户列表场景
    const FETCH_ONE_MODEL_UN = 'REFUND_ORDER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new RefundOrderRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IRefundOrderAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IRefundOrderAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function payment(RefundOrder $refundOrder)
    {
        return $this->getAdapter()->payment($refundOrder);
    }

    public function approve(RefundOrder $refundOrder)
    {
        return $this->getAdapter()->approve($refundOrder);
    }

    public function reject(RefundOrder $refundOrder)
    {
        return $this->getAdapter()->reject($refundOrder);
    }
}
