<?php
namespace Trial\Order\RefundOrder\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Crew\Model\Crew;
use Trial\Enterprise\Model\Enterprise;
use Trial\Member\Model\Member;
use Trial\Order\CommonOrder\Model\Order;
use Trial\Order\RefundOrder\Repository\RefundOrderRepository;
use Trial\Order\ServiceOrder\Model\ServiceOrder;

class RefundOrder implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    const REFUND_STATUS = [
        'UNDER_REVIEW' => 0,
        'AGREE' => 2,
        'REFUSE' => -2,
        'PAYMENT' => 4
    ];

    const REFUND_STATUS_OA_CN = [
        0 => '等待商家确认',
        2 => '待退款',
        -2 => '商家拒绝',
        4 => '退款完成',
    ];

    const REFUND_STATUS_BUYER_CN = [
        0 => '待处理',
        2 => '待退款',
        -2 => '退款失败',
        4 => '退款到账',
    ];

    const REFUND_STATUS_SELLER_CN = [
        0 => '待处理',
        2 => '待转账',
        -2 => '退款失败',
        4 => '退款到账',
    ];

    private $id;
    private $order;
    private $member;
    private $enterprise;
    private $crew;
    private $refundReason;
    private $refuseReason;
    private $voucher;
    private $orderNumber;
    private $orderCategory;
    private $goodsName;
    private $timeRecord;
    private $repository;

    public function __construct($id = 0)
    {
        $this->id = $id;
        $this->order = new ServiceOrder();
        $this->member = new Member();
        $this->crew = new Crew();
        $this->enterprise = new Enterprise();
        $this->refundReason = '';
        $this->refuseReason = '';
        $this->voucher = array();
        $this->orderNumber = '';
        $this->goodsName = '';
        $this->timeRecord = [];
        $this->orderCategory = 0;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = 0;

        $this->repository = new RefundOrderRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->order);
        unset($this->member);
        unset($this->enterprise);
        unset($this->crew);
        unset($this->refundReason);
        unset($this->refuseReason);
        unset($this->voucher);
        unset($this->orderNumber);
        unset($this->goodsName);
        unset($this->timeRecord);
        unset($this->orderCategory);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setRefundReason(string $refundReason) : void
    {
        $this->refundReason = $refundReason;
    }

    public function getRefundReason(): string
    {
        return $this->refundReason;
    }

    public function setRefuseReason(string $refuseReason) : void
    {
        $this->refuseReason = $refuseReason;
    }

    public function getRefuseReason(): string
    {
        return $this->refuseReason;
    }

    public function setVoucher(array $voucher) : void
    {
        $this->voucher = $voucher;
    }

    public function getVoucher() : array
    {
        return $this->voucher;
    }

    public function setOrderNumber(string $orderNumber) : void
    {
        $this->orderNumber = $orderNumber;
    }

    public function getOrderNumber() : string
    {
        return $this->orderNumber;
    }

    public function setOrderCategory(int $orderCategory) : void
    {
        $this->orderCategory = $orderCategory;
    }

    public function getOrderCategory() : int
    {
        return $this->orderCategory;
    }

    public function setGoodsName(string $goodsName) : void
    {
        $this->goodsName = $goodsName;
    }

    public function getGoodsName() : string
    {
        return $this->goodsName;
    }

    public function setTimeRecord(array $timeRecord) : void
    {
        $this->timeRecord = $timeRecord;
    }

    public function getTimeRecord() : array
    {
        return $this->timeRecord;
    }

    public function setOrder(IObject $order) : void
    {
        $this->order = $order;
    }

    public function getOrder() : IObject
    {
        return $this->order;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->repository;
    }

    // 商家审核通过
    public function approve()
    {
        $this->setStatus(self::REFUND_STATUS['AGREE']);
        $this->setStatusTime(Core::$container->get('time'));
        return $this->getIOperatAbleAdapter()->approve($this);
    }

    // 商家审核拒绝
    public function reject()
    {
        $this->setStatus(self::REFUND_STATUS['REFUSE']);
        $this->setStatusTime(Core::$container->get('time'));
        return $this->getIOperatAbleAdapter()->reject($this);
    }

    // OA打款
    public function payment()
    {
        $this->setStatus(self::REFUND_STATUS['PAYMENT']);
        $this->setStatusTime(Core::$container->get('time'));
        return $this->getIOperatAbleAdapter()->payment($this);
    }
}
