<?php
namespace Trial\Order\RefundOrder\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\Crew\Translator\CrewRestfulTranslator;
use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;
use Trial\Member\Translator\MemberRestfulTranslator;
use Trial\Order\RefundOrder\Model\NullRefundOrder;
use Trial\Order\RefundOrder\Model\RefundOrder;
use Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class RefundOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $refundOrder = null)
    {
        return $this->translateToObject($expression, $refundOrder);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $refundOrder = null)
    {
        if (empty($expression)) {
            return NullRefundOrder::getInstance();
        }

        if ($refundOrder == null) {
            $refundOrder = new RefundOrder();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $refundOrder->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['refundReason'])) {
            $refundOrder->setRefundReason($attributes['refundReason']);
        }
        if (isset($attributes['refuseReason'])) {
            $refundOrder->setRefuseReason($attributes['refuseReason']);
        }
        if (isset($attributes['voucher'])) {
            $refundOrder->setVoucher($attributes['voucher']);
        }
        if (isset($attributes['status'])) {
            $refundOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $refundOrder->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $refundOrder->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $refundOrder->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['orderNumber'])) {
            $refundOrder->setOrderNumber($attributes['orderNumber']);
        }
        if (isset($attributes['orderCategory'])) {
            $refundOrder->setOrderCategory($attributes['orderCategory']);
        }
        if (isset($attributes['goodsName'])) {
            $refundOrder->setGoodsName($attributes['goodsName']);
        }
        if (isset($attributes['timeRecord'])) {
            $refundOrder->setTimeRecord($attributes['timeRecord']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['order']['data'])) {
            if (isset($expression['included'])) {
                $serviceOrder = $this->changeArrayFormat(
                    $relationships['order']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $serviceOrder = $this->changeArrayFormat($relationships['order']['data']);
            }

            $refundOrder->setOrder($this->getServiceOrderRestfulTranslator()->arrayToObject($serviceOrder));
        }

        if (isset($relationships['enterprise']['data'])) {
            if (isset($expression['included'])) {
                $enterprise = $this->changeArrayFormat(
                    $relationships['enterprise']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            }

            $refundOrder->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }

        if (isset($relationships['member']['data'])) {
            if (isset($expression['included'])) {
                $member = $this->changeArrayFormat(
                    $relationships['member']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $member = $this->changeArrayFormat($relationships['member']['data']);
            }

            $refundOrder->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        if (isset($relationships['crew']['data'])) {
            if (isset($expression['included'])) {
                $crew = $this->changeArrayFormat(
                    $relationships['crew']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $crew = $this->changeArrayFormat($relationships['crew']['data']);
            }

            $refundOrder->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $refundOrder;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($refundOrder, array $keys = array())
    {
        $expression = array();

        if (!$refundOrder instanceof RefundOrder) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'refundReason',
                'refuseReason',
                'voucher',
                'member',
                'enterprise',
                'order',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'refunds'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $refundOrder->getId();
        }

        $attributes = array();

        if (in_array('refundReason', $keys)) {
            $attributes['refundReason'] = $refundOrder->getRefundReason();
        }
        if (in_array('refuseReason', $keys)) {
            $attributes['refuseReason'] = $refundOrder->getRefuseReason();
        }
        if (in_array('voucher', $keys)) {
            $attributes['voucher'] = $refundOrder->getVoucher();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $refundOrder->getMember()->getId()
                )
            );
        }
        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprises',
                    'id' => $refundOrder->getEnterprise()->getId()
                )
            );
        }
        if (in_array('order', $keys)) {
            $expression['data']['relationships']['order']['data'] = array(
                array(
                    'type' => 'orders',
                    'id' => $refundOrder->getOrder()->getId()
                )
            );
        }
        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => Core::$container->get('crew')->getId()
                )
            );
        }

        return $expression;
    }

    protected function getServiceOrderRestfulTranslator() : IRestfulTranslator
    {
        return new ServiceOrderRestfulTranslator();
    }

    protected function getEnterpriseRestfulTranslator() : IRestfulTranslator
    {
        return new EnterpriseRestfulTranslator();
    }
    protected function getMemberRestfulTranslator() : IRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }
    protected function getCrewRestfulTranslator() : IRestfulTranslator
    {
        return new CrewRestfulTranslator();
    }
}
