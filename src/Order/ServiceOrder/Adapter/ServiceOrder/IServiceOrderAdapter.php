<?php
namespace Trial\Order\ServiceOrder\Adapter\ServiceOrder;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Order\ServiceOrder\Model\ServiceOrder;

interface IServiceOrderAdapter extends IFetchAbleAdapter, IOperatAbleAdapter
{
    public function paymentFailure(ServiceOrder $serviceOrder) : bool;
}
