<?php
namespace Trial\Statistical\Adapter;

class StatisticalAdapterFactory
{
    const MAPS = array(
        'staticsActiveEnterprise'=>
            'Trial\Statistical\Adapter\StaticsActiveEnterpriseAdapter',
        'staticsServiceAuthenticationCount'=>
            'Trial\Statistical\Adapter\StaticsServiceAuthenticationCountAdapter',
        'staticsServiceRequirementCount'=>
            'Trial\Statistical\Adapter\StaticsServiceRequirementCountAdapter',
        'staticsServiceCount'=>
            'Trial\Statistical\Adapter\StaticsServiceCountAdapter',
        'staticsMemberCouponCount'=>
            'Trial\Statistical\Adapter\StaticsMemberCouponCountAdapter',
        'staticsEnterpriseOrderVolume'=>
            'Trial\Statistical\Adapter\StaticsEnterpriseOrderVolumeAdapter',
        'staticsLoanProductCount'=>
            'Trial\Statistical\Adapter\StaticsLoanProductCountAdapter',
        'staticsFinanceCount'=>
            'Trial\Statistical\Adapter\StaticsFinanceCountAdapter',
        'trialBalance'=>
            'Trial\Statistical\Adapter\TrialBalanceCountAdapter',
        'staticsEnterpriseAuthenticationCount'=>
            'Trial\Statistical\Adapter\StaticsEnterpriseAuthenticationCountAdapter',
        'staticsTrialBalance'=>
            'Trial\Statistical\Adapter\StaticsTrialBalanceCountAdapter',
        'staticsMaxVoucherNumber'=>
            'Trial\Statistical\Adapter\StaticsMaxVoucherNumberAdapter',
        'staticsFinalExamination'=>
            'Trial\Statistical\Adapter\StaticsFinalExaminationAdapter',
        'staticsVoucherProfitAndLossSubject'=>
            'Trial\Statistical\Adapter\StaticsVoucherProfitAndLossSubjectAdapter',
        'staticsEnterpriseLoanProductInfoCount'=>
            'Trial\Statistical\Adapter\StaticsEnterpriseLoanProductInfoCountAdapter',
        'staticsFinalCheckoutStatus'=>
            'Trial\Statistical\Adapter\StaticsFinalCheckoutStatusAdapter',
        'staticsBalanceSheet'=>
            'Trial\Statistical\Adapter\StaticsBalanceSheetAdapter',
        'staticsIncomeStatement'=>
            'Trial\Statistical\Adapter\StaticsIncomeStatementAdapter',
        'staticsCashFlowStatement'=>
            'Trial\Statistical\Adapter\StaticsCashFlowStatementAdapter',
        'staticsServiceCategory'=>
            'Trial\Statistical\Adapter\StaticsServiceCategoryAdapter',
        'staticsPolicyByMonth'=>
            'Trial\Statistical\Adapter\StaticsPolicyByMonthAdapter',
        'staticsByJoinByMonth'=>
            'Trial\Statistical\Adapter\StaticsByJoinByMonthAdapter',
        'staticsServiceAndFinanceByMonth'=>
            'Trial\Statistical\Adapter\StaticsServiceAndFinanceByMonthAdapter',
        'staticsServiceAndFinanceCount'=>
            'Trial\Statistical\Adapter\StaticsServiceAndFinanceCountAdapter',
        'staticsByAmountTransaction'=>
            'Trial\Statistical\Adapter\StaticsByAmountTransactionAdapter',
        'staticsByServiceOrder'=>
            'Trial\Statistical\Adapter\StaticsByServiceOrderAdapter',
        'staticsBusinessNotice'=>
            'Trial\Statistical\Adapter\StaticsBusinessNoticeAdapter',
        'staticsCommodityScore'=>
            'Trial\Statistical\Adapter\StaticsCommodityScoreAdapter',
        'staticsEnterpriseScore'=>
            'Trial\Statistical\Adapter\StaticsEnterpriseScoreAdapter',
        'staticsCollectionByMember'=>
            'Trial\Statistical\Adapter\StaticsCollectionByMemberAdapter',
        'staticsCommodityByEnterprise'=>
            'Trial\Statistical\Adapter\StaticsCommodityByEnterpriseAdapter',
        'staticsCollectionByCategoryOrDetail'=>
            'Trial\Statistical\Adapter\StaticsCollectionByCategoryOrDetailAdapter',
        'staticsTagCategoryUsageCount'=>
            'Trial\Statistical\Adapter\StaticsTagCategoryUsageCountAdapter',
        'staticsTagUsageCount'=>
            'Trial\Statistical\Adapter\StaticsTagUsageCountAdapter',
        'staticsFinishOrderByEnterprise'=>
            'Trial\Statistical\Adapter\StaticsFinishOrderByEnterpriseAdapter'
    );

    public function getAdapter(string $type) : IStatisticalAdapter
    {
        $adapter = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($adapter) ? new $adapter : false;
    }
}
