<?php
namespace Trial\Statistical\Adapter;

interface IStatisticalAdapter
{
    public function analyse(array $filter = array());
}
