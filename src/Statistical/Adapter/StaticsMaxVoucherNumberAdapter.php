<?php
namespace Trial\Statistical\Adapter;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Statistical\Translator\StatisticalRestfulTranslator;
use Trial\Statistical\Model\NullStatistical;

use Marmot\Core;

class StaticsMaxVoucherNumberAdapter extends GuzzleAdapter implements IStatisticalAdapter
{
    use StatisticalAdapterTrait;

    public function analyse(array $filter = array())
    {
        $this->get(
            $this->getResource().'/staticsMaxVoucherNumber',
            array(
                'filter'=>$filter
            )
        );

        return $this->isSuccess() ? $this->translateToObject() : NullStatistical::getInstance();
    }
}
