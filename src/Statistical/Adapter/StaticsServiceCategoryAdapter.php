<?php
namespace Trial\Statistical\Adapter;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Trial\Statistical\Model\NullStatistical;

class StaticsServiceCategoryAdapter extends GuzzleAdapter implements IStatisticalAdapter
{
    use StatisticalAdapterTrait;

    public function analyse(array $filter = array())
    {
        $this->get(
            $this->getResource().'/staticsParentCategoryServiceCount',
            array(
                'filter'=>$filter
            )
        );

        return $this->isSuccess() ? $this->translateToObject() : NullStatistical::getInstance();
    }
}
