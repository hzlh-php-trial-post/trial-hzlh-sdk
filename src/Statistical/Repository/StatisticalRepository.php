<?php
namespace Trial\Statistical\Repository;

use Marmot\Core;

use Trial\Statistical\Adapter\IStatisticalAdapter;
use Trial\Statistical\Adapter\StatisticalAdapterFactory;
use Trial\Statistical\Adapter\StatisticalRestfulAdapter;

class StatisticalRepository implements IStatisticalAdapter
{
    private $adapter;

    public function __construct(IStatisticalAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getAdapter() : IStatisticalAdapter
    {
        return $this->adapter;
    }

    public function analyse(array $filter = array())
    {
        return $this->getAdapter()->analyse($filter);
    }
}
