<?php
namespace Trial\Statistical\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\Statistical\Model\Statistical;
use Trial\Statistical\Model\NullStatistical;

class StatisticalRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $statistical = null)
    {
        return $this->translateToObject($expression, $statistical);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $statistical = null)
    {
        if (empty($expression)) {
            return NullStatistical::getInstance();
        }

        if ($statistical == null) {
            $statistical = new Statistical();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $statistical->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['result'])) {
            $statistical->setResult($attributes['result']);
        }
        
        return $statistical;
    }

    public function objectToArray($statistical, array $keys = array())
    {
        unset($statistical);
        unset($keys);
        return false;
    }
}
