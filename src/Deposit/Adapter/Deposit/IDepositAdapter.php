<?php
namespace Trial\Deposit\Adapter\Deposit;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Trial\Deposit\Model\Deposit;

interface IDepositAdapter extends IAsyncAdapter, IFetchAbleAdapter
{
    public function deposit(Deposit $deposit) : bool;

    public function pay(Deposit $deposit) : bool;

    public function paymentFailure(Deposit $deposit) : bool;
}
