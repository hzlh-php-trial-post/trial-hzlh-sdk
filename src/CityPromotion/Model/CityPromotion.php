<?php
namespace Trial\CityPromotion\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IOnShelfAble;
use Trial\Common\Model\IRecommendAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\OnShelfAbleTrait;
use Trial\Common\Model\RecommendAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;
use Trial\Common\Adapter\IRecommendAbleAdapter;

use Trial\Crew\Model\Crew;
use Trial\CityPromotion\Repository\CityPromotionRepository;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class CityPromotion implements IObject, IOperatAble, IOnShelfAble, IRecommendAble
{
    use Object, OperatAbleTrait, OnShelfAbleTrait, RecommendAbleTrait;

    /**
     * [$id id]
     * @var [int]
     */
    private $id;
    /**
     * [$title 标题]
     * @var [string]
     */
    private $title;
    /**
     * [$detail 内容]
     * @var [array]
     */
    private $detail;
    /**
     * [$recommendStatus 推荐至主页]
     * @var [int]
     */
    private $recommendStatus;
    /**
     * [$cover 封面图]
     * @var [array]
     */
    private $cover;
    /**
     * [$crew 操作人员]
     * @var [Crew]
     */
    private $crew;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->title = '';
        $this->detail = array();
        $this->cover = array();
        $this->status = IOnShelfAble::STATUS['ONSHELF'];
        $this->recommendStatus = IRecommendAble::RECOMMEND_HOMEPAGE_STATUS['NO'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->repository = new CityPromotionRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->detail);
        unset($this->cover);
        unset($this->status);
        unset($this->recommendStatus);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->crew);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCover(array $cover) : void
    {
        $this->cover = $cover;
    }

    public function getCover() : array
    {
        return $this->cover;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setDetail(array $detail) : void
    {
        $this->detail = $detail;
    }

    public function getDetail() : array
    {
        return $this->detail;
    }

    public function setRecommendStatus(int $recommendStatus) : void
    {
        $this->recommendStatus = $recommendStatus;
    }

    public function getRecommendStatus() : int
    {
        return $this->recommendStatus;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : CityPromotionRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOnShelfAbleAdapter() : IOnShelfAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIRecommendAbleAdapter() : IRecommendAbleAdapter
    {
        return $this->getRepository();
    }
}
