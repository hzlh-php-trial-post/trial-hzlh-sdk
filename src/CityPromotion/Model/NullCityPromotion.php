<?php
namespace Trial\CityPromotion\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullRecommendAbleTrait;
use Trial\Common\Model\NullOnShelfAbleTrait;
use Trial\Common\Model\NullOperatAbleTrait;

class NullCityPromotion extends CityPromotion implements INull
{
    use NullOnShelfAbleTrait, NullOperatAbleTrait, NullRecommendAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
