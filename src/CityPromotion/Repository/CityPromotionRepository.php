<?php
namespace Trial\CityPromotion\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\RecommendAbleRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\OnShelfAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\CityPromotion\Model\CityPromotion;
use Trial\CityPromotion\Adapter\CityPromotion\ICityPromotionAdapter;
use Trial\CityPromotion\Adapter\CityPromotion\CityPromotionMockAdapter;
use Trial\CityPromotion\Adapter\CityPromotion\CityPromotionRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class CityPromotionRepository extends Repository implements ICityPromotionAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait,
        RecommendAbleRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_CITY_PROMOTION_LIST'; //OA列表场景
    const PORTAL_LIST_MODEL_UN = 'PORTAL_CITY_PROMOTION_LIST'; //门户列表场景
    const FETCH_ONE_MODEL_UN = 'CITY_PROMOTION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CityPromotionRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ICityPromotionAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ICityPromotionAdapter
    {
        return new CityPromotionMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
