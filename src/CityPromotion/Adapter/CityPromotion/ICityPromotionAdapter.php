<?php
namespace Trial\CityPromotion\Adapter\CityPromotion;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;
use Trial\Common\Adapter\IRecommendAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface ICityPromotionAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IOnShelfAbleAdapter, IRecommendAbleAdapter//phpcs:ignore
{
}
