<?php
namespace Trial\CityPromotion\Adapter\CityPromotion;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\CityPromotion\Model\CityPromotion;
use Trial\CityPromotion\Model\NullCityPromotion;
use Trial\CityPromotion\Translator\CityPromotionRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\RecommendAbleRestfulAdapterTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class CityPromotionRestfulAdapter extends GuzzleAdapter implements ICityPromotionAdapter
{
    use CommonMapErrorsTrait,
        RecommendAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        OnShelfAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_CITY_PROMOTION_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'PORTAL_CITY_PROMOTION_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'CITY_PROMOTION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CityPromotionRestfulTranslator();
        $this->resource = 'cityPromotions';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        $mapError = [
            32003 => CITY_PROMOTION_OUT_OF_RECOMMEND_RANGE
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCityPromotion::getInstance());
    }

    protected function addAction(CityPromotion $cityPromotion) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $cityPromotion,
            array(
                'title',
                'cover',
                'detail',
                'crew'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($cityPromotion);
            return true;
        }

        return false;
    }

    protected function editAction(CityPromotion $cityPromotion) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $cityPromotion,
            array(
                'title',
                'cover',
                'detail',
                'videoLink'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$cityPromotion->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($cityPromotion);
            return true;
        }

        return false;
    }
}
