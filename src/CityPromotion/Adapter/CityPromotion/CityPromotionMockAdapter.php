<?php
namespace Trial\CityPromotion\Adapter\CityPromotion;

use Trial\Common\Adapter\RecommendAbleMockAdapterTrait;
use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleMockAdapterTrait;

use Trial\CityPromotion\Utils\MockFactory;

class CityPromotionMockAdapter implements ICityPromotionAdapter
{
    use OperatAbleMockAdapterTrait, OnShelfAbleMockAdapterTrait, RecommendAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateCityPromotionObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $cityPromotionList = array();

        foreach ($ids as $id) {
            $cityPromotionList[] = MockFactory::generateCityPromotionObject($id);
        }

        return $cityPromotionList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateCityPromotionObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateCityPromotionObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
