<?php
namespace Trial\CityPromotion\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\CityPromotion\Model\CityPromotion;
use Trial\CityPromotion\Model\NullCityPromotion;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Crew\Translator\CrewRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class CityPromotionRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $cityPromotion = null)
    {
        return $this->translateToObject($expression, $cityPromotion);
    }

    protected function translateToObject(array $expression, $cityPromotion = null)
    {
        if (empty($expression)) {
            return NullCityPromotion::getInstance();
        }
        if ($cityPromotion === null) {
            $cityPromotion = new CityPromotion();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $cityPromotion->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $cityPromotion->setTitle($attributes['title']);
        }
        if (isset($attributes['cover'])) {
            $cityPromotion->setCover($attributes['cover']);
        }
        if (isset($attributes['detail'])) {
            $cityPromotion->setDetail($attributes['detail']);
        }
        if (isset($attributes['recommendStatus'])) {
            $cityPromotion->setRecommendStatus($attributes['recommendStatus']);
        }
        if (isset($attributes['status'])) {
            $cityPromotion->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $cityPromotion->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $cityPromotion->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $cityPromotion->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $cityPromotion->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $cityPromotion;
    }

    public function objectToArray($cityPromotion, array $keys = array())
    {
        $expression = array();

        if (!$cityPromotion instanceof CityPromotion) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'title',
                'cover',
                'detail',
                'recommendStatus',
                'crew',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'cityPromotions'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $cityPromotion->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $cityPromotion->getTitle();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $cityPromotion->getCover();
        }
        if (in_array('detail', $keys)) {
            $attributes['detail'] = $cityPromotion->getDetail();
        }
        if (in_array('recommendStatus', $keys)) {
            $attributes['recommendStatus'] = $cityPromotion->getRecommendStatus();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $cityPromotion->getCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
