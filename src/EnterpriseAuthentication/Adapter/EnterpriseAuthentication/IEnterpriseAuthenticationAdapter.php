<?php
namespace Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IEnterpriseAuthenticationAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
