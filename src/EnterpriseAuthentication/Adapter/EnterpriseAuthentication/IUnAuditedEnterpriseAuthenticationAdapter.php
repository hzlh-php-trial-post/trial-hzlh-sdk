<?php
namespace Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;
use Trial\Common\Adapter\IApplyAbleAdapter;

//phpcs:ignore
interface IUnAuditedEnterpriseAuthenticationAdapter extends IFetchAbleAdapter, IResubmitAbleAdapter, IApplyAbleAdapter, IAsyncAdapter
{
}
