<?php
namespace Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Trial\EnterpriseAuthentication\Translator\UnAuditedEnterpriseAuthenticationRestfulTranslator; //phpcs:ignore
use Trial\EnterpriseAuthentication\Model\UnAuditedEnterpriseAuthentication;
use Trial\EnterpriseAuthentication\Model\NullUnAuditedEnterpriseAuthentication;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class UnAuditedEnterpriseAuthenticationRestfulAdapter extends GuzzleAdapter implements IUnAuditedEnterpriseAuthenticationAdapter //phpcs:ignore
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'OA_UNAUDIT_ENTERPRISE_AUTHENTICATION_LIST'=>[
            'fields'=>[
                'enterprises'=>
                'id,name,unifiedSocialCreditCode,contactsCellphone' //phpcs:ignore
            ],
            'include'=>'relation'
        ],
        'PORTAL_UNAUDIT_ENTERPRISE_AUTHENTICATION_LIST'=>[
            'fields'=>[],
            'include'=>'relation'
        ],

        'UNAUDIT_ENTERPRISE_AUTHENTICATION_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'relation'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new UnAuditedEnterpriseAuthenticationRestfulTranslator();
        $this->resource = 'unAuditedEnterpriseAuthentications';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => ENTERPRISE_EXIST
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullUnAuditedEnterpriseAuthentication::getInstance());
    }
    /**
     * [resubmitAction 重新认证]
     * @param  UnAuditedEnterpriseAuthentication $unAuditedEnterpriseAuthentication [认证信息对象]
     * @return [bool]                                   [返回类型]
     */
    protected function resubmitAction(UnAuditedEnterpriseAuthentication $unAuditedEnterpriseAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $unAuditedEnterpriseAuthentication,
            array(
                'type',
                'powerAttorney',
                'name',
                'cardId',
                'positivePhoto',
                'reversePhoto',
                'handheldPhoto',
                'handheldPhoto',
            )
        );
     
        $this->patch(
            $this->getResource().'/'.$unAuditedEnterpriseAuthentication->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($unAuditedEnterpriseAuthentication);
            return true;
        }

        return false;
    }
}
