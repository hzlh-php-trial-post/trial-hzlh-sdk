<?php
namespace Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

use Trial\EnterpriseAuthentication\Model\EnterpriseAuthentication;
use Trial\EnterpriseAuthentication\Model\NullEnterpriseAuthentication;
use Trial\EnterpriseAuthentication\Translator\EnterpriseAuthenticationRestfulTranslator;

class EnterpriseAuthenticationRestfulAdapter extends GuzzleAdapter implements IEnterpriseAuthenticationAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'OA_ENTERPRISE_AUTHENTICATION_LIST'=>[
            'fields'=>[
                'enterprise'=>
                    'id,name,unifiedSocialCreditCode,contactsCellphone'
            ],
            'include'=>'enterprise'
        ],
        'PORTAL_ENTERPRISE_AUTHENTICATION_LIST'=>[
            'fields'=>[
                'enterprise'=>
                    'id,name,unifiedSocialCreditCode,contactsCellphone'
                ],
            'include'=>'enterprise'
        ],
        'ENTERPRISE_AUTHENTICATION_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'enterprise'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new EnterpriseAuthenticationRestfulTranslator();
        $this->resource = 'enterpriseAuthentications';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullEnterpriseAuthentication::getInstance());
    }
    /**
     * [addAction 认证企业信息]
     * @param EnterpriseAuthentication $enterpriseAuthentication [企业对象]
     * @return [bool]                [返回类型]
     */
    protected function addAction(EnterpriseAuthentication $enterpriseAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $enterpriseAuthentication,
            array(
                'type',
                'powerAttorney',
                'name',
                'cardId',
                'positivePhoto',
                'reversePhoto',
                'handheldPhoto',
                'enterprise'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($enterpriseAuthentication);
            return true;
        }
        return false;
    }
    /**
     * [editAction 编辑企业信息]
     * @param  EnterpriseAuthentication $enterpriseAuthentication [企业对象]
     * @return [bool]                 [返回类型]
     */
    protected function editAction(EnterpriseAuthentication $enterpriseAuthentication) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $enterpriseAuthentication,
            array(
                'type',
                'powerAttorney',
                'name',
                'cardId',
                'positivePhoto',
                'reversePhoto',
                'handheldPhoto',
                'handheldPhoto',
            )
        );

        $this->patch(
            $this->getResource().'/'.$enterpriseAuthentication->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterpriseAuthentication);
            return true;
        }

        return false;
    }
}
