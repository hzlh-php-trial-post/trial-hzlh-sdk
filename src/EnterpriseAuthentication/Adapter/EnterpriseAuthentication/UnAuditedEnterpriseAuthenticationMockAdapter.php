<?php
namespace Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication;

use Trial\Common\Adapter\ApplyAbleMockAdapterTrait;

use Trial\Common\Model\IResubmitAble;

use Trial\EnterpriseAuthentication\Model\UnAuditedEnterpriseAuthentication;
use Trial\EnterpriseAuthentication\Utils\UnAuditedEnterpriseAuthenticationMockFactory;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class UnAuditedEnterpriseAuthenticationMockAdapter implements IUnAuditedEnterpriseAuthenticationAdapter
{
    use ApplyAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return UnAuditedEnterpriseAuthenticationMockFactory::generateUnAuditedEnterpriseAuthenticationObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedEnterpriseAuthenticationList = array();

        foreach ($ids as $id) {
            $unAuditedEnterpriseAuthenticationList[] = UnAuditedEnterpriseAuthenticationMockFactory::generateUnAuditedEnterpriseAuthenticationObject($id);//phpcs:ignore
        }

        return $unAuditedEnterpriseAuthenticationList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
    /**
     * [resubmitAction 重新认证]
     * @param  UnAuditedEnterpriseAuthentication $unAuditedEnterpriseAuthentication [认证信息对象]
     * @return [bool]                                   [返回类型]
     */
    public function resubmit(IResubmitAble $unAuditedEnterpriseAuthentication) : bool
    {
        unset($unAuditedEnterpriseAuthentication);
        return true;
    }

    public function fetchOneAsync(int $id)
    {
        return UnAuditedEnterpriseAuthenticationMockFactory::generateUnAuditedEnterpriseAuthenticationObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = UnAuditedEnterpriseAuthenticationMockFactory::generateUnAuditedEnterpriseAuthenticationObject($id);//phpcs:ignore
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
