<?php
namespace Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;

use Trial\EnterpriseAuthentication\Model\NullEnterpriseAuthentication;
use Trial\EnterpriseAuthentication\Utils\EnterpriseAuthenticationMockFactory;

class EnterpriseAuthenticationMockAdapter implements IEnterpriseAuthenticationAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return EnterpriseAuthenticationMockFactory::generateEnterpriseAuthenticationObject(NullEnterpriseAuthentication::getInstance(), $id);//phpcs:ignore
    }

    public function fetchList(array $ids) : array
    {
        $enterpriseList = array();

        foreach ($ids as $id) {
            $enterpriseList[] = EnterpriseAuthenticationMockFactory::generateEnterpriseAuthenticationObject(NullEnterpriseAuthentication::getInstance(), $id);//phpcs:ignore
        }

        return $enterpriseList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return EnterpriseAuthenticationMockFactory::generateEnterpriseAuthenticationObject(NullEnterpriseAuthentication::getInstance(), $id);//phpcs:ignore
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = EnterpriseAuthenticationMockFactory::generateEnterpriseAuthenticationObject(NullEnterpriseAuthentication::getInstance(), $id);//phpcs:ignore
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
