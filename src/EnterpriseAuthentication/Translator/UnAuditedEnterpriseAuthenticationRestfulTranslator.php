<?php
namespace Trial\EnterpriseAuthentication\Translator;

use Trial\EnterpriseAuthentication\Model\UnAuditedEnterpriseAuthentication;
use Trial\EnterpriseAuthentication\Model\NullUnAuditedEnterpriseAuthentication;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class UnAuditedEnterpriseAuthenticationRestfulTranslator extends EnterpriseAuthenticationRestfulTranslator
{
    public function arrayToObject(array $expression, $unAuditedEnterpriseAuthentication = null)
    {
        return $this->translateToObject($expression, $unAuditedEnterpriseAuthentication);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $unAuditedEnterpriseAuthentication = null)
    {
        if (empty($expression)) {
            return NullUnAuditedEnterpriseAuthentication::getInstance();
        }

        if ($unAuditedEnterpriseAuthentication == null) {
            $unAuditedEnterpriseAuthentication = new UnAuditedEnterpriseAuthentication();
        }

        $unAuditedEnterpriseAuthentication = parent::translateToObject($expression, $unAuditedEnterpriseAuthentication);

        $data =  $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['rejectReason'])) {
            $unAuditedEnterpriseAuthentication->setRejectReason($attributes['rejectReason']);
        }

        if (isset($attributes['applyStatus'])) {
            $unAuditedEnterpriseAuthentication->setApplyStatus($attributes['applyStatus']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['relation']['data'])) {
            $relation = $this->changeArrayFormat($relationships['relation']['data']);
            $unAuditedEnterpriseAuthentication->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($relation)
            );
        }

        return $unAuditedEnterpriseAuthentication;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($unAuditedEnterpriseAuthentication, array $keys = array())
    {
        $enterprise = parent::objectToArray($unAuditedEnterpriseAuthentication, $keys);
        
        if (!$unAuditedEnterpriseAuthentication instanceof UnAuditedEnterpriseAuthentication) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'type',
                'powerAttorney',
                'name',
                'cardId',
                'positivePhoto',
                'reversePhoto',
                'handheldPhoto',
                'rejectReason'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'unAuditedEnterpriseAuthentications'
            )
        );

        $attributes = array();

        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $unAuditedEnterpriseAuthentication->getRejectReason();
        }

        $expression['data']['attributes'] = array_merge($enterprise['data']['attributes'], $attributes);
        
        return $expression;
    }
}
