<?php
namespace Trial\EnterpriseAuthentication\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\EnterpriseAuthentication\Model\EnterpriseAuthentication;
use Trial\EnterpriseAuthentication\Model\LegalPersonInfo;
use Trial\EnterpriseAuthentication\Model\ContactsInfo;
use Trial\EnterpriseAuthentication\Model\NullEnterpriseAuthentication;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class EnterpriseAuthenticationRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseAuthenticationRestfulTranslator()
    {
        return new nterpriseRestfulTranslator();
    }

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $enterpriseAuthentication = null)
    {
        return $this->translateToObject($expression, $enterpriseAuthentication);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $enterpriseAuthentication = null)
    {
        if (empty($expression)) {
            return NullEnterpriseAuthentication::getInstance();
        }

        if ($enterpriseAuthentication == null) {
            $enterpriseAuthentication = new EnterpriseAuthentication();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $enterpriseAuthentication->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['enterpriseAuthenticationType'])) {
            $enterpriseAuthentication->setType($attributes['enterpriseAuthenticationType']);
        }
        if (isset($attributes['powerAttorney'])) {
            $enterpriseAuthentication->setPowerAttorney($attributes['powerAttorney']);
        }
        if (isset($attributes['name'])) {
            $enterpriseAuthentication->setName($attributes['name']);
        }
        if (isset($attributes['cardId'])) {
            $enterpriseAuthentication->setCardId($attributes['cardId']);
        }
        if (isset($attributes['positivePhoto'])) {
            $enterpriseAuthentication->setPositivePhoto($attributes['positivePhoto']);
        }
        if (isset($attributes['reversePhoto'])) {
            $enterpriseAuthentication->setReversePhoto($attributes['reversePhoto']);
        }
        if (isset($attributes['handheldPhoto'])) {
            $enterpriseAuthentication->setHandheldPhoto($attributes['handheldPhoto']);
        }
        if (isset($attributes['createTime'])) {
            $enterpriseAuthentication->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $enterpriseAuthentication->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $enterpriseAuthentication->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $enterpriseAuthentication->setStatusTime($attributes['statusTime']);
        }
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $enterpriseAuthentication->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }

        return $enterpriseAuthentication;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($enterpriseAuthentication, array $keys = array())
    {

        if (!$enterpriseAuthentication instanceof EnterpriseAuthentication) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'type',
                'powerAttorney',
                'name',
                'cardId',
                'positivePhoto',
                'reversePhoto',
                'handheldPhoto',
                'enterprise'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'enterpriseAuthentications'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $enterpriseAuthentication->getId();
        }

        $attributes = array();

        if (in_array('type', $keys)) {
            $attributes['type'] = $enterpriseAuthentication->getType();
        }
        if (in_array('powerAttorney', $keys)) {
            $attributes['powerAttorney'] = $enterpriseAuthentication->getPowerAttorney();
        }
        if (in_array('name', $keys)) {
            $attributes['name'] = $enterpriseAuthentication->getName();
        }
        if (in_array('cardId', $keys)) {
            $attributes['cardId'] = $enterpriseAuthentication->getCardId();
        }
        if (in_array('positivePhoto', $keys)) {
            $attributes['positivePhoto'] = $enterpriseAuthentication->getPositivePhoto();
        }
        if (in_array('reversePhoto', $keys)) {
            $attributes['reversePhoto'] = $enterpriseAuthentication->getReversePhoto();
        }
        if (in_array('handheldPhoto', $keys)) {
            $attributes['handheldPhoto'] = $enterpriseAuthentication->getHandheldPhoto();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprises',
                    'id' => $enterpriseAuthentication->getEnterprise()->getId(),
                )
             );
        }

        return $expression;
    }
}
