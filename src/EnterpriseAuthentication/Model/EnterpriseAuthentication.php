<?php
namespace Trial\EnterpriseAuthentication\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Enterprise\Model\Enterprise;

use Trial\EnterpriseAuthentication\Repository\EnterpriseAuthenticationRepository;

use Trial\Appointment\Model\IAppointmentAble;

class EnterpriseAuthentication implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    const STATUS_NORMAL = 0;

    const TYPE = array(
        'LEGAL_PERSON' => 1, //法人
        'AUTHORIZER' => 2 //授权人
    );
    private $id;

    private $type;

    /**
     * @var array $powerAttorney 授权委托书
     */
    private $powerAttorney;

    /**
     * @var string $enterpriseName 企业名称
     */
    private $enterpriseName;
    /**
     * @var string $name 法人/授权委托人姓名
     */
    private $name;
    /**
     * @var string $cardId 法人/授权委托人身份证号
     */
    private $cardId;
    /**
     * @var string $positivePhoto 法人/授权委托人身份证正面照
     */
    private $positivePhoto;
    /**
     * @var string $reversePhoto 法人/授权委托人身份证反面照
     */
    private $reversePhoto;
    /**
     * @var string $handheldPhoto 法人/授权委托人身份证手持照
     */
    private $handheldPhoto;
    /**
     * @var Enterprise $enterprise 企业
     */
    private $enterprise;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->type = self::TYPE['LEGAL_PERSON'];
        $this->powerAttorney = array();
        $this->enterpriseName = '';
        $this->name = '';
        $this->cardId = '';
        $this->positivePhoto = array();
        $this->reversePhoto = array();
        $this->handheldPhoto = array();
        $this->enterprise = new Enterprise();
        $this->status = self::STATUS_NORMAL;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new EnterpriseAuthenticationRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->type);
        unset($this->powerAttorney);
        unset($this->enterpriseName);
        unset($this->name);
        unset($this->cardId);
        unset($this->positivePhoto);
        unset($this->reversePhoto);
        unset($this->handheldPhoto);
        unset($this->enterprise);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setType(int $type) : void
    {
        $this->type = in_array($type, self::TYPE) ? $type : self::TYPE['LEGAL_PERSON'];
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function setPowerAttorney(array $powerAttorney) : void
    {
        $this->powerAttorney = $powerAttorney;
    }

    public function getPowerAttorney() : array
    {
        return $this->powerAttorney;
    }

    public function setEnterpriseName(string $enterpriseName) : void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName() : string
    {
        return $this->enterpriseName;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setCardId(string $cardId) : void
    {
        $this->cardId = $cardId;
    }

    public function getCardId() : string
    {
        return $this->cardId;
    }

    public function setPositivePhoto(array $positivePhoto) : void
    {
        $this->positivePhoto = $positivePhoto;
    }

    public function getPositivePhoto() : array
    {
        return $this->positivePhoto;
    }

    public function setReversePhoto(array $reversePhoto) : void
    {
        $this->reversePhoto = $reversePhoto;
    }

    public function getReversePhoto() : array
    {
        return $this->reversePhoto;
    }

    public function setHandheldPhoto(array $handheldPhoto) : void
    {
        $this->handheldPhoto = $handheldPhoto;
    }

    public function getHandheldPhoto() : array
    {
        return $this->handheldPhoto;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository()
    {
        return $this->repository;
    }
    
    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
