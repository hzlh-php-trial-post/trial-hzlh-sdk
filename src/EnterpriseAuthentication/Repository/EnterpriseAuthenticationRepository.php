<?php
namespace Trial\EnterpriseAuthentication\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication\IEnterpriseAuthenticationAdapter;
use Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication\EnterpriseAuthenticationMockAdapter;
use Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication\EnterpriseAuthenticationRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class EnterpriseAuthenticationRepository extends Repository implements IEnterpriseAuthenticationAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_ENTERPRISE_AUTHENTICATION_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_ENTERPRISE_AUTHENTICATION_LIST';
    const FETCH_ONE_MODEL_UN = 'ENTERPRISE_AUTHENTICATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new EnterpriseAuthenticationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IEnterpriseAuthenticationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IEnterpriseAuthenticationAdapter
    {
        return new EnterpriseAuthenticationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
