<?php
namespace Trial\EnterpriseAuthentication\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ResubmitAbleRepositoryTrait;
use Trial\Common\Repository\ApplyAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication\IUnAuditedEnterpriseAuthenticationAdapter;
use Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication\UnAuditedEnterpriseAuthenticationMockAdapter;
use Trial\EnterpriseAuthentication\Adapter\EnterpriseAuthentication\UnAuditedEnterpriseAuthenticationRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class UnAuditedEnterpriseAuthenticationRepository extends Repository implements IUnAuditedEnterpriseAuthenticationAdapter//phpcs:ignore
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_UNAUDIT_ENTERPRISE_AUTHENTICATION_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_UNAUDIT_ENTERPRISE_AUTHENTICATION_LIST';
    const FETCH_ONE_MODEL_UN = 'UNAUDIT_ENTERPRISE_AUTHENTICATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new UnAuditedEnterpriseAuthenticationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IUnAuditedEnterpriseAuthenticationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IUnAuditedEnterpriseAuthenticationAdapter
    {
        return new UnAuditedEnterpriseAuthenticationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
