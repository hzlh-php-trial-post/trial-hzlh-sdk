<?php
namespace Trial\ServiceCategory\Adapter\ServiceCategory;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IParentCategoryAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
