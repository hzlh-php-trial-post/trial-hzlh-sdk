<?php
namespace Trial\ServiceCategory\Adapter\ServiceCategory;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

interface IServiceCategoryAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IEnableAbleAdapter
{
}
