<?php
namespace Trial\ServiceCategory\Model;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IEnableAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\EnableAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Trial\ServiceCategory\Repository\ServiceCategoryRepository;

class ServiceCategory implements IObject, IOperatAble, IEnableAble
{
    use OperatAbleTrait, EnableAbleTrait, Object;

    const IS_QUALIFICATION = array(
        'NO' => 0,
        'YES' => 2
    );

    const IS_ENTERPRISE_VERIFY = array(
        'NO' => 0,
        'YES' => 2
    );

    private $id;

    private $name;

    private $parentCategory;

    private $isQualification;

    private $isEnterpriseVerify;

    private $qualificationName;

    private $commission;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->parentCategory = new ParentCategory();
        $this->isQualification = self::IS_QUALIFICATION['NO'];
        $this->isEnterpriseVerify = self::IS_ENTERPRISE_VERIFY['YES'];
        $this->qualificationName = '';
        $this->commission = 0.00;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->parentCategory);
        unset($this->isQualification);
        unset($this->isEnterpriseVerify);
        unset($this->qualificationName);
        unset($this->commission);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setParentCategory(ParentCategory $parentCategory) : void
    {
        $this->parentCategory = $parentCategory;
    }

    public function getParentCategory() : ParentCategory
    {
        return $this->parentCategory;
    }

    public function setIsQualification(int $isQualification) : void
    {
        $this->isQualification =
            in_array($isQualification, self::IS_QUALIFICATION) ?
            $isQualification : self::IS_QUALIFICATION['NO'];
    }

    public function getIsQualification() : int
    {
        return $this->isQualification;
    }

    public function setIsEnterpriseVerify(int $isEnterpriseVerify) : void
    {
        $this->isEnterpriseVerify =
        in_array($isEnterpriseVerify, self::IS_ENTERPRISE_VERIFY) ?
            $isEnterpriseVerify :
            self::IS_ENTERPRISE_VERIFY['YES'];
    }

    public function getIsEnterpriseVerify() : int
    {
        return $this->isEnterpriseVerify;
    }

    public function setQualificationName(string $qualificationName) : void
    {
        $this->qualificationName = $qualificationName;
    }

    public function getQualificationName() : string
    {
        return $this->qualificationName;
    }

    public function setCommission(float $commission) : void
    {
        $this->commission = $commission;
    }

    public function getCommission() : float
    {
        return $this->commission;
    }

    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }
}
