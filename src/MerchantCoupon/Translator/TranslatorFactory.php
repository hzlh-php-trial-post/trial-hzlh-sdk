<?php
namespace Trial\MerchantCoupon\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\MerchantCoupon\Model\MerchantCoupon;
use Trial\TradeRecord\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        MerchantCoupon::RELEASE_TYPE['PLATFORM'] =>
        'Trial\Crew\Translator\CrewRestfulTranslator',
        MerchantCoupon::RELEASE_TYPE['MERCHANT'] =>
        'Trial\Enterprise\Translator\EnterpriseRestfulTranslator',
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
