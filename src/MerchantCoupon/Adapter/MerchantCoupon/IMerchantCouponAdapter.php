<?php
namespace Trial\MerchantCoupon\Adapter\MerchantCoupon;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\MerchantCoupon\Model\MerchantCoupon;

interface IMerchantCouponAdapter extends IFetchAbleAdapter, IOperatAbleAdapter
{
    public function deletes(MerchantCoupon $merchantCoupon) : bool;

    public function discontinue(MerchantCoupon $merchantCoupon) : bool;
}
