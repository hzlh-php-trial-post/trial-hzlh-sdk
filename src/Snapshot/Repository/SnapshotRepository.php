<?php
namespace Trial\Snapshot\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\Snapshot\Adapter\Snapshot\ISnapshotAdapter;
use Trial\Snapshot\Adapter\Snapshot\SnapshotMockAdapter;
use Trial\Snapshot\Adapter\Snapshot\SnapshotRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Snapshot\Model\Snapshot;

class SnapshotRepository extends Repository implements ISnapshotAdapter
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'SNAPSHOT_LIST';
    const FETCH_ONE_MODEL_UN = 'SNAPSHOT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new SnapshotRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ISnapshotAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ISnapshotAdapter
    {
        return new SnapshotMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
