<?php
namespace Trial\Snapshot\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\NullTranslator;

use Trial\Snapshot\Model\ISnapshotAble;

class TranslatorFactory
{
    const MAPS = array(
        ISnapshotAble::CATEGORY['SERVICE']=>
        'Trial\Service\Translator\ServiceRestfulTranslator',
        ISnapshotAble::CATEGORY['DELIVERY_ADDRESS']=>
        'Trial\DeliveryAddress\Translator\DeliveryAddressRestfulTranslator',
        ISnapshotAble::CATEGORY['LOAN_PRODUCT']=>
        'Trial\LoanProduct\Translator\LoanProductRestfulTranslator'
    );

    public function getTranslator(int $category) : ITranslator
    {
        $translator = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
