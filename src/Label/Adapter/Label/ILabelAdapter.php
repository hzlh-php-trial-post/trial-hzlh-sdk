<?php
namespace Trial\Label\Adapter\Label;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface ILabelAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
