<?php
namespace Trial\Label\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\Label\Adapter\Label\ILabelAdapter;
use Trial\Label\Adapter\Label\LabelMockAdapter;
use Trial\Label\Adapter\Label\LabelRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class LabelRepository extends Repository implements ILabelAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'LABEL_LIST';
    const FETCH_ONE_MODEL_UN = 'LABEL_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new LabelRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ILabelAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ILabelAdapter
    {
        return new LabelMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
