<?php
namespace Trial\LoanRequirement\Adapter\LoanRequirement;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\LoanRequirement\Model\LoanRequirement;
use Trial\LoanRequirement\Model\TenderInformation;
use Trial\LoanRequirement\Model\NullLoanRequirement;
use Trial\LoanRequirement\Translator\LoanRequirementRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class LoanRequirementRestfulAdapter extends GuzzleAdapter implements ILoanRequirementAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_LOAN_REQUIREMENT_LIST'=>[
                'fields'=>[],
                'include'=> 'member,loanPurpose,collateral,tenderInformations,tenderInformations.enterprise'
            ],
            'OA_LOAN_REQUIREMENT_LIST'=>[
                'fields'=>[],
                'include'=> 'member,loanPurpose,collateral,tenderInformations,tenderInformations.enterprise'
            ],
            'LOAN_REQUIREMENT_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'member,applicant,loanPurpose,,collateral,tenderInformations,tenderInformations.enterprise,tenderInformations.transactionInfo,tenderInformations.loanResultInfo,labels'//phpcs:ignore
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new LoanRequirementRestfulTranslator();
        $this->resource = 'loanRequirements';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullLoanRequirement::getInstance());
    }

    protected function addAction(LoanRequirement $loanRequirement) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanRequirement,
            array(
                'title',
                'description',
                'loanObject',
                'contactName',
                'contactPhone',
                'validityTime',
                'area',
                'loanPurpose',
                'loanPurposeDescribe',
                'collateralDescribe',
                'loanAmount',
                'loanTerm',
                'isHaveCollateral',
                'collateral',
                'isAllowViewCreditInformation',
                'attachments',
                'labels',
                'member'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }

    protected function resubmitAction(LoanRequirement $loanRequirement) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanRequirement,
            array(
                'title',
                'description',
                'loanObject',
                'contactName',
                'contactPhone',
                'validityTime',
                'area',
                'loanPurpose',
                'loanPurposeDescribe',
                'collateralDescribe',
                'loanAmount',
                'loanTerm',
                'isHaveCollateral',
                'collateral',
                'isAllowViewCreditInformation',
                'attachments',
                'labels'
            )
        );

        $this->patch(
            $this->getResource().'/'.$loanRequirement->getId().'/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }

    protected function editAction(LoanRequirement $loanRequirement)
    {
        unset($loanRequirement);

        return false;
    }

    public function deletes(LoanRequirement $loanRequirement) : bool
    {
        $this->patch(
            $this->getResource().'/'.$loanRequirement->getId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }

    public function revoke(LoanRequirement $loanRequirement) : bool
    {
        $this->patch(
            $this->getResource().'/'.$loanRequirement->getId().'/revoke'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }

    public function close(LoanRequirement $loanRequirement) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanRequirement,
            array(
                'closeReason'
            )
        );

        $this->patch(
            $this->getResource().'/'.$loanRequirement->getId().'/close',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }

    public function bid(LoanRequirement $loanRequirement) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanRequirement,
            array(
                'tenderInformations'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $loanRequirement->getId() . '/bid',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }

    public function matching(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        $this->patch(
            $this->getResource() . '/' . $loanRequirement->getId() . '/matching/'.$tenderInformation->getId()
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }

    public function transaction(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanRequirement,
            array(
                'transactionInfo',
            )
        );

        $this->patch(
            $this->getResource() . '/' . $loanRequirement->getId() . '/transaction/'.$tenderInformation->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }

    public function completed(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $loanRequirement,
            array(
                'loanResultInfo'
            )
        );

        $this->patch(
            $this->getResource() . '/' . $loanRequirement->getId() . '/completed/'.$tenderInformation->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($loanRequirement);
            return true;
        }

        return false;
    }
}
