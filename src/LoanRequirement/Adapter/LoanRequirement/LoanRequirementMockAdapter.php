<?php
namespace Trial\LoanRequirement\Adapter\LoanRequirement;

use Trial\Common\Adapter\ApplyAbleMockAdapterTrait;
use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleMockAdapterTrait;

use Trial\LoanRequirement\Utils\MockFactory;

use Trial\LoanRequirement\Model\LoanRequirement;
use Trial\LoanRequirement\Model\TenderInformation;

/**
 * @SuppressWarnings(PHPMD)
 */
class LoanRequirementMockAdapter implements ILoanRequirementAdapter
{
    use ApplyAbleMockAdapterTrait, ResubmitAbleMockAdapterTrait, OperatAbleMockAdapterTrait;

    public function deletes(LoanRequirement $loanRequirement) : bool
    {
        unset($loanRequirement);
        return true;
    }

    public function revoke(LoanRequirement $loanRequirement) : bool
    {
        unset($loanRequirement);
        return true;
    }

    public function close(LoanRequirement $loanRequirement) : bool
    {
        unset($loanRequirement);
        return true;
    }

    public function bid(LoanRequirement $loanRequirement) : bool
    {
        unset($loanRequirement);
        return true;
    }

    public function matching(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        unset($loanRequirement);
        unset($tenderInformation);
        return true;
    }

    public function transaction(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        unset($loanRequirement);
        unset($tenderInformation);
        return true;
    }

    public function completed(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        unset($loanRequirement);
        unset($tenderInformation);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generateLoanRequirementObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $loanRequirementList = array();

        foreach ($ids as $id) {
            $loanRequirementList[] = MockFactory::generateLoanRequirementObject($id);
        }

        return $loanRequirementList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateLoanRequirementObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $loanRequirementList = array();

        foreach ($ids as $id) {
            $loanRequirementList[] = MockFactory::generateLoanRequirementObject($id);
        }

        return $loanRequirementList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
