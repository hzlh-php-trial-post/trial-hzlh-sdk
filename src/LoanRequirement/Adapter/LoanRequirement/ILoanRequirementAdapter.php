<?php
namespace Trial\LoanRequirement\Adapter\LoanRequirement;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;
use Trial\Common\Adapter\IApplyAbleAdapter;

use Trial\LoanRequirement\Model\LoanRequirement;
use Trial\LoanRequirement\Model\TenderInformation;

interface ILoanRequirementAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IResubmitAbleAdapter, IAsyncAdapter, IApplyAbleAdapter  //phpcs:ignore
{
    public function deletes(LoanRequirement $loanRequirement) : bool;

    public function revoke(LoanRequirement $loanRequirement) : bool;

    public function close(LoanRequirement $loanRequirement) : bool;

    public function bid(LoanRequirement $loanRequirement) : bool;

    public function matching(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool;

    public function transaction(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool;

    public function completed(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool;
}
