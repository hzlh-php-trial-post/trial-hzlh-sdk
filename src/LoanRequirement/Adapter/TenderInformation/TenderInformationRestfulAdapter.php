<?php
namespace Trial\LoanRequirement\Adapter\TenderInformation;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\LoanRequirement\Model\TenderInformation;
use Trial\LoanRequirement\Model\NullTenderInformation;
use Trial\LoanRequirement\Translator\TenderInformationRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class TenderInformationRestfulAdapter extends GuzzleAdapter implements ITenderInformationAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_TENDER_INFORMATION_LIST'=>[
                'fields'=>[],
                'include'=> 'enterprise'
            ],
            'OA_TENDER_INFORMATION_LIST'=>[
                'fields'=>[],
                'include'=> 'enterprise'
            ],
            'TENDER_INFORMATION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'enterprise'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new TenderInformationRestfulTranslator();
        $this->resource = 'tenderInformations';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullTenderInformation::getInstance());
    }
}
