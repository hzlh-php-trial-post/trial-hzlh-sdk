<?php
namespace Trial\LoanRequirement\Adapter\TenderInformation;

use Trial\LoanRequirement\Utils\MockFactory;

class TenderInformationMockAdapter implements ITenderInformationAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateTenderInformationObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $tenderInformationList = array();

        foreach ($ids as $id) {
            $tenderInformationList[] = MockFactory::generateTenderInformationObject($id);
        }

        return $tenderInformationList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateTenderInformationObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $tenderInformationList = array();

        foreach ($ids as $id) {
            $tenderInformationList[] = MockFactory::generateTenderInformationObject($id);
        }

        return $tenderInformationList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
