<?php
namespace Trial\LoanRequirement\Adapter\TenderInformation;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;

interface ITenderInformationAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
