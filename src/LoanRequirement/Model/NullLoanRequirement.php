<?php
namespace Trial\LoanRequirement\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullApplyAbleTrait;
use Trial\Common\Model\NullOperatAbleTrait;
use Trial\Common\Model\NullResubmitAbleTrait;

class NullLoanRequirement extends LoanRequirement implements INull
{
    use NullApplyAbleTrait, NullOperatAbleTrait, NullResubmitAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function deletes(): bool
    {
        return $this->resourceNotExist();
    }

    public function revoke(): bool
    {
        return $this->resourceNotExist();
    }

    public function close(): bool
    {
        return $this->resourceNotExist();
    }

    public function bid(): bool
    {
        return $this->resourceNotExist();
    }

    public function matching($tenderInformation): bool
    {
        unset($tenderInformation);
        return $this->resourceNotExist();
    }

    public function transaction($tenderInformation): bool
    {
        unset($tenderInformation);
        return $this->resourceNotExist();
    }

    public function completed($tenderInformation): bool
    {
        unset($tenderInformation);
        return $this->resourceNotExist();
    }
}
