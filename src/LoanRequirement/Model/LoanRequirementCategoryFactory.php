<?php
namespace Trial\LoanRequirement\Model;

use Trial\Common\Model\IApplyAble;

class LoanRequirementCategoryFactory
{
    const LOAN_REQUIREMENT_STATUS_CN = array(
        LoanRequirement::LOAN_REQUIREMENT_STATUS['NORMAL'] =>'待匹配',
        LoanRequirement::LOAN_REQUIREMENT_STATUS['MATCHED'] =>'待办理',
        LoanRequirement::LOAN_REQUIREMENT_STATUS['HANDLED'] =>'待办理',
        LoanRequirement::LOAN_REQUIREMENT_STATUS['COMPLETED_LOAN_SUCCESS'] =>'已完成',
        LoanRequirement::LOAN_REQUIREMENT_STATUS['COMPLETED_LOAN_FAIL'] =>'已完成',
        LoanRequirement::LOAN_REQUIREMENT_STATUS['REVOKED'] =>'已撤销',
        LoanRequirement::LOAN_REQUIREMENT_STATUS['CLOSED'] =>'已关闭',
        LoanRequirement::LOAN_REQUIREMENT_STATUS['DELETED'] =>'已删除',
    );

    const LOAN_REQUIREMENT_APPLY_STATUS_CN = array(
        IApplyAble::APPLY_STATUS['PENDING'] =>'待审核',
        IApplyAble::APPLY_STATUS['APPROVE'] =>'已通过',
        IApplyAble::APPLY_STATUS['REJECT'] =>'已驳回',
    );

    const LOAN_RESULT_STATUS_CN = array(
        LoanResultInfo::LOAN_RESULT_STATUS['PENDING'] => '待处理',
        LoanResultInfo::LOAN_RESULT_STATUS['SUCCESS'] => '成功',
        LoanResultInfo::LOAN_RESULT_STATUS['FAIL'] => '失败'
    );

    const IS_HAVE_COLLATERAL_CN = array(
        LoanRequirement::IS_HAVE_COLLATERAL['YES'] =>'是',
        LoanRequirement::IS_HAVE_COLLATERAL['NO'] =>'否'
    );

    const IS_ALLOW_VIEW_CREDIT_INFORMATION_CN = array(
        LoanRequirement::IS_ALLOW_VIEW_CREDIT_INFORMATION['YES'] =>'是',
        LoanRequirement::IS_ALLOW_VIEW_CREDIT_INFORMATION['NO'] =>'否'
    );

    const LOAN_OBJECT_CN = array(
        LoanRequirement::LOAN_OBJECT['ENTERPRISE'] => '企业',
        LoanRequirement::LOAN_OBJECT['NATURAL_PERSON'] => '个人'
    );

    const TENDER_INFORMATION_STATUS_CN = array(
        TenderInformation::TENDER_INFORMATION_STATUS['BID'] =>'待匹配',
        TenderInformation::TENDER_INFORMATION_STATUS['MATCHED'] =>'待办理',
        TenderInformation::TENDER_INFORMATION_STATUS['HANDLED'] =>'待办理',
        TenderInformation::TENDER_INFORMATION_STATUS['COMPLETED_LOAN_SUCCESS'] =>'已完成',
        TenderInformation::TENDER_INFORMATION_STATUS['COMPLETED_LOAN_FAIL'] =>'已完成',
        TenderInformation::TENDER_INFORMATION_STATUS['REVOKED'] =>'已撤销',
        TenderInformation::TENDER_INFORMATION_STATUS['CLOSED'] =>'已关闭',
        TenderInformation::TENDER_INFORMATION_STATUS['DELETED'] =>'已删除',
        TenderInformation::TENDER_INFORMATION_STATUS['INVALID'] =>'已失效',
    );
}
