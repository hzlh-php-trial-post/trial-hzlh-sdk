<?php
namespace Trial\LoanRequirement\Model;

use Marmot\Core;

class TransactionInfo
{
    private $id;

    private $transactionTime;

    private $transactionArea;

    private $transactionAddress;

    private $contactName;

    private $contactPhone;

    private $remark;

    private $carryData;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->transactionTime = '';
        $this->transactionArea = '';
        $this->transactionAddress = '';
        $this->contactName = '';
        $this->contactPhone = '';
        $this->remark = '';
        $this->carryData = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->transactionTime);
        unset($this->transactionArea);
        unset($this->transactionAddress);
        unset($this->contactName);
        unset($this->contactPhone);
        unset($this->remark);
        unset($this->carryData);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setTransactionTime(string $transactionTime) : void
    {
        $this->transactionTime = $transactionTime;
    }

    public function getTransactionTime() : string
    {
        return $this->transactionTime;
    }
    
    public function setTransactionArea(string $transactionArea) : void
    {
        $this->transactionArea = $transactionArea;
    }

    public function getTransactionArea() : string
    {
        return $this->transactionArea;
    }
    
    public function setTransactionAddress(string $transactionAddress) : void
    {
        $this->transactionAddress = $transactionAddress;
    }

    public function getTransactionAddress() : string
    {
        return $this->transactionAddress;
    }
    
    public function setContactName(string $contactName) : void
    {
        $this->contactName = $contactName;
    }

    public function getContactName() : string
    {
        return $this->contactName;
    }
    
    public function setContactPhone(string $contactPhone) : void
    {
        $this->contactPhone = $contactPhone;
    }

    public function getContactPhone() : string
    {
        return $this->contactPhone;
    }
    
    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }
    
    public function setCarryData(string $carryData) : void
    {
        $this->carryData = $carryData;
    }

    public function getCarryData() : string
    {
        return $this->carryData;
    }
}
