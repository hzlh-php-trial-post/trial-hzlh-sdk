<?php
namespace Trial\LoanRequirement\Model;

use Marmot\Core;

use Trial\Enterprise\Model\Enterprise;

class TenderInformation
{
    const TENDER_INFORMATION_STATUS = array(
        'BID' => 0, //接受邀约/待匹配
        'MATCHED' => 2, //已匹配/待办理
        'HANDLED' => 4, //已办理/待办理
        'COMPLETED_LOAN_SUCCESS' => 6, //完成-贷款成功
        'COMPLETED_LOAN_FAIL' => 8, //完成-贷款失败
        'REVOKED' => -2, //撤销
        'CLOSED' => -4, //关闭
        'DELETED' => -6, //删除
        'INVALID' => -8, //失效
    );

    private $id;

    private $enterprise;

    private $creditMinLine;

    private $creditMaxLine;

    private $creditMinLoanTerm;

    private $creditMaxLoanTerm;

    private $creditValidityTime;

    private $status;

    /**
     * @var TransactionInfo $transactionInfo 办理信息
     */
    private $transactionInfo;

    /**
     * @var LoanResultInfo $loanResultInfo 贷款结果信息
     */
    private $loanResultInfo;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->enterprise = new Enterprise();
        $this->creditMinLine = 0.0;
        $this->creditMaxLine = 0.0;
        $this->creditMinLoanTerm = 0;
        $this->creditMaxLoanTerm = 0;
        $this->creditValidityTime = 0;
        $this->transactionInfo = new TransactionInfo();
        $this->loanResultInfo = new LoanResultInfo();
        $this->status = self::TENDER_INFORMATION_STATUS['BID'];
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->enterprise);
        unset($this->creditMinLine);
        unset($this->creditMaxLine);
        unset($this->creditMinLoanTerm);
        unset($this->creditMaxLoanTerm);
        unset($this->creditValidityTime);
        unset($this->transactionInfo);
        unset($this->loanResultInfo);
        unset($this->status);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setCreditMinLine(float $creditMinLine) : void
    {
        $this->creditMinLine = $creditMinLine;
    }

    public function getCreditMinLine() : float
    {
        return $this->creditMinLine;
    }

    public function setCreditMaxLine(float $creditMaxLine) : void
    {
        $this->creditMaxLine = $creditMaxLine;
    }

    public function getCreditMaxLine() : float
    {
        return $this->creditMaxLine;
    }
    
    public function setCreditMinLoanTerm(int $creditMinLoanTerm) : void
    {
        $this->creditMinLoanTerm = $creditMinLoanTerm;
    }

    public function getCreditMinLoanTerm() : int
    {
        return $this->creditMinLoanTerm;
    }
    
    public function setCreditMaxLoanTerm(int $creditMaxLoanTerm) : void
    {
        $this->creditMaxLoanTerm = $creditMaxLoanTerm;
    }

    public function getCreditMaxLoanTerm() : int
    {
        return $this->creditMaxLoanTerm;
    }
    
    public function setCreditValidityTime(int $creditValidityTime) : void
    {
        $this->creditValidityTime = $creditValidityTime;
    }

    public function getCreditValidityTime() : int
    {
        return $this->creditValidityTime;
    }
    
    public function setTransactionInfo(TransactionInfo $transactionInfo) : void
    {
        $this->transactionInfo = $transactionInfo;
    }

    public function getTransactionInfo() : TransactionInfo
    {
        return $this->transactionInfo;
    }

    public function setLoanResultInfo(LoanResultInfo $loanResultInfo) : void
    {
        $this->loanResultInfo = $loanResultInfo;
    }

    public function getLoanResultInfo() : LoanResultInfo
    {
        return $this->loanResultInfo;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::TENDER_INFORMATION_STATUS) ?
                        $status :
                        self::TENDER_INFORMATION_STATUS['BID'];
    }

    public function getStatus() : int
    {
        return $this->status;
    }
}
