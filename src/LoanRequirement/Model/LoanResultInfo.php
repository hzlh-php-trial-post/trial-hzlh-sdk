<?php
namespace Trial\LoanRequirement\Model;

use Marmot\Core;

class LoanResultInfo
{
    const LOAN_RESULT_STATUS = array(
        'PENDING' => 0, //待审核
        'SUCCESS' => 2, //成功
        'FAIL' => -2 //失败
    );

    private $id;

    private $repaymentMethod;

    private $loanAmount;

    private $loanTerm;

    private $loanFailReason;

    private $status;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->repaymentMethod = 0;
        $this->loanAmount = 0.0;
        $this->loanTerm = 0;
        $this->loanFailReason = '';
        $this->status = self::LOAN_RESULT_STATUS['PENDING'];
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->repaymentMethod);
        unset($this->loanAmount);
        unset($this->loanTerm);
        unset($this->loanFailReason);
        unset($this->status);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setRepaymentMethod(int $repaymentMethod) : void
    {
        $this->repaymentMethod = $repaymentMethod;
    }

    public function getRepaymentMethod() : int
    {
        return $this->repaymentMethod;
    }
    
    public function setLoanAmount(float $loanAmount) : void
    {
        $this->loanAmount = $loanAmount;
    }

    public function getLoanAmount() : float
    {
        return $this->loanAmount;
    }
    
    public function setLoanTerm(int $loanTerm) : void
    {
        $this->loanTerm = $loanTerm;
    }

    public function getLoanTerm() : int
    {
        return $this->loanTerm;
    }

    public function setLoanFailReason(string $loanFailReason) : void
    {
        $this->loanFailReason = $loanFailReason;
    }

    public function getLoanFailReason() : string
    {
        return $this->loanFailReason;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::LOAN_RESULT_STATUS) ?
                        $status :
                        self::LOAN_RESULT_STATUS['PENDING'];
    }

    public function getStatus() : int
    {
        return $this->status;
    }
}
