<?php
namespace Trial\LoanRequirement\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IApplyAble;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IResubmitAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\ApplyAbleTrait;
use Trial\Common\Model\ResubmitAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;

use Trial\LoanRequirement\Repository\LoanRequirementRepository;

use Trial\Member\Model\Member;
use Trial\Dictionary\Model\Dictionary;

use Trial\Enterprise\Model\Enterprise;
use Trial\Enterprise\Model\NullEnterprise;

use Trial\Label\Model\Label;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class LoanRequirement implements IObject, IApplyAble, IOperatAble, IResubmitAble
{
    use Object, OperatAbleTrait, ResubmitAbleTrait, ApplyAbleTrait;

    const LOAN_REQUIREMENT_STATUS = array(
        'NORMAL' => 0, //正常/待处理
        'MATCHED' => 2, //已匹配/待办理
        'HANDLED' => 4, //已办理/待办理
        'COMPLETED_LOAN_SUCCESS' => 6, //完成-贷款成功
        'COMPLETED_LOAN_FAIL' => 8, //完成-贷款失败
        'REVOKED' => -2, //撤销
        'CLOSED' => -4, //关闭
        'DELETED' => -6, //删除
    );

    const LOAN_OBJECT = array(
        'ENTERPRISE' => 1, //企业
        'NATURAL_PERSON' => 2 //个人
    );

    const IS_HAVE_COLLATERAL = array(
        'YES' => 0, //是
        'NO' => -2 //否
    );

    const IS_ALLOW_VIEW_CREDIT_INFORMATION = array(
        'YES' => 0, //是
        'NO' => -2 //否
    );

    private $id;

    private $number;

    private $title;

    private $description;

    private $borrowerName;

    private $applicant;

    private $loanObject;

    private $contactName;

    private $contactPhone;

    private $area;

    private $validityTime;

    private $loanPurpose;

    private $loanPurposeDescribe;

    private $loanAmount;

    private $loanTerm;

    private $isHaveCollateral;

    private $collateral;

    private $collateralDescribe;

    private $isAllowViewCreditInformation;

    private $attachments;

    private $labels;

    private $member;

    private $tenderInformations;
    /**
     * @var TransactionInfo $transactionInfo 办理信息
     */
    private $transactionInfo;

    /**
     * @var LoanResultInfo $loanResultInfo 贷款结果信息
     */
    private $loanResultInfo;

    private $winBiddingEnterprise;

    private $closeReason;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->number = '';
        $this->title = '';
        $this->description = '';
        $this->borrowerName = '';
        $this->loanObject = self::LOAN_OBJECT['ENTERPRISE'];
        $this->applicant =  NullEnterprise::getInstance();
        $this->contactName = '';
        $this->contactPhone = '';
        $this->area = '';
        $this->validityTime = 0;
        $this->loanPurpose = new Dictionary();
        $this->loanPurposeDescribe = '';
        $this->loanAmount = 0.0;
        $this->loanTerm = 0;
        $this->isHaveCollateral = self::IS_HAVE_COLLATERAL['YES'];
        $this->collateral = new Dictionary();
        $this->collateralDescribe = '';
        $this->isAllowViewCreditInformation = self::IS_ALLOW_VIEW_CREDIT_INFORMATION['YES'];
        $this->attachments = array();
        $this->labels = array();
        $this->closeReason = '';
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->applyStatus = IApplyAble::APPLY_STATUS['PENDING'];
        $this->rejectReason = '';
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::LOAN_REQUIREMENT_STATUS['NORMAL'];
        $this->tenderInformations = array();
        $this->transactionInfo = new TransactionInfo();
        $this->loanResultInfo = new LoanResultInfo();
        $this->winBiddingEnterprise = new Enterprise();
        $this->repository = new LoanRequirementRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->title);
        unset($this->description);
        unset($this->borrowerName);
        unset($this->applicant);
        unset($this->loanObject);
        unset($this->contactName);
        unset($this->contactPhone);
        unset($this->area);
        unset($this->validityTime);
        unset($this->loanPurpose);
        unset($this->loanPurposeDescribe);
        unset($this->loanAmount);
        unset($this->loanTerm);
        unset($this->isHaveCollateral);
        unset($this->collateral);
        unset($this->collateralDescribe);
        unset($this->isAllowViewCreditInformation);
        unset($this->attachments);
        unset($this->labels);
        unset($this->closeReason);
        unset($this->member);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->tenderInformations);
        unset($this->transactionInfo);
        unset($this->loanResultInfo);
        unset($this->winBiddingEnterprise);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setBorrowerName(string $borrowerName) : void
    {
        $this->borrowerName = $borrowerName;
    }

    public function getBorrowerName() : string
    {
        return $this->borrowerName;
    }

    public function setAttachments(array $attachments) : void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments() : array
    {
        return $this->attachments;
    }

    public function setLoanObject(int $loanObject) : void
    {
        $this->loanObject = in_array($loanObject, self::LOAN_OBJECT) ?
                        $loanObject :
                        self::LOAN_OBJECT['ENTERPRISE'];
    }

    public function getLoanObject() : int
    {
        return $this->loanObject;
    }

    public function setApplicant($applicant)
    {
        $this->applicant = $applicant;
    }

    public function getApplicant()
    {
        return $this->applicant;
    }

    public function setContactName(string $contactName) : void
    {
        $this->contactName = $contactName;
    }

    public function getContactName() : string
    {
        return $this->contactName;
    }

    public function setContactPhone(string $contactPhone) : void
    {
        $this->contactPhone = $contactPhone;
    }

    public function getContactPhone() : string
    {
        return $this->contactPhone;
    }

    public function setValidityTime(int $validityTime) : void
    {
        $this->validityTime = $validityTime;
    }

    public function getValidityTime() : int
    {
        return $this->validityTime;
    }

    public function setArea(string $area) : void
    {
        $this->area = $area;
    }

    public function getArea() : string
    {
        return $this->area;
    }

    public function setLoanPurpose(Dictionary $loanPurpose) : void
    {
        $this->loanPurpose = $loanPurpose;
    }

    public function getLoanPurpose() : Dictionary
    {
        return $this->loanPurpose;
    }

    public function setLoanPurposeDescribe(string $loanPurposeDescribe) : void
    {
        $this->loanPurposeDescribe = $loanPurposeDescribe;
    }

    public function getLoanPurposeDescribe() : string
    {
        return $this->loanPurposeDescribe;
    }
    
    public function setLoanAmount(float $loanAmount) : void
    {
        $this->loanAmount = $loanAmount;
    }

    public function getLoanAmount() : float
    {
        return $this->loanAmount;
    }

    public function setLoanTerm(int $loanTerm) : void
    {
        $this->loanTerm = $loanTerm;
    }

    public function getLoanTerm() : int
    {
        return $this->loanTerm;
    }

    public function setIsHaveCollateral(int $isHaveCollateral) : void
    {
        $this->isHaveCollateral = in_array($isHaveCollateral, self::IS_HAVE_COLLATERAL) ?
                        $isHaveCollateral :
                        self::IS_HAVE_COLLATERAL['YES'];
    }

    public function getIsHaveCollateral() : int
    {
        return $this->isHaveCollateral;
    }

    public function setCollateral(Dictionary $collateral) : void
    {
        $this->collateral = $collateral;
    }

    public function getCollateral() : Dictionary
    {
        return $this->collateral;
    }

    public function setCollateralDescribe(string $collateralDescribe) : void
    {
        $this->collateralDescribe = $collateralDescribe;
    }

    public function getCollateralDescribe() : string
    {
        return $this->collateralDescribe;
    }

    public function setIsAllowViewCreditInformation(int $isAllowViewCreditInformation) : void
    {
        $this->isAllowViewCreditInformation = in_array(
            $isAllowViewCreditInformation,
            self::IS_ALLOW_VIEW_CREDIT_INFORMATION
        ) ? $isAllowViewCreditInformation : self::IS_ALLOW_VIEW_CREDIT_INFORMATION['YES'];
    }

    public function getIsAllowViewCreditInformation() : int
    {
        return $this->isAllowViewCreditInformation;
    }

    public function addLabel(Label $label) : void
    {
        $this->labels[] = $label;
    }

    public function clearLabels() : void
    {
        $this->labels = [];
    }

    public function getLabels() : array
    {
        return $this->labels;
    }

    public function setCloseReason(string $closeReason) : void
    {
        $this->closeReason = $closeReason;
    }

    public function getCloseReason() : string
    {
        return $this->closeReason;
    }

    public function setRejectReason(string $rejectReason): void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }
    
    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function addTenderInformation(TenderInformation $tenderInformation) : void
    {
        $this->tenderInformations[] = $tenderInformation;
    }

    public function clearTenderInformations() : void
    {
        $this->tenderInformations = [];
    }

    public function getTenderInformations() : array
    {
        return $this->tenderInformations;
    }
    
    public function setTransactionInfo(TransactionInfo $transactionInfo) : void
    {
        $this->transactionInfo = $transactionInfo;
    }

    public function getTransactionInfo() : TransactionInfo
    {
        return $this->transactionInfo;
    }

    public function setLoanResultInfo(LoanResultInfo $loanResultInfo) : void
    {
        $this->loanResultInfo = $loanResultInfo;
    }

    public function getLoanResultInfo() : LoanResultInfo
    {
        return $this->loanResultInfo;
    }

    public function setWinBiddingEnterprise(Enterprise $winBiddingEnterprise) : void
    {
        $this->winBiddingEnterprise = $winBiddingEnterprise;
    }

    public function getWinBiddingEnterprise() : Enterprise
    {
        return $this->winBiddingEnterprise;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::LOAN_REQUIREMENT_STATUS) ?
                        $status :
                        self::LOAN_REQUIREMENT_STATUS['NORMAL'];
    }

    protected function getRepository() : LoanRequirementRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }

    public function revoke() : bool
    {
        return $this->getRepository()->revoke($this);
    }

    public function close() : bool
    {
        return $this->getRepository()->close($this);
    }

    public function bid() : bool
    {
        return $this->getRepository()->bid($this);
    }

    public function matching($tenderInformation) : bool
    {
        return $this->getRepository()->matching($this, $tenderInformation);
    }

    public function transaction($tenderInformation) : bool
    {
        return $this->getRepository()->transaction($this, $tenderInformation);
    }

    public function completed($tenderInformation) : bool
    {
        return $this->getRepository()->completed($this, $tenderInformation);
    }
}
