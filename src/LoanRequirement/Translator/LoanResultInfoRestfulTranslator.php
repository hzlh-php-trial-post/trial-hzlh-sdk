<?php
namespace Trial\LoanRequirement\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\LoanRequirement\Model\LoanResultInfo;
use Trial\LoanRequirement\Model\NullLoanResultInfo;
use Trial\Common\Translator\RestfulTranslatorTrait;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class LoanResultInfoRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function arrayToObject(array $expression, $loanResultInfo = null)
    {
        return $this->translateToObject($expression, $loanResultInfo);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $loanResultInfo = null)
    {
        if (empty($expression)) {
            return NullLoanResultInfo::getInstance();
        }

        if ($loanResultInfo == null) {
            $loanResultInfo = new LoanResultInfo();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $loanResultInfo->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['status'])) {
            $loanResultInfo->setStatus($attributes['status']);
        }
        if (isset($attributes['repaymentMethod'])) {
            $loanResultInfo->setRepaymentMethod($attributes['repaymentMethod']);
        }
        if (isset($attributes['loanAmount'])) {
            $loanResultInfo->setLoanAmount($attributes['loanAmount']);
        }
        if (isset($attributes['loanTerm'])) {
            $loanResultInfo->setLoanTerm($attributes['loanTerm']);
        }
        if (isset($attributes['loanFailReason'])) {
            $loanResultInfo->setLoanFailReason($attributes['loanFailReason']);
        }

        return $loanResultInfo;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($loanResultInfo, array $keys = array())
    {
        if (!$loanResultInfo instanceof LoanResultInfo) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'loanResultInfoStatus',
                'repaymentMethod',
                'loanAmount',
                'loanTerm',
                'loanFailReason'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'loanResultInfos'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $loanResultInfo->getId();
        }

        $attributes = array();

        if (in_array('loanResultInfoStatus', $keys)) {
            $attributes['status'] = $loanResultInfo->getStatus();
        }
        if (in_array('repaymentMethod', $keys)) {
            $attributes['repaymentMethod'] = $loanResultInfo->getRepaymentMethod();
        }
        if (in_array('loanAmount', $keys)) {
            $attributes['loanAmount'] = $loanResultInfo->getLoanAmount();
        }
        if (in_array('loanTerm', $keys)) {
            $attributes['loanTerm'] = $loanResultInfo->getLoanTerm();
        }
        if (in_array('loanFailReason', $keys)) {
            $attributes['loanFailReason'] = $loanResultInfo->getLoanFailReason();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
