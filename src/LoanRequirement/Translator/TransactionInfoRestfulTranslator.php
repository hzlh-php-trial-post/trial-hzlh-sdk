<?php
namespace Trial\LoanRequirement\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\LoanRequirement\Model\TransactionInfo;
use Trial\LoanRequirement\Model\NullTransactionInfo;
use Trial\Common\Translator\RestfulTranslatorTrait;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class TransactionInfoRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function arrayToObject(array $expression, $transactionInfo = null)
    {
        return $this->translateToObject($expression, $transactionInfo);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $transactionInfo = null)
    {
        if (empty($expression)) {
            return NullTransactionInfo::getInstance();
        }

        if ($transactionInfo == null) {
            $transactionInfo = new TransactionInfo();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $transactionInfo->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['transactionTime'])) {
            $transactionInfo->setTransactionTime($attributes['transactionTime']);
        }
        if (isset($attributes['transactionArea'])) {
            $transactionInfo->setTransactionArea($attributes['transactionArea']);
        }
        if (isset($attributes['transactionAddress'])) {
            $transactionInfo->setTransactionAddress($attributes['transactionAddress']);
        }
        if (isset($attributes['contactName'])) {
            $transactionInfo->setContactName($attributes['contactName']);
        }
        if (isset($attributes['contactPhone'])) {
            $transactionInfo->setContactPhone($attributes['contactPhone']);
        }
        if (isset($attributes['remark'])) {
            $transactionInfo->setRemark($attributes['remark']);
        }
        if (isset($attributes['carryData'])) {
            $transactionInfo->setCarryData($attributes['carryData']);
        }

        return $transactionInfo;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($transactionInfo, array $keys = array())
    {
        if (!$transactionInfo instanceof TransactionInfo) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'transactionTime',
                'transactionArea',
                'transactionAddress',
                'contactName',
                'contactPhone',
                'remark',
                'carryData'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'transactionInfos'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $transactionInfo->getId();
        }

        $attributes = array();

        if (in_array('transactionTime', $keys)) {
            $attributes['transactionTime'] = $transactionInfo->getTransactionTime();
        }
        if (in_array('transactionArea', $keys)) {
            $attributes['transactionArea'] = $transactionInfo->getTransactionArea();
        }
        if (in_array('transactionAddress', $keys)) {
            $attributes['transactionAddress'] = $transactionInfo->getTransactionAddress();
        }
        if (in_array('contactName', $keys)) {
            $attributes['contactName'] = $transactionInfo->getContactName();
        }
        if (in_array('contactPhone', $keys)) {
            $attributes['contactPhone'] = $transactionInfo->getContactPhone();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $transactionInfo->getRemark();
        }
        if (in_array('carryData', $keys)) {
            $attributes['carryData'] = $transactionInfo->getCarryData();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
