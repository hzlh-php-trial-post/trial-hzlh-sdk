<?php
namespace Trial\LoanRequirement\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\LoanRequirement\Model\LoanRequirement;
use Trial\LoanRequirement\Model\NullLoanRequirement;

use Trial\Member\Translator\MemberRestfulTranslator;

use Trial\Dictionary\Translator\DictionaryRestfulTranslator;

use Trial\Label\Translator\LabelRestfulTranslator;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class LoanRequirementRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getDictionaryRestfulTranslator()
    {
        return new DictionaryRestfulTranslator();
    }

    public function getLabelRestfulTranslator()
    {
        return new LabelRestfulTranslator();
    }

    public function getLoanResultInfoRestfulTranslator()
    {
        return new LoanResultInfoRestfulTranslator();
    }

    public function getTransactionInfoRestfulTranslator()
    {
        return new TransactionInfoRestfulTranslator();
    }

    public function getTenderInformationRestfulTranslator()
    {
        return new TenderInformationRestfulTranslator();
    }

    public function arrayToObject(array $expression, $loanRequirement = null)
    {
        return $this->translateToObject($expression, $loanRequirement);
    }

    protected function translateToObject(array $expression, $loanRequirement = null)
    {
        if (empty($expression)) {
            return NullLoanRequirement::getInstance();
        }
        if ($loanRequirement === null) {
            $loanRequirement = new LoanRequirement();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $loanRequirement->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $loanRequirement->setTitle($attributes['title']);
        }
        if (isset($attributes['number'])) {
            $loanRequirement->setNumber($attributes['number']);
        }
        if (isset($attributes['description'])) {
            $loanRequirement->setDescription($attributes['description']);
        }
        if (isset($attributes['borrowerName'])) {
            $loanRequirement->setBorrowerName($attributes['borrowerName']);
        }
        if (isset($attributes['loanObject'])) {
            $loanRequirement->setLoanObject($attributes['loanObject']);
        }
        if (isset($attributes['contactName'])) {
            $loanRequirement->setContactName($attributes['contactName']);
        }
        if (isset($attributes['contactPhone'])) {
            $loanRequirement->setContactPhone($attributes['contactPhone']);
        }
        if (isset($attributes['area'])) {
            $loanRequirement->setArea($attributes['area']);
        }
        if (isset($attributes['validityTime'])) {
            $loanRequirement->setValidityTime($attributes['validityTime']);
        }
        if (isset($attributes['loanPurposeDescribe'])) {
            $loanRequirement->setLoanPurposeDescribe($attributes['loanPurposeDescribe']);
        }
        if (isset($attributes['loanAmount'])) {
            $loanRequirement->setLoanAmount($attributes['loanAmount']);
        }
        if (isset($attributes['loanTerm'])) {
            $loanRequirement->setLoanTerm($attributes['loanTerm']);
        }
        if (isset($attributes['isHaveCollateral'])) {
            $loanRequirement->setIsHaveCollateral($attributes['isHaveCollateral']);
        }
        if (isset($attributes['collateralDescribe'])) {
            $loanRequirement->setCollateralDescribe($attributes['collateralDescribe']);
        }
        if (isset($attributes['isAllowViewCreditInformation'])) {
            $loanRequirement->setIsAllowViewCreditInformation($attributes['isAllowViewCreditInformation']);
        }
        if (isset($attributes['attachments'])) {
            $loanRequirement->setAttachments($attributes['attachments']);
        }
        if (isset($attributes['labels'])) {
            $loanRequirement->setLabels($attributes['labels']);
        }
        if (isset($attributes['closeReason'])) {
            $loanRequirement->setCloseReason($attributes['closeReason']);
        }
        if (isset($attributes['applyStatus'])) {
            $loanRequirement->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['rejectReason'])) {
            $loanRequirement->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['status'])) {
            $loanRequirement->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $loanRequirement->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $loanRequirement->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $loanRequirement->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['winBiddingEnterprise']['data'])) {
            $winBiddingEnterprise = $this->changeArrayFormat($relationships['winBiddingEnterprise']['data']);
            $loanRequirement->setWinBiddingEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($winBiddingEnterprise)
            );
        }
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $loanRequirement->setMember(
                $this->getMemberRestfulTranslator()->arrayToObject($member)
            );
        }
        if (isset($relationships['loanPurpose']['data'])) {
            $loanPurpose = $this->changeArrayFormat($relationships['loanPurpose']['data']);
            $loanRequirement->setLoanPurpose(
                $this->getDictionaryRestfulTranslator()->arrayToObject($loanPurpose)
            );
        }
        if (isset($relationships['collateral']['data'])) {
            $collateral = $this->changeArrayFormat($relationships['collateral']['data']);
            $loanRequirement->setCollateral(
                $this->getDictionaryRestfulTranslator()->arrayToObject($collateral)
            );
        }

        if (isset($relationships['applicant']['data'])) {
            if (isset($expression['included'])) {
                $applicant = $this->changeArrayFormat($relationships['applicant']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $applicant = $this->changeArrayFormat($relationships['applicant']['data']);
            }

            $applicantRestfulTranslator = $this->getTranslatorFactory()->getTranslator($attributes['loanObject']);
            $loanRequirement->setApplicant($applicantRestfulTranslator->arrayToObject($applicant));
        }

        if (isset($relationships['labels']['data'])) {
            $this->setUpLabels($relationships['labels']['data'], $loanRequirement);
        }
        if (isset($relationships['tenderInformations']['data'])) {
            $tenderInformations = $relationships['tenderInformations']['data'];

            foreach ($tenderInformations as $tenderInformationsArray) {
                if (isset($expression['included'])) {
                    $tenderInformations = $this->changeArrayFormat($tenderInformationsArray, $expression['included']);
                }
                if (!isset($expression['included'])) {
                    $tenderInformations = $this->changeArrayFormat($tenderInformationsArray);
                }
    
                $tenderInformationsObject = $this->getTenderInformationRestfulTranslator()->arrayToObject($tenderInformations);//phpcs:ignore
                $loanRequirement->addTenderInformation($tenderInformationsObject);
            }
        }

        return $loanRequirement;
    }

    protected function setUpLabels(array $labels, LoanRequirement $loanRequirement)
    {
        foreach ($labels as $labelsArray) {
            $labels = $this->changeArrayFormat($labelsArray);
            $labelsObject = $this->getLabelRestfulTranslator()->arrayToObject($labels);
            $loanRequirement->addLabel($labelsObject);
        }
    }

    public function objectToArray($loanRequirement, array $keys = array())
    {
        $expression = array();

        if (!$loanRequirement instanceof LoanRequirement) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'title',
                'description',
                'loanObject',
                'contactName',
                'contactPhone',
                'area',
                'validityTime',
                'loanPurpose',
                'loanPurposeDescribe',
                'loanAmount',
                'loanTerm',
                'isHaveCollateral',
                'collateral',
                'collateralDescribe',
                'isAllowViewCreditInformation',
                'attachments',
                'labels',
                'closeReason',
                'rejectReason',
                'member',
                'tenderInformations',
                'transactionInfo',
                'loanResultInfo'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'loanRequirements'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $loanRequirement->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $loanRequirement->getTitle();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $loanRequirement->getDescription();
        }
        if (in_array('loanObject', $keys)) {
            $attributes['loanObject'] = $loanRequirement->getLoanObject();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachments'] = $loanRequirement->getAttachments();
        }
        if (in_array('contactName', $keys)) {
            $attributes['contactName'] = $loanRequirement->getContactName();
        }
        if (in_array('contactPhone', $keys)) {
            $attributes['contactPhone'] = $loanRequirement->getContactPhone();
        }
        if (in_array('validityTime', $keys)) {
            $attributes['validityTime'] = $loanRequirement->getValidityTime();
        }
        if (in_array('area', $keys)) {
            $attributes['area'] = $loanRequirement->getArea();
        }
        if (in_array('loanPurposeDescribe', $keys)) {
            $attributes['loanPurposeDescribe'] = $loanRequirement->getLoanPurposeDescribe();
        }
        if (in_array('loanAmount', $keys)) {
            $attributes['loanAmount'] = $loanRequirement->getLoanAmount();
        }
        if (in_array('loanTerm', $keys)) {
            $attributes['loanTerm'] = $loanRequirement->getLoanTerm();
        }
        if (in_array('isHaveCollateral', $keys)) {
            $attributes['isHaveCollateral'] = $loanRequirement->getIsHaveCollateral();
        }
        if (in_array('collateralDescribe', $keys)) {
            $attributes['collateralDescribe'] = $loanRequirement->getCollateralDescribe();
        }
        if (in_array('isAllowViewCreditInformation', $keys)) {
            $attributes['isAllowViewCreditInformation'] = $loanRequirement->getIsAllowViewCreditInformation();
        }
        if (in_array('closeReason', $keys)) {
            $attributes['closeReason'] = $loanRequirement->getCloseReason();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $loanRequirement->getRejectReason();
        }
        
        $expression['data']['attributes'] = $attributes;

        if (in_array('labels', $keys)) {
            $labelsArray = $this->setUpLabelsArray($loanRequirement);
            $expression['data']['relationships']['labels']['data'] = $labelsArray;
        }

        if (in_array('loanPurpose', $keys)) {
            $expression['data']['relationships']['loanPurpose']['data'] = array(
                array(
                    'type' => 'dictionaries',
                    'id' => $loanRequirement->getLoanPurpose()->getId()
                )
            );
        }
        if (in_array('collateral', $keys)) {
            $expression['data']['relationships']['collateral']['data'] = array(
                array(
                    'type' => 'dictionaries',
                    'id' => $loanRequirement->getCollateral()->getId()
                )
            );
        }
        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $loanRequirement->getMember()->getId()
                )
            );
        }

        if (in_array('loanResultInfo', $keys)) {
            $expression['data']['relationships']['loanResultInfo'] =
                $this->getLoanResultInfoRestfulTranslator()->objectToArray($loanRequirement->getLoanResultInfo());
        }

        if (in_array('transactionInfo', $keys)) {
            $expression['data']['relationships']['transactionInfo'] =
                $this->getTransactionInfoRestfulTranslator()->objectToArray($loanRequirement->getTransactionInfo());
        }

        if (in_array('tenderInformations', $keys)) {
            $tenderInformationsArray = $this->setUpTenderInformationsArray($loanRequirement);
            $expression['data']['relationships']['tenderInformation']= $tenderInformationsArray;
        }

        return $expression;
    }

    protected function setUpLabelsArray(LoanRequirement $loanRequirement)
    {
        $labelsArray = [];

        $labels = $loanRequirement->getLabels();
        foreach ($labels as $labelsKey) {
            $labelsArray[] = array(
                    'type' => 'labels',
                    'id' => $labelsKey->getId()
                );
        }

        return $labelsArray;
    }
    
    protected function setUpTenderInformationsArray(LoanRequirement $loanRequirement)
    {
        $tenderInformationsArray = [];

        $tenderInformations = $loanRequirement->getTenderInformations();
        foreach ($tenderInformations as $tenderInformationsKey) {
            $tenderInformationsArray = $this->getTenderInformationRestfulTranslator()->objectToArray($tenderInformationsKey);//phpcs:ignore
        }

        return $tenderInformationsArray;
    }
}
