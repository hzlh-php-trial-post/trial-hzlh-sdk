<?php
namespace Trial\LoanRequirement\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\LoanRequirement\Model\LoanRequirement;
use Trial\TradeRecord\Translator\NullRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        LoanRequirement::LOAN_OBJECT['ENTERPRISE'] =>
        'Trial\Enterprise\Translator\EnterpriseRestfulTranslator',
        LoanRequirement::LOAN_OBJECT['NATURAL_PERSON'] =>
        'Trial\NaturalPerson\Translator\NaturalPersonRestfulTranslator'
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
