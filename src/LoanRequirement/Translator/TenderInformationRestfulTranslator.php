<?php
namespace Trial\LoanRequirement\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\LoanRequirement\Model\TenderInformation;
use Trial\LoanRequirement\Model\NullTenderInformation;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class TenderInformationRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getTransactionInfoRestfulTranslator()
    {
        return new TransactionInfoRestfulTranslator();
    }

    public function getLoanResultInfoRestfulTranslator()
    {
        return new LoanResultInfoRestfulTranslator();
    }

    public function arrayToObject(array $expression, $tenderInformation = null)
    {
        return $this->translateToObject($expression, $tenderInformation);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $tenderInformation = null)
    {
        if (empty($expression)) {
            return NullTenderInformation::getInstance();
        }

        if ($tenderInformation == null) {
            $tenderInformation = new TenderInformation();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $tenderInformation->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['creditMinLine'])) {
            $tenderInformation->setCreditMinLine($attributes['creditMinLine']);
        }
        if (isset($attributes['creditMaxLine'])) {
            $tenderInformation->setCreditMaxLine($attributes['creditMaxLine']);
        }
        if (isset($attributes['creditMinLoanTerm'])) {
            $tenderInformation->setCreditMinLoanTerm($attributes['creditMinLoanTerm']);
        }
        if (isset($attributes['creditMaxLoanTerm'])) {
            $tenderInformation->setCreditMaxLoanTerm($attributes['creditMaxLoanTerm']);
        }
        if (isset($attributes['creditValidityTime'])) {
            $tenderInformation->setCreditValidityTime($attributes['creditValidityTime']);
        }
        if (isset($attributes['status'])) {
            $tenderInformation->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $tenderInformation->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }
        if (isset($relationships['transactionInfo']['data'])) {
            $transactionInfo = $this->changeArrayFormat($relationships['transactionInfo']['data']);
            $tenderInformation->setTransactionInfo(
                $this->getTransactionInfoRestfulTranslator()->arrayToObject($transactionInfo)
            );
        }
        if (isset($relationships['loanResultInfo']['data'])) {
            $loanResultInfo = $this->changeArrayFormat($relationships['loanResultInfo']['data']);
            $tenderInformation->setLoanResultInfo(
                $this->getLoanResultInfoRestfulTranslator()->arrayToObject($loanResultInfo)
            );
        }

        return $tenderInformation;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($tenderInformation, array $keys = array())
    {
        if (!$tenderInformation instanceof TenderInformation) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'creditMinLine',
                'creditMaxLine',
                'creditMinLoanTerm',
                'creditMaxLoanTerm',
                'creditValidityTime',
                'enterprise'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'tenderInformations'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $tenderInformation->getId();
        }

        $attributes = array();

        if (in_array('creditMinLine', $keys)) {
            $attributes['creditMinLine'] = $tenderInformation->getCreditMinLine();
        }
        if (in_array('creditMaxLine', $keys)) {
            $attributes['creditMaxLine'] = $tenderInformation->getCreditMaxLine();
        }
        if (in_array('creditMinLoanTerm', $keys)) {
            $attributes['creditMinLoanTerm'] = $tenderInformation->getCreditMinLoanTerm();
        }
        if (in_array('creditMaxLoanTerm', $keys)) {
            $attributes['creditMaxLoanTerm'] = $tenderInformation->getCreditMaxLoanTerm();
        }
        if (in_array('creditValidityTime', $keys)) {
            $attributes['creditValidityTime'] = $tenderInformation->getCreditValidityTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprises',
                    'id' => $tenderInformation->getEnterprise()->getId()
                )
            );
        }

        return $expression;
    }
}
