<?php
namespace Trial\LoanRequirement\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\LoanRequirement\Adapter\TenderInformation\ITenderInformationAdapter;
use Trial\LoanRequirement\Adapter\TenderInformation\TenderInformationMockAdapter;
use Trial\LoanRequirement\Adapter\TenderInformation\TenderInformationRestfulAdapter;

use Trial\LoanRequirement\Model\TenderInformation;

class TenderInformationRepository extends Repository implements ITenderInformationAdapter
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const PORTAL_LIST_MODEL_UN = 'PORTAL_TENDER_INFORMATION_LIST';
    const OA_LIST_MODEL_UN = 'OA_TENDER_INFORMATION_LIST';
    const FETCH_ONE_MODEL_UN = 'TENDER_INFORMATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new TenderInformationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ITenderInformationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ITenderInformationAdapter
    {
        return new TenderInformationMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
