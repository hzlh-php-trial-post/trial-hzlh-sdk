<?php
namespace Trial\LoanRequirement\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ApplyAbleRepositoryTrait;
use Trial\Common\Repository\ResubmitAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\LoanRequirement\Adapter\LoanRequirement\ILoanRequirementAdapter;
use Trial\LoanRequirement\Adapter\LoanRequirement\LoanRequirementMockAdapter;
use Trial\LoanRequirement\Adapter\LoanRequirement\LoanRequirementRestfulAdapter;

use Trial\LoanRequirement\Model\LoanRequirement;
use Trial\LoanRequirement\Model\TenderInformation;

class LoanRequirementRepository extends Repository implements ILoanRequirementAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ApplyAbleRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const PORTAL_LIST_MODEL_UN = 'PORTAL_LOAN_REQUIREMENT_LIST';
    const OA_LIST_MODEL_UN = 'OA_LOAN_REQUIREMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'LOAN_REQUIREMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new LoanRequirementRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ILoanRequirementAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ILoanRequirementAdapter
    {
        return new LoanRequirementMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(LoanRequirement $loanRequirement) : bool
    {
        return $this->getAdapter()->deletes($loanRequirement);
    }

    public function revoke(LoanRequirement $loanRequirement) : bool
    {
        return $this->getAdapter()->revoke($loanRequirement);
    }

    public function close(LoanRequirement $loanRequirement) : bool
    {
        return $this->getAdapter()->close($loanRequirement);
    }

    public function bid(LoanRequirement $loanRequirement) : bool
    {
        return $this->getAdapter()->bid($loanRequirement);
    }

    public function matching(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        return $this->getAdapter()->matching($loanRequirement, $tenderInformation);
    }

    public function transaction(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        return $this->getAdapter()->transaction($loanRequirement, $tenderInformation);
    }

    public function completed(LoanRequirement $loanRequirement, TenderInformation $tenderInformation) : bool
    {
        return $this->getAdapter()->completed($loanRequirement, $tenderInformation);
    }
}
