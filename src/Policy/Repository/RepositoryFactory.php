<?php
namespace Trial\Policy\Repository;

use Trial\PolicyProduct\Model\PolicyProduct;

use Common\Repository\NullRepository;

class RepositoryFactory
{
    const MAPS = array(
        PolicyProduct::CATEGORY['SERVICE']=>
        'Trial\Service\Repository\ServiceRepository',
        PolicyProduct::CATEGORY['FINANCE']=>
        'Trial\LoanProduct\Repository\LoanProductRepository',
    );

    public function getRepository(int $category)
    {
        $repository = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($repository) ? new $repository : NullRepository::getInstance();
    }
}
