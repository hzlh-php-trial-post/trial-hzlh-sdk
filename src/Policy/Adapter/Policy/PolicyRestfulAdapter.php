<?php
namespace Trial\Policy\Adapter\Policy;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Policy\Model\Policy;
use Trial\Policy\Model\NullPolicy;
use Trial\Policy\Translator\PolicyRestfulTranslator;
use Trial\Policy\Translator\PolicyProductRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class PolicyRestfulAdapter extends GuzzleAdapter implements IPolicyAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        OnShelfAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_POLICY_LIST'=>[
                'fields'=>[
                    'policies' =>
                        'title,dispatchDepartments,image,number,createTime,updateTime,status,level,specials,tag'
                ],
                'include'=> 'crew,dispatchDepartments,labels,policyProducts,policyProducts.enterprise,policyProducts.product,level,applicableObjects,applicableIndustries,classifies,specials' //phpcs:ignore
            ],
            'PORTAL_POLICY_LIST'=>[
                'fields'=>[],
                'include'=> 'crew,dispatchDepartments,labels,policyProducts,policyProducts.enterprise,policyProducts.product,level,applicableObjects,applicableIndustries,classifies,specials' //phpcs:ignore
            ],
            'POLICY_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew,dispatchDepartments,labels,policyProducts,policyProducts.enterprise,policyProducts.product,level,applicableObjects,applicableIndustries,classifies,specials' //phpcs:ignore
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PolicyRestfulTranslator();
        $this->resource = 'policies';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPolicy::getInstance());
    }

    protected function addAction(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array(
                'title',
                'applicableObjects',
                'dispatchDepartments',
                'applicableIndustries',
                'level',
                'classifies',
                'detail',
                'description',
                'image',
                'attachments',
                'labels',
                'processingFlow',
                'admissibleAddress',
                'crew',
                'tag'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }

    protected function editAction(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array(
                'title',
                'applicableObjects',
                'dispatchDepartments',
                'applicableIndustries',
                'level',
                'classifies',
                'detail',
                'description',
                'image',
                'attachments',
                'labels',
                'processingFlow',
                'admissibleAddress',
                'tag'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$policy->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }

    public function relationProduct(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array('policyProducts')
        );

        $this->patch(
            $this->getResource().'/'.$policy->getId().'/relationProduct',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }

    public function cancelRelationProduct(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array('policyProducts')
        );

        $this->patch(
            $this->getResource().'/'.$policy->getId().'/cancelRelationProduct',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }
 
    public function relationSpecial(Policy $policy) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policy,
            array('specials')
        );

        $this->patch(
            $this->getResource().'/'.$policy->getId().'/relationSpecial',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policy);
            return true;
        }

        return false;
    }
}
