<?php
namespace Trial\Policy\Translator;

use Trial\Common\Translator\CategoryTranslator;

use Trial\Policy\Model\NullPolicyCategory;

class PolicyCategoryTranslator extends CategoryTranslator
{
    public function arrayToObject(array $expression, $category = null)
    {
        unset($category);
        unset($expression);
        return new NullPolicyCategory(0, '');
    }
}
