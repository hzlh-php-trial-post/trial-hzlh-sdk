<?php
namespace Trial\Dictionary\Adapter\Dictionary;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

interface IDictionaryAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IEnableAbleAdapter
{
}
