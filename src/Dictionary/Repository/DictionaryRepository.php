<?php
namespace Trial\Dictionary\Repository;

use Trial\Dictionary\Adapter\Dictionary\DictionaryRestfulAdapter;
use Trial\Dictionary\Adapter\Dictionary\DictionaryMockAdapter;
use Trial\Dictionary\Adapter\Dictionary\IDictionaryAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

class DictionaryRepository extends Repository implements IDictionaryAdapter
{
    use OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ErrorRepositoryTrait;
    
    private $adapter;

    const LIST_MODEL_UN = 'DICTIONARY_LIST';
    const FETCH_ONE_MODEL_UN = 'DICTIONARY_FETCH_ONE';
    
    public function __construct()
    {
        $this->adapter = new DictionaryRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }
    
    public function getActualAdapter() : IDictionaryAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IDictionaryAdapter
    {
        return new DictionaryMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
