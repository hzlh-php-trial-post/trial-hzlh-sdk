<?php
namespace Trial\GovernmentOrgans\Adapter\GovernmentOrgans;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleMockAdapterTrait;

use Trial\GovernmentOrgans\Utils\MockFactory;

class GovernmentOrgansMockAdapter implements IGovernmentOrgansAdapter
{
    use OperatAbleMockAdapterTrait, OnShelfAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateGovernmentOrgansObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $governmentOrgansList = array();

        foreach ($ids as $id) {
            $governmentOrgansList[] = MockFactory::generateGovernmentOrgansObject($id);
        }

        return $governmentOrgansList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateGovernmentOrgansObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateGovernmentOrgansObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
