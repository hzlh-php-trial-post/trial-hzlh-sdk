<?php
namespace Trial\GovernmentOrgans\Adapter\GovernmentOrgans;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IGovernmentOrgansAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IOnShelfAbleAdapter
{
}
