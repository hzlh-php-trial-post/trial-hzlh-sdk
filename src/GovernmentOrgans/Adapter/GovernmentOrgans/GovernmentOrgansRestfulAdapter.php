<?php
namespace Trial\GovernmentOrgans\Adapter\GovernmentOrgans;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\GovernmentOrgans\Model\GovernmentOrgans;
use Trial\GovernmentOrgans\Model\NullGovernmentOrgans;
use Trial\GovernmentOrgans\Translator\GovernmentOrgansRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class GovernmentOrgansRestfulAdapter extends GuzzleAdapter implements IGovernmentOrgansAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        OnShelfAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_GOVERNMENT_ORGANS_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'PORTAL_GOVERNMENT_ORGANS_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'GOVERNMENT_ORGANS_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new GovernmentOrgansRestfulTranslator();
        $this->resource = 'governmentOrgans';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullGovernmentOrgans::getInstance());
    }

    protected function addAction(GovernmentOrgans $governmentOrgans) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $governmentOrgans,
            array(
                'name',
                'area',
                'address',
                'serviceTime',
                'hotline',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($governmentOrgans);
            return true;
        }

        return false;
    }

    protected function editAction(GovernmentOrgans $governmentOrgans) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $governmentOrgans,
            array(
                'name',
                'area',
                'address',
                'serviceTime',
                'hotline'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$governmentOrgans->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($governmentOrgans);
            return true;
        }

        return false;
    }
}
