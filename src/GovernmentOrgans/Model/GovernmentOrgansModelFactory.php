<?php
namespace Trial\GovernmentOrgans\Model;

use Trial\Common\Model\IRecommendAble;
use Trial\Common\Model\IOnShelfAble;

class GovernmentOrgansModelFactory
{
    const STATUS_CN = array(
        IOnShelfAble::STATUS['ONSHELF'] => '已上架',
        IOnShelfAble::STATUS['OFFSTOCK'] => '已下架'
    );
}
