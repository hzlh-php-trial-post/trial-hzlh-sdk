<?php
namespace Trial\GovernmentOrgans\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\IOnShelfAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Model\OnShelfAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;

use Trial\Crew\Model\Crew;
use Trial\GovernmentOrgans\Repository\GovernmentOrgansRepository;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class GovernmentOrgans implements IObject, IOperatAble, IOnShelfAble
{
    use Object, OperatAbleTrait, OnShelfAbleTrait;

    /**
     * [$id id]
     * @var [int]
     */
    private $id;
    /**
     * [$name 名称]
     * @var [string]
     */
    private $name;
    /**
     * [$area 地址]
     * @var [string]
     */
    private $area;
    /**
     * [$address 详细地址]
     * @var [string]
     */
    private $address;
    /**
     * [$serviceTime 服务时间]
     * @var [string]
     */
    private $serviceTime;
    /**
     * [$hotline 热线电话]
     * @var [string]
     */
    private $hotline;
    /**
     * [$crew 操作人员]
     * @var [Crew]
     */
    private $crew;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->area = '';
        $this->address = '';
        $this->hotline = '';
        $this->serviceTime = '';
        $this->status = IOnShelfAble::STATUS['ONSHELF'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->repository = new GovernmentOrgansRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->area);
        unset($this->address);
        unset($this->hotline);
        unset($this->serviceTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->crew);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setArea(string $area) : void
    {
        $this->area = $area;
    }

    public function getArea() : string
    {
        return $this->area;
    }

    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function setHotline(string $hotline) : void
    {
        $this->hotline = $hotline;
    }

    public function getHotline() : string
    {
        return $this->hotline;
    }

    public function setServiceTime(string $serviceTime) : void
    {
        $this->serviceTime = $serviceTime;
    }

    public function getServiceTime() : string
    {
        return $this->serviceTime;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : GovernmentOrgansRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOnShelfAbleAdapter() : IOnShelfAbleAdapter
    {
        return $this->getRepository();
    }
}
