<?php
namespace Trial\GovernmentOrgans\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\OnShelfAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\GovernmentOrgans\Model\GovernmentOrgans;
use Trial\GovernmentOrgans\Adapter\GovernmentOrgans\IGovernmentOrgansAdapter;
use Trial\GovernmentOrgans\Adapter\GovernmentOrgans\GovernmentOrgansMockAdapter;
use Trial\GovernmentOrgans\Adapter\GovernmentOrgans\GovernmentOrgansRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class GovernmentOrgansRepository extends Repository implements IGovernmentOrgansAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_GOVERNMENT_ORGANS_LIST'; //OA列表场景
    const PORTAL_LIST_MODEL_UN = 'PORTAL_GOVERNMENT_ORGANS_LIST'; //门户列表场景
    const FETCH_ONE_MODEL_UN = 'GOVERNMENT_ORGANS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new GovernmentOrgansRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IGovernmentOrgansAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IGovernmentOrgansAdapter
    {
        return new GovernmentOrgansMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
