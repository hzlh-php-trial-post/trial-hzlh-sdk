<?php
namespace Trial\GovernmentOrgans\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\GovernmentOrgans\Model\GovernmentOrgans;
use Trial\GovernmentOrgans\Model\NullGovernmentOrgans;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Crew\Translator\CrewRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class GovernmentOrgansRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $governmentOrgans = null)
    {
        return $this->translateToObject($expression, $governmentOrgans);
    }

    protected function translateToObject(array $expression, $governmentOrgans = null)
    {
        if (empty($expression)) {
            return NullGovernmentOrgans::getInstance();
        }
        if ($governmentOrgans === null) {
            $governmentOrgans = new GovernmentOrgans();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $governmentOrgans->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $governmentOrgans->setName($attributes['name']);
        }
        if (isset($attributes['area'])) {
            $governmentOrgans->setArea($attributes['area']);
        }
        if (isset($attributes['address'])) {
            $governmentOrgans->setAddress($attributes['address']);
        }
        if (isset($attributes['serviceTime'])) {
            $governmentOrgans->setServiceTime($attributes['serviceTime']);
        }
        if (isset($attributes['hotline'])) {
            $governmentOrgans->setHotline($attributes['hotline']);
        }
        if (isset($attributes['status'])) {
            $governmentOrgans->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $governmentOrgans->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $governmentOrgans->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $governmentOrgans->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $governmentOrgans->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $governmentOrgans;
    }

    public function objectToArray($governmentOrgans, array $keys = array())
    {
        $expression = array();

        if (!$governmentOrgans instanceof GovernmentOrgans) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'name',
                'area',
                'address',
                'serviceTime',
                'hotline',
                'crew',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'governmentOrgans'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $governmentOrgans->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $governmentOrgans->getName();
        }
        if (in_array('area', $keys)) {
            $attributes['area'] = $governmentOrgans->getArea();
        }
        if (in_array('address', $keys)) {
            $attributes['address'] = $governmentOrgans->getAddress();
        }
        if (in_array('serviceTime', $keys)) {
            $attributes['serviceTime'] = $governmentOrgans->getServiceTime();
        }
        if (in_array('hotline', $keys)) {
            $attributes['hotline'] = $governmentOrgans->getHotline();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $governmentOrgans->getCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
