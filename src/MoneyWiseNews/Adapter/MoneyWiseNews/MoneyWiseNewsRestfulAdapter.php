<?php
namespace Trial\MoneyWiseNews\Adapter\MoneyWiseNews;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\MoneyWiseNews\Model\MoneyWiseNews;
use Trial\MoneyWiseNews\Model\NullMoneyWiseNews;
use Trial\MoneyWiseNews\Translator\MoneyWiseNewsRestfulTranslator;
use Trial\MoneyWiseNews\Translator\MoneyWiseNewsProductRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\TopAbleRestfulAdapterTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class MoneyWiseNewsRestfulAdapter extends GuzzleAdapter implements IMoneyWiseNewsAdapter
{
    use CommonMapErrorsTrait,
        TopAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        OnShelfAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_MONEY_WISE_NEWS_LIST'=>[
                'fields'=>[
                    'news' => 'number,title,category,stick,createTime,updateTime,status'
                ],
                'include'=> 'crew,category' //phpcs:ignore
            ],
            'PORTAL_MONEY_WISE_NEWS_LIST'=>[
                'fields'=>[
                    'news' => 'number,title,description,cover,category,stick,createTime,updateTime,collection,pageViews' //phpcs:ignore
                ],
                'include'=> 'crew,category' //phpcs:ignore
            ],
            'MONEY_WISE_NEWS_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew,category' //phpcs:ignore
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new MoneyWiseNewsRestfulTranslator();
        $this->resource = 'news';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            114 => RESOURCE_STICK_ENABLED,
            115 => RESOURCE_STICK_DISABLED,
            116 => MONEY_WISE_NEWS_STICK_IS_FULL,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullMoneyWiseNews::getInstance());
    }

    protected function addAction(MoneyWiseNews $moneyWiseNews) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $moneyWiseNews,
            array(
                'title',
                'source',
                'detail',
                'cover',
                'description',
                'category',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($moneyWiseNews);
            return true;
        }

        return false;
    }

    protected function editAction(MoneyWiseNews $moneyWiseNews) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $moneyWiseNews,
            array(
                'title',
                'source',
                'detail',
                'cover',
                'description',
                'category'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$moneyWiseNews->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($moneyWiseNews);
            return true;
        }

        return false;
    }
}
