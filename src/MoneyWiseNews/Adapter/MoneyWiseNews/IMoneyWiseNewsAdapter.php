<?php
namespace Trial\MoneyWiseNews\Adapter\MoneyWiseNews;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;
use Trial\Common\Adapter\ITopAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IMoneyWiseNewsAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IOnShelfAbleAdapter, ITopAbleAdapter //phpcs:ignore
{
}
