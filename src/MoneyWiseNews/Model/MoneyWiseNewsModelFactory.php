<?php
namespace Trial\MoneyWiseNews\Model;

use Trial\Common\Model\ITopAble;
use Trial\Common\Model\IOnShelfAble;

class MoneyWiseNewsModelFactory
{
    const STICK_CN = array(
        ITopAble::STICK['ENABLED'] => '置顶',
        ITopAble::STICK['DISABLED'] => '取消置顶'
    );

    const STATUS_CN = array(
        IOnShelfAble::STATUS['ONSHELF'] => '上架',
        IOnShelfAble::STATUS['OFFSTOCK'] => '下架'
    );
}
