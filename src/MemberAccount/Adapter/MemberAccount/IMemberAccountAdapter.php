<?php
namespace Trial\MemberAccount\Adapter\MemberAccount;

interface IMemberAccountAdapter
{
    public function fetchOne($id);
}
