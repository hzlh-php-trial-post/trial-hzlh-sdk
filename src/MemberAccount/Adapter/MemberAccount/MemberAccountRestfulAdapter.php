<?php
namespace Trial\MemberAccount\Adapter\MemberAccount;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;

use Trial\MemberAccount\Model\NullMemberAccount;
use Trial\MemberAccount\Model\MemberAccount;

use Trial\MemberAccount\Translator\MemberAccountRestfulTranslator;

class MemberAccountRestfulAdapter extends GuzzleAdapter implements IMemberAccountAdapter
{
    use CommonMapErrorsTrait, FetchAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'MEMBER_ACCOUNT_FETCH_ONE' => [
            'fields' => [],
            'include' => 'member',
        ],
    ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct($uri, $authKey);
        $this->translator = new MemberAccountRestfulTranslator();
        $this->resource = 'memberAccounts';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullMemberAccount::getInstance());
    }
}
