<?php
namespace Trial\MemberAccount\Adapter\MemberAccount;

use Trial\MemberAccount\Model\MemberAccount;
use Trial\MemberAccount\Utils\MockFactory;

class MemberAccountMockAdapter implements IMemberAccountAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateMemberAccountObject($id);
    }
}
