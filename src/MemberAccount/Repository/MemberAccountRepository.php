<?php
namespace Trial\MemberAccount\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;

use Trial\MemberAccount\Adapter\MemberAccount\IMemberAccountAdapter;
use Trial\MemberAccount\Adapter\MemberAccount\MemberAccountMockAdapter;
use Trial\MemberAccount\Adapter\MemberAccount\MemberAccountRestfulAdapter;

use Trial\MemberAccount\Model\MemberAccount;

class MemberAccountRepository extends Repository implements IMemberAccountAdapter
{
    use FetchRepositoryTrait, ErrorRepositoryTrait;

    private $adapter;

    const FETCH_ONE_MODEL_UN = 'MEMBER_ACCOUNT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new MemberAccountRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IMemberAccountAdapter
    {
        return new MemberAccountMockAdapter();
    }

    public function getActualAdapter(): IMemberAccountAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
