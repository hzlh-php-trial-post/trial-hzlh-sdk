<?php
namespace Trial\FinancialQA\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\FinancialQA\Adapter\FinancialQuestion\IFinancialQuestionAdapter;
use Trial\FinancialQA\Adapter\FinancialQuestion\FinancialQuestionMockAdapter;
use Trial\FinancialQA\Adapter\FinancialQuestion\FinancialQuestionRestfulAdapter;

use Trial\FinancialQA\Model\FinancialQuestion;

class FinancialQuestionRepository extends Repository implements IFinancialQuestionAdapter
{
    use OperatAbleRepositoryTrait,
        FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'FINANCIAL_QUESTION_LIST';
    const FETCH_ONE_MODEL_UN = 'FINANCIAL_QUESTION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new FinancialQuestionRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IFinancialQuestionAdapter
    {

        return $this->adapter;
    }

    public function getMockAdapter() : IFinancialQuestionAdapter
    {

        return new FinancialQuestionMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    /**
     * [platformDelete 平台删除]
     * @param  FinancialQuestion $financialQuestion [object]
     * @return [type]                               [bool]
     */
    public function platformDelete(FinancialQuestion $financialQuestion) : bool
    {
        return $this->getAdapter()->platformDelete($financialQuestion);
    }
    /**
     * [deletes 本人删除]
     * @param  FinancialQuestion $financialQuestion [object]
     * @return [type]                               [bool]
     */
    public function deletes(FinancialQuestion $financialQuestion) : bool
    {
        return $this->getAdapter()->deletes($financialQuestion);
    }
}
