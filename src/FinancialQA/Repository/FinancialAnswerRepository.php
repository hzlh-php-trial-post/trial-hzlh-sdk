<?php
namespace Trial\FinancialQA\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\FinancialQA\Adapter\FinancialAnswer\IFinancialAnswerAdapter;
use Trial\FinancialQA\Adapter\FinancialAnswer\FinancialAnswerMockAdapter;
use Trial\FinancialQA\Adapter\FinancialAnswer\FinancialAnswerRestfulAdapter;

use Trial\FinancialQA\Model\FinancialAnswer;

class FinancialAnswerRepository extends Repository implements IFinancialAnswerAdapter
{
    use OperatAbleRepositoryTrait,
        FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'FINANCIAL_ANSWER_LIST';
    const FETCH_ONE_MODEL_UN = 'FINANCIAL_ANSWER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new FinancialAnswerRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IFinancialAnswerAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IFinancialAnswerAdapter
    {
        return new FinancialAnswerMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
    /**
     * [platformDelete 平台删除]
     * @param  FinancialAnswer $financialAnswer [object]
     * @return [type]                           [bool]
     */
    public function platformDelete(FinancialAnswer $financialAnswer) : bool
    {
        return $this->getAdapter()->platformDelete($financialAnswer);
    }
    /**
     * [questionerDelete 提问者删除]
     * @param  FinancialAnswer $financialAnswer [object]
     * @return [type]                           [bool]
     */
    public function questionerDelete(FinancialAnswer $financialAnswer) : bool
    {
        return $this->getAdapter()->questionerDelete($financialAnswer);
    }
    /**
     * [deletes 本人删除]
     * @param  FinancialAnswer $financialAnswer [object]
     * @return [type]                           [bool]
     */
    public function deletes(FinancialAnswer $financialAnswer) : bool
    {
        return $this->getAdapter()->deletes($financialAnswer);
    }
}
