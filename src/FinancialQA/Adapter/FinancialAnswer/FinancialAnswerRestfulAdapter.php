<?php
namespace Trial\FinancialQA\Adapter\FinancialAnswer;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\FinancialQA\Model\FinancialAnswer;
use Trial\FinancialQA\Model\NullFinancialAnswer;
use Trial\FinancialQA\Translator\FinancialAnswerRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class FinancialAnswerRestfulAdapter extends GuzzleAdapter implements IFinancialAnswerAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'FINANCIAL_ANSWER_LIST'=>[
                'fields'=>[],
                'include'=> 'member,financialQuestion'
            ],
            'FINANCIAL_ANSWER_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'member,financialQuestion'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new FinancialAnswerRestfulTranslator();
        $this->resource = 'financialAnswers';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullFinancialAnswer::getInstance());
    }

    protected function addAction(FinancialAnswer $financialAnswer) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financialAnswer,
            array(
                'question',
                'member',
                'content'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financialAnswer);
            return true;
        }

        return false;
    }

    protected function editAction(FinancialAnswer $financialAnswer) : bool
    {
        unset($financialAnswer);
        return false;
    }
    /**
     * [deletes 本人删除]
     * @param  FinancialAnswer $financialAnswer [object]
     * @return [type]                           [bool]
     */
    public function deletes(FinancialAnswer $financialAnswer) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            )
        );

        $this->delete(
            $this->getResource().'/'.$financialAnswer->getId().'/delete',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financialAnswer);
            return true;
        }

        return false;
    }
    /**
     * [platformDelete 平台删除]
     * @param  FinancialQuestion $financialQuestion [object]
     * @return [type]                               [bool]
     */
    public function platformDelete(FinancialAnswer $financialAnswer) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            )
        );

        $this->delete(
            $this->getResource().'/'.$financialAnswer->getId().'/delete',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financialAnswer);
            return true;
        }

        return false;
    }
    /**
     * [questionerDelete 提问者删除]
     * @param  FinancialQuestion $financialQuestion [object]
     * @return [type]                               [bool]
     */
    public function questionerDelete(FinancialAnswer $financialAnswer) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financialAnswer,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            )
        );

        $this->delete(
            $this->getResource().'/'.$financialAnswer->getId().'/delete',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financialAnswer);
            return true;
        }

        return false;
    }
}
