<?php
namespace Trial\FinancialQA\Adapter\FinancialAnswer;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IFinancialAnswerAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter
{
}
