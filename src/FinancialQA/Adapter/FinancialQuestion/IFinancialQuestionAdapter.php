<?php
namespace Trial\FinancialQA\Adapter\FinancialQuestion;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IFinancialQuestionAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter
{
}
