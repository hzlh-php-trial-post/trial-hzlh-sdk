<?php
namespace Trial\FinancialQA\Adapter\FinancialQuestion;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\FinancialQA\Model\FinancialQuestion;
use Trial\FinancialQA\Model\NullFinancialQuestion;
use Trial\FinancialQA\Translator\FinancialQuestionRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class FinancialQuestionRestfulAdapter extends GuzzleAdapter implements IFinancialQuestionAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'FINANCIAL_QUESTION_LIST'=>[
                'fields'=>[],
                'include'=> 'member,category,answer'
            ],
            'FINANCIAL_QUESTION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'member,category,answer'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new FinancialQuestionRestfulTranslator();
        $this->resource = 'financialQuestions';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullFinancialQuestion::getInstance());
    }

    protected function addAction(FinancialQuestion $financialQuestion) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financialQuestion,
            array(
                'category',
                'content',
                'member'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financialQuestion);
            return true;
        }

        return false;
    }

    protected function editAction(FinancialQuestion $financialQuestion) : bool
    {
        unset($financialQuestion);
        return false;
    }
    /**
     * [platformDelete 平台删除]
     * @param  FinancialQuestion $financialQuestion [object]
     * @return [type]                               [bool]
     */
    public function platformDelete(FinancialQuestion $financialQuestion) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financialQuestion,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            )
        );

        $this->delete(
            $this->getResource().'/'.$financialQuestion->getId().'/delete',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financialQuestion);
            return true;
        }

        return false;
    }
    /**
     * [deletes 本人删除]
     * @param  FinancialQuestion $financialQuestion [object]
     * @return [type]                               [bool]
     */
    public function deletes(FinancialQuestion $financialQuestion) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $financialQuestion,
            array(
                'deleteType',
                'deleteReason',
                'deleteBy'
            )
        );

        $this->delete(
            $this->getResource().'/'.$financialQuestion->getId().'/delete',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($financialQuestion);
            return true;
        }

        return false;
    }
}
