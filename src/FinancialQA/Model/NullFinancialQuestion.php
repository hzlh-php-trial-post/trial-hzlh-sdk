<?php
namespace Trial\FinancialQA\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullOperatAbleTrait;

class NullFinancialQuestion extends FinancialQuestion implements INull
{
    use NullOperatAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function platformDelete() : bool
    {
        return $this->resourceNotExist();
    }

    public function deletes() : bool
    {
        return $this->resourceNotExist();
    }
}
