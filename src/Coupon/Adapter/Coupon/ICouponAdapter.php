<?php
namespace Trial\Coupon\Adapter\Coupon;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Coupon\Model\Coupon;

interface ICouponAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function receive(Coupon $coupon) : bool;

    public function deletes(Coupon $coupon) : bool;
}
