<?php
namespace Trial\Coupon\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\Coupon\Model\Coupon;
use Trial\Coupon\Adapter\Coupon\ICouponAdapter;
use Trial\Coupon\Adapter\Coupon\CouponMockAdapter;
use Trial\Coupon\Adapter\Coupon\CouponRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class CouponRepository extends Repository implements ICouponAdapter
{
    use FetchRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const PORTAL_LIST_MODEL_UN = 'PORTAL_COUPON_LIST';
    const FETCH_ONE_MODEL_UN = 'COUPON_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CouponRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ICouponAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ICouponAdapter
    {
        return new CouponMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function receive(Coupon $coupon) : bool
    {
        return $this->getAdapter()->receive($coupon);
    }

    public function deletes(Coupon $coupon) : bool
    {
        return $this->getAdapter()->deletes($coupon);
    }
}
