<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IEnableAble;

interface IEnableAbleAdapter
{
    public function enable(IEnableAble $enableAbleObject) : bool;

    public function disable(IEnableAble $enableAbleObject) : bool;
}
