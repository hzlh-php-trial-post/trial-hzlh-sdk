<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\ITopAble;

trait TopAbleMockAdapterTrait
{
    public function top(ITopAble $topAbleObject) : bool
    {
        unset($topAbleObject);
        return true;
    }

    public function cancelTop(ITopAble $topAbleObject) : bool
    {
        unset($topAbleObject);
        return true;
    }
}
