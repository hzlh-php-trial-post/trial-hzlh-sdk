<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IOnShelfAble;

trait OnShelfAbleMockAdapterTrait
{
    public function onShelf(IOnShelfAble $onShelfAbleObject) : bool
    {
        unset($onShelfAbleObject);
        return true;
    }

    public function offStock(IOnShelfAble $onShelfAbleObject) : bool
    {
        unset($onShelfAbleObject);
        return true;
    }
}
