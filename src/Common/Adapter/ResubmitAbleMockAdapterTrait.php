<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IResubmitAble;

trait ResubmitAbleMockAdapterTrait
{
    public function resubmit(IResubmitAble $resubmitAbleObject) : bool
    {
        unset($resubmitAbleObject);
        return true;
    }
}
