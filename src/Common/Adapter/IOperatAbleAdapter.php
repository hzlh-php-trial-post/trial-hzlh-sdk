<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IOperatAble;

interface IOperatAbleAdapter
{
    public function add(IOperatAble $operatAbleObject) : bool;

    public function edit(IOperatAble $operatAbleObject) : bool;
}
