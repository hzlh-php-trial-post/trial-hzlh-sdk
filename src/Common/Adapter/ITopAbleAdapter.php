<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\ITopAble;

interface ITopAbleAdapter
{
    public function top(ITopAble $topAbleObject) : bool;

    public function cancelTop(ITopAble $topAbleObject) : bool;
}
