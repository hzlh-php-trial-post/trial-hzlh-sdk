<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IResubmitAble;

interface IResubmitAbleAdapter
{
    public function resubmit(IResubmitAble $resubmitAbleObject) : bool;
}
