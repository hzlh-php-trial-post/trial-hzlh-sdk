<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IOnShelfAble;

interface IOnShelfAbleAdapter
{
    public function onShelf(IOnShelfAble $onShelfAbleObject) : bool;

    public function offStock(IOnShelfAble $onShelfAbleObject) : bool;
}
