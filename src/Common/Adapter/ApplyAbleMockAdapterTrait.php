<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IApplyAble;

trait ApplyAbleMockAdapterTrait
{
    public function approve(IApplyAble $applyAbleObject) : bool
    {
        unset($applyAbleObject);
        return true;
    }

    public function reject(IApplyAble $applyAbleObject) : bool
    {
        unset($applyAbleObject);
        return true;
    }
}
