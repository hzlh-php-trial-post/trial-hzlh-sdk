<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IRecommendAble;

interface IRecommendAbleAdapter
{
    public function recommend(IRecommendAble $recommendAbleObject) : bool;

    public function cancelRecommend(IRecommendAble $recommendAbleObject) : bool;
}
