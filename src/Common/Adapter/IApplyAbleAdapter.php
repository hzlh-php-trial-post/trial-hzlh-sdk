<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IApplyAble;

interface IApplyAbleAdapter
{
    public function approve(IApplyAble $applyAbleObject) : bool;

    public function reject(IApplyAble $applyAbleObject) : bool;
}
