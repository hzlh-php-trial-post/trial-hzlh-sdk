<?php
namespace Trial\Common\Adapter;

use Trial\Common\Model\IModifyStatusAble;

trait ModifyStatusAbleMockAdapterTrait
{
    public function revoke(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        unset($modifyStatusAbleObject);
        return true;
    }

    public function close(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        unset($modifyStatusAbleObject);
        return true;
    }

    public function deletes(IModifyStatusAble $modifyStatusAbleObject) : bool
    {
        unset($modifyStatusAbleObject);
        return true;
    }
}
