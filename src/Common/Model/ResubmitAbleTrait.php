<?php
namespace Trial\Common\Model;

use Trial\Common\Adapter\IResubmitAbleAdapter;

trait ResubmitAbleTrait
{
    public function resubmit() : bool
    {
        $repository = $this->getIResubmitAbleAdapter();

        return $repository->resubmit($this);
    }

    abstract protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter;
}
