<?php
namespace Trial\Common\Model;

interface IResubmitAble
{
    public function resubmit() : bool;
}
