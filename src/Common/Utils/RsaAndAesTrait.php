<?php
namespace Trial\Common\Utils;

use Trial\Common\Utils\AesUtils;
use Trial\Common\Utils\RsaUtils;

trait RsaAndAesTrait
{
    protected function getAesUtils()
    {
        return new AesUtils();
    }

    protected function getRsaUtils()
    {
        return new RsaUtils();
    }
}
