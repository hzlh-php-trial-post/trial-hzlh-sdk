<?php
namespace Trial\Common\CommandHandler;

use Trial\Common\Model\IApplyAble;
use Trial\Common\Command\ApproveCommand;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

abstract class ApproveCommandHandler implements ICommandHandler
{
    abstract protected function fetchIApplyObject($id) : IApplyAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ApproveCommand $command)
    {
        $this->approveAble = $this->fetchIApplyObject($command->id);

        if ($this->approveAble->approve()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
