<?php
namespace Trial\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Trial\Common\Model\IOnShelfAble;
use Trial\Common\Command\OnShelfCommand;

abstract class OnShelfCommandHandler implements ICommandHandler
{
    abstract protected function fetchIOnShelfObject($id) : IOnShelfAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(OnShelfCommand $command)
    {
        $this->onShelfAble = $this->fetchIOnShelfObject($command->id);

        if ($this->onShelfAble->onShelf()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
