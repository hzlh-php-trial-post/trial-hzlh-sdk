<?php
namespace Trial\Common\CommandHandler;

use Trial\Common\Model\IApplyAble;
use Trial\Common\Command\RejectCommand;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

abstract class RejectCommandHandler implements ICommandHandler
{
    abstract protected function fetchIApplyObject($id) : IApplyAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(RejectCommand $command)
    {
        $this->rejectAble = $this->fetchIApplyObject($command->id);

        $this->rejectAble->setRejectReason($command->rejectReason);
        
        if ($this->rejectAble->reject()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
