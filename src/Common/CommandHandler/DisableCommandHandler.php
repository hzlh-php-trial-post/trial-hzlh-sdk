<?php
namespace Trial\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Trial\Common\Model\IEnableAble;
use Trial\Common\Command\DisableCommand;

abstract class DisableCommandHandler implements ICommandHandler
{
    abstract protected function fetchIEnableObject($id) : IEnableAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(DisableCommand $command)
    {
        $this->enableAble = $this->fetchIEnableObject($command->id);

        if ($this->enableAble->disable()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
