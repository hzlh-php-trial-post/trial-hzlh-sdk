<?php
namespace Trial\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Trial\Common\Model\IRecommendAble;
use Trial\Common\Command\CancelRecommendCommand;

abstract class CancelRecommendCommandHandler implements ICommandHandler
{
    abstract protected function fetchIRecommendObject($id) : IRecommendAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(CancelRecommendCommand $command)
    {
        $this->recommendAble = $this->fetchIRecommendObject($command->id);

        if ($this->recommendAble->cancelRecommend()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
