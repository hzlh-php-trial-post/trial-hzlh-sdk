<?php
namespace Trial\Common\CommandHandler;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Trial\Log\Model\ILogAble;
use Trial\Common\Utils\LogDriverCommandHandlerTrait;

use Trial\Common\Model\IModifyStatusAble;
use Trial\Common\Command\RevokeCommand;

abstract class RevokeCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;
    abstract protected function fetchIModifyStatusObject($id) : IModifyStatusAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(RevokeCommand $command)
    {
        $this->revokeAble = $this->fetchIModifyStatusObject($command->id);

        if ($this->revokeAble->revoke()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }
}
