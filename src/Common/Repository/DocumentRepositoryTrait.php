<?php
namespace Trial\Common\Repository;

use Trial\Common\Model\Document;

trait DocumentRepositoryTrait
{
	public function add(Document $document) 
	{
		return $this->getAdapter()->add($document);
	}

	public function fetchOne(Document $document)
	{
		return $this->getAdapter()->fetchOne($document);
	}
}