<?php
namespace Trial\Common\Repository;

use Trial\Common\Model\ITopAble;

trait TopAbleRepositoryTrait
{
    public function top(ITopAble $topAbleObject) : bool
    {
        return $this->getAdapter()->top($topAbleObject);
    }

    public function cancelTop(ITopAble $topAbleObject) : bool
    {
        return $this->getAdapter()->cancelTop($topAbleObject);
    }
}
