<?php
namespace Trial\Common\Repository;

use Trial\Common\Model\IApplyAble;

trait ApplyAbleRepositoryTrait
{
    public function approve(IApplyAble $applyAbleObject) : bool
    {
        return $this->getAdapter()->approve($applyAbleObject);
    }

    public function reject(IApplyAble $applyAbleObject) : bool
    {
        return $this->getAdapter()->reject($applyAbleObject);
    }
}
