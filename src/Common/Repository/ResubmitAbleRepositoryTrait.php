<?php
namespace Trial\Common\Repository;

use Trial\Common\Model\IResubmitAble;

trait ResubmitAbleRepositoryTrait
{
    public function resubmit(IResubmitAble $resubmitAbleObject) : bool
    {
        return $this->getAdapter()->resubmit($resubmitAbleObject);
    }
}
