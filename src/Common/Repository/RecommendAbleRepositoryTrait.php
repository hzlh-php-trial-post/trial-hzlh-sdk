<?php
namespace Trial\Common\Repository;

use Trial\Common\Model\IRecommendAble;

trait RecommendAbleRepositoryTrait
{
    public function recommend(IRecommendAble $recommendAbleObject) : bool
    {
        return $this->getAdapter()->recommend($recommendAbleObject);
    }

    public function cancelRecommend(IRecommendAble $recommendAbleObject) : bool
    {
        return $this->getAdapter()->cancelRecommend($recommendAbleObject);
    }
}
