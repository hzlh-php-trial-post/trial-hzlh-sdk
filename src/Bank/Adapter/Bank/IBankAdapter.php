<?php
namespace Trial\Bank\Adapter\Bank;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IBankAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
