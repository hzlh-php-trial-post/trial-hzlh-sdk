<?php
namespace Trial\Bank\Repository;

use Trial\Bank\Adapter\Bank\IBankAdapter;
use Trial\Bank\Adapter\Bank\BankMockAdapter;
use Trial\Bank\Adapter\Bank\BankRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

class BankRepository extends Repository implements IBankAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'BANK_LIST';
    const FETCH_ONE_MODEL_UN = 'BANK_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new BankRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IBankAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBankAdapter
    {
        return new BankMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
