<?php
namespace Trial\DispatchDepartment\Adapter\DispatchDepartment;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IEnableAbleAdapter;

interface IDispatchDepartmentAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter, IAsyncAdapter //phpcs:ignore
{
}
