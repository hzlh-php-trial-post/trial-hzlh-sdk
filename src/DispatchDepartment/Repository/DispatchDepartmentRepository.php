<?php
namespace Trial\DispatchDepartment\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\EnableAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\DispatchDepartment\Adapter\DispatchDepartment\IDispatchDepartmentAdapter;
use Trial\DispatchDepartment\Adapter\DispatchDepartment\DispatchDepartmentMockAdapter;
use Trial\DispatchDepartment\Adapter\DispatchDepartment\DispatchDepartmentRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class DispatchDepartmentRepository extends Repository implements IDispatchDepartmentAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'DISPATCHDEPARTMENT_LIST';
    const FETCH_ONE_MODEL_UN = 'DISPATCHDEPARTMENT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new DispatchDepartmentRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IDispatchDepartmentAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IDispatchDepartmentAdapter
    {
        return new DispatchDepartmentMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
