<?php
namespace Trial\BankCard\Adapter\BankCard;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\BankCard\Model\BankCard;

interface IBankCardAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function bind(BankCard $bankCard) : bool;

    public function unBind(BankCard $bankCard) : bool;
}
