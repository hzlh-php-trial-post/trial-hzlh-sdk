<?php
namespace Trial\AccountBook\Translator;

use Trial\AccountBook\Model\AccountBook;
use Trial\AccountBook\Model\NullAccountBook;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\AccountTemplate\Translator\AccountTemplateRestfulTranslator;

class AccountBookRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getAccountTemplateRestfulTranslator()
    {
        return new AccountTemplateRestfulTranslator();
    }

    public function arrayToObject(array $expression, $accountBook = null)
    {
        return $this->translateToObject($expression, $accountBook);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $accountBook = null)
    {
        if (empty($expression)) {
            return NullAccountBook::getInstance();
        }

        if ($accountBook == null) {
            $accountBook = new AccountBook();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $accountBook->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['subjectId'])) {
            $accountBook->setSubjectId($attributes['subjectId']);
        }
        if (isset($attributes['subjectName'])) {
            $accountBook->setSubjectName($attributes['subjectName']);
        }
        if (isset($attributes['subjectParentId'])) {
            $accountBook->setSubjectParentId($attributes['subjectParentId']);
        }
        if (isset($attributes['subjectNumber'])) {
            $accountBook->setSubjectNumber($attributes['subjectNumber']);
        }
        if (isset($attributes['accountingPeriod'])) {
            $accountBook->setAccountingPeriod($attributes['accountingPeriod']);
        }
        if (isset($attributes['level'])) {
            $accountBook->setLevel($attributes['level']);
        }
        if (isset($attributes['direction'])) {
            $accountBook->setDirection($attributes['direction']);
        }
        if (isset($attributes['sourceType'])) {
            $accountBook->setSourceType($attributes['sourceType']);
        }
        if (isset($attributes['accountTemplate'])) {
            $accountBook->setAccountTemplate($attributes['accountTemplate']);
        }
        if (isset($attributes['periodBegins'])) {
            $accountBook->setPeriodBegins($attributes['periodBegins']);
        }
        if (isset($attributes['voucherRecords'])) {
            $accountBook->setVoucherRecords($attributes['voucherRecords']);
        }
        if (isset($attributes['updateTime'])) {
            $accountBook->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $accountBook->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $accountBook->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['accountTemplate']['data'])) {
            $accountTemplate = $this->changeArrayFormat($relationships['accountTemplate']['data']);
            $accountBook->setAccountTemplate(
                $this->getAccountTemplateRestfulTranslator()->arrayToObject($accountTemplate)
            );
        }

        return $accountBook;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($accountBook, array $keys = array())
    {
        $expression = array();

        if (!$accountBook instanceof AccountBook) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'subjectId',
                'subjectName'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'accountBooks'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $accountBook->getId();
        }

        $attributes = array();

        if (in_array('subjectId', $keys)) {
            $attributes['subjectId'] = $accountBook->getSubjectId();
        }
        if (in_array('subjectName', $keys)) {
            $attributes['subjectName'] = $accountBook->getSubjectName();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
