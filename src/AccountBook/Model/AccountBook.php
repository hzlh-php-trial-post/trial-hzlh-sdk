<?php
namespace Trial\AccountBook\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Trial\AccountTemplate\Model\AccountTemplate;

class AccountBook implements IObject
{
    use Object;

    const STATUS_NORMAL = 0; //正常

    private $id;

    private $subjectId;

    private $subjectName;

    private $subjectNumber;

    private $accountingPeriod;

    private $subjectParentId;

    private $level;

    private $direction;

    private $sourceType;

    private $accountTemplate;

    private $periodBegins;

    private $voucherRecords;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->subjectId = '';
        $this->subjectName = '';
        $this->subjectParentId = '';
        $this->subjectNumber = 0;
        $this->accountingPeriod = 0;
        $this->level = 0;
        $this->direction = 0;
        $this->sourceType = 0;
        $this->accountTemplate = new AccountTemplate();
        $this->periodBegins = array();
        $this->voucherRecords = array();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->subjectId);
        unset($this->subjectName);
        unset($this->subjectParentId);
        unset($this->subjectNumber);
        unset($this->accountingPeriod);
        unset($this->level);
        unset($this->direction);
        unset($this->sourceType);
        unset($this->accountTemplate);
        unset($this->periodBegins);
        unset($this->voucherRecords);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSubjectId(string $subjectId) : void
    {
        $this->subjectId = $subjectId;
    }

    public function getSubjectId() : string
    {
        return $this->subjectId;
    }

    public function setSubjectName(string $subjectName) : void
    {
        $this->subjectName = $subjectName;
    }

    public function getSubjectName() : string
    {
        return $this->subjectName;
    }

    public function setSubjectNumber(int $subjectNumber) : void
    {
        $this->subjectNumber = $subjectNumber;
    }

    public function getSubjectNumber() : int
    {
        return $this->subjectNumber;
    }

    public function setSubjectParentId(int $subjectParentId) : void
    {
        $this->subjectParentId = $subjectParentId;
    }

    public function getSubjectParentId() : int
    {
        return $this->subjectParentId;
    }

    public function setAccountingPeriod(int $accountingPeriod) : void
    {
        $this->accountingPeriod = $accountingPeriod;
    }

    public function getAccountingPeriod() : int
    {
        return $this->accountingPeriod;
    }

    public function setLevel(int $level) : void
    {
        $this->level = $level;
    }

    public function getLevel() : int
    {
        return $this->level;
    }

    public function setDirection(int $direction) : void
    {
        $this->direction = $direction;
    }

    public function getDirection() : int
    {
        return $this->direction;
    }

    public function setSourceType(int $sourceType) : void
    {
        $this->sourceType = $sourceType;
    }

    public function getSourceType() : int
    {
        return $this->sourceType;
    }

    public function setAccountTemplate(AccountTemplate $accountTemplate) : void
    {
        $this->accountTemplate = $accountTemplate;
    }

    public function getAccountTemplate() : AccountTemplate
    {
        return $this->accountTemplate;
    }

    public function setPeriodBegins(array $periodBegins) : void
    {
        $this->periodBegins = $periodBegins;
    }

    public function getPeriodBegins() : array
    {
        return $this->periodBegins;
    }

    public function setVoucherRecords(array $voucherRecords) : void
    {
        $this->voucherRecords = $voucherRecords;
    }

    public function getVoucherRecords() : array
    {
        return $this->voucherRecords;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
