<?php
namespace Trial\AccountBook\Model;

use Marmot\Interfaces\INull;

class NullAccountBook extends AccountBook implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
