<?php
namespace Trial\AccountBook\Adapter\AccountBook;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\AccountBook\Model\NullAccountBook;
use Trial\AccountBook\Translator\AccountBookRestfulTranslator;

use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class AccountBookRestfulAdapter extends GuzzleAdapter implements IAccountBookAdapter
{
    use FetchAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'ACCOUNT_BOOK_LIST'=>[
                'fields'=>[],
                'include'=> 'accountTemplate'
            ],
            'ACCOUNT_BOOK_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'accountTemplate'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new AccountBookRestfulTranslator();
        $this->resource = 'accountBooks';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullAccountBook::getInstance());
    }
}
