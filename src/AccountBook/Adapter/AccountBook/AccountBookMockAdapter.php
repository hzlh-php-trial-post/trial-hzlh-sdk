<?php
namespace Trial\AccountBook\Adapter\AccountBook;

use Trial\AccountBook\Utils\MockFactory;

class AccountBookMockAdapter implements IAccountBookAdapter
{
    public function fetchOne($id)
    {
        return MockFactory::generateAccountBookObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $accountBookList = array();

        foreach ($ids as $id) {
            $accountBookList[] = MockFactory::generateAccountBookObject($id);
        }

        return $accountBookList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateAccountBookObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $accountBookList = array();

        foreach ($ids as $id) {
            $accountBookList[] = MockFactory::generateAccountBookObject($id);
        }

        return $accountBookList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
