<?php
namespace Trial\AccountBook\Adapter\AccountBook;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IAccountBookAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
