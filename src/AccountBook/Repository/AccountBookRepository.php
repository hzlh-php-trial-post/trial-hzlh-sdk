<?php
namespace Trial\AccountBook\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\AccountBook\Adapter\AccountBook\IAccountBookAdapter;
use Trial\AccountBook\Adapter\AccountBook\AccountBookMockAdapter;
use Trial\AccountBook\Adapter\AccountBook\AccountBookRestfulAdapter;

class AccountBookRepository extends Repository implements IAccountBookAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ACCOUNT_BOOK_LIST';
    const FETCH_ONE_MODEL_UN = 'ACCOUNT_BOOK_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new AccountBookRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IAccountBookAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IAccountBookAdapter
    {
        return new AccountBookMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
