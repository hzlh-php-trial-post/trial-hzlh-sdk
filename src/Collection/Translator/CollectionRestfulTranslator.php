<?php
namespace Trial\Collection\Translator;

use Trial\Collection\Model\Collection;
use Trial\Collection\Model\NullCollection;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Member\Translator\MemberRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class CollectionRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function arrayToObject(array $expression, $collection = null)
    {
        return $this->translateToObject($expression, $collection);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $collection = null)
    {
        if (empty($expression)) {
            return NullCollection::getInstance();
        }

        if ($collection == null) {
            $collection = new Collection();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $collection->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['primaryId'])) {
            $collection->setPrimaryId($attributes['primaryId']);
        }
        if (isset($attributes['category'])) {
            $collection->setCategory($attributes['category']);
        }
        if (isset($attributes['primaryObject'])) {
            $collection->setPrimaryObject($attributes['primaryObject']);
        }
        if (isset($attributes['createTime'])) {
            $collection->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $collection->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $collection->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $collection->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $collection->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        return $collection;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($collection, array $keys = array())
    {
        $expression = array();

        if (!$collection instanceof Collection) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'primaryId',
                'category',
                'primaryObject',
                'member'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'collections'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $collection->getId();
        }

        $attributes = array();

        if (in_array('primaryId', $keys)) {
            $attributes['primaryId'] = $collection->getPrimaryId();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $collection->getCategory();
        }
        if (in_array('primaryObject', $keys)) {
            $attributes['primaryObject'] = $collection->getPrimaryObject();
        }
        if (in_array('ids', $keys)) {
            $attributes['ids'] = $collection->getIds();
        }
         $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['members']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $collection->getMember()->getId()
                )
             );
        }

        return $expression;
    }
}
