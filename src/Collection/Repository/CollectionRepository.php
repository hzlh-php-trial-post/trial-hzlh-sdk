<?php
namespace Trial\Collection\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;

use Trial\Collection\Adapter\Collection\ICollectionAdapter;
use Trial\Collection\Adapter\Collection\CollectionMockAdapter;
use Trial\Collection\Adapter\Collection\CollectionRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Collection\Model\Collection;

class CollectionRepository extends Repository implements ICollectionAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const PORTAL_LIST_MODEL_UN = 'PORTAL_COLLECTION_LIST';
    const FETCH_ONE_MODEL_UN = 'COLLECTION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CollectionRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : ICollectionAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ICollectionAdapter
    {
        return new CollectionMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function cancelCollect(Collection $collection) : bool
    {
        return $this->getAdapter()->cancelCollect($collection);
    }
}
