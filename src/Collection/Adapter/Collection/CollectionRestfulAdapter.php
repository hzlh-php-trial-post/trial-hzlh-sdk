<?php
namespace Trial\Collection\Adapter\Collection;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Collection\Model\Collection;
use Trial\Collection\Model\NullCollection;
use Trial\Collection\Translator\CollectionRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class CollectionRestfulAdapter extends GuzzleAdapter implements ICollectionAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'PORTAL_COLLECTION_LIST'=>[
                'fields'=>[],
                'include'=> ''
            ],
            'COLLECTION_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> ''
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CollectionRestfulTranslator();
        $this->resource = 'collections';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCollection::getInstance());
    }

    protected function addAction(Collection $collection) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $collection,
            array(
                'primaryId',
                'category',
                'member'
            )
        );

        $this->post(
            $this->getResource().'/collect',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($collection);
            return true;
        }

        return false;
    }

    protected function editAction(Collection $collection) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $collection,
            array(
                'launchType',
                'type',
                'place',
                'image',
                'link',
                'remark'
            )
        );

        $this->patch(
            $this->getResource().'/'.$collection->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($collection);
            return true;
        }

        return false;
    }

    public function cancelCollect(Collection $sollection) : bool
    {
        return $this->cancelCollectAction($sollection);
    }
    protected function cancelCollectAction(Collection $sollection) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $sollection,
            array(
                'ids'
            )
        );

        $this->patch(
            $this->getResource().'/cancelCollect',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($sollection);
            return true;
        }

        return false;
    }
}
