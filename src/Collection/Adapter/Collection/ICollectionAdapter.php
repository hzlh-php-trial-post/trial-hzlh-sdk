<?php
namespace Trial\Collection\Adapter\Collection;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;
use Trial\Collection\Model\Collection;

interface ICollectionAdapter extends IFetchAbleAdapter, IOperatAbleAdapter
{
    public function cancelCollect(Collection $collection);
}
