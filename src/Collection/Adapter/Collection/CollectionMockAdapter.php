<?php
namespace Trial\Collection\Adapter\Collection;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;

use Trial\Collection\Model\Collection;
use Trial\Collection\Model\NullCollection;
use Trial\Collection\Utils\CollectionMockFactory;

class CollectionMockAdapter implements ICollectionAdapter
{
    use OperatAbleMockAdapterTrait;

    public function cancelCollect(Collection $collection) : bool
    {
        unset($collection);
        return true;
    }

    public function fetchOne($id)
    {
        return CollectionMockFactory::generateCollectionObject(NullCollection::getInstance(), $id);
    }

    public function fetchList(array $ids) : array
    {
        $collectionList = array();

        foreach ($ids as $id) {
            $collectionList[] = CollectionMockFactory::generateCollectionObject(NullCollection::getInstance(), $id);
        }

        return $collectionList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return CollectionMockFactory::generateCollectionObject(NullCollection::getInstance(), $id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = CollectionMockFactory::generateCollectionObject(NullCollection::getInstance(), $id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
