<?php
namespace Trial\Collection\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\Member\Model\Member;

use Trial\Collection\Repository\CollectionRepository;

class Collection implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    /**
     * @var int $id id
     */
    private $id;

    private $member;

    private $primaryId;

    private $category;

    private $primaryObject;

    private $repository;

    private $ids;

    const COLLECTION_CATEGORY = array(
        'DEFAULT' => 0,
        'SERVICE' => 1,
        'LOAN_PRODUCT' => 2,
        'ENTERPRISE' => 3,
    );

    const COLLECTION_STATUS = array(
        'COLLECTED' => 2,
        'CANCEL' => -2,
    );

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->member = new Member();
        $this->primaryId = 0;
        $this->primaryObject = array();
        $this->category = self::COLLECTION_CATEGORY['DEFAULT'];
        $this->status = self::COLLECTION_STATUS['CANCEL'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new CollectionRepository();
        $this->ids = array();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->primaryId);
        unset($this->category);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
        unset($this->primaryObject);
        unset($this->repository);
        unset($this->ids);
    }
    
    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setMember(Member $member) :void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setCategory(int $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setPrimaryId(int $primaryId) : void
    {
        $this->primaryId = $primaryId;
    }

    public function getPrimaryId() : int
    {
        return $this->primaryId;
    }

    public function setPrimaryObject(array $primaryObject) : void
    {
        $this->primaryObject = $primaryObject;
    }

    public function getPrimaryObject() : array
    {
        return $this->primaryObject;
    }

    protected function getRepository() : CollectionRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
    
    protected function addAction(): bool
    {
        $repository = $this->getIOperatAbleAdapter();

        return $repository->add($this);
    }

    protected function editAction(): bool
    {
        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'update_time',
                'create_time'
            )
        );
    }

    public function setIds(string $ids) : void
    {
        $this->ids = $ids;
    }

    public function getIds() : string
    {
        return $this->ids;
    }

    public function cancelCollect() : bool
    {
        return $this->getRepository()->cancelCollect($this);
    }
}
