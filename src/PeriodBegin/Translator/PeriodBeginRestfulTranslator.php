<?php
namespace Trial\PeriodBegin\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\PeriodBegin\Model\PeriodBegin;
use Trial\PeriodBegin\Model\NullPeriodBegin;

use Trial\AccountTemplate\Translator\AccountTemplateRestfulTranslator;

class PeriodBeginRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getAccountTemplateRestfulTranslator()
    {
        return new AccountTemplateRestfulTranslator();
    }

    public function arrayToObject(array $expression, $periodBegin = null)
    {
        return $this->translateToObject($expression, $periodBegin);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $periodBegin = null)
    {
        if (empty($expression)) {
            return NullPeriodBegin::getInstance();
        }

        if ($periodBegin == null) {
            $periodBegin = new PeriodBegin();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $periodBegin->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['periodBeginBalance'])) {
            $periodBegin->setPeriodBeginBalance($attributes['periodBeginBalance']);
        }
        if (isset($attributes['accumulatedDebitAmount'])) {
            $periodBegin->setAccumulatedDebitAmount($attributes['accumulatedDebitAmount']);
        }
        if (isset($attributes['accumulatedCreditAmount'])) {
            $periodBegin->setAccumulatedCreditAmount($attributes['accumulatedCreditAmount']);
        }
        if (isset($attributes['yearBeginBalance'])) {
            $periodBegin->setYearBeginBalance($attributes['yearBeginBalance']);
        }
        if (isset($attributes['accountSubjectId'])) {
            $periodBegin->setAccountSubjectId($attributes['accountSubjectId']);
        }
        if (isset($attributes['status'])) {
            $periodBegin->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['accountTemplate']['data'])) {
            $accountTemplate = $this->changeArrayFormat($relationships['accountTemplate']['data']);
            $periodBegin->setAccountTemplate(
                $this->getAccountTemplateRestfulTranslator()->arrayToObject($accountTemplate)
            );
        }

        return $periodBegin;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($periodBegin, array $keys = array())
    {
        if (!$periodBegin instanceof PeriodBegin) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'periodBeginBalance',
                'accumulatedDebitAmount',
                'accumulatedCreditAmount',
                'yearBeginBalance',
                'accountTemplate',
                'accountSubjectId'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'periodBegins'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $periodBegin->getId();
        }

        $attributes = array();

        if (in_array('periodBeginBalance', $keys)) {
            $attributes['periodBeginBalance'] = $periodBegin->getPeriodBeginBalance();
        }
        if (in_array('accumulatedDebitAmount', $keys)) {
            $attributes['accumulatedDebitAmount'] = $periodBegin->getAccumulatedDebitAmount();
        }
        if (in_array('accumulatedCreditAmount', $keys)) {
            $attributes['accumulatedCreditAmount'] = $periodBegin->getAccumulatedCreditAmount();
        }
        if (in_array('yearBeginBalance', $keys)) {
            $attributes['yearBeginBalance'] = $periodBegin->getYearBeginBalance();
        }
        if (in_array('accountSubjectId', $keys)) {
            $attributes['accountSubjectId'] = $periodBegin->getAccountSubjectId();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('accountTemplate', $keys)) {
            $expression['data']['relationships']['accountTemplate']['data'] = array(
                array(
                    'type'=>'accountTemplates',
                    'id'=>$periodBegin->getAccountTemplate()->getId()
                )
            );
        }

        return $expression;
    }
}
