<?php
namespace Trial\PeriodBegin\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\AccountTemplate\Model\AccountTemplate;

use Trial\PeriodBegin\Repository\PeriodBeginRepository;

class PeriodBegin implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    /**
     * [$id 主键Id]
     * @var [int]
     */
    private $id;
    /**
     * [$periodBeginBalance 期初余额]
     * @var [float]
     */
    private $periodBeginBalance;
    /**
     * [$accumulatedDebitAmount 借方累计]
     * @var [float]
     */
    private $accumulatedDebitAmount;
    /**
     * [$accumulatedCreditAmount 贷方累计]
     * @var [float]
     */
    private $accumulatedCreditAmount;
    /**
     * [$yearBeginBalance 年初余额]
     * @var [float]
     */
    private $yearBeginBalance;
    /**
     * [$accountTemplate 所属帐套]
     * @var [AccountTemplate]
     */
    private $accountTemplate;
    /**
     * [$accountSubjectId 所属科目]
     * @var [int]
     */
    private $accountSubjectId;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->periodBeginBalance = 0.00;
        $this->accumulatedDebitAmount = 0.00;
        $this->accumulatedCreditAmount = 0.00;
        $this->yearBeginBalance = 0.00;
        $this->accountTemplate = new AccountTemplate();
        $this->accountSubjectId = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = 0;
        $this->statusTime = 0;
        $this->repository = new PeriodBeginRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->periodBeginBalance);
        unset($this->accumulatedDebitAmount);
        unset($this->accumulatedCreditAmount);
        unset($this->yearBeginBalance);
        unset($this->accountTemplate);
        unset($this->accountSubjectId);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setPeriodBeginBalance(float $periodBeginBalance): void
    {
        $this->periodBeginBalance = $periodBeginBalance;
    }

    public function getPeriodBeginBalance(): float
    {
        return $this->periodBeginBalance;
    }

    public function setAccumulatedDebitAmount(float $accumulatedDebitAmount): void
    {
        $this->accumulatedDebitAmount = $accumulatedDebitAmount;
    }

    public function getAccumulatedDebitAmount(): float
    {
        return $this->accumulatedDebitAmount;
    }

    public function setAccumulatedCreditAmount(float $accumulatedCreditAmount): void
    {
        $this->accumulatedCreditAmount = $accumulatedCreditAmount;
    }

    public function getAccumulatedCreditAmount(): float
    {
        return $this->accumulatedCreditAmount;
    }

    public function setYearBeginBalance(float $yearBeginBalance): void
    {
        $this->yearBeginBalance = $yearBeginBalance;
    }

    public function getYearBeginBalance(): float
    {
        return $this->yearBeginBalance;
    }

    public function setAccountTemplate(AccountTemplate $accountTemplate): void
    {
        $this->accountTemplate = $accountTemplate;
    }

    public function getAccountTemplate(): AccountTemplate
    {
        return $this->accountTemplate;
    }

    public function setAccountSubjectId(int $accountSubjectId): void
    {
        $this->accountSubjectId = $accountSubjectId;
    }

    public function getAccountSubjectId(): int
    {
        return $this->accountSubjectId;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository(): PeriodBeginRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
