<?php
namespace Trial\PeriodBegin\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\PeriodBegin\Adapter\PeriodBegin\IPeriodBeginAdapter;
use Trial\PeriodBegin\Adapter\PeriodBegin\PeriodBeginMockAdapter;
use Trial\PeriodBegin\Adapter\PeriodBegin\PeriodBeginRestfulAdapter;
use Trial\PeriodBegin\Model\PeriodBegin;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class PeriodBeginRepository extends Repository implements IPeriodBeginAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'PERIOD_BEGIN_LIST';
    const FETCH_ONE_MODEL_UN = 'PERIOD_BEGIN_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PeriodBeginRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IPeriodBeginAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IPeriodBeginAdapter
    {
        return new PeriodBeginMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
