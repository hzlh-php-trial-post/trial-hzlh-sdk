<?php
namespace Trial\PeriodBegin\Adapter\PeriodBegin;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;

use Trial\PeriodBegin\Model\PeriodBegin;
use Trial\PeriodBegin\Utils\MockFactory;

class PeriodBeginMockAdapter implements IPeriodBeginAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generatePeriodBeginObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $periodBeginList = array();

        foreach ($ids as $id) {
            $periodBeginList[] = MockFactory::generatePeriodBeginObject($id);
        }

        return $periodBeginList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generatePeriodBeginObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $periodBeginList = array();

        foreach ($ids as $id) {
            $periodBeginList[] = MockFactory::generatePeriodBeginObject($id);
        }

        return $periodBeginList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
