<?php
namespace Trial\PeriodBegin\Adapter\PeriodBegin;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IPeriodBeginAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
