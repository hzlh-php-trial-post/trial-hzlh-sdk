<?php
namespace Trial\PeriodBegin\Adapter\PeriodBegin;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\PeriodBegin\Model\PeriodBegin;
use Trial\PeriodBegin\Model\NullPeriodBegin;
use Trial\PeriodBegin\Translator\PeriodBeginRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class PeriodBeginRestfulAdapter extends GuzzleAdapter implements IPeriodBeginAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'PERIOD_BEGIN_LIST'=>[
            'fields'=>[],
            'include'=> 'periodBegin,accountTemplate'
        ],
        'PERIOD_BEGIN_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'periodBegin,accountTemplate'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PeriodBeginRestfulTranslator();
        $this->resource = 'periodBegins';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPeriodBegin::getInstance());
    }

    protected function addAction(PeriodBegin $periodBegin) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $periodBegin,
            array(
                'periodBeginBalance',
                'accumulatedDebitAmount',
                'accumulatedCreditAmount',
                'accountSubjectId',
                'accountTemplate'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($periodBegin);
            return true;
        }

        return false;
    }

    protected function editAction(PeriodBegin $periodBegin) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $periodBegin,
            array(
                'periodBeginBalance',
                'accumulatedDebitAmount',
                'accumulatedCreditAmount',
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$periodBegin->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($periodBegin);
            return true;
        }

        return false;
    }
}
