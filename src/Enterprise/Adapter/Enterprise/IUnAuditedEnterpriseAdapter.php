<?php
namespace Trial\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;
use Trial\Common\Adapter\IApplyAbleAdapter;

interface IUnAuditedEnterpriseAdapter extends IFetchAbleAdapter, IResubmitAbleAdapter, IApplyAbleAdapter, IAsyncAdapter
{
}
