<?php
namespace Trial\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IEnterpriseAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
