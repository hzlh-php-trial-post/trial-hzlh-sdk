<?php
namespace Trial\TradeRecord\Repository;

use Trial\TradeRecord\Adapter\TradeRecord\ITradeRecordAdapter;
use Trial\TradeRecord\Adapter\TradeRecord\TradeRecordMockAdapter;
use Trial\TradeRecord\Adapter\TradeRecord\TradeRecordRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

class TradeRecordRepository extends Repository implements ITradeRecordAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_TRADE_RECORD_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_TRADE_RECORD_LIST';
    const FETCH_ONE_MODEL_UN = 'TRADE_RECORD_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new TradeRecordRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ITradeRecordAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ITradeRecordAdapter
    {
        return new TradeRecordMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
