<?php
namespace Trial\TradeRecord\Adapter\TradeRecord;

use Trial\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface ITradeRecordAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
