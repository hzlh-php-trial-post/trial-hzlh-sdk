<?php
namespace Trial\TradeRecord\Translator;

use Trial\TradeRecord\Model\TradeRecord;
use Trial\TradeRecord\Model\NullTradeRecord;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\MemberAccount\Translator\MemberAccountRestfulTranslator;

use Trial\TradeRecord\Translator\TranslatorFactory;

class TradeRecordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberAccountRestfulTranslator() : MemberAccountRestfulTranslator
    {
        return new MemberAccountRestfulTranslator();
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function arrayToObject(array $expression, $tradeRecord = null)
    {
        return $this->translateToObject($expression, $tradeRecord);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     *

     */
    protected function translateToObject(array $expression, $tradeRecord = null)
    {
        if (empty($expression)) {
            return NullTradeRecord::getInstance();
        }

        if ($tradeRecord == null) {
            $tradeRecord = new TradeRecord();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $tradeRecord->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['tradeTime'])) {
            $tradeRecord->setTradeTime($attributes['tradeTime']);
        }
        if (isset($attributes['tradeType'])) {
            $tradeRecord->setType($attributes['tradeType']);
        }
        if (isset($attributes['tradeMoney'])) {
            $tradeRecord->setTradeMoney($attributes['tradeMoney']);
        }
        if (isset($attributes['debtor'])) {
            $tradeRecord->setDebtor($attributes['debtor']);
        }
        if (isset($attributes['creditor'])) {
            $tradeRecord->setCreditor($attributes['creditor']);
        }
        if (isset($attributes['balance'])) {
            $tradeRecord->setBalance($attributes['balance']);
        }
        if (isset($attributes['comment'])) {
            $tradeRecord->setComment($attributes['comment']);
        }
        if (isset($attributes['createTime'])) {
            $tradeRecord->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $tradeRecord->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $tradeRecord->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $tradeRecord->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['memberAccount']['data'])) {
            if (isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat(
                    $relationships['memberAccount']['data'],
                    $expression['included']
                );
            }
            if (!isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data']);
            }

            $tradeRecord->setMemberAccount(
                $this->getMemberAccountRestfulTranslator()->arrayToObject($memberAccount)
            );
        }
        if (isset($relationships['reference']['data'])) {
            if (isset($expression['included'])) {
                $reference = $this->changeArrayFormat($relationships['reference']['data'], $expression['included']);
            }
            if (!isset($expression['included'])) {
                $reference = $this->changeArrayFormat($relationships['reference']['data']);
            }

            $referenceRestfulTranslator = $this->getTranslatorFactory()->getTranslator($attributes['tradeType']);
            $tradeRecord->setReference($referenceRestfulTranslator->arrayToObject($reference));
        }

        return $tradeRecord;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     *
     */
    public function objectToArray($tradeRecord, array $keys = array())
    {
        unset($tradeRecord);
        unset($keys);
        
        return array();
    }
}
