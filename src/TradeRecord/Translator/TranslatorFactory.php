<?php
namespace Trial\TradeRecord\Translator;

use Trial\TradeRecord\Model\TradeRecord;

use Marmot\Interfaces\IRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        TradeRecord::TRADE_RECORD_TYPES['DEPOSIT'] =>
        'Trial\Deposit\Translator\DepositRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['DEPOSIT_PLATFORM'] =>
        'Trial\Deposit\Translator\DepositRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_ENTERPRISE'] =>
        'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_PLATFORM'] =>
        'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_BUYER_ENTERPRISE'] =>
        'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SELLER_ENTERPRISE'] =>
        'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_PLATFORM'] =>
        'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SUBSIDY'] =>
            'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['BUYER_REFUND'] =>
        'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['PLATFORM_REFUND'] =>
        'Trial\Order\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL'] =>
        'Trial\Withdrawal\Translator\WithdrawalRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL_PLATFORM'] =>
        'Trial\Withdrawal\Translator\WithdrawalRestfulTranslator',
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
