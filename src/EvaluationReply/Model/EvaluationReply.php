<?php
namespace Trial\EvaluationReply\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\EvaluationReply\Repository\EvaluationReplyRepository;

use Trial\Evaluation\Model\Evaluation;
use Trial\Enterprise\Model\Enterprise;

class EvaluationReply implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    const REPLY_TYPE = [
        'NULL' => 0, //未知
        'EVALUATION' => 1, //回复评价
        'REPLY' => 2 //回复别人的回复
    ];

    const STATUS = [
        'NORMAL' => 0, //正常
        'MASKED' => -2 //已屏蔽
    ];

    private $id;
    
    private $evaluation;
    
    private $enterprise;
    
    private $content;
    
    private $replyType;
    
    private $fromReplyId;

    private $reason;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->evaluation = new Evaluation();
        $this->enterprise = new Enterprise();
        $this->content = '';
        $this->replyType = self::REPLY_TYPE['NULL'];
        $this->fromReplyId = 0;
        $this->reason = '';
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new EvaluationReplyRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->evaluation);
        unset($this->enterprise);
        unset($this->content);
        unset($this->replyType);
        unset($this->fromReplyId);
        unset($this->reason);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setEvaluation(Evaluation $evaluation) : void
    {
        $this->evaluation = $evaluation;
    }

    public function getEvaluation() : Evaluation
    {
        return $this->evaluation;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function setReplyType(int $replyType) : void
    {
        $this->replyType = in_array($replyType, self::REPLY_TYPE) ? $replyType : self::REPLY_TYPE['NULL'];
    }

    public function getReplyType() : int
    {
        return $this->replyType;
    }

    public function setFromReplyId(int $fromReplyId) : void
    {
        $this->fromReplyId = $fromReplyId;
    }

    public function getFromReplyId() : int
    {
        return $this->fromReplyId;
    }

    public function setReason(string $reason) : void
    {
        $this->reason = $reason;
    }

    public function getReason() : string
    {
        return $this->reason;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }

    protected function getRepository() : EvaluationReplyRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function maskedEvaluationReply() : bool
    {
        return $this->getRepository()->maskedEvaluationReply($this);
    }

    public function cancelMaskedEvaluationReply() : bool
    {
        return $this->getRepository()->cancelMaskedEvaluationReply($this);
    }
}
