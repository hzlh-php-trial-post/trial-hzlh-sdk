<?php
namespace Trial\EvaluationReply\Adapter\EvaluationReply;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\EvaluationReply\Model\EvaluationReply;

use Marmot\Interfaces\IAsyncAdapter;

interface IEvaluationReplyAdapter extends IAsyncAdapter, IFetchAbleAdapter, IOperatAbleAdapter
{
    public function maskedEvaluationReply(EvaluationReply $evaluationReply);
    
    public function cancelMaskedEvaluationReply(EvaluationReply $evaluationReply);
}
