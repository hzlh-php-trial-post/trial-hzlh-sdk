<?php
namespace Trial\EvaluationReply\Adapter\EvaluationReply;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\EvaluationReply\Model\EvaluationReply;
use Trial\EvaluationReply\Model\NullEvaluationReply;
use Trial\EvaluationReply\Translator\EvaluationReplyRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class EvaluationReplyRestfulAdapter extends GuzzleAdapter implements IEvaluationReplyAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'EVALUATION_REPLY_LIST'=>[
            'fields'=>[],
            'include'=>'enterprise,evaluation,evaluation.order,evaluation.member'
        ],
        'EVALUATION_REPLY_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'enterprise,evaluation,evaluation.order,evaluation.member'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new EvaluationReplyRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'evaluationReplies';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullEvaluationReply::getInstance());
    }

    public function maskedEvaluationReply(EvaluationReply $evaluationReply) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $evaluationReply,
            array(
                'reason'
            )
        );

        $this->patch(
            $this->getResource().'/'.$evaluationReply->getId().'/masked',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($evaluationReply);
            return true;
        }

        return false;
    }

    public function cancelMaskedEvaluationReply(EvaluationReply $evaluationReply) : bool
    {
        $this->patch(
            $this->getResource().'/'.$evaluationReply->getId().'/cancelMasked'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($evaluationReply);
            return true;
        }
        return false;
    }

    protected function addAction(EvaluationReply $evaluationReply) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $evaluationReply,
            array(
                'content',
                'replyType',
                'fromReplyId',
                'evaluation',
                'enterprise'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($evaluationReply);
            return true;
        }

        return false;
    }

    protected function editAction(EvaluationReply $evaluationReply) : bool
    {
        unset($evaluationReply);
        return false;
    }
}
