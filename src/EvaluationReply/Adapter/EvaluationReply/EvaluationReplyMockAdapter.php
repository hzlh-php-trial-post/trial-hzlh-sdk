<?php
namespace Trial\EvaluationReply\Adapter\EvaluationReply;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;

use Trial\EvaluationReply\Model\EvaluationReply;
use Trial\EvaluationReply\Utils\MockFactory;

class EvaluationReplyMockAdapter implements IEvaluationReplyAdapter
{
    use OperatAbleMockAdapterTrait;

    public function maskedEvaluationReply(EvaluationReply $evaluationReply) : bool
    {
        unset($evaluationReply);
        return true;
    }
    
    public function cancelMaskedEvaluationReply(EvaluationReply $evaluationReply)
    {
        unset($evaluationReply);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generateEvaluationReplyObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $evaluationReplyList = array();

        foreach ($ids as $id) {
            $evaluationReplyList[] = MockFactory::generateEvaluationReplyObject($id);
        }

        return $evaluationReplyList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateEvaluationReplyObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $evaluationReplyList = array();

        foreach ($ids as $id) {
            $evaluationReplyList[] = MockFactory::generateEvaluationReplyObject($id);
        }

        return $evaluationReplyList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
