<?php
namespace Trial\EvaluationReply\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\EvaluationReply\Model\EvaluationReply;
use Trial\EvaluationReply\Model\NullEvaluationReply;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Enterprise\Translator\EnterpriseRestfulTranslator;
use Trial\Evaluation\Translator\EvaluationRestfulTranslator;

class EvaluationReplyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getEvaluationRestfulTranslator()
    {
        return new EvaluationRestfulTranslator();
    }

    public function arrayToObject(array $expression, $evaluationReply = null)
    {
        return $this->translateToObject($expression, $evaluationReply);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $evaluationReply = null)
    {
        if (empty($expression)) {
            return NullEvaluationReply::getInstance();
        }

        if ($evaluationReply == null) {
            $evaluationReply = new EvaluationReply();
        }

        $data =  $expression['data'];
        if (isset($data['id'])) {
            $id = $data['id'];
            $evaluationReply->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['content'])) {
            $evaluationReply->setContent($attributes['content']);
        }
        if (isset($attributes['replyType'])) {
            $evaluationReply->setReplyType($attributes['replyType']);
        }
        if (isset($attributes['fromReplyId'])) {
            $evaluationReply->setFromReplyId($attributes['fromReplyId']);
        }
        if (isset($attributes['reason'])) {
            $evaluationReply->setReason($attributes['reason']);
        }
        if (isset($attributes['status'])) {
            $evaluationReply->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $evaluationReply->setUpdateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $evaluationReply->setCreateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $evaluationReply->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);

            $evaluationReply->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }
        if (isset($relationships['evaluation']['data'])) {
            if (isset($expression['included'])) {
                $evaluation = $this->changeArrayFormat(
                    $relationships['evaluation']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $evaluation = $this->changeArrayFormat($relationships['evaluation']['data']);
            }

            $evaluationReply->setEvaluation($this->getEvaluationRestfulTranslator()->arrayToObject($evaluation));
        }

        return $evaluationReply;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($evaluationReply, array $keys = array())
    {
        if (!$evaluationReply instanceof EvaluationReply) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'content',
                'replyType',
                'fromReplyId',
                'reason',
                'evaluation',
                'enterprise'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'evaluationReplies',
                'id'=>$evaluationReply->getId()
            )
        );

        $attributes = array();
        if (in_array('content', $keys)) {
            $attributes['content'] = $evaluationReply->getContent();
        }
        if (in_array('replyType', $keys)) {
            $attributes['replyType'] = $evaluationReply->getReplyType();
        }
        if (in_array('fromReplyId', $keys)) {
            $attributes['fromReplyId'] = $evaluationReply->getFromReplyId();
        }
        if (in_array('reason', $keys)) {
            $attributes['reason'] = $evaluationReply->getReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('evaluation', $keys)) {
            $expression['data']['relationships']['evaluation']['data'] = array(
                array(
                    'type'=>'evaluations',
                    'id'=>$evaluationReply->getEvaluation()->getId()
                )
            );
        }
        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type'=>'enterprises',
                    'id'=>$evaluationReply->getEnterprise()->getId()
                )
            );
        }

        return $expression;
    }
}
