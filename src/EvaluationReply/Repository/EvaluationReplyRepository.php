<?php
namespace Trial\EvaluationReply\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\EvaluationReply\Adapter\EvaluationReply\IEvaluationReplyAdapter;
use Trial\EvaluationReply\Adapter\EvaluationReply\EvaluationReplyMockAdapter;
use Trial\EvaluationReply\Adapter\EvaluationReply\EvaluationReplyRestfulAdapter;
use Trial\EvaluationReply\Model\EvaluationReply;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class EvaluationReplyRepository extends Repository implements IEvaluationReplyAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'EVALUATION_REPLY_LIST';
    const FETCH_ONE_MODEL_UN = 'EVALUATION_REPLY_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new EvaluationReplyRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IEvaluationReplyAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IEvaluationReplyAdapter
    {
        return new EvaluationReplyMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function maskedEvaluationReply(EvaluationReply $evaluationReply) : bool
    {
        return $this->getAdapter()->maskedEvaluationReply($evaluationReply);
    }

    public function cancelMaskedEvaluationReply(EvaluationReply $evaluationReply) : bool
    {
        return $this->getAdapter()->cancelMaskedEvaluationReply($evaluationReply);
    }
}
