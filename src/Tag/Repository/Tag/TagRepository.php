<?php
namespace Trial\Tag\Repository\Tag;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Tag\Adapter\Tag\TagRestfulAdapter;
use Trial\Tag\Adapter\Tag\ITagAdapter;
use Trial\Tag\Adapter\Tag\TagMockAdapter;

class TagRepository extends Repository implements ITagAdapter
{

    use AsyncRepositoryTrait,
    FetchRepositoryTrait,
    OperatAbleRepositoryTrait,
    ErrorRepositoryTrait;

    private $adapter;
    
    const LIST_MODEL_UN = 'TAG_LIST';
    const FETCH_ONE_MODEL_UN = 'TAG_FETCH_ONE';
    
    public function __construct()
    {
        $this->adapter = new TagRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }
    
    public function getActualAdapter() : ITagAdapter
    {
        return $this->adapter;
    }
    
    public function getMockAdapter() : ITagAdapter
    {
        return new TagMockAdapter();
    }
    
    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
