<?php
namespace Trial\Tag\Adapter\Tag;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface ITagAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
