<?php
namespace Trial\Tag\Adapter\Tag;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\Tag\Model\Tag\Tag;
use Trial\Tag\Model\Tag\NullTag;
use Trial\Tag\Translator\Tag\TagRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class TagRestfulAdapter extends GuzzleAdapter implements ITagAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'TAG_LIST'=>[
                'fields'=>[
                    'tags'=>'name,pid,remark,status,createTime,updateTime,statusTime'
                ],
                'include'=> 'crew'
            ],
            'TAG_FETCH_ONE'=>[
                'tags'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new TagRestfulTranslator();
        $this->resource = 'tags';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => TAG_NAME_EXIST,
            208 => TAG_NAME_FORMAT_ERROR,
            122 => TAG_REMARK_FORMAT_ERROR
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullTag::getInstance());
    }

    protected function addAction(Tag $tag) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $tag,
            array(
                'name',
                'pid',
                'remark',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($tag);
            return true;
        }

        return false;
    }

    protected function editAction(Tag $tag) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $tag,
            array(
                'pid',
                'remark'
            )
        );

        $this->patch(
            $this->getResource().'/'.$tag->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($tag);
            return true;
        }

        return false;
    }
}
