<?php
namespace Trial\Tag\Model\Tag;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullOperatAbleTrait;
use Trial\Common\Model\NullEnableAbleTrait;

class NullTag extends Tag implements INull
{
    use NullOperatAbleTrait, NullEnableAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
