<?php
namespace Trial\Tag\Model\Tag;

use Marmot\Common\Model\IObject;
use Marmot\Core;

use Marmot\Common\Model\Object;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Crew\Model\Crew;
use Trial\Tag\Repository\Tag\TagRepository;

class Tag implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    const DATA_LABLE_STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2
    );

    private $id;

    private $name;

    private $remark;

    private $pid;

    private $tags;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->remark = '';
        $this->pid = 0;
        $this->status = self::DATA_LABLE_STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->crew = new Crew();
        $this->repository = new TagRepository();
        $this->tags = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->remark);
        unset($this->pid);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->crew);
        unset($this->repository);
        unset($this->tags);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setPid(int $pid) : void
    {
        $this->pid = $pid;
    }

    public function getPid() : int
    {
        return $this->pid;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setTags(string $tags) : void
    {
        $this->tags = $tags;
    }

    public function getRoles() : string
    {
        return $this->roles;
    }

    protected function getRepository() : TagRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function addAction() : bool
    {
        return $this->getIOperatAbleAdapter()->add($this);
    }

    protected function editAction() : bool
    {
        return $this->getIOperatAbleAdapter()->edit($this);
    }
}
