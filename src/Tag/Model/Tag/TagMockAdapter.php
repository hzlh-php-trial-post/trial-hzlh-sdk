<?php
namespace Trial\Tag\Adapter\Tag;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\EnableAbleMockAdapterTrait;

use Trial\Tag\Model\Tag;
use Trial\Tag\Utils\MockFactory;

class TagMockAdapter implements ITagAdapter
{
    use OperatAbleMockAdapterTrait, EnableAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateTagObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $tagList = array();

        foreach ($ids as $id) {
            $tagList[] = MockFactory::generateTagObject($id);
        }

        return $tagList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateTagObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateTagObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
