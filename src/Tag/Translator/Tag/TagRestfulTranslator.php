<?php
namespace Trial\Tag\Translator\Tag;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\Tag\Model\Tag\Tag;
use Trial\Tag\Model\Tag\NullTag;
use Trial\Crew\Translator\CrewRestfulTranslator;

class TagRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $tag = null)
    {
        return $this->translateToObject($expression, $tag);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $tag = null)
    {
        if (empty($expression)) {
            return NullTag::getInstance();
        }

        if ($tag == null) {
            $tag = new Tag();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $tag->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $tag->setName($attributes['name']);
        }
        if (isset($attributes['remark'])) {
            $tag->setRemark($attributes['remark']);
        }
        if (isset($attributes['pid'])) {
            $tag->setPid($attributes['pid']);
        }
        if (isset($attributes['createTime'])) {
            $tag->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $tag->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $tag->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $tag->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $tag->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $tag;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($tag, array $keys = array())
    {
        $expression = array();

        if (!$tag instanceof Tag) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'remark',
                'pid',
                'status',
                'createTime',
                'updateTime',
                'statusTime',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'tags'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $tag->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $tag->getName();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $tag->getRemark();
        }
        if (in_array('pid', $keys)) {
            $attributes['pid'] = $tag->getPid();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $tag->getStatus();
        }
 
        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $tag->getCrew()->getId()
                )
             );
        }

        return $expression;
    }
}
