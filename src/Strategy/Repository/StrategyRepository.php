<?php
namespace Trial\Strategy\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Strategy\Adapter\Strategy\IStrategyAdapter;
use Trial\Strategy\Adapter\Strategy\StrategyMockAdapter;
use Trial\Strategy\Adapter\Strategy\StrategyRestfulAdapter;

class StrategyRepository extends Repository implements IStrategyAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait;

    const LIST_MODEL_UN = 'STRATEGY_LIST';
    const FETCH_ONE_MODEL_UN = 'STRATEGY_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new StrategyRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function getActualAdapter() : IStrategyAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IStrategyAdapter
    {
        return new StrategyMockAdapter();
    }
}
