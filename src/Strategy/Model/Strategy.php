<?php
namespace Trial\Strategy\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;
use Marmot\Core;

use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Crew\Model\Crew;
use Trial\Strategy\Repository\StrategyRepository;

class Strategy implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const STRATEGY_ID = 1;

    private $id;

    private $commission;

    private $subsidy;

    private $minProfit;

    private $maxProfit;

    private $isCanEdit;

    private $crew;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->commission = 0;
        $this->subsidy = 0;
        $this->minProfit = 0;
        $this->maxProfit = 0;
        $this->isCanEdit = 0;
        $this->status = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->status = 0;
        $this->repository = new StrategyRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->commission);
        unset($this->subsidy);
        unset($this->minProfit);
        unset($this->maxProfit);
        unset($this->isCanEdit);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    protected function getRepository() : StrategyRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCommission(int $commission) : void
    {
        $this->commission = $commission;
    }

    public function getCommission() : int
    {
        return $this->commission;
    }

    public function setSubsidy(int $subsidy) : void
    {
        $this->subsidy = $subsidy;
    }

    public function getSubsidy() : int
    {
        return $this->subsidy;
    }

    public function setMinProfit(int $minProfit) : void
    {
        $this->minProfit = $minProfit;
    }

    public function getMinProfit() : int
    {
        return $this->minProfit;
    }

    public function setMaxProfit(int $maxProfit) : void
    {
        $this->maxProfit = $maxProfit;
    }

    public function getMaxProfit() : int
    {
        return $this->maxProfit;
    }

    public function setIsCanEdit(int $isCanEdit) : void
    {
        $this->isCanEdit = $isCanEdit;
    }

    public function getIsCanEdit() : int
    {
        return $this->isCanEdit;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function edit(): bool
    {
        $repository = $this->getRepository();

        return $repository->edit($this);
    }
}
