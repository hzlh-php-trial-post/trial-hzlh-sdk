<?php
namespace Trial\Strategy\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;
use Trial\Crew\Translator\CrewRestfulTranslator;
use Trial\Strategy\Model\NullStrategy;
use Trial\Strategy\Model\Strategy;

class StrategyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $strategy = null)
    {
        return $this->translateToObject($expression, $strategy);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $strategy = null)
    {
        if (empty($expression)) {
            return NullStrategy::getInstance();
        }

        if ($strategy == null) {
            $strategy = new Strategy();
        }

        $data = $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $strategy->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['commission'])) {
            $strategy->setCommission($attributes['commission']);
        }
        if (isset($attributes['subsidy'])) {
            $strategy->setSubsidy($attributes['subsidy']);
        }
        if (isset($attributes['isCanEdit'])) {
            $strategy->setIsCanEdit($attributes['isCanEdit']);
        }
        if (isset($attributes['maxProfit'])) {
            $strategy->setMaxProfit($attributes['maxProfit']);
        }
        if (isset($attributes['minProfit'])) {
            $strategy->setMinProfit($attributes['minProfit']);
        }
        if (isset($attributes['createTime'])) {
            $strategy->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $strategy->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $strategy->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $strategy->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($expression['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $strategy->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $strategy;
    }


    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($strategy, array $keys = array())
    {
        $expression = array();

        if (!$strategy instanceof Strategy) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'commission',
                'subsidy',
                'isCanEdit',
                'maxProfit',
                'minProfit',
                'status',
                // 'createTime',
                // 'updateTime',
                // 'statusTime',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'strategyConfigs'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $strategy->getId();
        }

        $attributes = array();

        if (in_array('commission', $keys)) {
            $attributes['commission'] = $strategy->getCommission();
        }
        if (in_array('subsidy', $keys)) {
            $attributes['subsidy'] = $strategy->getSubsidy();
        }
        if (in_array('isCanEdit', $keys)) {
            $attributes['isCanEdit'] = $strategy->getIsCanEdit();
        }
        if (in_array('maxProfit', $keys)) {
            $attributes['maxProfit'] = $strategy->getMaxProfit();
        }
        if (in_array('minProfit', $keys)) {
            $attributes['minProfit'] = $strategy->getMinProfit();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $strategy->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $strategy->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $strategy->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $strategy->getStatusTime();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crew',
                    'id' => $strategy->getCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
