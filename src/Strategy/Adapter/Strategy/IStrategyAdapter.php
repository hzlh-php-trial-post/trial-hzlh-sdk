<?php
namespace Trial\Strategy\Adapter\Strategy;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IStrategyAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{

}
