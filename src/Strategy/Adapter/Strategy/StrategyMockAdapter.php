<?php
namespace Trial\Strategy\Adapter\Strategy;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Model\IOperatAble;

use Trial\Strategy\Utils\MockFactory;

class StrategyMockAdapter implements IStrategyAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateStrategyObject($id);
    }

    public function fetchList(array $ids): array
    {
        $strategyList = array();

        foreach ($ids as $id) {
            $strategyList[] = MockFactory::generateStrategyObject($id);
        }

        return $strategyList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateStrategyObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $strategyList = array();

        foreach ($ids as $id) {
            $strategyList[] = MockFactory::generateStrategyObject($id);
        }

        return $strategyList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function edit(IOperatAble $operatAbleObject) : bool
    {
        unset($operatAbleObject);

        return true;
    }
}
