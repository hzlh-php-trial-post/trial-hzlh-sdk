<?php
namespace Trial\Strategy\Adapter\Strategy;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Trial\Strategy\Model\NullStrategy;
use Trial\Strategy\Model\Strategy;
use Trial\Strategy\Translator\StrategyRestfulTranslator;

class StrategyRestfulAdapter extends GuzzleAdapter implements IStrategyAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    const SCENARIOS = [
        'STRATEGY_LIST'=>[
            'fields'=>[],
            'include'=>[]
        ],
        'STRATEGY_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>[]
        ]
    ];
    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new StrategyRestfulTranslator();
        $this->resource = 'strategyConfigs';
        $this->scenario = array();
    }

    protected function getMapError() : array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullStrategy::getInstance());
    }

    protected function addAction(Strategy $strategy): bool
    {
        unset($strategy);
        return false;
    }

    protected function editAction(Strategy $strategy): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $strategy,
            array(
                "commission",
                "subsidy",
                "isCanEdit",
                "maxProfit",
                "minProfit",
                "crew"
            )
        );

        $this->patch(
            $this->getResource().'/'.$strategy->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($strategy);
            return true;
        }

        return false;
    }
}
