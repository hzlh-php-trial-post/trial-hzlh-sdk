<?php
namespace Trial\Service\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\ApplyAbleRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ResubmitAbleRepositoryTrait;
use Trial\Common\Repository\ModifyStatusAbleRepositoryTrait;

use Trial\Service\Adapter\Service\IServiceAdapter;
use Trial\Service\Adapter\Service\ServiceMockAdapter;
use Trial\Service\Adapter\Service\ServiceRestfulAdapter;

use Trial\Service\Model\Service;

class ServiceRepository extends Repository implements IServiceAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ApplyAbleRepositoryTrait,
        OperatAbleRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ModifyStatusAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_SERVICE_LIST';
    const PORTAL_LIST_MODEL_UN = 'PORTAL_SERVICE_LIST';
    const FETCH_ONE_MODEL_UN = 'SERVICE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ServiceRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getMockAdapter(): IServiceAdapter
    {
        return new ServiceMockAdapter();
    }

    public function getActualAdapter(): IServiceAdapter
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function portalFetchOne($id)
    {
        return $this->getAdapter()->portalFetchOne($id);
    }

    public function onShelf(Service $service): bool
    {
        return $this->getAdapter()->onShelf($service);
    }

    public function offStock(Service $service): bool
    {
        return $this->getAdapter()->offStock($service);
    }

    public function tagSearchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) {
        return $this->getAdapter()->tagSearchAsync($filter, $sort, $offset, $size);
    }
}
