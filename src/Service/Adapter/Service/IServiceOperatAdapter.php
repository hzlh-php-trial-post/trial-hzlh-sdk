<?php
namespace Trial\Service\Adapter\Service;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;

interface IServiceOperatAdapter extends IOperatAbleAdapter, IResubmitAbleAdapter, IApplyAbleAdapter
{
}
