<?php
namespace Trial\Service\Adapter\Service;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IModifyStatusAbleAdapter;

interface IServiceAdapter extends IAsyncAdapter, IFetchAbleAdapter, IServiceOperatAdapter, IModifyStatusAbleAdapter
{
}
