<?php
namespace Trial\Service\Adapter\Service;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Server;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ResubmitAbleRestfulAdapterTrait;
use Trial\Common\Adapter\ModifyStatusAbleRestfulAdapterTrait;

use Trial\Service\Model\NullService;
use Trial\Service\Model\Service;

use Trial\Service\Translator\ServiceRestfulTranslator;

class ServiceRestfulAdapter extends GuzzleAdapter implements IServiceAdapter
{
    use CommonMapErrorsTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        ResubmitAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ModifyStatusAbleRestfulAdapterTrait;

    const SCENARIOS = [
        'OA_SERVICE_LIST' => [
            'fields' => [
                'services'=>
                    'number,title,cover,serviceCategory,enterprise,applyStatus,updateTime,status,minPrice,volume,tag'
            ],
            'include' => 'enterprise,snapshots,serviceCategory,tag',
        ],
        'PORTAL_SERVICE_LIST' => [
            'fields' => [],
            'include' => 'enterprise,snapshots,serviceCategory,tag',
        ],
        'SERVICE_FETCH_ONE' => [
            'fields' => [],
            'include' => 'enterprise,snapshots,serviceCategory,tag',
        ],
    ];

    private $translator;

    private $resource;

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ServiceRestfulTranslator();
        $this->resource = 'services';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullService::getInstance());
    }

    public function portalFetchOne($id)
    {
        return $this->portalFetchOneAction($id, NullService::getInstance());
    }

    protected function portalFetchOneAction(int $id, INull $null)
    {
        $header = array(
            'session_id' => session_id(),
            'ip' => Server::get('REMOTE_ADDR'),
            'member_id' => Core::$container->get('user')->getId()
        );

        $this->get(
            $this->getResource().'/'.$id,
            array(),
            $header
        );

        return $this->isSuccess() ? $this->translateToObject() : $null;
    }

    /**
     * [addAction 发布服务]
     * @param Service $service [object]
     * @return [type]           [bool]
     */
    protected function addAction(Service $service): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $service,
            array(
                'serviceCategory',
                'title',
                'cover',
                'price',
                'contract',
                'detail',
                'serviceObjects',
                'enterprise',
                'tag',
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($service);
            return true;
        }

        return false;
    }
    /**
     * [editAction 编辑]
     * @param  Service $service [object]
     * @return [type]           [bool]
     */
    protected function editAction(Service $service): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $service,
            array(
                'serviceCategory',
                'title',
                'cover',
                'price',
                'contract',
                'detail',
                'serviceObjects',
                'tag',
            )
        );
        
        $this->patch(
            $this->getResource() . '/' . $service->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($service);
            return true;
        }

        return false;
    }
    /**
     * [resubmitAction 重新认证]
     * @param  Service $service [object]
     * @return [type]           [bool]
     */
    protected function resubmitAction(Service $service): bool
    {
        $data = $this->getTranslator()->objectToArray(
            $service,
            array(
                'serviceCategory',
                'title',
                'cover',
                'price',
                'contract',
                'detail',
                'serviceObjects',
                'tag',
            )
        );

        $this->patch(
            $this->getResource() . '/' . $service->getId() . '/resubmit',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($service);
            return true;
        }

        return false;
    }
    /**
     * [onShelf 上架服务]
     * @param  Service $service [object]
     * @return [type]           [bool]
     */
    public function onShelf(Service $service): bool
    {
        return $this->onShelfAction($service);
    }

    protected function onShelfAction(Service $service): bool
    {
        $this->patch(
            $this->getResource() . '/' . $service->getId() . '/onShelf'
        );
        if ($this->isSuccess()) {
            $this->translateToObject($service);
            return true;
        }
        return false;
    }
    /**
     * [offStock 下架服务]
     * @param  Service $service [object]
     * @return [type]           [bool]
     */
    public function offStock(Service $service): bool
    {
        return $this->offStockAction($service);
    }

    protected function offStockAction(Service $service): bool
    {
        $this->patch(
            $this->getResource() . '/' . $service->getId() . '/offStock'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($service);
            return true;
        }
        return false;
    }
}
