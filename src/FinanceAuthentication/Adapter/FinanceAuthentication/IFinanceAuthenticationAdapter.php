<?php
namespace Trial\FinanceAuthentication\Adapter\FinanceAuthentication;

use Marmot\Interfaces\IAsyncAdapter;
use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IFinanceAuthenticationAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
