<?php
namespace Trial\FinanceAuthentication\Adapter\FinanceAuthentication;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;
use Trial\Common\Adapter\IFetchAbleAdapter;
use Marmot\Interfaces\IAsyncAdapter;

interface IUnAuditedFinanceAuthenticationAdapter extends IResubmitAbleAdapter, IApplyAbleAdapter, IFetchAbleAdapter, IAsyncAdapter //phpcs:ignore
{
}
