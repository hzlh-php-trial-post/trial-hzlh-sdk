<?php
namespace Trial\FinanceAuthentication\Model;

use Trial\Common\Model\IApplyAble;
use Trial\Common\Model\IResubmitAble;

use Trial\Common\Model\ApplyAbleTrait;
use Trial\Common\Model\ResubmitAbleTrait;

use Trial\Common\Adapter\IApplyAbleAdapter;
use Trial\Common\Adapter\IResubmitAbleAdapter;

use Trial\FinanceAuthentication\Repository\UnAuditedFinanceAuthenticationRepository;

class UnAuditedFinanceAuthentication extends FinanceAuthentication implements IApplyAble, IResubmitAble
{
    use ApplyAbleTrait, ResubmitAbleTrait;

    private $rejectReason;

    private $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->rejectReason = '';
        $this->applyStatus = IApplyAble::APPLY_STATUS['PENDING'];
        $this->repository = new UnAuditedFinanceAuthenticationRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->rejectReason);
        unset($this->applyStatus);
        unset($this->repository);
    }

    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIResubmitAbleAdapter() : IResubmitAbleAdapter
    {
        return $this->getRepository();
    }
}
