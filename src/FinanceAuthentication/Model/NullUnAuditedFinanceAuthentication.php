<?php
namespace Trial\FinanceAuthentication\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Trial\Common\Model\NullResubmitAbleTrait;
use Trial\Common\Model\NullApplyAbleTrait;

class NullUnAuditedFinanceAuthentication extends UnAuditedFinanceAuthentication implements INull
{
    use NullResubmitAbleTrait,NullApplyAbleTrait;
    
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
