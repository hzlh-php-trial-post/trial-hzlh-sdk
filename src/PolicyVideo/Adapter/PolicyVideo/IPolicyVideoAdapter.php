<?php
namespace Trial\PolicyVideo\Adapter\PolicyVideo;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;
use Trial\Common\Adapter\IRecommendAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IPolicyVideoAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IOnShelfAbleAdapter, IRecommendAbleAdapter//phpcs:ignore
{
}
