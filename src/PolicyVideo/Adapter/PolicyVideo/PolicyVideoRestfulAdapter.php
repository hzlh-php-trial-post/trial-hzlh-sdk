<?php
namespace Trial\PolicyVideo\Adapter\PolicyVideo;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\PolicyVideo\Model\PolicyVideo;
use Trial\PolicyVideo\Model\NullPolicyVideo;
use Trial\PolicyVideo\Translator\PolicyVideoRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\RecommendAbleRestfulAdapterTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class PolicyVideoRestfulAdapter extends GuzzleAdapter implements IPolicyVideoAdapter
{
    use CommonMapErrorsTrait,
        RecommendAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        OnShelfAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_POLICY_VIDEO_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'PORTAL_POLICY_VIDEO_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'POLICY_VIDEO_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PolicyVideoRestfulTranslator();
        $this->resource = 'policyVideos';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPolicyVideo::getInstance());
    }

    protected function addAction(PolicyVideo $policyVideo) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policyVideo,
            array(
                'title',
                'source',
                'cover',
                'videoLink',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policyVideo);
            return true;
        }

        return false;
    }

    protected function editAction(PolicyVideo $policyVideo) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policyVideo,
            array(
                'title',
                'source',
                'cover',
                'videoLink'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$policyVideo->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policyVideo);
            return true;
        }

        return false;
    }
}
