<?php
namespace Trial\PolicyVideo\Adapter\PolicyVideo;

use Trial\Common\Adapter\RecommendAbleMockAdapterTrait;
use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleMockAdapterTrait;

use Trial\PolicyVideo\Utils\MockFactory;

class PolicyVideoMockAdapter implements IPolicyVideoAdapter
{
    use OperatAbleMockAdapterTrait, OnShelfAbleMockAdapterTrait, RecommendAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generatePolicyVideoObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $policyVideoList = array();

        foreach ($ids as $id) {
            $policyVideoList[] = MockFactory::generatePolicyVideoObject($id);
        }

        return $policyVideoList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generatePolicyVideoObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generatePolicyVideoObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
