<?php
namespace Trial\PolicyVideo\Model;

use Trial\Common\Model\IRecommendAble;
use Trial\Common\Model\IOnShelfAble;

class PolicyVideoModelFactory
{
    const RECOMMEND_CN = array(
        IRecommendAble::RECOMMEND_HOMEPAGE_STATUS['YES'] => '已推荐',
        IRecommendAble::RECOMMEND_HOMEPAGE_STATUS['NO'] => '未推荐'
    );

    const STATUS_CN = array(
        IOnShelfAble::STATUS['ONSHELF'] => '已上架',
        IOnShelfAble::STATUS['OFFSTOCK'] => '已下架'
    );
}
