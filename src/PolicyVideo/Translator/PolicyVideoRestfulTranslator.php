<?php
namespace Trial\PolicyVideo\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\PolicyVideo\Model\PolicyVideo;
use Trial\PolicyVideo\Model\NullPolicyVideo;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Crew\Translator\CrewRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class PolicyVideoRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $policyVideo = null)
    {
        return $this->translateToObject($expression, $policyVideo);
    }

    protected function translateToObject(array $expression, $policyVideo = null)
    {
        if (empty($expression)) {
            return NullPolicyVideo::getInstance();
        }
        if ($policyVideo === null) {
            $policyVideo = new PolicyVideo();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $policyVideo->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $policyVideo->setTitle($attributes['title']);
        }
        if (isset($attributes['number'])) {
            $policyVideo->setNumber($attributes['number']);
        }
        if (isset($attributes['source'])) {
            $policyVideo->setSource($attributes['source']);
        }
        if (isset($attributes['cover'])) {
            $policyVideo->setCover($attributes['cover']);
        }
        if (isset($attributes['videoLink'])) {
            $policyVideo->setVideoLink($attributes['videoLink']);
        }
        if (isset($attributes['recommendStatus'])) {
            $policyVideo->setRecommendStatus($attributes['recommendStatus']);
        }
        if (isset($attributes['status'])) {
            $policyVideo->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $policyVideo->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $policyVideo->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $policyVideo->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $policyVideo->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $policyVideo;
    }

    public function objectToArray($policyVideo, array $keys = array())
    {
        $expression = array();

        if (!$policyVideo instanceof PolicyVideo) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'title',
                'source',
                'cover',
                'videoLink',
                'recommendStatus',
                'crew',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'policyVideos'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $policyVideo->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $policyVideo->getTitle();
        }
        if (in_array('source', $keys)) {
            $attributes['source'] = $policyVideo->getSource();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $policyVideo->getCover();
        }
        if (in_array('videoLink', $keys)) {
            $attributes['videoLink'] = $policyVideo->getVideoLink();
        }
        if (in_array('recommendStatus', $keys)) {
            $attributes['recommendStatus'] = $policyVideo->getRecommendStatus();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $policyVideo->getCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
