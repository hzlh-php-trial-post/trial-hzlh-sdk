<?php
namespace Trial\PolicyVideo\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\RecommendAbleRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\OnShelfAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\PolicyVideo\Model\PolicyVideo;
use Trial\PolicyVideo\Adapter\PolicyVideo\IPolicyVideoAdapter;
use Trial\PolicyVideo\Adapter\PolicyVideo\PolicyVideoMockAdapter;
use Trial\PolicyVideo\Adapter\PolicyVideo\PolicyVideoRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class PolicyVideoRepository extends Repository implements IPolicyVideoAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait,
        RecommendAbleRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_POLICY_VIDEO_LIST'; //OA列表场景
    const PORTAL_LIST_MODEL_UN = 'PORTAL_POLICY_VIDEO_LIST'; //门户列表场景
    const FETCH_ONE_MODEL_UN = 'POLICY_VIDEO_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PolicyVideoRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IPolicyVideoAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IPolicyVideoAdapter
    {
        return new PolicyVideoMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
