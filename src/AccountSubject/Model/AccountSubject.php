<?php
namespace Trial\AccountSubject\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\AccountTemplate\Model\AccountTemplate;

use Trial\AccountSubject\Repository\AccountSubjectRepository;

class AccountSubject implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const CATEGORY = array(
        'NULL' => 0,//未知
        'ASSETS' => 1,//资产类
        'LIABILITIES' => 2,//负债类
        'EQUITY' => 3,//权益类
        'COST' => 4, //成本类
        'PROFIT_AND_LOSS' => 5//损益类
    );

    const DIRECTION = array(
        'BORROW' => 1,//借
        'LOAN' => 2//贷
    );

    const LEVEL = array(
        'ONE' => 1,
        'TWO' => 2,
        'THREE' => 3,
        'FOUR' => 4
    );

    const STATUS = array(
        'NORMAL' => 0, //正常
        'NOT_AVAILABLE' => -2, //不可用
        'DELETED' => -4 //已删除
    );
    /**
     * [$id 主键Id]
     * @var [int]
     */
    private $id;
    /**
     * [$name 科目名称]
     * @var [string]
     */
    private $name;
    /**
     * [$number 科目编码 ]
     * @var [string]
     */
    private $number;
    /**
     * [$parentId 上级id ]
     * @var [int]
     */
    private $parentId;
    /**
     * [$category 科目类别 ]
     * @var [int]
     */
    private $category;
    /**
     * [$balanceDirection 余额方向 ]
     * @var [int]
     */
    private $balanceDirection;
    /**
     * [$level 级别 ]
     * @var [int]
     */
    private $level;
    /**
     * [$accountTemplate 账套信息]
     * @var [AccountTemplate]
     */
    private $accountTemplate;
    /**
     * [$subjectId 科目id _前为id,_后为科目的key值]
     * @var [subjectId]
     */
    private $subjectId;
    /**
     * [$subject 科目数组]
     * @var [subject]
     */
    private $subject;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->number = '';
        $this->parentId = 0;
        $this->category = self::CATEGORY['NULL'];
        $this->balanceDirection = self::DIRECTION['BORROW'];
        $this->level = self::LEVEL['ONE'];
        $this->accountTemplate = new AccountTemplate();
        $this->subjectId = '';
        $this->subject = array();
        $this->status = self::STATUS['NORMAL'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new AccountSubjectRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->number);
        unset($this->parentId);
        unset($this->category);
        unset($this->balanceDirection);
        unset($this->level);
        unset($this->accountTemplate);
        unset($this->subjectId);
        unset($this->subject);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setParentId(int $parentId): void
    {
        $this->parentId = $parentId;
    }

    public function getParentId(): int
    {
        return $this->parentId;
    }

    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function setBalanceDirection(int $balanceDirection): void
    {
        $this->balanceDirection = $balanceDirection;
    }

    public function getBalanceDirection(): int
    {
        return $this->balanceDirection;
    }

    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setAccountTemplate(AccountTemplate $accountTemplate): void
    {
        $this->accountTemplate = $accountTemplate;
    }

    public function getAccountTemplate(): AccountTemplate
    {
        return $this->accountTemplate;
    }
    
    public function setSubjectId(string $subjectId): void
    {
        $this->subjectId = $subjectId;
    }

    public function getSubjectId(): string
    {
        return $this->subjectId;
    }

    public function setSubject(array $subject): void
    {
        $this->subject = $subject;
    }

    public function getSubject(): array
    {
        return $this->subject;
    }

    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(self::STATUS)) ?
            $status : self::STATUS['NORMAL'];
    }

    protected function getRepository(): AccountSubjectRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter(): IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function deletes() : bool
    {
        return $this->getRepository()->deletes($this);
    }
}
