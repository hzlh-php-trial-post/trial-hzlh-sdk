<?php
namespace Trial\AccountSubject\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\AccountSubject\Adapter\AccountSubject\IAccountSubjectAdapter;
use Trial\AccountSubject\Adapter\AccountSubject\AccountSubjectMockAdapter;
use Trial\AccountSubject\Adapter\AccountSubject\AccountSubjectRestfulAdapter;
use Trial\AccountSubject\Model\AccountSubject;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class AccountSubjectRepository extends Repository implements IAccountSubjectAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ACCOUNT_SUBJECT_LIST';
    const FETCH_ONE_MODEL_UN = 'ACCOUNT_SUBJECT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new AccountSubjectRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IAccountSubjectAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IAccountSubjectAdapter
    {
        return new AccountSubjectMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(AccountSubject $accountTemplate) : bool
    {
        return $this->getAdapter()->deletes($accountTemplate);
    }
}
