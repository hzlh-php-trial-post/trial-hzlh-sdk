<?php
namespace Trial\AccountSubject\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\AccountSubject\Model\AccountSubject;
use Trial\AccountSubject\Model\NullAccountSubject;

use Trial\AccountTemplate\Translator\AccountTemplateRestfulTranslator;

class AccountSubjectRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getAccountTemplateRestfulTranslator()
    {
        return new AccountTemplateRestfulTranslator();
    }

    public function arrayToObject(array $expression, $accountSubject = null)
    {
        return $this->translateToObject($expression, $accountSubject);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $accountSubject = null)
    {
        if (empty($expression)) {
            return NullAccountSubject::getInstance();
        }

        if ($accountSubject == null) {
            $accountSubject = new AccountSubject();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $accountSubject->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $accountSubject->setName($attributes['name']);
        }
        if (isset($attributes['number'])) {
            $accountSubject->setNumber($attributes['number']);
        }
        if (isset($attributes['parentId'])) {
            $accountSubject->setParentId($attributes['parentId']);
        }
        if (isset($attributes['category'])) {
            $accountSubject->setCategory($attributes['category']);
        }
        if (isset($attributes['balanceDirection'])) {
            $accountSubject->setBalanceDirection($attributes['balanceDirection']);
        }
        if (isset($attributes['level'])) {
            $accountSubject->setLevel($attributes['level']);
        }
        if (isset($attributes['status'])) {
            $accountSubject->setStatus($attributes['status']);
        }
        if (isset($attributes['subjectId'])) {
            $accountSubject->setSubjectId($attributes['subjectId']);
        }
        if (isset($attributes['subject'])) {
            $accountSubject->setSubject($attributes['subject']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['accountTemplate']['data'])) {
            $accountTemplate = $this->changeArrayFormat($relationships['accountTemplate']['data']);
            $accountSubject->setAccountTemplate(
                $this->getAccountTemplateRestfulTranslator()->arrayToObject($accountTemplate)
            );
        }

        return $accountSubject;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($accountSubject, array $keys = array())
    {
        if (!$accountSubject instanceof AccountSubject) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'name',
                'number',
                'parentId',
                'category',
                'balanceDirection',
                'status',
                'accountTemplate'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'accountSubjects'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $accountSubject->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $accountSubject->getName();
        }
        if (in_array('number', $keys)) {
            $attributes['number'] = $accountSubject->getNumber();
        }
        if (in_array('parentId', $keys)) {
            $attributes['parentId'] = $accountSubject->getParentId();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $accountSubject->getCategory();
        }
        if (in_array('balanceDirection', $keys)) {
            $attributes['balanceDirection'] = $accountSubject->getBalanceDirection();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $accountSubject->getStatus();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('accountTemplate', $keys)) {
            $expression['data']['relationships']['accountTemplate']['data'] = array(
                array(
                    'type'=>'accountTemplates',
                    'id'=>$accountSubject->getAccountTemplate()->getId()
                )
            );
        }

        return $expression;
    }
}
