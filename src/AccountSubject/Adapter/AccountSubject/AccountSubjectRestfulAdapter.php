<?php
namespace Trial\AccountSubject\Adapter\AccountSubject;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\AccountSubject\Model\AccountSubject;
use Trial\AccountSubject\Model\NullAccountSubject;
use Trial\AccountSubject\Translator\AccountSubjectRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class AccountSubjectRestfulAdapter extends GuzzleAdapter implements IAccountSubjectAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'ACCOUNT_SUBJECT_LIST'=>[
            'fields'=>[],
            'include'=> 'accountTemplate'
        ],
        'ACCOUNT_SUBJECT_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'accountTemplate'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new AccountSubjectRestfulTranslator();
        $this->resource = 'accountSubjects';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullAccountSubject::getInstance());
    }

    protected function addAction(AccountSubject $accountSubject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $accountSubject,
            array(
                'name',
                'number',
                'parentId',
                'category',
                'balanceDirection',
                'status',
                'accountTemplate'
            )
        );
        
        $this->post(
            $this->getResource().'/'.$accountSubject->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($accountSubject);
            return true;
        }

        return false;
    }

    protected function editAction(AccountSubject $accountSubject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $accountSubject,
            array(
                'name',
                'status',
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$accountSubject->getSubjectId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($accountSubject);
            return true;
        }

        return false;
    }

    public function deletes(AccountSubject $accountSubject) : bool
    {
        $this->delete(
            $this->getResource().'/'.$accountSubject->getSubjectId().'/delete'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($accountSubject);
            return true;
        }
        return false;
    }
}
