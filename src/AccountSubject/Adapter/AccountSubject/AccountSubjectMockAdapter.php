<?php
namespace Trial\AccountSubject\Adapter\AccountSubject;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;

use Trial\AccountSubject\Model\AccountSubject;
use Trial\AccountSubject\Utils\MockFactory;

class AccountSubjectMockAdapter implements IAccountSubjectAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateAccountSubjectObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $accountSubjectList = array();

        foreach ($ids as $id) {
            $accountSubjectList[] = MockFactory::generateAccountSubjectObject($id);
        }

        return $accountSubjectList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
    
    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateAccountSubjectObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $accountSubjectList = array();

        foreach ($ids as $id) {
            $accountSubjectList[] = MockFactory::generateAccountSubjectObject($id);
        }

        return $accountSubjectList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
