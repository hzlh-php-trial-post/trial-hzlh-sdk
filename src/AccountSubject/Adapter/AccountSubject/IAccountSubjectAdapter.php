<?php
namespace Trial\AccountSubject\Adapter\AccountSubject;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface IAccountSubjectAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
