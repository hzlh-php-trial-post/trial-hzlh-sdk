<?php
namespace Trial\ReportFormDraft\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Trial\Common\Model\IOperatAble;
use Trial\Common\Model\OperatAbleTrait;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\AccountTemplate\Model\AccountTemplate;

use Trial\ReportFormDraft\Repository\ReportFormDraftRepository;

class ReportFormDraft implements IObject, IOperatAble
{
    use Object, OperatAbleTrait;

    const TYPE = array(
        'BALANCE_SHEET' => 1, //资产负债表
        'INCOME_STATEMENT' => 2, //利润表
        'CASH_FLOW_STATEMENT' => 3 //现金流量表
    );

    const STATUS = array(
        'NORMAL' => 0, //正常
        'MODIFIED' => 1 //已修改
    );

    private $id;

    private $type;

    private $accountTemplate;
    
    private $reportForm;

    private $remark;
    
    private $accountingPeriod;

    private $lineId;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->type = self::TYPE['CASH_FLOW_STATEMENT'];
        $this->accountTemplate = new AccountTemplate();
        $this->reportForm = array();
        $this->remark = '';
        $this->accountingPeriod = 0;
        $this->lineId = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->repository = new ReportFormDraftRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->type);
        unset($this->accountTemplate);
        unset($this->reportForm);
        unset($this->remark);
        unset($this->accountingPeriod);
        unset($this->lineId);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setType(string $type) : void
    {
        $this->type = $type;
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function setAccountTemplate(AccountTemplate $accountTemplate) : void
    {
        $this->accountTemplate = $accountTemplate;
    }

    public function getAccountTemplate() : AccountTemplate
    {
        return $this->accountTemplate;
    }

    public function setLineId(int $lineId) : void
    {
        $this->lineId = $lineId;
    }

    public function getLineId() : int
    {
        return $this->lineId;
    }

    public function setReportForm(array $reportForm) : void
    {
        $this->reportForm = $reportForm;
    }

    public function getReportForm() : array
    {
        return $this->reportForm;
    }
    
    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setAccountingPeriod(int $accountingPeriod) : void
    {
        $this->accountingPeriod = $accountingPeriod;
    }

    public function getAccountingPeriod() : int
    {
        return $this->accountingPeriod;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
    
    public function resetAll() : bool
    {
        return $this->getRepository()->resetAll($this);
    }

    public function resetSingle() : bool
    {
        return $this->getRepository()->resetSingle($this);
    }

    protected function getRepository() : ReportFormDraftRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
