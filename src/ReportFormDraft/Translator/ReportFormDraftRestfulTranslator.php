<?php
namespace Trial\ReportFormDraft\Translator;

use Trial\ReportFormDraft\Model\ReportFormDraft;
use Trial\ReportFormDraft\Model\NullReportFormDraft;
use Trial\Common\Translator\RestfulTranslatorTrait;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\AccountTemplate\Translator\AccountTemplateRestfulTranslator;

class ReportFormDraftRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $reportFormDraft = null)
    {
        return $this->translateToObject($expression, $reportFormDraft);
    }

    public function getAccountTemplateRestfulTranslator()
    {
        return new AccountTemplateRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $reportFormDraft = null)
    {
        if (empty($expression)) {
            return NullReportFormDraft::getInstance();
        }

        if ($reportFormDraft == null) {
            $reportFormDraft = new ReportFormDraft();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $reportFormDraft->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['reportForm'])) {
            $reportFormDraft->setReportForm($attributes['reportForm']);
        }
        if (isset($attributes['type'])) {
            $reportFormDraft->setType($attributes['type']);
        }
        if (isset($attributes['accountingPeriod'])) {
            $reportFormDraft->setAccountingPeriod($attributes['accountingPeriod']);
        }
        if (isset($attributes['createTime'])) {
            $reportFormDraft->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $reportFormDraft->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $reportFormDraft->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $reportFormDraft->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['accountTemplate']['data'])) {
            $accountTemplate = $this->changeArrayFormat($relationships['accountTemplate']['data']);
            $reportFormDraft->setAccountTemplate(
                $this->getAccountTemplateRestfulTranslator()->arrayToObject($accountTemplate)
            );
        }

        return $reportFormDraft;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($reportFormDraft, array $keys = array())
    {
        $expression = array();

        if (!$reportFormDraft instanceof ReportFormDraft) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'type',
                'reportForm',
                'accountingPeriod',
                'accountTemplate',
                'lineId'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'reportFormDrafts'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $reportFormDraft->getId();
        }

        $attributes = array();

        if (in_array('type', $keys)) {
            $attributes['type'] = $reportFormDraft->getType();
        }
        if (in_array('reportForm', $keys)) {
            $attributes['reportForm'] = $reportFormDraft->getReportForm();
        }
        if (in_array('lineId', $keys)) {
            $attributes['lineId'] = $reportFormDraft->getLineId();
        }
        if (in_array('accountingPeriod', $keys)) {
            $attributes['accountingPeriod'] = $reportFormDraft->getAccountingPeriod();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('accountTemplate', $keys)) {
            $expression['data']['relationships']['accountTemplate']['data'] = array(
                array(
                    'type'=>'accountTemplates',
                    'id'=>$reportFormDraft->getAccountTemplate()->getId()
                )
            );
        }

        return $expression;
    }
}
