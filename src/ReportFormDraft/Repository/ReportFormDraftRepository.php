<?php
namespace Trial\ReportFormDraft\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;

use Trial\ReportFormDraft\Adapter\ReportFormDraft\IReportFormDraftAdapter;
use Trial\ReportFormDraft\Adapter\ReportFormDraft\ReportFormDraftMockAdapter;
use Trial\ReportFormDraft\Adapter\ReportFormDraft\ReportFormDraftRestfulAdapter;

use Trial\ReportFormDraft\Model\ReportFormDraft;

class ReportFormDraftRepository extends Repository implements IReportFormDraftAdapter
{
    use AsyncRepositoryTrait, FetchRepositoryTrait, OperatAbleRepositoryTrait, ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'REPORT_FORM_DRAFT_LIST';
    const FETCH_ONE_MODEL_UN = 'REPORT_FORM_DRAFT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ReportFormDraftRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IReportFormDraftAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IReportFormDraftAdapter
    {
        return new ReportFormDraftMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function resetSingle(ReportFormDraft $reportFormDraft) : bool
    {
        return $this->getAdapter()->resetSingle($reportFormDraft);
    }

    public function resetAll(ReportFormDraft $reportFormDraft) : bool
    {
        return $this->getAdapter()->resetAll($reportFormDraft);
    }
}
