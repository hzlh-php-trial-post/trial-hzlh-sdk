<?php
namespace Trial\ReportFormDraft\Adapter\ReportFormDraft;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;
use Trial\Common\Adapter\EnableAbleMockAdapterTrait;

use Trial\ReportFormDraft\Model\ReportFormDraft;
use Trial\ReportFormDraft\Utils\MockFactory;

class ReportFormDraftMockAdapter implements IReportFormDraftAdapter
{
    use OperatAbleMockAdapterTrait, EnableAbleMockAdapterTrait;

    public function resetAll(ReportFormDraft $reportFormDraft) : bool
    {
        unset($reportFormDraft);
        return true;
    }

    public function resetSingle(ReportFormDraft $reportFormDraft) : bool
    {
        unset($reportFormDraft);
        return true;
    }

    public function fetchOne($id)
    {
        return MockFactory::generateReportFormDraftObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $reportFormDraftList = array();

        foreach ($ids as $id) {
            $reportFormDraftList[] = MockFactory::generateReportFormDraftObject($id);
        }

        return $reportFormDraftList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateReportFormDraftObject($id);
    }

    public function fetchListAsync(array $ids) : array
    {
        $parentCategoryList = array();

        foreach ($ids as $id) {
            $parentCategoryList[] = MockFactory::generateReportFormDraftObject($id);
        }

        return $parentCategoryList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
