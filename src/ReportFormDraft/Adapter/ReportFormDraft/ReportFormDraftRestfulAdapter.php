<?php
namespace Trial\ReportFormDraft\Adapter\ReportFormDraft;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\ReportFormDraft\Model\ReportFormDraft;
use Trial\ReportFormDraft\Model\NullReportFormDraft;
use Trial\ReportFormDraft\Translator\ReportFormDraftRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class ReportFormDraftRestfulAdapter extends GuzzleAdapter implements IReportFormDraftAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'LABEL_LIST'=>[
                'fields'=>[],
                'include'=> 'accountTemplate'
            ],
            'LABEL_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'accountTemplate'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ReportFormDraftRestfulTranslator();
        $this->resource = 'reportFormDrafts';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapErrors = [
            100 => LABEL_NAME_EXIST
        ];
        $commonMapErrors = $this->commonMapErrors();

        return $mapErrors+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullReportFormDraft::getInstance());
    }

    protected function addAction() : bool
    {
        return false;
    }

    protected function editAction(ReportFormDraft $reportFormDraft) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $reportFormDraft,
            array(
                'reportForm'
            )
        );

        $this->patch(
            $this->getResource().'/'.$reportFormDraft->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($reportFormDraft);
            return true;
        }

        return false;
    }

    public function resetAll(ReportFormDraft $reportFormDraft) : bool
    {
        $this->patch(
            $this->getResource().'/'.$reportFormDraft->getId().'/resetAll'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($reportFormDraft);
            return true;
        }

        return false;
    }

    public function resetSingle(ReportFormDraft $reportFormDraft) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $reportFormDraft,
            array('lineId')
        );

        $this->patch(
            $this->getResource().'/'.$reportFormDraft->getId().'/resetSingle',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($reportFormDraft);
            return true;
        }

        return false;
    }
}
