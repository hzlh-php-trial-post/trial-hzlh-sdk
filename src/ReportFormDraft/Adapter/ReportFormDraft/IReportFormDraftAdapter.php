<?php
namespace Trial\ReportFormDraft\Adapter\ReportFormDraft;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

use Trial\ReportFormDraft\Model\ReportFormDraft;

interface IReportFormDraftAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
    public function resetAll(ReportFormDraft $reportFormDraft) : bool;

    public function resetSingle(ReportFormDraft $reportFormDraft) : bool;
}
