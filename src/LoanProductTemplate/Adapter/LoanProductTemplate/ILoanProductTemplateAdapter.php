<?php
namespace Trial\LoanProductTemplate\Adapter\LoanProductTemplate;

use Marmot\Interfaces\IAsyncAdapter;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;

interface ILoanProductTemplateAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter
{
}
