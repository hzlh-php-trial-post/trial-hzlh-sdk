<?php
namespace Trial\LoanProductTemplate\Adapter\LoanProductTemplate;

use Trial\Common\Adapter\OperatAbleMockAdapterTrait;

use Trial\LoanProductTemplate\Model\LoanProductTemplate;
use Trial\LoanProductTemplate\Utils\MockFactory;

class LoanProductTemplateMockAdapter implements ILoanProductTemplateAdapter
{
    use OperatAbleMockAdapterTrait;

    public function fetchOne($id)
    {
        return MockFactory::generateLoanProductTemplateObject($id);
    }

    public function fetchList(array $ids) : array
    {
        $accountTemplateList = array();

        foreach ($ids as $id) {
            $accountTemplateList[] = MockFactory::generateLoanProductTemplateObject($id);
        }

        return $accountTemplateList;
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) :array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset<$size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }

    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateLoanProductTemplateObject($id);
    }

    public function fetchListAsync(array $ids): array
    {
        $accountTemplateList = array();

        foreach ($ids as $id) {
            $accountTemplateList[] = MockFactory::generateLoanProductTemplateObject($id);
        }

        return $accountTemplateList;
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);

        $ids = [];

        for ($offset; $offset < $size; $offset++) {
            $ids[] = $offset;
        }

        $count = sizeof($ids);
        return array($this->fetchList($ids), $count);
    }
}
