<?php
namespace Trial\LoanProductTemplate\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\LoanProductTemplate\Adapter\LoanProductTemplate\ILoanProductTemplateAdapter;
use Trial\LoanProductTemplate\Adapter\LoanProductTemplate\LoanProductTemplateMockAdapter;
use Trial\LoanProductTemplate\Adapter\LoanProductTemplate\LoanProductTemplateRestfulAdapter;
use Trial\LoanProductTemplate\Model\LoanProductTemplate;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class LoanProductTemplateRepository extends Repository implements ILoanProductTemplateAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        OperatAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'LOAN_PRODUCT_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'LOAN_PRODUCT_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new LoanProductTemplateRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : ILoanProductTemplateAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ILoanProductTemplateAdapter
    {
        return new LoanProductTemplateMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function deletes(LoanProductTemplate $loanProductTemplate) : bool
    {
        return $this->getAdapter()->deletes($loanProductTemplate);
    }
}
