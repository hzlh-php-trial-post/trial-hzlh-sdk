<?php
namespace Trial\PolicySubject\Model;

use Trial\Common\Model\IOnShelfAble;

class PolicySubjectModelFactory
{
    const STATUS_CN = array(
        IOnShelfAble::STATUS['ONSHELF'] => '已上架',
        IOnShelfAble::STATUS['OFFSTOCK'] => '已下架'
    );
}
