<?php
namespace Trial\PolicySubject\Repository;

use Trial\Common\Repository\FetchRepositoryTrait;
use Trial\Common\Repository\OperatAbleRepositoryTrait;
use Trial\Common\Repository\OnShelfAbleRepositoryTrait;
use Trial\Common\Repository\AsyncRepositoryTrait;
use Trial\Common\Repository\ErrorRepositoryTrait;

use Trial\PolicySubject\Model\PolicySubject;
use Trial\PolicySubject\Adapter\PolicySubject\IPolicySubjectAdapter;
use Trial\PolicySubject\Adapter\PolicySubject\PolicySubjectMockAdapter;
use Trial\PolicySubject\Adapter\PolicySubject\PolicySubjectRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class PolicySubjectRepository extends Repository implements IPolicySubjectAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        OnShelfAbleRepositoryTrait,
        ErrorRepositoryTrait,
        AsyncRepositoryTrait;

    private $adapter;

    const OA_LIST_MODEL_UN = 'OA_POLICY_SUBJECT_LIST'; //OA列表场景
    const PORTAL_LIST_MODEL_UN = 'PORTAL_POLICY_SUBJECT_LIST'; //门户列表场景
    const FETCH_ONE_MODEL_UN = 'POLICY_SUBJECT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new PolicySubjectRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IPolicySubjectAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IPolicySubjectAdapter
    {
        return new PolicySubjectMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
