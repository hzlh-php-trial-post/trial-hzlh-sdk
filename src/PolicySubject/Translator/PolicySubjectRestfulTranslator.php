<?php
namespace Trial\PolicySubject\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Trial\PolicySubject\Model\PolicySubject;
use Trial\PolicySubject\Model\NullPolicySubject;

use Trial\Common\Translator\RestfulTranslatorTrait;

use Trial\Crew\Translator\CrewRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class PolicySubjectRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $policySubject = null)
    {
        return $this->translateToObject($expression, $policySubject);
    }

    protected function translateToObject(array $expression, $policySubject = null)
    {
        if (empty($expression)) {
            return NullPolicySubject::getInstance();
        }
        if ($policySubject === null) {
            $policySubject = new PolicySubject();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $policySubject->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $policySubject->setTitle($attributes['title']);
        }
        if (isset($attributes['cover'])) {
            $policySubject->setCover($attributes['cover']);
        }
        if (isset($attributes['status'])) {
            $policySubject->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $policySubject->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $policySubject->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $policySubject->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);
            $policySubject->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $policySubject;
    }

    public function objectToArray($policySubject, array $keys = array())
    {
        $expression = array();

        if (!$policySubject instanceof PolicySubject) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'title',
                'cover',
                'crew',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'policySubjects'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $policySubject->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $policySubject->getTitle();
        }
        if (in_array('cover', $keys)) {
            $attributes['cover'] = $policySubject->getCover();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $policySubject->getCrew()->getId()
                )
            );
        }

        return $expression;
    }
}
