<?php
namespace Trial\PolicySubject\Adapter\PolicySubject;

use Trial\Common\Adapter\IFetchAbleAdapter;
use Trial\Common\Adapter\IOperatAbleAdapter;
use Trial\Common\Adapter\IOnShelfAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IPolicySubjectAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IOnShelfAbleAdapter
{
}
