<?php
namespace Trial\PolicySubject\Adapter\PolicySubject;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Trial\PolicySubject\Model\PolicySubject;
use Trial\PolicySubject\Model\NullPolicySubject;
use Trial\PolicySubject\Translator\PolicySubjectRestfulTranslator;

use Trial\Common\Adapter\CommonMapErrorsTrait;
use Trial\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Trial\Common\Adapter\OnShelfAbleRestfulAdapterTrait;
use Trial\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;

class PolicySubjectRestfulAdapter extends GuzzleAdapter implements IPolicySubjectAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        OnShelfAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'OA_POLICY_SUBJECT_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'PORTAL_POLICY_SUBJECT_LIST'=>[
                'fields'=>[],
                'include'=> 'crew'
            ],
            'POLICY_SUBJECT_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crew'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PolicySubjectRestfulTranslator();
        $this->resource = 'policySubjects';
        $this->scenario = array();
    }

    protected function getMapErrors(): array
    {
        return $this->commonMapErrors();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullPolicySubject::getInstance());
    }

    protected function addAction(PolicySubject $policySubject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policySubject,
            array(
                'title',
                'cover',
                'crew'
            )
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policySubject);
            return true;
        }

        return false;
    }

    protected function editAction(PolicySubject $policySubject) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $policySubject,
            array(
                'title',
                'cover',
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$policySubject->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($policySubject);
            return true;
        }

        return false;
    }
}
